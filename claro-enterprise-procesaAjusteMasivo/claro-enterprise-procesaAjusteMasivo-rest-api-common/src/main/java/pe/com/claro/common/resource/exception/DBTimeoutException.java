package pe.com.claro.common.resource.exception;

/**
 * @author Juan Pablo Quezada.
 * @clase: DBException.java
 * @descripcion Clase para el manejo de excepciones de BD
 * @author_company: CLARO.
 * @fecha_de_creacion: 29-10-2014
 * @fecha_de_ultima_actualizacion: 29-10-2014
 * @version 1.0
 */
public class DBTimeoutException extends BaseException{

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -7519941877692528998L;
	private String				nombreBD;
	private String				nombreSP;

	/**
	 * Constructores
	 */
	public DBTimeoutException( String codError, String nombreBD, String nombreSP, String msjError, Exception objException ){
		super( codError, msjError, objException );
		this.nombreBD = nombreBD;
		this.nombreSP = nombreSP;
	}

	/**
	 * Constructores
	 */
	public DBTimeoutException( String codError, String nombreBD, String nombreSP, String msjError, Throwable objException ){
		super( codError, msjError, objException );
		this.nombreBD = nombreBD;
		this.nombreSP = nombreSP;
	}

	/**
	 * Constructores
	 */
	public DBTimeoutException( String msjError, Exception objException ){
		super( msjError, objException );
	}

	/**
	 * Constructores
	 */
	public DBTimeoutException( Exception objException ){
		super( objException );
	}

	/**
	 * Constructores
	 */
	public DBTimeoutException( String msjError ){
		super( msjError );
	}

	public String getNombreBD(){
		return nombreBD;
	}

	public void setNombreBD( String nombreBD ){
		this.nombreBD = nombreBD;
	}

	public String getNombreSP(){
		return nombreSP;
	}

	public void setNombreSP( String nombreSP ){
		this.nombreSP = nombreSP;
	}

}
