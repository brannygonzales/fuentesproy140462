package pe.com.claro.common.resource.util;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
public class HeaderRequestBean {
	 
	 @NotNull(message="idTransaccion")
	 private String idTransaccion ;
	 @NotNull(message="msgid")
	 private String msgid;
	 @NotNull(message="timestamp")
	 private String timestamp;
	 @NotNull(message="userId")
	 private String userId;
	 @NotNull(message="accept")
	 private String accept;
	
	public String getIdTransaccion(){
		return idTransaccion;
	}
	
	public void setIdTransaccion( String idTransaccion ){
		this.idTransaccion = idTransaccion;
	}
	
	public String getMsgid(){
		return msgid;
	}
	
	public void setMsgid( String msgid ){
		this.msgid = msgid;
	}
	
	public String getTimestamp(){
		return timestamp;
	}
	
	public void setTimestamp( String timestamp ){
		this.timestamp = timestamp;
	}
	
	public String getUserId(){
		return userId;
	}
	
	public void setUserId( String userId ){
		this.userId = userId;
	}
	
	public String getAccept(){
		return accept;
	}
	
	public void setAccept( String accept ){
		this.accept = accept;
	}

	@Override
	public String toString(){
		return "HeaderRequestBean [idTransaccion=" + idTransaccion + ", msgid=" + msgid + ", timestamp=" + timestamp + ", userId=" + userId + ", accept=" + accept + "]";
	}
	 
	
	
	
	 
	
	
}
