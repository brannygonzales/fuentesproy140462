package pe.com.claro.common.resource.exception;

public class WSException extends BaseException{

	private static final long	serialVersionUID	= 1L;
	private String				nombreWS;

	/**
	 * Constructores
	 */
	public WSException( String codError, String nombreWS, String msjError, Exception objException ){
		super( codError, msjError, objException );
		this.nombreWS = nombreWS;
	}
	public WSException( WSException objException ){
		super( objException.getCodigoError(), objException.getMsjError(), objException );
		this.nombreWS = objException.getNombreWS();

	}
	/**
	 * Constructores
	 */
	public WSException( String codError, String nombreWS, String msjError, Throwable objException ){
		super( codError, msjError, objException );
		this.nombreWS = nombreWS;
	}

	/**
	 * Constructores
	 */
	public WSException( String msjError, Exception objException ){
		super( msjError, objException );
	}

	/**
	 * Constructores
	 */
	public WSException( Exception objException ){
		super( objException );
	}

	/**
	 * Constructores
	 */
	public WSException( String msjError ){
		super( msjError );
	}

	public String getNombreWS(){
		return nombreWS;
	}

	public void setNombreWS( String nombreWS ){
		this.nombreWS = nombreWS;
	}

}
