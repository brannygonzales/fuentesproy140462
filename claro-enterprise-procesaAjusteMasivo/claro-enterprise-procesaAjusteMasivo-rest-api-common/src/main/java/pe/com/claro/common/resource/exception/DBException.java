package pe.com.claro.common.resource.exception;

public class DBException extends BaseException{

	private static final long	serialVersionUID	= 756863187125064820L;

	private String				nombreBD;
	private String				nombreSP;
	
	public DBException( String mensajeError ){
		super( mensajeError );
	}
	public DBException( Exception objException ){
		super( objException );
	}
	public DBException( DBException objException ){
		super( objException.getCodigoError(), objException.getMsjError(), objException );
		this.nombreBD = objException.getNombreBD();
		this.nombreSP = objException.getNombreSP();
	}
	public DBException( String mensajeError, Throwable e ){
		super( mensajeError, e );
	}

	public DBException( int codigoError, String mensajeError, Throwable e ){
		super( codigoError, mensajeError, e );
	}

	public DBException( String codigoError, String mensajeError, Throwable e ){
		super( codigoError, mensajeError, e );
	}
	
	public DBException( String codError, String nombreBD, String nombreSP, String msjError, Exception objException ){
		super( codError, msjError, objException );
		this.nombreBD = nombreBD;
		this.nombreSP = nombreSP;
	}
	
	public String getNombreBD(){
		return nombreBD;
	}

	public void setNombreBD( String nombreBD ){
		this.nombreBD = nombreBD;
	}

	public String getNombreSP(){
		return nombreSP;
	}

	public void setNombreSP( String nombreSP ){
		this.nombreSP = nombreSP;
	}

}
