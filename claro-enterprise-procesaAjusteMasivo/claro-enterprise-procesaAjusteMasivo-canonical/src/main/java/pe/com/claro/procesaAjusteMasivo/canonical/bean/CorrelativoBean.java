package pe.com.claro.procesaAjusteMasivo.canonical.bean;

import pe.com.claro.common.property.Constantes;

public class CorrelativoBean {

	private int id;
	private String tipoDocumento;
	private String prefijo;
	private long correlativo;
	
	@Override
	public String toString() {
		return new StringBuilder().append("id:").append(id).append(", ")
								  .append("tipoDocumento:").append(tipoDocumento).append(", ")
								  .append("prefijo:").append(prefijo).append(", ")
								  .append("correlativo :").append(correlativo).toString();
	}
	
	public String getNroDocumento() {
		return prefijo + Constantes.SEPARADOR_GUION +  String.format("%10s", correlativo).replace(' ','0'); 
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getPrefijo() {
		return prefijo;
	}
	public void setPrefijo(String prefijo) {
		this.prefijo = prefijo;
	}
	public long getCorrelativo() {
		return correlativo;
	}
	public void setCorrelativo(long correlativo) {
		this.correlativo = correlativo;
	}
	
}
