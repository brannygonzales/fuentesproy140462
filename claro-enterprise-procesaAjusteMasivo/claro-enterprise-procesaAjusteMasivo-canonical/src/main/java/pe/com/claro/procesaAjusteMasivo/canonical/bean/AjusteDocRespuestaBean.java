package pe.com.claro.procesaAjusteMasivo.canonical.bean;

import java.util.List;

public class AjusteDocRespuestaBean extends RespuestaBean {

	private List<AjusteDocBean> listaAjusteDoc;

	public List<AjusteDocBean> getListaAjusteDoc() {
		return listaAjusteDoc;
	}

	public void setListaAjusteDoc(List<AjusteDocBean> listaAjusteDoc) {
		this.listaAjusteDoc = listaAjusteDoc;
	}
	
}
