package pe.com.claro.procesaAjusteMasivo.canonical.bean;

public class AjusteDocPeticionBean {

	private String numeroDoc;
	private String customerId;
	
	public AjusteDocPeticionBean() {
	}
	
	public AjusteDocPeticionBean(String numeroDoc, String customerId) {
		this.numeroDoc = numeroDoc;
		this.customerId = customerId;
	}
	
	public String getNumeroDoc() {
		return numeroDoc;
	}
	public void setNumeroDoc(String numeroDoc) {
		this.numeroDoc = numeroDoc;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
}
