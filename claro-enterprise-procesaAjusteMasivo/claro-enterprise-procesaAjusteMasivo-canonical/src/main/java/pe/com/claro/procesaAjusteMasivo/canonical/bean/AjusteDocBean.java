package pe.com.claro.procesaAjusteMasivo.canonical.bean;

import java.util.Date;

public class AjusteDocBean {

	private String customerTrxId;
	private String trxNumber;
	private String comments;
	private Date trxDate;
	private Date dueDate;
	private double amountDueOriginal;
	private double amountDueRemaining;
	private String estado;
	
	public String getCustomerTrxId() {
		return customerTrxId;
	}
	public void setCustomerTrxId(String customerTrxId) {
		this.customerTrxId = customerTrxId;
	}
	public String getTrxNumber() {
		return trxNumber;
	}
	public void setTrxNumber(String trxNumber) {
		this.trxNumber = trxNumber;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Date getTrxDate() {
		return trxDate;
	}
	public void setTrxDate(Date trxDate) {
		this.trxDate = trxDate;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public double getAmountDueOriginal() {
		return amountDueOriginal;
	}
	public void setAmountDueOriginal(double amountDueOriginal) {
		this.amountDueOriginal = amountDueOriginal;
	}
	public double getAmountDueRemaining() {
		return amountDueRemaining;
	}
	public void setAmountDueRemaining(double amountDueRemaining) {
		this.amountDueRemaining = amountDueRemaining;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
}
