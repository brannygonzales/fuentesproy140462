package pe.com.claro.procesaAjusteMasivo.canonical.bean;

public class AjusteBean {

	private int id;
	private int procesoId;
	private String codAplicacion;
	private String usuario;
	private String tipoServicio;
	private String customerId;
	private String tipoAjuste;
	private String docReferencia;
	private String moneda;
	private double montoConIGV;
	private String descripcionAjuste;
	private String fechaRegistro;
	private String fechaVencimiento;
	private String codMotivoAjuste;
	private String tipoDocAjuste;
	private String nroTelefono;
	private double montoInafecto;
	private String nroDoc;
	private String notaTip;
	private String codAjuste;
	private String nroAjuste;
	private String estado;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getProcesoId() {
		return procesoId;
	}
	public void setProcesoId(int procesoId) {
		this.procesoId = procesoId;
	}
	public String getCodAplicacion() {
		return codAplicacion;
	}
	public void setCodAplicacion(String codAplicacion) {
		this.codAplicacion = codAplicacion;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getTipoServicio() {
		return tipoServicio;
	}
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getTipoAjuste() {
		return tipoAjuste;
	}
	public void setTipoAjuste(String tipoAjuste) {
		this.tipoAjuste = tipoAjuste;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public double getMontoConIGV() {
		return montoConIGV;
	} 
	public void setMontoConIGV(double montoConIGV) {
		this.montoConIGV = montoConIGV;
	}
	public String getDescripcionAjuste() {
		return descripcionAjuste;
	}
	public void setDescripcionAjuste(String descripcionAjuste) {
		this.descripcionAjuste = descripcionAjuste;
	}
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	public String getCodMotivoAjuste() {
		return codMotivoAjuste;
	}
	public void setCodMotivoAjuste(String codMotivoAjuste) {
		this.codMotivoAjuste = codMotivoAjuste;
	}
	public String getTipoDocAjuste() {
		return tipoDocAjuste;
	}
	public void setTipoDocAjuste(String tipoDocAjuste) {
		this.tipoDocAjuste = tipoDocAjuste;
	}
	public String getNroTelefono() {
		return nroTelefono;
	}
	public void setNroTelefono(String nroTelefono) {
		this.nroTelefono = nroTelefono;
	}
	public double getMontoInafecto() {
		return montoInafecto;
	}
	public void setMontoInafecto(double montoInafecto) {
		this.montoInafecto = montoInafecto;
	}
	public String getDocReferencia() {
		return docReferencia;
	}
	public void setDocReferencia(String docReferencia) {
		this.docReferencia = docReferencia;
	}
	public String getNroDoc() {
		return nroDoc;
	}
	public void setNroDoc(String nroDocAjuste) {
		this.nroDoc = nroDocAjuste;
	}
	public String getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public String getNotaTip() {
		return notaTip;
	}
	public void setNotaTip(String notaTip) {
		this.notaTip = notaTip;
	}
	public String getCodAjuste() {
		return codAjuste;
	}
	public void setCodAjuste(String codAjuste) {
		this.codAjuste = codAjuste;
	}
	public String getNroAjuste() {
		return nroAjuste;
	}
	public void setNroAjuste(String nroAjuste) {
		this.nroAjuste = nroAjuste;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}				
}
