package pe.com.claro.procesaAjusteMasivo.canonical.bean;

public class RespuestaBean {
	
	private boolean estado;
	private int	codigo;
	private String msg;
	
	public boolean isEstado() {
		return estado;
	}
	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}

}
