package pe.com.claro.procesaAjusteMasivo.resource;

import java.text.SimpleDateFormat;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.validation.ConstraintViolation;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

import pe.com.claro.common.property.ClaroUtil;
import pe.com.claro.common.property.Constantes;
import pe.com.claro.common.resource.exception.DBException;
import pe.com.claro.common.resource.exception.NotFoundException;
import pe.com.claro.common.resource.exception.WSException;
import pe.com.claro.common.resource.util.HeaderRequestBean;
import pe.com.claro.procesaAjusteMasivo.canonical.request.AjusteMasivoRequest;
import pe.com.claro.procesaAjusteMasivo.canonical.response.AjusteMasivoResponse;
import pe.com.claro.procesaAjusteMasivo.domain.service.AjusteMasivoService;
import pe.com.claro.procesaAjusteMasivo.resource.util.ApplicationConfig;
import pe.com.claro.procesaAjusteMasivo.resource.util.ValidadorUtil;

@Stateless
@Path(Constantes.PATH)
@Api(value = Constantes.RESOURCE, description = Constantes.DESCRIPCIONRESOURCE)
@Produces({ MediaType.APPLICATION_JSON })
public class AjusteMasivoResource {

	private static final Logger logger = LoggerFactory.getLogger(AjusteMasivoResource.class);
	private ApplicationConfig appConfig = new ApplicationConfig();
	String mensajeTransaccion;
	
	@EJB
	private AjusteMasivoService ajusteMasivoService;
	
	@POST
	@Path(Constantes.NOMBRE_METODO_AJUSTE)
	@Consumes({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = Constantes.DESCRIPCION_METODO_AJUSTE, notes = Constantes.NOTAS_METODO_AJUSTE, response = AjusteMasivoResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = Constantes.AJUSTE_MASIVO_MSG_200) })
	public Response procesarAjusteMasivo(@Context HttpHeaders httpHeaders, 
										 @ApiParam(value = "request", required = true) AjusteMasivoRequest request) throws NotFoundException, Exception {

		String result = null;
		AjusteMasivoResponse response = new AjusteMasivoResponse();
		long tiempoInicio = System.currentTimeMillis();
		
		String idTransaccion = httpHeaders.getRequestHeader("idTransaccion").toString();
		mensajeTransaccion = "[procesarAjusteMasivo idTx=" + idTransaccion + "] ";
		
		String datosRequest = new ObjectMapper().setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"))
												.writerWithDefaultPrettyPrinter()
												.writeValueAsString(request);
		log("Datos de entrada: " + datosRequest);

		try {
			String trazabilidadNoNull = ClaroUtil.getHttpHeadersNoNull(httpHeaders);
			org.apache.log4j.MDC.put("dataAudit", trazabilidadNoNull);
						
			log("[Actividad: Validar parametros obligatorios del Header]");			
			Set<ConstraintViolation<HeaderRequestBean>> constraintViolations = ValidadorUtil.validarParametrosObligatoriosHeader(httpHeaders);
			boolean datosValidos = constraintViolations.isEmpty();
			
			if (!datosValidos) { 
				String camposNoOk = getCamposNoOK(constraintViolations);
				response.setCodigoRespuesta(appConfig.getProperties().get(Constantes.AJUSTE_MASIVO_COD_IDF1).toString());
				response.setMensajeRespuesta(appConfig.getProperties().get(Constantes.AJUSTE_MASIVO_MSG_IDF1) + camposNoOk);
			} 
			else {
				if(ajusteMasivoService.procesarAjusteMasivo(mensajeTransaccion, idTransaccion, request)) {
					response.setCodigoRespuesta(appConfig.getProperties().get(Constantes.AJUSTE_MASIVO_COD_IDF0).toString());
					response.setMensajeRespuesta(appConfig.getProperties().get(Constantes.AJUSTE_MASIVO_MSG_IDF0).toString());
				}
			}
		} 
		catch (DBException e) {
			response.setCodigoRespuesta(appConfig.getProperties().get(Constantes.AJUSTE_MASIVO_COD_IDT1).toString());
			response.setMensajeRespuesta(appConfig.getProperties().get(Constantes.AJUSTE_MASIVO_MSG_IDT1).toString()
																		.replace(Constantes.REPLACE_BD, e.getNombreBD())
																		.replace(Constantes.REPLACE_SP, e.getNombreSP()));
		} 
		catch (WSException e) {
			response.setCodigoRespuesta(appConfig.getProperties().get(Constantes.AJUSTE_MASIVO_COD_IDT3).toString());
			response.setMensajeRespuesta(appConfig.getProperties().get(Constantes.AJUSTE_MASIVO_MSG_IDT3).toString()
																		.replace(Constantes.REPLACE_WS, e.getNombreWS()));
		}
		catch (Exception e) {
			logException(e);
			response.setCodigoRespuesta(appConfig.getProperties().get(Constantes.AJUSTE_MASIVO_COD_IDT2).toString());
			response.setMensajeRespuesta(appConfig.getProperties().get(Constantes.AJUSTE_MASIVO_MSG_IDT2).toString());
		} 
		finally {
			result = new ObjectMapper().setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"))
									   .writerWithDefaultPrettyPrinter()
									   .writeValueAsString(response);
		}

		log("Datos de salida: " + result);
		log("TiempoTotalTransaccion(ms)= " + (System.currentTimeMillis() - tiempoInicio));
		return Response.ok().entity(result).build();
	}
	
	@SuppressWarnings("rawtypes")
	private String getCamposNoOK(Set<ConstraintViolation<HeaderRequestBean>> constraintViolations) {
		StringBuilder camposNoOk = new StringBuilder();
		for (ConstraintViolation constraint : constraintViolations) {
			camposNoOk.append('[');
			camposNoOk.append(constraint.getMessage());
			camposNoOk.append(']');
		}
		return camposNoOk.toString();
	}


	private void log(String texto) {
		logger.info(mensajeTransaccion + texto);
	}
	
	private void logException(Exception e) {
		logger.error(mensajeTransaccion + "ERROR: [Exception] - [" + e.getMessage() + "] ", e);
	}

}
