package pe.com.claro.procesaAjusteMasivo.domain.repository;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;

import oracle.jdbc.OracleTypes;

import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.com.claro.common.domain.repository.AbstractRepository;
import pe.com.claro.common.property.Constantes;
import pe.com.claro.common.resource.exception.DBException;
import pe.com.claro.procesaAjusteMasivo.canonical.bean.AjusteBean;
import pe.com.claro.procesaAjusteMasivo.canonical.bean.AjusteDocBean;
import pe.com.claro.procesaAjusteMasivo.canonical.bean.AjusteDocPeticionBean;
import pe.com.claro.procesaAjusteMasivo.canonical.bean.AjusteDocRespuestaBean;
import pe.com.claro.procesaAjusteMasivo.canonical.bean.RespuestaBean;
import pe.com.claro.procesaAjusteMasivo.model.Formulario;

@Stateless
public class OACRepository extends AbstractRepository<Formulario> implements Serializable {

	private static final Logger logger = LoggerFactory.getLogger(OACRepository.class);
	private static final long serialVersionUID = 6079001789833563579L;

	@PersistenceContext(unitName = Constantes.PERSISTENCE_OAC)
	public void setPersistencesOAC(final EntityManager em) {
		this.entityManager = em;
	}

	@Context
	private Configuration config;
	
	public OACRepository() {
		super(Formulario.class);
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public RespuestaBean generarAjuste(String mensajeTransaccion, AjusteBean objAjusteBean) throws Exception{
		RespuestaBean objMensaje = new RespuestaBean();
		objMensaje.setEstado(false);
		String sp = config.getProperty(Constantes.SP_PROCESAR_AJUSTE).toString();
		
		try {
			log(mensajeTransaccion, "Inicio PROCEDURE: [" + sp + "]");
			
				Session session = this.entityManager.unwrap(Session.class);
				session.doWork(new Work() {

					@Override
					public void execute(Connection connection) throws SQLException {

						log(mensajeTransaccion, "PARAMETROS [INPUT]: [P_COD_APLICACION:" + objAjusteBean.getCodAplicacion() + "]");
						log(mensajeTransaccion, "PARAMETROS [INPUT]: [P_USUARIO:" + objAjusteBean.getUsuario() + "]");
						log(mensajeTransaccion, "PARAMETROS [INPUT]: [P_TIPO_SERVICIO:" + objAjusteBean.getTipoServicio() + "]");
						log(mensajeTransaccion, "PARAMETROS [INPUT]: [P_COD_CUENTA:" + objAjusteBean.getCustomerId() + "]");
						log(mensajeTransaccion, "PARAMETROS [INPUT]: [P_TIPO_AJUSTE:" + objAjusteBean.getTipoAjuste() + "]");
						log(mensajeTransaccion, "PARAMETROS [INPUT]: [P_NRO_DOC:" +   objAjusteBean.getNroDoc()+ "]");
						log(mensajeTransaccion, "PARAMETROS [INPUT]: [P_MONEDA:" + objAjusteBean.getMoneda() + "]");
						log(mensajeTransaccion, "PARAMETROS [INPUT]: [P_MONTO:" + objAjusteBean.getMontoConIGV() + "]");
						log(mensajeTransaccion, "PARAMETROS [INPUT]: [P_DESCRIP_AJT:" + objAjusteBean.getDescripcionAjuste() + "]");
						log(mensajeTransaccion, "PARAMETROS [INPUT]: [P_FEC_AJUSTE:" + objAjusteBean.getFechaRegistro() + "]");
						log(mensajeTransaccion, "PARAMETROS [INPUT]: [P_FEC_VECN_AJUSTE:" + objAjusteBean.getFechaVencimiento() + "]");
						log(mensajeTransaccion, "PARAMETROS [INPUT]: [P_COD_MOT_AJT:" + objAjusteBean.getCodMotivoAjuste() + "]");
						log(mensajeTransaccion, "PARAMETROS [INPUT]: [P_TIPO_DOC_AJUSTE:" + objAjusteBean.getTipoDocAjuste() + "]");
						log(mensajeTransaccion, "PARAMETROS [INPUT]: [P_NRO_DOC_AJUSTE:" +objAjusteBean.getDocReferencia() + "]");
						log(mensajeTransaccion, "PARAMETROS [INPUT]: [P_NRO_TELEFONO:" + objAjusteBean.getNroTelefono() + "]");
						log(mensajeTransaccion, "PARAMETROS [INPUT]: [P_MONTO_INAFECTO:" + objAjusteBean.getMontoInafecto() + "]");
						log(mensajeTransaccion, "PARAMETROS [INPUT]: [P_ID_PROCESO:" + objAjusteBean.getProcesoId() + "]");
					
						CallableStatement call = connection.prepareCall("{call " + sp + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
						call.setQueryTimeout(Integer.parseInt(config.getProperty(Constantes.QUERYTIMEOUT).toString()));
						call.setString("P_COD_APLICACION", objAjusteBean.getCodAplicacion());
						call.setString("P_USUARIO", objAjusteBean.getUsuario());
						call.setString("P_TIPO_SERVICIO", objAjusteBean.getTipoServicio());
						call.setString("P_COD_CUENTA", objAjusteBean.getCustomerId());
						call.setString("P_TIPO_AJUSTE", objAjusteBean.getTipoAjuste());
						call.setString("P_NRO_DOC", objAjusteBean.getNroDoc() );
						call.setString("P_MONEDA", objAjusteBean.getMoneda());
//						call.setDouble("P_MONTO", objAjusteBean.getMontoConIGV());
						call.setString("P_MONTO", String.valueOf(objAjusteBean.getMontoConIGV()));
						call.setString("P_DESCRIP_AJT", objAjusteBean.getDescripcionAjuste());
						call.setString("P_FEC_AJUSTE", objAjusteBean.getFechaRegistro());
						call.setString("P_FEC_VECN_AJUSTE", objAjusteBean.getFechaVencimiento());
						call.setString("P_COD_MOT_AJT", objAjusteBean.getCodMotivoAjuste());
						call.setString("P_TIPO_DOC_AJUSTE", objAjusteBean.getTipoDocAjuste());
						call.setString("P_NRO_DOC_AJUSTE",objAjusteBean.getDocReferencia());
						call.setString("P_NRO_TELEFONO", objAjusteBean.getNroTelefono()); 
//						call.setDouble("P_MONTO_INAFECTO", objAjusteBean.getMontoInafecto());
						call.setString("P_MONTO_INAFECTO", String.valueOf(objAjusteBean.getMontoInafecto()));
						call.setInt("P_ID_PROCESO", objAjusteBean.getProcesoId());
						call.registerOutParameter("P_COD_ERROR", Types.VARCHAR);
						call.execute();

						log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [P_COD_ERROR:" + call.getObject("P_COD_ERROR").toString() + "]");
					
						if(call.getObject("P_COD_ERROR").toString().equals(Constantes.PROCESO_EXITOSO))
							objMensaje.setEstado(true);
					}
				});
		} 
		catch (Exception e) {
			logException(mensajeTransaccion, e);
			throw new DBException(Constantes.TEXTO_VACIO, config.getProperty(Constantes.BD_OAC).toString(), sp, Constantes.TEXTO_VACIO, e);
		} 
		finally {
			log(mensajeTransaccion, "Fin PROCEDURE: [" + sp + "]");
		}

		return objMensaje;
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public AjusteDocRespuestaBean consultarAjuste(String mensajeTransaccion, AjusteDocPeticionBean inputBean) throws Exception {
		AjusteDocRespuestaBean outputBean = new AjusteDocRespuestaBean();
		String sp = config.getProperty(Constantes.SP_CONSULTAR_AJUSTE).toString();
		
		try {
			log(mensajeTransaccion, "Inicio PROCEDURE: [" + sp + "]");
			
			Session session = this.entityManager.unwrap(Session.class);
			session.doWork(new Work() {
				
				@Override
				public void execute(Connection connection) throws SQLException {
					log(mensajeTransaccion, "PARAMETROS [INPUT]: [PI_DOC_REFERENCIA: " + inputBean.getNumeroDoc() + "]");
					log(mensajeTransaccion, "PARAMETROS [INPUT]: [PI_CUSTOMER_ID: " + inputBean.getCustomerId() + "]");
					
					CallableStatement call = connection.prepareCall("{call " + sp + "(?,?,?,?,?)}");
					call.setQueryTimeout(Integer.parseInt(config.getProperty(Constantes.QUERYTIMEOUT).toString()));
					call.setString("PI_DOC_REFERENCIA", inputBean.getNumeroDoc());
					call.setString("PI_CUSTOMER_ID", inputBean.getCustomerId());
					call.registerOutParameter("PO_CURSOR_PROC", OracleTypes.CURSOR);
					call.registerOutParameter("PO_CODERROR", Types.VARCHAR);
					call.registerOutParameter("PO_DESERROR", Types.VARCHAR);
					call.execute();
					
					ResultSet rs = (ResultSet) call.getObject("PO_CURSOR_PROC");
					String codError = call.getObject("PO_CODERROR").toString();
					String desError = call.getObject("PO_DESERROR").toString();
					
					log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CODERROR: " + codError + "]");
					log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_DESERROR: " + desError + "]");
					
					List<AjusteDocBean> listaAjusteDoc = new ArrayList<AjusteDocBean>();
					AjusteDocBean ajusteDocBean;
					while (rs != null && rs.next()) {
						ajusteDocBean = new AjusteDocBean();
						ajusteDocBean.setCustomerTrxId(rs.getString("CUSTOMER_TRX_ID"));
						ajusteDocBean.setTrxNumber(rs.getString("TRX_NUMBER"));
						ajusteDocBean.setComments(rs.getString("COMMENTS"));
						ajusteDocBean.setTrxDate(rs.getDate("TRX_DATE"));
						ajusteDocBean.setDueDate(rs.getDate("DUE_DATE"));
						ajusteDocBean.setAmountDueOriginal(rs.getDouble("AMOUNT_DUE_ORIGINAL"));
						ajusteDocBean.setAmountDueRemaining(rs.getDouble("AMOUNT_DUE_REMAINING"));
						ajusteDocBean.setEstado(rs.getString("ATTRIBUTE6"));
						listaAjusteDoc.add(ajusteDocBean);
					}
					
					log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CURSOR_PROC][Cantidad de registros]: " + listaAjusteDoc.size());
					rs.close();
					
					outputBean.setListaAjusteDoc(listaAjusteDoc);
					outputBean.setCodigo(Integer.parseInt(codError));
					outputBean.setMsg(desError);
				}
			});
		} catch (Exception e) {
			logException(mensajeTransaccion, e);
			throw new DBException(Constantes.TEXTO_VACIO, config.getProperty(Constantes.BD_OAC).toString(), sp, Constantes.TEXTO_VACIO, e);
		} finally {
			log(mensajeTransaccion, "Fin PROCEDURE: [" + sp + "]");
		}
		
		return outputBean;
	}
	
	private void log(String mensajeTransaccion, String texto) {
		logger.info(mensajeTransaccion + texto);
	}
	
	private void logException(String mensajeTransaccion, Exception e) {
		logger.error(mensajeTransaccion + "ERROR: [Exception] - [" + e.getMessage() + "] ", e);
	}

	@Override
	protected Predicate[] getSearchPredicates(Root<Formulario> root, Formulario example) {
		return null;
	}

}
