package pe.com.claro.procesaAjusteMasivo.domain.repository;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;

import org.hibernate.Session;
import org.hibernate.dialect.OracleTypesHelper;
import org.hibernate.jdbc.Work;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.com.claro.common.domain.repository.AbstractRepository;
import pe.com.claro.common.property.Constantes;
import pe.com.claro.common.resource.exception.DBException;
import pe.com.claro.procesaAjusteMasivo.canonical.bean.AjusteBean;
import pe.com.claro.procesaAjusteMasivo.canonical.bean.CorrelativoBean;
import pe.com.claro.procesaAjusteMasivo.canonical.bean.RespuestaBean;
import pe.com.claro.procesaAjusteMasivo.canonical.request.AjusteMasivoRequest;
import pe.com.claro.procesaAjusteMasivo.model.Formulario;

@Stateless
public class SIOPRepository extends AbstractRepository<Formulario> implements Serializable {

	private static final Logger logger = LoggerFactory.getLogger(SIOPRepository.class);
	private static final long serialVersionUID = 6079001789833563579L;

	@Context
	private Configuration config;
	
	@PersistenceContext(unitName = Constantes.PERSISTENCE_SIOP)
	public void setPersistenceSIOP(final EntityManager em) {
		this.entityManager = em;
	}

	public SIOPRepository() {
		super(Formulario.class);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public CorrelativoBean obtenerCorrelativo(String mensajeTransaccion, String tipoDocumento) throws DBException{
		CorrelativoBean objCorrelativoBean = new CorrelativoBean();
		String sp = config.getProperty(Constantes.SP_OBTENER_CORRELATIVO).toString();

		try {
			log(mensajeTransaccion, "Inicio PROCEDURE: [" + sp + "]");
			
			Session session = this.entityManager.unwrap(Session.class);
			session.doWork(new Work() {

				@Override
				public void execute(Connection connection) throws SQLException {

					log(mensajeTransaccion, "PARAMETROS [INPUT]: [PI_TIPO_DOC:" + tipoDocumento + "]");
				
					CallableStatement call = connection.prepareCall("{call " + sp + "(?,?,?,?)}");
					call.setQueryTimeout(Integer.parseInt(config.getProperty(Constantes.QUERYTIMEOUT).toString()));
					call.setString("PI_TIPO_DOC", tipoDocumento);
					call.registerOutParameter("PO_CURSOR", OracleTypesHelper.INSTANCE.getOracleCursorTypeSqlType());
					call.registerOutParameter("PO_CODERROR", Types.VARCHAR);
					call.registerOutParameter("PO_DESERROR", Types.VARCHAR);
					call.execute();

					ResultSet rs = (ResultSet) call.getObject("PO_CURSOR");

					log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CODERROR:" + call.getObject("PO_CODERROR").toString() + "]");
					log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_DESERROR:" + call.getObject("PO_DESERROR").toString() + "]");

					if (rs != null) {
						rs.next();
						objCorrelativoBean.setId(rs.getInt("AM_ID"));
						objCorrelativoBean.setTipoDocumento(rs.getString("AM_TIPO_DOC"));
						objCorrelativoBean.setPrefijo(rs.getString("AM_PREFIJO"));
						objCorrelativoBean.setCorrelativo(rs.getLong("AM_CORRELATIVO"));
						
						log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CURSOR: " + objCorrelativoBean.toString() + "]");
					}
				}
			});
		} 
		catch (Exception e) {
			logException(mensajeTransaccion, e);
			throw  new DBException(Constantes.TEXTO_VACIO, config.getProperty(Constantes.BD_SIOP).toString(), sp, Constantes.TEXTO_VACIO, e);
		} 
		finally {
			log(mensajeTransaccion, "Fin PROCEDURE: [" + sp + "]");
		}

		return objCorrelativoBean;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public RespuestaBean devolverCorrelativo(String mensajeTransaccion, CorrelativoBean objCorrelativoBean) throws Exception{
		RespuestaBean objMensaje = new RespuestaBean();
		objMensaje.setEstado(false);
		String sp = config.getProperty(Constantes.SP_DEVOLVER_CORRELATIVO).toString();

		try {
			log(mensajeTransaccion, "Inicio PROCEDURE: [" + sp + "]");
			
			Session session = this.entityManager.unwrap(Session.class);
			session.doWork(new Work() {

				@Override
				public void execute(Connection connection) throws SQLException {

					log(mensajeTransaccion, "PARAMETROS [INPUT]: [PI_ID:" + objCorrelativoBean.getId() + "]");
				
					CallableStatement call = connection.prepareCall("{call " + sp + "(?,?,?)}");
					call.setQueryTimeout(Integer.parseInt(config.getProperty(Constantes.QUERYTIMEOUT).toString()));
					call.setInt("PI_ID", objCorrelativoBean.getId());
					call.registerOutParameter("PO_CODERROR", Types.VARCHAR);
					call.registerOutParameter("PO_DESERROR", Types.VARCHAR);
					call.execute();

					log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CODERROR:" + call.getObject("PO_CODERROR").toString() + "]");
					log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_DESERROR:" + call.getObject("PO_DESERROR").toString() + "]");
					
					if(call.getObject("PO_CODERROR").toString().equals(Constantes.PROCESO_EXITOSO))
						objMensaje.setEstado(true);
				}
			});
		} 
		catch (Exception e) {
			logException(mensajeTransaccion, e);
			throw new DBException(Constantes.TEXTO_VACIO, config.getProperty(Constantes.BD_SIOP).toString(), sp, Constantes.TEXTO_VACIO, e);
		} 
		finally {
			log(mensajeTransaccion, "Fin PROCEDURE: [" + sp + "]");
		}

		return objMensaje;
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<AjusteBean> obtenerAjustesCabecera(String mensajeTransaccion, AjusteMasivoRequest request ) throws DBException{
		List<AjusteBean> listaAjusteCabecera = new ArrayList<AjusteBean>();
		String sp = config.getProperty(Constantes.SP_OBTENER_AJUSTE_CABECERA).toString();

		try {
			log(mensajeTransaccion, " Inicio PROCEDURE: [" + sp + "]");
			
			Session session = this.entityManager.unwrap(Session.class);
			session.doWork(new Work() {

				@Override
				public void execute(Connection connection) throws SQLException {

					log(mensajeTransaccion, "PARAMETROS [INPUT]: [PI_ID_PROCESO:" + request.getProcesoId() + "]");
					log(mensajeTransaccion, "PARAMETROS [INPUT]: [PI_NRO_RECIBO:" + request.getNroRecibo() + "]");
				
					CallableStatement call = connection.prepareCall("{call " + sp + " (?,?,?,?,?)}");
					call.setQueryTimeout(Integer.parseInt(config.getProperty(Constantes.QUERYTIMEOUT).toString()));
					call.setString("PI_ID_PROCESO", request.getProcesoId());
					call.setString("PI_NRO_RECIBO", request.getNroRecibo());
					call.registerOutParameter("PO_CURSOR", OracleTypesHelper.INSTANCE.getOracleCursorTypeSqlType());
					call.registerOutParameter("PO_CODERROR", Types.VARCHAR);
					call.registerOutParameter("PO_DESERROR", Types.VARCHAR);
					call.execute();

					ResultSet rs = (ResultSet) call.getObject("PO_CURSOR");

					log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CODERROR:" + call.getObject("PO_CODERROR").toString() + "]");
					log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_DESERROR:" + call.getObject("PO_DESERROR").toString() + "]");

					if (rs != null) {
						AjusteBean objAjusteCabecera = null;
						
						while(rs.next()) {
							objAjusteCabecera = new AjusteBean();
							objAjusteCabecera.setId(rs.getInt("TAMCI_ID"));
							objAjusteCabecera.setProcesoId(rs.getInt("TAMCI_IDPROCESO"));
							objAjusteCabecera.setCodAplicacion(rs.getString("TAMCV_CODAPLICACION"));
							objAjusteCabecera.setUsuario(rs.getString("TAMCV_USUARIO"));
							objAjusteCabecera.setTipoServicio(rs.getString("TAMCV_TIPOSERVICIO"));
							objAjusteCabecera.setCustomerId(rs.getString("TAMCV_CUSTOMERID"));
							objAjusteCabecera.setTipoAjuste(rs.getString("TAMCV_TIPOAJUSTE"));
							objAjusteCabecera.setDocReferencia(rs.getString("TAMCV_DOCREFERENCIA"));
							objAjusteCabecera.setMoneda(rs.getString("TAMCV_MONEDA"));
							objAjusteCabecera.setMontoConIGV(rs.getDouble("TAMCN_MONTOCONIGV"));
							objAjusteCabecera.setDescripcionAjuste(rs.getString("TAMCV_DESCRIPCION"));
							objAjusteCabecera.setFechaRegistro(rs.getString("TAMCV_FECHAEMISIONAJ"));
							objAjusteCabecera.setFechaVencimiento(rs.getString("TAMCV_FECHAVENCAJ"));
							objAjusteCabecera.setCodMotivoAjuste(rs.getString("TAMCV_CODMOTAJT"));
							objAjusteCabecera.setTipoDocAjuste(rs.getString("TAMCV_TIPODOCAJUSTE"));
							objAjusteCabecera.setNroTelefono(rs.getString("TAMCV_LINEA"));
							objAjusteCabecera.setMontoInafecto(rs.getDouble("TAMCN_MONTOINAFECTO"));
							objAjusteCabecera.setNotaTip(rs.getString("TAMCV_NOTATIP"));
							objAjusteCabecera.setCodAjuste(rs.getString("TAMCV_CODAJUSTE"));
							objAjusteCabecera.setNroAjuste(rs.getString("TAMCV_NRODOCAJUSTE"));
							listaAjusteCabecera.add(objAjusteCabecera);
						}
						
						log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CURSOR: " + listaAjusteCabecera.size() + " registros devueltos]");
					}
				}
			});
		} 
		catch (Exception e) {
			logException(mensajeTransaccion, e);
			throw new DBException(Constantes.TEXTO_VACIO, config.getProperty(Constantes.BD_SIOP).toString(), sp, Constantes.TEXTO_VACIO, e);
		} 
		finally {
			log(mensajeTransaccion, " Fin PROCEDURE: [" + sp + "]");
		}

		return listaAjusteCabecera;
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public RespuestaBean actualizarAjusteCabecera(String mensajeTransaccion, AjusteBean objAjusteBean) throws Exception{
		RespuestaBean objMensaje = new RespuestaBean();
		objMensaje.setEstado(false);
		String sp = config.getProperty(Constantes.SP_ACTUALIZAR_AJUSTE_CABECERA).toString();

		try {
			log(mensajeTransaccion, "Inicio PROCEDURE: [" + sp + "]");
			
			Session session = this.entityManager.unwrap(Session.class);
			session.doWork(new Work() {

				@Override
				public void execute(Connection connection) throws SQLException {

					log(mensajeTransaccion, "PARAMETROS [INPUT]: [PI_ID:" + objAjusteBean.getId() + "]");
					log(mensajeTransaccion, "PARAMETROS [INPUT]: [PI_ESTADO:" + objAjusteBean.getEstado() + "]");
					log(mensajeTransaccion, "PARAMETROS [INPUT]: [PI_NRO_DOC:" + objAjusteBean.getNroDoc() + "]");
				
					CallableStatement call = connection.prepareCall("{call " + sp + "(?,?,?,?,?)}");
					call.setQueryTimeout(Integer.parseInt(config.getProperty(Constantes.QUERYTIMEOUT).toString()));
					call.setInt("PI_ID", objAjusteBean.getId());
					call.setString("PI_ESTADO", objAjusteBean.getEstado());
					call.setString("PI_NRO_DOC", objAjusteBean.getNroDoc());
					call.registerOutParameter("PO_CODERROR", Types.VARCHAR);
					call.registerOutParameter("PO_DESERROR", Types.VARCHAR);
					call.execute();

					log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CODERROR:" + call.getObject("PO_CODERROR").toString() + "]");
					log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_DESERROR:" + call.getObject("PO_DESERROR").toString() + "]");
					
					if(call.getObject("PO_CODERROR").toString().equals(Constantes.PROCESO_EXITOSO))
						objMensaje.setEstado(true);
				}
			});
		} 
		catch (Exception e) {
			logException(mensajeTransaccion, e);
			throw new DBException(Constantes.TEXTO_VACIO, config.getProperty(Constantes.BD_SIOP).toString(), sp, Constantes.TEXTO_VACIO, e);
		} 
		finally {
			log(mensajeTransaccion, "Fin PROCEDURE: [" + sp + "]");
		}

		return objMensaje;
	}
	
	private void log(String mensajeTransaccion, String texto) {
		logger.info(mensajeTransaccion + texto);
	}
	
	private void logException(String mensajeTransaccion, Exception e) {
		logger.error(mensajeTransaccion + "ERROR: [Exception] - [" + e.getMessage() + "] ", e);
	}

	@Override
	protected Predicate[] getSearchPredicates(Root<Formulario> root, Formulario example) {
		return null;
	} 

}
