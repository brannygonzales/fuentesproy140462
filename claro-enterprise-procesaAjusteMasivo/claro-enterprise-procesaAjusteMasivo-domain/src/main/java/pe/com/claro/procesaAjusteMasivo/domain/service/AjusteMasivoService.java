package pe.com.claro.procesaAjusteMasivo.domain.service;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.com.claro.common.property.Constantes;
import pe.com.claro.common.resource.exception.DBException;
import pe.com.claro.eai.crm.clarify.InteraccionType;
import pe.com.claro.eai.crmservices.clarify.transaccioninteracciones.NuevaInteraccion;
import pe.com.claro.eai.crmservices.clarify.transaccioninteracciones.NuevaInteraccionResponse;
import pe.com.claro.procesaAjusteMasivo.canonical.bean.AjusteBean;
import pe.com.claro.procesaAjusteMasivo.canonical.bean.AjusteDocPeticionBean;
import pe.com.claro.procesaAjusteMasivo.canonical.bean.AjusteDocRespuestaBean;
import pe.com.claro.procesaAjusteMasivo.canonical.bean.CorrelativoBean;
import pe.com.claro.procesaAjusteMasivo.canonical.request.AjusteMasivoRequest;
import pe.com.claro.procesaAjusteMasivo.domain.repository.OACRepository;
import pe.com.claro.procesaAjusteMasivo.domain.repository.SIOPRepository;
import pe.com.claro.procesaAjusteMasivo.service.transaccion.NuevaInteracciones;

@Stateless
public class AjusteMasivoService implements Serializable {

	private static final Logger logger = LoggerFactory.getLogger(AjusteMasivoService.class);
	private static final long serialVersionUID = -5283145043938691319L;

	@Context
	private Configuration configuration;
	  
	@EJB
	private SIOPRepository siopRepository;
	@EJB
	private OACRepository oacRepository;
	@EJB
	private NuevaInteracciones interacciones;
	 
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean procesarAjusteMasivo(String mensajeTransaccion, String idTransaccion, AjusteMasivoRequest request) throws Exception, DBException {
//		RespuestaBean objMensaje = null;
		
		log(mensajeTransaccion,"[Actividad: Obtener ajustes cabecera]");
		List<AjusteBean> listaAjusteCabecera = siopRepository.obtenerAjustesCabecera(mensajeTransaccion, request);
			
		for(AjusteBean ajuste : listaAjusteCabecera){
			CorrelativoBean objCorrelativoBean=null;
			log(mensajeTransaccion,"[Actividad: Obtener serie y correlativo]");
			
				objCorrelativoBean = siopRepository.obtenerCorrelativo(mensajeTransaccion, ajuste.getTipoAjuste());
			
			try {
				log(mensajeTransaccion,"[Actividad: Generar ajuste]");
				if(ajuste.getTipoAjuste()!=null && !ajuste.getTipoAjuste().isEmpty() && !ajuste.getTipoAjuste().equalsIgnoreCase("null") && ajuste.getTipoAjuste().equalsIgnoreCase(configuration.getProperty(Constantes.TIPO_AJUSTE_REC).toString())) {
					ajuste.setTipoAjuste(configuration.getProperty(Constantes.TIPO_AJUSTE_ND).toString());				 	
				}
				ajuste.setNroDoc(objCorrelativoBean.getNroDocumento());			 
//				objMensaje = oacRepository.generarAjuste(mensajeTransaccion, ajuste);
				oacRepository.generarAjuste(mensajeTransaccion, ajuste);
				
				log(mensajeTransaccion,"[Actividad: Consultar ajuste]");
				AjusteDocPeticionBean consultaAjuste = new AjusteDocPeticionBean(objCorrelativoBean.getNroDocumento(), ajuste.getCustomerId());
				AjusteDocRespuestaBean ajusteEncontrado = oacRepository.consultarAjuste(mensajeTransaccion, consultaAjuste);
				
//				if(objMensaje.isEstado()) {
				if(ajusteEncontrado.getCodigo() == 0 && ajusteEncontrado.getListaAjusteDoc() != null && !ajusteEncontrado.getListaAjusteDoc().isEmpty()) {
					ajuste.setEstado(Constantes.APROBADO_GENERACION);
					
					log(mensajeTransaccion,"[Actividad: Generar interación]");
					NuevaInteraccionResponse responseInteraccion = interacciones.nuevaInteraccion(mensajeTransaccion, generarRequestInteraccion(idTransaccion, ajuste));
					
					if(responseInteraccion.getAudit().getErrorCode() != Constantes.PROCESO_EXITOSO)
						ajuste.setEstado(Constantes.APROBADO_GENERACION_SINTIP);
				}
				else 
					devolverCorrelativo(mensajeTransaccion, ajuste, objCorrelativoBean);
			} 
			catch (Exception e) {
				logger.error(mensajeTransaccion + "Error Exception: " + e.getMessage(), e);
				devolverCorrelativo(mensajeTransaccion, ajuste, objCorrelativoBean);
			}
			
			log(mensajeTransaccion,"[Actividad: Actualizar datos en la tabla de ajustes]");
			siopRepository.actualizarAjusteCabecera(mensajeTransaccion, ajuste);
		}
		
		
		return true;
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private void devolverCorrelativo(String mensajeTransaccion, AjusteBean objAjuste, CorrelativoBean objCorrelativoBean) {
		try {
			objAjuste.setEstado(Constantes.RECHAZADO_GENERACION);
			objAjuste.setNroDoc(Constantes.TEXTO_VACIO);
			log(mensajeTransaccion,"[Actividad: Devolver serie y correlativo]");
			siopRepository.devolverCorrelativo(mensajeTransaccion, objCorrelativoBean);
		} 
		catch (Exception e) {}
	}
	
	private NuevaInteraccion generarRequestInteraccion(String idTransaccion, AjusteBean objAjuste) {
		 InteraccionType objInteraccion = new InteraccionType();
		 
         objInteraccion.setClase(configuration.getProperty(Constantes.WS_INTERACCIONES_CLASE).toString());
         objInteraccion.setCodigoEmpleado(configuration.getProperty(Constantes.WS_INTERACCIONES_COD_EMPLEADO).toString());
         objInteraccion.setCodigoSistema(configuration.getProperty(Constantes.WS_INTERACCIONES_COD_SISTEMA).toString());
         objInteraccion.setFlagCaso(configuration.getProperty(Constantes.WS_INTERACCIONES_FLAG_CASO).toString());
         objInteraccion.setHechoEnUno(configuration.getProperty(Constantes.WS_INTERACCIONES_HECHO_EN_UNO).toString());
         objInteraccion.setMetodoContacto(configuration.getProperty(Constantes.WS_INTERACCIONES_METODO_CONTACTO).toString());
//         objInteraccion.setNotas(configuration.getProperty(Constantes.WS_INTERACCIONES_NOTA).toString());
         String notas;
         if (Constantes.COD_TIPO_AJUSTE_REFACTURACION.equals(objAjuste.getCodAjuste()) 
        		 || Constantes.COD_TIPO_AJUSTE_REFACTURACION_TOTAL.equals(objAjuste.getCodAjuste())) {
        	 notas = objAjuste.getNotaTip().replace(Constantes.REPLACE_TIPO, objAjuste.getTipoAjuste()) + Constantes.TEXTO_ESPACIO + objAjuste.getDocReferencia();
         } else {
        	 notas = objAjuste.getNotaTip().replace(Constantes.REPLACE_NUM_RECIBO, objAjuste.getDocReferencia())
        			 .replace(Constantes.REPLACE_NUM_NOTACREDITO, objAjuste.getDocReferencia())
        			 .replace(Constantes.REPLACE_NUM_NOTADEBITO, objAjuste.getNroAjuste() == null ? Constantes.TEXTO_VACIO : objAjuste.getNroAjuste())
        			 .replace(Constantes.REPLACE_NUM_NOTARECIBO, objAjuste.getNroAjuste() == null ? Constantes.TEXTO_VACIO : objAjuste.getNroAjuste())
        			 .replace(Constantes.REPLACE_LINEA, objAjuste.getNroTelefono())
        			 .replace(Constantes.REPLACE_CUSTOMER_ID, objAjuste.getCustomerId());
         }
         objInteraccion.setNotas(notas);
         objInteraccion.setSubClase(configuration.getProperty(Constantes.WS_INTERACCIONES_SUBCLASE).toString());
         objInteraccion.setTelefono(objAjuste.getNroTelefono());
         objInteraccion.setTextResultado(configuration.getProperty(Constantes.WS_INTERACCIONES_RESULTADO).toString());
         objInteraccion.setTipo(configuration.getProperty(Constantes.WS_INTERACCIONES_TIPO).toString());
         objInteraccion.setTipoInteraccion(configuration.getProperty(Constantes.WS_INTERACCIONES_TIPO_INTERACCION).toString());
         
//         log(idTransaccion, "[setClase]" + objInteraccion.getCodigoEmpleado());
         log(idTransaccion, "[setClase]" + objInteraccion.getClase());
         log(idTransaccion, "[CodigoEmpleado]" + objInteraccion.getCodigoEmpleado());
         log(idTransaccion, "[setCodigoSistema]" + objInteraccion.getCodigoSistema());
         log(idTransaccion, "[setFlagCaso]" + objInteraccion.getFlagCaso());
         log(idTransaccion, "[setHechoEnUno]" + objInteraccion.getHechoEnUno());
         log(idTransaccion, "[setMetodoContacto]" + objInteraccion.getMetodoContacto());
         log(idTransaccion, "[setNotas]" + objInteraccion.getNotas());
         log(idTransaccion, "[setSubClase]" + objInteraccion.getSubClase());
         log(idTransaccion, "[setTelefono]" + objInteraccion.getTelefono());
         log(idTransaccion, "[setTextResultado]" + objInteraccion.getTextResultado());
         log(idTransaccion, "[setTipo]" + objInteraccion.getTipo());
         log(idTransaccion, "[setTipoInteraccion]" + objInteraccion.getTipoInteraccion());
         
         NuevaInteraccion request = new NuevaInteraccion();
         
         request.setInteraccion(objInteraccion);
         request.setTxId(idTransaccion);
         return request;
	}
	
	private void log(String mensajeTransaccion, String texto) {
		logger.info(mensajeTransaccion + texto);
	}

}
