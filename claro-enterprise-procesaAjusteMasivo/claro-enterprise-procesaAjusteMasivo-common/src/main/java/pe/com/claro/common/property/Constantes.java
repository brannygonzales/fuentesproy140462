package pe.com.claro.common.property;

public class Constantes {
	
	// URI Proyecto
	public static final String NOMBRERECURSO = "claro-enterprise-procesaAjusteMasivo";
	public static final String BASEPATH = "claro-enterprise-procesaAjusteMasivo/api";
	public static final String VERSION = "1.0.0";
	public static final String PATH = "/ajustemasivo";
	public static final String RESOURCE = "/ajusteMasivo";
	public static final String DESCRIPCIONRESOURCE = "Procesa los ajustes de forma masiva";

	// URI Ajuste 
	public static final String NOMBRE_METODO_AJUSTE = "/ajuste";
	public static final String DESCRIPCION_METODO_AJUSTE = "Realiza el ajuste masivo";
	public static final String NOTAS_METODO_AJUSTE = "El ajuste se realiza en base a las tablas propias de la BD de SIOP";
	
	// IDF - IDT
	public static final String AJUSTE_MASIVO_MSG_200  = "Operacion exitosa.";
	public static final String AJUSTE_MASIVO_COD_IDF0 = "AJUSTE_MASIVO_CODIGO_IDF0";
	public static final String AJUSTE_MASIVO_MSG_IDF0 = "AJUSTE_MASIVO_MENSAJE_IDF0";
	public static final String AJUSTE_MASIVO_COD_IDF1 = "AJUSTE_MASIVO_CODIGO_IDF1";
	public static final String AJUSTE_MASIVO_MSG_IDF1 = "AJUSTE_MASIVO_MENSAJE_IDF1";
			
	public static final String AJUSTE_MASIVO_COD_IDT1 = "AJUSTE_MASIVO_CODIGO_IDT1";
	public static final String AJUSTE_MASIVO_MSG_IDT1 = "AJUSTE_MASIVO_MENSAJE_IDT1";
	public static final String AJUSTE_MASIVO_COD_IDT2 = "AJUSTE_MASIVO_CODIGO_IDT2";
	public static final String AJUSTE_MASIVO_MSG_IDT2 = "AJUSTE_MASIVO_MENSAJE_IDT2";
	public static final String AJUSTE_MASIVO_COD_IDT3 = "AJUSTE_MASIVO_CODIGO_IDT3";
	public static final String AJUSTE_MASIVO_MSG_IDT3 = "AJUSTE_MASIVO_MENSAJE_IDT3";
	
	// Separadores
	public static final String SEPARADOR_PUNTO = ".";
	public static final String SEPARADOR_DOS_PUNTOS = ":";
	public static final String SEPARADOR_GUION = "-";
	
	// BD 
	public static final String QUERYTIMEOUT = "BD.QUERY.TIMEOUT";
	
	public static final String BD_SIOP = "BD.SIOP";
	public static final String SP_OBTENER_CORRELATIVO  = "BD.SIOP.SP.OBTENER_CORRELATIVO";
	public static final String SP_DEVOLVER_CORRELATIVO = "BD.SIOP.SP.DEVOLVER_CORRELATIVO";
	public static final String SP_OBTENER_AJUSTE_CABECERA	 = "BD.SIOP.SP.OBTENER_AJUSTE_CABECERA";
	public static final String SP_ACTUALIZAR_AJUSTE_CABECERA = "BD.SIOP.SP.ACTUALIZAR_AJUSTE_CABECERA";
	
	public static final String BD_OAC = "BD.OAC";
	public static final String SP_PROCESAR_AJUSTE = "BD.OAC.SP.PROCESAR_AJUSTE";
	public static final String SP_CONSULTAR_AJUSTE = "BD.OAC.SP.CONSULTAR_AJUSTE";
	
	// JNDI
	public static final String PERSISTENCE_SIOP = "JNDI_SIOP";
	public static final String PERSISTENCE_OAC  = "JNDI_OAC";
	
	// Util
	public static final String TEXTO_VACIO = "";
	public static final String PROCESO_EXITOSO = "0";
	public static final String REPLACE_BD = "[BD]";
	public static final String REPLACE_SP = "[SP]";
	public static final String REPLACE_WS = "[WS]";
	public static final String REPLACE_TIPO = "TIPO";
	public static final String REPLACE_NUM_RECIBO = "NUM_RECIBO";
	public static final String REPLACE_NUM_NOTACREDITO = "NUM_NOTACREDITO";
	public static final String REPLACE_NUM_NOTADEBITO = "NUM_NOTADEBITO";
	public static final String REPLACE_NUM_NOTARECIBO = "NUM_NOTARECIBO";
	public static final String REPLACE_LINEA = "LINEA";
	public static final String REPLACE_CUSTOMER_ID = "CUSTOMER_ID";
	public static final String TEXTO_ESPACIO = " ";
	
	// Estados de Ajustes
	public static final String APROBADO_GENERACION = "TEMP_APROBADO_GENERACION";
	public static final String APROBADO_GENERACION_SINTIP = "TEMP_APROBADO_GENERACION_SINTIP";
	public static final String RECHAZADO_GENERACION = "TEMP_RECHAZADO_GENERACION";
	public static final String TIPO_AJUSTE_REC = "TIPO_AJUSTE_REC";
	public static final String TIPO_AJUSTE_ND = "TIPO_AJUSTE_ND";
	// Interacion
	public static final String WS_INTERACCIONES = "WS_INTERACCIONES_URL";
	public static final String WS_INTERACCIONES_CONNECT_TIMEOUT_KEY = "com.sun.xml.ws.connect.timeout";
	public static final String WS_INTERACCIONES_CONNECT_TIMEOUT_VALOR = "WS_INTERACCIONES_URL_MAX_TIMEOUT_CONEXION";
	public static final String WS_INTERACCIONES_REQUEST_TIMEOUT_KEY = "com.sun.xml.ws.request.timeout";
	public static final String WS_INTERACCIONES_REQUEST_TIMEOUT_VALOR = "WS_INTERACCIONES_URL_MAX_TIMEOUT_REQUEST";
	public static final String WS_INTERACCIONES_TIPO="WS_INTERACCIONES_TIPO";
	public static final String WS_INTERACCIONES_CLASE="WS_INTERACCIONES_CLASE";
	public static final String WS_INTERACCIONES_SUBCLASE="WS_INTERACCIONES_SUBCLASE";
	public static final String WS_INTERACCIONES_COD_EMPLEADO="WS_INTERACCIONES_COD_EMPLEADO";
	public static final String WS_INTERACCIONES_COD_SISTEMA="WS_INTERACCIONES_COD_SISTEMA";
	public static final String WS_INTERACCIONES_FLAG_CASO="WS_INTERACCIONES_FLAG_CASO";
	public static final String WS_INTERACCIONES_HECHO_EN_UNO="WS_INTERACCIONES_HECHO_EN_UNO";
	public static final String WS_INTERACCIONES_METODO_CONTACTO="WS_INTERACCIONES_METODO_CONTACTO";
	public static final String WS_INTERACCIONES_RESULTADO="WS_INTERACCIONES_RESULTADO";
	public static final String WS_INTERACCIONES_TIPO_INTERACCION="WS_INTERACCIONES_TIPO_INTERACCION";
	public static final String WS_INTERACCIONES_NOTA="WS_INTERACCIONES_NOTA";
	
	// -----------------
	public static final String ID = "id";
	public static final String FORMATOFECHADEFAULT = "dd/MM/yyyy HH:mm:ss";
	public static final String IDTRANSACCION = "idTransaccion";
	public static final String MSGID = "msgid";
	public static final String USRID = "userId";
	public static final String TIMESTAMP = "timestamp";
	public static final String ACCEPT = "accept";
	public static final String API = "api";
	public static final String CORCHETE = "]";
	// -----------------
	public static final String CONFIGPROPERTIES = "config.properties";
	public static final String SWAGGERJAXRSCONFIG = "SwaggerJaxrsConfig";
	public static final String URLSWAGGERJAXRSCONFIG = "/SwaggerJaxrsConfig";
	public static final String HTML5CORSFILTER = "HTML5CorsFilter";
	public static final String URLPATTERNS = "/api/*";
	public static final String ACCESSCONTROLALLOWORIGIN = "Access-Control-Allow-Origin";
	public static final String ACCESSCONTROLALLOWMETHODS = "Access-Control-Allow-Methods";
	public static final String ACCESSCONTROLALLOWHEADERS = "Access-Control-Allow-Headers";
	public static final String ASTERISCO = "*";
	public static final String METODOSPERMITIDOS = "GET, POST, DELETE, PUT";
	public static final String CONTENTTYPE = "Content-Type";
	// -----------------
	public static final String PROPERTIESINTERNOS = "config.properties";
	public static final String PROPERTIESEXTERNOS = ".properties";
	public static final String PROPERTIESKEY = "claro.properties";
	public static final String CONSTANTENOJNDI = "javax.persistence.PersistenceException";
	
	public static final String COD_TIPO_AJUSTE_REFACTURACION = "AJUSTE_REFACTURACION";
	public static final String COD_TIPO_AJUSTE_REFACTURACION_TOTAL = "AJUSTE_REFACTURACION_TOTAL";
	
}
