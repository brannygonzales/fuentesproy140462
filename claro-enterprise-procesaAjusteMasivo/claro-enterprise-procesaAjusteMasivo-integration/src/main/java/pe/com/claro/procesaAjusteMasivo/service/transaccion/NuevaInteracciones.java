package pe.com.claro.procesaAjusteMasivo.service.transaccion;


import pe.com.claro.common.resource.exception.WSException;
import pe.com.claro.eai.crmservices.clarify.transaccioninteracciones.NuevaInteraccion;
import pe.com.claro.eai.crmservices.clarify.transaccioninteracciones.NuevaInteraccionResponse;

public interface NuevaInteracciones {
	public NuevaInteraccionResponse nuevaInteraccion(String mensajeTransaccion,NuevaInteraccion request) throws WSException;
}
