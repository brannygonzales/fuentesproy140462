package pe.com.claro.procesaAjusteMasivo.service.transaccion;

import javax.ejb.Stateless;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.com.claro.common.property.Constantes;
import pe.com.claro.common.resource.exception.WSException;
import pe.com.claro.eai.crmservices.clarify.transaccioninteracciones.NuevaInteraccion;
import pe.com.claro.eai.crmservices.clarify.transaccioninteracciones.NuevaInteraccionResponse;
import pe.com.claro.eai.crmservices.clarify.transaccioninteracciones.TransaccionInteracciones;
import pe.com.claro.eai.crmservices.clarify.transaccioninteracciones.TransaccionInteracciones_Service;
import pe.com.claro.eai.servicecommons.AuditType;

@Stateless
public class NuevaInteraccionesImpl implements NuevaInteracciones{
	
	private static final Logger logger = LoggerFactory.getLogger(NuevaInteraccionesImpl.class);
	
	@Context
	private Configuration config;
	
	@Override
	public NuevaInteraccionResponse nuevaInteraccion(String mensajeTransaccion, NuevaInteraccion request) throws WSException{	
		NuevaInteraccionResponse response = new NuevaInteraccionResponse();
		String urlWs = config.getProperty(Constantes.WS_INTERACCIONES).toString();
		
		try {
			log(mensajeTransaccion, "Inicio WS: [" + urlWs + "]");
	
			Holder<AuditType> auditResponse=new Holder<AuditType>();
			Holder<String> interaccionId= new Holder<String>();	
						
			TransaccionInteracciones_Service transaccionInteracciones_service = new TransaccionInteracciones_Service();
			TransaccionInteracciones  transaccionInteracciones = transaccionInteracciones_service.getTransaccionInteraccionesSOAP();
			BindingProvider bindingProvider = (BindingProvider)transaccionInteracciones;
			bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, urlWs);
			bindingProvider.getRequestContext().put(Constantes.WS_INTERACCIONES_CONNECT_TIMEOUT_KEY, Integer.parseInt(config.getProperty(Constantes.WS_INTERACCIONES_CONNECT_TIMEOUT_VALOR).toString()));
			bindingProvider.getRequestContext().put(Constantes.WS_INTERACCIONES_REQUEST_TIMEOUT_KEY, Integer.parseInt(config.getProperty(Constantes.WS_INTERACCIONES_REQUEST_TIMEOUT_VALOR).toString()));
			
			transaccionInteracciones.nuevaInteraccion(request.getTxId(), request.getInteraccion(), auditResponse, interaccionId);
			response.setAudit(auditResponse.value);
			response.setInteraccionId(interaccionId.value);
			
			log(mensajeTransaccion, "Codigo de respuesta del servicio Tipificacion: " + response.getAudit().getErrorCode());
			log(mensajeTransaccion, "Mensaje de respuesta del servicio Tipificacion: " + response.getAudit().getErrorMsg());
		}
		catch (Exception e){
			logException(mensajeTransaccion, e);
	    	throw new WSException( Constantes.TEXTO_VACIO, urlWs, Constantes.TEXTO_VACIO, e );
	    }
		finally {
			log(mensajeTransaccion, "Fin WS: [" + urlWs + "]");
		}
		return response;
	}
	
	private void log(String mensajeTransaccion, String texto) {
		logger.info(mensajeTransaccion + texto);
	}
	
	private void logException(String mensajeTransaccion, Exception e) {
		logger.error(mensajeTransaccion + "ERROR: [Exception] - [" + e.getMessage() + "] ", e);
	}

}
