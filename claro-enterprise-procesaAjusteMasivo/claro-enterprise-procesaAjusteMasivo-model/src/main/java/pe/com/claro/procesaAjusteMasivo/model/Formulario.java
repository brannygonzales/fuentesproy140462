package pe.com.claro.procesaAjusteMasivo.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
 
@Entity
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="codigoNodo")
public class Formulario implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private int codigoNodo;
	private String descripcionNodo;
	private int codigoEstado;
	private String estado;
	
	@Id	
	@Column(name="codigoNodo")
	public int getCodigoNodo() {
		return codigoNodo;
	}
	public void setCodigoNodo(int codigoNodo) {
		this.codigoNodo = codigoNodo;
	}
	public String getDescripcionNodo() {
		return descripcionNodo;
	}
	public void setDescripcionNodo(String descripcionNodo) {
		this.descripcionNodo = descripcionNodo;
	}
	public int getCodigoEstado() {
		return codigoEstado;
	}
	public void setCodigoEstado(int codigoEstado) {
		this.codigoEstado = codigoEstado;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	@Override
	public String toString() {
		return "Formulario [codigoNodo=" + codigoNodo + ", descripcionNodo=" + descripcionNodo + ", codigoEstado="
				+ codigoEstado + ", estado=" + estado + "]";
	}
	
	

}
