package pe.com.claro.procesararchivossga.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PropertiesExterno {

	@Value("${log4j.file.dir}")
	public String LOG4J_FILE_DIR;
	
	@Value("${db.sga.nombre}")
	public String DB_SGA_NOMBRE;
	
	@Value("${db.sga.owner}")
	public String DB_SGA_OWNER;
	
	@Value("${db.sga.pkg.ajustemasivos}")
	public String DB_SGA_PKG_AJUSTEMASIVOS;
	
	@Value("${db.sga.sp.spool.documento}")
	public String DB_SGA_SP_SPOOL_DOCUMENTO;
	
	@Value("${db.sga.timeout.ejecucion.segundos}")
	public Integer DB_SGA_TIMEOUT_EJECUCION_SEGUNDOS;
	
	@Value("${nombre.dia.corte}")
	public String NOMBRE_DIA_CORTE;
	
	@Value("${spool.tipo.documento}")
	public String SPOOL_TIPO_DOCUMENTO;
	
	@Value("${ws.envioCorreo.endpointaddress}")
	public String WS_ENVIOCORREO_ENDPOINTADDRESS;
	
	@Value("${ws.envioCorreo.max.timeout.conexion}")
	public String WS_ENVIOCORREO_MAX_TIMEOUT_CONEXION;
	
	@Value("${ws.envioCorreo.max.timeout.request}")
	public String WS_ENVIOCORREO_MAX_TIMEOUT_REQUEST;
	
	@Value("${ws.envioCorreo.destinatario}")
    public String WS_ENVIOCORREO_DESTINATARIO;
    
    @Value("${ws.envioCorreo.remitente}")
    public String WS_ENVIOCORREO_REMITENTE;
    
    @Value("${ws.envioCorreo.asunto}")
    public String WS_ENVIOCORREO_ASUNTO;
    
    @Value("${ws.envioCorreo.html.flag}")
    public String WS_ENVIOCORREO_HTMLFLAG;
    
    @Value("${ws.envioCorreo.mensaje.exito}")
    public String WS_ENVIOCORREO_MENSAJE_EXITO;
    
    @Value("${ws.envioCorreo.mensaje.error}")
    public String WS_ENVIOCORREO_MENSAJE_ERROR;
    
    @Value("${ws.param.auditoria.usuario.aplicacion}")
    public String WS_PARAM_AUDITORIA_USUARIOAPLICACION;
    
    @Value("${ws.param.auditoria.nombre.aplicacion}")
    public String WS_PARAM_AUDITORIA_NOMBREAPLICACION;
    
    @Value("${nombre.archivo.cabecera}")
    public String NOMBRE_ARCHIVO_CABECERA;
    
    @Value("${nombre.archivo.detalle}")
    public String NOMBRE_ARCHIVO_DETALLE;
	
}
