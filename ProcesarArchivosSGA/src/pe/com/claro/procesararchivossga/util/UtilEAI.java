package pe.com.claro.procesararchivossga.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import org.apache.log4j.Logger;
import org.apache.xmlbeans.XmlObject;







/**
 * @author epittman.
 * @clase: UtilEAI.java
 * @descripcion descripcion de la clase.
 * @fecha_de_creacion: dd-mm-yyyy.
 * @fecha_de_ultima_actualizacion: dd-mm-yyyy.
 * @version 1.0
 */
@SuppressWarnings( "unchecked" )
public class UtilEAI{

	private static final Logger logger = Logger.getLogger(UtilEAI.class);
	
	private static final Locale cLOCALE = new Locale("es");
	private static final String cCADENAVACIA = "";
	private static final String IP_LOCAL = "127.0.0.1";

	@SuppressWarnings( "rawtypes" )
	private static HashMap<Class, JAXBContext>	objMapaContexto	= new HashMap<Class, JAXBContext>();

	/**
	 * getJAXBContextFromClass
	 * @param objClase
	 * @return JAXBContext
	 */
	@SuppressWarnings( "rawtypes" )
	private JAXBContext obtenerContextoJaxBFromClass( String mensajeTransaccion, Class objClase ){

		JAXBContext objContexto = null;
		objContexto = objMapaContexto.get( objClase );

		if( objContexto == null ){
			try{
				logger.info( mensajeTransaccion + "INICIALIZANDO: [JaxContext...]" );

				objContexto = JAXBContext.newInstance( objClase );
				objMapaContexto.put( objClase, objContexto );
			}
			catch( Exception e ){
				logger.error( mensajeTransaccion + "ERROR creando 'JAXBContext': ", e );
			}
		}

		return objContexto;
	}

	/**
	 * transformarXmlTextFromJaxB
	 * @param mensajeTransaccion
	 * @param objJaxB
	 * @return String
	 */
	public String transformarXmlTextFromJaxB( String mensajeTransaccion, Object objJaxB ){

		String commandoRequestEnXml = null;
		JAXBContext objContexto = null;
		XmlObject objXML = null;

		try{
			objContexto = this.obtenerContextoJaxBFromClass( mensajeTransaccion, objJaxB.getClass() );

			Marshaller objMarshaller = objContexto.createMarshaller();
			StringWriter objStringWritter = new StringWriter();

			objMarshaller.marshal( objJaxB, objStringWritter );

			objXML = XmlObject.Factory.parse( objStringWritter.toString() );
			commandoRequestEnXml = objXML.toString();
		}
		catch( Exception e ){
			logger.error( mensajeTransaccion + "ERROR parseando object to 'XML': ", e );
		}

		return commandoRequestEnXml;
	}

	/**
	 * transfromarAnyObjectToXmlText
	 * @param objJaxB
	 * @return String
	 */
	@SuppressWarnings( "rawtypes" )
	public String transfromarAnyObjectToXmlText( String mensajeTransaccion, Object objJaxB ){

		String commandoRequestEnXml = null;
		JAXBContext objContexto = null;
		XmlObject objXML = null;

		try{
			objContexto = this.obtenerContextoJaxBFromClass( mensajeTransaccion, objJaxB.getClass() );

			Marshaller objMarshaller = objContexto.createMarshaller();
			StringWriter objStringWritter = new StringWriter();

			objMarshaller.marshal( new JAXBElement( new QName( "", objJaxB.getClass().getName() ), objJaxB.getClass(), objJaxB ), objStringWritter );
			objXML = XmlObject.Factory.parse( objStringWritter.toString() );

			commandoRequestEnXml = objXML.toString();
		}
		catch( Exception e ){
			logger.error( mensajeTransaccion + "ERROR parseando object to 'XML': ", e );
		}

		return commandoRequestEnXml;
	}

	/**
	 * EndpointURL
	 * @param objBinding
	 * @return actual_URL
	 */
	public String getEndpointURL( Object objBinding ){
		Map<String, Object> contextoRequest = ( (javax.xml.ws.BindingProvider)objBinding ).getRequestContext();
		return (String)contextoRequest.get( javax.xml.ws.BindingProvider.ENDPOINT_ADDRESS_PROPERTY );
	}

	/**
	 * setEndpointURL
	 * @param URL
	 * @param objBinding
	 * @return new_URL
	 */
	public String setEndpointURL( String URL, Object objBinding ){
		Map<String, Object> contextoRequest = ( (javax.xml.ws.BindingProvider)objBinding ).getRequestContext();
		return (String)contextoRequest.put( javax.xml.ws.BindingProvider.ENDPOINT_ADDRESS_PROPERTY, URL );
	}

	/**
	 * validaUrl valida la existencia de la 'URL'.
	 * @param mensajeTransaccion
	 * @param cadenaUrl
	 */
	public int validaUrl( String mensajeTransaccion, String cadenaUrl ){

		int respuestaURL = 0;

		URL url = null;
		URLConnection urlConexion = null;

		try{
			url = new URL( cadenaUrl );
			urlConexion = url.openConnection();

			urlConexion.setReadTimeout( 2000 ); // 2 SEGUNDOS ...

			urlConexion.setDoOutput( true );
			urlConexion.setDoInput( true );

			if( urlConexion instanceof HttpURLConnection ){

				logger.info( mensajeTransaccion + "" );
				logger.info( mensajeTransaccion + "====> URL:            " + urlConexion.getURL() );
				logger.info( mensajeTransaccion + "====> TIPO CONTENIDO: " + urlConexion.getContentType() );
				logger.info( mensajeTransaccion + "" );

				HttpURLConnection httpConexion = (HttpURLConnection)urlConexion;
				httpConexion.setReadTimeout( 2000 ); // 2 SEGUNDOS ...
				httpConexion.connect();

				respuestaURL = httpConexion.getResponseCode(); // 200 = OK
				logger.info( mensajeTransaccion + "Respuesta 'URL': [" + respuestaURL + "]" );

				if( respuestaURL == 200 ){
					logger.info( mensajeTransaccion + "Conexion Exitosa con la 'URL': [" + url + "]" );
					logger.info( mensajeTransaccion + urlConexion.getInputStream() + "" ); // Imprime URL
																								// en
																								// consola
																								// ...
				}
			}
		}
		catch( Exception e ){
			logger.error( mensajeTransaccion + "ERROR 'Exception': ", e );
		}

		return respuestaURL;
	}

	public String getStackTraceFromException( Exception exception ){
		StringWriter stringWriter = new StringWriter();
		exception.printStackTrace( new PrintWriter( stringWriter, true ) );
		return stringWriter.toString();
	}

	public String replaceNullVacio( String inputStr ){
		if( inputStr == null || inputStr.trim().equalsIgnoreCase( "null" ) || inputStr.trim().isEmpty() ){
			return cCADENAVACIA;
		}
		else{
			return inputStr;
		}
	}

	public static String formatDate( String mensajeTransaccion, Date date, String pattern ){
		String fecha = cCADENAVACIA;
		try{
			if( date != null ){
				SimpleDateFormat sdfFecha = new SimpleDateFormat( pattern, cLOCALE );
				fecha = sdfFecha.format( date );
			}
		}
		catch( Exception e ){
			logger.error( mensajeTransaccion + "ERROR 'Exception': ", e );
		}
		return fecha;
	}

	public Date parseDate( String mensajeTransaccion, String fecha, String pattern ){
		Date date = null;
		try{
			if( fecha != null && !fecha.trim().isEmpty() ){
				SimpleDateFormat sdfFecha = new SimpleDateFormat( pattern, cLOCALE );
				date = sdfFecha.parse( fecha );
			}
		}
		catch( Exception e ){
			logger.error( mensajeTransaccion + "ERROR 'Exception': ", e );
		}
		return date;
	}

	public static Date addDays( String mensajeTransaccion, Date date, int days ){
		Date newDate = null;
		try{
			if( date != null ){

				// Se agrega los dias
				Calendar cal = Calendar.getInstance();
				cal.setTime( date );
				cal.add( Calendar.DATE, days );

				newDate = cal.getTime();
			}
		}
		catch( Exception e ){
			logger.error( mensajeTransaccion + "ERROR 'Exception': ", e );
		}
		return newDate;
	}
    
    public String recortarCadena( int cantidadDigitos, String cadena ){
    	String resultado = cadena;
    	if ( cadena.length() > cantidadDigitos  ){
    		resultado = cadena.substring(0,cantidadDigitos);
    	}
    	return resultado;
    }
    
    public static String obtenerNombreDiaAhora() {
		Locale locale = new Locale("es", "PE");
		Calendar calAhora = Calendar.getInstance(locale);
		String nombreDia = calAhora.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, locale).toUpperCase();
		return nombreDia;
	}
    
    public static String obtenerIP(String mensajeLog) {
		try {
			InetAddress address = InetAddress.getLocalHost();
			return address.getHostAddress();
		} catch (UnknownHostException e) {
			logger.error(mensajeLog + "Error UnknownHostException: ", e);
			return IP_LOCAL;
		}
	}
    
}
