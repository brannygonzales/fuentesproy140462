package pe.com.claro.procesararchivossga.util;

import java.util.List;

import org.apache.log4j.Logger;

public class UtilArchivo {

	private static final Logger logger = Logger.getLogger(UtilArchivo.class);
	
	private static final String SALTO_LINEA = "\n";
	
	private UtilArchivo() {
	}
	
	public static <T> byte[] crearArchivoPlano(String trazabilidad, List<T> listaRegistros) {
		long tiempoProceso = System.currentTimeMillis();
		String nombreMetodo = "crearArchivo";
		logger.info( trazabilidad + "[INICIO] - METODO: [" + nombreMetodo + "] " );
		byte[] bytesArchivo = null;
		
		try {
			logger.info(trazabilidad + "Se comienza la creacion del archivo en memoria");
			StringBuffer stringBuffer = new StringBuffer();
			String lineaArchivo;
			
			for (T registro : listaRegistros) {
				lineaArchivo = registro.toString();
				stringBuffer.append(lineaArchivo);
				stringBuffer.append(SALTO_LINEA);
			}
			
			logger.debug(trazabilidad + "CONTENIDO DEL ARCHIVO:" + SALTO_LINEA + stringBuffer.toString());
			bytesArchivo = stringBuffer.toString().getBytes();
			logger.info(trazabilidad + "Archivo creado en memoria");
			logger.info(trazabilidad + "Cantidad de registros: " + listaRegistros.size());
		} catch (Exception e) {
			String msjError = "Error en la creacion del archivo: " + e.getMessage();
			logger.error(trazabilidad + msjError, e);
			bytesArchivo = null;
		} finally {
			logger.info( trazabilidad + "Tiempo TOTAL METODO: [" + ( System.currentTimeMillis() - tiempoProceso ) + " milisegundos ]" );
			logger.info( trazabilidad + "[FIN] - METODO: [" + nombreMetodo + "] " );
		}
		
		return bytesArchivo;
	}
	
}
