package pe.com.claro.procesararchivossga.util;

public class Constantes {

	public static final String PUNTO = ".";
	
	public static final String SQLTIMEOUTEXCEPTION = "SQLTIMEOUTEXCEPTION";
	
	public static final int _UNO = -1;
	public static final int _SIETE = -7;
	public static final int CERO = 0;
	public static final int UNO = 1;
	
	public static final String FORMATO_FECHA_YYYYMMDD = "yyyyMMdd";
	public static final String FORMATO_FECHA_DD_MM_YYYY = "dd/MM/yyyy";
	
	public static final String REPLACE_ID_LOTE = "[ID_LOTE]";
	public static final String REPLACE_FECHA_INI = "[FECHA_INI]";
	public static final String REPLACE_FECHA_FIN = "[FECHA_FIN]";
	public static final String REPLACE_TIPO_ERROR = "[TIPO_ERROR]";
	public static final String REPLACE_MENSAJE_ERROR = "[MENSAJE_ERROR]";
	
	public static final String TIPO_ERROR_BASE_DATOS = "de Base de Datos";
	public static final String TIPO_ERROR_INESPERADO = "inesperado";
	
}
