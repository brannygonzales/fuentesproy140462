package pe.com.claro.procesararchivossga.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pe.com.claro.procesararchivossga.bean.ConsultaSpoolDocumentoRequestBean;
import pe.com.claro.procesararchivossga.bean.ConsultaSpoolDocumentoResponseBean;
import pe.com.claro.procesararchivossga.dao.SgaDao;
import pe.com.claro.procesararchivossga.exception.DBException;
import pe.com.claro.procesararchivossga.service.EnvioCorreoSBSoap;
import pe.com.claro.procesararchivossga.service.types.ArchivoAdjuntoType;
import pe.com.claro.procesararchivossga.service.types.AuditoriaRequest;
import pe.com.claro.procesararchivossga.service.types.EnvioCorreoRequest;
import pe.com.claro.procesararchivossga.util.Constantes;
import pe.com.claro.procesararchivossga.util.PropertiesExterno;
import pe.com.claro.procesararchivossga.util.UtilArchivo;
import pe.com.claro.procesararchivossga.util.UtilEAI;

@Component
public class ProcesaArchivosSGABusiness {

	private static final Logger logger = Logger.getLogger(ProcesaArchivosSGABusiness.class);
	
	@Autowired
	private SgaDao sgaDao;
	
	@Autowired
	private PropertiesExterno propertiesExterno;
	
	@Autowired
	private EnvioCorreoSBSoap envioCorreoSBSoap;
	
	public void ejecutarProceso(String mensajeLog, String idTransaccion) {
		Date dateAhora = new Date();
		Date dateIni = UtilEAI.addDays(mensajeLog, dateAhora, Constantes._SIETE);
		Date dateFin = UtilEAI.addDays(mensajeLog, dateAhora, Constantes._UNO);
		
		try {
			String nombreDia = UtilEAI.obtenerNombreDiaAhora();
			String nombreDiaProperties = propertiesExterno.NOMBRE_DIA_CORTE;
			logger.info(mensajeLog + "nombreDia: " + nombreDia);
			logger.info(mensajeLog + "nombreDiaProperties: " + nombreDiaProperties);
			
			if (!nombreDiaProperties.equals(nombreDia)) {
				logger.info(mensajeLog + "No se genera SPOOL porque el dia configurado es: " + nombreDiaProperties);
				return;
			}
			
			logger.info(mensajeLog + "********** 1. Obtener registros para SPOOL **********");
			ConsultaSpoolDocumentoRequestBean requestBean = new ConsultaSpoolDocumentoRequestBean(
					propertiesExterno.SPOOL_TIPO_DOCUMENTO, dateIni, dateFin);
			ConsultaSpoolDocumentoResponseBean responseBean = sgaDao.consultarSpoolDocumento(mensajeLog, requestBean);
			
			if (responseBean.getCodigo() != Constantes.CERO || responseBean.getSpoolCab() == null 
					|| responseBean.getSpoolCab().size() <= Constantes.UNO || responseBean.getSpoolDet() == null 
					|| responseBean.getSpoolDet().size() <= Constantes.UNO) {
				logger.info(mensajeLog + "No hay registros para la generacion de SPOOL");
				return;
			}
			
			logger.info(mensajeLog + "********** 2. Generar archivos SPOOL **********");
			byte[] bytesCabecera = UtilArchivo.crearArchivoPlano(mensajeLog, responseBean.getSpoolCab());
			byte[] bytesDetalle = UtilArchivo.crearArchivoPlano(mensajeLog, responseBean.getSpoolDet());
			
			if (bytesCabecera == null || bytesDetalle == null) {
				logger.info(mensajeLog + "No se pudo generar los archivos SPOOL");
				return;
			}
			
			logger.info(mensajeLog + "********** 3. Enviar correo con SPOOL adjunto **********");
        	String idLote = UtilEAI.formatDate(mensajeLog, dateAhora, Constantes.FORMATO_FECHA_YYYYMMDD);
        	String nombreArchivoCab = propertiesExterno.NOMBRE_ARCHIVO_CABECERA.replace(Constantes.REPLACE_ID_LOTE, idLote);
        	String nombreArchivoDet = propertiesExterno.NOMBRE_ARCHIVO_DETALLE.replace(Constantes.REPLACE_ID_LOTE, idLote);
        	
        	List<ArchivoAdjuntoType> listaAdjuntos = new ArrayList<ArchivoAdjuntoType>();
        	ArchivoAdjuntoType adjuntoCab = new ArchivoAdjuntoType(nombreArchivoCab, bytesCabecera);
        	ArchivoAdjuntoType adjuntoDet = new ArchivoAdjuntoType(nombreArchivoDet, bytesDetalle);
        	listaAdjuntos.add(adjuntoCab);
        	listaAdjuntos.add(adjuntoDet);
        	
        	enviarCorreo(mensajeLog, idTransaccion, dateIni, dateFin, propertiesExterno.WS_ENVIOCORREO_MENSAJE_EXITO, listaAdjuntos);
		} catch (DBException e) {
			logger.error(mensajeLog + "Ocurrio una excepcion [DBException] en el proceso, se terminara el proceso: " + e.getMessage(), e);
			logger.info(mensajeLog + "********** 4. Enviar correo con detalle de error **********");
			String mensajeError = propertiesExterno.WS_ENVIOCORREO_MENSAJE_ERROR.replace(Constantes.REPLACE_TIPO_ERROR, 
					Constantes.TIPO_ERROR_BASE_DATOS).replace(Constantes.REPLACE_MENSAJE_ERROR, e.getCause().getMessage());
			enviarCorreo(mensajeLog, idTransaccion, dateIni, dateFin, mensajeError, null);
		} catch (Exception e) {
		    logger.error(mensajeLog + "Ocurrio una excepcion general en el proceso, se terminara el proceso: " + e.getMessage(), e);
		    logger.info(mensajeLog + "********** 4. Enviar correo con detalle de error **********");
		    String mensajeError = propertiesExterno.WS_ENVIOCORREO_MENSAJE_ERROR.replace(Constantes.REPLACE_TIPO_ERROR, 
		    		Constantes.TIPO_ERROR_INESPERADO).replace(Constantes.REPLACE_MENSAJE_ERROR, e.getMessage());
		    enviarCorreo(mensajeLog, idTransaccion, dateIni, dateFin, mensajeError, null);
	    }
	}
	
	private void enviarCorreo(String mensajeLog, String idTransaccion, Date fechaIni, Date fechaFin, String mensajeCorreo, 
			List<ArchivoAdjuntoType> listaAdjuntos) {
		EnvioCorreoRequest requestCorreo = new EnvioCorreoRequest();
		requestCorreo.setDestinatario(propertiesExterno.WS_ENVIOCORREO_DESTINATARIO);
     	requestCorreo.setRemitente(propertiesExterno.WS_ENVIOCORREO_REMITENTE);
     	requestCorreo.setAsunto(propertiesExterno.WS_ENVIOCORREO_ASUNTO.replace(Constantes.REPLACE_FECHA_INI, 
     			UtilEAI.formatDate(mensajeLog, fechaIni, Constantes.FORMATO_FECHA_DD_MM_YYYY)).replace(Constantes.REPLACE_FECHA_FIN, 
     			UtilEAI.formatDate(mensajeLog, fechaFin, Constantes.FORMATO_FECHA_DD_MM_YYYY)));
     	requestCorreo.setHtmlFlag(propertiesExterno.WS_ENVIOCORREO_HTMLFLAG);
     	
     	AuditoriaRequest auditoriaCorreo = new AuditoriaRequest();
     	auditoriaCorreo.setIdTransaccion(idTransaccion);
    	auditoriaCorreo.setIpAplicacion(UtilEAI.obtenerIP(mensajeLog));
    	auditoriaCorreo.setUsuarioAplicacion(propertiesExterno.WS_PARAM_AUDITORIA_USUARIOAPLICACION);
    	auditoriaCorreo.setNombreAplicacion(propertiesExterno.WS_PARAM_AUDITORIA_NOMBREAPLICACION);
    	requestCorreo.setAuditoriaRequest(auditoriaCorreo);
    	
    	if (listaAdjuntos != null && !listaAdjuntos.isEmpty()) {
    		requestCorreo.setListaArchivosAdjuntos(listaAdjuntos);
    	}
    	
    	requestCorreo.setMensaje(mensajeCorreo);
    	envioCorreoSBSoap.enviarCorreo(mensajeLog, requestCorreo);
	}
	
}
