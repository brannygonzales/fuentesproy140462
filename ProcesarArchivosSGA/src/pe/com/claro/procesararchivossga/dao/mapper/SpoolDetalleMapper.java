package pe.com.claro.procesararchivossga.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pe.com.claro.procesararchivossga.bean.SpoolDetalleBean;

public class SpoolDetalleMapper implements RowMapper<SpoolDetalleBean> {

	@Override
	public SpoolDetalleBean mapRow(ResultSet rs, int fila) throws SQLException {
		SpoolDetalleBean bean = new SpoolDetalleBean();
		bean.setSpoolDetSGA(rs.getString("K_SPOOL_DET_SGA"));
		return bean;
	}

}
