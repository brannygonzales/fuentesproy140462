package pe.com.claro.procesararchivossga.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pe.com.claro.procesararchivossga.bean.SpoolCabeceraBean;

public class SpoolCabeceraMapper implements RowMapper<SpoolCabeceraBean> {

	@Override
	public SpoolCabeceraBean mapRow(ResultSet rs, int fila) throws SQLException {
		SpoolCabeceraBean bean = new SpoolCabeceraBean();
		bean.setSpoolCabSGA(rs.getString("K_SPOOL_CAB_SGA"));
		return bean;
	}

}
