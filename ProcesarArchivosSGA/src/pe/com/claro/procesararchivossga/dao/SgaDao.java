package pe.com.claro.procesararchivossga.dao;

import pe.com.claro.procesararchivossga.bean.ConsultaSpoolDocumentoRequestBean;
import pe.com.claro.procesararchivossga.bean.ConsultaSpoolDocumentoResponseBean;
import pe.com.claro.procesararchivossga.exception.DBException;

public interface SgaDao {

	ConsultaSpoolDocumentoResponseBean consultarSpoolDocumento(String mensajeLog, 
			ConsultaSpoolDocumentoRequestBean requestBean) throws DBException;
	
}
