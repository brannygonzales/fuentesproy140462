package pe.com.claro.procesararchivossga.dao;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.com.claro.procesararchivossga.bean.ConsultaSpoolDocumentoRequestBean;
import pe.com.claro.procesararchivossga.bean.ConsultaSpoolDocumentoResponseBean;
import pe.com.claro.procesararchivossga.bean.SpoolCabeceraBean;
import pe.com.claro.procesararchivossga.bean.SpoolDetalleBean;
import pe.com.claro.procesararchivossga.dao.mapper.SpoolCabeceraMapper;
import pe.com.claro.procesararchivossga.dao.mapper.SpoolDetalleMapper;
import pe.com.claro.procesararchivossga.exception.DBException;
import pe.com.claro.procesararchivossga.util.Constantes;
import pe.com.claro.procesararchivossga.util.PropertiesExterno;

@Repository
public class SgaDaoImpl implements SgaDao {

	private static final Logger logger = Logger.getLogger(SgaDaoImpl.class);
	
	@Autowired
	@Qualifier("sgaDS")
	private DataSource sgaDS;
	
	@Autowired
	private PropertiesExterno propertiesExterno;
	
	@Override
	public ConsultaSpoolDocumentoResponseBean consultarSpoolDocumento(String mensajeLog,
			ConsultaSpoolDocumentoRequestBean requestBean) throws DBException {
		long tiempoInicio = System.currentTimeMillis();
		ConsultaSpoolDocumentoResponseBean responseBean = new ConsultaSpoolDocumentoResponseBean();
		String nombreMetodo = "consultarSpoolDocumento";
		StringBuffer storedProcedure = new StringBuffer();
		logger.info(mensajeLog + "[INICIO] - METODO: [" + nombreMetodo + " - DAO]");
		
		String nombreBD = propertiesExterno.DB_SGA_NOMBRE;
		String owner = propertiesExterno.DB_SGA_OWNER;
		String paquete = propertiesExterno.DB_SGA_PKG_AJUSTEMASIVOS;
		String procedure = propertiesExterno.DB_SGA_SP_SPOOL_DOCUMENTO;
		Integer timeoutEjecucion = propertiesExterno.DB_SGA_TIMEOUT_EJECUCION_SEGUNDOS;
		
		storedProcedure.append(owner);
		storedProcedure.append(Constantes.PUNTO);
		storedProcedure.append(paquete);
		storedProcedure.append(Constantes.PUNTO);
		storedProcedure.append(procedure);
		
		try {
			logger.info(mensajeLog + " - PROCEDURE: [" + storedProcedure.toString() + "]");
			logger.info(mensajeLog + " - TIMEOUT EJECUCION: [" + timeoutEjecucion + "]");
			
			Date fechaIni = new Date(requestBean.getFechaIni().getTime());
			Date fechaFin = new Date(requestBean.getFechaFin().getTime());
			
			SqlParameterSource inputProcedure = new MapSqlParameterSource()
					.addValue("K_TIPODOC", requestBean.getTipoDoc(), OracleTypes.VARCHAR)
					.addValue("K_FECHA_INI", fechaIni, OracleTypes.DATE)
					.addValue("K_FECHA_FIN", fechaFin, OracleTypes.DATE);
			
			logger.info(mensajeLog + " - PARAMETROS [INPUT] ");
			logger.info(mensajeLog + " - [INPUT][K_TIPODOC]: " + requestBean.getTipoDoc());
			logger.info(mensajeLog + " - [INPUT][K_FECHA_INI]: " + fechaIni);
			logger.info(mensajeLog + " - [INPUT][K_FECHA_FIN]: " + fechaFin);
			
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(sgaDS);
			JdbcTemplate jdbcTemplate = simpleJdbcCall.getJdbcTemplate();
			jdbcTemplate.setQueryTimeout(timeoutEjecucion);
			
			simpleJdbcCall.withoutProcedureColumnMetaDataAccess();
			simpleJdbcCall.withSchemaName(owner);
			simpleJdbcCall.withCatalogName(paquete);
			simpleJdbcCall.withProcedureName(procedure);
			
			simpleJdbcCall.declareParameters(
					new SqlParameter("K_TIPODOC", OracleTypes.VARCHAR),
					new SqlParameter("K_FECHA_INI", OracleTypes.DATE),
					new SqlParameter("K_FECHA_FIN", OracleTypes.DATE),
					new SqlOutParameter("KO_SPOOL_CAB", OracleTypes.CURSOR, new SpoolCabeceraMapper()),
					new SqlOutParameter("KO_SPOOL_DET", OracleTypes.CURSOR, new SpoolDetalleMapper()),
					new SqlOutParameter("KO_CODIGO", OracleTypes.INTEGER),
					new SqlOutParameter("KO_MENSAJE", OracleTypes.VARCHAR));
			Map<String, Object> outputProcedure = simpleJdbcCall.execute(inputProcedure);
			logger.info(mensajeLog + "SP ejecutado");
			
			List<SpoolCabeceraBean> listaSpoolCabecera = outputProcedure.get("KO_SPOOL_CAB") == null ? 
					new ArrayList<SpoolCabeceraBean>() : (List<SpoolCabeceraBean>) outputProcedure.get("KO_SPOOL_CAB");
			List<SpoolDetalleBean> listaSpoolDetalle = outputProcedure.get("KO_SPOOL_DET") == null ? 
					new ArrayList<SpoolDetalleBean>() : (List<SpoolDetalleBean>) outputProcedure.get("KO_SPOOL_DET");
			Integer codigo = (Integer) outputProcedure.get("KO_CODIGO");
			String mensaje = (String) outputProcedure.get("KO_MENSAJE");
			
			logger.info(mensajeLog + " - PARAMETROS [OUTPUT]");
			logger.info(mensajeLog + " - [OUTPUT][KO_SPOOL_CAB][Cantidad de registros]: [" + listaSpoolCabecera.size() + "]");
			logger.info(mensajeLog + " - [OUTPUT][KO_SPOOL_DET][Cantidad de registros]: [" + listaSpoolDetalle.size() + "]");
			logger.info(mensajeLog + " - [OUTPUT][KO_CODIGO]: [" + codigo + "]");
			logger.info(mensajeLog + " - [OUTPUT][KO_MENSAJE]: [" + mensaje + "]");
			
			responseBean.setCodigo(codigo);
			responseBean.setMensaje(mensaje);
			responseBean.setSpoolCab(listaSpoolCabecera);
			responseBean.setSpoolDet(listaSpoolDetalle);
		} catch (Exception e) {
			logger.error(mensajeLog + "ERROR: [Exception] - [" + e.getMessage() + "] ", e);
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			String descripcionError = String.valueOf(errors.toString());
			
			if (descripcionError.toUpperCase(Locale.getDefault()).contains(Constantes.SQLTIMEOUTEXCEPTION)) {
				throw new DBException("Error de Timeout hacia BD", e, nombreBD, storedProcedure.toString());
			} else {
				throw new DBException("Error de disponibilidad hacia BD", e, nombreBD, storedProcedure.toString());
			}
		} finally {
			logger.info(mensajeLog + " - Tiempo TOTAL METODO: [" + ( System.currentTimeMillis() - tiempoInicio ) + " milisegundos ]" );
			logger.info(mensajeLog + "[FIN] - METODO: [" + nombreMetodo + " - DAO] " );
		}
		
		return responseBean;
	}

}
