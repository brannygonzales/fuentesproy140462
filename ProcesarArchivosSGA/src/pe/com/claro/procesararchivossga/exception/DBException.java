package pe.com.claro.procesararchivossga.exception;

public class DBException extends BaseException {
	
	private static final long serialVersionUID = 756863187125064820L;
	
	private String nombreBD;
	private String nombreSP;
	
	public DBException(String mensajeError) {
		super(mensajeError);
	}
	
	public DBException(String mensajeError, Throwable e) {
		super(mensajeError, e);
	}
	
	public DBException(int codigoError, String mensajeError, Throwable e) {
		super(codigoError, mensajeError, e);
	}

	public DBException(String mensajeError, Throwable e, String nombreBD, String nombreSP) {
		super(mensajeError, e);
		this.nombreBD = nombreBD;
		this.nombreSP = nombreSP;
	}

	public String getNombreBD() {
		return nombreBD;
	}

	public void setNombreBD(String nombreBD) {
		this.nombreBD = nombreBD;
	}

	public String getNombreSP() {
		return nombreSP;
	}

	public void setNombreSP(String nombreSP) {
		this.nombreSP = nombreSP;
	}
	
}
