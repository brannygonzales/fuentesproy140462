package pe.com.claro.procesararchivossga.exception;

public class BaseException extends Exception {

	private static final long serialVersionUID = 5246661617731285863L;
	private int codigoError;
	private String mensajeError;
	
	public BaseException(String mensajeError, Throwable e) {
		super(mensajeError, e);
		this.mensajeError = mensajeError;
	}

	public BaseException(int codigoError, String mensajeError, Throwable e) {
		super(mensajeError, e);
		this.codigoError = codigoError;
		this.mensajeError = mensajeError;
	}
	
	public BaseException(String mensajeError) {
		super(mensajeError);
		this.mensajeError = mensajeError;
	}

	public int getCodigoError() {
		return codigoError;
	}

	public String getMensajeError() {
		return mensajeError;
	}

}
