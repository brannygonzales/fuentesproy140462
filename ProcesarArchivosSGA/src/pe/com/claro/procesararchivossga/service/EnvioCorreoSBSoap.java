package pe.com.claro.procesararchivossga.service;

import pe.com.claro.procesararchivossga.service.types.EnvioCorreoRequest;
import pe.com.claro.procesararchivossga.service.types.EnvioCorreoResponse;

/**
 * @author DMB
 *
 */
public interface EnvioCorreoSBSoap {
	
	EnvioCorreoResponse enviarCorreo(String mensajeLog, EnvioCorreoRequest request);
	
}
