package pe.com.claro.procesararchivossga.service.types;

public class EnvioCorreoResponse {

	private AuditoriaResponse auditoriaResponse;

	public AuditoriaResponse getAuditoriaResponse() {
		return auditoriaResponse;
	}

	public void setAuditoriaResponse(AuditoriaResponse auditoriaResponse) {
		this.auditoriaResponse = auditoriaResponse;
	}
	
}
