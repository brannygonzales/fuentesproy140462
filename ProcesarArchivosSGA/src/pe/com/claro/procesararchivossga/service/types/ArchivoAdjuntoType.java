package pe.com.claro.procesararchivossga.service.types;

public class ArchivoAdjuntoType {

	private String nombre;
	private String cabecera;
	private byte[] archivo;
	
	public ArchivoAdjuntoType() {
	}
	
	public ArchivoAdjuntoType(String nombre, byte[] archivo) {
		this.nombre = nombre;
		this.archivo = archivo;
	}

	public ArchivoAdjuntoType(String nombre, String cabecera, byte[] archivo) {
		this.nombre = nombre;
		this.cabecera = cabecera;
		this.archivo = archivo;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCabecera() {
		return cabecera;
	}
	public void setCabecera(String cabecera) {
		this.cabecera = cabecera;
	}
	public byte[] getArchivo() {
		return archivo;
	}
	public void setArchivo(byte[] archivo) {
		this.archivo = archivo;
	}
	
}
