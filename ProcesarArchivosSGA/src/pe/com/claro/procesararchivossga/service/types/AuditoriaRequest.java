package pe.com.claro.procesararchivossga.service.types;

public class AuditoriaRequest {

	private String idTransaccion;
	private String ipAplicacion;
	private String nombreAplicacion;
	private String usuarioAplicacion;
	
	public String getIdTransaccion() {
		return idTransaccion;
	}
	public void setIdTransaccion(String idTransaccion) {
		this.idTransaccion = idTransaccion;
	}
	public String getIpAplicacion() {
		return ipAplicacion;
	}
	public void setIpAplicacion(String ipAplicacion) {
		this.ipAplicacion = ipAplicacion;
	}
	public String getNombreAplicacion() {
		return nombreAplicacion;
	}
	public void setNombreAplicacion(String nombreAplicacion) {
		this.nombreAplicacion = nombreAplicacion;
	}
	public String getUsuarioAplicacion() {
		return usuarioAplicacion;
	}
	public void setUsuarioAplicacion(String usuarioAplicacion) {
		this.usuarioAplicacion = usuarioAplicacion;
	}
	
}
