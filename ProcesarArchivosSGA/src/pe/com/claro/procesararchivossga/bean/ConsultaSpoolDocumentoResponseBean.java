package pe.com.claro.procesararchivossga.bean;

import java.util.List;

public class ConsultaSpoolDocumentoResponseBean extends ResponseBean {

	private List<SpoolCabeceraBean> spoolCab;
	private List<SpoolDetalleBean> spoolDet;
	
	public List<SpoolCabeceraBean> getSpoolCab() {
		return spoolCab;
	}
	public void setSpoolCab(List<SpoolCabeceraBean> spoolCab) {
		this.spoolCab = spoolCab;
	}
	public List<SpoolDetalleBean> getSpoolDet() {
		return spoolDet;
	}
	public void setSpoolDet(List<SpoolDetalleBean> spoolDet) {
		this.spoolDet = spoolDet;
	}
	
}
