package pe.com.claro.procesararchivossga.bean;

import java.util.Date;

public class ConsultaSpoolDocumentoRequestBean {

	private String tipoDoc;
	private Date fechaIni;
	private Date fechaFin;
	
	public ConsultaSpoolDocumentoRequestBean() {
	}
	
	public ConsultaSpoolDocumentoRequestBean(String tipoDoc, Date fechaIni, Date fechaFin) {
		this.tipoDoc = tipoDoc;
		this.fechaIni = fechaIni;
		this.fechaFin = fechaFin;
	}
	
	public String getTipoDoc() {
		return tipoDoc;
	}
	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}
	public Date getFechaIni() {
		return fechaIni;
	}
	public void setFechaIni(Date fechaIni) {
		this.fechaIni = fechaIni;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	
}
