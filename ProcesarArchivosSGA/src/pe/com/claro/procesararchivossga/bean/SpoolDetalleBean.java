package pe.com.claro.procesararchivossga.bean;

public class SpoolDetalleBean {

	private String spoolDetSGA;

	public String getSpoolDetSGA() {
		return spoolDetSGA;
	}

	public void setSpoolDetSGA(String spoolDetSGA) {
		this.spoolDetSGA = spoolDetSGA;
	}

	@Override
	public String toString() {
		return spoolDetSGA;
	}
	
}
