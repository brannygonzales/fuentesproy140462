package pe.com.claro.procesararchivossga.bean;

public class SpoolCabeceraBean {

	private String spoolCabSGA;

	public String getSpoolCabSGA() {
		return spoolCabSGA;
	}

	public void setSpoolCabSGA(String spoolCabSGA) {
		this.spoolCabSGA = spoolCabSGA;
	}

	@Override
	public String toString() {
		return spoolCabSGA;
	}
	
}
