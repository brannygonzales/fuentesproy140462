package pe.com.claro.shell.sgagenerarajuste.bean;

import java.io.Serializable;

public class ListarRegistrosCursor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8518089530482327348L;
	private String amacnId;
	private String amadnId;
	private String amadcIdfac;
	private String amadcIdfacGen;
	private String amadvTicket;
	private String amadcCliente;
	private String amadvDocumento;
	private String amadvSerieDoc;
	private String amadcNumDoc;
	private String amadvMsisdnAfec;
	private String amadvMsisdnDevol;
	private String amadvIdinstprod;
	private String amadvCiclo;
	private String amadnMonto;
	private String amadnMontoCigv;
	private String amadnTasa;
	private String amadnInteres;
	private String amadnMontoTotal;
	private String amadvGlosa;
	private String amadnEstado;
	private String amadnCodigo;
	private String amadvMensaje;
	private String amadvUsureg;
	private String amaddFechareg;
	private String amadvUsumod;
	private String amaddFechamod;
	private String amadnIdcpto;
	private String amadnIdseccpto;
	private String amadvDesitm;
	
	public String getAmacnId() {
		return amacnId;
	}
	public void setAmacnId(String amacnId) {
		this.amacnId = amacnId;
	}
	public String getAmadnId() {
		return amadnId;
	}
	public void setAmadnId(String amadnId) {
		this.amadnId = amadnId;
	}
	public String getAmadcIdfac() {
		return amadcIdfac;
	}
	public void setAmadcIdfac(String amadcIdfac) {
		this.amadcIdfac = amadcIdfac;
	}
	public String getAmadcIdfacGen() {
		return amadcIdfacGen;
	}
	public void setAmadcIdfacGen(String amadcIdfacGen) {
		this.amadcIdfacGen = amadcIdfacGen;
	}
	public String getAmadvTicket() {
		return amadvTicket;
	}
	public void setAmadvTicket(String amadvTicket) {
		this.amadvTicket = amadvTicket;
	}
	public String getAmadcCliente() {
		return amadcCliente;
	}
	public void setAmadcCliente(String amadcCliente) {
		this.amadcCliente = amadcCliente;
	}
	public String getAmadvDocumento() {
		return amadvDocumento;
	}
	public void setAmadvDocumento(String amadvDocumento) {
		this.amadvDocumento = amadvDocumento;
	}
	public String getAmadvSerieDoc() {
		return amadvSerieDoc;
	}
	public void setAmadvSerieDoc(String amadvSerieDoc) {
		this.amadvSerieDoc = amadvSerieDoc;
	}
	public String getAmadcNumDoc() {
		return amadcNumDoc;
	}
	public void setAmadcNumDoc(String amadcNumDoc) {
		this.amadcNumDoc = amadcNumDoc;
	}
	public String getAmadvMsisdnAfec() {
		return amadvMsisdnAfec;
	}
	public void setAmadvMsisdnAfec(String amadvMsisdnAfec) {
		this.amadvMsisdnAfec = amadvMsisdnAfec;
	}
	public String getAmadvMsisdnDevol() {
		return amadvMsisdnDevol;
	}
	public void setAmadvMsisdnDevol(String amadvMsisdnDevol) {
		this.amadvMsisdnDevol = amadvMsisdnDevol;
	}
	public String getAmadvIdinstprod() {
		return amadvIdinstprod;
	}
	public void setAmadvIdinstprod(String amadvIdinstprod) {
		this.amadvIdinstprod = amadvIdinstprod;
	}
	public String getAmadvCiclo() {
		return amadvCiclo;
	}
	public void setAmadvCiclo(String amadvCiclo) {
		this.amadvCiclo = amadvCiclo;
	}
	public String getAmadnMonto() {
		return amadnMonto;
	}
	public void setAmadnMonto(String amadnMonto) {
		this.amadnMonto = amadnMonto;
	}
	public String getAmadnMontoCigv() {
		return amadnMontoCigv;
	}
	public void setAmadnMontoCigv(String amadnMontoCigv) {
		this.amadnMontoCigv = amadnMontoCigv;
	}
	public String getAmadnTasa() {
		return amadnTasa;
	}
	public void setAmadnTasa(String amadnTasa) {
		this.amadnTasa = amadnTasa;
	}
	public String getAmadnInteres() {
		return amadnInteres;
	}
	public void setAmadnInteres(String amadnInteres) {
		this.amadnInteres = amadnInteres;
	}
	public String getAmadnMontoTotal() {
		return amadnMontoTotal;
	}
	public void setAmadnMontoTotal(String amadnMontoTotal) {
		this.amadnMontoTotal = amadnMontoTotal;
	}
	public String getAmadvGlosa() {
		return amadvGlosa;
	}
	public void setAmadvGlosa(String amadvGlosa) {
		this.amadvGlosa = amadvGlosa;
	}
	public String getAmadnEstado() {
		return amadnEstado;
	}
	public void setAmadnEstado(String amadnEstado) {
		this.amadnEstado = amadnEstado;
	}
	public String getAmadnCodigo() {
		return amadnCodigo;
	}
	public void setAmadnCodigo(String amadnCodigo) {
		this.amadnCodigo = amadnCodigo;
	}
	public String getAmadvMensaje() {
		return amadvMensaje;
	}
	public void setAmadvMensaje(String amadvMensaje) {
		this.amadvMensaje = amadvMensaje;
	}
	public String getAmadvUsureg() {
		return amadvUsureg;
	}
	public void setAmadvUsureg(String amadvUsureg) {
		this.amadvUsureg = amadvUsureg;
	}
	public String getAmaddFechareg() {
		return amaddFechareg;
	}
	public void setAmaddFechareg(String amaddFechareg) {
		this.amaddFechareg = amaddFechareg;
	}
	public String getAmadvUsumod() {
		return amadvUsumod;
	}
	public void setAmadvUsumod(String amadvUsumod) {
		this.amadvUsumod = amadvUsumod;
	}
	public String getAmaddFechamod() {
		return amaddFechamod;
	}
	public void setAmaddFechamod(String amaddFechamod) {
		this.amaddFechamod = amaddFechamod;
	}
	public String getAmadnIdcpto() {
		return amadnIdcpto;
	}
	public void setAmadnIdcpto(String amadnIdcpto) {
		this.amadnIdcpto = amadnIdcpto;
	}
	public String getAmadnIdseccpto() {
		return amadnIdseccpto;
	}
	public void setAmadnIdseccpto(String amadnIdseccpto) {
		this.amadnIdseccpto = amadnIdseccpto;
	}
	public String getAmadvDesitm() {
		return amadvDesitm;
	}
	public void setAmadvDesitm(String amadvDesitm) {
		this.amadvDesitm = amadvDesitm;
	}

}
