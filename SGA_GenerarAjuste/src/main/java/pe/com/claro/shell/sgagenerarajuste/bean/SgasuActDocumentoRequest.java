package pe.com.claro.shell.sgagenerarajuste.bean;

import java.io.Serializable;

public class SgasuActDocumentoRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6332060489527537473L;
	private String kRegistro;
	private String kAmadvDocumento;
	private String kValor;
	public String getkRegistro() {
		return kRegistro;
	}
	public void setkRegistro(String kRegistro) {
		this.kRegistro = kRegistro;
	}
	public String getkValor() {
		return kValor;
	}
	public void setkValor(String kValor) {
		this.kValor = kValor;
	}
	public String getkAmadvDocumento() {
		return kAmadvDocumento;
	}
	public void setkAmadvDocumento(String kAmadvDocumento) {
		this.kAmadvDocumento = kAmadvDocumento;
	}
	
}
