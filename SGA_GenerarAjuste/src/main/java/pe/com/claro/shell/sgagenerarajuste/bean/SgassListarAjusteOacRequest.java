package pe.com.claro.shell.sgagenerarajuste.bean;

import java.io.Serializable;

public class SgassListarAjusteOacRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5030622585082450674L;
	
	private String kRegistro;

	public String getkRegistro() {
		return kRegistro;
	}

	public void setkRegistro(String kRegistro) {
		this.kRegistro = kRegistro;
	}

}
