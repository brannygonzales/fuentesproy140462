package pe.com.claro.shell.sgagenerarajuste.exception;

public class WSException extends BaseException {

    private static final long serialVersionUID = 1L;

    /**
     * Excepcion.
     * 
     * @param message
     *            mensaje de error, tipo String.
     */
    public WSException(String string) {
        super(string);
    }

    /**
     * Excepcion.
     * 
     * @param exception
     *            excepcion generada, tipo Exception.
     */
    public WSException(Exception exception) {
        super(exception);
    }

    /**
     * Excepcion.
     * 
     * @param message
     *            mensaje de error, tipo String.
     * @param exception
     *            excepcion generada, tipo Exception.
     */
    public WSException(String string, Exception exception) {
        super(string, exception);
    }

    /**
     * Excepcion.
     * 
     * @param code
     *            codigo de error, tipo String.
     * @param message
     *            mensaje de error, tipo String.
     */
    public WSException(String code, String message) {
        super(code, message);
    }

    /**
     * Excepcion.
     * 
     * @param code
     *            codigo de error, tipo String.
     * @param message
     *            mensaje de error, tipo String.
     * @param exception
     *            excepcion generada, tipo Exception.
     */
    public WSException(String code, String message, Exception exception) {
        super(code, message, exception);
    }

    /**
     * Excepcion para consumo de servicios web.
     * 
     * @param code
     *            codigo de error, tipo String.
     * @param message
     *            mensaje de error, tipo String.
     * @param nombreWS
     *            nombre del servicio web.
     * @param exception
     *            excepcion generada, tipo Exception.
     */
    public WSException(String code, String message, String nombreWS, Exception exception) {
        super(code, message, nombreWS, exception);
    }

}
