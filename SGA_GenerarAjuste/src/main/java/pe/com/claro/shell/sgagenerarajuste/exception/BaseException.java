package pe.com.claro.shell.sgagenerarajuste.exception;

public class BaseException extends Exception {

    private static final long serialVersionUID = 1L;

    private Exception exception;
    private String code;
    private String message;
    private String nombreSP;
    private String nombreBD;
    private String nombreWS;

    /**
     * Excepcion.
     * 
     * @param code
     *            codigo de error, tipo String.
     * @param message
     *            mensaje de error, tipo String.
     * @param exception
     *            excepcion generada, tipo Exception.
     */
    public BaseException(String code, String message, Exception exception) {
        super(message);
        this.exception = exception;
        this.code = code;
        this.message = message;
    }

    /**
     * Excepcion.
     * 
     * @param code
     *            codigo de error, tipo String.
     * @param message
     *            mensaje de error, tipo String.
     */
    public BaseException(String code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    /**
     * Excepcion.
     * 
     * @param message
     *            mensaje de error, tipo String.
     * @param exception
     *            excepcion generada, tipo Exception.
     */
    public BaseException(String message, Exception exception) {
        super(message);
        this.exception = exception;
        this.message = message;
    }

    /**
     * Excepcion.
     * 
     * @param exception
     *            excepcion generada, tipo Exception.
     */
    public BaseException(Exception exception) {
        this.exception = exception;
    }

    /**
     * Excepcion.
     * 
     * @param message
     *            mensaje de error, tipo String.
     */
    public BaseException(String message) {
        super(message);
        this.message = message;
    }

    /**
     * Excepcion para consumo de procedimientos.
     * 
     * @param code
     *            codigo de error, tipo String.
     * @param message
     *            mensaje de error, tipo String.
     * @param nombreBD
     *            nombre de Base de Datos, tipo String.
     * @param nombreSP
     *            nombre de Store Procedure, tipo String.
     * @param exception
     *            excepcion generada, tipo Exception.
     */
    public BaseException(String code, String message, String nombreBD, String nombreSP, Exception exception) {
        super(message);
        this.message = message;
        this.code = code;
        this.nombreBD = nombreBD;
        this.nombreSP = nombreSP;
    }

    /**
     * Excepcion para consumo de servicios web.
     * 
     * @param code
     *            codigo de error, tipo String.
     * @param message
     *            mensaje de error, tipo String.
     * @param nombreWS
     *            nombre del servicio web.
     * @param exception
     *            excepcion generada, tipo Exception.
     */
    public BaseException(String code, String message, String nombreWS, Exception exception) {
        super(message);
        this.message = message;
        this.code = code;
        this.nombreWS = nombreWS;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }

    public String getCode() {
        return code;
    }

    public String getNombreSP() {
        return nombreSP;
    }

    public String getNombreBD() {
        return nombreBD;
    }

    public String getNombreWS() {
        return nombreWS;
    }

}
