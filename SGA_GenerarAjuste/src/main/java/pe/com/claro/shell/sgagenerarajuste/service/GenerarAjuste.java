package pe.com.claro.shell.sgagenerarajuste.service;


import pe.com.claro.eai.util.enviocorreo.types.EnviarCorreoRequest;
import pe.com.claro.eai.util.enviocorreo.types.EnviarCorreoResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.PrPrcInterfaceAjusteRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.PrPrcInterfaceAjusteResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.PrValRegistroRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.PrValRegistroResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasiAjusteDocumentoRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasiAjusteDocumentoResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgassListarAjusteOacRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgassListarAjusteOacResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgassListarRegEnviarOacRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgassListarRegEnviarOacResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgassListarRegistrosRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgassListarRegistrosResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuActDocumentoRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuActDocumentoResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuConsultaLoteResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuValidaOacRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuValidaOacResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuValidarRegMasivoRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuValidarRegMasivoResponse;
import pe.com.claro.shell.sgagenerarajuste.exception.DBException;
import pe.com.claro.shell.sgagenerarajuste.exception.WSException;

public interface GenerarAjuste {
	public void generandoAjuste(String mensajeTransaccion,String idTransaccion) throws DBException, WSException;
	public SgasuConsultaLoteResponse consultaLote(String mensajeTransaccion)throws DBException;
	public SgasuValidarRegMasivoResponse validarRegistrosSga(String mensajeTransaccion,SgasuValidarRegMasivoRequest request)throws DBException;
	public SgassListarRegEnviarOacResponse listarRegistrosEnvioOac(String mensajeTransaccion,SgassListarRegEnviarOacRequest request)throws DBException;
	public PrValRegistroResponse validarAjusteOac(String mensajeTransaccion,PrValRegistroRequest request)throws DBException;
	
	public SgasiAjusteDocumentoResponse ejecutarAjusteSga(String mensajeTransaccion,SgasiAjusteDocumentoRequest request)throws DBException;
	public PrPrcInterfaceAjusteResponse registrarAjusteOac(String mensajeTransaccion,PrPrcInterfaceAjusteRequest request)throws DBException;
	public SgasuActDocumentoResponse actualizarAjusteEstadoSga(String mensajeTransaccion,SgasuActDocumentoRequest request)throws DBException;
	public SgassListarRegistrosResponse reporteProceso(String mensajeTransaccion,SgassListarRegistrosRequest request,EnviarCorreoRequest enCoRq)throws DBException;
	public SgasuValidaOacResponse actualizarEstadoSga(String mensajeTransaccion,SgasuValidaOacRequest request)throws DBException;
	public SgassListarAjusteOacResponse listarAjusteOac(String mensajeTransaccion,SgassListarAjusteOacRequest request)throws DBException;
	
	public EnviarCorreoResponse enviarCorreo(String msgTransaction,EnviarCorreoRequest request) throws WSException;
}
