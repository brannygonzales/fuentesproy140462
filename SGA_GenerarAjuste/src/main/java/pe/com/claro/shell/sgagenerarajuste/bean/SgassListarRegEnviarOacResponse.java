package pe.com.claro.shell.sgagenerarajuste.bean;

import java.io.Serializable;
import java.util.ArrayList;

public class SgassListarRegEnviarOacResponse implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8704702012205080723L;
	private ArrayList<ListarRegEnviarOacCursor> koCursor;
	private String koCodigo;
	private String koMensaje;
	public String getKoCodigo() {
		return koCodigo;
	}
	public void setKoCodigo(String koCodigo) {
		this.koCodigo = koCodigo;
	}
	public String getKoMensaje() {
		return koMensaje;
	}
	public void setKoMensaje(String koMensaje) {
		this.koMensaje = koMensaje;
	}
	public ArrayList<ListarRegEnviarOacCursor> getKoCursor() {
		return koCursor;
	}
	public void setKoCursor(ArrayList<ListarRegEnviarOacCursor> koCursor) {
		this.koCursor = koCursor;
	}
	
}
