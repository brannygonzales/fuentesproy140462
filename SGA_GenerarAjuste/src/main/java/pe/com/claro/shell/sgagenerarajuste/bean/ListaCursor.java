package pe.com.claro.shell.sgagenerarajuste.bean;

import java.io.Serializable;

public class ListaCursor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7644467467682159778L;
	private String docreferencia;
	private String ajuste;
	private String descripcion;
	private String codCliente;
	private String monto;
	
	public String getCodCliente() {
		return codCliente;
	}
	public void setCodCliente(String codCliente) {
		this.codCliente = codCliente;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getDocreferencia() {
		return docreferencia;
	}
	public void setDocreferencia(String docreferencia) {
		this.docreferencia = docreferencia;
	}
	public String getAjuste() {
		return ajuste;
	}
	public void setAjuste(String ajuste) {
		this.ajuste = ajuste;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
