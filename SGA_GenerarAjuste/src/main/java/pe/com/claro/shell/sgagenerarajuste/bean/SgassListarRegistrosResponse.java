package pe.com.claro.shell.sgagenerarajuste.bean;

import java.io.Serializable;
import java.util.ArrayList;

public class SgassListarRegistrosResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8692692651674489257L;
	private ArrayList<ListaCursor> koCursor;
	private String koCodigo;
	private String koMensaje;
	private String koCorreo;
	private String koNombreArchivo;
	
	public String getKoCorreo() {
		return koCorreo;
	}
	public void setKoCorreo(String koCorreo) {
		this.koCorreo = koCorreo;
	}
	public String getKoNombreArchivo() {
		return koNombreArchivo;
	}
	public void setKoNombreArchivo(String koNombreArchivo) {
		this.koNombreArchivo = koNombreArchivo;
	}
	public String getKoCodigo() {
		return koCodigo;
	}
	public void setKoCodigo(String koCodigo) {
		this.koCodigo = koCodigo;
	}
	public String getKoMensaje() {
		return koMensaje;
	}
	public void setKoMensaje(String koMensaje) {
		this.koMensaje = koMensaje;
	}
	public ArrayList<ListaCursor> getKoCursor() {
		return koCursor;
	}
	public void setKoCursor(ArrayList<ListaCursor> koCursor) {
		this.koCursor = koCursor;
	}
	
}
