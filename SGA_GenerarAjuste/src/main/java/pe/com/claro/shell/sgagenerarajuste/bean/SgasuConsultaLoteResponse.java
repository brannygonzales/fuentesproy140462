package pe.com.claro.shell.sgagenerarajuste.bean;

import java.io.Serializable;

public class SgasuConsultaLoteResponse implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8098258874473409210L;
	private String koRegistro;
	private String koCodigo;
	private String koMensaje;
	public String getKoRegistro() {
		return koRegistro;
	}
	public void setKoRegistro(String koRegistro) {
		this.koRegistro = koRegistro;
	}
	public String getKoCodigo() {
		return koCodigo;
	}
	public void setKoCodigo(String koCodigo) {
		this.koCodigo = koCodigo;
	}
	public String getKoMensaje() {
		return koMensaje;
	}
	public void setKoMensaje(String koMensaje) {
		this.koMensaje = koMensaje;
	}
	
}
