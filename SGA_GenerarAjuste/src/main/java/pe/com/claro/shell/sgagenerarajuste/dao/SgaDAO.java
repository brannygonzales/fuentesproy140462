package pe.com.claro.shell.sgagenerarajuste.dao;

import pe.com.claro.shell.sgagenerarajuste.bean.SgasiAjusteDocumentoRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasiAjusteDocumentoResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgassListarAjusteOacRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgassListarAjusteOacResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgassListarRegEnviarOacRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgassListarRegEnviarOacResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgassListarRegistrosRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgassListarRegistrosResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuActDocumentoRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuActDocumentoResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuConsultaLoteResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuValidaOacRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuValidarRegMasivoRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuValidarRegMasivoResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuValidaOacResponse;
import pe.com.claro.shell.sgagenerarajuste.exception.DBException;

public interface SgaDAO {
	public SgasuConsultaLoteResponse consultaLote(String mensajeTransaccion,SgasuConsultaLoteResponse response)throws DBException;
	public SgasuValidarRegMasivoResponse validarRegistrosSga(String mensajeTransaccion,SgasuValidarRegMasivoRequest request,SgasuValidarRegMasivoResponse response)throws DBException;
	public SgassListarRegEnviarOacResponse listarRegistrosEnvioOac(String mensajeTransaccion,SgassListarRegEnviarOacRequest request,SgassListarRegEnviarOacResponse response)throws DBException;
	public SgasiAjusteDocumentoResponse ejecutarAjusteSga(String mensajeTransaccion,SgasiAjusteDocumentoRequest request,SgasiAjusteDocumentoResponse response)throws DBException;
	public SgasuActDocumentoResponse actualizarAjusteEstadoSga(String mensajeTransaccion,SgasuActDocumentoRequest request,SgasuActDocumentoResponse response)throws DBException;
	public SgassListarRegistrosResponse reporteProceso(String mensajeTransaccion,SgassListarRegistrosRequest request,SgassListarRegistrosResponse response)throws DBException;
	
	public SgasuValidaOacResponse actualizarEstadoSga(String mensajeTransaccion,SgasuValidaOacRequest request,SgasuValidaOacResponse response)throws DBException;
	public SgassListarAjusteOacResponse listarAjusteOac(String mensajeTransaccion,SgassListarAjusteOacRequest request,SgassListarAjusteOacResponse response)throws DBException;

}
