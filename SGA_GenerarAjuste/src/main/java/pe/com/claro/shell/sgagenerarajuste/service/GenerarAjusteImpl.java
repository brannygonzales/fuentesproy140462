package pe.com.claro.shell.sgagenerarajuste.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.claro.eai.util.enviocorreo.EnvioCorreoSBPortType;
import pe.com.claro.eai.util.enviocorreo.types.AuditTypeRequest;
import pe.com.claro.eai.util.enviocorreo.types.EnviarCorreoRequest;
import pe.com.claro.eai.util.enviocorreo.types.EnviarCorreoResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.ListaCursor;
import pe.com.claro.shell.sgagenerarajuste.bean.ListarRegEnviarOacCursor;
import pe.com.claro.shell.sgagenerarajuste.bean.PrPrcInterfaceAjusteRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.PrPrcInterfaceAjusteResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.PrValRegistroRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.PrValRegistroResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasiAjusteDocumentoRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasiAjusteDocumentoResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgassListarAjusteOacCursor;
import pe.com.claro.shell.sgagenerarajuste.bean.SgassListarAjusteOacRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgassListarAjusteOacResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgassListarRegEnviarOacRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgassListarRegEnviarOacResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgassListarRegistrosRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgassListarRegistrosResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuActDocumentoRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuActDocumentoResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuConsultaLoteResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuValidaOacRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuValidaOacResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuValidarRegMasivoRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuValidarRegMasivoResponse;
import pe.com.claro.shell.sgagenerarajuste.dao.OacDAO;
import pe.com.claro.shell.sgagenerarajuste.dao.SgaDAO;
import pe.com.claro.shell.sgagenerarajuste.exception.DBException;
import pe.com.claro.shell.sgagenerarajuste.exception.WSException;
import pe.com.claro.shell.sgagenerarajuste.util.Constantes;
import pe.com.claro.shell.sgagenerarajuste.util.JAXBUtilitarios;
import pe.com.claro.shell.sgagenerarajuste.util.PropertiesExternos;

@Service
public class GenerarAjusteImpl implements GenerarAjuste{
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());
	@Autowired
	private PropertiesExternos propertiesExternos;
	
	@Autowired
	private SgaDAO sgaDAO;
	
	@Autowired
	private OacDAO oacDAO;
	
	@Autowired
	private EnvioCorreoSBPortType envioCorreoSBPortType;
	
	@Override
	public void generandoAjuste(String mensajeTransaccion,String idTransaccion) throws DBException, WSException{
SgasuConsultaLoteResponse response=new SgasuConsultaLoteResponse();
		
		EnviarCorreoRequest enCoRq= new EnviarCorreoRequest();
		AuditTypeRequest auditTypeRequest = new AuditTypeRequest();
		//Data para correo			
		
		auditTypeRequest.setIdTransaccion(idTransaccion);
		auditTypeRequest.setCodigoAplicacion("");
		auditTypeRequest.setUsrAplicacion("");
		
		enCoRq.setRemitente(propertiesExternos.envioCorreoRemitente);
		enCoRq.setDestinatario(propertiesExternos.envioCorreoDestinatario);
		enCoRq.setAuditRequest(auditTypeRequest);
		enCoRq.setAsunto(propertiesExternos.envioCorreoAsunto);
		enCoRq.setHtmlFlag(propertiesExternos.htmlFlag);
		SgassListarRegistrosRequest lsRgRq= new SgassListarRegistrosRequest();
		try {

			response=consultaLote(idTransaccion);
			SgasuValidarRegMasivoRequest request= new SgasuValidarRegMasivoRequest();
			
			if(response.getKoRegistro()!=null && !response.getKoRegistro().isEmpty() && !response.getKoRegistro().equals("null")) {
				lsRgRq.setkRegistro(response.getKoRegistro());
				request.setkRegistro(response.getKoRegistro());
				SgasuValidarRegMasivoResponse vresponse;
				vresponse=validarRegistrosSga(mensajeTransaccion,request);	
				if(vresponse.getKoCodigo()!=null && !vresponse.getKoCodigo().isEmpty() && vresponse.getKoCodigo().equals(propertiesExternos.vresponseGetKoCodigoOk)) {
					SgassListarRegEnviarOacRequest req= new SgassListarRegEnviarOacRequest();
					SgassListarRegEnviarOacResponse oacRes;
					req.setkRegistro(response.getKoRegistro());
					oacRes=listarRegistrosEnvioOac(mensajeTransaccion,req);
					if(oacRes.getKoCursor()!=null && oacRes.getKoCursor().size()>0) {
						for(ListarRegEnviarOacCursor rgcursor:oacRes.getKoCursor()) {
							PrValRegistroRequest valReq= new PrValRegistroRequest();
							PrValRegistroResponse valOac;
							valReq.setPdFechaAjuste(rgcursor.getPdFechaAjuste());
							valReq.setPnMontoAjuste(rgcursor.getPnMontoAjuste());
							valReq.setPnSaldoAjuste(rgcursor.getPnSaldoAjuste());
							valReq.setPvCodAplicacion(rgcursor.getPvCodAplicacion());
							valReq.setPvCodCuenta(rgcursor.getPvCodCuenta());
							valReq.setPvDocsAjuste(rgcursor.getPvDocsAjuste());
							valReq.setPvEstado(rgcursor.getPvEstado());
							valReq.setPvMonedaAjuste(rgcursor.getPvMonedaAjuste());
							valReq.setPvNroDocAjuste(rgcursor.getPvNroDocAjuste());
							valReq.setPvTipoAjuste(rgcursor.getPvTipoAjuste());
							valReq.setPvTipoServicio(rgcursor.getPvTipoServicio());
							valReq.setPvDocsAjusteSGA(rgcursor.getPvDocsAjusteSGA());
							valOac=validarAjusteOac(mensajeTransaccion,valReq);
							

							SgasuValidaOacRequest vaOacRq= new SgasuValidaOacRequest();
							if(valOac.getXvStatus().equals(propertiesExternos.valOacGetXvStatusOk)) {
								vaOacRq.setkCodigo(propertiesExternos.vaOacRqSetkCodigoOk);
								vaOacRq.setkAmadvDocumento(rgcursor.getPvDocsAjusteSGA());
								vaOacRq.setkRegistro(response.getKoRegistro());
								//QUE HACER CON LA SALIDA
							actualizarEstadoSga( mensajeTransaccion, vaOacRq);
							}
							else {
								vaOacRq.setkCodigo(propertiesExternos.vaOacRqSetkCodigoErr);
								vaOacRq.setkAmadvDocumento(rgcursor.getPvDocsAjusteSGA());
								vaOacRq.setkRegistro(response.getKoRegistro());
							actualizarEstadoSga( mensajeTransaccion, vaOacRq);
							
							}
						}
						
						SgasiAjusteDocumentoRequest docRequ= new  SgasiAjusteDocumentoRequest();
						
						docRequ.setkRegistro(response.getKoRegistro());						
						@SuppressWarnings("unused")
						SgasiAjusteDocumentoResponse docResp;
						docResp=ejecutarAjusteSga(mensajeTransaccion,docRequ);
						
						SgassListarAjusteOacRequest lAjRq= new SgassListarAjusteOacRequest();
						lAjRq.setkRegistro(response.getKoRegistro());
						SgassListarAjusteOacResponse lAjRs;
						lAjRs= listarAjusteOac( mensajeTransaccion, lAjRq);
						if(lAjRs.getKoCursor()!=null && lAjRs.getKoCursor().size()>0) {
							for(SgassListarAjusteOacCursor lAjOaCur:lAjRs.getKoCursor()) {
								PrPrcInterfaceAjusteRequest prInAjRq= new PrPrcInterfaceAjusteRequest();
								
								prInAjRq.setPdFechaAjuste(lAjOaCur.getPdFechaAjuste());
								prInAjRq.setPdFechaCancelacion(lAjOaCur.getPdFechaCancelacion());
								prInAjRq.setPdFechaVencAjuste(lAjOaCur.getPdFechaVencAjuste());
								prInAjRq.setPnMontoAjuste(lAjOaCur.getPnMontoAjuste());
								prInAjRq.setPnSaldoAjuste(lAjOaCur.getPnSaldoAjuste());
								prInAjRq.setPvCodAplicacion(lAjOaCur.getPvCodAplicacion());
								prInAjRq.setPvCodCuenta(lAjOaCur.getPvCodCuenta());
								prInAjRq.setPvCodMotivoAjuste(lAjOaCur.getPvCodMotivoAjuste());
								prInAjRq.setPvDescripAjuste(lAjOaCur.getPvDescripAjuste());
								prInAjRq.setPvDocsAjuste(lAjOaCur.getPvDocsAjuste());
								prInAjRq.setPvEstado(lAjOaCur.getPvEstado());
								prInAjRq.setPvIdReclamoOrigen(lAjOaCur.getPvIdReclamoOrigen());
								prInAjRq.setPvMonedaAjuste(lAjOaCur.getPvMonedaAjuste());
								prInAjRq.setPvNroDocAjuste(lAjOaCur.getPvNroDocAjuste());
								prInAjRq.setPvTipoAjuste(lAjOaCur.getPvTipoAjuste());
								prInAjRq.setPvTipoOperacion(lAjOaCur.getPvTipoOperacion());
								prInAjRq.setPvTipoServicio(lAjOaCur.getPvTipoServicio());
								prInAjRq.setPvTrxIdWs(lAjOaCur.getPvTrxIdWs());
								prInAjRq.setPvUsuarioAplic(lAjOaCur.getPvUsuarioAplic());
								
								PrPrcInterfaceAjusteResponse prInAjRs;
								prInAjRs=registrarAjusteOac( mensajeTransaccion,prInAjRq);
								SgasuActDocumentoRequest acDoRq= new SgasuActDocumentoRequest();
								if(prInAjRs.getXvStatus()!=null && !prInAjRs.getXvStatus().isEmpty() && prInAjRs.getXvStatus().equals(propertiesExternos.prInAjRsGetXvStatusOk)) {	
									acDoRq.setkValor(propertiesExternos.acDoRqSetkValorOk);
									acDoRq.setkRegistro(response.getKoRegistro());
									acDoRq.setkAmadvDocumento(lAjOaCur.getPvNroDocAjusteSGA());
									actualizarAjusteEstadoSga( mensajeTransaccion,acDoRq );
									
								}else {									
									acDoRq.setkValor(propertiesExternos.acDoRqSetkValorErr);
									acDoRq.setkRegistro(response.getKoRegistro());
									acDoRq.setkAmadvDocumento(lAjOaCur.getPvNroDocAjusteSGA());
									actualizarAjusteEstadoSga( mensajeTransaccion,acDoRq );
								}
							}						
							
							reporteProceso( mensajeTransaccion, lsRgRq,enCoRq);						
														
													
						}else {							
//							enCoRq.setMensaje("No se lograron ajustar para el registro/lote: "+response.getKoRegistro()+"\n"+
//						"Debido a que la lista viene nula, vacia o igual a 0");		
							enCoRq.setMensaje(response.getKoRegistro());
							reporteProceso( mensajeTransaccion, lsRgRq,enCoRq);
							
						}
					}else {						
						enCoRq.setMensaje("Envio Correo listarRegistrosEnvioOac cursor vacio ");						
						reporteProceso( mensajeTransaccion, lsRgRq,enCoRq);
					}					
				}else {				
				enCoRq.setMensaje(vresponse.getKoMensaje());				
				reporteProceso( mensajeTransaccion, lsRgRq,enCoRq);
				}
			}
			else{
				enCoRq.setMensaje("No existe registro.");				
				reporteProceso( mensajeTransaccion, lsRgRq,enCoRq);
			}
			
		} catch (DBException e) {
			LOGGER.error(Constantes.KO_CODIGO +e.getCode());
			LOGGER.error(Constantes.KO_MENSAJE +e.getMessage());
			LOGGER.error("En la base de datos");
			enCoRq.setMensaje(e.getCode() +Constantes.TEXTO_DOSPUNTOS +e.getMessage());	
			enviarCorreo( mensajeTransaccion, enCoRq);			
		} catch (Exception e) {			
			LOGGER.error("ERROR: en la Shell");
			enCoRq.setMensaje("ERROR: en la Shell");
			enviarCorreo( mensajeTransaccion, enCoRq);
		}
	}
	
	@Override
	public SgasuConsultaLoteResponse consultaLote(String mensajeTransaccion)
			throws DBException {
		LOGGER.info(mensajeTransaccion + " [INICIANDO Actividad 1: consultaLote]");
		SgasuConsultaLoteResponse response=new SgasuConsultaLoteResponse();
		try {
			response=sgaDAO.consultaLote(mensajeTransaccion, response);
			
		} catch (DBException e) {
			response.setKoCodigo(e.getCode());
			response.setKoMensaje(e.getMessage());
			LOGGER.error(Constantes.KO_CODIGO +e.getCode());
			LOGGER.error(Constantes.KO_MENSAJE +e.getMessage());
			LOGGER.error("En la base de datos");		
		} catch (Exception e) {			
			LOGGER.error("ERROR: al consultaLote");
		}finally {
			LOGGER.info(mensajeTransaccion + " [FINALIZAND Actividad 1: consultaLote]");
		}
		return response;
	}
	
	
	
	
	@Override
	public SgasuValidarRegMasivoResponse validarRegistrosSga(String mensajeTransaccion,
			SgasuValidarRegMasivoRequest request) throws DBException {
		LOGGER.info(mensajeTransaccion + " [INICIANDO Actividad 2: validarRegistrosSga]");		
	
		SgasuValidarRegMasivoResponse response= new SgasuValidarRegMasivoResponse();

		try {			
			response=sgaDAO.validarRegistrosSga(mensajeTransaccion,request, response);		

		} catch (DBException e) {
			response.setKoCodigo(e.getCode());
			response.setKoMensaje(e.getMessage());
			LOGGER.error(Constantes.KO_CODIGO +e.getCode());
			LOGGER.error(Constantes.KO_MENSAJE +e.getMessage());
			LOGGER.error("En la base de datos");
			
		} catch (Exception e) {
			LOGGER.error("ERROR: al spoolFacturacion");
		}finally {
			LOGGER.info(mensajeTransaccion + " [FINALIZAND Actividad 2: validarRegistrosSga]");
		}
		return response;
	}

	@Override
	public SgassListarRegEnviarOacResponse listarRegistrosEnvioOac(String mensajeTransaccion,
			SgassListarRegEnviarOacRequest request) throws DBException {
		LOGGER.info(mensajeTransaccion + " [INICIANDO Actividad 3: listarRegistrosEnvioOac]");
		SgassListarRegEnviarOacResponse response= new SgassListarRegEnviarOacResponse();
		try {			
			response=sgaDAO.listarRegistrosEnvioOac(mensajeTransaccion,request, response);

		} catch (DBException e) {
			response.setKoCodigo(e.getCode());
			response.setKoMensaje(e.getMessage());
			LOGGER.error(Constantes.KO_CODIGO +e.getCode());
			LOGGER.error(Constantes.KO_MENSAJE +e.getMessage());
			LOGGER.error("En la base de datos");
			
		} catch (Exception e) {
			LOGGER.error("ERROR: al spoolFacturacion");
		}finally {
			LOGGER.info(mensajeTransaccion + " [FINALIZAND Actividad 3: listarRegistrosEnvioOac]");
		}
		return response;
	}

	@Override
	public PrValRegistroResponse validarAjusteOac(String mensajeTransaccion, PrValRegistroRequest request) throws DBException {
		PrValRegistroResponse response =new PrValRegistroResponse();
		
		LOGGER.info(mensajeTransaccion + " [INICIANDO Actividad 4: validarAjusteOac]");
		try {
			
			response=oacDAO.validarAjusteOac(mensajeTransaccion, request, response);
			
		} catch (DBException e) {
			response.setXvStatus(e.getCode());
			response.setXvMessage(e.getMessage());
			LOGGER.error(Constantes.XV_STATUS +e.getCode());
			LOGGER.error(Constantes.XV_MESSAGE +e.getMessage());
			LOGGER.error("En la base de datos");
			
		} catch (Exception e) {
			LOGGER.error("ERROR: al spoolFacturacion");
		}finally {
			LOGGER.info(mensajeTransaccion + " [FINALIZAND Actividad 4: validarAjusteOac]");
		}
		return response;
	}
	

	@Override
	public SgasiAjusteDocumentoResponse ejecutarAjusteSga(String mensajeTransaccion,
			SgasiAjusteDocumentoRequest request) throws DBException {
		SgasiAjusteDocumentoResponse response = new SgasiAjusteDocumentoResponse();
		LOGGER.info(mensajeTransaccion + " [INICIANDO Actividad 6: ejecutarAjusteSga]");
		try {			
			response=sgaDAO.ejecutarAjusteSga(mensajeTransaccion, request, response);			
		} catch (DBException e) {
			response.setKoCodigo(e.getCode());
			response.setKoMensaje(e.getMessage());
			LOGGER.error(Constantes.KO_CODIGO +e.getCode());
			LOGGER.error(Constantes.KO_MENSAJE +e.getMessage());
			LOGGER.error("En la base de datos");
			
		} catch (Exception e) {
			LOGGER.error("ERROR: al spoolFacturacion");
		}finally {
			LOGGER.info(mensajeTransaccion + " [FINALIZAND Actividad 6: ejecutarAjusteSga]");
		}
		return response;
	}

	@Override
	public PrPrcInterfaceAjusteResponse registrarAjusteOac(String mensajeTransaccion,PrPrcInterfaceAjusteRequest request) throws DBException {
		PrPrcInterfaceAjusteResponse response = new PrPrcInterfaceAjusteResponse();
		LOGGER.info(mensajeTransaccion + " [INICIANDO Actividad 8: registrarAjusteOac]");
		try {
			
			response=oacDAO.registrarAjusteOac(mensajeTransaccion, request, response);

		} catch (DBException e) {
			response.setXvStatus(e.getCode());
			response.setXvMessage(e.getMessage());
			LOGGER.error(Constantes.XV_STATUS +e.getCode());
			LOGGER.error(Constantes.XV_MESSAGE +e.getMessage());
			LOGGER.error("En la base de datos");
			
		} catch (Exception e) {
			LOGGER.error("ERROR: al spoolFacturacion");
		}finally {
			LOGGER.info(mensajeTransaccion + " [FINALIZAND Actividad 8: registrarAjusteOac]");
		}
		return response;
	}

	@Override
	public SgasuActDocumentoResponse actualizarAjusteEstadoSga(String mensajeTransaccion, SgasuActDocumentoRequest request) throws DBException {
		LOGGER.info(mensajeTransaccion + " [INICIANDO Actividad 9: actualizarAjusteEstadoSga]");
		SgasuActDocumentoResponse response= new SgasuActDocumentoResponse();
		try {			
			response=sgaDAO.actualizarAjusteEstadoSga(mensajeTransaccion, request, response);
		} catch (DBException e) {
			response.setKoCodigo(e.getCode());
			response.setKoMensaje(e.getMessage());
			LOGGER.error(Constantes.KO_CODIGO +e.getCode());
			LOGGER.error(Constantes.KO_MENSAJE +e.getMessage());
			LOGGER.error("En la base de datos");
			
		} catch (Exception e) {
			LOGGER.error("ERROR: al spoolFacturacion");
		}finally {
			LOGGER.info(mensajeTransaccion + " [FINALIZAND Actividad 9: actualizarAjusteEstadoSga]");
		}
		return response;
	}

	@Override
	public SgassListarRegistrosResponse reporteProceso(String mensajeTransaccion, SgassListarRegistrosRequest request,EnviarCorreoRequest enCoRq) throws DBException {
		LOGGER.info(mensajeTransaccion + " [INICIANDO Actividad 10: reporteProceso]");
		SgassListarRegistrosResponse response = new SgassListarRegistrosResponse();

		try {
			String detalleCorreo="Detalle de la ejecución del archivo " +response.getKoNombreArchivo()+ " de generación de ajuste NOTA CREDITO de Clientes SGA. \n \t";
			String notaGenerado="";
			String notaNoGenerado="";
			if(request!=null && request.getkRegistro()!=null && !request.getkRegistro().isEmpty() && !request.getkRegistro().equals("null")) {
				response=sgaDAO.reporteProceso(mensajeTransaccion, request, response);
				detalleCorreo="Detalle de la ejecución del archivo " +response.getKoNombreArchivo()+ " de generación de ajuste NOTA CREDITO de Clientes SGA. \n \t";
					
					detalleCorreo+="Cantidad de Registros Cargados: "+response.getKoCursor().size()+"\n \t";
					int i=0;
					int j=0;
					for(ListaCursor listCu:response.getKoCursor()) {
						if(listCu.getDescripcion().equalsIgnoreCase(propertiesExternos.listCuOk)) {
							i++;
							notaGenerado+=listCu.getDocreferencia()+" | "+listCu.getCodCliente()+" | "+listCu.getAjuste()+" | "+listCu.getMonto()+"\n \t";
						}
						else {
							j++;
							notaNoGenerado+=listCu.getDocreferencia()+" | "+listCu.getCodCliente()+" | "+listCu.getDescripcion()+"\n \t";
						}
					}
					detalleCorreo+="Cantidad de ajuste Generados: "+i+"\n \t";
					detalleCorreo+="DOCREFERENCIA | CODIGOCLIENTE | NUMEROAJUSTE | IMPORTE"+"\n \t";
					detalleCorreo+=notaGenerado+"\n";
					detalleCorreo+="Cantidad de ajuste no Generados: "+j+"\n \t";
					detalleCorreo+="DOCREFERENCIA | CODIGOCLIENTE | MOTIVO"+"\n \t";
					detalleCorreo+=notaNoGenerado+"\n";
					enCoRq.setMensaje(detalleCorreo);
					enCoRq.setDestinatario(response.getKoCorreo());		
					enCoRq.setAsunto(propertiesExternos.envioCorreoAsunto+" "+response.getKoNombreArchivo());
					enviarCorreo( mensajeTransaccion, enCoRq);
		
			}else {
				detalleCorreo+=enCoRq.getMensaje();
				enCoRq.setMensaje(detalleCorreo);
				enviarCorreo( mensajeTransaccion, enCoRq);	
			}
				
				
		} catch (DBException e) {
			response.setKoCodigo(e.getCode());
			response.setKoMensaje(e.getMessage());
			LOGGER.error(Constantes.KO_CODIGO +e.getCode());
			LOGGER.error(Constantes.KO_MENSAJE +e.getMessage());
			LOGGER.error("En la base de datos");
			
		} catch (Exception e) {
			LOGGER.error("ERROR: al reporteProceso");
		}finally {
			LOGGER.info(mensajeTransaccion + " [FINALIZAND Actividad 10: reporteProceso]");
		}
		
		return response;
	}

	@Override
	public EnviarCorreoResponse enviarCorreo(String msgTransaction, EnviarCorreoRequest request) throws WSException {
		String nomMetodo = "enviarCorreo";
		LOGGER.info(msgTransaction + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,nomMetodo);
		EnviarCorreoResponse response= new EnviarCorreoResponse();
		LOGGER.info(msgTransaction + " Request de entrada: " + JAXBUtilitarios
				.anyObjectToXmlText(request));		
		
		try {
			response=envioCorreoSBPortType.enviarCorreo(request);
			LOGGER.info(msgTransaction + "Response devuelto: [" + JAXBUtilitarios.anyObjectToXmlText(response) + "]");
		} catch (Exception e)
	    {
			String nombreWS = propertiesExternos.envioCorreoWsdl;
			String msjError = propertiesExternos.codIdt3.replace("$ws", nombreWS) + " " +e.getMessage() ; 
	    	String codError = propertiesExternos.mensajeIdt3;    
	    	
	    	LOGGER.error( msgTransaction + " ERROR: [Exception WS: "+nombreWS+" ] - [" + e.getMessage() + "] ",e );
	    	throw new WSException( codError, msjError, nombreWS, e );
	    }
	
	finally {
		LOGGER.info(msgTransaction + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,nomMetodo);
	}	
		return response;

	}

	@Override
	public SgasuValidaOacResponse actualizarEstadoSga(String mensajeTransaccion, SgasuValidaOacRequest request)
			throws DBException {
		LOGGER.info(mensajeTransaccion + " [INICIANDO Actividad 5: actualizarEstadoSga]");
		
		
		SgasuValidaOacResponse response = new SgasuValidaOacResponse();
		try {
			response=sgaDAO.actualizarEstadoSga(mensajeTransaccion, request, response);
		} catch (DBException e) {
			response.setKoCodigo(e.getCode());
			response.setKoMensaje(e.getMessage());
			LOGGER.error(Constantes.KO_CODIGO +e.getCode());
			LOGGER.error(Constantes.KO_MENSAJE +e.getMessage());
			LOGGER.error("En la base de datos");
			
		} catch (Exception e) {
			LOGGER.error("ERROR: al spoolFacturacion");
		}finally {
			LOGGER.info(mensajeTransaccion + " [FINALIZAND Actividad 5: actualizarEstadoSga]");
		}
		
		return response;
	}

	@Override
	public SgassListarAjusteOacResponse listarAjusteOac(String mensajeTransaccion, SgassListarAjusteOacRequest request)
			throws DBException {
		LOGGER.info(mensajeTransaccion + " [INICIANDO Actividad 7: validarAjusteOac]");
		SgassListarAjusteOacResponse response = new SgassListarAjusteOacResponse();
		try {
			response=sgaDAO.listarAjusteOac(mensajeTransaccion, request, response);
		} catch (DBException e) {
			response.setKoCodigo(e.getCode());
			response.setKoMensaje(e.getMessage());
			LOGGER.error(Constantes.KO_CODIGO +e.getCode());
			LOGGER.error(Constantes.KO_MENSAJE +e.getMessage());
			LOGGER.error("En la base de datos");
			
		} catch (Exception e) {
			LOGGER.error("ERROR: al spoolFacturacion");
		}finally {
			LOGGER.info(mensajeTransaccion + " [FINALIZAND Actividad 7: validarAjusteOac]");
		}
		
		return response;
	}



	



}
