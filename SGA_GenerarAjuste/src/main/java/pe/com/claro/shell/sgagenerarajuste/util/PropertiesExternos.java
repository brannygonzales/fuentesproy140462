package pe.com.claro.shell.sgagenerarajuste.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PropertiesExternos {
    @Value("${log4j.file.dir}")
    public String vLog4JDir;
    
	// Conexion a TIEMEAI
    @Value("${db.sga}")
    public String dbSga;
    @Value("${db.oac}")
    public String dbOac;
    
    @Value("${db.sga.sp.sgasuconsultalote}")
    public String sgasuConsultaLote;
    
    @Value("${db.oac.owner.apps}")
    public String oacOwApps;
    
    @Value("${db.sga.owner.collections}")
    public String sgaOwItCollections;
    
    @Value("${db.sga.timeoutConexion}")
    public String dbSgaTimeoutConexion;
    @Value("${db.sga.timeoutEjecucion}")
    public String sgaTimeoutEjecucion;
    
    //PKG      
    @Value("${db.sga.pkg.ajustesmasivos}")
    public String sgaPkgAjustesMasivos;
    
    @Value("${db.oac.pkg.hbdooacsgaajustespkg}")
    public String oacHbdoOacSgaAjustesPkg;
    
    //sp
    @Value("${db.sga.sp.sgasuvalidarregmasivo}")
    public String sgaSpSgasuValidarRegMasivo;     
    
    @Value("${db.sga.sp.sgasslistarregenviaroac}")
    public String sgaSpgassListarRegEnviarOac;   
    
    @Value("${db.oac.sp.prvalregistro}")
    public String oacSpPrValRegistro;

    @Value("${db.sga.sp.sgasiajustedocumento}")
    public String sgaSpSgasiAjusteDocumento;

    @Value("${db.oac.sp.prprcinterfaceajuste}")
    public String oacSpPrPrcInterfaceAjuste;

    @Value("${db.sga.sp.sgasuactdocumento}")
    public String sgaSpSgasuActDocumento;

    @Value("${db.sga.sp.sgasslistarregistros}")
    public String sgaSpSgassListarRegistros;
    
    @Value("${db.sga.sp.sgasuvalidaoac}")
    public String sgasuValidaOac;
    
    @Value("${db.sga.sp.sgasslistarajusteoac}")
    public String sgassListarAjusteOac;
    
    @Value("${db.sga.sp.sgasuactdocumento}")
    public String sgasuActDocumento;
    
    @Value("${error.sqltimeoutexception}")
    public String errorSqlTimeoutException;
    
    
    
    @Value("${ws.envioCorreo.wsdl}")
    public String envioCorreoWsdl;
    
    @Value("${ws.enviocorreoWSService.remitente}")
    public String envioCorreoRemitente;
    
    @Value("${ws.enviocorreoWSService.asunto}")
    public String envioCorreoAsunto;
    
    @Value("${ws.enviocorreoWSService.destinatario}")
    public String envioCorreoDestinatario;
    
    @Value("${ws.enviocorreoWSService.htmlflag}")
    public String htmlFlag;
    
    @Value("${codigo.idt1}")
    public String codIdt1;
    @Value("${mensaje.idt1}")
    public String mensajeIdt1;
    
    @Value("${codigo.idt3}")
    public String codIdt3;
    @Value("${mensaje.idt3}")
    public String mensajeIdt3;    
    
    //VALORES
    @Value("${vresponse.getKoCodigo.ok}")
    public String vresponseGetKoCodigoOk ;

    @Value("${valOac.getXvStatus.ok}")
    public String valOacGetXvStatusOk ;

    @Value("${vaOacRq.setkCodigo.ok}")
    public String vaOacRqSetkCodigoOk ;

    @Value("${vaOacRq.setkCodigo.err}")
    public String vaOacRqSetkCodigoErr ;

    @Value("${prInAjRs.getXvStatus.ok}")
    public String prInAjRsGetXvStatusOk ;

    @Value("${acDoRq.setkValor.ok}")
    public String acDoRqSetkValorOk ;

    @Value("${acDoRq.setkValor.err}")
    public String acDoRqSetkValorErr ;
    
    @Value("${listCu.ok}")
    public String listCuOk ;
}
