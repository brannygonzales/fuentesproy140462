package pe.com.claro.shell.sgagenerarajuste.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pe.com.claro.shell.sgagenerarajuste.bean.ListarRegEnviarOacCursor;
import pe.com.claro.shell.sgagenerarajuste.util.Constantes;

public class ListarRegEnviarOacCursorMapper implements RowMapper<Object> {

	@Override
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		ListarRegEnviarOacCursor cursor= new ListarRegEnviarOacCursor();
		cursor.setPdFechaAjuste(rs.getString(Constantes.PD_FECHA_AJUSTE));
		cursor.setPnMontoAjuste(rs.getString(Constantes.PN_MONTO_AJUSTE));
		cursor.setPnSaldoAjuste(rs.getString(Constantes.PN_SALDO_AJUSTE));
		cursor.setPvCodAplicacion(rs.getString(Constantes.PV_COD_APLICACION));
		cursor.setPvCodCuenta(rs.getString(Constantes.PV_COD_CUENTA));
		cursor.setPvDocsAjuste(rs.getString(Constantes.PV_DOCS_AJUSTE));
		cursor.setPvEstado(rs.getString(Constantes.PV_ESTADO));
		cursor.setPvMonedaAjuste(rs.getString(Constantes.PV_MONEDA_AJUSTE));
		cursor.setPvNroDocAjuste(rs.getString(Constantes.PV_NRO_DOC_AJUSTE));
		cursor.setPvTipoAjuste(rs.getString(Constantes.PV_TIPO_AJUSTE));
		cursor.setPvTipoServicio(rs.getString(Constantes.PV_TIPO_SERVICIO));
		cursor.setPvDocsAjusteSGA(rs.getString(Constantes.PV_DOCS_AJUSTE_SGA));
		return cursor;
	}

}
