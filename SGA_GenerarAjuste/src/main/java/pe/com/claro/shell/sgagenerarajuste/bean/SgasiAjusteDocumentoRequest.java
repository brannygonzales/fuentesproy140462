package pe.com.claro.shell.sgagenerarajuste.bean;

import java.io.Serializable;

public class SgasiAjusteDocumentoRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2847488251148017275L;
	private String kRegistro;

	public String getkRegistro() {
		return kRegistro;
	}

	public void setkRegistro(String kRegistro) {
		this.kRegistro = kRegistro;
	}
}
