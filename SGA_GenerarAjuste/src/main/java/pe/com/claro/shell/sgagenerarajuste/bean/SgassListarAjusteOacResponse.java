package pe.com.claro.shell.sgagenerarajuste.bean;

import java.io.Serializable;
import java.util.ArrayList;

public class SgassListarAjusteOacResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2236624827440542167L;
	private ArrayList<SgassListarAjusteOacCursor> koCursor;
	private String koCodigo;
	private String koMensaje;
	public ArrayList<SgassListarAjusteOacCursor> getKoCursor() {
		return koCursor;
	}
	public void setKoCursor(ArrayList<SgassListarAjusteOacCursor> koCursor) {
		this.koCursor = koCursor;
	}
	public String getKoCodigo() {
		return koCodigo;
	}
	public void setKoCodigo(String koCodigo) {
		this.koCodigo = koCodigo;
	}
	public String getKoMensaje() {
		return koMensaje;
	}
	public void setKoMensaje(String koMensaje) {
		this.koMensaje = koMensaje;
	}
	
}
