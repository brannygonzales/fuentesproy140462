package pe.com.claro.shell.sgagenerarajuste.util;

public class Constantes {
	public static final String URL_CONTEXT = "./spring/applicationContext.xml";
	
	public static final String EJECUTANDO_SP = " Ejecutando SP: ";
	public static final String CORCHETE_IZQUIERDO = "[";
	public static final String CORCHETE_DERECHO = "]";
	public static final String TEXTO_VACIO = "";
	public static final String TEXTO_ESPACIO = " ";
	public static final String TEXTO_DOSPUNTOS = " : ";
	public static final String PARAMETRO_INICIO_METODO = "============ {} [INICIO de metodo {}] ============ ";
	public static final String PARAMETRO_FIN_METODO = "============ {} [FIN de metodo {}] ============ ";
	
	public static final String ERROR_JDBC_TIMEOUT = "timeOut";
	
	public static final String CODIGO_IDT1 = "codigo.idt1";
	public static final String MSG_IDT1 = "mensaje.idt1";
	
	public static final String PARAMETROS_OUTPUT = "[PARAMETROS OUTPUT]";
	public static final String PARAMETROS_INPUT = "[PARAMETROS INPUT]";

	public static final String KO_REGISTRO="KO_REGISTRO";
	
	public static final String K_REGISTRO="K_REGISTRO";
	public static final String KO_CODIGO="KO_CODIGO";
	public static final String KO_MENSAJE="KO_MENSAJE";
	public static final String KO_CURSOR="KO_CURSOR";
	public static final String K_VALOR="K_VALOR";
	public static final String KO_CORREO="KO_CORREO";
	public static final String KO_NOMBRE_ARCHIVO="KO_NOMBRE_ARCHIVO";
	
	public static final String K_AMADV_DOCUMENTO="K_AMADV_DOCUMENTO";
	public static final String K_CODIGO="K_CODIGO";
	
	public static final String PV_COD_APLICACION="PV_COD_APLICACION";
	public static final String PV_TIPO_SERVICIO="PV_TIPO_SERVICIO";
	public static final String PV_COD_CUENTA="PV_COD_CUENTA";
	public static final String PV_TIPO_AJUSTE="PV_TIPO_AJUSTE";
	public static final String PV_NRO_DOC_AJUSTE="PV_NRO_DOC_AJUSTE";
	public static final String PV_DOCS_AJUSTE="PV_DOCS_AJUSTE";
	public static final String PV_MONEDA_AJUSTE="PV_MONEDA_AJUSTE";
	public static final String PN_MONTO_AJUSTE="PN_MONTO_AJUSTE";
	public static final String PN_SALDO_AJUSTE="PN_SALDO_AJUSTE";
	public static final String PV_ESTADO="PV_ESTADO";
	public static final String PD_FECHA_AJUSTE="PD_FECHA_AJUSTE";
	public static final String PV_DOCS_AJUSTE_SGA ="PV_DOCS_AJUSTE_SGA";
	public static final String XV_STATUS="XV_STATUS";
	public static final String XV_MESSAGE="XV_MESSAGE";
	
	public static final String PV_TRX_ID_WS="PV_TRX_ID_WS";
	public static final String PV_USUARIO_APLIC="PV_USUARIO_APLIC";
	public static final String PV_TIPO_OPERACION="PV_TIPO_OPERACION";
	public static final String PV_ID_RECLAMO_ORIGEN="PV_ID_RECLAMO_ORIGEN";
	public static final String PV_COD_MOTIVO_AJUSTE="PV_COD_MOTIVO_AJUSTE";
	public static final String PV_DESCRIP_AJUSTE="PV_DESCRIP_AJUSTE";
	public static final String PD_FECHA_VENC_AJUSTE="PD_FECHA_VENC_AJUSTE";
	public static final String PD_FECHA_CANCELACION="PD_FECHA_CANCELACION";
	public static final String PV_NRO_DOC_AJUSTE_SGA="PV_NRO_DOC_AJUSTE_SGA";
	
	public static final String AMACN_ID="AMACN_ID";
	public static final String AMADN_ID="AMADN_ID";
	public static final String AMADC_IDFAC="AMADC_IDFAC";
	public static final String AMADC_IDFAC_GEN="AMADC_IDFAC_GEN";
	public static final String AMADV_TICKET="AMADV_TICKET";
	public static final String AMADC_CLIENTE="AMADC_CLIENTE";
	public static final String AMADV_DOCUMENTO="AMADV_DOCUMENTO";
	public static final String AMADV_SERIE_DOC="AMADV_SERIE_DOC";
	public static final String AMADC_NUM_DOC="AMADC_NUM_DOC";
	public static final String AMADV_MSISDN_AFEC="AMADV_MSISDN_AFEC";
	public static final String AMADV_MSISDN_DEVOL="AMADV_MSISDN_DEVOL";
	public static final String AMADV_IDINSTPROD="AMADV_IDINSTPROD";
	public static final String AMADV_CICLO="AMADV_CICLO";
	public static final String AMADN_MONTO="AMADN_MONTO";
	public static final String AMADN_MONTO_CIGV="AMADN_MONTO_CIGV";
	public static final String AMADN_TASA="AMADN_TASA";
	public static final String AMADN_INTERES="AMADN_INTERES";
	public static final String AMADN_MONTO_TOTAL="AMADN_MONTO_TOTAL";
	public static final String AMADV_GLOSA="AMADV_GLOSA";
	public static final String AMADN_ESTADO="AMADN_ESTADO";
	public static final String AMADN_CODIGO="AMADN_CODIGO";
	public static final String AMADV_MENSAJE="AMADV_MENSAJE";
	public static final String AMADV_USUREG="AMADV_USUREG";
	public static final String AMADD_FECHAREG="AMADD_FECHAREG";
	public static final String AMADV_USUMOD="AMADV_USUMOD";
	public static final String AMADD_FECHAMOD="AMADD_FECHAMOD";
	public static final String AMADN_IDCPTO="AMADN_IDCPTO";
	public static final String AMADN_IDSECCPTO="AMADN_IDSECCPTO";
	public static final String AMADV_DESITM="AMADV_DESITM";		
	
	public static final String DOCREFERENCIA="DOCREFERENCIA";
	public static final String AJUSTE="AJUSTE";
	public static final String DESCRIPCION="DESCRIPCION";
	public static final String CODCLIENTE="CODCLIENTE";
	public static final String MONTO="MONTO";
	
}
