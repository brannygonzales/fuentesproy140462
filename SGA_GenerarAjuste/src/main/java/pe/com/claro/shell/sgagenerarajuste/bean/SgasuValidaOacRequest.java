package pe.com.claro.shell.sgagenerarajuste.bean;

import java.io.Serializable;

public class SgasuValidaOacRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2507459070346854225L;
	private String kRegistro;
	private String kAmadvDocumento ;
	private String kCodigo;
	public String getkRegistro() {
		return kRegistro;
	}
	public void setkRegistro(String kRegistro) {
		this.kRegistro = kRegistro;
	}
	public String getkAmadvDocumento() {
		return kAmadvDocumento;
	}
	public void setkAmadvDocumento(String kAmadvDocumento) {
		this.kAmadvDocumento = kAmadvDocumento;
	}
	public String getkCodigo() {
		return kCodigo;
	}
	public void setkCodigo(String kCodigo) {
		this.kCodigo = kCodigo;
	}
	
}
