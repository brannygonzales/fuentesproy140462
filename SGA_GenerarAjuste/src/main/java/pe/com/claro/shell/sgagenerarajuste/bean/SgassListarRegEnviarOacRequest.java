package pe.com.claro.shell.sgagenerarajuste.bean;

import java.io.Serializable;

public class SgassListarRegEnviarOacRequest implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3997183555050348297L;
	private String kRegistro;

	public String getkRegistro() {
		return kRegistro;
	}

	public void setkRegistro(String kRegistro) {
		this.kRegistro = kRegistro;
	}
}
