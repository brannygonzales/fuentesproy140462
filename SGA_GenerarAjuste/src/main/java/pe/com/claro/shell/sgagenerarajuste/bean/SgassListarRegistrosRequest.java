package pe.com.claro.shell.sgagenerarajuste.bean;

import java.io.Serializable;

public class SgassListarRegistrosRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7595643385944136172L;
	private String kRegistro;

	public String getkRegistro() {
		return kRegistro;
	}

	public void setkRegistro(String kRegistro) {
		this.kRegistro = kRegistro;
	}
	
}
