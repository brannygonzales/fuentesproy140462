package pe.com.claro.shell.sgagenerarajuste.bean;

import java.io.Serializable;

public class SgasuValidarRegMasivoRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2353634178491145442L;
	private String kRegistro;

	public String getkRegistro() {
		return kRegistro;
	}

	public void setkRegistro(String kRegistro) {
		this.kRegistro = kRegistro;
	}

}
