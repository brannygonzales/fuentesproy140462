package pe.com.claro.shell.sgagenerarajuste.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pe.com.claro.shell.sgagenerarajuste.bean.ListarRegistrosCursor;
import pe.com.claro.shell.sgagenerarajuste.util.Constantes;

public class ListarRegistrosCursorMapper implements RowMapper<Object> {

	@Override
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		ListarRegistrosCursor listaRegistrosCursor=new ListarRegistrosCursor();
		listaRegistrosCursor.setAmacnId(rs.getString(Constantes.AMACN_ID));
		listaRegistrosCursor.setAmadnId(rs.getString(Constantes.AMADN_ID));
		listaRegistrosCursor.setAmadcIdfac(rs.getString(Constantes.AMADC_IDFAC));
		listaRegistrosCursor.setAmadcIdfacGen(rs.getString(Constantes.AMADC_IDFAC_GEN));
		listaRegistrosCursor.setAmadvTicket(rs.getString(Constantes.AMADV_TICKET));
		listaRegistrosCursor.setAmadcCliente(rs.getString(Constantes.AMADC_CLIENTE));
		listaRegistrosCursor.setAmadvDocumento(rs.getString(Constantes.AMADV_DOCUMENTO));
		listaRegistrosCursor.setAmadvSerieDoc(rs.getString(Constantes.AMADV_SERIE_DOC));
		listaRegistrosCursor.setAmadcNumDoc(rs.getString(Constantes.AMADC_NUM_DOC));
		listaRegistrosCursor.setAmadvMsisdnAfec(rs.getString(Constantes.AMADV_MSISDN_AFEC));
		listaRegistrosCursor.setAmadvMsisdnDevol(rs.getString(Constantes.AMADV_MSISDN_DEVOL));
		listaRegistrosCursor.setAmadvIdinstprod(rs.getString(Constantes.AMADV_IDINSTPROD));
		listaRegistrosCursor.setAmadvCiclo(rs.getString(Constantes.AMADV_CICLO));
		listaRegistrosCursor.setAmadnMonto(rs.getString(Constantes.AMADN_MONTO));
		listaRegistrosCursor.setAmadnMontoCigv(rs.getString(Constantes.AMADN_MONTO_CIGV));
		listaRegistrosCursor.setAmadnTasa(rs.getString(Constantes.AMADN_TASA));
		listaRegistrosCursor.setAmadnInteres(rs.getString(Constantes.AMADN_INTERES));
		listaRegistrosCursor.setAmadnMontoTotal(rs.getString(Constantes.AMADN_MONTO_TOTAL));
		listaRegistrosCursor.setAmadvGlosa(rs.getString(Constantes.AMADV_GLOSA));
		listaRegistrosCursor.setAmadnEstado(rs.getString(Constantes.AMADN_ESTADO));
		listaRegistrosCursor.setAmadnCodigo(rs.getString(Constantes.AMADN_CODIGO));
		listaRegistrosCursor.setAmadvMensaje(rs.getString(Constantes.AMADV_MENSAJE));
		listaRegistrosCursor.setAmadvUsureg(rs.getString(Constantes.AMADV_USUREG));
		listaRegistrosCursor.setAmaddFechareg(rs.getString(Constantes.AMADD_FECHAREG));
		listaRegistrosCursor.setAmadvUsumod(rs.getString(Constantes.AMADV_USUMOD));
		listaRegistrosCursor.setAmaddFechamod(rs.getString(Constantes.AMADD_FECHAMOD));
		listaRegistrosCursor.setAmadnIdcpto(rs.getString(Constantes.AMADN_IDCPTO));
		listaRegistrosCursor.setAmadnIdseccpto(rs.getString(Constantes.AMADN_IDSECCPTO));
		listaRegistrosCursor.setAmadvDesitm(rs.getString(Constantes.AMADV_DESITM));
		return listaRegistrosCursor;
	}

}
