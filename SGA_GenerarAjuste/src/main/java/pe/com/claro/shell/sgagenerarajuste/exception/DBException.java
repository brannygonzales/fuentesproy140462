package pe.com.claro.shell.sgagenerarajuste.exception;

public class DBException extends BaseException {

    private static final long serialVersionUID = 1L;

    /**
     * Excepcion.
     * 
     * @param message
     *            mensaje de error, tipo String.
     */
    public DBException(String string) {
        super(string);
    }

    /**
     * Excepcion.
     * 
     * @param exception
     *            excepcion generada, tipo Exception.
     */
    public DBException(Exception exception) {
        super(exception);
    }

    /**
     * Excepcion.
     * 
     * @param message
     *            mensaje de error, tipo String.
     * @param exception
     *            excepcion generada, tipo Exception.
     */
    public DBException(String string, Exception exception) {
        super(string, exception);
    }

    /**
     * Excepcion.
     * 
     * @param code
     *            codigo de error, tipo String.
     * @param message
     *            mensaje de error, tipo String.
     * @param exception
     *            excepcion generada, tipo Exception.
     */
    public DBException(String code, String message, Exception exception) {
        super(code, message, exception);
    }

    /**
     * Excepcion.
     * 
     * @param code
     *            codigo de error, tipo String.
     * @param message
     *            mensaje de error, tipo String.
     */
    public DBException(String code, String message) {
        super(code, message);
    }

    /**
     * Excepcion para consumo de procedimientos.
     * 
     * @param code
     *            codigo de error, tipo String.
     * @param message
     *            mensaje de error, tipo String.
     * @param nombreBD
     *            nombre de Base de Datos, tipo String.
     * @param nombreSP
     *            nombre de Store Procedure, tipo String.
     * @param exception
     *            excepcion generada, tipo Exception.
     */
    public DBException(String code, String message, String nombreBD, String nombreSP, Exception exception) {
        super(code, message, nombreBD, nombreSP, exception);
    }
}
