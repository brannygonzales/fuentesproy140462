package pe.com.claro.shell.sgagenerarajuste.bean;

import java.io.Serializable;

public class PrPrcInterfaceAjusteResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -631321264478074276L;
	private String xvStatus;
	private String xvMessage;
	public String getXvStatus() {
		return xvStatus;
	}
	public void setXvStatus(String xvStatus) {
		this.xvStatus = xvStatus;
	}
	public String getXvMessage() {
		return xvMessage;
	}
	public void setXvMessage(String xvMessage) {
		this.xvMessage = xvMessage;
	}
	
	
}
