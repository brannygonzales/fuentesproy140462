package pe.com.claro.shell.sgagenerarajuste;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import pe.com.claro.shell.sgagenerarajuste.service.GenerarAjuste;
import pe.com.claro.shell.sgagenerarajuste.util.Constantes;
import pe.com.claro.shell.sgagenerarajuste.util.PropertiesExternos;

@Component
public class SGA_GenerarAjusteMain {
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());
	private static ApplicationContext objContextoSpring;
	@Autowired
	private PropertiesExternos propertiesExternos;
	@Autowired
	private GenerarAjuste generarAjuste;
	
	public static void main(String[] args) {
		long tiempoInicial = System.currentTimeMillis();
		objContextoSpring = new ClassPathXmlApplicationContext(Constantes.URL_CONTEXT);
		SGA_GenerarAjusteMain main = objContextoSpring.getBean(SGA_GenerarAjusteMain.class);
		main.loadLog4J();

		String msjTxIn = "[IDTRANSACION: "+args[0]+"]";

		main.inicio(msjTxIn,args[0], tiempoInicial);

	}

	public void inicio(String msjTxIn,String idTran, long tiempoInicial) {
		try {			
			generarAjuste.generandoAjuste(msjTxIn,idTran);
		} catch (Exception e) {
			LOGGER.error("ERROR: al procesar la shell");
		} finally {
			try {
				LOGGER.info(msjTxIn + "---------------------------- Actividad FINAL ----------------------------");
				LOGGER.info(msjTxIn + "---11. FIN--- Tiempo total de proceso(ms): "
						+ (System.currentTimeMillis() - tiempoInicial) + " milisegundos.");
				((ConfigurableApplicationContext) objContextoSpring).close();
			} catch (Exception e) {
				LOGGER.error(msjTxIn + "[Ocurrio un error al cerrar el contexto]" + e.getMessage(), e);
			}
		}
	}

	public void loadLog4J() {
		PropertyConfigurator.configure(propertiesExternos.vLog4JDir);
	}
}
