package pe.com.claro.shell.sgagenerarajuste.dao;

import java.util.Map;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.com.claro.shell.sgagenerarajuste.bean.PrPrcInterfaceAjusteRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.PrPrcInterfaceAjusteResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.PrValRegistroRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.PrValRegistroResponse;
import pe.com.claro.shell.sgagenerarajuste.exception.DBException;
import pe.com.claro.shell.sgagenerarajuste.util.Constantes;
import pe.com.claro.shell.sgagenerarajuste.util.PropertiesExternos;

@Repository
public class OacDAOImpl implements OacDAO {
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());
	@Autowired
	private PropertiesExternos propertiesExternos;
	
	@Autowired
	@Qualifier("Oac")
	private DataSource Oac;
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public PrValRegistroResponse validarAjusteOac(String mensajeTransaccion, PrValRegistroRequest request,
			PrValRegistroResponse response) throws DBException {
		String nombreMetodo="validarAjusteOac";
		JdbcTemplate objJdbcTemplate = null;
		String bd=null;
		String owner = null;
		String procedure = null;
		String pkg= null;
		String execute_package= null;
		try {
			bd= propertiesExternos.dbOac;
			owner=propertiesExternos.oacOwApps;
			pkg=propertiesExternos.oacHbdoOacSgaAjustesPkg;
			procedure= propertiesExternos.oacSpPrValRegistro;
			execute_package=owner+"."+pkg+"."+procedure;
			LOGGER.info(mensajeTransaccion + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,nombreMetodo);

			 SimpleJdbcCall objJdbcCall = new SimpleJdbcCall(Oac);
			 objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			 objJdbcTemplate.setQueryTimeout(Integer.parseInt(propertiesExternos.dbSgaTimeoutConexion));
			 
			 SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess().withoutProcedureColumnMetaDataAccess()
						.withSchemaName(owner)
						.withCatalogName(pkg)
						.withProcedureName(procedure)
						.declareParameters(  
								new SqlParameter(Constantes.PV_COD_APLICACION,OracleTypes.VARCHAR),
								new SqlParameter(Constantes.PV_TIPO_SERVICIO,OracleTypes.VARCHAR),
								new SqlParameter(Constantes.PV_COD_CUENTA,OracleTypes.VARCHAR),
								new SqlParameter(Constantes.PV_TIPO_AJUSTE,OracleTypes.VARCHAR),
								new SqlParameter(Constantes.PV_NRO_DOC_AJUSTE,OracleTypes.VARCHAR),
								new SqlParameter(Constantes.PV_DOCS_AJUSTE,OracleTypes.VARCHAR),
								new SqlParameter(Constantes.PV_MONEDA_AJUSTE,OracleTypes.VARCHAR),
								new SqlParameter(Constantes.PN_MONTO_AJUSTE,OracleTypes.DOUBLE),
								new SqlParameter(Constantes.PN_SALDO_AJUSTE,OracleTypes.DOUBLE),
								new SqlParameter(Constantes.PV_ESTADO,OracleTypes.VARCHAR),
								new SqlParameter(Constantes.PD_FECHA_AJUSTE,OracleTypes.DATE),							
								new SqlOutParameter(Constantes.XV_STATUS, OracleTypes.VARCHAR),
								new SqlOutParameter(Constantes.XV_MESSAGE, OracleTypes.VARCHAR)
								);
			 LOGGER.info(mensajeTransaccion + Constantes.EJECUTANDO_SP + Constantes.CORCHETE_IZQUIERDO + execute_package + Constantes.CORCHETE_DERECHO);
			 LOGGER.info(mensajeTransaccion +  Constantes.PARAMETROS_INPUT);
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PV_COD_APLICACION + Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS+request.getPvCodAplicacion());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PV_TIPO_SERVICIO + Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS+request.getPvTipoServicio());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PV_COD_CUENTA + Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS+request.getPvCodCuenta());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PV_TIPO_AJUSTE + Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS+request.getPvTipoAjuste());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PV_NRO_DOC_AJUSTE + Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS+request.getPvNroDocAjuste());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PV_DOCS_AJUSTE+ Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS+request.getPvDocsAjuste());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PV_MONEDA_AJUSTE + Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS+request.getPvMonedaAjuste());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PN_MONTO_AJUSTE + Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS+request.getPnMontoAjuste());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PN_SALDO_AJUSTE + Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS+request.getPnSaldoAjuste());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PV_ESTADO + Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS+request.getPvEstado());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PD_FECHA_AJUSTE + Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS+request.getPdFechaAjuste());
 
			 
			 SqlParameterSource objParametrosIN = new MapSqlParameterSource()
					 .addValue(Constantes.PV_COD_APLICACION,request.getPvCodAplicacion(),OracleTypes.VARCHAR)
					 .addValue(Constantes.PV_TIPO_SERVICIO,request.getPvTipoServicio(),  OracleTypes.VARCHAR)
					 .addValue(Constantes.PV_COD_CUENTA,request.getPvCodCuenta(),        OracleTypes.VARCHAR)
					 .addValue(Constantes.PV_TIPO_AJUSTE,request.getPvTipoAjuste(),      OracleTypes.VARCHAR)
					 .addValue(Constantes.PV_NRO_DOC_AJUSTE,request.getPvNroDocAjuste(), OracleTypes.VARCHAR)
					 .addValue(Constantes.PV_DOCS_AJUSTE,request.getPvDocsAjuste(),      OracleTypes.VARCHAR)
					 .addValue(Constantes.PV_MONEDA_AJUSTE,request.getPvMonedaAjuste(),  OracleTypes.VARCHAR)
					 .addValue(Constantes.PN_MONTO_AJUSTE,request.getPnMontoAjuste(),    OracleTypes.DOUBLE)
					 .addValue(Constantes.PN_SALDO_AJUSTE,request.getPnSaldoAjuste(),    OracleTypes.DOUBLE)
					 .addValue(Constantes.PV_ESTADO,request.getPvEstado(),         		OracleTypes.VARCHAR)
					 .addValue(Constantes.PD_FECHA_AJUSTE,request.getPdFechaAjuste(), 	OracleTypes.DATE);
			 Map<String, Object> results = (Map<String, Object>)procedureConsulta.execute(objParametrosIN);
			
			 response.setXvStatus(String.valueOf(results.get(Constantes.XV_STATUS)));
			 response.setXvMessage(String.valueOf(results.get(Constantes.XV_MESSAGE)));
			 
			 LOGGER.info(mensajeTransaccion +  Constantes.PARAMETROS_OUTPUT);	
			 LOGGER.info(mensajeTransaccion+Constantes.TEXTO_ESPACIO +Constantes.CORCHETE_IZQUIERDO+ Constantes.XV_STATUS+Constantes.CORCHETE_DERECHO+Constantes.TEXTO_DOSPUNTOS + response.getXvStatus());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_ESPACIO +Constantes.CORCHETE_IZQUIERDO+ Constantes.XV_MESSAGE+Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + response.getXvMessage());
		}catch (Exception e) {
			String errorMsg = e + Constantes.TEXTO_VACIO;
			String msjError = null; 
		   	 String codError = null; 
			if(errorMsg.contains(Constantes.ERROR_JDBC_TIMEOUT)){
		   		 codError = String.valueOf(propertiesExternos.codIdt1);
		   		 msjError = String.valueOf(propertiesExternos.mensajeIdt1).replace("$bd", bd).replace("$sp", procedure)+" " +e.getMessage();
		   		throw new DBException(codError,msjError,e);  
		        }
			else {
				LOGGER.error(mensajeTransaccion + "ERROR: [Exception] - [" + e.getMessage() + "]", e);
				throw new DBException(response.getXvStatus(),response.getXvMessage(),e);  
			}
			
		}finally {
			LOGGER.info(mensajeTransaccion + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,nombreMetodo);
		}
		return response;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public PrPrcInterfaceAjusteResponse registrarAjusteOac(String mensajeTransaccion,
			PrPrcInterfaceAjusteRequest request, PrPrcInterfaceAjusteResponse response) throws DBException {
		String nombreMetodo="registrarAjusteOac";
		JdbcTemplate objJdbcTemplate = null;
		String bd=null;
		String owner = null;
		String procedure = null;
		String pkg= null;
		String execute_package= null;
		try {
			bd= propertiesExternos.dbOac;
			owner=propertiesExternos.oacOwApps;
			pkg=propertiesExternos.oacHbdoOacSgaAjustesPkg;
			procedure= propertiesExternos.oacSpPrPrcInterfaceAjuste;
			execute_package=owner+"."+pkg+"."+procedure;
			LOGGER.info(mensajeTransaccion + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,nombreMetodo);
			
			 SimpleJdbcCall objJdbcCall = new SimpleJdbcCall(Oac);
			 objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			 objJdbcTemplate.setQueryTimeout(Integer.parseInt(propertiesExternos.dbSgaTimeoutConexion));
			 
			 SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess().withoutProcedureColumnMetaDataAccess()
						.withSchemaName(owner)
						.withCatalogName(pkg)
						.withProcedureName(procedure)
						.declareParameters(
								new SqlParameter(Constantes.PV_TRX_ID_WS,OracleTypes.VARCHAR),
								new SqlParameter(Constantes.PV_COD_APLICACION,OracleTypes.VARCHAR),
								new SqlParameter(Constantes.PV_USUARIO_APLIC,OracleTypes.VARCHAR),
								new SqlParameter(Constantes.PV_TIPO_SERVICIO,OracleTypes.VARCHAR),
								new SqlParameter(Constantes.PV_COD_CUENTA,OracleTypes.VARCHAR),
								new SqlParameter(Constantes.PV_TIPO_OPERACION,OracleTypes.VARCHAR),
								new SqlParameter(Constantes.PV_TIPO_AJUSTE,OracleTypes.VARCHAR),
								new SqlParameter(Constantes.PV_NRO_DOC_AJUSTE,OracleTypes.VARCHAR),
								new SqlParameter(Constantes.PV_DOCS_AJUSTE,OracleTypes.VARCHAR),
								new SqlParameter(Constantes.PV_ID_RECLAMO_ORIGEN,OracleTypes.VARCHAR),
								new SqlParameter(Constantes.PV_MONEDA_AJUSTE,OracleTypes.VARCHAR),
								new SqlParameter(Constantes.PN_MONTO_AJUSTE,OracleTypes.DOUBLE),
								new SqlParameter(Constantes.PN_SALDO_AJUSTE,OracleTypes.DOUBLE),
								new SqlParameter(Constantes.PV_ESTADO,OracleTypes.VARCHAR),
								new SqlParameter(Constantes.PV_COD_MOTIVO_AJUSTE,OracleTypes.VARCHAR),
								new SqlParameter(Constantes.PV_DESCRIP_AJUSTE,OracleTypes.VARCHAR),
								new SqlParameter(Constantes.PD_FECHA_AJUSTE,OracleTypes.DATE),
								new SqlParameter(Constantes.PD_FECHA_VENC_AJUSTE,OracleTypes.DATE),
								new SqlParameter(Constantes.PD_FECHA_CANCELACION,OracleTypes.DATE),
								new SqlOutParameter(Constantes.XV_STATUS, OracleTypes.VARCHAR),
								new SqlOutParameter(Constantes.XV_MESSAGE, OracleTypes.VARCHAR)
								);
			 LOGGER.info(mensajeTransaccion + Constantes.EJECUTANDO_SP + Constantes.CORCHETE_IZQUIERDO + execute_package + Constantes.CORCHETE_DERECHO);
			 LOGGER.info(mensajeTransaccion +  Constantes.PARAMETROS_INPUT);
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PV_TRX_ID_WS +Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + request.getPvTrxIdWs());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PV_COD_APLICACION +Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + request.getPvCodAplicacion());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PV_USUARIO_APLIC +Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + request.getPvUsuarioAplic());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PV_TIPO_SERVICIO +Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + request.getPvTipoServicio());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PV_COD_CUENTA +Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + request.getPvCodCuenta());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PV_TIPO_OPERACION +Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + request.getPvTipoOperacion());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PV_TIPO_AJUSTE +Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + request.getPvTipoAjuste());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PV_NRO_DOC_AJUSTE +Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + request.getPvNroDocAjuste());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PV_DOCS_AJUSTE +Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + request.getPvDocsAjuste());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PV_ID_RECLAMO_ORIGEN +Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + request.getPvIdReclamoOrigen());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PV_MONEDA_AJUSTE +Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + request.getPvMonedaAjuste());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PN_MONTO_AJUSTE +Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + request.getPnMontoAjuste());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PN_SALDO_AJUSTE +Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + request.getPnSaldoAjuste());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PV_ESTADO +Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + request.getPvEstado());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PV_COD_MOTIVO_AJUSTE +Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + request.getPvCodMotivoAjuste());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PV_DESCRIP_AJUSTE +Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + request.getPvDescripAjuste());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PD_FECHA_AJUSTE +Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + request.getPdFechaAjuste());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PD_FECHA_VENC_AJUSTE +Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + request.getPdFechaVencAjuste());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.PD_FECHA_CANCELACION +Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + request.getPdFechaCancelacion());
			 
			 SqlParameterSource objParametrosIN = new MapSqlParameterSource()
						.addValue(Constantes.PV_TRX_ID_WS,request.getPvTrxIdWs(),OracleTypes.VARCHAR)
						.addValue(Constantes.PV_COD_APLICACION,request.getPvCodAplicacion(),OracleTypes.VARCHAR)
						.addValue(Constantes.PV_USUARIO_APLIC,request.getPvUsuarioAplic(),OracleTypes.VARCHAR)
						.addValue(Constantes.PV_TIPO_SERVICIO,request.getPvTipoServicio(),OracleTypes.VARCHAR)
						.addValue(Constantes.PV_COD_CUENTA,request.getPvCodCuenta(),OracleTypes.VARCHAR)
						.addValue(Constantes.PV_TIPO_OPERACION,request.getPvTipoOperacion(),OracleTypes.VARCHAR)
						.addValue(Constantes.PV_TIPO_AJUSTE,request.getPvTipoAjuste(),OracleTypes.VARCHAR)
						.addValue(Constantes.PV_NRO_DOC_AJUSTE,request.getPvNroDocAjuste(),OracleTypes.VARCHAR)
						.addValue(Constantes.PV_DOCS_AJUSTE,request.getPvDocsAjuste(),OracleTypes.VARCHAR)
						.addValue(Constantes.PV_ID_RECLAMO_ORIGEN,request.getPvIdReclamoOrigen(),OracleTypes.VARCHAR)
						.addValue(Constantes.PV_MONEDA_AJUSTE,request.getPvMonedaAjuste(),OracleTypes.VARCHAR)
						.addValue(Constantes.PN_MONTO_AJUSTE,request.getPnMontoAjuste(),OracleTypes.DOUBLE)
						.addValue(Constantes.PN_SALDO_AJUSTE,request.getPnSaldoAjuste(),OracleTypes.DOUBLE)
						.addValue(Constantes.PV_ESTADO,request.getPvEstado(),OracleTypes.VARCHAR)
						.addValue(Constantes.PV_COD_MOTIVO_AJUSTE,request.getPvCodMotivoAjuste(),OracleTypes.VARCHAR)
						.addValue(Constantes.PV_DESCRIP_AJUSTE,request.getPvDescripAjuste(),OracleTypes.VARCHAR)
						.addValue(Constantes.PD_FECHA_AJUSTE,request.getPdFechaAjuste(),OracleTypes.DATE)
						.addValue(Constantes.PD_FECHA_VENC_AJUSTE,request.getPdFechaVencAjuste(),OracleTypes.DATE)
						.addValue(Constantes.PD_FECHA_CANCELACION,request.getPdFechaCancelacion(),OracleTypes.DATE);
			 
			 Map<String, Object> results = (Map<String, Object>)procedureConsulta.execute(objParametrosIN);
		
			 response.setXvStatus(String.valueOf(results.get(Constantes.XV_STATUS)));
			 response.setXvMessage(String.valueOf(results.get(Constantes.XV_MESSAGE)));
			 
			 LOGGER.info(mensajeTransaccion +  Constantes.PARAMETROS_OUTPUT);	
			 LOGGER.info(mensajeTransaccion+Constantes.TEXTO_ESPACIO +Constantes.CORCHETE_IZQUIERDO+ Constantes.XV_STATUS+Constantes.CORCHETE_DERECHO+Constantes.TEXTO_DOSPUNTOS + response.getXvStatus());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_ESPACIO +Constantes.CORCHETE_IZQUIERDO+ Constantes.XV_MESSAGE+Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + response.getXvMessage());

		}catch (Exception e) {
			String errorMsg = e + Constantes.TEXTO_VACIO;
			String msjError = null; 
		   	 String codError = null; 
			if(errorMsg.contains(Constantes.ERROR_JDBC_TIMEOUT)){
		   		 codError = String.valueOf(propertiesExternos.codIdt1);
		   		 msjError = String.valueOf(propertiesExternos.mensajeIdt1).replace("$bd", bd).replace("$sp", procedure)+" " +e.getMessage();
		   		throw new DBException(codError,msjError,e);  
		        }
			else {
				LOGGER.error(mensajeTransaccion + "ERROR: [Exception] - [" + e.getMessage() + "]", e);
				throw new DBException(response.getXvStatus(),response.getXvMessage(),e); 
			}
			
		}finally {
			LOGGER.info(mensajeTransaccion + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,nombreMetodo);
		}
		return response;
	}

}
