package pe.com.claro.shell.sgagenerarajuste.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pe.com.claro.shell.sgagenerarajuste.bean.SgassListarAjusteOacCursor;
import pe.com.claro.shell.sgagenerarajuste.util.Constantes;

public class SgassListarAjusteOacCursorMapper  implements RowMapper<Object>{

	@Override
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		SgassListarAjusteOacCursor cursor= new SgassListarAjusteOacCursor();
		cursor.setPdFechaAjuste(rs.getString(Constantes.PD_FECHA_AJUSTE));
		cursor.setPdFechaCancelacion(rs.getString(Constantes.PD_FECHA_CANCELACION));
		cursor.setPdFechaVencAjuste(rs.getString(Constantes.PD_FECHA_VENC_AJUSTE));
		cursor.setPnMontoAjuste(rs.getString(Constantes.PN_MONTO_AJUSTE));
		cursor.setPnSaldoAjuste(rs.getString(Constantes.PN_SALDO_AJUSTE));
		cursor.setPvCodAplicacion(rs.getString(Constantes.PV_COD_APLICACION));
		cursor.setPvCodCuenta(rs.getString(Constantes.PV_COD_CUENTA));
		cursor.setPvCodMotivoAjuste(rs.getString(Constantes.PV_COD_MOTIVO_AJUSTE));
		cursor.setPvDescripAjuste(rs.getString(Constantes.PV_DESCRIP_AJUSTE));
		cursor.setPvDocsAjuste(rs.getString(Constantes.PV_DOCS_AJUSTE));
		cursor.setPvEstado(rs.getString(Constantes.PV_ESTADO));
		cursor.setPvIdReclamoOrigen(rs.getString(Constantes.PV_ID_RECLAMO_ORIGEN));
		cursor.setPvMonedaAjuste(rs.getString(Constantes.PV_MONEDA_AJUSTE));
		cursor.setPvNroDocAjuste(rs.getString(Constantes.PV_NRO_DOC_AJUSTE));
		cursor.setPvTipoAjuste(rs.getString(Constantes.PV_TIPO_AJUSTE));
		cursor.setPvTipoOperacion(rs.getString(Constantes.PV_TIPO_OPERACION));
		cursor.setPvTipoServicio(rs.getString(Constantes.PV_TIPO_SERVICIO));
		cursor.setPvTrxIdWs(rs.getString(Constantes.PV_TRX_ID_WS));
		cursor.setPvUsuarioAplic(rs.getString(Constantes.PV_USUARIO_APLIC));
		cursor.setPvNroDocAjusteSGA(rs.getString(Constantes.PV_NRO_DOC_AJUSTE_SGA));
		return cursor;
	}

}



























