package pe.com.claro.shell.sgagenerarajuste.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pe.com.claro.shell.sgagenerarajuste.bean.ListaCursor;
import pe.com.claro.shell.sgagenerarajuste.util.Constantes;

public class ListaCursorMapper implements RowMapper<Object>{

	@Override
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		ListaCursor cursor=new ListaCursor();
		cursor.setAjuste(rs.getString(Constantes.AJUSTE));
		cursor.setDescripcion(rs.getString(Constantes.DESCRIPCION));
		cursor.setDocreferencia(rs.getString(Constantes.DOCREFERENCIA));
		cursor.setCodCliente(rs.getString(Constantes.CODCLIENTE));
		cursor.setMonto(rs.getString(Constantes.MONTO));
		return cursor;
	}

}
