package pe.com.claro.shell.sgagenerarajuste.dao;

import pe.com.claro.shell.sgagenerarajuste.bean.PrPrcInterfaceAjusteRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.PrPrcInterfaceAjusteResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.PrValRegistroRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.PrValRegistroResponse;
import pe.com.claro.shell.sgagenerarajuste.exception.DBException;

public interface OacDAO {
	public PrValRegistroResponse validarAjusteOac(String mensajeTransaccion,PrValRegistroRequest request,PrValRegistroResponse response)throws DBException;
	public PrPrcInterfaceAjusteResponse registrarAjusteOac(String mensajeTransaccion,PrPrcInterfaceAjusteRequest request,PrPrcInterfaceAjusteResponse response)throws DBException;
}
