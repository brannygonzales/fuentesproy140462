package pe.com.claro.shell.sgagenerarajuste.bean;

import java.io.Serializable;

public class SgasiAjusteDocumentoResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8320808042956922131L;
	private String koCodigo;
	private String koMensaje;
	public String getKoCodigo() {
		return koCodigo;
	}
	public void setKoCodigo(String koCodigo) {
		this.koCodigo = koCodigo;
	}
	public String getKoMensaje() {
		return koMensaje;
	}
	public void setKoMensaje(String koMensaje) {
		this.koMensaje = koMensaje;
	}
	
}
