package pe.com.claro.shell.sgagenerarajuste.dao;

import java.util.ArrayList;
import java.util.Map;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.com.claro.shell.sgagenerarajuste.bean.ListaCursor;
import pe.com.claro.shell.sgagenerarajuste.bean.ListarRegEnviarOacCursor;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasiAjusteDocumentoRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasiAjusteDocumentoResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgassListarAjusteOacCursor;
import pe.com.claro.shell.sgagenerarajuste.bean.SgassListarAjusteOacRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgassListarAjusteOacResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgassListarRegEnviarOacRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgassListarRegEnviarOacResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgassListarRegistrosRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgassListarRegistrosResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuActDocumentoRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuActDocumentoResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuConsultaLoteResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuValidaOacRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuValidaOacResponse;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuValidarRegMasivoRequest;
import pe.com.claro.shell.sgagenerarajuste.bean.SgasuValidarRegMasivoResponse;
import pe.com.claro.shell.sgagenerarajuste.exception.DBException;
import pe.com.claro.shell.sgagenerarajuste.mapper.ListaCursorMapper;
import pe.com.claro.shell.sgagenerarajuste.mapper.ListarRegEnviarOacCursorMapper;
import pe.com.claro.shell.sgagenerarajuste.mapper.SgassListarAjusteOacCursorMapper;
import pe.com.claro.shell.sgagenerarajuste.util.Constantes;
import pe.com.claro.shell.sgagenerarajuste.util.PropertiesExternos;

@Repository
public class SgaDAOImpl implements SgaDAO {
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());
	@Autowired
	private PropertiesExternos propertiesExternos;
	
	@Autowired
	@Qualifier("Sga")
	private DataSource Sga;
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public SgasuConsultaLoteResponse consultaLote(String mensajeTransaccion, SgasuConsultaLoteResponse response)
			throws DBException {
		String nombreMetodo="consultaLote";
		JdbcTemplate objJdbcTemplate = null;
		String bd=null;
		String owner = null;
		String procedure = null;
		String pkg= null;
		String execute_package= null;
		try {
			bd= propertiesExternos.dbSga;
			owner=propertiesExternos.sgaOwItCollections;
			pkg=propertiesExternos.sgaPkgAjustesMasivos;
			procedure= propertiesExternos.sgasuConsultaLote;
			execute_package=owner+"."+pkg+"."+procedure;
			LOGGER.info(mensajeTransaccion + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,nombreMetodo);
			 SimpleJdbcCall objJdbcCall = new SimpleJdbcCall(Sga);
			 objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			 objJdbcTemplate.setQueryTimeout(Integer.parseInt(propertiesExternos.dbSgaTimeoutConexion));
			 
			 SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess().withoutProcedureColumnMetaDataAccess()
						.withSchemaName(owner)
						.withCatalogName(pkg)
						.withProcedureName(procedure)
						.declareParameters(
								new SqlOutParameter(Constantes.KO_REGISTRO, OracleTypes.NUMBER),
								new SqlOutParameter(Constantes.KO_CODIGO, OracleTypes.INTEGER),
								new SqlOutParameter(Constantes.KO_MENSAJE, OracleTypes.VARCHAR)
								);
			 LOGGER.info(mensajeTransaccion + Constantes.EJECUTANDO_SP + Constantes.CORCHETE_IZQUIERDO + execute_package + Constantes.CORCHETE_DERECHO);	
			 
			 Map<String, Object> results = (Map<String, Object>)procedureConsulta.execute();

			 response.setKoRegistro(String.valueOf(results.get(Constantes.KO_REGISTRO)));
			 response.setKoCodigo(String.valueOf(results.get(Constantes.KO_CODIGO)));
			 response.setKoMensaje(String.valueOf(results.get(Constantes.KO_MENSAJE)));
			 LOGGER.info(mensajeTransaccion + Constantes.PARAMETROS_OUTPUT);

			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_ESPACIO +Constantes.CORCHETE_IZQUIERDO+  Constantes.KO_REGISTRO  +Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS+response.getKoRegistro());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_ESPACIO +Constantes.CORCHETE_IZQUIERDO+  Constantes.KO_CODIGO + Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS+response.getKoCodigo());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_ESPACIO +Constantes.CORCHETE_IZQUIERDO+  Constantes.KO_MENSAJE  + Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS+response.getKoMensaje());
			 
		}catch (Exception e) {
			String errorMsg = e + Constantes.TEXTO_VACIO;
			String msjError = null; 
		   	 String codError = null; 
			if(errorMsg.contains(Constantes.ERROR_JDBC_TIMEOUT)){
		   		 codError = String.valueOf(propertiesExternos.codIdt1);
		   		 msjError = String.valueOf(propertiesExternos.mensajeIdt1).replace("$bd", bd).replace("$sp", procedure)+" " +e.getMessage();
		   		throw new DBException(codError,msjError,e);  
		        }
			else {
				LOGGER.error(mensajeTransaccion + "ERROR: [Exception] - [" + e.getMessage() + "]", e);
				throw new DBException(response.getKoCodigo(),response.getKoMensaje(),e);  
			}
			
		}finally {
			LOGGER.info(mensajeTransaccion + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,nombreMetodo);
		}
		return response;
	}
	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public SgasuValidarRegMasivoResponse validarRegistrosSga(String mensajeTransaccion,
			SgasuValidarRegMasivoRequest request, SgasuValidarRegMasivoResponse response) throws DBException {
		String nombreMetodo="validarRegistrosSga";
		JdbcTemplate objJdbcTemplate = null;
		String bd=null;
		String owner = null;
		String procedure = null;
		String pkg= null;
		String execute_package= null;
		try {
			bd= propertiesExternos.dbSga;
			owner=propertiesExternos.sgaOwItCollections;
			pkg=propertiesExternos.sgaPkgAjustesMasivos;
			procedure= propertiesExternos.sgaSpSgasuValidarRegMasivo;
			execute_package=owner+"."+pkg+"."+procedure;
			LOGGER.info(mensajeTransaccion + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,nombreMetodo);
			LOGGER.info(mensajeTransaccion + Constantes.EJECUTANDO_SP + Constantes.CORCHETE_IZQUIERDO + execute_package + Constantes.CORCHETE_DERECHO);
			 SimpleJdbcCall objJdbcCall = new SimpleJdbcCall(Sga);
			 objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			 objJdbcTemplate.setQueryTimeout(Integer.parseInt(propertiesExternos.dbSgaTimeoutConexion));
			 
			 SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess().withoutProcedureColumnMetaDataAccess()
						.withSchemaName(owner)
						.withCatalogName(pkg)
						.withProcedureName(procedure)
						.declareParameters(
								new SqlParameter( Constantes.K_REGISTRO, OracleTypes.NUMBER ),
								new SqlOutParameter(Constantes.KO_CODIGO, OracleTypes.INTEGER),
								new SqlOutParameter(Constantes.KO_MENSAJE, OracleTypes.VARCHAR)
								);
			 LOGGER.info(mensajeTransaccion + Constantes.PARAMETROS_INPUT);
			 LOGGER.info(mensajeTransaccion + Constantes.TEXTO_ESPACIO +Constantes.CORCHETE_IZQUIERDO+Constantes.K_REGISTRO +Constantes.CORCHETE_DERECHO+Constantes.TEXTO_DOSPUNTOS+ request.getkRegistro());			 
			 
			 SqlParameterSource objParametrosIN = new MapSqlParameterSource()
					 .addValue(Constantes.K_REGISTRO, request.getkRegistro(),OracleTypes.NUMBER);	
			 
			 Map<String, Object> results = (Map<String, Object>)procedureConsulta.execute(objParametrosIN);
			 
			 response.setKoCodigo(String.valueOf(results.get(Constantes.KO_CODIGO)));
			 response.setKoMensaje(String.valueOf(results.get(Constantes.KO_MENSAJE)));
			 LOGGER.info(mensajeTransaccion + Constantes.PARAMETROS_OUTPUT);
			 LOGGER.info(mensajeTransaccion+Constantes.TEXTO_ESPACIO +Constantes.CORCHETE_IZQUIERDO+ Constantes.KO_CODIGO+Constantes.CORCHETE_DERECHO+Constantes.TEXTO_DOSPUNTOS + response.getKoCodigo());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_ESPACIO +Constantes.CORCHETE_IZQUIERDO+ Constantes.KO_MENSAJE+Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + response.getKoMensaje());

		}catch (Exception e) {
			String errorMsg = e + Constantes.TEXTO_VACIO;
			String msjError = null; 
		   	 String codError = null; 
			if(errorMsg.contains(Constantes.ERROR_JDBC_TIMEOUT)){
		   		 codError = String.valueOf(propertiesExternos.codIdt1);
		   		 msjError = String.valueOf(propertiesExternos.mensajeIdt1).replace("$bd", bd).replace("$sp", procedure)+" " +e.getMessage();
		   		throw new DBException(codError,msjError,e);  
		        }
			else {
				LOGGER.error(mensajeTransaccion + "ERROR: [Exception] - [" + e.getMessage() + "]", e);
				throw new DBException(response.getKoCodigo(),response.getKoMensaje(),e);  
			}
			
		}finally {
			LOGGER.info(mensajeTransaccion + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,nombreMetodo);
		}
		return response;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public SgassListarRegEnviarOacResponse listarRegistrosEnvioOac(String mensajeTransaccion,
			SgassListarRegEnviarOacRequest request, SgassListarRegEnviarOacResponse response) throws DBException {
		String nombreMetodo="listarRegistrosEnvioOac";
		JdbcTemplate objJdbcTemplate = null;
		String bd=null;
		String owner = null;
		String procedure = null;
		String pkg= null;
		String execute_package= null;
		try {
			bd= propertiesExternos.dbSga;
			owner=propertiesExternos.sgaOwItCollections;
			pkg=propertiesExternos.sgaPkgAjustesMasivos;
			procedure= propertiesExternos.sgaSpgassListarRegEnviarOac;
			execute_package=owner+"."+pkg+"."+procedure;
			LOGGER.info(mensajeTransaccion + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,nombreMetodo);
			 SimpleJdbcCall objJdbcCall = new SimpleJdbcCall(Sga);
			 objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			 objJdbcTemplate.setQueryTimeout(Integer.parseInt(propertiesExternos.dbSgaTimeoutConexion));
			 
			 SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess().withoutProcedureColumnMetaDataAccess()
						.withSchemaName(owner)
						.withCatalogName(pkg)
						.withProcedureName(procedure)
						.declareParameters(
								new SqlParameter( Constantes.K_REGISTRO, OracleTypes.NUMBER ),
								new SqlOutParameter(Constantes.KO_CURSOR, OracleTypes.CURSOR,new ListarRegEnviarOacCursorMapper()),
								new SqlOutParameter(Constantes.KO_CODIGO, OracleTypes.INTEGER),
								new SqlOutParameter(Constantes.KO_MENSAJE, OracleTypes.VARCHAR)
								);
			 LOGGER.info(mensajeTransaccion + Constantes.EJECUTANDO_SP + Constantes.CORCHETE_IZQUIERDO + execute_package + Constantes.CORCHETE_DERECHO);
			 LOGGER.info(mensajeTransaccion + Constantes.PARAMETROS_INPUT);
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.K_REGISTRO + Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS+ request.getkRegistro());
			 
			 SqlParameterSource objParametrosIN = new MapSqlParameterSource()
					 .addValue(Constantes.K_REGISTRO, request.getkRegistro(),OracleTypes.NUMBER);	
			 
			 Map<String, Object> results = (Map<String, Object>)procedureConsulta.execute(objParametrosIN);
			 
			 ArrayList<ListarRegEnviarOacCursor> listaRegistro= (ArrayList<ListarRegEnviarOacCursor>) results.get(Constantes.KO_CURSOR); 	
			 response.setKoCursor(listaRegistro);
			 response.setKoCodigo(String.valueOf(results.get(Constantes.KO_CODIGO)));
			 response.setKoMensaje(String.valueOf(results.get(Constantes.KO_MENSAJE)));
			 
			 LOGGER.info(mensajeTransaccion +  Constantes.PARAMETROS_OUTPUT);
			 LOGGER.info(mensajeTransaccion+Constantes.TEXTO_ESPACIO +Constantes.CORCHETE_IZQUIERDO+ Constantes.KO_CURSOR+Constantes.CORCHETE_DERECHO+ " longitud "+Constantes.TEXTO_DOSPUNTOS +response.getKoCursor().size());
			 LOGGER.info(mensajeTransaccion+Constantes.TEXTO_ESPACIO +Constantes.CORCHETE_IZQUIERDO+ Constantes.KO_CODIGO+Constantes.CORCHETE_DERECHO+Constantes.TEXTO_DOSPUNTOS + response.getKoCodigo());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_ESPACIO +Constantes.CORCHETE_IZQUIERDO+ Constantes.KO_MENSAJE+Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + response.getKoMensaje());
		}catch (Exception e) {
			String errorMsg = e + Constantes.TEXTO_VACIO;
			String msjError = null; 
		   	 String codError = null; 
			if(errorMsg.contains(Constantes.ERROR_JDBC_TIMEOUT)){
		   		 codError = String.valueOf(propertiesExternos.codIdt1);
		   		 msjError = String.valueOf(propertiesExternos.mensajeIdt1).replace("$bd", bd).replace("$sp", procedure)+" " +e.getMessage();
		   		throw new DBException(codError,msjError,e);  
		        }
			else {
				LOGGER.error(mensajeTransaccion + "ERROR: [Exception] - [" + e.getMessage() + "]", e);
				throw new DBException(response.getKoCodigo(),response.getKoMensaje(),e);  
			}
			
		}finally {
			LOGGER.info(mensajeTransaccion + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,nombreMetodo);
		}
		return response;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public SgasiAjusteDocumentoResponse ejecutarAjusteSga(String mensajeTransaccion,
			SgasiAjusteDocumentoRequest request, SgasiAjusteDocumentoResponse response) throws DBException {
		String nombreMetodo="ejecutarAjusteSga";
		JdbcTemplate objJdbcTemplate = null;
		String bd=null;
		String owner = null;
		String procedure = null;
		String pkg= null;
		String execute_package= null;
		try {
			bd= propertiesExternos.dbSga;
			owner=propertiesExternos.sgaOwItCollections;
			pkg=propertiesExternos.sgaPkgAjustesMasivos;
			procedure= propertiesExternos.sgaSpSgasiAjusteDocumento;
			execute_package=owner+"."+pkg+"."+procedure;
			LOGGER.info(mensajeTransaccion + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,nombreMetodo);
			
			 SimpleJdbcCall objJdbcCall = new SimpleJdbcCall(Sga);
			 objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			 objJdbcTemplate.setQueryTimeout(Integer.parseInt(propertiesExternos.dbSgaTimeoutConexion));
			 
			 SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess().withoutProcedureColumnMetaDataAccess()
						.withSchemaName(owner)
						.withCatalogName(pkg)
						.withProcedureName(procedure)
						.declareParameters(
								new SqlParameter( Constantes.K_REGISTRO, OracleTypes.NUMBER ),
								new SqlOutParameter(Constantes.KO_CODIGO, OracleTypes.INTEGER),
								new SqlOutParameter(Constantes.KO_MENSAJE, OracleTypes.VARCHAR)
								);
			 LOGGER.info(mensajeTransaccion + Constantes.EJECUTANDO_SP + Constantes.CORCHETE_IZQUIERDO + execute_package + Constantes.CORCHETE_DERECHO);
			 LOGGER.info(mensajeTransaccion + Constantes.PARAMETROS_INPUT);
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.K_REGISTRO + Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS+ request.getkRegistro());

			 SqlParameterSource objParametrosIN = new MapSqlParameterSource()
					 .addValue(Constantes.K_REGISTRO, request.getkRegistro(),OracleTypes.NUMBER);
			 Map<String, Object> results = (Map<String, Object>)procedureConsulta.execute(objParametrosIN);
	
			 response.setKoCodigo(String.valueOf(results.get(Constantes.KO_CODIGO)));
			 response.setKoMensaje(String.valueOf(results.get(Constantes.KO_MENSAJE)));
			 
			 LOGGER.info(mensajeTransaccion +  Constantes.PARAMETROS_OUTPUT);						 
			 LOGGER.info(mensajeTransaccion+Constantes.TEXTO_ESPACIO +Constantes.CORCHETE_IZQUIERDO+ Constantes.KO_CODIGO+Constantes.CORCHETE_DERECHO+Constantes.TEXTO_DOSPUNTOS + response.getKoCodigo());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_ESPACIO +Constantes.CORCHETE_IZQUIERDO+ Constantes.KO_MENSAJE+Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + response.getKoMensaje());
		}catch (Exception e) {
			String errorMsg = e + Constantes.TEXTO_VACIO;
			String msjError = null; 
		   	 String codError = null; 
			if(errorMsg.contains(Constantes.ERROR_JDBC_TIMEOUT)){
		   		 codError = String.valueOf(propertiesExternos.codIdt1);
		   		 msjError = String.valueOf(propertiesExternos.mensajeIdt1).replace("$bd", bd).replace("$sp", procedure)+" " +e.getMessage();
		   		throw new DBException(codError,msjError,e);  
		        }
			else {
				LOGGER.error(mensajeTransaccion + "ERROR: [Exception] - [" + e.getMessage() + "]", e);
				throw new DBException(response.getKoCodigo(),response.getKoMensaje(),e);  
			}
			
		}finally {
			LOGGER.info(mensajeTransaccion + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,nombreMetodo);
		}
		return response;
		
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public SgasuActDocumentoResponse actualizarAjusteEstadoSga(String mensajeTransaccion, SgasuActDocumentoRequest request,
			SgasuActDocumentoResponse response) throws DBException {
		String nombreMetodo="actualizarAjusteEstadoSga";
		JdbcTemplate objJdbcTemplate = null;
		String bd=null;
		String owner = null;
		String procedure = null;
		String pkg= null;
		String execute_package= null;
		try {
			bd= propertiesExternos.dbSga;
			owner=propertiesExternos.sgaOwItCollections;
			pkg=propertiesExternos.sgaPkgAjustesMasivos;
			procedure= propertiesExternos.sgaSpSgasuActDocumento;
			execute_package=owner+"."+pkg+"."+procedure;
			LOGGER.info(mensajeTransaccion + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,nombreMetodo);
			
			 SimpleJdbcCall objJdbcCall = new SimpleJdbcCall(Sga);
			 objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			 objJdbcTemplate.setQueryTimeout(Integer.parseInt(propertiesExternos.dbSgaTimeoutConexion));
			 
			 SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess().withoutProcedureColumnMetaDataAccess()
						.withSchemaName(owner)
						.withCatalogName(pkg)
						.withProcedureName(procedure)
						.declareParameters(  
								new SqlParameter(Constantes.K_REGISTRO, OracleTypes.NUMBER),
								new SqlParameter(Constantes.K_AMADV_DOCUMENTO, OracleTypes.VARCHAR),
								new SqlParameter(Constantes.K_VALOR, OracleTypes.INTEGER),
								new SqlOutParameter(Constantes.KO_CODIGO, OracleTypes.INTEGER),
								new SqlOutParameter(Constantes.KO_MENSAJE, OracleTypes.VARCHAR)
								);
			 LOGGER.info(mensajeTransaccion + Constantes.EJECUTANDO_SP + Constantes.CORCHETE_IZQUIERDO + execute_package + Constantes.CORCHETE_DERECHO);
			 LOGGER.info(mensajeTransaccion +  Constantes.PARAMETROS_INPUT);	
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.K_REGISTRO + Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS+ request.getkRegistro());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.K_AMADV_DOCUMENTO + Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS+ request.getkRegistro());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.K_VALOR + Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS+ request.getkValor());
			 SqlParameterSource objParametrosIN = new MapSqlParameterSource()
					 .addValue(Constantes.K_REGISTRO, request.getkRegistro(),OracleTypes.NUMBER)
					 .addValue(Constantes.K_AMADV_DOCUMENTO, request.getkAmadvDocumento(),OracleTypes.VARCHAR)
					 .addValue(Constantes.K_VALOR, request.getkValor(),OracleTypes.INTEGER);
			 Map<String, Object> results = (Map<String, Object>)procedureConsulta.execute(objParametrosIN);
	
			 response.setKoCodigo(String.valueOf(results.get(Constantes.KO_CODIGO)));
			 response.setKoMensaje(String.valueOf(results.get(Constantes.KO_MENSAJE)));
			 LOGGER.info(mensajeTransaccion +  Constantes.PARAMETROS_OUTPUT);			
			 
			 LOGGER.info(mensajeTransaccion+Constantes.TEXTO_ESPACIO +Constantes.CORCHETE_IZQUIERDO+ Constantes.KO_CODIGO+Constantes.CORCHETE_DERECHO+Constantes.TEXTO_DOSPUNTOS + response.getKoCodigo());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_ESPACIO +Constantes.CORCHETE_IZQUIERDO+ Constantes.KO_MENSAJE+Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + response.getKoMensaje());
}catch (Exception e) {
			String errorMsg = e + Constantes.TEXTO_VACIO;
			String msjError = null; 
		   	 String codError = null; 
			if(errorMsg.contains(Constantes.ERROR_JDBC_TIMEOUT)){
		   		 codError = String.valueOf(propertiesExternos.codIdt1);
		   		 msjError = String.valueOf(propertiesExternos.mensajeIdt1).replace("$bd", bd).replace("$sp", procedure)+" " +e.getMessage();
		   		throw new DBException(codError,msjError,e);  
		        }
			else {
				LOGGER.error(mensajeTransaccion + "ERROR: [Exception] - [" + e.getMessage() + "]", e);
				throw new DBException(response.getKoCodigo(),response.getKoMensaje(),e);  
			}
			
		}finally {
			LOGGER.info(mensajeTransaccion + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,nombreMetodo);
		}
		return response;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public SgassListarRegistrosResponse reporteProceso(String mensajeTransaccion, SgassListarRegistrosRequest request,
			SgassListarRegistrosResponse response) throws DBException {
		String nombreMetodo="reporteProceso";
		JdbcTemplate objJdbcTemplate = null;
		String bd=null;
		String owner = null;
		String procedure = null;
		String pkg= null;
		String execute_package= null;
		try {
			bd= propertiesExternos.dbSga;
			owner=propertiesExternos.sgaOwItCollections;
			pkg=propertiesExternos.sgaPkgAjustesMasivos;
			procedure= propertiesExternos.sgaSpSgassListarRegistros;
			execute_package=owner+"."+pkg+"."+procedure;
			LOGGER.info(mensajeTransaccion + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,nombreMetodo);
			 SimpleJdbcCall objJdbcCall = new SimpleJdbcCall(Sga);
			 objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			 objJdbcTemplate.setQueryTimeout(Integer.parseInt(propertiesExternos.dbSgaTimeoutConexion));
			 
			 SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess().withoutProcedureColumnMetaDataAccess()
						.withSchemaName(owner)
						.withCatalogName(pkg)
						.withProcedureName(procedure)
						.declareParameters(
								new SqlParameter( Constantes.K_REGISTRO, OracleTypes.NUMBER ),   
								new SqlOutParameter( Constantes.KO_CORREO, OracleTypes.VARCHAR ),
								new SqlOutParameter( Constantes.KO_NOMBRE_ARCHIVO, OracleTypes.VARCHAR ),
								new SqlOutParameter(Constantes.KO_CURSOR, OracleTypes.CURSOR,new ListaCursorMapper()),
								new SqlOutParameter(Constantes.KO_CODIGO, OracleTypes.INTEGER),
								new SqlOutParameter(Constantes.KO_MENSAJE, OracleTypes.VARCHAR)
								);
			 LOGGER.info(mensajeTransaccion + Constantes.EJECUTANDO_SP + Constantes.CORCHETE_IZQUIERDO + execute_package + Constantes.CORCHETE_DERECHO);
			 LOGGER.info(mensajeTransaccion + Constantes.PARAMETROS_INPUT);
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.K_REGISTRO + Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS+ request.getkRegistro());
			 
			 SqlParameterSource objParametrosIN = new MapSqlParameterSource()
					 .addValue("K_REGISTRO", request.getkRegistro(), OracleTypes.NUMBER );
	
			 
			 Map<String, Object> results = (Map<String, Object>)procedureConsulta.execute(objParametrosIN);
			 response.setKoCorreo(String.valueOf(results.get(Constantes.KO_CORREO)));
			 response.setKoNombreArchivo(String.valueOf(results.get(Constantes.KO_NOMBRE_ARCHIVO)));
			 ArrayList<ListaCursor> listaRegistrosCursor= (ArrayList<ListaCursor>) results.get(Constantes.KO_CURSOR);			
			 response.setKoCursor(listaRegistrosCursor);
			 response.setKoCodigo(String.valueOf(results.get(Constantes.KO_CODIGO)));
			 response.setKoMensaje(String.valueOf(results.get(Constantes.KO_MENSAJE)));
			 LOGGER.info(mensajeTransaccion + Constantes.PARAMETROS_OUTPUT);

			 LOGGER.info(mensajeTransaccion + Constantes.KO_CURSOR  +" Longitud: "+ Constantes.CORCHETE_IZQUIERDO +response.getKoCursor().size()+Constantes.CORCHETE_DERECHO);
			 LOGGER.info(mensajeTransaccion+ Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO + Constantes.KO_CODIGO+ Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + response.getKoCodigo());
			 LOGGER.info(mensajeTransaccion+ Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO + Constantes.KO_MENSAJE+ Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS  + response.getKoMensaje());
		}catch (Exception e) {
			String errorMsg = e + Constantes.TEXTO_VACIO;
			String msjError = null; 
		   	 String codError = null; 
			if(errorMsg.contains(Constantes.ERROR_JDBC_TIMEOUT)){
		   		 codError = String.valueOf(propertiesExternos.codIdt1);
		   		 msjError = String.valueOf(propertiesExternos.mensajeIdt1).replace("$bd", bd).replace("$sp", procedure)+" " +e.getMessage();
		   		throw new DBException(codError,msjError,e);  
		        }
			else {
				LOGGER.error(mensajeTransaccion + "ERROR: [Exception] - [" + e.getMessage() + "]", e);
				throw new DBException(response.getKoCodigo(),response.getKoMensaje(),e);  
			}
			
		}finally {
			LOGGER.info(mensajeTransaccion + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,nombreMetodo);
		}
		return response;
	}

	@Override
	@SuppressWarnings("unchecked")
	public SgasuValidaOacResponse actualizarEstadoSga(String mensajeTransaccion, SgasuValidaOacRequest request,
			SgasuValidaOacResponse response) throws DBException {
		String nombreMetodo="actualizarEstadoSga";
		JdbcTemplate objJdbcTemplate = null;
		String bd=null;
		String owner = null;
		String procedure = null;
		String pkg= null;
		String execute_package= null;
		try {
			bd= propertiesExternos.dbSga;
			owner=propertiesExternos.sgaOwItCollections;
			pkg=propertiesExternos.sgaPkgAjustesMasivos;
			procedure= propertiesExternos.sgasuValidaOac;
			execute_package=owner+"."+pkg+"."+procedure;
			LOGGER.info(mensajeTransaccion + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,nombreMetodo);
			 SimpleJdbcCall objJdbcCall = new SimpleJdbcCall(Sga);
			 objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			 objJdbcTemplate.setQueryTimeout(Integer.parseInt(propertiesExternos.dbSgaTimeoutConexion));
			 
			 SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess().withoutProcedureColumnMetaDataAccess()
						.withSchemaName(owner)
						.withCatalogName(pkg)
						.withProcedureName(procedure)
						.declareParameters(
								new SqlParameter( Constantes.K_REGISTRO, OracleTypes.NUMBER ),   
								new SqlParameter(Constantes.K_AMADV_DOCUMENTO, OracleTypes.VARCHAR),
								new SqlParameter(Constantes.K_CODIGO, OracleTypes.VARCHAR),
								new SqlOutParameter(Constantes.KO_CODIGO, OracleTypes.INTEGER),
								new SqlOutParameter(Constantes.KO_MENSAJE, OracleTypes.VARCHAR)
								);
			 LOGGER.info(mensajeTransaccion + Constantes.EJECUTANDO_SP + Constantes.CORCHETE_IZQUIERDO + execute_package + Constantes.CORCHETE_DERECHO);
			 LOGGER.info(mensajeTransaccion + Constantes.PARAMETROS_INPUT);
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.K_REGISTRO + Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS+ request.getkRegistro());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.K_AMADV_DOCUMENTO + Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS+ request.getkAmadvDocumento());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.K_CODIGO + Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS+ request.getkCodigo());
			 SqlParameterSource objParametrosIN = new MapSqlParameterSource()
					 .addValue(Constantes.K_REGISTRO, Integer.parseInt(request.getkRegistro()) )
					 .addValue(Constantes.K_AMADV_DOCUMENTO, String.valueOf(request.getkAmadvDocumento()) )
					 .addValue(Constantes.K_CODIGO, String.valueOf(request.getkCodigo()) );
	
			 
			 Map<String, Object> results = (Map<String, Object>)procedureConsulta.execute(objParametrosIN);
	
			 response.setKoCodigo(String.valueOf(results.get(Constantes.KO_CODIGO)));
			 response.setKoMensaje(String.valueOf(results.get(Constantes.KO_MENSAJE)));
			 
			 LOGGER.info(mensajeTransaccion + Constantes.PARAMETROS_OUTPUT);
			 LOGGER.info(mensajeTransaccion  +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+  Constantes.KO_CODIGO + Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + response.getKoCodigo());
			 LOGGER.info(mensajeTransaccion  +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+  Constantes.KO_MENSAJE + Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS + response.getKoMensaje());
		}catch (Exception e) {
			String errorMsg = e + Constantes.TEXTO_VACIO;
			String msjError = null; 
		   	 String codError = null; 
			if(errorMsg.contains(Constantes.ERROR_JDBC_TIMEOUT)){
		   		 codError = String.valueOf(propertiesExternos.codIdt1);
		   		 msjError = String.valueOf(propertiesExternos.mensajeIdt1).replace("$bd", bd).replace("$sp", procedure)+" " +e.getMessage();
		   		throw new DBException(codError,msjError,e);  
		        }
			else {
				LOGGER.error(mensajeTransaccion + "ERROR: [Exception] - [" + e.getMessage() + "]", e);
				throw new DBException(response.getKoCodigo(),response.getKoMensaje(),e);  
			}
			
		}finally {
			LOGGER.info(mensajeTransaccion + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,nombreMetodo);
		}
		return response;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public SgassListarAjusteOacResponse listarAjusteOac(String mensajeTransaccion, SgassListarAjusteOacRequest request,
			SgassListarAjusteOacResponse response) throws DBException {
		String nombreMetodo="listarAjusteOac";
		JdbcTemplate objJdbcTemplate = null;
		String bd=null;
		String owner = null;
		String procedure = null;
		String pkg= null;
		String execute_package= null;
		try {
			bd= propertiesExternos.dbSga;
			owner=propertiesExternos.sgaOwItCollections;
			pkg=propertiesExternos.sgaPkgAjustesMasivos;
			procedure= propertiesExternos.sgassListarAjusteOac;
			execute_package=owner+"."+pkg+"."+procedure;
			LOGGER.info(mensajeTransaccion + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,nombreMetodo);
			 SimpleJdbcCall objJdbcCall = new SimpleJdbcCall(Sga);
			 objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			 objJdbcTemplate.setQueryTimeout(Integer.parseInt(propertiesExternos.dbSgaTimeoutConexion));
			 
			 SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess().withoutProcedureColumnMetaDataAccess()
						.withSchemaName(owner)
						.withCatalogName(pkg)
						.withProcedureName(procedure)
						.declareParameters(
								new SqlParameter( Constantes.K_REGISTRO, OracleTypes.NUMBER ),   
								new SqlOutParameter(Constantes.KO_CURSOR, OracleTypes.CURSOR,new SgassListarAjusteOacCursorMapper()),
								new SqlOutParameter(Constantes.KO_CODIGO, OracleTypes.INTEGER),
								new SqlOutParameter(Constantes.KO_MENSAJE, OracleTypes.VARCHAR)
								);
			 LOGGER.info(mensajeTransaccion + Constantes.EJECUTANDO_SP + Constantes.CORCHETE_IZQUIERDO + execute_package + Constantes.CORCHETE_DERECHO);
			 LOGGER.info(mensajeTransaccion + Constantes.PARAMETROS_INPUT);
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.K_REGISTRO + Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS+ request.getkRegistro());
			 
			 SqlParameterSource objParametrosIN = new MapSqlParameterSource()
					 .addValue(Constantes.K_REGISTRO, request.getkRegistro(), OracleTypes.NUMBER );
	
			 
			 Map<String, Object> results = (Map<String, Object>)procedureConsulta.execute(objParametrosIN);
			 ArrayList<SgassListarAjusteOacCursor> listaRegistrosCursor= (ArrayList<SgassListarAjusteOacCursor>) results.get(Constantes.KO_CURSOR); 	
			 response.setKoCursor(listaRegistrosCursor);
			 response.setKoCodigo(String.valueOf(results.get(Constantes.KO_CODIGO)));
			 response.setKoMensaje(String.valueOf(results.get(Constantes.KO_MENSAJE)));
			 LOGGER.info(mensajeTransaccion + Constantes.PARAMETROS_OUTPUT);

			 LOGGER.info(mensajeTransaccion + Constantes.KO_CURSOR  +" Longitud: "+ Constantes.CORCHETE_IZQUIERDO +response.getKoCursor().size()+Constantes.CORCHETE_DERECHO);
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.KO_CODIGO + Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS+response.getKoCodigo());
			 LOGGER.info(mensajeTransaccion +Constantes.TEXTO_VACIO+ Constantes.CORCHETE_IZQUIERDO+ Constantes.KO_MENSAJE  + Constantes.CORCHETE_DERECHO +Constantes.TEXTO_DOSPUNTOS+response.getKoMensaje());
		}catch (Exception e) {
			String errorMsg = e + Constantes.TEXTO_VACIO;
			String msjError = null; 
		   	 String codError = null; 
			if(errorMsg.contains(Constantes.ERROR_JDBC_TIMEOUT)){
		   		 codError = String.valueOf(propertiesExternos.codIdt1);
		   		 msjError = String.valueOf(propertiesExternos.mensajeIdt1).replace("$bd", bd).replace("$sp", procedure)+" " +e.getMessage();
		   		throw new DBException(codError,msjError,e);  
		        }
			else {
				LOGGER.error(mensajeTransaccion + "ERROR: [Exception] - [" + e.getMessage() + "]", e);
				throw new DBException(response.getKoCodigo(),response.getKoMensaje(),e);  
			}
			
		}finally {
			LOGGER.info(mensajeTransaccion + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,nombreMetodo);
		}
		return response;
	}



}
