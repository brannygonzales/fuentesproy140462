package pe.com.claro.bss.oss.ajustemasivo.service.correo;

import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.WSException;
import pe.com.claro.eai.util.enviocorreo.types.EnviarCorreoRequest;
import pe.com.claro.eai.util.enviocorreo.types.EnviarCorreoResponse;

public interface EnvioCorreo {
	public EnviarCorreoResponse enviarCorreo(String msgTransaction,EnviarCorreoRequest request) throws WSException;
}
