package pe.com.claro.bss.oss.ajustemasivo.service.transaccion;

import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.WSException;
import pe.com.claro.eai.crmservices.clarify.transaccioninteracciones.NuevaInteraccion;
import pe.com.claro.eai.crmservices.clarify.transaccioninteracciones.NuevaInteraccionResponse;

public interface NuevaInteracciones {
	public NuevaInteraccionResponse nuevaInteraccion(String mensajeTransaccion,NuevaInteraccion request) throws WSException;
}
