package pe.com.claro.bss.oss.ajustemasivo.service.correo;

import javax.ejb.Stateless;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;
import javax.xml.ws.BindingProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.com.claro.bss.oss.ajustemasivo.common.property.Constantes;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.WSException;
import pe.com.claro.eai.util.enviocorreo.EnvioCorreoSBPortType;
import pe.com.claro.eai.util.enviocorreo.EnvioCorreoSBPortTypeSOAP11BindingQSService;
import pe.com.claro.eai.util.enviocorreo.types.EnviarCorreoRequest;
import pe.com.claro.eai.util.enviocorreo.types.EnviarCorreoResponse;

@Stateless
public class EnvioCorreoImpl implements EnvioCorreo{
	private static final Logger LOG = LoggerFactory.getLogger(EnvioCorreoImpl.class);
	
	private EnvioCorreoSBPortType envioCorreoSBPortType;
	@Context
	private Configuration configuration;
	
	@Override
	public EnviarCorreoResponse enviarCorreo(String msgTransaction,EnviarCorreoRequest request) throws WSException {
		
		LOG.info(msgTransaction + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ENVIAR_CORREO);
		EnviarCorreoResponse response= new EnviarCorreoResponse();

		try {
		EnvioCorreoSBPortTypeSOAP11BindingQSService service= new EnvioCorreoSBPortTypeSOAP11BindingQSService();
		envioCorreoSBPortType= service.getEnvioCorreoSBPortTypeSOAP11BindingQSPort();
		
		 BindingProvider bindingProvider=(BindingProvider)envioCorreoSBPortType;
		 bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, String.valueOf(configuration.getProperty(Constantes.WSSERVICEENPOINTADDRESS_CORREO)));
			bindingProvider.getRequestContext().put(Constantes.COM_SUN_XML_WS_CONNECT_TIMEOUT, Integer.valueOf(String.valueOf(configuration.getProperty(Constantes.ENVIOCORREOWSSERVICE_MAX_TIMEOUT_CONEXION))));
			bindingProvider.getRequestContext().put(Constantes.COM_SUN_XML_WS_REQUEST_TIMEOUT, Integer.valueOf(String.valueOf(configuration.getProperty(Constantes.ENVIOCORREOWSSERVICE_MAX_TIMEOUT_REQUEST))));
			
		response=envioCorreoSBPortType.enviarCorreo(request);
		LOG.info(msgTransaction+" Codigo de respuesta del servicio correo: \n"+ response.getAuditResponse().getCodigoRespuesta());
		LOG.info(msgTransaction+" Mensaje de respuesta del servicio correo: \n"+response.getAuditResponse().getMensajeRespuesta());
		}
		
		    catch (Exception e)
		    {
				String nombreWS = String.valueOf(configuration.getProperty(Constantes.WSSERVICEENPOINTADDRESS_CORREO));
				String msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT3)).replace("$ws", nombreWS) + " " +e.getMessage() ; 
		    	String codError = String.valueOf(configuration.getProperty(Constantes.CODIGO_IDT3));    
		    	
		    	LOG.error( msgTransaction + " ERROR: [Exception WS: "+nombreWS+" ] - [" + e.getMessage() + "] ",e );
		    	throw new WSException( codError, msjError, nombreWS, e );
		    }
		
		finally {
			LOG.info(msgTransaction + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ENVIAR_CORREO);
		}		
		
		return response;
	}

}
