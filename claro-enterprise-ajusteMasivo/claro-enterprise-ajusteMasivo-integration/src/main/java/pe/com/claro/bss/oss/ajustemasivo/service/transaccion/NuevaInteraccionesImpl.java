package pe.com.claro.bss.oss.ajustemasivo.service.transaccion;

import javax.ejb.Stateless;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.com.claro.bss.oss.ajustemasivo.common.property.Constantes;
import pe.com.claro.bss.oss.ajustemasivo.common.property.JAXBUtilitarios;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.WSException;
import pe.com.claro.eai.crmservices.clarify.transaccioninteracciones.NuevaInteraccion;
import pe.com.claro.eai.crmservices.clarify.transaccioninteracciones.NuevaInteraccionResponse;
import pe.com.claro.eai.crmservices.clarify.transaccioninteracciones.TransaccionInteracciones;
import pe.com.claro.eai.crmservices.clarify.transaccioninteracciones.TransaccionInteracciones_Service;
import pe.com.claro.eai.servicecommons.AuditType;


@Stateless
public class NuevaInteraccionesImpl implements NuevaInteracciones{
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());
	private TransaccionInteracciones transaccionInteracciones;
	@Context
	private Configuration configuration;
	
	@Override
	public NuevaInteraccionResponse nuevaInteraccion(String msgTransaction, NuevaInteraccion request) throws WSException{	
		LOGGER.info(msgTransaction + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_NUEVA_INTERACCION);
		NuevaInteraccionResponse response = new NuevaInteraccionResponse();
		try {
			AuditType audit=new AuditType();
			String interaccion= new String();
			Holder<AuditType> auditResponse=new Holder<AuditType>();
			Holder<String> interaccionId= new Holder<String>();	
			LOGGER.info(msgTransaction + "Datos de entrada:\n" + JAXBUtilitarios.anyObjectToXmlText(request));
			LOGGER.info(msgTransaction + "URL WSDL: " + configuration.getProperty(Constantes.WSSERVICEENPOINTADDRESS_INTERACCION));
			
			TransaccionInteracciones_Service transaccionInteracciones_service= new TransaccionInteracciones_Service();
			transaccionInteracciones=transaccionInteracciones_service.getTransaccionInteraccionesSOAP();
			BindingProvider bindingProvider=(BindingProvider)transaccionInteracciones;
			bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, String.valueOf(configuration.getProperty(Constantes.WSSERVICEENPOINTADDRESS_INTERACCION)));
			bindingProvider.getRequestContext().put(Constantes.COM_SUN_XML_WS_CONNECT_TIMEOUT, Integer.valueOf(String.valueOf(configuration.getProperty(Constantes.TRANSACTIONWSSERVICE_MAX_TIMEOUT_CONEXION))));
			bindingProvider.getRequestContext().put(Constantes.COM_SUN_XML_WS_REQUEST_TIMEOUT, Integer.valueOf(String.valueOf(configuration.getProperty(Constantes.TRANSACTIONWSSERVICE_MAX_TIMEOUT_REQUEST))));
			
			transaccionInteracciones.nuevaInteraccion(request.getTxId(), request.getInteraccion(), auditResponse, interaccionId);
			audit=auditResponse.value;
			interaccion=interaccionId.value;
			response.setAudit(audit);
			response.setInteraccionId(interaccion);
			LOGGER.info(msgTransaction+" Codigo de respuesta del servicio Tipificacion: \n"+response.getAudit().getErrorCode());
			LOGGER.info(msgTransaction+" Mensaje de respuesta del servicio Tipificacion: \n" + response.getAudit().getErrorMsg());
		}catch (Exception e)
	    {
			String nombreWS = String.valueOf(configuration.getProperty(Constantes.WSSERVICEENPOINTADDRESS_INTERACCION));
			String msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT3)).replace("$ws", nombreWS) + " " +e.getMessage() ; 
	    	String codError = String.valueOf(configuration.getProperty(Constantes.CODIGO_IDT3));    
	    	
	    	LOGGER.error( msgTransaction + " ERROR: [Exception WS: "+nombreWS+" ] - [" + e.getMessage() + "] ",e );
	    	throw new WSException( codError, msjError, nombreWS, e );
	    }
		finally {
			LOGGER.info(msgTransaction + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_NUEVA_INTERACCION);
		}
		return response;
	}

}
