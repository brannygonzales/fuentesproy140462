package pe.com.claro.bss.oss.ajustemasivo.common.property;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Pattern;

import javax.ws.rs.core.HttpHeaders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class ClaroUtil{
	private static final Logger LOG = LoggerFactory.getLogger(ClaroUtil.class);
	
	public static Calendar toCalendar(final String iso8601string) {
		Calendar calendar = GregorianCalendar.getInstance();
		try {
			boolean exito = false;
			String s = iso8601string.replace("Z", "+00:00");
			if (iso8601string.length() == Constantes.VEINTE) { // *** Sin Precision de Milisegundos
				s = s.substring(0, 22) + s.substring(23);
				Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault()).parse(s);
				calendar.setTime(date);
				exito = true;
			}
			if (iso8601string.length() == Constantes.VEINTE_CUATRO) { // *** Con Precision de Milisegundos
				s = s.substring(0, 26) + s.substring(27);
				Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.getDefault()).parse(s);
				calendar.setTime(date);
				exito = true;
			}
			if (!exito) {
				calendar = null;
			}

		} catch (IndexOutOfBoundsException e) {
			LOG.error("Ocurrio un error al recorrer la cadena de Fecha", e);
			calendar = null;
		} catch (Exception e) {
			LOG.error("Ocurrio un error al convertir a Date la cadena de la fecha", e);
			calendar = null;
		}
		return calendar;
	}
	
	public static DateFormat getLocalFormat() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX", Locale.getDefault());
		dateFormat.setTimeZone(TimeZone.getDefault());
		return dateFormat;
	}

	public static String printPrettyJSONString(Object o) throws JsonProcessingException {
		return new ObjectMapper().setDateFormat(ClaroUtil.getLocalFormat())
				.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false).writerWithDefaultPrettyPrinter()
				.writeValueAsString(o);
	}

	public static String getAllHttpHeaders(HttpHeaders httpHeaders) {
		StringBuffer data = new StringBuffer();

		Set<String> headerKeys = httpHeaders.getRequestHeaders().keySet();
		for (String header : headerKeys) {
			data.append(header + ":" + httpHeaders.getRequestHeader(header).get(0) + "\n");
		}
		return data.toString().trim();
	}
	public static String getHttpHeadersNoNull(HttpHeaders httpHeaders) {
		String idTransaccion = httpHeaders.getRequestHeader(Constantes.IDTRANSACCION) != null
				? httpHeaders.getRequestHeader(Constantes.IDTRANSACCION).get(0) : "";
		String msgid = httpHeaders.getRequestHeader(Constantes.MSGID) != null ? httpHeaders.getRequestHeader(Constantes.MSGID).get(0)
				: "";
		String timestamp =  "";				
		Calendar a = ClaroUtil.toCalendar( httpHeaders.getRequestHeader( Constantes.TIMESTAMP ) != null? httpHeaders.getRequestHeader( Constantes.TIMESTAMP ).get( 0 ).toString(): Constantes.TEXTOVACIO );
		if( a != null )
			timestamp = ClaroUtil.dateAString( a.getTime() );
		String userId = httpHeaders.getRequestHeader(Constantes.USRID) != null ? httpHeaders.getRequestHeader(Constantes.USRID).get(0) : "";
		String accept = httpHeaders.getRequestHeader(Constantes.ACCEPT) != null ? httpHeaders.getRequestHeader(Constantes.ACCEPT).get(0) : "";
		StringBuffer data = new StringBuffer();
		data.append(Constantes.CORCHETE_IZQUIERDO+Constantes.IDTRANSACCION+"=");
		data.append(idTransaccion);
		data.append(" " + Constantes.MSGID+"=");
		data.append(msgid);		
		data.append(" "+Constantes.TIMESTAMP+"=");
		data.append(timestamp);
		data.append(" "+Constantes.USRID+"=");
		data.append(userId);
		data.append(" "+Constantes.ACCEPT+"=");
		data.append(accept);
		data.append(Constantes.CORCHETE_DERECHO);
		return data.toString();
	}

	public static String getHttpHeaders( HttpHeaders httpHeaders ){

		if( httpHeaders.getRequestHeader( Constantes.IDTRANSACCION ) == null )
			return null;
		if( httpHeaders.getRequestHeader( Constantes.MSGID ) == null )
			return null;
		if( httpHeaders.getRequestHeader( Constantes.TIMESTAMP ) == null )
			return null;
		if( httpHeaders.getRequestHeader( Constantes.USRID ) == null )
			return null;

		String idTransaccion = httpHeaders.getRequestHeader( Constantes.IDTRANSACCION ) != null? httpHeaders.getRequestHeader( Constantes.IDTRANSACCION ).get( 0 ): "";
		String msgid = httpHeaders.getRequestHeader( Constantes.MSGID ) != null? httpHeaders.getRequestHeader( Constantes.MSGID ).get( 0 ): "";
		String timestamp = "";

		String userId = httpHeaders.getRequestHeader( Constantes.USRID ) != null? httpHeaders.getRequestHeader( Constantes.USRID ).get( 0 ): "";

		StringBuffer data = new StringBuffer();
		data.append(Constantes.CORCHETE_IZQUIERDO  + Constantes.IDTRANSACCION + "=" );
		data.append( idTransaccion );
		data.append( " " + Constantes.MSGID + "=" );
		data.append( msgid );
		data.append( " " + Constantes.TIMESTAMP + "=" );
		data.append( timestamp );
		data.append( " " + Constantes.USRID + "=" );
		data.append( userId );
		data.append( " " + Constantes.ACCEPT + "=" );
		data.append( httpHeaders.getMediaType() );
		data.append( Constantes.CORCHETE_DERECHO );
		return data.toString();
	}

	public static String nuloAVacio( Object object ){

		if( object == null ){
			return Constantes.TEXTOVACIO;
		}
		else{
			return object.toString();
		}
	}

	public static String verifiyNull( Object object ){
		String a = null;
		if( object != null ){
			a = object.toString();
		}
		return a;
	}

	public static Integer convertirInteger( Object object ){

		Integer res = null;
		if( object != null ){
			if( object instanceof BigDecimal ){
				BigDecimal bd = (BigDecimal)object;
				res = bd.intValueExact();
			}
			else{
				LOG.info(object.getClass().getSimpleName() );
			}
		}
		return res;
	}

	public static Float convertirFloat( Object object ){
		Float res = null;
		if( object != null ){
			if( object instanceof BigDecimal ){
				BigDecimal bd = (BigDecimal)object;
				res = bd.floatValue();
			}
		}
		return res;
	}

	/**
	 * Genera un String a partir de un Date, si fecha es NULL retorna ""
	 * (vac�o).
	 * @param fecha
	 * tipo Date
	 * @return String de la forma dd/MM/yyyy
	 */
	public static String dateAString( Date fecha ){
		if( fecha == null ){
			return Constantes.TEXTOVACIO;
		}
		return dateAString( fecha, Constantes.FORMATOFECHADEFAULT );
	}

	/**
	 * Genera un String a partir de un Date de acuerdo al fomrato enviado, si
	 * fecha es NULL toma la fecha actual.
	 * @param fecha
	 * @param formato
	 * @return
	 */
	public static String dateAString( Date fecha, String formato ){
		SimpleDateFormat formatoDF = new SimpleDateFormat( formato, Locale.ROOT );
		return formatoDF.format( fecha );
	}
	  public static final List<String> obtenerTramaOk(String texto, String split)
	  {
	    List<String> linea = new ArrayList<String>();
	    if ((texto != null) && (!texto.isEmpty())) {
	      String[] lisTrama = texto.split(Pattern.quote(split));
	      for (String trama : lisTrama) {
	        linea.add(trama);
	      }
	    }

	    return linea;
	  }

	  public static final String numeroDescripcion(String trama, String buscar, int posicion) {
	    if ((trama != null) && (!trama.isEmpty())) {
	      String[] listaAjError = trama.split(Pattern.quote("|"));
	      for (int e = 0; e < listaAjError.length; e++) {
	        if (listaAjError[e].indexOf(buscar) > 1) {
	          String[] a = listaAjError[e].split(";");
	          return a[posicion];
	        }
	      }
	    }
	    return "";
	  }
}
