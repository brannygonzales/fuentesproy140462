package pe.com.claro.bss.oss.ajustemasivo.common.property;

public class Constantes {

	public static final String NOMBRERECURSO = "claro-enterprise-ajusteMasivo";
	public static final String PROPERTIESEXTERNOS = ".properties";
	public static final String PROPERTIESKEY = "claro.properties";
	public static final String SEPARADORPUNTO = ".";
	public static final String VERSION = "1.0.0";
	public static final String PATH = "/bss/oss/ajusteMasivo";
	public static final String BASEPATH = "claro-bss-oss-encolaDetalle-resource/api";

	public static final String RESOURCE = "/bss/oss/ajusteMasivo";
	public static final String COM_SUN_XML_WS_CONNECT_TIMEOUT = "com.sun.xml.ws.connect.timeout";
	public static final String COM_SUN_XML_WS_REQUEST_TIMEOUT = "com.sun.xml.ws.request.timeout";

	// correo
	public static final String WSSERVICEENPOINTADDRESS_CORREO = "ws.enviocorreoWSService.enpointAddress";
	public static final String CORREO_REMITENTE = "ws.enviocorreoWSService.remitente";
	public static final String CORREO_ASUNTO = "ws.enviocorreoWSService.asunto";
	public static final String ENVIOCORREOWSSERVICE_MAX_TIMEOUT_REQUEST="ws.envioCorreoWSService.max.timeout.request";
	public static final String ENVIOCORREOWSSERVICE_MAX_TIMEOUT_CONEXION="ws.envioCorreoWSService.max.timeout.conexion";
	
	// interaccion
	public static final String WSSERVICEENPOINTADDRESS_INTERACCION = "ws.transactionWSService.enpointAddress";
	public static final String TRANSACCIONINTERACCIONES_INTERACCION_ERROR = "transaccionInteracciones.interaccion.error";
	public static final String TRANSACTIONWSSERVICE_MAX_TIMEOUT_REQUEST="ws.transactionWSService.max.timeout.request";
	public static final String TRANSACTIONWSSERVICE_MAX_TIMEOUT_CONEXION="ws.transactionWSService.max.timeout.conexion";

	// detalle
	public static final String NOMBRE_METODO_DETALLE = "/encolarDetalle";
	public static final String WSSERVICEENPOINTADDRESS_DETALLE = "ws.service.endpoint.address.detalle";

	// cabecera
	public static final String NOMBRE_METODO_CABECERA = "/encolarCabecera";
	public static final String WSSERVICEENPOINTADDRESS_CABECERA = "ws.service.endpoint.address.cabecera";

	// reporte - CONSULTARDOCSUNATSIOP
	public static final String NOMBRE_METODO_CONSULTARDOCSUNATSIOP = "/consultarDocSunatSIOP";
	public static final String WSSERVICEENPOINTADDRESS_CONSULTARDOCSUNATSIOP = "ws.service.endpoint.address.consultarDocSunatSIOP";

	// listaProceso
	public static final String NOMBRE_METODO_LISTAPROCESO = "/listaProceso";
	public static final String WSSERVICEENPOINTADDRESS_LISTAPROCESO = "ws.service.endpoint.address.lista.proceso";

	// anular
	public static final String NOMBRE_METODO_ANULAR = "/anular";
	public static final String WSSERVICEENPOINTADDRESS_ANULAR = "ws.service.endpoint.address.anular";

	public static final String EJECUTANDO_SP = "Ejecutando SP: ";

	// *** Conexion a BD SIOP	
	public static final String PERSISTENCEPACKAGEUNIT_DETALLE = "ajustemasivo.tora10g.siop";
	public static final String PERSISTENCEPACKAGEUNIT_ENCOLARDETALLE_SGA = "ds.noxa.sga.jndi";
	public static final String PERSISTENCEPACKAGEUNIT_CABECERA = "ajustemasivo.tora10g.siop";
	public static final String PERSISTENCEPACKAGEUNIT_ENCOLARCABECERA_SGA = "ds.noxa.sga.jndi";
	
	public static final String PERSISTENCEPACKAGEUNIT_CONSULTARDOCSUNATSIOP = "ajustemasivo.tora10g.siop";
	public static final String PERSISTENCEPACKAGEUNIT_INSERT_TRX_PROCESO = "ajustemasivo.tora10g.siop";
	public static final String PERSISTENCEPACKAGEUNIT_LINEAPROCESO = "ajustemasivo.tora10g.siop";
	
	public static final String PERSISTENCEPACKAGEUNIT_OBTENER_LINEA = "ajustemasivo.tora10g.siop";//"ds.siopdb.jndi";
	public static final String PERSISTENCEPACKAGEUNIT_ANULAR_AJUSTE = "ds.noxa.oacdb.jndi";
	public static final String PERSISTENCEPACKAGEUNIT_ACTUALIZAR_AJUSTE = "ajustemasivo.tora10g.siop";//"ds.siopdb.jndi";
	public static final String PERSISTENCEPACKAGEUNIT_ANULAR_AJUSTE_SGA = "ds.noxa.sga.jndi";
	
		// *** Conexion a BD SIOP DETALLE
	public static final String NOMBRE_BD_DETALLE = "nombre.bd.detalle";
	public static final String OWNER_BD_DETALLE = "owner.bd.detalle";
	public static final String NOMBRE_PKG_DETALLE = "nombre.pkg.detalle";
	public static final String NOMBRE_SP_DETALLE = "nombre.sp.detalle";
	public static final String NOMBRE_TM_DETALLE = "nombre.tm.detalle";
	
	// *** Conexion a BD SGA EncolcardetalleSgaRepository
	public static final String NOMBRE_BD_ENCOLARDETALLE_SGA = "nombre.bd.encolardetalle.sga";
	public static final String OWNER_BD_ENCOLARDETALLE_SGA = "owner.bd.encolardetalle.sga";
	public static final String NOMBRE_PKG_ENCOLARDETALLE_SGA = "nombre.pkg.encolardetalle.sga";
	public static final String NOMBRE_SP_ENCOLARDETALLE_SGA = "nombre.sp.encolardetalle.sga";
	public static final String NOMBRE_TM_ENCOLARDETALLE_SGA = "nombre.tm.encolardetalle.sga";

	// *** Conexion a BD SIOP INSERT_TRX_PROCESO
	public static final String NOMBRE_BD_INSERT_TRX_PROCESO = "nombre.bd.insert.trx.proceso";
	public static final String OWNER_BD_INSERT_TRX_PROCESO = "owner.bd.insert.trx.proceso";
	public static final String NOMBRE_PKG_INSERT_TRX_PROCESO = "nombre.pkg.insert.trx.proceso";
	public static final String NOMBRE_SP_INSERT_TRX_PROCESO = "nombre.sp.insert.trx.proceso";
	public static final String NOMBRE_TM_INSERT_TRX_PROCESO = "nombre.tm.insert.trx.proceso";
	// *** Conexion a BD SIOP CABECERA
	public static final String NOMBRE_BD_CABECERA = "nombre.bd.cabecera";
	public static final String OWNER_BD_CABECERA = "owner.bd.cabecera";
	public static final String NOMBRE_PKG_CABECERA = "nombre.pkg.cabecera";
	public static final String NOMBRE_SP_CABECERA = "nombre.sp.cabecera";
	public static final String NOMBRE_TM_CABECERA = "nombre.tm.cabecera";
	
	// *** Conexion a BD SGA EncolcarCabeceraSgaRepository
	public static final String NOMBRE_BD_ENCOLARCABECERA_SGA = "nombre.bd.encolarcabecera.sga";
	public static final String OWNER_BD_ENCOLARCABECERA_SGA = "owner.bd.encolarcabecera.sga";
	public static final String NOMBRE_PKG_ENCOLARCABECERA_SGA = "nombre.pkg.encolarcabecera.sga";
	public static final String NOMBRE_SP_ENCOLARCABECERA_SGA = "nombre.sp.encolarcabecera.sga";
	public static final String NOMBRE_TM_ENCOLARCABECERA_SGA = "nombre.tm.encolarcabecera.sga";
	// *** Conexion a BD SIOP REPORTE - CONSULTARDOCSUNATSIOP
	public static final String NOMBRE_BD_CONSULTARDOCSUNATSIOP = "nombre.bd.consultardocsunatsiop";
	public static final String OWNER_BD_CONSULTARDOCSUNATSIOP = "owner.bd.consultardocsunatsiop";
	public static final String NOMBRE_PKG_CONSULTARDOCSUNATSIOP = "nombre.pkg.consultardocsunatsiop";
	public static final String NOMBRE_SP_CONSULTARDOCSUNATSIOP = "nombre.sp.consultardocsunatsiop";
	public static final String NOMBRE_TM_CONSULTARDOCSUNATSIOP = "nombre.tm.consultardocsunatsiop";
	// *** Conexion a BD SGA AnularAjusteRepository
	public static final String NOMBRE_BD_ANULAR = "nombre.bd.anular";
	public static final String OWNER_BD_ANULAR = "owner.bd.anular";
	public static final String NOMBRE_PKG_ANULAR = "nombre.pkg.anular";
	public static final String NOMBRE_SP_ANULAR = "nombre.sp.anular";
	public static final String NOMBRE_TM_ANULAR = "nombre.tm.anular";
		
	// *** anular SGA
	public static final String NOMBRE_BD_SGASI_REGISTRAR_DOC_ANULAR="nombre.bd.sgasi.registrar.doc.anular";
	public static final String OWNER_BD_SGASI_REGISTRAR_DOC_ANULAR="owner.bd.sgasi.registrar.doc.anular";
	public static final String NOMBRE_PKG_SGASI_REGISTRAR_DOC_ANULAR="nombre.pkg.sgasi.registrar.doc.anular";
	public static final String NOMBRE_SP_SGASI_REGISTRAR_DOC_ANULAR="nombre.sp.sgasi.registrar.doc.anular";	
	public static final String NOMBRE_TM_SGASI_REGISTRAR_DOC_ANULAR="nombre.tm.sgasi.registrar.doc.anular";
	
	public static final String NOMBRE_BD_SGASU_VALIDAR_REG_ANULAR_SGA = "nombre.bd.sgasu.validar.reg.anular.sga";
	public static final String OWNER_BD_SGASU_VALIDAR_REG_ANULAR_SGA = "owner.bd.sgasu.validar.reg.anular.sga";
	public static final String NOMBRE_PKG_SGASU_VALIDAR_REG_ANULAR_SGA = "nombre.pkg.sgasu.validar.reg.anular.sga";
	public static final String NOMBRE_SP_SGASU_VALIDAR_REG_ANULAR_SGA = "nombre.sp.sgasu.validar.reg.anular.sga";
	public static final String NOMBRE_TM_SGASU_VALIDAR_REG_ANULAR_SGA = "nombre.tm.sgasu.validar.reg.anular.sga";
	
	public static final String NOMBRE_BD_SGASS_LISTAR_REG_VALIDAR_OAC = "nombre.bd.sgass.listar.reg.validar.oac";
	public static final String OWNER_BD_SGASS_LISTAR_REG_VALIDAR_OAC = "owner.bd.sgass.listar.reg.validar.oac";
	public static final String NOMBRE_PKG_SGASS_LISTAR_REG_VALIDAR_OAC = "nombre.pkg.sgass.listar.reg.validar.oac";
	public static final String NOMBRE_SP_SGASS_LISTAR_REG_VALIDAR_OAC = "nombre.sp.sgass.listar.reg.validar.oac";
	public static final String NOMBRE_TM_SGASS_LISTAR_REG_VALIDAR_OAC = "nombre.tm.sgass.listar.reg.validar.oac";
	
	public static final String NOMBRE_BD_PR_VAL_REGISTRO = "nombre.bd.pr.val.registro";
	public static final String OWNER_BD_PR_VAL_REGISTRO = "owner.bd.pr.val.registro";
	public static final String NOMBRE_PKG_PR_VAL_REGISTRO = "nombre.pkg.pr.val.registro";
	public static final String NOMBRE_SP_PR_VAL_REGISTRO = "nombre.sp.pr.val.registro";
	public static final String NOMBRE_TM_PR_VAL_REGISTRO = "nombre.tm.pr.val.registro";
	
	public static final String NOMBRE_BD_SGASU_VALIDA_OAC = "nombre.bd.sgasu.valida.oac";
	public static final String OWNER_BD_SGASU_VALIDA_OAC = "owner.bd.sgasu.valida.oac";
	public static final String NOMBRE_PKG_SGASU_VALIDA_OAC = "nombre.pkg.sgasu.valida.oac";
	public static final String NOMBRE_SP_SGASU_VALIDA_OAC = "nombre.sp.sgasu.valida.oac";
	public static final String NOMBRE_TM_SGASU_VALIDA_OAC = "nombre.tm.sgasu.valida.oac";
	
	public static final String NOMBRE_BD_SGASS_LISTAR_REG_ANULAR_OAC = "nombre.bd.sgass.listar.reg.anular.oac";
	public static final String OWNER_BD_SGASS_LISTAR_REG_ANULAR_OAC = "owner.bd.sgass.listar.reg.anular.oac";
	public static final String NOMBRE_PKG_SGASS_LISTAR_REG_ANULAR_OAC = "nombre.pkg.sgass.listar.reg.anular.oac";
	public static final String NOMBRE_SP_SGASS_LISTAR_REG_ANULAR_OAC = "nombre.sp.sgass.listar.reg.anular.oac";
	public static final String NOMBRE_TM_SGASS_LISTAR_REG_ANULAR_OAC = "nombre.tm.sgass.listar.reg.anular.oac";
	
	public static final String NOMBRE_BD_PR_PRC_INTERFACE_AJUSTE = "nombre.bd.pr.prc.interface.ajuste";
	public static final String OWNER_BD_PR_PRC_INTERFACE_AJUSTE = "owner.bd.pr.prc.interface.ajuste";
	public static final String NOMBRE_PKG_PR_PRC_INTERFACE_AJUSTE = "nombre.pkg.pr.prc.interface.ajuste";
	public static final String NOMBRE_SP_PR_PRC_INTERFACE_AJUSTE = "nombre.sp.pr.prc.interface.ajuste";
	public static final String NOMBRE_TM_PR_PRC_INTERFACE_AJUSTE = "nombre.tm.pr.prc.interface.ajuste";
	
	public static final String NOMBRE_BD_SGASU_ANULAR_DOCUMENTO_SGA = "nombre.bd.sgasu.anular.documento.sga";
	public static final String OWNER_BD_SGASU_ANULAR_DOCUMENTO_SGA = "owner.bd.sgasu.anular.documento.sga";
	public static final String NOMBRE_PKG_SGASU_ANULAR_DOCUMENTO_SGA = "nombre.pkg.sgasu.anular.documento.sga";
	public static final String NOMBRE_SP_SGASU_ANULAR_DOCUMENTO_SGA = "nombre.sp.sgasu.anular.documento.sga";
	public static final String NOMBRE_TM_SGASU_ANULAR_DOCUMENTO_SGA = "nombre.tm.sgasu.anular.documento.sga";
	
	public static final String NOMBRE_BD_SGASS_LISTAR_RESULTADOS_REG = "nombre.bd.sgass.listar.resultados.reg";
	public static final String OWNER_BD_SGASS_LISTAR_RESULTADOS_REG = "owner.bd.sgass.listar.resultados.reg";
	public static final String NOMBRE_PKG_SGASS_LISTAR_RESULTADOS_REG = "nombre.pkg.sgass.listar.resultados.reg";
	public static final String NOMBRE_SP_SGASS_LISTAR_RESULTADOS_REG = "nombre.sp.sgass.listar.resultados.reg";
	public static final String NOMBRE_TM_SGASS_LISTAR_RESULTADOS_REG = "nombre.tm.sgass.listar.resultados.reg";
	
	// *** Conexion Actualizar el estado en los ajustes masivos
	public static final String NOMBRE_BD_LISTAPROCESO = "nombre.bd.lista.proceso";
	public static final String OWNER_BD_LISTAPROCESO = "owner.bd.lista.proceso";
	public static final String NOMBRE_PKG_LISTAPROCESO = "nombre.pkg.lista.proceso";
	public static final String NOMBRE_SP_LISTAPROCESO = "nombre.sp.lista.proceso";
	public static final String NOMBRE_TM_LISTAPROCESO = "nombre.tm.lista.proceso";
	
	// *** Conexion Actualizar el estado en los ajustes masivos
	public static final String NOMBRE_BD_ACTUALIZAR_AJUSTE = "nombre.bd.actualizar.ajuste";
	public static final String OWNER_BD_ACTUALIZAR_AJUSTE = "owner.bd.actualizar.ajuste";
	public static final String NOMBRE_PKG_ACTUALIZAR_AJUSTE = "nombre.pkg.actualizar.ajuste";
	public static final String NOMBRE_SP_ACTUALIZAR_AJUSTE = "nombre.sp.actualizar.ajuste";
	public static final String NOMBRE_TM_ACTUALIZAR_AJUSTE = "nombre.tm.actualizar.ajuste";	
	
	
	public static final String VALOR_ANULADO_TIPIFICADO="valor.anulado.tipificado";
	public static final String VALOR_ANULADO_SIN_TIPICAR="valor.anulado.sin.tipicar";
	public static final String LINEA_ANULADAS_TIPICADO_SIOP="linea.anuladas.tipificado.siop";
	public static final String LINEA_ANULADAS_TIPICAR_SINSIOP="linea.anuladas.tipificar.sinsiop";
	public static final String LINEA_ANULADAS_SINTIPICAR_SIOP="linea.anuladas.sintipificar.siop";
	public static final String LINEA_ANULADAS_SINTIPICAR_SINSIOP="linea.anuladas.sintipificar.sinsiop";
	public static final String LINEA_SINANULADAS_SINTIPICAR_SINSIOP="linea.sinanuladas.sintipificar.sinsiop";
	
	public static final String ANULAR_OAC_SGA= "anular.oac.sga";
	// *** Conexion ObtenerLinea
	public static final String NOMBRE_BD_OBTENER = "db.oac.bd";
	public static final String OWNER_BD_OBTENER = "db.oac.owner";
	public static final String NOMBRE_PKG_OBTENER = "db.oac.paquete";
	public static final String NOMBRE_SP_OBTENER = "db.oac.procedure";
	public static final String NOMBRE_TM_OBTENER = "db.oac.tm";
	// -----------------
	
	// *** Parametros de Auditoria - Header Request Transport
	public static final String IDTRANSACCION = "idTransaccion";
	public static final String MSGID = "msgid";
	public static final String USRID = "userId";
	public static final String TIMESTAMP = "timestamp";
	public static final String ACCEPT = "accept";
	public static final String API = "api";

	// -----------------
	public static final String TEXTOVACIO = "";
	public static final String TEXTO_NULL = "NULL";
	public static final String FORMATOFECHADEFAULT = "dd/MM/yyyy HH:mm:ss";

	public static final int CERO = 0;
	public static final int UNO = 1;
	public static final int DOS = 2;
	public static final int TRES = 3;
	public static final int CUATRO = 4;
	public static final int CINCO = 5;
	public static final int SEIS = 6;
	public static final int SIETE = 7;
	public static final int OCHO = 8;
	public static final int NUEVE = 9;
	public static final int DIEZ = 10;
	public static final int ONCE = 11;
	public static final int DOCE = 12;
	public static final int TRECE = 13;
	public static final int CATORCE =14;
	public static final int QUINCE =15;
	public static final int DIECISEIS =16;
	public static final int DIECISIETE=17;
	public static final int DIECIOCHO=18;
	public static final int DIECINUEVE=19;
	public static final int VEINTE = 20;
	public static final int VEINTIUNO = 21;
	public static final int VEINTE_CUATRO = 24;
	public static final String TEXTO_VACIO = "";

	public static final String URLPATTERNS = "/api/*";
	public static final String HTML5CORSFILTER = "HTML5CorsFilter";

	public static final String ACCESSCONTROLALLOWORIGIN = "Access-Control-Allow-Origin";
	public static final String ACCESSCONTROLALLOWMETHODS = "Access-Control-Allow-Methods";
	public static final String ACCESSCONTROLALLOWHEADERS = "Access-Control-Allow-Headers";
	public static final String ASTERISCO = "*";
	public static final String METODOSPERMITIDOS = "GET, POST, DELETE, PUT";
	public static final String CONTENTTYPE = "Content-Type";

	public static final String SWAGGERJAXRSCONFIG = "SwaggerJaxrsConfig";
	public static final String URLSWAGGERJAXRSCONFIG = "/SwaggerJaxrsConfig";

	public static final String PARAMETROS_INPUT = "[PARAMETROS INPUT]";
	// ANULAR
	public static final String PO_AJUSTES_OK = " [PO_AJUSTES_OK] : ";

	// DETALLE
	public static final String PI_ID_PROCESO = " [PI_ID_PROCESO] : ";
	public static final String PI_TIPO_AJUSTE = " [PI_TIPO_AJUSTE] : ";
	public static final String PI_COD_AJUSTE = " [PI_COD_AJUSTE] : ";
	public static final String PI_TRAMA = " [PI_TRAMA] : ";
	public static final String PO_CANT_OK = " [PO_CANT_OK] : ";
	public static final String PO_CANT_ERROR = " [PO_CANT_ERROR] : ";
	public static final String PO_AJUSTES_ERROR = " [PO_AJUSTES_ERROR] : ";
	// INSERT_TRX_PROCESO
	public static final String PI_SIOPTV_DESCRIPCION=" [PI_SIOPTV_DESCRIPCION] : ";
	public static final String PI_SIOPTV_USUARIO=" [PI_SIOPTV_USUARIO] : ";
	public static final String PI_SIOPTD_FECHAEJECUCION=" [PI_SIOPTD_FECHAEJECUCION] : ";
	public static final String PI_SIOPTV_ESTADO=" [PI_SIOPTV_ESTADO] : ";
	public static final String PI_SIOPTV_TIPOTRX=" [PI_SIOPTV_TIPOTRX] : ";
	public static final String PI_SIOPTV_NOMBREARCH=" [PI_SIOPTV_NOMBREARCH] : ";
	public static final String PI_SINO=" [PI_SINO] : ";
	public static final String PI_SIOPTV_ARCH_MODIF=" [PI_SIOPTV_ARCH_MODIF] : ";
	public static final String PI_SIOPTV_TIPO_PROCESO=" [PI_SIOPTV_TIPO_PROCESO] : ";
	public static final String PI_SIOPTV_TIPO_SERVICIO=" [PI_SIOPTV_TIPO_SERVICIO] : ";
	public static final String PI_SIOPTD_MONTO=" [PI_SIOPTD_MONTO] : ";
	public static final String PI_SIOPTV_DETALLE=" [PI_SIOPTV_DETALLE] : ";
	public static final String PI_SIOPTV_GESTION=" [PI_SIOPTV_GESTION] : ";
	
	
	// CABECERA
	public static final String PI_NOMBRE_ARCHIVO = " [PI_NOMBRE_ARCHIVO] : ";
	public static final String PI_CORREO_DESTINO = " [PI_CORREO_DESTINO] : ";
	public static final String PI_USU_TIPIFICACION = " [PI_USU_TIPIFICACION] : ";
	public static final String PI_NOTA_TIPIFICACION = " [PI_NOTA_TIPIFICACION] : ";
	public static final String PI_TIPO_NOTIFICACION = " [PI_TIPO_NOTIFICACION] : ";
	public static final String PI_ENVIAR_POST_FACT = " [PI_ENVIAR_POST_FACT] : ";
	public static final String PI_CODAJUSTE=" [PI_CODAJUSTE] : ";
	public static final String PI_CODAPLICACION=" [PI_CODAPLICACION] : ";
	public static final String PI_TOTALREG=" [PI_TOTALREG] : ";
	public static final String PI_TIPOSPOOLHP=" [PI_TIPOSPOOLHP] : ";
	public static final String PI_RECIBO_RETENIDOS = " [PI_RECIBO_RETENIDOS] : ";
	public static final String PI_ESTADO = " [PI_ESTADO] : ";
	public static final String PI_USUARIO = " [PI_USUARIO] : ";
	public static final String  PO_IDXLS =" [PO_IDXLS] : ";
	public static final String  PI_IDXLS =" [PI_IDXLS] : ";
	public static final String PO_IDPROCESO = " [PO_IDPROCESO] : ";

	// REPORTE - ENCOLARCABECERA
	public static final String PI_FECHA_INI = " [PI_FECHA_INI] : ";
	public static final String PI_FECHA_FIN = " [PI_FECHA_FIN] : ";

	// OBTENER LINEA
	public static final String PI_AJUSTE = " [PI_AJUSTE] : ";
	public static final String PI_CUSTOMER_ID = " [PI_CUSTOMER_ID] : ";
	public static final String PI_LINEA = " [PI_LINEA] : ";
	public static final String PO_LINEA = " [PO_LINEA] : ";

	//VALIDA SGA
	public static final String K_TIPO =" [K_TIPO] : ";
	public static final String K_TRAMA =" [K_TRAMA] : ";
	public static final String K_NUMREG =" [K_NUMREG] : ";
	public static final String K_USUARIO =" [K_USUARIO] : ";
	public static final String K_REGISTRO =" [K_REGISTRO] : ";
	public static final String KO_CANT_OK =" [KO_CANT_OK] : ";
	public static final String KO_CANT_ERR =" [KO_CANT_ERR] : ";
	public static final String KO_TRAMA_OK =" [KO_TRAMA_OK] : ";
	public static final String KO_TRAMA_ERR =" [KO_TRAMA_ERR] : ";
	public static final String KO_REGISTRO =" [KO_REGISTRO] : ";
	public static final String KO_CODIGO =" [KO_CODIGO] : ";
	public static final String KO_MENSAJE =" [KO_MENSAJE] : ";
	public static final String PO_COD_PROCESO=" [PO_COD_PROCESO] : ";

	public static final String K_CORREO = " [K_CORREO] : ";
	public static final String K_CODAPLI = " [K_CODAPLI] : ";
	public static final String K_ESTADO = " [K_ESTADO] : ";
	public static final String K_NOMBRE_ARCHIVO = " [K_NOMBRE_ARCHIVO] : ";
	
	public static final String K_TIPODOC =" [K_TIPODOC] : ";
	public static final String K_TIPIF =" [K_TIPIF] : ";
	public static final String K_TIPIF_DET =" [K_TIPIF_DET] : ";
	
	//lista proceso
	public static final String PPOPROCESO_LISTA ="proceso.lista";
	public static final String PPOPROCESO_LISTA0 ="proceso.lista0";
	public static final String PPOPROCESO_LISTA1 ="proceso.lista1";
	public static final String PPOPROCESO_LISTA2 ="proceso.lista2";
	public static final String PPOPROCESO_LISTA3 ="proceso.lista3";

	public static final String P_TIPOPROCESO =" [P_TIPOPROCESO] : ";
	public static final String P_TIPOSERVICIO =" [P_TIPOSERVICIO] : ";
	public static final String P_ESTADO =" [P_ESTADO] : ";
	public static final String P_ARCHIVO =" [P_ARCHIVO] : ";
	public static final String P_FECHADESDE =" [P_FECHADESDE] : ";
	public static final String P_FECHAHASTA =" [P_FECHAHASTA] : ";
	public static final String PO_CURSOR =" [PO_CURSOR] : ";

	public static final String SIOPTN_CODPROCESO=" [SIOPTN_CODPROCESO] ";
	public static final String TIPO_PROCESO=" [TIPO_PROCESO] ";
	public static final String TIPO_SERVICIO=" [TIPO_SERVICIO] ";
	public static final String ARCHIVO=" [ARCHIVO] ";
	public static final String ESTADO=" [ESTADO] ";
	public static final String SIOPTD_FECHAREGISTRO=" [SIOPTD_FECHAREGISTRO] ";
	public static final String SIOPTV_USUARIO=" [SIOPTV_USUARIO] ";
	public static final String SIOPTV_CANT_REG_OK=" [SIOPTV_CANT_REG_OK] ";
	public static final String SIOPTV_CANT_REG_ERROR=" [SIOPTV_CANT_REG_ERROR] ";
	public static final String SIOPTV_CANT_REGISTROS=" [SIOPTV_CANT_REGISTROS] ";
	
	// SALIDA COMUN
	public static final String PO_CODERROR = " [PO_CODERROR] : ";
	public static final String PO_DESERROR = " [PO_DESERROR] : ";
	
	//ANULAR SGA
	public static final String PV_TRX_ID_WS="PV_TRX_ID_WS";
	public static final String PV_COD_APLICACION="PV_COD_APLICACION";
	public static final String PV_USUARIO_APLIC="PV_USUARIO_APLIC";
	public static final String PV_TIPO_SERVICIO="PV_TIPO_SERVICIO";
	public static final String PV_COD_CUENTA="PV_COD_CUENTA";
	public static final String PV_TIPO_OPERACION="PV_TIPO_OPERACION";
	public static final String PV_TIPO_AJUSTE="PV_TIPO_AJUSTE";
	public static final String PV_NRO_DOC_AJUSTE="PV_NRO_DOC_AJUSTE";
	public static final String PV_DOCS_AJUSTE="PV_DOCS_AJUSTE";
	public static final String PV_ID_RECLAMO_ORIGEN="PV_ID_RECLAMO_ORIGEN";
	public static final String PV_MONEDA_AJUSTE="PV_MONEDA_AJUSTE";
	public static final String PN_MONTO_AJUSTE="PN_MONTO_AJUSTE";
	public static final String PN_SALDO_AJUSTE="PN_SALDO_AJUSTE";
	public static final String PV_ESTADO="PV_ESTADO";
	public static final String PV_COD_MOTIVO_AJUSTE="PV_COD_MOTIVO_AJUSTE";
	public static final String PV_DESCRIP_AJUSTE="PV_DESCRIP_AJUSTE";
	public static final String PD_FECHA_AJUSTE="PD_FECHA_AJUSTE";
	public static final String PD_FECHA_VENC_AJUSTE="PD_FECHA_VENC_AJUSTE";
	public static final String PD_FECHA_CANCELACION="PD_FECHA_CANCELACION";
	public static final String XV_STATUS="XV_STATUS";
	public static final String XV_MESSAGE="XV_MESSAGE";
	public static final String KO_CURSOR="KO_CURSOR";
	public static final String KO_REGISTRO_OK="KO_REGISTRO_OK";
	public static final String KO_REGISTRO_ERROR="KO_REGISTRO_ERROR";
	public static final String K_AMADV_DOCUMENTO="K_AMADV_DOCUMENTO";
	public static final String K_CODIGO="K_CODIGO";
	
	public static final String PARAMETRO_INICIO_METODO = "============ {} [INICIO de metodo {}] ============ ";
	public static final String PARAMETRO_FIN_METODO = "============ {} [FIN de metodo {}] ============ ";
	public static final String METODO_ENCOLARDETALLE = "encolarDetalle";
	public static final String METODO_ENCOLARCABECERA = "encolarCabecera";
	public static final String METODO_CONSULTARDOCSUNATSIOP = "consultarDocSunatSIOP";
	public static final String METODO_OBTENERLINEA = "obtenerLinea";
	public static final String METODO_ANULAR = "anular";
	public static final String METODO_LISTAPROCESO = "listaProceso";
	public static final String METODO_ACTUALIZAR_AJUSTE = "actualizarAjuste";
	public static final String METODO_ENVIAR_CORREO = "enviarCorreo";
	public static final String METODO_NUEVA_INTERACCION = "nuevaInteraccion";
	public static final String METODO_ENVIAR_BSCS = "envioBscs";
	public static final String PARAMETROS_OUTPUT = "[PARAMETROS OUTPUT]";
	public static final String REPORTE_SUNAT_DB_OK = "0";
	public static final String PO_CUR_SALIDA_CANTIDAD = "[PO_CUR_SALIDA_CANTIDAD]: ";

	public static final String VALOR_0 = "0";
	public static final String ERROR_JDBC_TIMEOUT = "timeOut";
	public static final String CODIGO_IDT1 = "codigo.idt1";
	public static final String CODIGO_IDT2 = "codigo.idt2";
	public static final String CODIGO_IDT3 = "codigo.idt3";
	public static final String MSG_IDT1 = "mensaje.idt1";
	public static final String MSG_IDT2 = "mensaje.idt2";
	public static final String MSG_IDT3 = "mensaje.idt3";

	public static final String CODIGO_IDT4 = "codigo.idt4";
	public static final String MSG_IDT4 = "mensaje.idt4";

	public static final String CODIGO_IDT5 = "codigo.idt5";
	public static final String MSG_IDT5 = "mensaje.idt5";
	
	public static final String DESCRIPCIONRESOURCE = "Ajustes masivos";

	public static final String DESCRIPCION_METODO_DETALLE = "Obtiene la detalle de una trama";
	public static final String NOTAS_METODO_DETALLE = "Detalle de ajuste masivos";

	public static final String DESCRIPCION_METODO_CABECERA = "Obtiene la cabecera de una trama";
	public static final String NOTAS_METODO_CABECERA = "Cabecera de ajuste masivos";

	public static final String DESCRIPCION_METODO_LISTAPROCESO = "Obtiene la lista de procesos de ajustes";
	public static final String NOTAS_METODO_LISTAPROCESO = "Lista de procesos de ajustes masivos";
	
	public static final String DESCRIPCION_METODO_CONSULTARDOCSUNATSIOP = "Consultar documentos de SUNAT";
	public static final String NOTAS_METODO_CONSULTARDOCSUNATSIOP = "Documentos SUNAT de SIOP";

	public static final String ID = "id";
	public static final String CODIGO_IDF0 = "codigo.idf0";
	public static final String MSJ_IDF0 = "mensaje.idf0";
	public static final String MENSAJE_IDF0 = "Operacion Exitosa";
	public static final String MENSAJE_IDF1 = "No se realiz� la operacion o correctamente";
	
	public static final String MENSAJE_IDT1 = "Error de Timeout";

	public static final String CORCHETE_IZQUIERDO = "[";
	public static final String CORCHETE_DERECHO = "]";
	public static final String DOS_PUNTOS = " : ";
	public static final String CODIGO_IDF2 = "codigo.idf2";
	public static final String MENSAJE_IDF2 = "mensaje.idf2";

	public static final String ERROR_INTERNO = "Errro Interno";

	public static final String BSCS="ws.bscs";
	public static final String SGA="ws.sga";	
	public static final String ENCOLARDETALLE_SGA="encolardetalle.sga";
	public static final String ENCOLARCABECERA_SGA="encolarcabecera.sga";
	public static final String ENCOLARDETALLE_SGA_V="encolardetalle.sga_v";
	public static final String ENCOLARCABECERA_SGA_V="encolarcabecera.sga_v";
	
	public static final String ENCOLARCABECERA_BSCS1="encolarcabecera.bscs1";
	public static final String ENCOLARCABECERA_BSCS2="encolarcabecera.bscs2";
	public static final String ENCOLARCABECERA_BSCS3="encolarcabecera.bscs3";
	public static final String ENCOLARCABECERA_BSCS4="encolarcabecera.bscs4";
	public static final String ENCOLARCABECERA_BSCS5="encolarcabecera.bscs5";
	public static final String ENCOLARCABECERA_BSCS6="encolarcabecera.bscs6";
	public static final String ENCOLARCABECERA_BSCS7="encolarcabecera.bscs7";
	public static final String ENCOLARCABECERA_BSCS8="encolarcabecera.bscs8";
	public static final String ENCOLARDETALLE_BSCS1="encolardetalle.bscs1";
	public static final String ENCOLARDETALLE_BSCS2="encolardetalle.bscs2";
	public static final String ENCOLARDETALLE_BSCS3="encolardetalle.bscs3";
	public static final String ENCOLARDETALLE_BSCS4="encolardetalle.bscs4";
	public static final String ENCOLARDETALLE_BSCS5="encolardetalle.bscs5";
	public static final String ENCOLARDETALLE_BSCS6="encolardetalle.bscs6";
	public static final String ENCOLARDETALLE_BSCS7="encolardetalle.bscs7";
	public static final String ENCOLARDETALLE_BSCS8="encolardetalle.bscs8";
	
	public static final String PO_COD_OK ="OK";
	public static final String PO_COD_ERROR = "ERROR";
	public static final String PO_COD_ERROR_V = "V";
	public static final String ANULADO_E="E";
	public static final String TIPIFICADO_P = "P";
	public static final String ANULADO="A";
	public static final String ANULADO_TIPIFICADO = "AT";
	public static final String TIPIFICADO = "T";
	public static final String TIPIFICADO_ERROR = "TE";
	public static final String SIN_ANULAR = "E";
	public static final String WS_REINTENTOS_ENPOINTADDRESS = "ws.reintentos.enpointAddress";
	public static final String TRAMA_AJUSTE = "trama.ajuste";
	public static final String TRAMA_NUMERO_AJUSTE = "trama.numero.ajuste";
	public static final String TRAMA_CODIGO = "trama.codigo";
	public static final String TRAMA_AJUSTE_NUMERO = "trama.ajuste.numero";
	public static final String TRAMA_AJUSTE_DESCRIPCION = "trama.ajuste.descripcion";
	
	public static final String PRO_PI_SIOPTV_DESCRIPCION="pi.ioptv.descripcion";
    public static final String PRO_PI_SIOPTV_ESTADO="pi.sioptv.estado";
    public static final String PRO_PI_SIOPTV_TIPOTRX="pi.sioptv.tipotrx";
    public static final String PRO_PI_SINO="pi.sino";
    public static final String PRO_PI_SIOPTV_TIPO_PROCESO="pi.sioptv.tipo.proceso";
    public static final String PRO_PI_SIOPTV_TIPO_SERVICIO="pi.sioptv.tipo.servicio";
    
    public static final String LINEA_ANULADAS_TIPIFICADA_CLASE="linea.anuladas.tipificada.clase";
    public static final String LINEA_ANULADAS_TIPIFICADA_CODIGOEMPLEADO="linea.anuladas.tipificada.codigoempleado";
    public static final String LINEA_ANULADAS_TIPIFICADA_CODIGOSISTEMA="linea.anuladas.tipificada.codigosistema";
    public static final String LINEA_ANULADAS_TIPIFICADA_FLAGCASO="linea.anuladas.tipificada.flagcaso";
    public static final String LINEA_ANULADAS_TIPIFICADA_HECHOENUNO="linea.anuladas.tipificada.hechoenuno";
    public static final String LINEA_ANULADAS_TIPIFICADA_METODOCONTACTO="linea.anuladas.tipificada.metodocontacto";
    public static final String LINEA_ANULADAS_TIPIFICADA_SUBCLASE="linea.anuladas.tipificada.subclase";
    public static final String LINEA_ANULADAS_TIPIFICADA_TEXTRESULTADO="linea.anuladas.tipificada.textResultado";
    public static final String LINEA_ANULADAS_TIPIFICADA_TIPO="linea.anuladas.tipificada.tipo";
    public static final String LINEA_ANULADAS_TIPIFICADA_TIPOINTERACCION="linea.anuladas.tipificada.tipoInteraccion";
    public static final String LINEA_ANULADAS_TIPIFICADA0="linea.anuladas.tipificada0";
    public static final String LINEA_ANULADAS_TIPIFICADA1="linea.anuladas.tipificada1";
    public static final String LINEA_ANULADAS_TIPIFICADA2="linea.anuladas.tipificada2";
    
    public static final String ENCOLARCABECERA_SGA_NC = "encolarcabecera.sga.nc";
    public static final String ENCOLARCABECERA_SGA_ABONO ="encolarcabecera.sga.abono";
    public static final String ENCOLARCABECERA_SGA_ND = "encolarcabecera.sga.nd";
    public static final String ENCOLARCABECERA_SGA_CARGO ="encolarcabecera.sga.cargo";
    public static final String ENCOLARCABECERA_SGA_NC1 = "encolarcabecera.sga.nc1";
    public static final String ENCOLARCABECERA_SGA_ND1 = "encolarcabecera.sga.nd1";
    
    public static final String ENCOLARCABECERA_SGA_REGISTRANDO = "encolarcabecera.sga.registrando";
    public static final String ENCOLARCABECERA_SGA_REGISTRANDO_V = "encolarcabecera.sga.registrando_v";
}
