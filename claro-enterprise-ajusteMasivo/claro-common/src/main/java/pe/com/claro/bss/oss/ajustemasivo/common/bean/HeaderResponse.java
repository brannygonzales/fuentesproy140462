package pe.com.claro.bss.oss.ajustemasivo.common.bean;

public class HeaderResponse {
	
	private String idTransaccion;
	
	public String getIdTransaccion() {
		return idTransaccion;
	}
	public void setIdTransaccion(String idTransaccion) {
		this.idTransaccion = idTransaccion;
	}
	
}
