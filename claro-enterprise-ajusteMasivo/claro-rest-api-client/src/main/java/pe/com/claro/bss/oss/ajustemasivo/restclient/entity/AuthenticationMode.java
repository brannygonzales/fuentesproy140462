package pe.com.claro.bss.oss.ajustemasivo.restclient.entity;

public enum AuthenticationMode {
	BASIC_AUTH, SHARED_SECRET_KEY
}
