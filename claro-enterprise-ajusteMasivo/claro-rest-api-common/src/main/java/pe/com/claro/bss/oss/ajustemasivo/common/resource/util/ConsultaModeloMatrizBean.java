package pe.com.claro.bss.oss.ajustemasivo.common.resource.util;

public class ConsultaModeloMatrizBean {
	
	private String codigoModeloMatriz;

	private String codigoModelo;
	private String codigoMaestra;
	private String descripcionEstado;
	private String descripcionEquipo;
	
	public String getCodigoModeloMatriz() {
		return codigoModeloMatriz;
	}
	public void setCodigoModeloMatriz(String codigoModeloMatriz) {
		this.codigoModeloMatriz = codigoModeloMatriz;
	}

	public String getCodigoModelo() {
		return codigoModelo;
	}
	public void setCodigoModelo(String codigoModelo) {
		this.codigoModelo = codigoModelo;
	}
	public String getCodigoMaestra() {
		return codigoMaestra;
	}
	public void setCodigoMaestra(String codigoMaestra) {
		this.codigoMaestra = codigoMaestra;
	}
	public String getDescripcionEstado() {
		return descripcionEstado;
	}
	public void setDescripcionEstado(String descripcionEstado) {
		this.descripcionEstado = descripcionEstado;
	}
	public String getDescripcionEquipo() {
		return descripcionEquipo;
	}
	public void setDescripcionEquipo(String descripcionEquipo) {
		this.descripcionEquipo = descripcionEquipo;
	}

}
