package pe.com.claro.bss.oss.ajustemasivo.common.resource.util;

public class ConsultaTrazabilidadBean {

	private String tipoDocumento;
	private String numeroDocumento;
	private String flagConsulta;
	private String codigoMaestra;
	private String codigoEquipo;
	private String fechaInicial;
	private String fechaFinal;
	private String usuarioRegistro;
	
	private String pCodigoIdentificador;
		 
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public String getFlagConsulta() {
		return flagConsulta;
	}
	public void setFlagConsulta(String flagConsulta) {
		this.flagConsulta = flagConsulta;
	}
	public String getCodigoMaestra() {
		return codigoMaestra;
	}
	public void setCodigoMaestra(String codigoMaestra) {
		this.codigoMaestra = codigoMaestra;
	}
	public String getCodigoEquipo() {
		return codigoEquipo;
	}
	public void setCodigoEquipo(String codigoEquipo) {
		this.codigoEquipo = codigoEquipo;
	}
	public String getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public String getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	public String getUsuarioRegistro() {
		return usuarioRegistro;
	}
	public void setUsuarioRegistro(String usuarioRegistro) {
		this.usuarioRegistro = usuarioRegistro;
	}
	public String getpCodigoIdentificador() {
		return pCodigoIdentificador;
	}
	public void setpCodigoIdentificador(String pCodigoIdentificador) {
		this.pCodigoIdentificador = pCodigoIdentificador;
	}
	
	
	
}
