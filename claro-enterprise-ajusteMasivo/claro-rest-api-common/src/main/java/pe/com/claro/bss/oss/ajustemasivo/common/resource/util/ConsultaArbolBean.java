package pe.com.claro.bss.oss.ajustemasivo.common.resource.util; 

public class ConsultaArbolBean {

	private String codigoMaestra;
	private String codigoArbol;
	private String descripcionArbol;
	
	public String getCodigoMaestra() {
		return codigoMaestra;
	}
	public void setCodigoMaestra(String codigoMaestra) {
		this.codigoMaestra = codigoMaestra;
	}
	public String getCodigoArbol() {
		return codigoArbol;
	}
	public void setCodigoArbol(String codigoArbol) {
		this.codigoArbol = codigoArbol;
	}
	public String getDescripcionArbol() {
		return descripcionArbol;
	}
	public void setDescripcionArbol(String descripcionArbol) {
		this.descripcionArbol = descripcionArbol;
	}
	
	 
	
}
