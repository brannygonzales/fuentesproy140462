package pe.com.claro.bss.oss.ajustemasivo.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "codigoEstilo")
public class Estilos implements Serializable {

	
	private static final long serialVersionUID = 1L;
	private int codigoEstilo;
	private int codigoControl;
	private String descripcionEstilo;
	private String descripcionEstado;
	
	@Id
	@Column(name="codigoEstilo")
	public int getCodigoEstilo() {
		return codigoEstilo;
	}

	public void setCodigoEstilo(int codigoEstilo) {
		this.codigoEstilo = codigoEstilo;
	}

	public String getDescripcionEstilo() {
		return descripcionEstilo;
	}

	public void setDescripcionEstilo(String descripcionEstilo) {
		this.descripcionEstilo = descripcionEstilo;
	}

	public int getCodigoControl() {
		return codigoControl;
	}

	public void setCodigoControl(int codigoControl) {
		this.codigoControl = codigoControl;
	}

	public String getDescripcionEstado() {
		return descripcionEstado;
	}

	public void setDescripcionEstado(String descripcionEstado) {
		this.descripcionEstado = descripcionEstado;
	}	

}
