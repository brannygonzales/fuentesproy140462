package pe.com.claro.bss.oss.ajustemasivo.model;


import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "codigoControl")
public class Control implements Serializable {

	private static final long serialVersionUID = 1L;

	private int codigoControl;
	private String descripcionControl;
	private int ordenControl;
	private String tipoControl;
	private int codigoFormulario;
	private String descripcionEstado;
	private List<Estilos> listaEstilos;

	@Id
	@Column(name = "codigoControl")
	public int getCodigoControl() {
		return codigoControl;
	}

	public void setCodigoControl(int codigoControl) {
		this.codigoControl = codigoControl;
	}

	public String getDescripcionControl() {
		return descripcionControl;
	}

	public void setDescripcionControl(String descripcionControl) {
		this.descripcionControl = descripcionControl;
	}

	public int getOrdenControl() {
		return ordenControl;
	}

	public void setOrdenControl(int ordenControl) {
		this.ordenControl = ordenControl;
	}

	 

	public int getCodigoFormulario() {
		return codigoFormulario;
	}

	public void setCodigoFormulario(int codigoFormulario) {
		this.codigoFormulario = codigoFormulario;
	}

	public String getDescripcionEstado() {
		return descripcionEstado;
	}

	public void setDescripcionEstado(String descripcionEstado) {
		this.descripcionEstado = descripcionEstado;
	}

	@Transient
	public List<Estilos> getListaEstilos() {
		return listaEstilos;
	}

	public void setListaEstilos(List<Estilos> listaEstilos) {
		this.listaEstilos = listaEstilos;
	}

	public String getTipoControl() {
		return tipoControl;
	}

	public void setTipoControl(String tipoControl) {
		this.tipoControl = tipoControl;
	}
	

}
