package pe.com.claro.bss.oss.ajustemasivo.resource.util;

import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.ws.rs.core.HttpHeaders;

import pe.com.claro.bss.oss.ajustemasivo.common.property.Constantes;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.util.HeaderRequestBean;


public class ValidadorUtil{
	
	public static final Set<ConstraintViolation<HeaderRequestBean>> validarParametrosObligatoriosHeader( HttpHeaders httpHeaders ){
		HeaderRequestBean header = new HeaderRequestBean();
		if(httpHeaders.getRequestHeader(Constantes.ACCEPT)!=null) header.setAccept( httpHeaders.getRequestHeader(Constantes.ACCEPT).get( 0 ));
		if(httpHeaders.getRequestHeader(Constantes.IDTRANSACCION)!=null) header.setIdTransaccion( httpHeaders.getRequestHeader(Constantes.IDTRANSACCION).get( 0 ));
		if(httpHeaders.getRequestHeader(Constantes.MSGID)!=null) header.setMsgid(httpHeaders.getRequestHeader(Constantes.MSGID).get( 0 ));
		if(httpHeaders.getRequestHeader(Constantes.TIMESTAMP)!=null) header.setTimestamp( httpHeaders.getRequestHeader(Constantes.TIMESTAMP).get( 0 ));
		if(httpHeaders.getRequestHeader(Constantes.USRID)!=null) header.setUserId( httpHeaders.getRequestHeader(Constantes.USRID).get( 0 ));
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		return validator.validate(header);
	}
}
