package pe.com.claro.bss.oss.ajustemasivo.resource;

import java.text.SimpleDateFormat;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.validation.ConstraintViolation;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

import pe.com.claro.bss.oss.ajustemasivo.canonical.request.AnulacionAjusteRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.request.DocEnviosSunatRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.request.EncolaCabRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.request.EncolarDetalleRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.request.ListaProcesoRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.EncolaCabResponse;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.EncolarDetalleResponse;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.ListaProcesoResponse;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.AnulacionAjusteResponse;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.DocEnviosSunatResponse;
import pe.com.claro.bss.oss.ajustemasivo.common.bean.HeaderRequest;
import pe.com.claro.bss.oss.ajustemasivo.common.property.ClaroUtil;
import pe.com.claro.bss.oss.ajustemasivo.common.property.Constantes;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.DBException;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.NotFoundException;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.WSException;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.util.HeaderRequestBean;
import pe.com.claro.bss.oss.ajustemasivo.domain.service.AnularAjusteService;
import pe.com.claro.bss.oss.ajustemasivo.domain.service.EconlaCabeceraService;
import pe.com.claro.bss.oss.ajustemasivo.domain.service.EncolarDetalleService;
import pe.com.claro.bss.oss.ajustemasivo.domain.service.EnvioSunatService;
import pe.com.claro.bss.oss.ajustemasivo.domain.service.ListaProcesoService;
import pe.com.claro.bss.oss.ajustemasivo.resource.util.ApplicationConfig;
import pe.com.claro.bss.oss.ajustemasivo.resource.util.ValidadorUtil;


@Stateless
@Path(Constantes.PATH)
@Api(value = Constantes.RESOURCE, description = Constantes.DESCRIPCIONRESOURCE)
@Produces({ MediaType.APPLICATION_JSON })
public class GestionaRecaudacionResource {

	private static final Logger LOG = LoggerFactory.getLogger(GestionaRecaudacionResource.class);


	@EJB
	private EncolarDetalleService serviceDetalle;
	
	@EJB
	private EconlaCabeceraService serviceCabecera;
	
	@EJB
	private EnvioSunatService envioSunatService;
	
	 @EJB
	 private AnularAjusteService anulacionAjusteService;
	 
	 @EJB
	 private ListaProcesoService listaProcesoService;
	 
	private ApplicationConfig appConfig = new ApplicationConfig();
	
	@POST
	@Path(Constantes.NOMBRE_METODO_DETALLE)
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = Constantes.DESCRIPCION_METODO_DETALLE, notes = Constantes.NOTAS_METODO_DETALLE, response = EncolarDetalleResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = Constantes.MENSAJE_IDF0),
			@ApiResponse(code = 200, message = Constantes.MENSAJE_IDF1),
			@ApiResponse(code = 400, message = Constantes.MENSAJE_IDT1) })
	public Response encolarDetalle(@Context HttpHeaders httpHeaders,
			@ApiParam(value = "El objeto EncolaCabResponse", required = true) EncolarDetalleRequest request)
			throws NotFoundException, Exception {
		HeaderRequest headerRequest = new HeaderRequest(httpHeaders);
		String idTransaccion = headerRequest.getIdTransaccion();
		
		long tiempoInicio = System.currentTimeMillis();
		String nombreMetodo = Constantes.METODO_ENCOLARDETALLE;
		String trazabilidad = "[" + nombreMetodo + " idTx=" + idTransaccion + "] ";
		String result = null;

		EncolarDetalleResponse response= new EncolarDetalleResponse();
		
		String usuarioReg = null;
		String headerPrint = ClaroUtil.printPrettyJSONString(headerRequest);
		LOG.info(trazabilidad + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ENCOLARDETALLE);
		LOG.info(trazabilidad + "HeaderRequest Recibido: \n{}" + headerPrint);

		String responsePrint = null;
		try {
			String trazabilidadNoNull = ClaroUtil.getHttpHeadersNoNull(httpHeaders);
			org.apache.log4j.MDC.put("dataAudit", trazabilidadNoNull);
			LOG.info(trazabilidad + "[Actividad 1. Validar Parametros Obligatorios]");
			Set<ConstraintViolation<HeaderRequestBean>> constraintViolations = ValidadorUtil
					.validarParametrosObligatoriosHeader(httpHeaders);
			boolean datosValidos = constraintViolations.isEmpty();

			usuarioReg = "" + httpHeaders.getRequestHeader("userId") + "";
			usuarioReg = this.limpiarValor(usuarioReg);
			
			String requestPrint = ClaroUtil.printPrettyJSONString(request);
			LOG.info(trazabilidad + "**** Body Request: ....  \n{}", requestPrint);
			if (!datosValidos) {

				String camposNoOk = getCamposNoOK(constraintViolations);
				
				response.setPoCoderror(
						appConfig.getProperties().get(Constantes.CODIGO_IDF2).toString());
				response.setPoDeserror(
						appConfig.getProperties().get(Constantes.MENSAJE_IDF2).toString() + camposNoOk);

			} else {								
				LOG.info(trazabilidad + " Validando campos ");
				usuarioReg = "" + httpHeaders.getRequestHeader("userId") + "";
				usuarioReg = this.limpiarValor(usuarioReg);
				
				LOG.info(trazabilidad + "[Actividad 2. Obtener Detalle SUNAT]");
				response=serviceDetalle.encolarDetalle(trazabilidad, request);
				responsePrint = ClaroUtil.printPrettyJSONString(response);
				if(response.getPoCoderror().equalsIgnoreCase(String.valueOf(Constantes.CERO))){
					response.setPoCoderror(String.valueOf(appConfig.getProperties().get(Constantes.CODIGO_IDF0)));
					response.setPoDeserror(String.valueOf(appConfig.getProperties().get(Constantes.MSJ_IDF0)));	
				}
			}
		}catch (DBException e) {
			response.setPoCoderror(e.getCodigoError() );
			response.setPoDeserror(e.getMessage());			
		}catch (WSException e) {			
			response.setPoCoderror(e.getCodigoError());
			response.setPoDeserror(e.getMessage());
		}
		catch (Exception ex) {
			LOG.error(trazabilidad + " Error Generico Generado : ", ex.getMessage());
			response.setPoCoderror(appConfig.getProperties().get(Constantes.CODIGO_IDT4).toString());
			response.setPoDeserror(appConfig.getProperties().get(Constantes.MSG_IDT4).toString());

		} finally {
			result = new ObjectMapper().setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"))
					.writerWithDefaultPrettyPrinter().writeValueAsString(response);
			LOG.info(trazabilidad + "Response General:\n{}", responsePrint);
			LOG.info(trazabilidad + "Tiempo total de proceso(ms): {} milisegundos.", System.currentTimeMillis() - tiempoInicio);
			LOG.info(trazabilidad + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,nombreMetodo);
			
	}
		return Response.ok().entity(result).build();
	}
	
	
	@POST
	@Path(Constantes.NOMBRE_METODO_CABECERA)
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = Constantes.DESCRIPCION_METODO_CABECERA, notes = Constantes.NOTAS_METODO_CABECERA, response = EncolaCabResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = Constantes.MENSAJE_IDF0),
			@ApiResponse(code = 200, message = Constantes.MENSAJE_IDF1),
			@ApiResponse(code = 400, message = Constantes.MENSAJE_IDT1) })
	public Response encolarCabecera(@Context HttpHeaders httpHeaders,
			@ApiParam(value = "El objeto EncolaCabResponse", required = true) EncolaCabRequest request)
			throws NotFoundException, Exception {
		HeaderRequest headerRequest = new HeaderRequest(httpHeaders);
		String idTransaccion = headerRequest.getIdTransaccion();
		
		long tiempoInicio = System.currentTimeMillis();
		String nombreMetodo = Constantes.METODO_ENCOLARCABECERA;
		String trazabilidad = "[" + nombreMetodo + " idTx=" + idTransaccion + "] ";
		String result = null;

		EncolaCabResponse response= new EncolaCabResponse();
		
		String usuarioReg = null;
		String headerPrint = ClaroUtil.printPrettyJSONString(headerRequest);
		LOG.info(trazabilidad + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ENCOLARCABECERA);
		LOG.info(trazabilidad + "HeaderRequest Recibido: \n{}" + headerPrint);

		String responsePrint = null;
		try {
			String trazabilidadNoNull = ClaroUtil.getHttpHeadersNoNull(httpHeaders);
			org.apache.log4j.MDC.put("dataAudit", trazabilidadNoNull);
			LOG.info(trazabilidad + "[Actividad 1. Validar Parametros Obligatorios]");
			Set<ConstraintViolation<HeaderRequestBean>> constraintViolations = ValidadorUtil
					.validarParametrosObligatoriosHeader(httpHeaders);
			boolean datosValidos = constraintViolations.isEmpty();

			usuarioReg = "" + httpHeaders.getRequestHeader("userId") + "";
			usuarioReg = this.limpiarValor(usuarioReg);
			
			String requestPrint = ClaroUtil.printPrettyJSONString(request);
			LOG.info(trazabilidad + "**** Body Request: ....  \n{}", requestPrint);
			if (!datosValidos) {

				String camposNoOk = getCamposNoOK(constraintViolations);
				
				response.setPoCoderror(
						appConfig.getProperties().get(Constantes.CODIGO_IDF2).toString());
				response.setPoDeserror(
						appConfig.getProperties().get(Constantes.MENSAJE_IDF2).toString() + camposNoOk);

			} else {
				LOG.info(trazabilidad + " Validando campos ");
				usuarioReg = "" + httpHeaders.getRequestHeader("userId") + "";
				usuarioReg = this.limpiarValor(usuarioReg);
				
				LOG.info(trazabilidad + "[Actividad 2. Obtener cabecera SUNAT]");
				response=serviceCabecera.encolarCabecera(trazabilidad, request);
				responsePrint = ClaroUtil.printPrettyJSONString(response);
				if(response.getPoCoderror().equalsIgnoreCase(String.valueOf(Constantes.CERO))){
					response.setPoCoderror(String.valueOf(appConfig.getProperties().get(Constantes.CODIGO_IDF0)));
					response.setPoDeserror(String.valueOf(appConfig.getProperties().get(Constantes.MSJ_IDF0)));	
				}
			}
		}catch (DBException e) {
			response.setPoCoderror(e.getCodigoError() );
			response.setPoDeserror(e.getMessage());			
		}catch (WSException e) {			
			response.setPoCoderror(e.getCodigoError());
			response.setPoDeserror(e.getMessage());
		}
		catch (Exception ex) {
			LOG.error(trazabilidad + " Error Generico Generado : ", ex.getMessage());
			response.setPoCoderror(appConfig.getProperties().get(Constantes.CODIGO_IDT4).toString());
			response.setPoDeserror(appConfig.getProperties().get(Constantes.MSG_IDT4).toString());

		} finally {
			result = new ObjectMapper().setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"))
					.writerWithDefaultPrettyPrinter().writeValueAsString(response);
			LOG.info(trazabilidad + "Response General:\n{}", responsePrint);
			LOG.info(trazabilidad + "Tiempo total de proceso(ms): {} milisegundos.", System.currentTimeMillis() - tiempoInicio);
			LOG.info(trazabilidad + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,nombreMetodo);
	}
		return Response.ok().entity(result).build();
	}
	
	
	@POST
	@Path(Constantes.NOMBRE_METODO_CONSULTARDOCSUNATSIOP)
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = Constantes.DESCRIPCION_METODO_CONSULTARDOCSUNATSIOP, notes = Constantes.NOTAS_METODO_CONSULTARDOCSUNATSIOP, response = DocEnviosSunatResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = Constantes.MENSAJE_IDF0),
			@ApiResponse(code = 200, message = Constantes.MENSAJE_IDF1),
			@ApiResponse(code = 400, message = Constantes.MENSAJE_IDT1) })
	public Response consultarDocSunatSIOP(@Context HttpHeaders httpHeaders,
			@ApiParam(value = "El objeto ReporteSunatResponse", required = true) DocEnviosSunatRequest request)
			throws NotFoundException, Exception {
		HeaderRequest headerRequest = new HeaderRequest(httpHeaders);
		String idTransaccion = headerRequest.getIdTransaccion();
		
		long tiempoInicio = System.currentTimeMillis();
		String nombreMetodo = Constantes.METODO_CONSULTARDOCSUNATSIOP;
		String trazabilidad = "[" + nombreMetodo + " idTx=" + idTransaccion + "] ";
		String result = null;

		DocEnviosSunatResponse response= new DocEnviosSunatResponse();
		
		String usuarioReg = null;
		String headerPrint = ClaroUtil.printPrettyJSONString(headerRequest);
		LOG.info(trazabilidad + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,nombreMetodo);
		LOG.info(trazabilidad + "HeaderRequest Recibido: \n{}" + headerPrint);

		String responsePrint = null;
		
		try {
			String trazabilidadNoNull = ClaroUtil.getHttpHeadersNoNull(httpHeaders);
			org.apache.log4j.MDC.put("dataAudit", trazabilidadNoNull);
			LOG.info(trazabilidad + "[Actividad 1. Validar Parametros Obligatorios]");
			Set<ConstraintViolation<HeaderRequestBean>> constraintViolations = ValidadorUtil
					.validarParametrosObligatoriosHeader(httpHeaders);
			boolean datosValidos = constraintViolations.isEmpty();

			usuarioReg = "" + httpHeaders.getRequestHeader("userId") + "";
			usuarioReg = this.limpiarValor(usuarioReg);
			
			String requestPrint = ClaroUtil.printPrettyJSONString(request);
			LOG.info(trazabilidad + "**** Body Request: ....  \n{}", requestPrint);
			
			if (!datosValidos) {

				String camposNoOk = getCamposNoOK(constraintViolations);
				response.setDesError(
						appConfig.getProperties().get(Constantes.MENSAJE_IDF2).toString() + camposNoOk);
				response.setCodError(
						appConfig.getProperties().get(Constantes.CODIGO_IDF2).toString());
			} else {
				LOG.info(trazabilidad + " Validando campos ");
				usuarioReg = "" + httpHeaders.getRequestHeader("userId") + "";
				usuarioReg = this.limpiarValor(usuarioReg);

				LOG.info(trazabilidad + "[Actividad 2. Obtener documentos SUNAT]");
				if(request.getPiFechaIni()!=null || request.getPiFechaFin()!=null) {	
		response= envioSunatService.consultarDocSunatSIOP(trazabilidad, request.getPiFechaIni(),request.getPiFechaFin());
	responsePrint = ClaroUtil.printPrettyJSONString(response);
	LOG.info(":::::::::::::::::::::::::\n "+response.getCodError());
	if(response.getCodError().equalsIgnoreCase(String.valueOf(Constantes.CERO))){
		response.setCodError(String.valueOf(appConfig.getProperties().get(Constantes.CODIGO_IDF0)));
		response.setDesError(String.valueOf(appConfig.getProperties().get(Constantes.MSJ_IDF0)));	
	}
}

			}
		}catch (DBException e) {
			response.setCodError(e.getCodigoError() );
			response.setDesError(e.getMessage());				
		}catch (WSException e) {			
			response.setCodError(e.getCodigoError());
			response.setDesError(e.getMessage());
		}
		catch (Exception ex) {
			LOG.error(trazabilidad + " Error Generico Generado : ", ex.getMessage());
			response.setCodError(appConfig.getProperties().get(Constantes.CODIGO_IDT4).toString());
			response.setDesError(appConfig.getProperties().get(Constantes.MSG_IDT4).toString());

		} finally {
			result = new ObjectMapper().setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"))
					.writerWithDefaultPrettyPrinter().writeValueAsString(response);
			LOG.info(trazabilidad + "Response General:\n{}", responsePrint);
			LOG.info(trazabilidad + "Tiempo total de proceso(ms): {} milisegundos.", System.currentTimeMillis() - tiempoInicio);
			LOG.info(trazabilidad + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,nombreMetodo);
	}
		return Response.ok().entity(result).build();

	}
	
	
	
	
	@POST
	  @Path(Constantes.NOMBRE_METODO_ANULAR)
	  @Consumes({ MediaType.APPLICATION_JSON })
	  @Produces({ MediaType.APPLICATION_JSON })
	  @ApiOperation(value="Anular Ajuste SUNAT", notes="Anular Ajuste SUNAT", response=AnulacionAjusteResponse.class)
	  @ApiResponses({@com.wordnik.swagger.annotations.ApiResponse(code=200, message=Constantes.MENSAJE_IDF0),
		  @com.wordnik.swagger.annotations.ApiResponse(code=200, message=Constantes.MENSAJE_IDF1), 
		  @com.wordnik.swagger.annotations.ApiResponse(code=400, message=Constantes.MENSAJE_IDT1)})
	  public Response anular(@Context HttpHeaders httpHeaders, @ApiParam(value="El objeto AnulacionAjusteResponse", required=true) AnulacionAjusteRequest request) throws NotFoundException, Exception { HeaderRequest headerRequest = new HeaderRequest(httpHeaders);
	    String idTransaccion = headerRequest.getIdTransaccion();

	    long tiempoInicio = System.currentTimeMillis();
	    String nombreMetodo = "anular";	    
	    String trazabilidad = "[" + nombreMetodo + " idTx=" + idTransaccion + "] ";
	    String result = null;

	    AnulacionAjusteResponse response = new AnulacionAjusteResponse();

	    String usuarioReg = null;
	    String headerPrint = ClaroUtil.printPrettyJSONString(headerRequest);
	    LOG.info(trazabilidad + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,nombreMetodo);
	    LOG.info(trazabilidad + "HeaderRequest Recibido: \n{}" + headerPrint);

	    String responsePrint = null;
	    try
	    {
	    	String trazabilidadNoNull = ClaroUtil.getHttpHeadersNoNull(httpHeaders);
			org.apache.log4j.MDC.put("dataAudit", trazabilidadNoNull);
			LOG.info(trazabilidad + "[Actividad 1. Validar Parametros Obligatorios]");	      

	      Set<ConstraintViolation<HeaderRequestBean>> constraintViolations = ValidadorUtil.validarParametrosObligatoriosHeader(httpHeaders);

	      boolean datosValidos = constraintViolations.isEmpty();

	      usuarioReg = "" + httpHeaders.getRequestHeader("userId") + "";
		  usuarioReg = this.limpiarValor(usuarioReg);
		  
	      String requestPrint = ClaroUtil.printPrettyJSONString(request);
	      LOG.info(trazabilidad + "**** Body Request: ....  \n{}", requestPrint);	      

	      if (!datosValidos)
	      {
				String camposNoOk = getCamposNoOK(constraintViolations);
				response.setPoCoderror(
						appConfig.getProperties().get(Constantes.MENSAJE_IDF2).toString() + camposNoOk);
				response.setPoDeserror(
						appConfig.getProperties().get(Constantes.CODIGO_IDF2).toString());
	      }
	      else {	
	        LOG.info(trazabilidad + " Validando campos ");
	 
	        usuarioReg = "" + httpHeaders.getRequestHeader("userId") + "";
	        usuarioReg = limpiarValor(usuarioReg);

	        LOG.info(trazabilidad + " [Actividad 2. Anular Ajustes SUNAT] ");
	        response = this.anulacionAjusteService.anular(trazabilidad, headerRequest, request);

	        responsePrint = ClaroUtil.printPrettyJSONString(response);
			if(response.getPoCoderror().equalsIgnoreCase(String.valueOf(Constantes.CERO))){
				response.setPoCoderror(String.valueOf(appConfig.getProperties().get(Constantes.CODIGO_IDF0)));
				response.setPoDeserror(String.valueOf(appConfig.getProperties().get(Constantes.MSJ_IDF0)));	
			}
	      }
	    }
	    catch (DBException e)
	    {	     
	      response.setPoCoderror(e.getCodigoError());
	      response.setPoDeserror(e.getMsjError());
	    }
	    catch (WSException e) {
	      response.setPoCoderror(e.getCodigoError());
	      response.setPoDeserror(e.getMsjError());
	    }

	    catch (Exception ex)
	    {	      
	      LOG.error(trazabilidad + " Error Generico Generado : ", ex.getMessage());
	      response.setPoCoderror(appConfig.getProperties().get(Constantes.CODIGO_IDT4).toString());
	      response.setPoDeserror(appConfig.getProperties().get(Constantes.MSG_IDT4).toString());
	    }
	    finally
	    {
	      result = new ObjectMapper().setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"))
	        .writerWithDefaultPrettyPrinter().writeValueAsString(response);
	      LOG.info(trazabilidad + "Response General:\n{}", responsePrint);
		  LOG.info(trazabilidad + "Tiempo total de proceso(ms): {} milisegundos.", System.currentTimeMillis() - tiempoInicio);
	      LOG.info(trazabilidad + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,nombreMetodo);
	    }
	    return Response.ok().entity(result).build();
	  }	
	
	
	
	@POST
	@Path(Constantes.NOMBRE_METODO_LISTAPROCESO)
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = Constantes.DESCRIPCION_METODO_LISTAPROCESO, notes = Constantes.NOTAS_METODO_LISTAPROCESO, response = ListaProcesoResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = Constantes.MENSAJE_IDF0),
			@ApiResponse(code = 200, message = Constantes.MENSAJE_IDF1),
			@ApiResponse(code = 400, message = Constantes.MENSAJE_IDT1) })
	public Response listaProceso(@Context HttpHeaders httpHeaders,
			@ApiParam(value = "El objeto ListaProcesoResponse", required = true) ListaProcesoRequest request)
			throws NotFoundException, Exception {
		HeaderRequest headerRequest = new HeaderRequest(httpHeaders);
		String idTransaccion = headerRequest.getIdTransaccion();
		
		long tiempoInicio = System.currentTimeMillis();
		String nombreMetodo = Constantes.METODO_LISTAPROCESO;
		String trazabilidad = "[" + nombreMetodo + " idTx=" + idTransaccion + "] ";
		String result = null;

		ListaProcesoResponse response= new ListaProcesoResponse();
		
		String usuarioReg = null;
		String headerPrint = ClaroUtil.printPrettyJSONString(headerRequest);
		LOG.info(trazabilidad + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_LISTAPROCESO);
		LOG.info(trazabilidad + "HeaderRequest Recibido: \n{}" + headerPrint);

		String responsePrint = null;
		try {
			String trazabilidadNoNull = ClaroUtil.getHttpHeadersNoNull(httpHeaders);
			org.apache.log4j.MDC.put("dataAudit", trazabilidadNoNull);
			LOG.info(trazabilidad + "[Actividad 1. Validar Parametros Obligatorios]");
			Set<ConstraintViolation<HeaderRequestBean>> constraintViolations = ValidadorUtil
					.validarParametrosObligatoriosHeader(httpHeaders);
			boolean datosValidos = constraintViolations.isEmpty();

			usuarioReg = "" + httpHeaders.getRequestHeader("userId") + "";
			usuarioReg = this.limpiarValor(usuarioReg);
			
			String requestPrint = ClaroUtil.printPrettyJSONString(request);
			LOG.info(trazabilidad + "**** Body Request: ....  \n{}", requestPrint);
			if (!datosValidos) {

				String camposNoOk = getCamposNoOK(constraintViolations);
				
				response.setPoCoderror(
						appConfig.getProperties().get(Constantes.CODIGO_IDF2).toString());
				response.setPoDeserror(
						appConfig.getProperties().get(Constantes.MENSAJE_IDF2).toString() + camposNoOk);

			} else {
				LOG.info(trazabilidad + " Validando campos ");
				usuarioReg = "" + httpHeaders.getRequestHeader("userId") + "";
				usuarioReg = this.limpiarValor(usuarioReg);
				
				LOG.info(trazabilidad + "[Actividad 2. Obtener lista de procesos]");
				response=listaProcesoService.listaProceso(trazabilidad, request);
				responsePrint = ClaroUtil.printPrettyJSONString(response);
				if(response.getPoCoderror().equalsIgnoreCase(String.valueOf(Constantes.CERO))){
					response.setPoCoderror(String.valueOf(appConfig.getProperties().get(Constantes.CODIGO_IDF0)));
					response.setPoDeserror(String.valueOf(appConfig.getProperties().get(Constantes.MSJ_IDF0)));	
				}
			}
		}catch (DBException e) {
			response.setPoCoderror(e.getCodigoError() );
			response.setPoDeserror(e.getMessage());			
		}catch (WSException e) {			
			response.setPoCoderror(e.getCodigoError());
			response.setPoDeserror(e.getMessage());
		}
		catch (Exception ex) {
			LOG.error(trazabilidad + " Error Generico Generado : ", ex.getMessage());
			response.setPoCoderror(appConfig.getProperties().get(Constantes.CODIGO_IDT4).toString());
			response.setPoDeserror(appConfig.getProperties().get(Constantes.MSG_IDT4).toString());

		} finally {
			result = new ObjectMapper().setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"))
					.writerWithDefaultPrettyPrinter().writeValueAsString(response);
			LOG.info(trazabilidad + "Response General:\n{}", responsePrint);
			LOG.info(trazabilidad + "Tiempo total de proceso(ms): {} milisegundos.", System.currentTimeMillis() - tiempoInicio);
			LOG.info(trazabilidad + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,nombreMetodo);
	}
		return Response.ok().entity(result).build();
	}
	
	
	@SuppressWarnings("rawtypes")
	private String getCamposNoOK(Set<ConstraintViolation<HeaderRequestBean>> constraintViolations) {
		StringBuilder camposNoOk = new StringBuilder();
		for (ConstraintViolation constraint : constraintViolations) {
			camposNoOk.append(Constantes.CORCHETE_IZQUIERDO);
			camposNoOk.append(constraint.getMessage());
			camposNoOk.append(Constantes.CORCHETE_DERECHO);
		}
		return camposNoOk.toString();
	}

	private String limpiarValor(String valor) {

		String salida = "";
		try {

			salida = valor.replace(Constantes.CORCHETE_IZQUIERDO, Constantes.TEXTOVACIO)
					.replace(Constantes.CORCHETE_DERECHO, Constantes.TEXTOVACIO).toUpperCase();

		} catch (Exception e) {
			LOG.error("ERROR: [Exception] - [" + e.getMessage() + "] ", e);
		}

		return salida;

	}
	}

