/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.claro.bss.oss.ajustemasivo.resource.util;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;

import pe.com.claro.bss.oss.ajustemasivo.common.property.Constantes;


@WebFilter( filterName = Constantes.HTML5CORSFILTER, urlPatterns = { Constantes.URLPATTERNS} )
public class HTML5CorsFilter implements javax.servlet.Filter{

	@Override
	public void doFilter( ServletRequest request, ServletResponse response, FilterChain chain ) throws IOException, ServletException{
		HttpServletResponse res = (HttpServletResponse)response;
		res.addHeader( Constantes.ACCESSCONTROLALLOWORIGIN, Constantes.ASTERISCO );
		res.addHeader( Constantes.ACCESSCONTROLALLOWMETHODS, Constantes.METODOSPERMITIDOS);
		res.addHeader( Constantes.ACCESSCONTROLALLOWHEADERS, Constantes.CONTENTTYPE);
		chain.doFilter( request, response );
	}

	@Override
	public void init( FilterConfig filterConfig ) throws ServletException{}

	@Override
	public void destroy(){}

}
