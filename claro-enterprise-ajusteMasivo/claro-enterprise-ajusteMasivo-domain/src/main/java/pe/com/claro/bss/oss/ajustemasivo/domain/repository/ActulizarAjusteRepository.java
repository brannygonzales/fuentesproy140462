package pe.com.claro.bss.oss.ajustemasivo.domain.repository;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;

import org.hibernate.annotations.QueryHints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.com.claro.bss.oss.ajustemasivo.canonical.bean.ActualizarAjusteBean;
import pe.com.claro.bss.oss.ajustemasivo.common.domain.repository.AbstractRepository;
import pe.com.claro.bss.oss.ajustemasivo.common.property.Constantes;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.DBException;
import pe.com.claro.bss.oss.ajustemasivo.model.Formulario;

@Stateless
public class ActulizarAjusteRepository extends AbstractRepository<Formulario>{
	private static final Logger LOG = LoggerFactory.getLogger(ActulizarAjusteRepository.class);

	  @Context
	  private Configuration configuration;

	  @PersistenceContext(unitName=Constantes.PERSISTENCEPACKAGEUNIT_ACTUALIZAR_AJUSTE)
	  public void setPersistenceUnit00(EntityManager em) { 
		  entityManager = em;
	  }

	  public ActulizarAjusteRepository()
	  {
	    super(Formulario.class);
	  }
	  
	  
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ActualizarAjusteBean actualizarAjuste(String mensajeTransaccion,ActualizarAjusteBean actualizarAjusteBean) throws DBException{
			LOG.info(mensajeTransaccion + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ACTUALIZAR_AJUSTE);
		    String OWNER = null;
		    String PAQUETE = null;
		    String PROCEDURE = null;
		    String BD = null;
		    
		    OWNER = String.valueOf(configuration.getProperty(Constantes.OWNER_BD_ACTUALIZAR_AJUSTE));
			BD = String.valueOf(configuration.getProperty(Constantes.NOMBRE_BD_ACTUALIZAR_AJUSTE));
			PAQUETE = String.valueOf(configuration.getProperty(Constantes.NOMBRE_PKG_ACTUALIZAR_AJUSTE));
			PROCEDURE = String.valueOf(configuration.getProperty(Constantes.NOMBRE_SP_ACTUALIZAR_AJUSTE));
		    String executePackage = OWNER + "." + PAQUETE + "." + PROCEDURE;
			try {
			    LOG.info(mensajeTransaccion + Constantes.EJECUTANDO_SP + Constantes.CORCHETE_IZQUIERDO + executePackage + Constantes.CORCHETE_DERECHO);
			    LOG.info(mensajeTransaccion + Constantes.PARAMETROS_INPUT);
			    LOG.info(mensajeTransaccion + Constantes.PI_CUSTOMER_ID+ actualizarAjusteBean.getPiCustomerId());
			    LOG.info(mensajeTransaccion + Constantes.PI_AJUSTE+ actualizarAjusteBean.getPiAjuste());
			    LOG.info(mensajeTransaccion + Constantes.PI_ESTADO+ actualizarAjusteBean.getPiEstado());
//			    executePackage =  PAQUETE + "." + PROCEDURE;
			StoredProcedureQuery procedureQuery = entityManager.createStoredProcedureQuery(executePackage);
			procedureQuery.setHint(QueryHints.TIMEOUT_JPA, Integer.parseInt(String.valueOf(configuration.getProperty(Constantes.NOMBRE_TM_ACTUALIZAR_AJUSTE))));
		      procedureQuery.registerStoredProcedureParameter(Constantes.UNO, String.class, ParameterMode.IN);
		      procedureQuery.registerStoredProcedureParameter(Constantes.DOS, String.class, ParameterMode.IN);
		      procedureQuery.registerStoredProcedureParameter(Constantes.TRES, String.class, ParameterMode.IN);
		      procedureQuery.registerStoredProcedureParameter(Constantes.CUATRO, String.class, ParameterMode.OUT);
		      procedureQuery.registerStoredProcedureParameter(Constantes.CINCO, String.class, ParameterMode.OUT);

		      procedureQuery.setParameter(Constantes.UNO, actualizarAjusteBean.getPiCustomerId());
		      procedureQuery.setParameter(Constantes.DOS, actualizarAjusteBean.getPiAjuste());
		      procedureQuery.setParameter(Constantes.TRES, actualizarAjusteBean.getPiEstado());
		      
		      procedureQuery.execute();
		      
		      LOG.info(mensajeTransaccion + Constantes.PARAMETROS_OUTPUT);
		      LOG.info(mensajeTransaccion +  Constantes.PO_CODERROR+ procedureQuery.getOutputParameterValue(Constantes.CUATRO));
		      LOG.info(mensajeTransaccion + Constantes.PO_DESERROR + procedureQuery.getOutputParameterValue(Constantes.CINCO));
		       
		      actualizarAjusteBean.setPoCoderror(String.valueOf(procedureQuery.getOutputParameterValue(Constantes.CUATRO)));
		      actualizarAjusteBean.setPoDeserror(String.valueOf(procedureQuery.getOutputParameterValue(Constantes.CINCO)));
}catch (Exception ex)
		      {
		        String errorMsg = ex + "";
		        String msjError = ex.getMessage();
		        String codError = null;
		        String nombreBD = BD;
		        String nombreSP = PROCEDURE;
		        if(errorMsg.contains(Constantes.ERROR_JDBC_TIMEOUT)){
		  	   		 codError = String.valueOf(configuration.getProperty(Constantes.CODIGO_IDT1));
		  	   		 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT1)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
		  	        } else {
		  	       	 codError =String.valueOf(configuration.getProperty( Constantes.CODIGO_IDT2));
		  	       	 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT2)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
		  	        }
		        actualizarAjusteBean.setPoCoderror(codError);
		        actualizarAjusteBean.setPoDeserror(msjError);
		  		LOG.error( mensajeTransaccion + " ERROR: [Exception BD: "+BD+" ] - [" + ex.getMessage() + "] ",ex );
		  		
		  		throw new DBException(codError,nombreBD,nombreSP,msjError,ex);  
		    }finally {
		  		LOG.info(mensajeTransaccion + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ACTUALIZAR_AJUSTE);
		  	}	
			return actualizarAjusteBean;
		}
	@Override
	protected Predicate[] getSearchPredicates(Root<Formulario> root, Formulario example) {
		return new Predicate[0];
	}

}
