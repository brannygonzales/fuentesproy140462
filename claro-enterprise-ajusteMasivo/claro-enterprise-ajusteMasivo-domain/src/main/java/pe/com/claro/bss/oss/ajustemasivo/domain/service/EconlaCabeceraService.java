package pe.com.claro.bss.oss.ajustemasivo.domain.service;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.com.claro.bss.oss.ajustemasivo.canonical.request.EncolaCabRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.EncolaCabResponse;
import pe.com.claro.bss.oss.ajustemasivo.common.property.Constantes;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.DBException;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.WSException;
import pe.com.claro.bss.oss.ajustemasivo.domain.repository.EconlaCabeceraBscsRepository;
import pe.com.claro.bss.oss.ajustemasivo.domain.repository.EconlaCabeceraSgaRepository;
import pe.com.claro.bss.oss.ajustemasivo.domain.repository.InsertTrxProcesoRepository;

@Stateless
public class EconlaCabeceraService {
	private static final Logger LOG = LoggerFactory.getLogger(EconlaCabeceraService.class);

	@Context
	private Configuration configuration;
	
	@Resource
	private EJBContext context;
	
	@EJB
	private InsertTrxProcesoRepository insertTrxProcesoRepository;
	
	@EJB
	private EconlaCabeceraBscsRepository  bscsRepository;
	@EJB
	private EconlaCabeceraSgaRepository sgaRepository;

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public EncolaCabResponse encolarCabecera(String mensajeTransaccion,EncolaCabRequest request) throws WSException, DBException {
		EncolaCabResponse response= new EncolaCabResponse();
		LOG.info(mensajeTransaccion + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ENCOLARCABECERA);
try {
	if (request.getPiCodAjuste()!=null && !request.getPiCodAjuste().isEmpty()) {
		if(request.getPiCodAjuste().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.ENCOLARCABECERA_SGA)))) {
			LOG.info(mensajeTransaccion+ " [Actividad 3] SGA");
			request.setPiCodAjuste(String.valueOf(configuration.getProperty(Constantes.ENCOLARCABECERA_SGA_V)));
			if(request.getPiTipoAjuste().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.ENCOLARCABECERA_SGA_NC)))||request.getPiTipoAjuste().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.ENCOLARCABECERA_SGA_ABONO)))) {
				request.setPiTipoAjuste(String.valueOf(configuration.getProperty(Constantes.ENCOLARCABECERA_SGA_NC1)));
			}else if(request.getPiTipoAjuste().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.ENCOLARCABECERA_SGA_ND)))||request.getPiTipoAjuste().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.ENCOLARCABECERA_SGA_CARGO)))) {
				request.setPiTipoAjuste(String.valueOf(configuration.getProperty(Constantes.ENCOLARCABECERA_SGA_ND1)));
			}
			if(request.getPiEstado().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.ENCOLARCABECERA_SGA_REGISTRANDO)))) {
				request.setPiEstado(String.valueOf(configuration.getProperty(Constantes.ENCOLARCABECERA_SGA_REGISTRANDO_V)));
			}
			response=sgaRepository.encolarCabecera(mensajeTransaccion, request);
		}
		else if(request.getPiCodAjuste().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.ENCOLARCABECERA_BSCS1))) || request.getPiCodAjuste().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.ENCOLARCABECERA_BSCS2))) || request.getPiCodAjuste().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.ENCOLARCABECERA_BSCS3))) || request.getPiCodAjuste().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.ENCOLARCABECERA_BSCS4))) ||
				request.getPiCodAjuste().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.ENCOLARCABECERA_BSCS5))) || request.getPiCodAjuste().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.ENCOLARCABECERA_BSCS6))) || request.getPiCodAjuste().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.ENCOLARCABECERA_BSCS7))) || request.getPiCodAjuste().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.ENCOLARCABECERA_BSCS8))))  {			
			LOG.info(mensajeTransaccion+ " [Actividad 3] BSCS");
			response=insertTrxProcesoRepository.encolarCabecera(mensajeTransaccion, request);
			
			LOG.info(mensajeTransaccion+ " [Actividad 4] BSCS");
			response=bscsRepository.encolarCabecera(mensajeTransaccion, request,response.getPoIdXLS());			
		}
		else {
			response.setPoCoderror(String.valueOf(configuration.getProperty(Constantes.CODIGO_IDT5)));
			response.setPoDeserror(String.valueOf(configuration.getProperty(Constantes.MSG_IDT5)));
		}
	}
	
}
		catch (DBException e) {			
			String msjError = e.getMessage(); 
	    	String codError = e.getCodigoError(); 	    	
	    	throw new DBException(codError,msjError,e);  
		}		
		catch (Exception ex) {
			String nombreWS = String.valueOf(configuration.getProperty(Constantes.WSSERVICEENPOINTADDRESS_CABECERA));
			String msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT3)).replace("$ws", nombreWS) + " " +ex.getMessage() ; 
	    	String codError = String.valueOf(configuration.getProperty(Constantes.CODIGO_IDT3));    
	    	
	    	LOG.error( mensajeTransaccion + " ERROR: [Exception WS: "+nombreWS+" ] - [" + ex.getMessage() + "] ",ex );
	    	throw new WSException( codError, msjError, nombreWS, ex );
		} finally {
			LOG.info(mensajeTransaccion + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ENCOLARCABECERA);
		}
		return response;
	}

}
