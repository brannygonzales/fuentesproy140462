package pe.com.claro.bss.oss.ajustemasivo.domain.repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;

import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import oracle.jdbc.OracleTypes;
import pe.com.claro.bss.oss.ajustemasivo.canonical.bean.SgassListarRegValidarOacBean;
import pe.com.claro.bss.oss.ajustemasivo.canonical.request.SgassListarRegValidarOacRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.SgassListarRegValidarOacResponse;
import pe.com.claro.bss.oss.ajustemasivo.common.domain.repository.AbstractRepository;
import pe.com.claro.bss.oss.ajustemasivo.common.property.Constantes;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.DBException;
import pe.com.claro.bss.oss.ajustemasivo.model.Formulario;

@Stateless
public class SgassListarRegValidarOacReposytory extends AbstractRepository<Formulario> {
	private static final Logger LOG = LoggerFactory.getLogger(SgassListarRegValidarOacReposytory.class);
	@Context
	private Configuration configuration;
	
	@PersistenceContext(unitName = Constantes.PERSISTENCEPACKAGEUNIT_ANULAR_AJUSTE_SGA)
	public void setPersistenceUnit(final EntityManager em) {
		this.entityManager = em;
	}
	public SgassListarRegValidarOacReposytory() {
		super(Formulario.class);
	}
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public SgassListarRegValidarOacResponse anular(String mensajeTransaccion,SgassListarRegValidarOacRequest request) throws DBException {
		SgassListarRegValidarOacResponse response = new SgassListarRegValidarOacResponse();
		LOG.info(mensajeTransaccion + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ANULAR);
		String OWNER = null;
		String PAQUETE = null;
		String PROCEDURE = null;
		String BD = null;
		
		BD = configuration.getProperty(Constantes.NOMBRE_BD_SGASS_LISTAR_REG_VALIDAR_OAC).toString();
		OWNER = configuration.getProperty(Constantes.OWNER_BD_SGASS_LISTAR_REG_VALIDAR_OAC).toString();
		PAQUETE = configuration.getProperty(Constantes.NOMBRE_PKG_SGASS_LISTAR_REG_VALIDAR_OAC).toString();
		PROCEDURE = configuration.getProperty(Constantes.NOMBRE_SP_SGASS_LISTAR_REG_VALIDAR_OAC).toString();
		String execute_package = OWNER+ Constantes.SEPARADORPUNTO+PAQUETE+Constantes.SEPARADORPUNTO+PROCEDURE;
		try {
		Session session = (Session) this.entityManager.unwrap(Session.class);
		session.doWork(new Work() {
			@Override
            public void execute(Connection connection) throws SQLException {      
				LOG.info(mensajeTransaccion + Constantes.EJECUTANDO_SP + Constantes.CORCHETE_IZQUIERDO + execute_package + Constantes.CORCHETE_DERECHO);
				LOG.info(mensajeTransaccion + Constantes.PARAMETROS_INPUT);
				LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.K_REGISTRO + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getkRegistro());
    			
    			CallableStatement call = connection.prepareCall("call " + execute_package + " (?,?,?,?)");
    			call.setQueryTimeout(Integer.parseInt(String.valueOf(configuration.getProperty(Constantes.NOMBRE_TM_SGASS_LISTAR_REG_VALIDAR_OAC))));
    			
    			call.setInt(Constantes.UNO, Integer.parseInt(request.getkRegistro()));
    			call.registerOutParameter(Constantes.DOS, OracleTypes.CURSOR);
    			call.registerOutParameter(Constantes.TRES,  Types.INTEGER);
    			call.registerOutParameter(Constantes.CUATRO,  Types.VARCHAR);
    			
    			call.execute();
    			
    			ResultSet resultSet = (ResultSet) call.getObject(Constantes.DOS);
    			if(resultSet!=null) {
    				List<SgassListarRegValidarOacBean> koCursor= new ArrayList<>();
    	    		  while(resultSet.next()) {
    	    			  SgassListarRegValidarOacBean listarRegValidarOacBean =new SgassListarRegValidarOacBean();
    	    			  
    	    			  listarRegValidarOacBean.setPvCodAplicacion(String.valueOf(resultSet.getObject(Constantes.UNO)));
    	    			  listarRegValidarOacBean.setPvTipoServicio(String.valueOf(resultSet.getObject(Constantes.DOS)));
    	    			  listarRegValidarOacBean.setPvCodCuenta(String.valueOf(resultSet.getObject(Constantes.TRES)));
    	    			  listarRegValidarOacBean.setPvTipoAjuste(String.valueOf(resultSet.getObject(Constantes.CUATRO)));
    	    			  listarRegValidarOacBean.setPvNroDocAjuste(String.valueOf(resultSet.getObject(Constantes.CINCO)));
    	    			  listarRegValidarOacBean.setPvDocsAjuste(String.valueOf(resultSet.getObject(Constantes.SEIS)));
    	    			  listarRegValidarOacBean.setPvMonedaAjuste(String.valueOf(resultSet.getObject(Constantes.SIETE)));
    	    			  listarRegValidarOacBean.setPnMontoAjuste(String.valueOf(resultSet.getObject(Constantes.OCHO)));
    	    			  listarRegValidarOacBean.setPnSaldoAjuste(String.valueOf(resultSet.getObject(Constantes.NUEVE)));
    	    			  listarRegValidarOacBean.setPvEstado(String.valueOf(resultSet.getObject(Constantes.DIEZ)));
    	    			  listarRegValidarOacBean.setPdFechaAjuste(String.valueOf(resultSet.getObject(Constantes.ONCE)));
    	    			  listarRegValidarOacBean.setPvDocsAjusteSga(String.valueOf(resultSet.getObject(Constantes.DOCE)));
    	    			  
    	    			  koCursor.add(listarRegValidarOacBean);
    	    		  }
    	    		  response.setKoCursor(koCursor);
    			}else {
    				response.setKoCursor(null);
    			}
    			LOG.info(mensajeTransaccion + Constantes.PARAMETROS_OUTPUT);
    			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.KO_CURSOR + Constantes.CORCHETE_DERECHO + " Cantidad "+ Constantes.DOS_PUNTOS +((response.getKoCursor()!=null)?response.getKoCursor().size():0));
    			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.KO_CODIGO + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + call.getObject(Constantes.TRES));
    			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.KO_MENSAJE + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + call.getObject(Constantes.CUATRO));
    			
    			response.setKoCodigo(String.valueOf(call.getObject(Constantes.TRES)));
    			response.setKoMensaje(String.valueOf(call.getObject(Constantes.CUATRO)));
			}});
}catch (Exception ex)
{
      String errorMsg = ex + "";
      String msjError = ex.getMessage();
      String codError = null;
      String nombreBD = BD;
      String nombreSP = PROCEDURE;
      if(errorMsg.contains(Constantes.ERROR_JDBC_TIMEOUT)){
	   		 codError = String.valueOf(configuration.getProperty(Constantes.CODIGO_IDT1));
	   		 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT1)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
	        } else {
	       	 codError =String.valueOf(configuration.getProperty( Constantes.CODIGO_IDT2));
	       	 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT2)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
	        }
		LOG.error( mensajeTransaccion + " ERROR: [Exception BD: "+BD+" ] - [" + ex.getMessage() + "] ",ex );
		throw new DBException(codError,nombreBD,nombreSP,msjError,ex);  
  }finally {
		LOG.info(mensajeTransaccion + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ANULAR);
	}	
		return response;
	}

	@Override
	protected Predicate[] getSearchPredicates(Root<Formulario> root, Formulario example) {
		return new Predicate[0];
	}

}
