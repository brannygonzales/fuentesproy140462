package pe.com.claro.bss.oss.ajustemasivo.domain.repository;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;

import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import oracle.jdbc.internal.OracleTypes;
import pe.com.claro.bss.oss.ajustemasivo.canonical.request.EncolaCabRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.EncolaCabResponse;
import pe.com.claro.bss.oss.ajustemasivo.common.domain.repository.AbstractRepository;
import pe.com.claro.bss.oss.ajustemasivo.common.property.Constantes;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.DBException;
import pe.com.claro.bss.oss.ajustemasivo.model.Formulario;

@Stateless
public class EconlaCabeceraBscsRepository extends AbstractRepository<Formulario>{
	private static final Logger LOG = LoggerFactory.getLogger(EconlaCabeceraBscsRepository.class);
	@Context
	private Configuration configuration;
	
	@PersistenceContext(unitName = Constantes.PERSISTENCEPACKAGEUNIT_CABECERA)
	public void setPersistenceUnit(final EntityManager em) {
		this.entityManager = em;
	}
	
	public EconlaCabeceraBscsRepository() {
		super(Formulario.class);
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public EncolaCabResponse encolarCabecera(String mensajeTransaccion,EncolaCabRequest request, String piIdProceso) throws DBException, Exception {
		EncolaCabResponse response= new EncolaCabResponse();
		LOG.info(mensajeTransaccion + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ENCOLARCABECERA);
		
		String OWNER = null;
		String PAQUETE = null;
		String PROCEDURE = null;
		String BD = null;

		BD = configuration.getProperty(Constantes.NOMBRE_BD_CABECERA).toString();
		OWNER = configuration.getProperty(Constantes.OWNER_BD_CABECERA).toString();
		PAQUETE = configuration.getProperty(Constantes.NOMBRE_PKG_CABECERA).toString();
		PROCEDURE = configuration.getProperty(Constantes.NOMBRE_SP_CABECERA).toString();
		String execute_package = OWNER+ Constantes.SEPARADORPUNTO+PAQUETE+Constantes.SEPARADORPUNTO+PROCEDURE;
		
		try {
			Session session = (Session) this.entityManager.unwrap(Session.class);
			session.doWork(new Work() {
                @Override
                public void execute(Connection connection) throws SQLException {
			LOG.info(mensajeTransaccion + Constantes.EJECUTANDO_SP + Constantes.CORCHETE_IZQUIERDO + execute_package+ Constantes.CORCHETE_DERECHO);
			LOG.info(mensajeTransaccion + Constantes.PARAMETROS_INPUT);
			
			LOG.info(mensajeTransaccion +Constantes.PI_ID_PROCESO +piIdProceso );
			LOG.info(mensajeTransaccion +Constantes.PI_NOMBRE_ARCHIVO +request.getPiNombreArchivo() );
			LOG.info(mensajeTransaccion +Constantes.PI_CORREO_DESTINO +request.getPiCorreoDestino());
			LOG.info(mensajeTransaccion +Constantes.PI_USU_TIPIFICACION +request.getPiUsuTipificacion());
			LOG.info(mensajeTransaccion +Constantes.PI_NOTA_TIPIFICACION +request.getPiNotaTipificacion());
			LOG.info(mensajeTransaccion +Constantes.PI_TIPO_NOTIFICACION +request.getPiTipoNotificacion());
			LOG.info(mensajeTransaccion +Constantes.PI_RECIBO_RETENIDOS +request.getPiReciboRetenidos());
			LOG.info(mensajeTransaccion +Constantes.PI_ENVIAR_POST_FACT +request.getPiEnviarPostFact());
			LOG.info(mensajeTransaccion + Constantes.PI_CODAJUSTE +request.getPiCodAjuste());
			LOG.info(mensajeTransaccion + Constantes.PI_TIPO_AJUSTE +request.getPiTipoAjuste());
			LOG.info(mensajeTransaccion +Constantes.PI_USUARIO +request.getPiUsuario());
			LOG.info(mensajeTransaccion + Constantes.PI_CODAPLICACION +request.getPiCodAplicacion());
			LOG.info(mensajeTransaccion + Constantes.PI_TOTALREG +request.getPiTotalReg());
			LOG.info(mensajeTransaccion + Constantes.PI_TIPOSPOOLHP +request.getPiTipospoolhp());
			LOG.info(mensajeTransaccion +Constantes.PI_ESTADO +request.getPiEstado());
			
			CallableStatement call = connection.prepareCall("call " + execute_package + " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			call.setQueryTimeout(Integer.parseInt(String.valueOf(configuration.getProperty(Constantes.NOMBRE_TM_CABECERA))));			
			
			if(piIdProceso!=null && !piIdProceso.isEmpty())
				call.setBigDecimal(Constantes.UNO, BigDecimal.valueOf(Double.valueOf(piIdProceso)));				
			else {
				call.setNull(Constantes.UNO, OracleTypes.NULL);
			}
			call.setString(Constantes.DOS, request.getPiNombreArchivo());
			call.setString(Constantes.TRES, request.getPiCorreoDestino());
			call.setString(Constantes.CUATRO, request.getPiUsuTipificacion());
			call.setString(Constantes.CINCO, request.getPiNotaTipificacion());
			call.setString(Constantes.SEIS, request.getPiTipoNotificacion());
			call.setString(Constantes.SIETE, request.getPiReciboRetenidos());
			call.setString(Constantes.OCHO, request.getPiEnviarPostFact());
			call.setString(Constantes.NUEVE, request.getPiCodAjuste());
			call.setString(Constantes.DIEZ, request.getPiTipoAjuste());
			call.setString(Constantes.ONCE, request.getPiUsuario());
			call.setString(Constantes.DOCE, request.getPiCodAplicacion());
			
		
			if(request.getPiTotalReg()!=null && !request.getPiTotalReg().isEmpty()) {
				call.setBigDecimal(Constantes.TRECE, BigDecimal.valueOf(Double.valueOf(request.getPiTotalReg())));				
			}else {
				call.setNull(Constantes.TRECE, OracleTypes.NULL);
			}
			if(request.getPiTipospoolhp()!=null && !request.getPiTipospoolhp().isEmpty()) {
				call.setBigDecimal(Constantes.CATORCE, BigDecimal.valueOf(Double.valueOf(request.getPiTipospoolhp())));				
			}
			else {
				call.setNull(Constantes.CATORCE, OracleTypes.NULL);
			}
				
			call.setString(Constantes.QUINCE, request.getPiEstado());
			
			call.registerOutParameter(Constantes.DIECISEIS,Types.INTEGER);
			call.registerOutParameter(Constantes.DIECISIETE,Types.VARCHAR);
			call.registerOutParameter(Constantes.DIECIOCHO,Types.VARCHAR);
			call.execute();
			
			LOG.info(mensajeTransaccion + Constantes.PARAMETROS_OUTPUT);
			LOG.info(mensajeTransaccion +Constantes.PO_IDXLS +String.valueOf(call.getObject(Constantes.DIECISEIS)));
			LOG.info(mensajeTransaccion +Constantes.PO_CODERROR +String.valueOf(call.getObject(Constantes.DIECISIETE)));
			LOG.info(mensajeTransaccion +Constantes.PO_DESERROR +String.valueOf(call.getObject(Constantes.DIECIOCHO)));
			
			
			response.setPoIdXLS(String.valueOf(call.getObject(Constantes.DIECISEIS)));
			response.setPoCoderror(String.valueOf(call.getObject(Constantes.DIECISIETE)));
			response.setPoDeserror(String.valueOf(call.getObject(Constantes.DIECIOCHO)));
                }});
		}catch (Exception ex) {
			String errorMsg = ex + Constantes.TEXTOVACIO;
		   	 String msjError = null; 
		   	 String codError = null; 
		   	 String nombreBD = BD;
		   	 String nombreSP = execute_package;
		   	 if(errorMsg.contains(Constantes.ERROR_JDBC_TIMEOUT)){
		   		 codError = String.valueOf(configuration.getProperty(Constantes.CODIGO_IDT1));
		   		 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT1)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
		        } else {
		       	 codError =String.valueOf(configuration.getProperty( Constantes.CODIGO_IDT2));
		       	 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT2)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
		        }
			LOG.error( mensajeTransaccion + " ERROR: [Exception BD: "+BD+" ] - [" + ex.getMessage() + "] ",ex );
			throw new DBException(codError,nombreBD,nombreSP,msjError,ex);  
		}finally {
			LOG.info(mensajeTransaccion + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ENCOLARCABECERA);
		}		
		
		return response;
	}
	@Override
	protected Predicate[] getSearchPredicates(Root<Formulario> root, Formulario example) {
		return new Predicate[0];
	}

}
