package pe.com.claro.bss.oss.ajustemasivo.domain.repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;

import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.com.claro.bss.oss.ajustemasivo.canonical.request.EncolaCabRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.EncolaCabResponse;
import pe.com.claro.bss.oss.ajustemasivo.common.domain.repository.AbstractRepository;
import pe.com.claro.bss.oss.ajustemasivo.common.property.Constantes;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.DBException;
import pe.com.claro.bss.oss.ajustemasivo.model.Formulario;

@Stateless
public class InsertTrxProcesoRepository extends AbstractRepository<Formulario> {
	private static final Logger LOG = LoggerFactory.getLogger(InsertTrxProcesoRepository.class);
	@Context
	private Configuration configuration;
	
	@PersistenceContext(unitName = Constantes.PERSISTENCEPACKAGEUNIT_INSERT_TRX_PROCESO)
	public void setPersistenceUnit(final EntityManager em) {
		this.entityManager = em;
	}
	public InsertTrxProcesoRepository() {
		super(Formulario.class);
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public EncolaCabResponse encolarCabecera(String mensajeTransaccion, EncolaCabRequest request)
			throws DBException, Exception {
		EncolaCabResponse response= new EncolaCabResponse();
LOG.info(mensajeTransaccion + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ENCOLARCABECERA);
		
		String OWNER = null;
		String PAQUETE = null;
		String PROCEDURE = null;
		String BD = null;
		OWNER = configuration.getProperty(Constantes.OWNER_BD_INSERT_TRX_PROCESO).toString();
		BD = configuration.getProperty(Constantes.NOMBRE_BD_INSERT_TRX_PROCESO).toString();
		PAQUETE = configuration.getProperty(Constantes.NOMBRE_PKG_INSERT_TRX_PROCESO).toString();
		PROCEDURE = configuration.getProperty(Constantes.NOMBRE_SP_INSERT_TRX_PROCESO).toString();
		String execute_package = OWNER+ Constantes.SEPARADORPUNTO+PAQUETE+Constantes.SEPARADORPUNTO+PROCEDURE;
		
		try {
			Session session = (Session) this.entityManager.unwrap(Session.class);
		
			session.doWork(new Work() {
                @Override
                public void execute(Connection connection) throws SQLException {
                	
                	LOG.info(mensajeTransaccion + Constantes.EJECUTANDO_SP + Constantes.CORCHETE_IZQUIERDO + execute_package + Constantes.CORCHETE_DERECHO);
        			LOG.info(mensajeTransaccion + Constantes.PARAMETROS_INPUT);
        			
        			LOG.info(mensajeTransaccion + Constantes.PI_SIOPTV_DESCRIPCION     +  String.valueOf(configuration.getProperty(Constantes.PRO_PI_SIOPTV_DESCRIPCION)) +" "+ request.getPiNombreArchivo());
        			LOG.info(mensajeTransaccion + Constantes.PI_SIOPTV_USUARIO         +  request.getPiUsuario());
        			LOG.info(mensajeTransaccion + Constantes.PI_SIOPTD_FECHAEJECUCION  +  Constantes.TEXTO_NULL);
        			LOG.info(mensajeTransaccion + Constantes.PI_SIOPTV_ESTADO          +  String.valueOf(configuration.getProperty(Constantes.PRO_PI_SIOPTV_ESTADO)));
        			LOG.info(mensajeTransaccion + Constantes.PI_SIOPTV_TIPOTRX         +  String.valueOf(configuration.getProperty(Constantes.PRO_PI_SIOPTV_TIPOTRX)));
        			LOG.info(mensajeTransaccion + Constantes.PI_SIOPTV_NOMBREARCH      +  request.getPiNombreArchivo());
        			LOG.info(mensajeTransaccion + Constantes.PI_SINO                   +  String.valueOf(configuration.getProperty(Constantes.PRO_PI_SINO)));
        			LOG.info(mensajeTransaccion + Constantes.PI_SIOPTV_ARCH_MODIF      +  Constantes.TEXTO_NULL);
        			LOG.info(mensajeTransaccion + Constantes.PI_SIOPTV_TIPO_PROCESO    +  String.valueOf(configuration.getProperty(Constantes.PRO_PI_SIOPTV_TIPO_PROCESO)));
        			LOG.info(mensajeTransaccion + Constantes.PI_SIOPTV_TIPO_SERVICIO   +  String.valueOf(configuration.getProperty(Constantes.PRO_PI_SIOPTV_TIPO_SERVICIO)));
        			LOG.info(mensajeTransaccion + Constantes.PI_SIOPTD_MONTO           +  Constantes.TEXTO_NULL);
        			LOG.info(mensajeTransaccion + Constantes.PI_SIOPTV_DETALLE         +  Constantes.TEXTO_NULL);
        			LOG.info(mensajeTransaccion + Constantes.PI_SIOPTV_GESTION         +  Constantes.TEXTO_NULL);
        			
        			CallableStatement call = connection.prepareCall("call " + execute_package + " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        			call.setQueryTimeout(Integer.parseInt(String.valueOf(configuration.getProperty(Constantes.NOMBRE_TM_INSERT_TRX_PROCESO))));
        			call.setString(Constantes.UNO, String.valueOf(configuration.getProperty(Constantes.PRO_PI_SIOPTV_DESCRIPCION)) + request.getPiNombreArchivo());
        			call.setString(Constantes.DOS,request.getPiUsuario());
        			call.setNull(Constantes.TRES,Types.NULL);
        			call.setString(Constantes.CUATRO,String.valueOf(configuration.getProperty(Constantes.PRO_PI_SIOPTV_ESTADO)));
        			call.setString(Constantes.CINCO,String.valueOf(configuration.getProperty(Constantes.PRO_PI_SIOPTV_TIPOTRX)));
        			call.setString(Constantes.SEIS,request.getPiNombreArchivo());
        			call.setInt(Constantes.SIETE,Integer.parseInt(String.valueOf(configuration.getProperty(Constantes.PRO_PI_SINO))));
        			call.setNull(Constantes.OCHO,Types.NULL);
        			call.setInt(Constantes.NUEVE,Integer.parseInt(String.valueOf(configuration.getProperty(Constantes.PRO_PI_SIOPTV_TIPO_PROCESO))));
        			call.setInt(Constantes.DIEZ,Integer.parseInt(String.valueOf(configuration.getProperty(Constantes.PRO_PI_SIOPTV_TIPO_SERVICIO))));
        			call.setNull(Constantes.ONCE,Types.NULL);
        			call.setNull(Constantes.DOCE,Types.NULL);
        			call.setNull(Constantes.TRECE,Types.NULL);
        			call.registerOutParameter(Constantes.CATORCE,Types.INTEGER);
        			call.registerOutParameter(Constantes.QUINCE,Types.INTEGER);
        			call.registerOutParameter(Constantes.DIECISEIS,Types.VARCHAR);
        			
        			call.execute();
        			LOG.info(mensajeTransaccion + Constantes.PARAMETROS_OUTPUT);
        			LOG.info(mensajeTransaccion + Constantes.PO_COD_PROCESO + call.getObject(Constantes.CATORCE) );
        			LOG.info(mensajeTransaccion + Constantes.PO_CODERROR +call.getObject(Constantes.QUINCE));
        			LOG.info(mensajeTransaccion + Constantes.PO_DESERROR +call.getObject(Constantes.DIECISEIS));
        			
        			response.setPoIdXLS(String.valueOf(call.getObject(Constantes.CATORCE)));
        			response.setPoCoderror(String.valueOf(call.getObject(Constantes.QUINCE)));
        			response.setPoDeserror(String.valueOf(call.getObject(Constantes.DIECISEIS)));		


                }});
		}catch (Exception ex) {
			String errorMsg = ex + Constantes.TEXTOVACIO;
		   	 String msjError = null; 
		   	 String codError = null; 
		   	 String nombreBD = BD;
		   	 String nombreSP = PROCEDURE;
		   	 if(errorMsg.contains(Constantes.ERROR_JDBC_TIMEOUT)){
		   		 codError = String.valueOf(configuration.getProperty(Constantes.CODIGO_IDT1));
		   		 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT1)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
		        } else {
		       	 codError =String.valueOf(configuration.getProperty( Constantes.CODIGO_IDT2));
		       	 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT2)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
		        }
			LOG.error( mensajeTransaccion + " ERROR: [Exception BD: "+BD+" ] - [" + ex.getMessage() + "] ",ex );
			throw new DBException(codError,nombreBD,nombreSP,msjError,ex);  
		}finally {
			LOG.info(mensajeTransaccion + " ====== [Fin] En {} ====== ",Constantes.METODO_ENCOLARCABECERA);
		}	
		return response;
	}
	@Override
	protected Predicate[] getSearchPredicates(Root<Formulario> root, Formulario example) {
		return new Predicate[0];
	}

}
