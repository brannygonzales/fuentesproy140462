package pe.com.claro.bss.oss.ajustemasivo.domain.repository;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;

import org.hibernate.annotations.QueryHints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.com.claro.bss.oss.ajustemasivo.canonical.request.ObtenerLineaRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.ObtenerLineaResponse;
import pe.com.claro.bss.oss.ajustemasivo.common.domain.repository.AbstractRepository;
import pe.com.claro.bss.oss.ajustemasivo.common.property.Constantes;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.DBException;
import pe.com.claro.bss.oss.ajustemasivo.model.Formulario;


@Stateless
public class ObtenerLineaRepository extends AbstractRepository<Formulario>
{
  private static final Logger LOG = LoggerFactory.getLogger(ObtenerLineaRepository.class);

  @Context
  private Configuration configuration;

  @PersistenceContext(unitName=Constantes.PERSISTENCEPACKAGEUNIT_OBTENER_LINEA)
  public void setPersistenceUnit00S(EntityManager em) { 
	  this.entityManager = em;
  }

  public ObtenerLineaRepository()
  {
    super(Formulario.class);
  }

  public ObtenerLineaResponse obtenerLinea(String mensajeTransaccion, ObtenerLineaRequest request)
    throws DBException, Exception
  {
	  LOG.info(mensajeTransaccion + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_OBTENERLINEA);
	  ObtenerLineaResponse response = new ObtenerLineaResponse();    
    String OWNER = null;
    String PAQUETE = null;
    String PROCEDURE = null;
    String BD = null;

	OWNER = configuration.getProperty(Constantes.OWNER_BD_ANULAR).toString();
	BD = configuration.getProperty(Constantes.NOMBRE_BD_ANULAR).toString();
	PAQUETE = configuration.getProperty(Constantes.NOMBRE_PKG_ANULAR).toString();
	PROCEDURE = configuration.getProperty(Constantes.NOMBRE_SP_ANULAR).toString();
    String execute_package = OWNER + "." + PAQUETE + "." + PROCEDURE;
    try
    {
      LOG.info(mensajeTransaccion + Constantes.EJECUTANDO_SP + Constantes.CORCHETE_IZQUIERDO + execute_package + Constantes.CORCHETE_DERECHO);
      LOG.info(mensajeTransaccion + Constantes.PARAMETROS_INPUT);      
      LOG.info(mensajeTransaccion + Constantes.PI_CUSTOMER_ID+ request.getPiCustomerId());
      LOG.info(mensajeTransaccion + Constantes.PI_AJUSTE+ request.getPiAjuste());

      StoredProcedureQuery procedureQuery = this.entityManager.createStoredProcedureQuery(execute_package);
      procedureQuery.setHint(QueryHints.TIMEOUT_JPA, Integer.parseInt(String.valueOf(configuration.getProperty(Constantes.NOMBRE_TM_ANULAR))));
      procedureQuery.registerStoredProcedureParameter(Constantes.UNO, String.class, ParameterMode.IN);
      procedureQuery.registerStoredProcedureParameter(Constantes.DOS, String.class, ParameterMode.IN);
      procedureQuery.registerStoredProcedureParameter(Constantes.TRES, String.class, ParameterMode.OUT);
      procedureQuery.registerStoredProcedureParameter(Constantes.CUATRO, String.class, ParameterMode.OUT);
      procedureQuery.registerStoredProcedureParameter(Constantes.CINCO, String.class, ParameterMode.OUT);

      procedureQuery.setParameter(Constantes.UNO, request.getPiCustomerId());
      procedureQuery.setParameter(Constantes.DOS, request.getPiAjuste());

      procedureQuery.execute();
      
      LOG.info(mensajeTransaccion + Constantes.PARAMETROS_OUTPUT);
      LOG.info(mensajeTransaccion + Constantes.PO_LINEA+ procedureQuery.getOutputParameterValue(Constantes.TRES));
      LOG.info(mensajeTransaccion + Constantes.CORCHETE_IZQUIERDO+ Constantes.PO_COD_OK+ Constantes.CORCHETE_DERECHO+Constantes.DOS_PUNTOS+ procedureQuery.getOutputParameterValue(Constantes.CUATRO));
      LOG.info(mensajeTransaccion + Constantes.PO_DESERROR + procedureQuery.getOutputParameterValue(Constantes.CINCO));
      
      response.setPoLinea((procedureQuery.getOutputParameterValue(Constantes.TRES) != null ? procedureQuery.getOutputParameterValue(Constantes.TRES) : "").toString());
      response.setPoCoderror((procedureQuery.getOutputParameterValue(Constantes.CUATRO) != null ? procedureQuery.getOutputParameterValue(Constantes.CUATRO) : "").toString());
      response.setPoDeserror((procedureQuery.getOutputParameterValue(Constantes.CINCO) != null ? procedureQuery.getOutputParameterValue(Constantes.CINCO) : "").toString());

      
    }
    catch (Exception ex)
    {
      String errorMsg = ex + "";
      String msjError = ex.getMessage();
      String codError = null;
      String nombreBD = BD;
      String nombreSP = PROCEDURE;
      if(errorMsg.contains(Constantes.ERROR_JDBC_TIMEOUT)){
	   		 codError = String.valueOf(configuration.getProperty(Constantes.CODIGO_IDT1));
	   		 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT1)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
	        } else {
	       	 codError =String.valueOf(configuration.getProperty( Constantes.CODIGO_IDT2));
	       	 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT2)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
	        }
		LOG.error( mensajeTransaccion + " ERROR: [Exception BD: "+BD+" ] - [" + ex.getMessage() + "] ",ex );
		throw new DBException(codError,nombreBD,nombreSP,msjError,ex);  
  }finally {
		LOG.info(mensajeTransaccion + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_OBTENERLINEA);
	}	
    return response;
  }
  
  protected Predicate[] getSearchPredicates(Root<Formulario> root, Formulario example) {
    return new Predicate[0];
  }
}