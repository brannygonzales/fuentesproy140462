package pe.com.claro.bss.oss.ajustemasivo.domain.repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;

import org.hibernate.annotations.QueryHints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.com.claro.bss.oss.ajustemasivo.canonical.bean.DocEnviadosSunatBean;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.DocEnviosSunatResponse;
import pe.com.claro.bss.oss.ajustemasivo.common.domain.repository.AbstractRepository;
import pe.com.claro.bss.oss.ajustemasivo.common.property.Constantes;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.DBException;
import pe.com.claro.bss.oss.ajustemasivo.model.Formulario;


@Stateless
public class EnvioSunatRepository extends AbstractRepository<Formulario> {
	private static final Logger LOG = LoggerFactory.getLogger(EnvioSunatRepository.class);


	@Context
	private Configuration configuration;
	
	@PersistenceContext(unitName = Constantes.PERSISTENCEPACKAGEUNIT_CONSULTARDOCSUNATSIOP)
	public void setPersistenceUnit00(final EntityManager em) {
		this.entityManager = em;
	}
	
public EnvioSunatRepository() {
	super(Formulario.class);
}

@SuppressWarnings({ "unused", "unchecked" })
public DocEnviosSunatResponse consultarDocSunatSIOP(String mensajeTransaccion, String fechaIni, String fechaFin)  throws DBException, Exception {
	
	List<Object[]> resultList = new ArrayList<>();
	List<DocEnviadosSunatBean> listaEnviadosSunat = new ArrayList<>();
	DocEnviadosSunatBean enviadosSunat=null;
	DocEnviosSunatResponse response=new DocEnviosSunatResponse();
	LOG.info(mensajeTransaccion + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_CONSULTARDOCSUNATSIOP);
	String OWNER = null;
	String PAQUETE = null;
	String PROCEDURE = null;
	String BD = null;
	
	BD = configuration.getProperty(Constantes.NOMBRE_BD_CONSULTARDOCSUNATSIOP).toString();
	OWNER = configuration.getProperty(Constantes.OWNER_BD_CONSULTARDOCSUNATSIOP).toString();
	PAQUETE = configuration.getProperty(Constantes.NOMBRE_PKG_CONSULTARDOCSUNATSIOP).toString();
	PROCEDURE = configuration.getProperty(Constantes.NOMBRE_SP_CONSULTARDOCSUNATSIOP).toString();
	String execute_package = OWNER+ Constantes.SEPARADORPUNTO+PAQUETE+Constantes.SEPARADORPUNTO+PROCEDURE;

	try {
		LOG.info(mensajeTransaccion + Constantes.EJECUTANDO_SP + Constantes.CORCHETE_IZQUIERDO + execute_package + Constantes.CORCHETE_DERECHO);
		LOG.info(mensajeTransaccion + Constantes.PARAMETROS_INPUT);
		LOG.info(mensajeTransaccion + Constantes.PI_FECHA_INI + fechaIni);
		LOG.info(mensajeTransaccion + Constantes.PI_FECHA_FIN + fechaFin);
		StoredProcedureQuery procedureQuery = entityManager.createStoredProcedureQuery(execute_package);
		procedureQuery.setHint(QueryHints.TIMEOUT_JPA, Integer.parseInt(String.valueOf(configuration.getProperty(Constantes.NOMBRE_TM_CONSULTARDOCSUNATSIOP))));
	procedureQuery.registerStoredProcedureParameter(Constantes.UNO, String.class, ParameterMode.IN);
	procedureQuery.registerStoredProcedureParameter(Constantes.DOS, String.class, ParameterMode.IN);
	procedureQuery.registerStoredProcedureParameter(Constantes.TRES,  void.class, ParameterMode.REF_CURSOR);
	procedureQuery.registerStoredProcedureParameter(Constantes.CUATRO, String.class, ParameterMode.OUT);
	procedureQuery.registerStoredProcedureParameter(Constantes.CINCO, String.class, ParameterMode.OUT);
	procedureQuery.setParameter(Constantes.UNO, fechaIni);
	procedureQuery.setParameter(Constantes.DOS, fechaFin);

	procedureQuery.execute();
	resultList = procedureQuery.getResultList();
	
	LOG.info(mensajeTransaccion + Constantes.PARAMETROS_OUTPUT);
	LOG.info(mensajeTransaccion + Constantes.PO_CODERROR+ procedureQuery.getOutputParameterValue(Constantes.CUATRO));
	LOG.info(mensajeTransaccion + Constantes.PO_DESERROR+ procedureQuery.getOutputParameterValue(Constantes.CINCO));
	response.setCodError(String.valueOf(procedureQuery.getOutputParameterValue(Constantes.CUATRO)));
	response.setDesError(String.valueOf(procedureQuery.getOutputParameterValue(Constantes.CINCO)));

	if (procedureQuery.getOutputParameterValue(Constantes.CUATRO).equals(Constantes.REPORTE_SUNAT_DB_OK)) {
	if ((resultList != null) &&(!resultList.isEmpty())) {
			for (Object[] obj : resultList) {
				enviadosSunat = new DocEnviadosSunatBean();
				// -------------------------------------------------------------
				enviadosSunat.setId(String.valueOf((BigDecimal) obj[0]));
				enviadosSunat.setLote(String.valueOf( obj[1]));
				enviadosSunat.setTipo(String.valueOf(obj[2]));
				enviadosSunat.setFchEmision( String.valueOf(obj[3]));					
				enviadosSunat.setFchEjecucion(String.valueOf(obj[4]));
				enviadosSunat.setLegado(String.valueOf( obj[5]));
				enviadosSunat.setRegLegado(String.valueOf((BigDecimal) obj[6]));
				enviadosSunat.setRegEntregados(String.valueOf((BigDecimal) obj[7]));
				enviadosSunat.setMontoAnulado(String.valueOf((BigDecimal) obj[8]));
				enviadosSunat.setMontoEntregado(String.valueOf((BigDecimal) obj[9]));
				enviadosSunat.setLogError(String.valueOf( obj[10]));
				enviadosSunat.setEstado(String.valueOf((BigDecimal) obj[11]));
				enviadosSunat.setFechaRegistro(String.valueOf(obj[12]));
				enviadosSunat.setUsuarioRegistro(String.valueOf( obj[13]));
				enviadosSunat.setFechaActualiza(String.valueOf(obj[14]));
				enviadosSunat.setUsuarioActualiza(String.valueOf( obj[15]));
			listaEnviadosSunat.add(enviadosSunat);
			}
			LOG.info(mensajeTransaccion + Constantes.PO_CUR_SALIDA_CANTIDAD +listaEnviadosSunat.size());					
			response.setListaEnviadosSunatType(listaEnviadosSunat);
		}
	}
	
	
	}catch (Exception ex) {
		String errorMsg = ex + Constantes.TEXTO_VACIO;
	   	 String msjError = null; 
	   	 String codError = null; 
	   	 String nombreBD = BD;
	   	 String nombreSP = PROCEDURE;
	   	 if(errorMsg.contains(Constantes.ERROR_JDBC_TIMEOUT)){
	   		 codError = String.valueOf(configuration.getProperty(Constantes.CODIGO_IDT1));
	   		 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT1)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
	        } else {
	       	 codError =String.valueOf(configuration.getProperty( Constantes.CODIGO_IDT2));
	       	 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT2)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
	        }
		LOG.error( mensajeTransaccion + " ERROR: [Exception BD: "+BD+" ] - [" + ex.getMessage() + "] ",ex );
		throw new DBException(codError,nombreBD,nombreSP,msjError,ex);  
	}finally {
		LOG.info(mensajeTransaccion + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_CONSULTARDOCSUNATSIOP);
	}		
	
	return response;
}
	@Override
	protected Predicate[] getSearchPredicates(Root<Formulario> root, Formulario example) {
		return new Predicate[0];
	}

}
