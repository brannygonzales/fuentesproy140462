package pe.com.claro.bss.oss.ajustemasivo.domain.service;


import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.com.claro.bss.oss.ajustemasivo.canonical.bean.ActualizarAjusteBean;
import pe.com.claro.bss.oss.ajustemasivo.canonical.bean.DetalleAnulacion;
import pe.com.claro.bss.oss.ajustemasivo.canonical.bean.LineasAnuladosTipificados;
import pe.com.claro.bss.oss.ajustemasivo.canonical.bean.SgassListarRegAnularOacBean;
import pe.com.claro.bss.oss.ajustemasivo.canonical.bean.SgassListarRegValidarOacBean;
import pe.com.claro.bss.oss.ajustemasivo.canonical.request.AnulacionAjusteRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.request.ObtenerLineaRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.request.PrPrcInterfaceAjusteRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.request.PrValRegistroRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.request.SgasiRegistrarDocAnularRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.request.SgassListarRegAnularOacRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.request.SgassListarRegValidarOacRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.request.SgassListarResultadosRegRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.request.SgasuAnularDocumentoSgaRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.request.SgasuValidaOacRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.request.SgasuValidarRegAnularSgaRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.AnulacionAjusteResponse;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.ObtenerLineaResponse;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.PrPrcInterfaceAjusteResponse;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.PrValRegistroResponse;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.SgasiRegistrarDocAnularResponse;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.SgassListarRegAnularOacResponse;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.SgassListarRegValidarOacResponse;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.SgassListarResultadosRegResponse;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.SgasuAnularDocumentoSgaResponse;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.SgasuValidaOacResponse;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.SgasuValidarRegAnularSgaResponse;
import pe.com.claro.bss.oss.ajustemasivo.common.bean.HeaderRequest;
import pe.com.claro.bss.oss.ajustemasivo.common.property.ClaroUtil;
import pe.com.claro.bss.oss.ajustemasivo.common.property.Constantes;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.DBException;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.WSException;
import pe.com.claro.bss.oss.ajustemasivo.domain.repository.ActulizarAjusteRepository;
import pe.com.claro.bss.oss.ajustemasivo.domain.repository.AnularAjusteRepository;
import pe.com.claro.bss.oss.ajustemasivo.domain.repository.ObtenerLineaRepository;
import pe.com.claro.bss.oss.ajustemasivo.domain.repository.PrPrcInterfaceAjusteReposytory;
import pe.com.claro.bss.oss.ajustemasivo.domain.repository.PrValRegistroReposytory;
import pe.com.claro.bss.oss.ajustemasivo.domain.repository.SgasiRegistrarDocAnularRepository;
import pe.com.claro.bss.oss.ajustemasivo.domain.repository.SgassListarRegAnularOacReposytory;
import pe.com.claro.bss.oss.ajustemasivo.domain.repository.SgassListarRegValidarOacReposytory;
import pe.com.claro.bss.oss.ajustemasivo.domain.repository.SgassListarResultadosRegReposytory;
import pe.com.claro.bss.oss.ajustemasivo.domain.repository.SgasuAnularDocumentoSgaReposytory;
import pe.com.claro.bss.oss.ajustemasivo.domain.repository.SgasuValidaOacReposytory;
import pe.com.claro.bss.oss.ajustemasivo.domain.repository.SgasuValidarRegAnularSgaReposytory;
import pe.com.claro.bss.oss.ajustemasivo.service.correo.EnvioCorreo;
import pe.com.claro.bss.oss.ajustemasivo.service.transaccion.NuevaInteracciones;
import pe.com.claro.eai.crm.clarify.InteraccionType;
import pe.com.claro.eai.crmservices.clarify.transaccioninteracciones.NuevaInteraccion;
import pe.com.claro.eai.crmservices.clarify.transaccioninteracciones.NuevaInteraccionResponse;
import pe.com.claro.eai.util.enviocorreo.types.AuditTypeRequest;
import pe.com.claro.eai.util.enviocorreo.types.EnviarCorreoRequest;


@Stateless
public class AnularAjusteService {
	private static final Logger LOGGER = LoggerFactory.getLogger(AnularAjusteService.class);
	
	@EJB
	  private EnvioCorreo envioCorreo;

	  @EJB
	  private NuevaInteracciones interacciones;

	  @Context
	  private Configuration configuration;

	  @EJB
	  private AnularAjusteRepository anularAjusteRepository;

	  @EJB
	  private ObtenerLineaRepository lineaRepository;
	  
	  @EJB
	  private ActulizarAjusteRepository actulizarAjusteRepository;
	 
	  
	  @EJB
	  private SgasiRegistrarDocAnularRepository sgasiRegistrarDocAnularRepository;
	  
	  @EJB
	  private SgasuValidarRegAnularSgaReposytory sgasuValidarRegAnularSgaReposytory;
	  
	  @EJB
	  private SgassListarRegValidarOacReposytory sgassListarRegValidarOacReposytory;
	  
	  @EJB
	  private PrValRegistroReposytory prValRegistroReposytory;
	  
	  @EJB
	  private SgasuValidaOacReposytory sgasuValidaOacReposytory;
	  
	  @EJB
	  private SgassListarRegAnularOacReposytory sgassListarRegAnularOacReposytory;
	  
	  @EJB
	  private PrPrcInterfaceAjusteReposytory prPrcInterfaceAjusteReposytory;
	  
	  @EJB
	  private SgasuAnularDocumentoSgaReposytory sgasuAnularDocumentoSgaReposytory;
	  
	  @EJB
	  private SgassListarResultadosRegReposytory sgassListarResultadosRegReposytory;
	  
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	  public AnulacionAjusteResponse anular(String msgTransaction, HeaderRequest headerRequest, AnulacionAjusteRequest request) throws DBException, Exception { 	        
		LOGGER.info(msgTransaction + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ANULAR);
	  	
		AnulacionAjusteResponse response = new AnulacionAjusteResponse();
	  	
	  	try {
	    if (request.getPiCliente().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.BSCS)))) {
	    	
	    	ObtenerLineaResponse lineaResponse= new ObtenerLineaResponse();
		    List<LineasAnuladosTipificados> lineasAnuladasTipificadas = new ArrayList<LineasAnuladosTipificados>();
		    List<String> lineas = new ArrayList<String>();	    
		    ObtenerLineaRequest lineaRequest = new ObtenerLineaRequest();	    	
	    	String[] trama = request.getPiTrama().split(Pattern.quote("|"));
	    	LOGGER.info(msgTransaction+ " [Actividad 3: obtener lineas de la trama en BSCS]");
	    	for (String custumerIdAjuste : trama) {
	  	      String[] obLinea = custumerIdAjuste.split(";");
	  	      lineaRequest.setPiCustomerId(obLinea[0]);
	  	      lineaRequest.setPiAjuste(obLinea[1]);
	  	     lineaResponse = lineaRepository.obtenerLinea(msgTransaction, lineaRequest);
	  	    lineas.add(lineaResponse.getPoLinea());	  	    
	  	    }

	  	    for (int t = 0; t < trama.length; t++) {
	  	      LineasAnuladosTipificados lat = new LineasAnuladosTipificados();
	  	      lat.setTrama(trama[t]);
	  	      lat.setLinea(lineas.get(t));
	  	      lat.setAnulados(Constantes.ANULADO_E);
	  	      lat.setTitpificados(Constantes.TIPIFICADO_P);
	  	      lineasAnuladasTipificadas.add(lat);
	  	    }
	    	
	    	 response = envioBscs(msgTransaction,response, lineasAnuladasTipificadas, request, headerRequest);
	    	 
	    	 
	      
	  	    String anuladosTipificadosSiop = "";
	  	    String anuladosTipificados = "";
	  	    String anuladosSinTipificarSiop = "";
	  	    String anuladosSinTipificar = "";
	  	    String sinAnular = "";
	  	    String  mensajeCorreo="";
	  	    for (int lda = 0; lda < response.getDetalleAnulacion().size(); lda++) {
	  	      if (((DetalleAnulacion)response.getDetalleAnulacion().get(lda)).getTipo().equals(Constantes.UNO+Constantes.TEXTO_VACIO))
	  	      {	  	    	
	  	        anuladosTipificadosSiop = "\t" + ((DetalleAnulacion)response.getDetalleAnulacion().get(lda)).getTipo() + "; " + 
	  	        		((DetalleAnulacion)response.getDetalleAnulacion().get(lda)).getAjuste() + "; " + 
	  	          ((DetalleAnulacion)response.getDetalleAnulacion().get(lda)).getDescripcion() + " \n \t";
	  	        
	  	      } else if (((DetalleAnulacion)response.getDetalleAnulacion().get(lda)).getTipo().equals(Constantes.DOS+Constantes.TEXTO_VACIO))	  	    	  
	  	      {
	  	    	anuladosTipificados = "\t" + ((DetalleAnulacion)response.getDetalleAnulacion().get(lda)).getTipo() + "; " + 
	  	          ((DetalleAnulacion)response.getDetalleAnulacion().get(lda)).getAjuste() + "; " + 
	  	          ((DetalleAnulacion)response.getDetalleAnulacion().get(lda)).getDescripcion() + " \n \t"; } 
	  	    else if (((DetalleAnulacion)response.getDetalleAnulacion().get(lda)).getTipo().equals(Constantes.TRES+Constantes.TEXTO_VACIO))	  	    	  
	  	      {
	  	    	anuladosSinTipificarSiop = "\t" + ((DetalleAnulacion)response.getDetalleAnulacion().get(lda)).getTipo() + "; " + 
	  	          ((DetalleAnulacion)response.getDetalleAnulacion().get(lda)).getAjuste() + "; " + 
	  	          ((DetalleAnulacion)response.getDetalleAnulacion().get(lda)).getDescripcion() + " \n \t"; } 
	  	  else if (((DetalleAnulacion)response.getDetalleAnulacion().get(lda)).getTipo().equals(Constantes.CUATRO+Constantes.TEXTO_VACIO))	  	    	  
  	      {
  	        anuladosSinTipificar = "\t" + ((DetalleAnulacion)response.getDetalleAnulacion().get(lda)).getTipo() + "; " + 
  	          ((DetalleAnulacion)response.getDetalleAnulacion().get(lda)).getAjuste() + "; " + 
  	          ((DetalleAnulacion)response.getDetalleAnulacion().get(lda)).getDescripcion() + " \n \t"; } 
	  	      else  if (((DetalleAnulacion)response.getDetalleAnulacion().get(lda)).getTipo().equals(Constantes.CINCO+Constantes.TEXTO_VACIO))
	  	        {
	  	        sinAnular = sinAnular + ((DetalleAnulacion)response.getDetalleAnulacion().get(lda)).getTipo() + "; " + 
	  	          ((DetalleAnulacion)response.getDetalleAnulacion().get(lda)).getAjuste() + "; " + 
	  	          ((DetalleAnulacion)response.getDetalleAnulacion().get(lda)).getDescripcion() + " \n \t";
	  	      }
	  	    }
	  	    if(anuladosTipificadosSiop!=null && !anuladosTipificadosSiop.isEmpty()) {
	  	    	mensajeCorreo = String.valueOf(configuration.getProperty(Constantes.LINEA_ANULADAS_TIPICADO_SIOP))+" \n" + anuladosTipificadosSiop + "\n ";
	  	    }
	  	  else if(anuladosTipificadosSiop!=null && !anuladosTipificadosSiop.isEmpty()){
	  	    	mensajeCorreo +=String.valueOf(configuration.getProperty(Constantes.LINEA_ANULADAS_TIPICAR_SINSIOP))+" \n" + anuladosTipificados + "\n ";
	  	    }
	  	else if(anuladosSinTipificarSiop!=null && !anuladosSinTipificarSiop.isEmpty()){
  	    	mensajeCorreo +=String.valueOf(configuration.getProperty(Constantes.LINEA_ANULADAS_SINTIPICAR_SIOP))+" \n"  + anuladosSinTipificarSiop+"\n ";
  	    }
	  	else if(anuladosSinTipificar!=null && !anuladosSinTipificar.isEmpty()){
  	    	mensajeCorreo +=String.valueOf(configuration.getProperty(Constantes.LINEA_ANULADAS_SINTIPICAR_SINSIOP))+" \n"+anuladosSinTipificar+"\n ";
  	    }
	  	    else if(sinAnular!=null && !sinAnular.isEmpty()){
	  	    	mensajeCorreo +=String.valueOf(configuration.getProperty(Constantes.LINEA_SINANULADAS_SINTIPICAR_SINSIOP))+" \n"+ sinAnular;
	  	    }
	  	 
	  	    envioCorreo(msgTransaction,headerRequest,request,mensajeCorreo); 	      

	    }
	    else if (request.getPiCliente().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.SGA)))) {
	    	response=envioSga( msgTransaction, headerRequest,response, request);
	    	
	    }
	    else {
	      response.setPoCoderror(Constantes.PO_COD_ERROR_V);
	      response.setPoDeserror("No ingreso "+String.valueOf(configuration.getProperty(Constantes.BSCS))+" o "+String.valueOf(configuration.getProperty(Constantes.SGA)));
	    }
	  	}catch (DBException e) {			
	    	throw new DBException(e); 
	  	}	    catch(WSException e) {	
			throw new WSException( e );
			}		
		catch (Exception ex) {
			String nombreWS = String.valueOf(configuration.getProperty(Constantes.WSSERVICEENPOINTADDRESS_ANULAR));
			String msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT3)).replace("$ws", nombreWS) + " " +ex.getMessage() ; 
	    	String codError = String.valueOf(configuration.getProperty(Constantes.CODIGO_IDT3));    	    	
	    	LOGGER.error( msgTransaction + " ERROR: [Exception WS: "+nombreWS+" ] - [" + ex.getMessage() + "] ",ex );
	    	throw new WSException( codError, msjError, nombreWS, ex );
		} finally {
			LOGGER.info(msgTransaction + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ANULAR);
		}
	    
	    return response; 
	    }

	  private void envioCorreo(String msgTransaction, HeaderRequest headerRequest, AnulacionAjusteRequest request,String mensajeCorreo) throws WSException 
	  {
	    EnviarCorreoRequest correo = new EnviarCorreoRequest();
	    
		try {
		      AuditTypeRequest auditTypeRequest = new AuditTypeRequest();
		      auditTypeRequest.setIdTransaccion(headerRequest.getIdTransaccion());
		      auditTypeRequest.setCodigoAplicacion(headerRequest.getMsgid());
		      auditTypeRequest.setUsrAplicacion(headerRequest.getUserId());
	
		      correo.setAuditRequest(auditTypeRequest);
		      
		      correo.setAsunto(String.valueOf(configuration.getProperty(Constantes.CORREO_ASUNTO))+" "+request.getPiArchivo() );
		      correo.setRemitente(String.valueOf(configuration.getProperty(Constantes.CORREO_REMITENTE)));
		      correo.setDestinatario(request.getPiCorreoDestino());
		      correo.setMensaje(mensajeCorreo);
		      correo.setHtmlFlag(String.valueOf(Constantes.CERO));
	
		      envioCorreo.enviarCorreo(msgTransaction, correo);
		      
			}catch(WSException e) {	
				throw new WSException(  e );
				}
		 }
	  private AnulacionAjusteResponse envioSga(String msgTransaction,HeaderRequest headerRequest,AnulacionAjusteResponse response,AnulacionAjusteRequest request) throws DBException, WSException {
		  LOGGER.info(msgTransaction + "Inicio de anular SGA");
		  String cAnuladosOacSga = "\t";
		  String cSinAnular= "\t";
		  String mensajeCorreo;
		  try {			  
			  List<DetalleAnulacion> listSgaAnu= new ArrayList<DetalleAnulacion>();
			  
		  LOGGER.info(msgTransaction + "ACTIVIDAD 3");
		  SgasiRegistrarDocAnularRequest request1 =new SgasiRegistrarDocAnularRequest();
		  if(request.getPiTipoAjuste().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.ENCOLARCABECERA_SGA_NC)))||request.getPiTipoAjuste().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.ENCOLARCABECERA_SGA_ABONO)))) {
			  request1.setkTipo(String.valueOf(configuration.getProperty(Constantes.ENCOLARCABECERA_SGA_NC1)));
			}else if(request.getPiTipoAjuste().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.ENCOLARCABECERA_SGA_ND)))||request.getPiTipoAjuste().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.ENCOLARCABECERA_SGA_CARGO)))) {
				request1.setkTipo(String.valueOf(configuration.getProperty(Constantes.ENCOLARCABECERA_SGA_ND1)));
			}
		  request1.setkTrama(request.getPiTrama());
		  request1.setkUsuario(request.getPiUsuario());
		  SgasiRegistrarDocAnularResponse response1= validarDocumentoSGA(msgTransaction,request1);
		  LOGGER.info(msgTransaction + "ACTIVIDAD 4 response1.getKoCodigo() "+response1.getKoCodigo());
		  if(response1.getKoCodigo()!=null && !response1.getKoCodigo().equalsIgnoreCase("null") && response1.getKoCodigo().equals("0")) {
			  LOGGER.info(msgTransaction + "ACTIVIDAD 4");
			  SgasuValidarRegAnularSgaRequest request2= new SgasuValidarRegAnularSgaRequest();
			  request2.setkRegistro(response1.getKoRegistro());
			  SgasuValidarRegAnularSgaResponse response2=validarRegistroSga(msgTransaction,request2);
			  
			  if(response2.getKoCodigo()!=null && !response2.getKoCodigo().equalsIgnoreCase("null") && response2.getKoCodigo().equals("0")) {
			  LOGGER.info(msgTransaction + "ACTIVIDAD 5");
			  SgassListarRegValidarOacRequest request3= new SgassListarRegValidarOacRequest();
			  request3.setkRegistro(response1.getKoRegistro());
			  SgassListarRegValidarOacResponse response3=listarRgistrosValidarOac(msgTransaction,request3);			  
			  LOGGER.info(msgTransaction + "ACTIVIDAD 6 response3.getKoCursor().size() "+response3.getKoCursor().size());
			  if(response3.getKoCursor()!=null && response3.getKoCursor().size()!=0) {
				  LOGGER.info(msgTransaction + "ACTIVIDAD 6");
				  
				  for(SgassListarRegValidarOacBean bean:response3.getKoCursor()) {
					  PrValRegistroRequest request4= new PrValRegistroRequest();
					  request4.setPdFechaAjuste(bean.getPdFechaAjuste());
					  request4.setPnMontoAjuste(bean.getPnMontoAjuste());
					  request4.setPnSaldoAjuste(bean.getPnSaldoAjuste());
					  request4.setPvCodAplicacion(bean.getPvCodAplicacion());
					  request4.setPvCodCuenta(bean.getPvCodCuenta());
					  request4.setPvDocsAjuste(bean.getPvDocsAjuste());
					  request4.setPvEstado(bean.getPvEstado());
					  request4.setPvMonedaAjuste(bean.getPvMonedaAjuste());
					  request4.setPvNroDocAjuste(bean.getPvNroDocAjuste());
					  request4.setPvTipoAjuste(bean.getPvTipoAjuste());
					  request4.setPvTipoServicio(bean.getPvTipoServicio());
					  
					  PrValRegistroResponse response4=validarRegistrosOac(msgTransaction,request4);
					  
					  if(response4.getXvStatus()!=null && response4.getXvStatus().equals("1") && !response4.getXvStatus().equalsIgnoreCase("null")){
						  LOGGER.info(msgTransaction + "ACTIVIDAD 7");
						  SgasuValidaOacRequest request5= new SgasuValidaOacRequest();
						  request5.setkRegistro(response1.getKoRegistro());
						  request5.setkAmadvDocumento(bean.getPvDocsAjusteSga());
						  request5.setkCodigo(response4.getXvStatus());
						  actualizarEstadoSga(msgTransaction,request5);						  
					  }
				  }
				  
				  LOGGER.info(msgTransaction + "ACTIVIDAD 8");
				  SgassListarRegAnularOacRequest request6 = new SgassListarRegAnularOacRequest();
				  request6.setkRegistro(response1.getKoRegistro());				  
				  SgassListarRegAnularOacResponse response6=listarRegistrosAnularOac(msgTransaction,request6);
				  LOGGER.info(msgTransaction + "ACTIVIDAD 8 response6.getKoCursor().size() "+ response6.getKoCursor().size());
				  
				  if(response6.getKoCursor()!=null && response6.getKoCursor().size()!=0) {
					  LOGGER.info(msgTransaction + "ACTIVIDAD 9");
					  for(SgassListarRegAnularOacBean bean2:response6.getKoCursor()) {
						  PrPrcInterfaceAjusteRequest request7=new PrPrcInterfaceAjusteRequest();
						  request7.setPdFechaAjuste(bean2.getPdFechaAjuste());
						  request7.setPdFechaCancelacion(bean2.getPdFechaCancelacion());
						  request7.setPdFechaVencAjuste(bean2.getPdFechaVencAjuste());
						  request7.setPnMontoAjuste(bean2.getPnMontoAjuste());
						  request7.setPnSaldoAjuste(bean2.getPnSaldoAjuste());
						  request7.setPvCodAplicacion(bean2.getPvCodAplicacion());
						  request7.setPvCodCuenta(bean2.getPvCodCuenta());
						  request7.setPvCodMotivoAjuste(bean2.getPvCodMotivoAjuste());
						  request7.setPvDescripAjuste(bean2.getPvDescripAjuste());
						  request7.setPvDocsAjuste(bean2.getPvDocsAjuste());
						  request7.setPvEstado(bean2.getPvEstado());
						  request7.setPvIdReclamoOrigen(bean2.getPvIdReclamoOrigen());
						  request7.setPvMonedaAjuste(bean2.getPvMonedaAjuste());
						  request7.setPvNroDocAjuste(bean2.getPvNroDocAjuste());
						  request7.setPvTipoAjuste(bean2.getPvTipoAjuste());
						  request7.setPvTipoOperacion(bean2.getPvTipoOperacion());
						  request7.setPvTipoServicio(bean2.getPvTipoServicio());
						  request7.setPvTrxIdWs(bean2.getPvTrxIdWs());
						  request7.setPvUsuarioAplic(bean2.getPvUsuarioAplic());
						  PrPrcInterfaceAjusteResponse response7=anularDocumentoOac(msgTransaction,request7);
						  LOGGER.info(msgTransaction + "ACTIVIDAD 9 response7.getXvStatus() "+response7.getXvStatus());
						  if(response7.getXvStatus()!=null && !response7.getXvStatus().equalsIgnoreCase("null") && response7.getXvStatus().equals("0")) {
							  LOGGER.info(msgTransaction + "ACTIVIDAD 10");
							  
							  SgasuAnularDocumentoSgaRequest request8= new SgasuAnularDocumentoSgaRequest();
							  request8.setkRegistro(response1.getKoRegistro());
							  request8.setkAmadvDocumento(bean2.getPvNroDocAjusteSga());
							  request8.setkCodigo(response7.getXvStatus());
							  anularDocumentoSga(msgTransaction,request8);

						  }
					  }
					  LOGGER.info(msgTransaction + "ACTIVIDAD 11");
					  SgassListarResultadosRegRequest request9= new SgassListarResultadosRegRequest();
					  request9.setkRegistro(response1.getKoRegistro());
					  SgassListarResultadosRegResponse response9=listarDatosProcesadoNoProcesados(msgTransaction,request9);
					  int inumero=0;
					  LOGGER.info(msgTransaction + "ACTIVIDAD 11 response9.getKoCantOk() "+response9.getKoCantOk());
					  if(response9.getKoCantOk()!=null && !response9.getKoCantOk().isEmpty() && !response9.getKoCantOk().equalsIgnoreCase("null") && !response9.getKoCantOk().equals("0") ) {
						  response.setPoCantOk(response9.getKoCantOk());
						  DetalleAnulacion anuladosOacSga=new DetalleAnulacion();//Anulados en OAC y SGA	
						  List<String> aocSga=correoAnuladosSGA("|"+response9.getKoTramaOk() );
						  String [] tramaFinal=null;
						  
				        for(int i=0;i<aocSga.size();i++) {
				        	tramaFinal=aocSga.get(i).split(";");
				        	anuladosOacSga.setTipo(String.valueOf(Constantes.SEIS));				        	
				        	anuladosOacSga.setNumero(tramaFinal[0]);
				        	anuladosOacSga.setAjuste(tramaFinal[3]);
				        	anuladosOacSga.setDescripcion(String.valueOf(configuration.getProperty(Constantes.ANULAR_OAC_SGA)));        	    	
				        	cAnuladosOacSga += Constantes.SEIS +"; "+tramaFinal[3]+"; "+anuladosOacSga.getDescripcion()+" \n";
				    	    listSgaAnu.add(anuladosOacSga);
				    	    inumero=Integer.parseInt(tramaFinal[0]);
				        }
				        
					  }else {
						  response.setPoCantOk("0");
					  }
					  LOGGER.info(msgTransaction + "ACTIVIDAD 11 response9.getKoCantErr() "+response9.getKoCantErr());
					  if(response9.getKoCantErr()!=null && !response9.getKoCantErr().isEmpty() && !response9.getKoCantErr().equalsIgnoreCase("null") && !response9.getKoCantErr().equals("0")) {
						  response.setPoCantError(response9.getKoCantErr());
						  DetalleAnulacion sinAnular=new DetalleAnulacion();//Sin Anular
						  List<String> error=correoAnuladosSGA("|"+response9.getKoCantErr() );
						  String [] tramaFinalError=null;
					        for(int i=0;i<error.size();i++) {
					        	tramaFinalError=error.get(i).split(";");
					        	sinAnular.setTipo(String.valueOf(Constantes.SEIS));					        	
					        	sinAnular.setNumero(""+(inumero++));
					        	sinAnular.setAjuste(tramaFinalError[0]);
					        	sinAnular.setDescripcion(tramaFinalError[1]);        	    	
					        	cSinAnular += Constantes.SEIS +"; "+ tramaFinalError[0]+"; "+tramaFinalError[1]+" \n";
					    	    listSgaAnu.add(sinAnular);				
					        }
					  }else {
						  response.setPoCantError("0");
					  }

					  response.setPoAjustesOk(response9.getKoTramaOk());
					  response.setPoAjustesError(response9.getKoTramaErr());
					  response.setPoCoderror(response9.getKoCodigo());
					  response.setPoDeserror(response9.getKoMensaje());
					  				  
				  }				  
			  }			  
			  }			  
		  }else if(response1.getKoCodigo()!=null && !response1.getKoCodigo().equalsIgnoreCase("null")){
			  response.setPoCantOk(response1.getKoCantOk());
			  response.setPoCantError(response1.getKoCantErr());
			  response.setPoAjustesError(request.getPiTrama());
			  response.setPoCoderror(response1.getKoCodigo());
			  response.setPoDeserror(response1.getKoMensaje());
			  }
		  //Modificacion 11-09
		  if(listSgaAnu.size()>0){
			  for(int z=0;z<listSgaAnu.size();z++){
				  if(response.getPoDeserror().equalsIgnoreCase(Constantes.PO_COD_OK)){
					  listSgaAnu.get(z).setTipo(String.valueOf(Constantes.UNO));
				  }else{
					  listSgaAnu.get(z).setTipo(String.valueOf(Constantes.DOS));
				  }	
			  }
		  }
		  //Modificacion 11-09
		  response.setDetalleAnulacion(listSgaAnu);	
		  } catch (DBException e) {
				throw new DBException(e); 
			}finally {
				LOGGER.info(msgTransaction + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO," anular SGA");
			}
		  
		  mensajeCorreo = ("Anulados en OAC y SGA \n" + cAnuladosOacSga +  "\n Sin Anular \n" + cSinAnular);
		  envioCorreo(msgTransaction,headerRequest,request,mensajeCorreo); 
		  return response;
	  }
	  private SgasiRegistrarDocAnularResponse validarDocumentoSGA(String msgTransaction,SgasiRegistrarDocAnularRequest request) throws DBException {
		  LOGGER.info(msgTransaction + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,"obtenerLoteProcesoAnular");
		  SgasiRegistrarDocAnularResponse response;
		  try {
				response= sgasiRegistrarDocAnularRepository.anular(msgTransaction,request);
			} catch (DBException e) {
				throw new DBException(e); 
			}finally {
				LOGGER.info(msgTransaction + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,"obtenerLoteProcesoAnular");
			}
			  return response;
	  }
	  private SgasuValidarRegAnularSgaResponse validarRegistroSga(String msgTransaction,SgasuValidarRegAnularSgaRequest request) throws DBException  {
		  LOGGER.info(msgTransaction + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,"validarRegistroSga");
		  SgasuValidarRegAnularSgaResponse response;
		  try {
				response= sgasuValidarRegAnularSgaReposytory.anular(msgTransaction,request);
			} catch (DBException e) {
				throw new DBException(e); 
			}finally {
				LOGGER.info(msgTransaction + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,"validarRegistroSga");
			}
			  return response;
	  }
	  private SgassListarRegValidarOacResponse listarRgistrosValidarOac(String msgTransaction,SgassListarRegValidarOacRequest request) throws DBException{
		  LOGGER.info(msgTransaction + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,"listarRgistrosValidarOac");
		  SgassListarRegValidarOacResponse response;		  
		  try {
				response= sgassListarRegValidarOacReposytory.anular(msgTransaction, request);
			} catch (DBException e) {
				throw new DBException(e); 
			}finally {
				LOGGER.info(msgTransaction + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,"listarRgistrosValidarOac");
			}
		  return response;
	  }
	  private PrValRegistroResponse validarRegistrosOac(String msgTransaction,PrValRegistroRequest request)throws DBException {
		  LOGGER.info(msgTransaction + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,"validarRegistrosOac");
		  PrValRegistroResponse response;		  
		  try {
				response= prValRegistroReposytory.anular(msgTransaction, request);
			} catch (DBException e) {
				throw new DBException(e); 
			}finally {
				LOGGER.info(msgTransaction + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,"validarRegistrosOac");
			}
		  return response;
	  }
	  private SgasuValidaOacResponse actualizarEstadoSga(String msgTransaction,SgasuValidaOacRequest request) throws DBException{
		  
		  LOGGER.info(msgTransaction + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,"actualizarEstadoSga");
		  SgasuValidaOacResponse response;		  
		  try {
				response= sgasuValidaOacReposytory.anular(msgTransaction, request);
			} catch (DBException e) {
				throw new DBException(e); 
			}finally {
				LOGGER.info(msgTransaction + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,"actualizarEstadoSga");
			}
		  return response;
	  }
	  private SgassListarRegAnularOacResponse listarRegistrosAnularOac(String msgTransaction,SgassListarRegAnularOacRequest request) throws DBException  {
		  
		  LOGGER.info(msgTransaction + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,"listarRegistrosAnularOac");
		  SgassListarRegAnularOacResponse response;		  
		  try {
				response= sgassListarRegAnularOacReposytory.anular(msgTransaction, request);
			} catch (DBException e) {
				throw new DBException(e); 
			}finally {
				LOGGER.info(msgTransaction + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,"listarRegistrosAnularOac");
			}
		  return response;
	  }	 
	  private PrPrcInterfaceAjusteResponse anularDocumentoOac(String msgTransaction,PrPrcInterfaceAjusteRequest request) throws DBException {
		  
		  LOGGER.info(msgTransaction + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,"anularDocumentoOac");
		  PrPrcInterfaceAjusteResponse response;		  
		  try {
				response= prPrcInterfaceAjusteReposytory.anular(msgTransaction, request);
			} catch (DBException e) {
				throw new DBException(e); 
			}finally {
				LOGGER.info(msgTransaction + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,"anularDocumentoOac");
			}
		  return response;
	  }
	  private SgasuAnularDocumentoSgaResponse anularDocumentoSga(String msgTransaction,SgasuAnularDocumentoSgaRequest request) throws DBException {
		  
		  LOGGER.info(msgTransaction + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,"anularDocumentoSga");
		  SgasuAnularDocumentoSgaResponse response;		  
		  try {
				response= sgasuAnularDocumentoSgaReposytory.anular(msgTransaction, request);
			} catch (DBException e) {
				throw new DBException(e); 
			}finally {
				LOGGER.info(msgTransaction + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,"anularDocumentoSga");
			}
		  return response;
		  
	  }
	  private SgassListarResultadosRegResponse listarDatosProcesadoNoProcesados(String msgTransaction,SgassListarResultadosRegRequest request) throws DBException {
		  LOGGER.info(msgTransaction + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,"anularDocumentoSga");
		  SgassListarResultadosRegResponse response;		  
		  try {
				response= sgassListarResultadosRegReposytory.anular(msgTransaction, request);
			} catch (DBException e) {
				throw new DBException(e); 
			}finally {
				LOGGER.info(msgTransaction + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,"anularDocumentoSga");
			}
		  return response;
	  }
	  

	 
	 public List<String> correoAnuladosSGA(String trama ) {
		   String[] ptrama = trama.split(Pattern.quote("|"));
		            List<String> ab= new ArrayList<>();
		            for (int i=1;i<ptrama.length;i++) {
		            	ab.add(ptrama[i]);	            		            
		            	}
		            return ab;	 		 
	 }
	 
	  private AnulacionAjusteResponse envioBscs(String msgTransaction,AnulacionAjusteResponse response, 
			  List<LineasAnuladosTipificados> lineasAnuladasTipificadas, AnulacionAjusteRequest request, HeaderRequest headerRequest) throws DBException, WSException{
		  LOGGER.info(msgTransaction + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ENVIAR_BSCS);
		  List<DetalleAnulacion> LisDeAnulacion = new ArrayList<DetalleAnulacion>();
	    try {
	    
	    List<String> lineasAnuladasOk = new ArrayList<String>();
	    NuevaInteraccionResponse interaccionResponse;
	    LOGGER.info(msgTransaction+ " [Actividad 4: anular documento en BSCS]");
	        
	    response = anularAjusteRepository.anular(msgTransaction, request);

	    LOGGER.info(msgTransaction+" Obteniendo lineas ajustes ok");
	    lineasAnuladasOk = ClaroUtil.obtenerTramaOk(response.getPoAjustesOk(), "|");	   
	    
	    for (int p = 0; p < lineasAnuladasTipificadas.size(); p++) {
	        for (String aok : lineasAnuladasOk) {
	          if (lineasAnuladasTipificadas.get(p).getTrama().contains(aok)) {
	            lineasAnuladasTipificadas.get(p).setAnulados(Constantes.ANULADO);
	            LOGGER.info(msgTransaction+" Anuladas OK index {} y valor {}", p, lineasAnuladasTipificadas.get(p).getAnulados());
	          }
	        }
	      }
	    LOGGER.info(msgTransaction+ " [Actividad 5: tipificar lineas anuladas BSCS]");
	    for (int j = 0; j < lineasAnuladasTipificadas.size(); j++) {
	        if (lineasAnuladasTipificadas.get(j).getAnulados().equals(Constantes.ANULADO)) {
	          NuevaInteraccion intRequest = new NuevaInteraccion();
	          intRequest.setTxId(headerRequest.getIdTransaccion());
	          InteraccionType interaccionType = new InteraccionType();
	          interaccionType.setClase(String.valueOf(configuration.getProperty(Constantes.LINEA_ANULADAS_TIPIFICADA_CLASE)));
	          interaccionType.setCodigoEmpleado(String.valueOf(configuration.getProperty(Constantes.LINEA_ANULADAS_TIPIFICADA_CODIGOEMPLEADO)));
	          interaccionType.setCodigoSistema(String.valueOf(configuration.getProperty(Constantes.LINEA_ANULADAS_TIPIFICADA_CODIGOSISTEMA)));
	          interaccionType.setCuenta(request.getPiUsuTipificacion());
	          interaccionType.setFlagCaso(String.valueOf(configuration.getProperty(Constantes.LINEA_ANULADAS_TIPIFICADA_FLAGCASO)));
	          interaccionType.setHechoEnUno(String.valueOf(configuration.getProperty(Constantes.LINEA_ANULADAS_TIPIFICADA_HECHOENUNO)));
	          interaccionType.setMetodoContacto(String.valueOf(configuration.getProperty(Constantes.LINEA_ANULADAS_TIPIFICADA_METODOCONTACTO)));
       
	          String notaTipificacion=request.getPiNotaTipificacion();	  	  
	          String [] at=lineasAnuladasTipificadas.get(j).getTrama().split(";");
	          notaTipificacion=notaTipificacion.replaceAll(String.valueOf(configuration.getProperty(Constantes.LINEA_ANULADAS_TIPIFICADA0)), at[0]);
	          notaTipificacion=notaTipificacion.replaceAll(String.valueOf(configuration.getProperty(Constantes.LINEA_ANULADAS_TIPIFICADA1)), at[1]);
	          notaTipificacion=notaTipificacion.replaceAll(String.valueOf(configuration.getProperty(Constantes.LINEA_ANULADAS_TIPIFICADA2)), at[2]);
	          LOGGER.info(msgTransaction+ " notaTipificacion: "+notaTipificacion);
	          request.setPiNotaTipificacion(notaTipificacion);
		    
	          interaccionType.setNotas(request.getPiNotaTipificacion());
	          interaccionType.setObjId("");
	          interaccionType.setSiteObjId("");
	          LOGGER.info(msgTransaction+ " setSubClase: "+String.valueOf(configuration.getProperty(Constantes.LINEA_ANULADAS_TIPIFICADA_SUBCLASE)));
	          interaccionType.setSubClase(String.valueOf(configuration.getProperty(Constantes.LINEA_ANULADAS_TIPIFICADA_SUBCLASE)));
	          
	          interaccionType.setTelefono((lineasAnuladasTipificadas.get(j)).getLinea());
	          interaccionType.setTextResultado(String.valueOf(configuration.getProperty(Constantes.LINEA_ANULADAS_TIPIFICADA_TEXTRESULTADO)));
	          interaccionType.setTipo(String.valueOf(configuration.getProperty(Constantes.LINEA_ANULADAS_TIPIFICADA_TIPO)));
	          interaccionType.setTipoInteraccion(String.valueOf(configuration.getProperty(Constantes.LINEA_ANULADAS_TIPIFICADA_TIPOINTERACCION)));
	          intRequest.setInteraccion(interaccionType);

	          interaccionResponse = interacciones.nuevaInteraccion(msgTransaction, intRequest);

	          if (interaccionResponse.getAudit().getErrorCode().equals(String.valueOf(configuration.getProperty(Constantes.TRANSACCIONINTERACCIONES_INTERACCION_ERROR)))) {
	            LOGGER.info("Se remueve la siguiente linea: ");
	            (lineasAnuladasTipificadas.get(j)).setAnulados(Constantes.ANULADO_TIPIFICADO);
	            (lineasAnuladasTipificadas.get(j)).setTitpificados(Constantes.TIPIFICADO);
	          } else {
	            (lineasAnuladasTipificadas.get(j)).setTitpificados(Constantes.TIPIFICADO_ERROR);
	          }
	        }
	      }
	    String anuladosOk = "";
	    LOGGER.info(msgTransaction+ " [Actividad 6: actualizar ajuste documento en BSCS]");
	    for (int de = 0; de < lineasAnuladasTipificadas.size(); de++) {
	    	ActualizarAjusteBean actualizarAjusteBean= new ActualizarAjusteBean();
	      DetalleAnulacion deAnulacion = new DetalleAnulacion();
	      String[] ajuste = (lineasAnuladasTipificadas.get(de)).getTrama().split(";");
	      deAnulacion.setAjuste(ajuste[Integer.parseInt(String.valueOf(configuration.getProperty(Constantes.TRAMA_AJUSTE)))]);
	      deAnulacion.setDescripcion(""); 
	      if ((response.getPoAjustesError() != null) && (!response.getPoAjustesError().isEmpty())) {
	    	  deAnulacion.setTipo(String.valueOf(Constantes.CINCO));	
	        deAnulacion.setNumero(ClaroUtil.numeroDescripcion(response.getPoAjustesError(), ajuste[Integer.parseInt(String.valueOf(configuration.getProperty(Constantes.TRAMA_CODIGO)))] + ";" + ajuste[Integer.parseInt(String.valueOf(configuration.getProperty(Constantes.TRAMA_AJUSTE)))], Integer.parseInt(String.valueOf(configuration.getProperty(Constantes.TRAMA_AJUSTE_NUMERO)))));
	        deAnulacion.setDescripcion(ClaroUtil.numeroDescripcion(response.getPoAjustesError(), ajuste[Integer.parseInt(String.valueOf(configuration.getProperty(Constantes.TRAMA_CODIGO)))] + ";" + ajuste[Integer.parseInt(String.valueOf(configuration.getProperty(Constantes.TRAMA_AJUSTE)))], Integer.parseInt(String.valueOf(configuration.getProperty(Constantes.TRAMA_AJUSTE_DESCRIPCION)))));
	      }

	      if ((lineasAnuladasTipificadas.get(de)).getAnulados().equals(Constantes.ANULADO)) {	    	  
	        
	        deAnulacion.setNumero(String.valueOf(de+1));

	        
	        actualizarAjusteBean.setPiAjuste(ajuste[Integer.parseInt(String.valueOf(configuration.getProperty(Constantes.TRAMA_AJUSTE)))]);
	        actualizarAjusteBean.setPiCustomerId(ajuste[Integer.parseInt(String.valueOf(configuration.getProperty(Constantes.TRAMA_CODIGO)))]);
	        actualizarAjusteBean.setPiEstado(String.valueOf(configuration.getProperty(Constantes.VALOR_ANULADO_SIN_TIPICAR)));
	        
	         actualizarAjusteBean= actulizarAjusteRepository.actualizarAjuste(msgTransaction, actualizarAjusteBean);
	         
	         if(actualizarAjusteBean.getPoCoderror()!=null && !actualizarAjusteBean.getPoCoderror().equals("0")) {
	        	 deAnulacion.setTipo(String.valueOf(Constantes.CUATRO));
	        	 deAnulacion.setDescripcion(String.valueOf(configuration.getProperty(Constantes.LINEA_ANULADAS_SINTIPICAR_SINSIOP)));
	         }else {
	        	 deAnulacion.setTipo(String.valueOf(Constantes.TRES));
	        	 deAnulacion.setDescripcion(String.valueOf(configuration.getProperty(Constantes.LINEA_ANULADAS_SINTIPICAR_SIOP)));
	         }
	         anuladosOk = anuladosOk + (lineasAnuladasTipificadas.get(de)).getTrama() + "|";
	      } else if ((lineasAnuladasTipificadas.get(de)).getAnulados().equals(Constantes.ANULADO_TIPIFICADO)) {
	        
	        deAnulacion.setNumero(String.valueOf(de+1));
	        actualizarAjusteBean.setPiAjuste(ajuste[Integer.parseInt(String.valueOf(configuration.getProperty(Constantes.TRAMA_AJUSTE)))]);
	        actualizarAjusteBean.setPiCustomerId(ajuste[Integer.parseInt(String.valueOf(configuration.getProperty(Constantes.TRAMA_CODIGO)))]);
	        actualizarAjusteBean.setPiEstado(String.valueOf(configuration.getProperty(Constantes.VALOR_ANULADO_TIPIFICADO)));
	        
	        actualizarAjusteBean=actulizarAjusteRepository.actualizarAjuste(msgTransaction, actualizarAjusteBean);
	        

	        if(actualizarAjusteBean.getPoCoderror()!=null && !actualizarAjusteBean.getPoCoderror().equals("0")) {
	        	deAnulacion.setTipo(String.valueOf(Constantes.DOS));
	        	 deAnulacion.setDescripcion(String.valueOf(configuration.getProperty(Constantes.LINEA_ANULADAS_TIPICAR_SINSIOP)));
	         }else {
	        	 deAnulacion.setTipo(String.valueOf(Constantes.UNO));
	        	 deAnulacion.setDescripcion(String.valueOf(configuration.getProperty(Constantes.LINEA_ANULADAS_TIPICADO_SIOP)));
	         }
	        anuladosOk = anuladosOk + (lineasAnuladasTipificadas.get(de)).getTrama() + "|";
	      }

	      LisDeAnulacion.add(deAnulacion);

	    }
	      if ((anuladosOk != null) && (!anuladosOk.isEmpty())) {
		      response.setPoAjustesOk(anuladosOk.substring(0, anuladosOk.length() - 1));
		    }
		    response.setDetalleAnulacion(LisDeAnulacion);
	 
	    
	    } catch (DBException e){	     
	    	throw new DBException(e);
	    	
		}catch (WSException e) {
			throw new WSException( e );
		 }	
	    finally {
	    LOGGER.info(msgTransaction + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ENVIAR_BSCS);
		 }
	    return response;
	  }
	}
