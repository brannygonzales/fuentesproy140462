package pe.com.claro.bss.oss.ajustemasivo.domain.repository;
import java.io.Serializable;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;

import org.hibernate.annotations.QueryHints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.com.claro.bss.oss.ajustemasivo.canonical.request.AnulacionAjusteRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.AnulacionAjusteResponse;
import pe.com.claro.bss.oss.ajustemasivo.common.domain.repository.AbstractRepository;
import pe.com.claro.bss.oss.ajustemasivo.model.Formulario;
import pe.com.claro.bss.oss.ajustemasivo.common.property.Constantes;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.DBException;

@Stateless
public class AnularAjusteRepository extends AbstractRepository<Formulario> implements Serializable {
	private static final Logger LOG = LoggerFactory.getLogger(AnularAjusteRepository.class);
	private static final long serialVersionUID = 1079001789833563579L;

	@Context
	private Configuration configuration;
	
	@PersistenceContext(unitName = Constantes.PERSISTENCEPACKAGEUNIT_ANULAR_AJUSTE)
	public void setPersistenceUnit00(final EntityManager em) {
		this.entityManager = em;
	}
	
public AnularAjusteRepository() {
	super(Formulario.class);
}

@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public AnulacionAjusteResponse anular(String mensajeTransaccion,AnulacionAjusteRequest request) throws DBException {
	AnulacionAjusteResponse response=new AnulacionAjusteResponse();
	
	LOG.info(mensajeTransaccion + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ANULAR);
	String OWNER = null;
	String PAQUETE = null;
	String PROCEDURE = null;
	String BD = null;
    
    BD = this.configuration.getProperty(Constantes.NOMBRE_BD_OBTENER).toString();
    OWNER = this.configuration.getProperty(Constantes.OWNER_BD_OBTENER).toString();
    PAQUETE = this.configuration.getProperty(Constantes.NOMBRE_PKG_OBTENER).toString();
    PROCEDURE = this.configuration.getProperty(Constantes.NOMBRE_SP_OBTENER).toString();
	String execute_package = OWNER+ Constantes.SEPARADORPUNTO+PAQUETE+Constantes.SEPARADORPUNTO+PROCEDURE;

	try {
		LOG.info(mensajeTransaccion + Constantes.EJECUTANDO_SP + Constantes.CORCHETE_IZQUIERDO + execute_package + Constantes.CORCHETE_DERECHO);
		LOG.info(mensajeTransaccion + Constantes.PARAMETROS_INPUT);
		LOG.info(mensajeTransaccion + Constantes.PI_TRAMA + request.getPiTrama());
		LOG.info(mensajeTransaccion + Constantes.PI_USUARIO + request.getPiUsuario());
		LOG.info(mensajeTransaccion + Constantes.PI_TIPO_AJUSTE + request.getPiTipoAjuste());
		LOG.info(mensajeTransaccion + Constantes.PI_NOTA_TIPIFICACION + request.getPiNotaTipificacion());
		
		StoredProcedureQuery procedureQuery = entityManager.createStoredProcedureQuery(execute_package);
		procedureQuery.setHint(QueryHints.TIMEOUT_JPA, Integer.parseInt(String.valueOf(configuration.getProperty(Constantes.NOMBRE_TM_OBTENER))));
	procedureQuery.registerStoredProcedureParameter(Constantes.UNO, String.class, ParameterMode.IN);
	procedureQuery.registerStoredProcedureParameter(Constantes.DOS, String.class, ParameterMode.IN);
	procedureQuery.registerStoredProcedureParameter(Constantes.TRES,  String.class, ParameterMode.IN);
	procedureQuery.registerStoredProcedureParameter(Constantes.CUATRO, String.class, ParameterMode.IN);
	procedureQuery.registerStoredProcedureParameter(Constantes.CINCO, Integer.class, ParameterMode.OUT);
	procedureQuery.registerStoredProcedureParameter(Constantes.SEIS, Integer.class, ParameterMode.OUT);
	procedureQuery.registerStoredProcedureParameter(Constantes.SIETE, String.class, ParameterMode.OUT);
	procedureQuery.registerStoredProcedureParameter(Constantes.OCHO, String.class, ParameterMode.OUT);
	procedureQuery.registerStoredProcedureParameter(Constantes.NUEVE, String.class, ParameterMode.OUT);
	procedureQuery.registerStoredProcedureParameter(Constantes.DIEZ, String.class, ParameterMode.OUT);
	
	procedureQuery.setParameter(Constantes.UNO, request.getPiTrama());
	procedureQuery.setParameter(Constantes.DOS, request.getPiUsuario());
	procedureQuery.setParameter(Constantes.TRES, request.getPiTipoAjuste());
	procedureQuery.setParameter(Constantes.CUATRO, request.getPiNotaTipificacion());
	
	procedureQuery.execute();
	
	LOG.info(mensajeTransaccion + Constantes.PARAMETROS_OUTPUT);
	LOG.info(mensajeTransaccion + Constantes.PO_CANT_OK+ procedureQuery.getOutputParameterValue(Constantes.CINCO));	
	LOG.info(mensajeTransaccion + Constantes.PO_CANT_ERROR+ procedureQuery.getOutputParameterValue(Constantes.SEIS));
	LOG.info(mensajeTransaccion + Constantes.PO_AJUSTES_OK+ procedureQuery.getOutputParameterValue(Constantes.SIETE));
	LOG.info(mensajeTransaccion + Constantes.PO_AJUSTES_ERROR+ procedureQuery.getOutputParameterValue(Constantes.OCHO));
	LOG.info(mensajeTransaccion + Constantes.PO_CODERROR+ procedureQuery.getOutputParameterValue(Constantes.NUEVE));
	LOG.info(mensajeTransaccion + Constantes.PO_DESERROR+ procedureQuery.getOutputParameterValue(Constantes.DIEZ));

	response.setPoCantOk(String.valueOf(procedureQuery.getOutputParameterValue(Constantes.CINCO)));
	response.setPoCantError(String.valueOf(procedureQuery.getOutputParameterValue(Constantes.SEIS)));
	response.setPoAjustesOk(String.valueOf(procedureQuery.getOutputParameterValue(Constantes.SIETE)));
	response.setPoAjustesError(String.valueOf(procedureQuery.getOutputParameterValue(Constantes.OCHO)));
	response.setPoCoderror(String.valueOf(procedureQuery.getOutputParameterValue(Constantes.NUEVE)));
	response.setPoDeserror(String.valueOf(procedureQuery.getOutputParameterValue(Constantes.DIEZ)));
	
	
	}catch (Exception ex) {
		String errorMsg = ex + Constantes.TEXTO_VACIO;
	   	 String msjError = null; 
	   	 String codError = null; 
	   	 String nombreBD = BD;
	   	 String nombreSP = PROCEDURE;
	   	 if(errorMsg.contains(Constantes.ERROR_JDBC_TIMEOUT)){
	   		 codError = String.valueOf(configuration.getProperty(Constantes.CODIGO_IDT1));
	   		 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT1)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
	        } else {
	       	 codError =String.valueOf(configuration.getProperty( Constantes.CODIGO_IDT2));
	       	 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT2)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
	        }
		LOG.error( mensajeTransaccion + " ERROR: [Exception BD: "+BD+" ] - [" + ex.getMessage() + "] ",ex );
		throw new DBException(codError,nombreBD,nombreSP,msjError,ex); 
	}finally {
		LOG.info(mensajeTransaccion + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ANULAR);
	}		
	
	return response;
}

@Override
protected Predicate[] getSearchPredicates(Root<Formulario> root, Formulario example) {
	return new Predicate[0];
}

}
