package pe.com.claro.bss.oss.ajustemasivo.domain.repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;

import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.com.claro.bss.oss.ajustemasivo.canonical.request.SgassListarResultadosRegRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.SgassListarResultadosRegResponse;
import pe.com.claro.bss.oss.ajustemasivo.common.domain.repository.AbstractRepository;
import pe.com.claro.bss.oss.ajustemasivo.common.property.Constantes;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.DBException;
import pe.com.claro.bss.oss.ajustemasivo.model.Formulario;

@Stateless
public class SgassListarResultadosRegReposytory extends AbstractRepository<Formulario>{
	private static final Logger LOG = LoggerFactory.getLogger(SgassListarResultadosRegReposytory.class);
	@Context
	private Configuration configuration;
	
	@PersistenceContext(unitName = Constantes.PERSISTENCEPACKAGEUNIT_ANULAR_AJUSTE_SGA)
	public void setPersistenceUnit(final EntityManager em) {
		this.entityManager = em;
	}
	public SgassListarResultadosRegReposytory() {
		super(Formulario.class);
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public SgassListarResultadosRegResponse anular(String mensajeTransaccion,SgassListarResultadosRegRequest request) throws DBException {
		SgassListarResultadosRegResponse response=new SgassListarResultadosRegResponse();
		LOG.info(mensajeTransaccion + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ANULAR);
		
		String OWNER = null;
		String PAQUETE = null;
		String PROCEDURE = null;
		String BD = null;
		
		BD = configuration.getProperty(Constantes.NOMBRE_BD_SGASS_LISTAR_RESULTADOS_REG).toString();
		OWNER = configuration.getProperty(Constantes.OWNER_BD_SGASS_LISTAR_RESULTADOS_REG).toString();
		PAQUETE = configuration.getProperty(Constantes.NOMBRE_PKG_SGASS_LISTAR_RESULTADOS_REG).toString();
		PROCEDURE = configuration.getProperty(Constantes.NOMBRE_SP_SGASS_LISTAR_RESULTADOS_REG).toString();
		String execute_package = OWNER+ Constantes.SEPARADORPUNTO+PAQUETE+Constantes.SEPARADORPUNTO+PROCEDURE;
		try {
			Session session = (Session) this.entityManager.unwrap(Session.class);
			session.doWork(new Work() {
                @Override
                public void execute(Connection connection) throws SQLException {   
                	LOG.info(mensajeTransaccion + Constantes.EJECUTANDO_SP + Constantes.CORCHETE_IZQUIERDO + execute_package+ Constantes.CORCHETE_DERECHO);
        			LOG.info(mensajeTransaccion + Constantes.PARAMETROS_INPUT);
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.K_REGISTRO + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getkRegistro());
        			
        			CallableStatement call = connection.prepareCall("call " + execute_package + " (?,?,?,?,?,?,?)");
        			call.setQueryTimeout(Integer.parseInt(String.valueOf(configuration.getProperty(Constantes.NOMBRE_TM_SGASS_LISTAR_RESULTADOS_REG))));
        			
        			call.setInt(Constantes.UNO, Integer.parseInt(request.getkRegistro()));
        			call.registerOutParameter(Constantes.DOS, Types.INTEGER);
        			call.registerOutParameter(Constantes.TRES, Types.INTEGER);
        			call.registerOutParameter(Constantes.CUATRO,  Types.VARCHAR);
        			call.registerOutParameter(Constantes.CINCO,  Types.VARCHAR);
        			call.registerOutParameter(Constantes.SEIS, Types.INTEGER);
        			call.registerOutParameter(Constantes.SIETE,  Types.VARCHAR);
        			call.execute();
        			
        			LOG.info(mensajeTransaccion + Constantes.PARAMETROS_OUTPUT);
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.KO_CANT_OK + Constantes.CORCHETE_DERECHO +  Constantes.DOS_PUNTOS +call.getObject(Constantes.DOS));
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.KO_CANT_ERR + Constantes.CORCHETE_DERECHO +  Constantes.DOS_PUNTOS +call.getObject(Constantes.TRES));
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.KO_TRAMA_OK + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + call.getObject(Constantes.CUATRO));
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.KO_TRAMA_ERR + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + call.getObject(Constantes.CINCO));
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.KO_CODIGO + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + call.getObject(Constantes.SEIS));
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.KO_MENSAJE + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + call.getObject(Constantes.SIETE));
        			response.setKoCantOk(String.valueOf(call.getObject(Constantes.DOS)));
        			response.setKoCantErr(String.valueOf(call.getObject(Constantes.TRES)));
        			response.setKoTramaOk(String.valueOf(call.getObject(Constantes.CUATRO)));
        			response.setKoTramaErr(String.valueOf(call.getObject(Constantes.CINCO)));
        			response.setKoCodigo(String.valueOf(call.getObject(Constantes.SEIS)));
        			response.setKoMensaje(String.valueOf(call.getObject(Constantes.SIETE)));
                }});
		}catch (Exception ex) {
			String errorMsg = ex + Constantes.TEXTOVACIO;
		   	 String msjError = null; 
		   	 String codError = null; 
		   	 String nombreBD = BD;
		   	 String nombreSP = execute_package;
		   	 if(errorMsg.contains(Constantes.ERROR_JDBC_TIMEOUT)){
		   		 codError = String.valueOf(configuration.getProperty(Constantes.CODIGO_IDT1));
		   		 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT1)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
		        } else {
		       	 codError =String.valueOf(configuration.getProperty( Constantes.CODIGO_IDT2));
		       	 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT2)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
		        }
			LOG.error( mensajeTransaccion + " ERROR: [Exception BD: "+BD+" ] - [" + ex.getMessage() + "] ",ex );
			throw new DBException(codError,nombreBD,nombreSP,msjError,ex);  
		}finally {
			LOG.info(mensajeTransaccion + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ANULAR);
		}
		return response;
	}

		
	
	@Override
	protected Predicate[] getSearchPredicates(Root<Formulario> root, Formulario example) {
		return new Predicate[0];
	}

}
