package pe.com.claro.bss.oss.ajustemasivo.domain.repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;

import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.com.claro.bss.oss.ajustemasivo.canonical.request.PrValRegistroRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.PrValRegistroResponse;
import pe.com.claro.bss.oss.ajustemasivo.common.domain.repository.AbstractRepository;
import pe.com.claro.bss.oss.ajustemasivo.common.property.Constantes;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.DBException;
import pe.com.claro.bss.oss.ajustemasivo.model.Formulario;

@Stateless
public class PrValRegistroReposytory extends AbstractRepository<Formulario> {

	private static final Logger LOG = LoggerFactory.getLogger(PrValRegistroReposytory.class);
	
	@Context
	private Configuration configuration;
	
	@PersistenceContext(unitName = Constantes.PERSISTENCEPACKAGEUNIT_ANULAR_AJUSTE)
	public void setPersistenceUnit00(final EntityManager em) {
		this.entityManager = em;
	}

	public PrValRegistroReposytory() {
		super(Formulario.class);
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public PrValRegistroResponse anular(String mensajeTransaccion,PrValRegistroRequest request) throws DBException{
		PrValRegistroResponse response= new PrValRegistroResponse();
		LOG.info(mensajeTransaccion + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ANULAR);
		String OWNER = null;
		String PAQUETE = null;
		String PROCEDURE = null;
		String BD = null;
		
		BD = configuration.getProperty(Constantes.NOMBRE_BD_PR_VAL_REGISTRO).toString();
		OWNER = configuration.getProperty(Constantes.OWNER_BD_PR_VAL_REGISTRO).toString();
		PAQUETE = configuration.getProperty(Constantes.NOMBRE_PKG_PR_VAL_REGISTRO).toString();
		PROCEDURE = configuration.getProperty(Constantes.NOMBRE_SP_PR_VAL_REGISTRO).toString();
		String execute_package = OWNER+ Constantes.SEPARADORPUNTO+PAQUETE+Constantes.SEPARADORPUNTO+PROCEDURE;
		try {
		Session session = (Session) this.entityManager.unwrap(Session.class);
		session.doWork(new Work() {
            @Override
            public void execute(Connection connection) throws SQLException {
            	LOG.info(mensajeTransaccion + Constantes.EJECUTANDO_SP + Constantes.CORCHETE_IZQUIERDO + execute_package+ Constantes.CORCHETE_DERECHO);
    			LOG.info(mensajeTransaccion + Constantes.PARAMETROS_INPUT);
    			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PV_COD_APLICACION + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPvCodAplicacion());
    			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PV_TIPO_SERVICIO + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPvTipoServicio());
    			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PV_COD_CUENTA + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPvCodCuenta());
    			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PV_TIPO_AJUSTE + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPvTipoAjuste());
    			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PV_NRO_DOC_AJUSTE + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPvNroDocAjuste());
    			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PV_DOCS_AJUSTE + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPvDocsAjuste());
    			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PV_MONEDA_AJUSTE + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPvMonedaAjuste());
    			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PN_MONTO_AJUSTE + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPnMontoAjuste());
    			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PN_SALDO_AJUSTE + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPnSaldoAjuste());
    			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PV_ESTADO + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPvEstado());
    			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PD_FECHA_AJUSTE + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPdFechaAjuste());
    			
    			CallableStatement call = connection.prepareCall("call " + execute_package + " (?,?,?,?,?,?,?,?,?,?,?,?,?)");
    			call.setQueryTimeout(Integer.parseInt(String.valueOf(configuration.getProperty(Constantes.NOMBRE_TM_PR_VAL_REGISTRO))));
    			
    			call.setString(Constantes.UNO,request.getPvCodAplicacion());
    			call.setString(Constantes.DOS,request.getPvTipoServicio());
    			call.setString(Constantes.TRES,request.getPvCodCuenta());
    			call.setString(Constantes.CUATRO,request.getPvTipoAjuste());
    			call.setString(Constantes.CINCO,request.getPvNroDocAjuste());
    			call.setString(Constantes.SEIS,request.getPvDocsAjuste());
    			call.setString(Constantes.SIETE,request.getPvMonedaAjuste());
    			if(request.getPnMontoAjuste()!=null && !request.getPnMontoAjuste().isEmpty() && !request.getPnMontoAjuste().equalsIgnoreCase("null")) {
    				call.setDouble(Constantes.OCHO,Double.parseDouble(request.getPnMontoAjuste()));
    			}else {
    				call.setNull(Constantes.OCHO,Types.DOUBLE);
    			}
    			if(request.getPnSaldoAjuste()!=null && !request.getPnSaldoAjuste().isEmpty() && !request.getPnSaldoAjuste().equalsIgnoreCase("null")) {
    				call.setDouble(Constantes.NUEVE,Double.parseDouble(request.getPnSaldoAjuste()));
    			}else {
    				call.setNull(Constantes.NUEVE,Types.DOUBLE);
    			}
    			call.setString(Constantes.DIEZ,request.getPvEstado());
    			if(request.getPdFechaAjuste()!=null && !request.getPdFechaAjuste().isEmpty() && !request.getPdFechaAjuste().equalsIgnoreCase("null")) {

    				SimpleDateFormat format = new SimpleDateFormat("DD/MM/YYYY");    				
						java.util.Date parsed;
						try {
							parsed = format.parse(request.getPdFechaAjuste());
							call.setDate(Constantes.ONCE,new Date(parsed.getTime()));
						} catch (ParseException ex) {
							String errorMsg = ex + Constantes.TEXTOVACIO;
						   	 String msjError = null; 
						   	 String codError = null; 
						   	 String nombreBD = configuration.getProperty(Constantes.NOMBRE_BD_PR_VAL_REGISTRO).toString();
						   	 String nombreSP = execute_package;
						   	 if(errorMsg.contains(Constantes.ERROR_JDBC_TIMEOUT)){
						   		 codError = String.valueOf(configuration.getProperty(Constantes.CODIGO_IDT1));
						   		 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT1)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
						        } else {
						       	 codError =String.valueOf(configuration.getProperty( Constantes.CODIGO_IDT2));
						       	 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT2)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
						        }
							LOG.error( mensajeTransaccion + " ERROR: [Exception BD: "+nombreBD+" ] - [" + ex.getMessage() + "] ",ex );
						}
    				
    			}else {
    				call.setNull(Constantes.ONCE,Types.DATE);    				
    			}
    			call.registerOutParameter(Constantes.DOCE,Types.VARCHAR);
    			call.registerOutParameter(Constantes.TRECE,Types.VARCHAR);
    			
    			call.execute();
    			
    			LOG.info(mensajeTransaccion + Constantes.PARAMETROS_OUTPUT);
    			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.XV_STATUS + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + call.getObject(Constantes.DOCE));
    			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.XV_MESSAGE + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + call.getObject(Constantes.TRECE));
    			
    			response.setXvStatus(String.valueOf(call.getObject(Constantes.DOCE)));
    			response.setXvMessage(String.valueOf(call.getObject(Constantes.TRECE)));
    	
            }});
	}catch (Exception ex) {
		String errorMsg = ex + Constantes.TEXTOVACIO;
	   	 String msjError = null; 
	   	 String codError = null; 
	   	 String nombreBD = BD;
	   	 String nombreSP = execute_package;
	   	 if(errorMsg.contains(Constantes.ERROR_JDBC_TIMEOUT)){
	   		 codError = String.valueOf(configuration.getProperty(Constantes.CODIGO_IDT1));
	   		 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT1)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
	        } else {
	       	 codError =String.valueOf(configuration.getProperty( Constantes.CODIGO_IDT2));
	       	 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT2)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
	        }
		LOG.error( mensajeTransaccion + " ERROR: [Exception BD: "+BD+" ] - [" + ex.getMessage() + "] ",ex );
		throw new DBException(codError,nombreBD,nombreSP,msjError,ex);  
	}finally {
		LOG.info(mensajeTransaccion + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ANULAR);
	}	
		return response;
		
	}
	@Override
	protected Predicate[] getSearchPredicates(Root<Formulario> root, Formulario example) {
		return new Predicate[0];
	}

}
