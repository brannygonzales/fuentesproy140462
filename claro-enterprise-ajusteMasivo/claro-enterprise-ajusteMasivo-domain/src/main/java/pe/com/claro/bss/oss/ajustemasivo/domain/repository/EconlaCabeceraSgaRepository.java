package pe.com.claro.bss.oss.ajustemasivo.domain.repository;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;

import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.com.claro.bss.oss.ajustemasivo.canonical.request.EncolaCabRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.EncolaCabResponse;
import pe.com.claro.bss.oss.ajustemasivo.common.property.Constantes;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.DBException;

@Stateless
public class EconlaCabeceraSgaRepository implements Serializable {
	private static final Logger LOG = LoggerFactory.getLogger(EconlaCabeceraSgaRepository.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Context
	private Configuration configuration;
	
	@PersistenceContext(unitName = Constantes.PERSISTENCEPACKAGEUNIT_ENCOLARCABECERA_SGA)
	private EntityManager entityManager;
	
	public EconlaCabeceraSgaRepository () {
		
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public EncolaCabResponse encolarCabecera(String mensajeTransaccion,EncolaCabRequest request) throws DBException, Exception {
		EncolaCabResponse response= new EncolaCabResponse();
		LOG.info(mensajeTransaccion + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ENCOLARCABECERA);
		
		String OWNER = null;
		String PAQUETE = null;
		String PROCEDURE = null;
		String BD = null;

		BD = configuration.getProperty(Constantes.NOMBRE_BD_ENCOLARCABECERA_SGA).toString();
		OWNER = configuration.getProperty(Constantes.OWNER_BD_ENCOLARCABECERA_SGA).toString();
		PAQUETE = configuration.getProperty(Constantes.NOMBRE_PKG_ENCOLARCABECERA_SGA).toString();
		PROCEDURE = configuration.getProperty(Constantes.NOMBRE_SP_ENCOLARCABECERA_SGA).toString();
		String execute_package = OWNER+ Constantes.SEPARADORPUNTO+PAQUETE+Constantes.SEPARADORPUNTO+PROCEDURE;
		try {
			Session session = (Session) this.entityManager.unwrap(Session.class);
			session.doWork(new Work() {
                @Override
                public void execute(Connection connection) throws SQLException {
			LOG.info(mensajeTransaccion + Constantes.EJECUTANDO_SP + Constantes.CORCHETE_IZQUIERDO + execute_package+ Constantes.CORCHETE_DERECHO);
			LOG.info(mensajeTransaccion + Constantes.PARAMETROS_INPUT);
			
			LOG.info(mensajeTransaccion +Constantes.K_TIPO +request.getPiCodAjuste() );
			LOG.info(mensajeTransaccion +Constantes.K_TIPODOC + request.getPiTipoAjuste());
			LOG.info(mensajeTransaccion +Constantes.K_TIPIF  );
			LOG.info(mensajeTransaccion +Constantes.K_TIPIF_DET  );
			LOG.info(mensajeTransaccion +Constantes.K_NUMREG + request.getPiTotalReg() );
			LOG.info(mensajeTransaccion +Constantes.K_CORREO +request.getPiCorreoDestino() );
			LOG.info(mensajeTransaccion +Constantes.K_CODAPLI +request.getPiCodAplicacion() );
			LOG.info(mensajeTransaccion +Constantes.K_USUARIO +request.getPiUsuario() );
			LOG.info(mensajeTransaccion +Constantes.K_ESTADO +request.getPiEstado() );			
			LOG.info(mensajeTransaccion +Constantes.K_NOMBRE_ARCHIVO +request.getPiNombreArchivo() );
			CallableStatement call = connection.prepareCall("call " + execute_package + " (?,?,?,?,?,?,?,?,?,?,?,?,?)");
			call.setQueryTimeout(Integer.parseInt(String.valueOf(configuration.getProperty(Constantes.NOMBRE_TM_ENCOLARCABECERA_SGA))));
			call.setString(Constantes.UNO,request.getPiCodAjuste());
			call.setString(Constantes.DOS,request.getPiTipoAjuste());
			call.setNull(Constantes.TRES, Types.NULL);
			call.setNull(Constantes.CUATRO,Types.NULL);
			call.setString(Constantes.CINCO, request.getPiTotalReg());
			call.setString(Constantes.SEIS,request.getPiCorreoDestino());
			call.setString(Constantes.SIETE,request.getPiCodAplicacion());
			call.setString(Constantes.OCHO,request.getPiUsuario());
			call.setString(Constantes.NUEVE,request.getPiEstado());
			call.setString(Constantes.DIEZ,request.getPiNombreArchivo());
			call.registerOutParameter(Constantes.ONCE,Types.INTEGER);
			call.registerOutParameter(Constantes.DOCE,Types.INTEGER);
			call.registerOutParameter(Constantes.TRECE,Types.VARCHAR);
			
			call.execute();
			LOG.info(mensajeTransaccion + Constantes.PARAMETROS_OUTPUT);
			LOG.info(mensajeTransaccion +Constantes.KO_REGISTRO + call.getObject(Constantes.ONCE));
			LOG.info(mensajeTransaccion +Constantes.KO_CODIGO +call.getObject(Constantes.DOCE));
			LOG.info(mensajeTransaccion +Constantes.KO_MENSAJE +call.getObject(Constantes.TRECE));
			
			response.setPoIdXLS(String.valueOf(call.getObject(Constantes.ONCE)));
			response.setPoCoderror(String.valueOf(call.getObject(Constantes.DOCE)));
			response.setPoDeserror(String.valueOf(call.getObject(Constantes.TRECE)));
                }});
		}catch (Exception ex) {
			String errorMsg = ex + Constantes.TEXTOVACIO;
		   	 String msjError = null; 
		   	 String codError = null; 
		   	 String nombreBD = BD;
		   	 String nombreSP = execute_package;
		   	 if(errorMsg.contains(Constantes.ERROR_JDBC_TIMEOUT)){
		   		 codError = String.valueOf(configuration.getProperty(Constantes.CODIGO_IDT1));
		   		 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT1)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
		        } else {
		       	 codError =String.valueOf(configuration.getProperty( Constantes.CODIGO_IDT2));
		       	 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT2)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
		        }
			LOG.error( mensajeTransaccion + " ERROR: [Exception BD: "+BD+" ] - [" + ex.getMessage() + "] ",ex );
			throw new DBException(codError,nombreBD,nombreSP,msjError,ex);  
		}finally {
			LOG.info(mensajeTransaccion + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ENCOLARCABECERA);
		}		
		
		return response;	
	}
}
