package pe.com.claro.bss.oss.ajustemasivo.domain.repository;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;

import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.com.claro.bss.oss.ajustemasivo.canonical.request.PrPrcInterfaceAjusteRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.PrPrcInterfaceAjusteResponse;
import pe.com.claro.bss.oss.ajustemasivo.common.domain.repository.AbstractRepository;
import pe.com.claro.bss.oss.ajustemasivo.common.property.Constantes;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.DBException;
import pe.com.claro.bss.oss.ajustemasivo.model.Formulario;

@Stateless
public class PrPrcInterfaceAjusteReposytory extends AbstractRepository<Formulario> implements Serializable{
	private static final Logger LOG = LoggerFactory.getLogger(PrPrcInterfaceAjusteReposytory.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = -5262625782150312365L;
	@Context
	private Configuration configuration;
	
	@PersistenceContext(unitName = Constantes.PERSISTENCEPACKAGEUNIT_ANULAR_AJUSTE)
	public void setPersistenceUnit00(final EntityManager em) {
		this.entityManager = em;
	}
	public PrPrcInterfaceAjusteReposytory(){
		super(Formulario.class);
	}
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public PrPrcInterfaceAjusteResponse anular(String mensajeTransaccion,PrPrcInterfaceAjusteRequest request) throws DBException {
		PrPrcInterfaceAjusteResponse response= new PrPrcInterfaceAjusteResponse();
		LOG.info(mensajeTransaccion + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ANULAR);
		
		String OWNER = null;
		String PAQUETE = null;
		String PROCEDURE = null;
		String BD = null;
		
		BD = configuration.getProperty(Constantes.NOMBRE_BD_PR_PRC_INTERFACE_AJUSTE).toString();
		OWNER = configuration.getProperty(Constantes.OWNER_BD_PR_PRC_INTERFACE_AJUSTE).toString();
		PAQUETE = configuration.getProperty(Constantes.NOMBRE_PKG_PR_PRC_INTERFACE_AJUSTE).toString();
		PROCEDURE = configuration.getProperty(Constantes.NOMBRE_SP_PR_PRC_INTERFACE_AJUSTE).toString();
		String execute_package = OWNER+ Constantes.SEPARADORPUNTO+PAQUETE+Constantes.SEPARADORPUNTO+PROCEDURE;

		try {
			Session session = (Session) this.entityManager.unwrap(Session.class);
			session.doWork(new Work() {
                @Override
                public void execute(Connection connection) throws SQLException {
                	LOG.info(mensajeTransaccion + Constantes.EJECUTANDO_SP + Constantes.CORCHETE_IZQUIERDO + execute_package+ Constantes.CORCHETE_DERECHO);
        			LOG.info(mensajeTransaccion + Constantes.PARAMETROS_INPUT);
        			
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PV_TRX_ID_WS + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPvTrxIdWs());
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PV_COD_APLICACION + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPvCodAplicacion());
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PV_USUARIO_APLIC + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPvUsuarioAplic());
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PV_TIPO_SERVICIO + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPvTipoServicio());
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PV_COD_CUENTA + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPvCodCuenta());
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PV_TIPO_OPERACION + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPvTipoOperacion());
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PV_TIPO_AJUSTE + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPvTipoAjuste());
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PV_NRO_DOC_AJUSTE + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPvNroDocAjuste());
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PV_DOCS_AJUSTE + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPvDocsAjuste());
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PV_ID_RECLAMO_ORIGEN + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPvIdReclamoOrigen());
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PV_MONEDA_AJUSTE + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPvMonedaAjuste());
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PN_MONTO_AJUSTE + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPnMontoAjuste());
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PN_SALDO_AJUSTE + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPnSaldoAjuste());
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PV_ESTADO + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPvEstado());
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PV_COD_MOTIVO_AJUSTE + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPvCodMotivoAjuste());
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PV_DESCRIP_AJUSTE + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPvDescripAjuste());
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PD_FECHA_AJUSTE + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPdFechaAjuste());
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PD_FECHA_VENC_AJUSTE + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPdFechaVencAjuste());
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.PD_FECHA_CANCELACION + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + request.getPdFechaCancelacion());        					
        			
        			CallableStatement call = connection.prepareCall("call " + execute_package + " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        			call.setQueryTimeout(Integer.parseInt(String.valueOf(configuration.getProperty(Constantes.NOMBRE_TM_PR_PRC_INTERFACE_AJUSTE))));
        			call.setString(Constantes.UNO,request.getPvTrxIdWs());
        			call.setString(Constantes.DOS,request.getPvCodAplicacion());
        			call.setString(Constantes.TRES,request.getPvUsuarioAplic());
        			call.setString(Constantes.CUATRO,request.getPvTipoServicio());
        			call.setString(Constantes.CINCO,request.getPvCodCuenta());
        			call.setString(Constantes.SEIS,request.getPvTipoOperacion());
        			call.setString(Constantes.SIETE,request.getPvTipoAjuste());
        			call.setString(Constantes.OCHO,request.getPvNroDocAjuste());
        			call.setString(Constantes.NUEVE,request.getPvDocsAjuste());
        			call.setString(Constantes.DIEZ,request.getPvIdReclamoOrigen());
        			call.setString(Constantes.ONCE,request.getPvMonedaAjuste());
        			if(request.getPnMontoAjuste()!=null && !request.getPnMontoAjuste().isEmpty() && !request.getPnMontoAjuste().equalsIgnoreCase("null")) {
        				call.setDouble(Constantes.DOCE,Double.parseDouble(request.getPnMontoAjuste()));
        			}else {
        				call.setNull(Constantes.DOCE,Types.DOUBLE);
        			}
        			if(request.getPnSaldoAjuste()!=null && !request.getPnSaldoAjuste().isEmpty() && !request.getPnSaldoAjuste().equalsIgnoreCase("null")) {
        				call.setDouble(Constantes.TRECE,Double.parseDouble(request.getPnSaldoAjuste()));
        			}else {
        				call.setNull(Constantes.TRECE,Types.DOUBLE);
        			}
        			
        			call.setString(Constantes.CATORCE,request.getPvEstado());
        			call.setString(Constantes.QUINCE,request.getPvCodMotivoAjuste());
        			call.setString(Constantes.DIECISEIS,request.getPvDescripAjuste());
        			if(request.getPdFechaAjuste()!=null && !request.getPdFechaAjuste().isEmpty() && !request.getPdFechaAjuste().equalsIgnoreCase("null")
        					) {       				
        				
        				SimpleDateFormat format = new SimpleDateFormat("DD/MM/YYYY");    				
						java.util.Date parsed;
						try {
							parsed = format.parse(request.getPdFechaAjuste());
							call.setDate(Constantes.DIECISIETE,new Date(parsed.getTime()));
						} catch (ParseException ex) {
							String errorMsg = ex + Constantes.TEXTOVACIO;
						   	 String msjError = null; 
						   	 String codError = null; 
						   	 String nombreBD = configuration.getProperty(Constantes.NOMBRE_BD_PR_VAL_REGISTRO).toString();
						   	 String nombreSP = execute_package;
						   	 if(errorMsg.contains(Constantes.ERROR_JDBC_TIMEOUT)){
						   		 codError = String.valueOf(configuration.getProperty(Constantes.CODIGO_IDT1));
						   		 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT1)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
						        } else {
						       	 codError =String.valueOf(configuration.getProperty( Constantes.CODIGO_IDT2));
						       	 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT2)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
						        }
							LOG.error( mensajeTransaccion + " ERROR: [Exception BD: "+nombreBD+" ] - [" + ex.getMessage() + "] ",ex );
						}
        			      
        			}else {
        				call.setNull(Constantes.DIECISIETE,Types.DATE);
        			}
        			if(request.getPdFechaVencAjuste()!=null && !request.getPdFechaVencAjuste().isEmpty() && !request.getPdFechaVencAjuste().equalsIgnoreCase("null")
        					) {
        				SimpleDateFormat format = new SimpleDateFormat("DD/MM/YYYY");    				
						java.util.Date parsed;
						try {
							parsed = format.parse(request.getPdFechaVencAjuste());
							call.setDate(Constantes.DIECIOCHO,new Date(parsed.getTime()));
						} catch (ParseException ex) {
							String errorMsg = ex + Constantes.TEXTOVACIO;
						   	 String msjError = null; 
						   	 String codError = null; 
						   	 String nombreBD = configuration.getProperty(Constantes.NOMBRE_BD_PR_VAL_REGISTRO).toString();
						   	 String nombreSP = execute_package;
						   	 if(errorMsg.contains(Constantes.ERROR_JDBC_TIMEOUT)){
						   		 codError = String.valueOf(configuration.getProperty(Constantes.CODIGO_IDT1));
						   		 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT1)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
						        } else {
						       	 codError =String.valueOf(configuration.getProperty( Constantes.CODIGO_IDT2));
						       	 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT2)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
						        }
							LOG.error( mensajeTransaccion + " ERROR: [Exception BD: "+nombreBD+" ] - [" + ex.getMessage() + "] ",ex );
						}
        			}else {
        				call.setNull(Constantes.DIECIOCHO,Types.DATE);
        			}
        			if(request.getPdFechaCancelacion()!=null && !request.getPdFechaCancelacion().isEmpty() && !request.getPdFechaCancelacion().equalsIgnoreCase("null")
        					) {
        				SimpleDateFormat format = new SimpleDateFormat("DD/MM/YYYY");    				
						java.util.Date parsed;
						try {
							parsed = format.parse(request.getPdFechaCancelacion());
							call.setDate(Constantes.DIECINUEVE,new Date(parsed.getTime()));
						} catch (ParseException ex) {
							String errorMsg = ex + Constantes.TEXTOVACIO;
						   	 String msjError = null; 
						   	 String codError = null; 
						   	 String nombreBD = configuration.getProperty(Constantes.NOMBRE_BD_PR_VAL_REGISTRO).toString();
						   	 String nombreSP = execute_package;
						   	 if(errorMsg.contains(Constantes.ERROR_JDBC_TIMEOUT)){
						   		 codError = String.valueOf(configuration.getProperty(Constantes.CODIGO_IDT1));
						   		 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT1)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
						        } else {
						       	 codError =String.valueOf(configuration.getProperty( Constantes.CODIGO_IDT2));
						       	 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT2)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
						        }
							LOG.error( mensajeTransaccion + " ERROR: [Exception BD: "+nombreBD+" ] - [" + ex.getMessage() + "] ",ex );
						}
        			}else {
        				call.setNull(Constantes.DIECINUEVE,Types.DATE);
        			}
        			
        			call.registerOutParameter(Constantes.VEINTE,Types.VARCHAR);
        			call.registerOutParameter(Constantes.VEINTIUNO,Types.VARCHAR);
        			
        			call.execute();
        			
        			LOG.info(mensajeTransaccion + Constantes.PARAMETROS_OUTPUT);
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.XV_STATUS + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + call.getObject(Constantes.VEINTE));
        			LOG.info(mensajeTransaccion +" "+ Constantes.CORCHETE_IZQUIERDO + Constantes.XV_MESSAGE + Constantes.CORCHETE_DERECHO + Constantes.DOS_PUNTOS + call.getObject(Constantes.VEINTIUNO));
        			
        			response.setXvStatus(String.valueOf(call.getObject(Constantes.VEINTE)));
        			response.setXvMessage(String.valueOf(call.getObject(Constantes.VEINTIUNO)));
        	
                }});
		}catch (Exception ex) {
			String errorMsg = ex + Constantes.TEXTOVACIO;
		   	 String msjError = null; 
		   	 String codError = null; 
		   	 String nombreBD = BD;
		   	 String nombreSP = execute_package;
		   	 if(errorMsg.contains(Constantes.ERROR_JDBC_TIMEOUT)){
		   		 codError = String.valueOf(configuration.getProperty(Constantes.CODIGO_IDT1));
		   		 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT1)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
		        } else {
		       	 codError =String.valueOf(configuration.getProperty( Constantes.CODIGO_IDT2));
		       	 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT2)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
		        }
			LOG.error( mensajeTransaccion + " ERROR: [Exception BD: "+BD+" ] - [" + ex.getMessage() + "] ",ex );
			throw new DBException(codError,nombreBD,nombreSP,msjError,ex);  
		}finally {
			LOG.info(mensajeTransaccion + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ANULAR);
		}	
		return response;
	}

	@Override
	protected Predicate[] getSearchPredicates(Root<Formulario> root, Formulario example) {
		return new Predicate[0];
	}

}
