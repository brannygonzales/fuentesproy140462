package pe.com.claro.bss.oss.ajustemasivo.domain.repository;

import java.math.BigDecimal;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;

import org.hibernate.annotations.QueryHints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.com.claro.bss.oss.ajustemasivo.canonical.request.EncolarDetalleRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.EncolarDetalleResponse;
import pe.com.claro.bss.oss.ajustemasivo.common.domain.repository.AbstractRepository;
import pe.com.claro.bss.oss.ajustemasivo.common.property.Constantes;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.DBException;
import pe.com.claro.bss.oss.ajustemasivo.model.Formulario;

@Stateless
public class EncolarDetalleBscsRepository extends AbstractRepository<Formulario> {
	private static final Logger LOG = LoggerFactory.getLogger(EncolarDetalleBscsRepository.class);
	@Context
	private Configuration configuration;
	
	@PersistenceContext(unitName = Constantes.PERSISTENCEPACKAGEUNIT_DETALLE)
	public void setPersistenceUnit(final EntityManager em) {
		this.entityManager = em;
	}
	public EncolarDetalleBscsRepository() {
		super(Formulario.class);
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public EncolarDetalleResponse encolarDetalle(String mensajeTransaccion, EncolarDetalleRequest request)
			throws DBException, Exception {
		EncolarDetalleResponse response= new EncolarDetalleResponse();
LOG.info(mensajeTransaccion + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ENCOLARDETALLE);
		
		String OWNER = null;
		String PAQUETE = null;
		String PROCEDURE = null;
		String BD = null;
		OWNER = configuration.getProperty(Constantes.OWNER_BD_DETALLE).toString();
		BD = configuration.getProperty(Constantes.NOMBRE_BD_DETALLE).toString();
		PAQUETE = configuration.getProperty(Constantes.NOMBRE_PKG_DETALLE).toString();
		PROCEDURE = configuration.getProperty(Constantes.NOMBRE_SP_DETALLE).toString();
		String execute_package = OWNER+ Constantes.SEPARADORPUNTO+PAQUETE+Constantes.SEPARADORPUNTO+PROCEDURE;

		try {
			
			LOG.info(mensajeTransaccion + Constantes.EJECUTANDO_SP + Constantes.CORCHETE_IZQUIERDO + execute_package + Constantes.CORCHETE_DERECHO);
			LOG.info(mensajeTransaccion + Constantes.PARAMETROS_INPUT);
			LOG.info(mensajeTransaccion + Constantes.PI_IDXLS +request.getPiIdXLS());
			LOG.info(mensajeTransaccion + Constantes.PI_COD_AJUSTE +request.getPiCodAjuste());
			LOG.info(mensajeTransaccion + Constantes.PI_TRAMA +request.getPiTrama());
			StoredProcedureQuery procedureQuery = entityManager.createStoredProcedureQuery(execute_package);
			procedureQuery.setHint(QueryHints.TIMEOUT_JPA, Integer.parseInt(String.valueOf(configuration.getProperty(Constantes.NOMBRE_TM_DETALLE))));
			procedureQuery.registerStoredProcedureParameter(Constantes.UNO, BigDecimal.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter(Constantes.DOS, String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter(Constantes.TRES,  String.class, ParameterMode.IN);
			procedureQuery.registerStoredProcedureParameter(Constantes.CUATRO, BigDecimal.class, ParameterMode.OUT);
			procedureQuery.registerStoredProcedureParameter(Constantes.CINCO, BigDecimal.class, ParameterMode.OUT);
			procedureQuery.registerStoredProcedureParameter(Constantes.SEIS, String.class, ParameterMode.OUT);
			procedureQuery.registerStoredProcedureParameter(Constantes.SIETE, String.class, ParameterMode.OUT);
			procedureQuery.registerStoredProcedureParameter(Constantes.OCHO, String.class, ParameterMode.OUT);
			

			procedureQuery.setParameter(Constantes.UNO, BigDecimal.valueOf(Double.valueOf(request.getPiIdXLS())));	
			procedureQuery.setParameter(Constantes.DOS, request.getPiCodAjuste());		
			procedureQuery.setParameter(Constantes.TRES, request.getPiTrama());
			
			procedureQuery.execute();
			
			LOG.info(mensajeTransaccion + Constantes.PARAMETROS_OUTPUT);
			LOG.info(mensajeTransaccion + Constantes.PO_CANT_OK + String.valueOf(procedureQuery.getOutputParameterValue(Constantes.CUATRO)));
			LOG.info(mensajeTransaccion + Constantes.PO_CANT_ERROR +String.valueOf(procedureQuery.getOutputParameterValue(Constantes.CINCO)));
			LOG.info(mensajeTransaccion + Constantes.PO_AJUSTES_ERROR +String.valueOf(procedureQuery.getOutputParameterValue(Constantes.SEIS)));
			LOG.info(mensajeTransaccion + Constantes.PO_CODERROR +String.valueOf(procedureQuery.getOutputParameterValue(Constantes.SIETE)));
			LOG.info(mensajeTransaccion + Constantes.PO_DESERROR +String.valueOf(procedureQuery.getOutputParameterValue(Constantes.OCHO)));
			
			response.setPoCantOk(String.valueOf(procedureQuery.getOutputParameterValue(Constantes.CUATRO)));
			response.setPoCantError(String.valueOf(procedureQuery.getOutputParameterValue(Constantes.CINCO)));
			response.setPoAjustesError(String.valueOf(procedureQuery.getOutputParameterValue(Constantes.SEIS)));		
			response.setPoCoderror(String.valueOf(procedureQuery.getOutputParameterValue(Constantes.SIETE)));
			response.setPoDeserror(String.valueOf(procedureQuery.getOutputParameterValue(Constantes.OCHO)));
			
			
		}catch (Exception ex) {
			String errorMsg = ex + Constantes.TEXTOVACIO;
		   	 String msjError = null; 
		   	 String codError = null; 
		   	 String nombreBD = BD;
		   	 String nombreSP = PROCEDURE;
		   	 if(errorMsg.contains(Constantes.ERROR_JDBC_TIMEOUT)){
		   		 codError = String.valueOf(configuration.getProperty(Constantes.CODIGO_IDT1));
		   		 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT1)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
		        } else {
		       	 codError =String.valueOf(configuration.getProperty( Constantes.CODIGO_IDT2));
		       	 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT2)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
		        }
			LOG.error( mensajeTransaccion + " ERROR: [Exception BD: "+BD+" ] - [" + ex.getMessage() + "] ",ex );
			throw new DBException(codError,nombreBD,nombreSP,msjError,ex);  
		}finally {
			LOG.info(mensajeTransaccion + " ====== [Fin] En {} ====== ",Constantes.METODO_ENCOLARDETALLE);
		}	
		return response;
	}

	@Override
	protected Predicate[] getSearchPredicates(Root<Formulario> root, Formulario example) {
		return new Predicate[0];
	}

}
