package pe.com.claro.bss.oss.ajustemasivo.domain.service;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.com.claro.bss.oss.ajustemasivo.canonical.request.ListaProcesoRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.ListaProcesoResponse;
import pe.com.claro.bss.oss.ajustemasivo.common.property.Constantes;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.DBException;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.WSException;
import pe.com.claro.bss.oss.ajustemasivo.domain.repository.ListaProcesoRepository;

@Stateless
public class ListaProcesoService {
	private static final Logger LOG = LoggerFactory.getLogger(ListaProcesoService.class);

	@Context
	private Configuration configuration;
	
	@Resource
	private EJBContext context;
	
	@EJB
	private ListaProcesoRepository repository;
	


	public ListaProcesoResponse listaProceso(String mensajeTransaccion,ListaProcesoRequest request) throws WSException, DBException {
		ListaProcesoResponse response = new ListaProcesoResponse();
		LOG.info(mensajeTransaccion + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_LISTAPROCESO);
		try {
			for(int i=0; i<4;i++) {
				LOG.info(Constantes.PPOPROCESO_LISTA+i+": "+String.valueOf(configuration.getProperty(Constantes.PPOPROCESO_LISTA+i)));
				
			response=repository.listaProceso(mensajeTransaccion, request,String.valueOf(configuration.getProperty(Constantes.PPOPROCESO_LISTA+i)));
if(response.getpFinalizadoOK().equals("1") || response.getpFinalizadoError().equals("1") || response.getpEnEjecucion().equals("1")||response.getpPendiente().equals("1")) {
	return response;
}
			}
			//
		}catch (DBException e) {			
			String msjError = e.getMessage(); 
	    	String codError = e.getCodigoError(); 	    	
	    	throw new DBException(codError,msjError,e);  
		}		
		catch (Exception ex) {
			String nombreWS = String.valueOf(configuration.getProperty(Constantes.WSSERVICEENPOINTADDRESS_LISTAPROCESO));
			String msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT3)).replace("$ws", nombreWS) + " " +ex.getMessage() ; 
	    	String codError = String.valueOf(configuration.getProperty(Constantes.CODIGO_IDT3));    
	    	
	    	LOG.error( mensajeTransaccion + " ERROR: [Exception WS: "+nombreWS+" ] - [" + ex.getMessage() + "] ",ex );
	    	throw new WSException( codError, msjError, nombreWS, ex );
		} finally {
			LOG.info(mensajeTransaccion + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_LISTAPROCESO);
		}
		return response;
		
	}
}
