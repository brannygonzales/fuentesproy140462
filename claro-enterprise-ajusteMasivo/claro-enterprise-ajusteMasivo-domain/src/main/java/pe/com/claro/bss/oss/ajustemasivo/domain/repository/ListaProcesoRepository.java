package pe.com.claro.bss.oss.ajustemasivo.domain.repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;

import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import oracle.jdbc.OracleTypes;
import pe.com.claro.bss.oss.ajustemasivo.canonical.bean.ListaProcesoBean;
import pe.com.claro.bss.oss.ajustemasivo.canonical.request.ListaProcesoRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.ListaProcesoResponse;
import pe.com.claro.bss.oss.ajustemasivo.common.domain.repository.AbstractRepository;
import pe.com.claro.bss.oss.ajustemasivo.common.property.Constantes;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.DBException;
import pe.com.claro.bss.oss.ajustemasivo.model.Formulario;

@Stateless
public class ListaProcesoRepository extends AbstractRepository<Formulario>{
	private static final Logger LOG = LoggerFactory.getLogger(ListaProcesoRepository.class);

	  @Context
	  private  Configuration configuration;

	  @PersistenceContext(unitName=Constantes.PERSISTENCEPACKAGEUNIT_LINEAPROCESO)
	  public void setPersistenceUnit00(final EntityManager em) {
			this.entityManager = em;
		}
	  public ListaProcesoRepository() {
			super(Formulario.class);
		}

@SuppressWarnings({ "unused" })
public ListaProcesoResponse	listaProceso(String mensajeTransaccion,ListaProcesoRequest request,String estado)throws DBException {
	 LOG.info(mensajeTransaccion + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_LISTAPROCESO);
	 ListaProcesoResponse response = new ListaProcesoResponse();
	  List<ListaProcesoBean> listPoCursor= new ArrayList<>();
	  ListaProcesoBean lbean=null;
	  List<Object[]> resultList = new ArrayList<>();
	   String OWNER = null;
	   String PAQUETE = null;
	   String PROCEDURE = null;
	   String BD = null;
	
		  response.setpFinalizadoOK("0");    		
		  response.setpFinalizadoError("0");
		  response.setpEnEjecucion("0");
		  response.setpPendiente("0");
   
	OWNER = configuration.getProperty(Constantes.OWNER_BD_LISTAPROCESO).toString();
	BD = configuration.getProperty(Constantes.NOMBRE_BD_LISTAPROCESO).toString();
	PAQUETE = configuration.getProperty(Constantes.NOMBRE_PKG_LISTAPROCESO).toString();
	PROCEDURE = configuration.getProperty(Constantes.NOMBRE_SP_LISTAPROCESO).toString();
   String execute_package = OWNER + "." + PAQUETE + "." + PROCEDURE;
	try {
		Session session = (Session) this.entityManager.unwrap(Session.class);
		session.doWork(new Work() {


			@Override
            public void execute(Connection connection) throws SQLException {
				
            	CallableStatement call = connection.prepareCall("call " + execute_package + " (?,?,?,?,?,?,?,?,?)");
            	call.setQueryTimeout(Integer.parseInt(String.valueOf(configuration.getProperty(Constantes.NOMBRE_TM_LISTAPROCESO))));
           	 LOG.info(mensajeTransaccion + Constantes.EJECUTANDO_SP + Constantes.CORCHETE_IZQUIERDO + execute_package + Constantes.CORCHETE_DERECHO);
           	 LOG.info(mensajeTransaccion + Constantes.PARAMETROS_INPUT);
           	 LOG.info(mensajeTransaccion + Constantes.P_TIPOPROCESO + request.getpTipoproceso());
    	     LOG.info(mensajeTransaccion + Constantes.P_TIPOSERVICIO+ request.getpTiposervicio());
    	     LOG.info(mensajeTransaccion + Constantes.P_ARCHIVO+ request.getpArchivo());


    	     call.setString(Constantes.UNO, request.getpTipoproceso());
    	     call.setString(Constantes.DOS, request.getpTiposervicio());
    	     call.setString(Constantes.TRES, estado);
    	     call.setString(Constantes.CUATRO, request.getpArchivo());
    	     call.setNull(Constantes.CINCO, Types.NULL);
    	     call.setNull(Constantes.SEIS, Types.NULL);    	     
    	     
    	     call.registerOutParameter(Constantes.SIETE, OracleTypes.CURSOR);
    	     call.registerOutParameter(Constantes.OCHO,  Types.INTEGER);
    	     call.registerOutParameter(Constantes.NUEVE,  Types.VARCHAR);
    	     
    	     call.execute();
    	  // get refcursor and convert it to ResultSet
             ResultSet resultSet = (ResultSet) call.getObject(Constantes.SIETE);
           
    	  if(resultSet!=null) {
    		 
    		 List<ListaProcesoBean> lproceso= new ArrayList<>();
    		  while(resultSet.next()) {
    			  ListaProcesoBean listaProcesoBean= new ListaProcesoBean();
    			  
    			  listaProcesoBean.setSioptnCodproceso(String.valueOf(resultSet.getLong(Constantes.UNO)));
    			  listaProcesoBean.setTipoProceso(String.valueOf(resultSet.getString(Constantes.DOS)));
    			  listaProcesoBean.setTipoServicio(String.valueOf(resultSet.getString(Constantes.TRES)));
    			  listaProcesoBean.setArchivo(String.valueOf(resultSet.getString(Constantes.CUATRO)));
    			  listaProcesoBean.setEstado(String.valueOf(resultSet.getString(Constantes.CINCO)));
    			  listaProcesoBean.setSioptdFecharegistro(String.valueOf(resultSet.getObject(Constantes.SEIS)));
    			  listaProcesoBean.setSioptvUsuario(String.valueOf(resultSet.getString(Constantes.SIETE)));
    			  listaProcesoBean.setSioptvCantRegOk(String.valueOf(resultSet.getLong(Constantes.OCHO)));
    			  listaProcesoBean.setSioptvCantRegError(String.valueOf(resultSet.getLong(Constantes.NUEVE)));
    			  listaProcesoBean.setSioptvCantRegistros(String.valueOf(resultSet.getLong(Constantes.DIEZ)));
    			  
    			  lproceso.add(listaProcesoBean);
    		  }
    		  response.setPoCursor(lproceso);
    		  if(lproceso.size()>0) {
    			  if(String.valueOf(configuration.getProperty(Constantes.PPOPROCESO_LISTA2)).equals(estado)) {
        			  response.setpFinalizadoOK("1");    		
        			  response.setpFinalizadoError("0");
        			  response.setpEnEjecucion("0");
        			  response.setpPendiente("0");
        		  }
        		  else if(String.valueOf(configuration.getProperty(Constantes.PPOPROCESO_LISTA3)).equals(estado)) {
        			  response.setpFinalizadoOK("0");
        			  response.setpFinalizadoError("1");
        			  response.setpEnEjecucion("0");
        			  response.setpPendiente("0");
        		  }
        		  else if(String.valueOf(configuration.getProperty(Constantes.PPOPROCESO_LISTA1)).equals(estado)) {
        			  response.setpFinalizadoOK("0");
        			  response.setpFinalizadoError("0");
        			  response.setpEnEjecucion("1");
        			  response.setpPendiente("0");
        		  }
        		  else if(String.valueOf(configuration.getProperty(Constantes.PPOPROCESO_LISTA0)).equals(estado)) {
        			  response.setpFinalizadoOK("0");
        			  response.setpFinalizadoError("0");
        			  response.setpEnEjecucion("0");
        			  response.setpPendiente("1");
        		  }
     
    		  }
    		  

    	  }else {    		 
    		  response.setPoCursor(null);
    	  }
    	  
    	     LOG.info(mensajeTransaccion + Constantes.PARAMETROS_OUTPUT);
    	     LOG.info(mensajeTransaccion + Constantes.PO_CODERROR+ call.getObject(Constantes.OCHO));
    	     LOG.info(mensajeTransaccion + Constantes.PO_DESERROR+ call.getObject(Constantes.NUEVE));
    	     
    	     
    	     response.setPoCoderror(String.valueOf(String.valueOf(call.getObject(Constantes.OCHO))));
        	 response.setPoDeserror(String.valueOf(String.valueOf(call.getObject(Constantes.NUEVE))));

            }
            });
            
		     
	   
	     
	   
	}catch (Exception ex)
    {
	      String errorMsg = ex + "";
	      String msjError = ex.getMessage();
	      String codError = null;
	      String nombreBD = BD;
	      String nombreSP = PROCEDURE;
	      if(errorMsg.contains(Constantes.ERROR_JDBC_TIMEOUT)){
		   		 codError = String.valueOf(configuration.getProperty(Constantes.CODIGO_IDT1));
		   		 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT1)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
		        } else {
		       	 codError =String.valueOf(configuration.getProperty( Constantes.CODIGO_IDT2));
		       	 msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT2)).replace("$bd", nombreBD).replace("$sp", nombreSP)+" " +ex.getMessage();
		        }
			LOG.error( mensajeTransaccion + " ERROR: [Exception BD: "+BD+" ] - [" + ex.getMessage() + "] ",ex );
			throw new DBException(codError,nombreBD,nombreSP,msjError,ex);  
	  }finally {
			LOG.info(mensajeTransaccion + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_LISTAPROCESO);
		}	
	return response;
	
}

	@Override
	protected Predicate[] getSearchPredicates(Root<Formulario> root, Formulario example) {
		return new Predicate[0];
	}
}
