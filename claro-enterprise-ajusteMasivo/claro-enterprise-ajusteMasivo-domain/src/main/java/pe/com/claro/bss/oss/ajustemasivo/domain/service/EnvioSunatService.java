package pe.com.claro.bss.oss.ajustemasivo.domain.service;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.com.claro.bss.oss.ajustemasivo.canonical.response.DocEnviosSunatResponse;
import pe.com.claro.bss.oss.ajustemasivo.common.property.Constantes;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.DBException;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.WSException;
import pe.com.claro.bss.oss.ajustemasivo.domain.repository.EnvioSunatRepository;



@Stateless
public class EnvioSunatService {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnvioSunatService.class);

	@Context
	private Configuration configuration;
	
	@Resource
	private EJBContext context;
	
	@EJB
	private EnvioSunatRepository envioSunatRepository;
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public DocEnviosSunatResponse consultarDocSunatSIOP(String msgTransaction, String fechaIni,String fechaFin) throws WSException, DBException {
		
		LOGGER.info(msgTransaction + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_CONSULTARDOCSUNATSIOP);
		DocEnviosSunatResponse response= new DocEnviosSunatResponse();
		try {
			LOGGER.info(msgTransaction + " Parametros de entrada: [msgTransaction:" + msgTransaction + "]");
			response = envioSunatRepository.consultarDocSunatSIOP(msgTransaction, fechaIni,fechaFin);		
		}
		
		catch (DBException e) {			
			String msjError = e.getMessage(); 
	    	String codError = e.getCodigoError(); 	    	
	    	throw new DBException(codError,msjError,e);  
		}		
		catch (Exception ex) {
			String nombreWS = String.valueOf(configuration.getProperty(Constantes.WSSERVICEENPOINTADDRESS_CONSULTARDOCSUNATSIOP));
			String msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT3)).replace("$ws", nombreWS) + " " +ex.getMessage() ; 
	    	String codError = String.valueOf(configuration.getProperty(Constantes.CODIGO_IDT3));    
	    	
	    	LOGGER.error( msgTransaction + " ERROR: [Exception WS: "+nombreWS+" ] - [" + ex.getMessage() + "] ",ex );
	    	throw new WSException( codError, msjError, nombreWS, ex );
		} finally {
			LOGGER.info(msgTransaction + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_CONSULTARDOCSUNATSIOP);
		}
		return response;
	}
}
