package pe.com.claro.bss.oss.ajustemasivo.domain.service;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.com.claro.bss.oss.ajustemasivo.canonical.request.EncolarDetalleRequest;
import pe.com.claro.bss.oss.ajustemasivo.canonical.response.EncolarDetalleResponse;
import pe.com.claro.bss.oss.ajustemasivo.common.property.Constantes;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.DBException;
import pe.com.claro.bss.oss.ajustemasivo.common.resource.exception.WSException;
import pe.com.claro.bss.oss.ajustemasivo.domain.repository.EncolarDetalleBscsRepository;
import pe.com.claro.bss.oss.ajustemasivo.domain.repository.EncolarDetalleSgaRepository;

@Stateless
public class EncolarDetalleService {
	private static final Logger LOG = LoggerFactory.getLogger(EncolarDetalleService.class);

	@Context
	private Configuration configuration;
	
	@Resource
	private EJBContext context;
	
	@EJB
	private EncolarDetalleBscsRepository bscsRepository;
	
	@EJB
	private EncolarDetalleSgaRepository sgaRepository;
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public EncolarDetalleResponse encolarDetalle(String mensajeTransaccion,EncolarDetalleRequest request)throws WSException, DBException {
		EncolarDetalleResponse response=new EncolarDetalleResponse();
		LOG.info(mensajeTransaccion + Constantes.PARAMETRO_INICIO_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ENCOLARDETALLE);
		try {			
			if(request.getPiCodAjuste().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.ENCOLARDETALLE_SGA)))) {
				request.setPiCodAjuste(String.valueOf(configuration.getProperty(Constantes.ENCOLARDETALLE_SGA_V)));
				response=sgaRepository.encolarDetalle(mensajeTransaccion, request);
			}else if(request.getPiCodAjuste().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.ENCOLARDETALLE_BSCS1))) ||request.getPiCodAjuste().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.ENCOLARDETALLE_BSCS2)))|| request.getPiCodAjuste().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.ENCOLARDETALLE_BSCS3))) || request.getPiCodAjuste().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.ENCOLARDETALLE_BSCS4)))||
					request.getPiCodAjuste().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.ENCOLARDETALLE_BSCS5))) || request.getPiCodAjuste().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.ENCOLARDETALLE_BSCS6))) || request.getPiCodAjuste().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.ENCOLARDETALLE_BSCS7)))|| request.getPiCodAjuste().equalsIgnoreCase(String.valueOf(configuration.getProperty(Constantes.ENCOLARDETALLE_BSCS8)))){
				response=bscsRepository.encolarDetalle(mensajeTransaccion, request);
			}else {
				response.setPoCoderror(String.valueOf(configuration.getProperty(Constantes.CODIGO_IDT5)));
				response.setPoDeserror(String.valueOf(configuration.getProperty(Constantes.MSG_IDT5)));
			}
			
		}catch (DBException e) {			
			String msjError = e.getMessage(); 
	    	String codError = e.getCodigoError(); 	    	
	    	throw new DBException(codError,msjError,e);  
		}		
		catch (Exception ex) {
			String nombreWS = String.valueOf(configuration.getProperty(Constantes.WSSERVICEENPOINTADDRESS_DETALLE));
			String msjError = String.valueOf(configuration.getProperty(Constantes.MSG_IDT3)).replace("$ws", nombreWS) + " " +ex.getMessage() ; 
	    	String codError = String.valueOf(configuration.getProperty(Constantes.CODIGO_IDT3));    
	    	
	    	LOG.error( mensajeTransaccion + " ERROR: [Exception WS: "+nombreWS+" ] - [" + ex.getMessage() + "] ",ex );
	    	throw new WSException( codError, msjError, nombreWS, ex );
		} finally {
			LOG.info(mensajeTransaccion + Constantes.PARAMETRO_FIN_METODO,Constantes.TEXTO_VACIO,Constantes.METODO_ENCOLARDETALLE);
		}
		return response;
	}
}
