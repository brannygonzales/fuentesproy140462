package pe.com.claro.bss.oss.ajustemasivo.canonical.request;

import java.io.Serializable;

public class EncolaCabRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String piNombreArchivo;
	private String piCorreoDestino;
	private String piUsuTipificacion;
	private String piNotaTipificacion;
	private String piTipoNotificacion;
	private String piReciboRetenidos;
	private String piEnviarPostFact;
	private String piCodAjuste;
	private String piTipoAjuste;
	private String piUsuario;
	private String piCodAplicacion;
	private String piTotalReg;
	private String piTipospoolhp;
	private String piEstado;
	
	
	
	public String getPiCodAjuste() {
		return piCodAjuste;
	}
	public void setPiCodAjuste(String piCodAjuste) {
		this.piCodAjuste = piCodAjuste;
	}
	public String getPiCodAplicacion() {
		return piCodAplicacion;
	}
	public void setPiCodAplicacion(String piCodAplicacion) {
		this.piCodAplicacion = piCodAplicacion;
	}
	public String getPiTotalReg() {
		return piTotalReg;
	}
	public void setPiTotalReg(String piTotalReg) {
		this.piTotalReg = piTotalReg;
	}
	public String getPiTipoAjuste() {
		return piTipoAjuste;
	}
	public void setPiTipoAjuste(String piTipoAjuste) {
		this.piTipoAjuste = piTipoAjuste;
	}
	public String getPiNombreArchivo() {
		return piNombreArchivo;
	}
	public void setPiNombreArchivo(String piNombreArchivo) {
		this.piNombreArchivo = piNombreArchivo;
	}
	public String getPiCorreoDestino() {
		return piCorreoDestino;
	}
	public void setPiCorreoDestino(String piCorreoDestino) {
		this.piCorreoDestino = piCorreoDestino;
	}
	public String getPiUsuTipificacion() {
		return piUsuTipificacion;
	}
	public void setPiUsuTipificacion(String piUsuTipificacion) {
		this.piUsuTipificacion = piUsuTipificacion;
	}
	public String getPiNotaTipificacion() {
		return piNotaTipificacion;
	}
	public void setPiNotaTipificacion(String piNotaTipificacion) {
		this.piNotaTipificacion = piNotaTipificacion;
	}
	public String getPiTipoNotificacion() {
		return piTipoNotificacion;
	}
	public void setPiTipoNotificacion(String piTipoTipificacion) {
		this.piTipoNotificacion = piTipoTipificacion;
	}
	public String getPiEnviarPostFact() {
		return piEnviarPostFact;
	}
	public void setPiEnviarPostFact(String piEnviarPostFact) {
		this.piEnviarPostFact = piEnviarPostFact;
	}
	public String getPiReciboRetenidos() {
		return piReciboRetenidos;
	}
	public void setPiReciboRetenidos(String piReciboRetenidos) {
		this.piReciboRetenidos = piReciboRetenidos;
	}
	public String getPiEstado() {
		return piEstado;
	}
	public void setPiEstado(String piEstado) {
		this.piEstado = piEstado;
	}
	public String getPiUsuario() {
		return piUsuario;
	}
	public void setPiUsuario(String piUsuario) {
		this.piUsuario = piUsuario;
	}
	public String getPiTipospoolhp() {
		return piTipospoolhp;
	}
	public void setPiTipospoolhp(String piTipospoolhp) {
		this.piTipospoolhp = piTipospoolhp;
	}
	

}
