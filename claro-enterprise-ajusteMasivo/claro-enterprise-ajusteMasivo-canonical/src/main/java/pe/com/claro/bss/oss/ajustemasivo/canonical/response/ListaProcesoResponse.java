package pe.com.claro.bss.oss.ajustemasivo.canonical.response;

import java.io.Serializable;
import java.util.List;

import pe.com.claro.bss.oss.ajustemasivo.canonical.bean.ListaProcesoBean;

public class ListaProcesoResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<ListaProcesoBean> poCursor;
	private String poCoderror;
	private String poDeserror;
	private String pFinalizadoOK;
	private String pFinalizadoError;
	private String pEnEjecucion;
	private String pPendiente;

	public String getPoCoderror() {
		return poCoderror;
	}
	public void setPoCoderror(String poCoderror) {
		this.poCoderror = poCoderror;
	}
	public String getPoDeserror() {
		return poDeserror;
	}
	public void setPoDeserror(String poDeserror) {
		this.poDeserror = poDeserror;
	}
	public List<ListaProcesoBean> getPoCursor() {
		return poCursor;
	}
	public void setPoCursor(List<ListaProcesoBean> poCursor) {
		this.poCursor = poCursor;
	}
	public String getpFinalizadoOK() {
		return pFinalizadoOK;
	}
	public void setpFinalizadoOK(String pFinalizadoOK) {
		this.pFinalizadoOK = pFinalizadoOK;
	}
	public String getpFinalizadoError() {
		return pFinalizadoError;
	}
	public void setpFinalizadoError(String pFinalizadoError) {
		this.pFinalizadoError = pFinalizadoError;
	}
	public String getpEnEjecucion() {
		return pEnEjecucion;
	}
	public void setpEnEjecucion(String pEnEjecucion) {
		this.pEnEjecucion = pEnEjecucion;
	}
	public String getpPendiente() {
		return pPendiente;
	}
	public void setpPendiente(String pPendiente) {
		this.pPendiente = pPendiente;
	}



}
