package pe.com.claro.bss.oss.ajustemasivo.canonical.response;

import java.io.Serializable;

public class AnularDocMasivoOacResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7798588402540755584L;
	private String koCantOk;
	private String koCantErr;
	private String koTramaOk;
	private String koTramaErr;
	private String koCodigo;
	private String koMensaje;
	public String getKoCantOk() {
		return koCantOk;
	}
	public void setKoCantOk(String koCantOk) {
		this.koCantOk = koCantOk;
	}
	public String getKoCantErr() {
		return koCantErr;
	}
	public void setKoCantErr(String koCantErr) {
		this.koCantErr = koCantErr;
	}
	public String getKoTramaOk() {
		return koTramaOk;
	}
	public void setKoTramaOk(String koTramaOk) {
		this.koTramaOk = koTramaOk;
	}
	public String getKoTramaErr() {
		return koTramaErr;
	}
	public void setKoTramaErr(String koTramaErr) {
		this.koTramaErr = koTramaErr;
	}
	public String getKoCodigo() {
		return koCodigo;
	}
	public void setKoCodigo(String koCodigo) {
		this.koCodigo = koCodigo;
	}
	public String getKoMensaje() {
		return koMensaje;
	}
	public void setKoMensaje(String koMensaje) {
		this.koMensaje = koMensaje;
	}
	
}
