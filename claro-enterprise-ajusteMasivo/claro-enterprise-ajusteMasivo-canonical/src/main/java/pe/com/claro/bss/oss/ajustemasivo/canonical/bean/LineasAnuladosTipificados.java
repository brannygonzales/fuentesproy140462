package pe.com.claro.bss.oss.ajustemasivo.canonical.bean;

import java.io.Serializable;

public class LineasAnuladosTipificados implements Serializable {
/**
	 * 
	 */
	private static final long serialVersionUID = -8836092975606955571L;
	private String trama;
private String linea;
private String anulados;
private String titpificados;
public String getLinea() {
	return linea;
}
public void setLinea(String linea) {
	this.linea = linea;
}
public String getAnulados() {
	return anulados;
}
public void setAnulados(String anulados) {
	this.anulados = anulados;
}
public String getTitpificados() {
	return titpificados;
}
public void setTitpificados(String titpificados) {
	this.titpificados = titpificados;
}
public String getTrama() {
	return trama;
}
public void setTrama(String trama) {
	this.trama = trama;
}

}
