package pe.com.claro.bss.oss.ajustemasivo.canonical.request;

import java.io.Serializable;

public class SgasuValidarRegAnularSgaRequest implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6326731189195311962L;
	private String kRegistro;

	public String getkRegistro() {
		return kRegistro;
	}

	public void setkRegistro(String kRegistro) {
		this.kRegistro = kRegistro;
	}
	
}
