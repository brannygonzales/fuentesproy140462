package pe.com.claro.bss.oss.ajustemasivo.canonical.request;

import java.io.Serializable;

public class SgassListarRegAnularOacRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8192959883902787815L;
	private String kRegistro;
	public String getkRegistro() {
		return kRegistro;
	}
	public void setkRegistro(String kRegistro) {
		this.kRegistro = kRegistro;
	}
	

}
