package pe.com.claro.bss.oss.ajustemasivo.canonical.response;

import java.io.Serializable;

public class EncolaCabResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	private String poIdXLS;
	private String poCoderror;
	private String poDeserror;

	public String getPoCoderror() {
		return poCoderror;
	}
	public void setPoCoderror(String poCoderror) {
		this.poCoderror = poCoderror;
	}
	public String getPoDeserror() {
		return poDeserror;
	}
	public void setPoDeserror(String poDeserror) {
		this.poDeserror = poDeserror;
	}
	public String getPoIdXLS() {
		return poIdXLS;
	}
	public void setPoIdXLS(String poIdXLS) {
		this.poIdXLS = poIdXLS;
	}
	
}
