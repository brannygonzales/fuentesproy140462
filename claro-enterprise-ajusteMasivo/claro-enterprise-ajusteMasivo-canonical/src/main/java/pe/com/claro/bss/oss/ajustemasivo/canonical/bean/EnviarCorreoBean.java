package pe.com.claro.bss.oss.ajustemasivo.canonical.bean;

import java.io.Serializable;

public class EnviarCorreoBean implements Serializable {
	/**
		 * 
		 */
	private static final long serialVersionUID = 6688133787376104097L;
	private String asunto;
	private String remitente;
	private String destinatario;
	private String mensaje;
	private String htmlFlag;

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getRemitente() {
		return remitente;
	}

	public void setRemitente(String remitente) {
		this.remitente = remitente;
	}

	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getHtmlFlag() {
		return htmlFlag;
	}

	public void setHtmlFlag(String htmlFlag) {
		this.htmlFlag = htmlFlag;
	}

}
