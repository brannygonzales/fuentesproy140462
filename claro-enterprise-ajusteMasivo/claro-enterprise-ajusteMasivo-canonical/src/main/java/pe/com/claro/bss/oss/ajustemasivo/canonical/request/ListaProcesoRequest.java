package pe.com.claro.bss.oss.ajustemasivo.canonical.request;

import java.io.Serializable;

public class ListaProcesoRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String pTipoproceso;
	private String pTiposervicio;
	private String pArchivo;

	
	public String getpTipoproceso() {
		return pTipoproceso;
	}
	public void setpTipoproceso(String pTipoproceso) {
		this.pTipoproceso = pTipoproceso;
	}
	public String getpTiposervicio() {
		return pTiposervicio;
	}
	public void setpTiposervicio(String pTiposervicio) {
		this.pTiposervicio = pTiposervicio;
	}

	public String getpArchivo() {
		return pArchivo;
	}
	public void setpArchivo(String pArchivo) {
		this.pArchivo = pArchivo;
	}
	
}
