package pe.com.claro.bss.oss.ajustemasivo.canonical.response;

import java.io.Serializable;

public class SgasiRegistrarDocAnularResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String koCantOk;
	private String koCantErr;
	private String koRegistro;
	private String koCodigo;
	private String koMensaje;
	public String getKoCantOk() {
		return koCantOk;
	}
	public void setKoCantOk(String koCantOk) {
		this.koCantOk = koCantOk;
	}
	public String getKoCantErr() {
		return koCantErr;
	}
	public void setKoCantErr(String koCantErr) {
		this.koCantErr = koCantErr;
	}
	public String getKoRegistro() {
		return koRegistro;
	}
	public void setKoRegistro(String koRegistro) {
		this.koRegistro = koRegistro;
	}
	public String getKoCodigo() {
		return koCodigo;
	}
	public void setKoCodigo(String koCodigo) {
		this.koCodigo = koCodigo;
	}
	public String getKoMensaje() {
		return koMensaje;
	}
	public void setKoMensaje(String koMensaje) {
		this.koMensaje = koMensaje;
	}

}
