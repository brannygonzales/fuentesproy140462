package pe.com.claro.bss.oss.ajustemasivo.canonical.request;

import java.io.Serializable;

public class SgassListarRegValidarOacRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3298721400667101932L;
	private String kRegistro;
	
	public String getkRegistro() {
		return kRegistro;
	}
	public void setkRegistro(String kRegistro) {
		this.kRegistro = kRegistro;
	}
		
}
