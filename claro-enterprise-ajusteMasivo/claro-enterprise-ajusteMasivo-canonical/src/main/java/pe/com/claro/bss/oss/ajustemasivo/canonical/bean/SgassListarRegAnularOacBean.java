package pe.com.claro.bss.oss.ajustemasivo.canonical.bean;

import java.io.Serializable;

public class SgassListarRegAnularOacBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3615305290163580014L;
	private String pvTrxIdWs;
	private String pvCodAplicacion;
	private String pvUsuarioAplic;
	private String pvTipoServicio;
	private String pvCodCuenta;
	private String pvTipoOperacion;
	private String pvTipoAjuste;
	private String pvNroDocAjuste;
	private String pvDocsAjuste;
	private String pvIdReclamoOrigen;
	private String pvMonedaAjuste;
	private String pnMontoAjuste;
	private String pnSaldoAjuste;
	private String pvEstado;
	private String pvCodMotivoAjuste;
	private String pvDescripAjuste;
	private String pdFechaAjuste;
	private String pdFechaVencAjuste;
	private String pdFechaCancelacion;
	private String pvNroDocAjusteSga;
	
	public String getPvTrxIdWs() {
		return pvTrxIdWs;
	}
	public void setPvTrxIdWs(String pvTrxIdWs) {
		this.pvTrxIdWs = pvTrxIdWs;
	}
	public String getPvCodAplicacion() {
		return pvCodAplicacion;
	}
	public void setPvCodAplicacion(String pvCodAplicacion) {
		this.pvCodAplicacion = pvCodAplicacion;
	}
	public String getPvUsuarioAplic() {
		return pvUsuarioAplic;
	}
	public void setPvUsuarioAplic(String pvUsuarioAplic) {
		this.pvUsuarioAplic = pvUsuarioAplic;
	}
	public String getPvTipoServicio() {
		return pvTipoServicio;
	}
	public void setPvTipoServicio(String pvTipoServicio) {
		this.pvTipoServicio = pvTipoServicio;
	}
	public String getPvCodCuenta() {
		return pvCodCuenta;
	}
	public void setPvCodCuenta(String pvCodCuenta) {
		this.pvCodCuenta = pvCodCuenta;
	}
	public String getPvTipoOperacion() {
		return pvTipoOperacion;
	}
	public void setPvTipoOperacion(String pvTipoOperacion) {
		this.pvTipoOperacion = pvTipoOperacion;
	}
	public String getPvTipoAjuste() {
		return pvTipoAjuste;
	}
	public void setPvTipoAjuste(String pvTipoAjuste) {
		this.pvTipoAjuste = pvTipoAjuste;
	}
	public String getPvNroDocAjuste() {
		return pvNroDocAjuste;
	}
	public void setPvNroDocAjuste(String pvNroDocAjuste) {
		this.pvNroDocAjuste = pvNroDocAjuste;
	}
	public String getPvDocsAjuste() {
		return pvDocsAjuste;
	}
	public void setPvDocsAjuste(String pvDocsAjuste) {
		this.pvDocsAjuste = pvDocsAjuste;
	}
	public String getPvIdReclamoOrigen() {
		return pvIdReclamoOrigen;
	}
	public void setPvIdReclamoOrigen(String pvIdReclamoOrigen) {
		this.pvIdReclamoOrigen = pvIdReclamoOrigen;
	}
	public String getPvMonedaAjuste() {
		return pvMonedaAjuste;
	}
	public void setPvMonedaAjuste(String pvMonedaAjuste) {
		this.pvMonedaAjuste = pvMonedaAjuste;
	}
	public String getPnMontoAjuste() {
		return pnMontoAjuste;
	}
	public void setPnMontoAjuste(String pnMontoAjuste) {
		this.pnMontoAjuste = pnMontoAjuste;
	}
	public String getPnSaldoAjuste() {
		return pnSaldoAjuste;
	}
	public void setPnSaldoAjuste(String pnSaldoAjuste) {
		this.pnSaldoAjuste = pnSaldoAjuste;
	}
	public String getPvEstado() {
		return pvEstado;
	}
	public void setPvEstado(String pvEstado) {
		this.pvEstado = pvEstado;
	}
	public String getPvCodMotivoAjuste() {
		return pvCodMotivoAjuste;
	}
	public void setPvCodMotivoAjuste(String pvCodMotivoAjuste) {
		this.pvCodMotivoAjuste = pvCodMotivoAjuste;
	}
	public String getPvDescripAjuste() {
		return pvDescripAjuste;
	}
	public void setPvDescripAjuste(String pvDescripAjuste) {
		this.pvDescripAjuste = pvDescripAjuste;
	}
	public String getPdFechaAjuste() {
		return pdFechaAjuste;
	}
	public void setPdFechaAjuste(String pdFechaAjuste) {
		this.pdFechaAjuste = pdFechaAjuste;
	}
	public String getPdFechaVencAjuste() {
		return pdFechaVencAjuste;
	}
	public void setPdFechaVencAjuste(String pdFechaVencAjuste) {
		this.pdFechaVencAjuste = pdFechaVencAjuste;
	}
	public String getPdFechaCancelacion() {
		return pdFechaCancelacion;
	}
	public void setPdFechaCancelacion(String pdFechaCancelacion) {
		this.pdFechaCancelacion = pdFechaCancelacion;
	}
	public String getPvNroDocAjusteSga() {
		return pvNroDocAjusteSga;
	}
	public void setPvNroDocAjusteSga(String pvNroDocAjusteSga) {
		this.pvNroDocAjusteSga = pvNroDocAjusteSga;
	}
	

}
