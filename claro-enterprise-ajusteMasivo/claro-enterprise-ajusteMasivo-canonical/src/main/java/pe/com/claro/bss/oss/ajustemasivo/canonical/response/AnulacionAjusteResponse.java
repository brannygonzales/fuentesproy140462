package pe.com.claro.bss.oss.ajustemasivo.canonical.response;

import java.io.Serializable;
import java.util.List;

import pe.com.claro.bss.oss.ajustemasivo.canonical.bean.DetalleAnulacion;

public class AnulacionAjusteResponse implements Serializable {

	private static final long serialVersionUID = -848535489967407580L;
	
	private String poCantOk;
	private String poCantError;
	private String poAjustesOk;
	private String poAjustesError;
	private String poCoderror;
	private String poDeserror;
	private List<DetalleAnulacion> detalleAnulacion;
	
	public String getPoCantOk() {
		return poCantOk;
	}
	public void setPoCantOk(String poCantOk) {
		this.poCantOk = poCantOk;
	}
	public String getPoCantError() {
		return poCantError;
	}
	public void setPoCantError(String poCantError) {
		this.poCantError = poCantError;
	}
	public String getPoAjustesOk() {
		return poAjustesOk;
	}
	public void setPoAjustesOk(String poAjustesOk) {
		this.poAjustesOk = poAjustesOk;
	}
	public String getPoAjustesError() {
		return poAjustesError;
	}
	public void setPoAjustesError(String poAjustesError) {
		this.poAjustesError = poAjustesError;
	}
	public String getPoCoderror() {
		return poCoderror;
	}
	public void setPoCoderror(String poCoderror) {
		this.poCoderror = poCoderror;
	}
	public String getPoDeserror() {
		return poDeserror;
	}
	public void setPoDeserror(String poDeserror) {
		this.poDeserror = poDeserror;
	}

	public List<DetalleAnulacion> getDetalleAnulacion() {
		return detalleAnulacion;
	}
	public void setDetalleAnulacion(List<DetalleAnulacion> detalleAnulacion) {
		this.detalleAnulacion = detalleAnulacion;
	}

	
}
