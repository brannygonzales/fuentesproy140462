package pe.com.claro.bss.oss.ajustemasivo.canonical.bean;

import java.io.Serializable;

public class SgassListarResultadosRegErrorBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8795047608208930862L;
	private String amadvTicket;
	private String amadcCliente;
	private String amadvDocumento;
	private String amadvCiclo;
	private String amadnMontoTotal;
	private String amadvGlosa;
	private String amadnEstado;
	public String getAmadvTicket() {
		return amadvTicket;
	}
	public void setAmadvTicket(String amadvTicket) {
		this.amadvTicket = amadvTicket;
	}
	public String getAmadcCliente() {
		return amadcCliente;
	}
	public void setAmadcCliente(String amadcCliente) {
		this.amadcCliente = amadcCliente;
	}
	public String getAmadvDocumento() {
		return amadvDocumento;
	}
	public void setAmadvDocumento(String amadvDocumento) {
		this.amadvDocumento = amadvDocumento;
	}
	public String getAmadvCiclo() {
		return amadvCiclo;
	}
	public void setAmadvCiclo(String amadvCiclo) {
		this.amadvCiclo = amadvCiclo;
	}
	public String getAmadnMontoTotal() {
		return amadnMontoTotal;
	}
	public void setAmadnMontoTotal(String amadnMontoTotal) {
		this.amadnMontoTotal = amadnMontoTotal;
	}
	public String getAmadvGlosa() {
		return amadvGlosa;
	}
	public void setAmadvGlosa(String amadvGlosa) {
		this.amadvGlosa = amadvGlosa;
	}
	public String getAmadnEstado() {
		return amadnEstado;
	}
	public void setAmadnEstado(String amadnEstado) {
		this.amadnEstado = amadnEstado;
	}
	
}
