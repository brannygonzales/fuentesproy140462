package pe.com.claro.bss.oss.ajustemasivo.canonical.response;

import java.io.Serializable;

public class EncolarDetalleResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String poCantOk;
	private String poCantError;
	private String poAjustesError;
	private String poCoderror;
	private String poDeserror;
	public String getPoCantOk() {
		return poCantOk;
	}
	public void setPoCantOk(String poCantOk) {
		this.poCantOk = poCantOk;
	}
	public String getPoCantError() {
		return poCantError;
	}
	public void setPoCantError(String poCantError) {
		this.poCantError = poCantError;
	}
	public String getPoAjustesError() {
		return poAjustesError;
	}
	public void setPoAjustesError(String poAjustesError) {
		this.poAjustesError = poAjustesError;
	}
	public String getPoCoderror() {
		return poCoderror;
	}
	public void setPoCoderror(String poCoderror) {
		this.poCoderror = poCoderror;
	}
	public String getPoDeserror() {
		return poDeserror;
	}
	public void setPoDeserror(String poDeserror) {
		this.poDeserror = poDeserror;
	}
	
}
