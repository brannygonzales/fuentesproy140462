package pe.com.claro.bss.oss.ajustemasivo.canonical.request;

import java.io.Serializable;

public class ObtenerLineaRequest   implements Serializable {

	private static final long serialVersionUID = 7953646300196218206L;
	  private String piCustomerId;
	  private String piAjuste;

	  public String getPiCustomerId()
	  {
	    return this.piCustomerId;
	  }
	  public void setPiCustomerId(String piCustomerId) {
	    this.piCustomerId = piCustomerId;
	  }
	  public String getPiAjuste() {
	    return this.piAjuste;
	  }
	  public void setPiAjuste(String piAjuste) {
	    this.piAjuste = piAjuste;
	  }

}
