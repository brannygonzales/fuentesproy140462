package pe.com.claro.bss.oss.ajustemasivo.canonical.response;

import java.io.Serializable;
import java.util.List;

import pe.com.claro.bss.oss.ajustemasivo.canonical.bean.DocEnviadosSunatBean;

public class DocEnviosSunatResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private String codError;
	private String desError;
	private List<DocEnviadosSunatBean> listaEnviadosSunatBean;

	public String getCodError() {
		return codError;
	}

	public void setCodError(String codError) {
		this.codError = codError;
	}

	public String getDesError() {
		return desError;
	}

	public void setDesError(String desError) {
		this.desError = desError;
	}
	public List<DocEnviadosSunatBean> getListaEnviadosSunatType() {
		return listaEnviadosSunatBean;
	}

	public void setListaEnviadosSunatType(List<DocEnviadosSunatBean> listaFormaPagoType) {
		this.listaEnviadosSunatBean = listaFormaPagoType;
	}
}
