package pe.com.claro.bss.oss.ajustemasivo.canonical.request;

import java.io.Serializable;

public class SgasiRegistrarDocAnularRequest implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String kTipo;
	private String kTrama;
	private String kUsuario;
	public String getkTipo() {
		return kTipo;
	}
	public void setkTipo(String kTipo) {
		this.kTipo = kTipo;
	}
	public String getkTrama() {
		return kTrama;
	}
	public void setkTrama(String kTrama) {
		this.kTrama = kTrama;
	}
	public String getkUsuario() {
		return kUsuario;
	}
	public void setkUsuario(String kUsuario) {
		this.kUsuario = kUsuario;
	}
	
}
