package pe.com.claro.bss.oss.ajustemasivo.canonical.bean;

import java.io.Serializable;

public class DetalleAnulacion implements Serializable{
  private static final long serialVersionUID = -1370930437372092633L;
  private String tipo;
  private String numero;
  private String ajuste;
  private String descripcion;

  public String getTipo()
  {
    return this.tipo;
  }
  public void setTipo(String tipo) {
    this.tipo = tipo;
  }
  public String getNumero() {
    return this.numero;
  }
  public void setNumero(String numero) {
    this.numero = numero;
  }
  public String getAjuste() {
    return this.ajuste;
  }
  public void setAjuste(String ajuste) {
    this.ajuste = ajuste;
  }
  public String getDescripcion() {
    return this.descripcion;
  }
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }
}