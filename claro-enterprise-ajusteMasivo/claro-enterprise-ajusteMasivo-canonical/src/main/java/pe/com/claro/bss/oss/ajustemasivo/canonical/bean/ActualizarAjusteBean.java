package pe.com.claro.bss.oss.ajustemasivo.canonical.bean;

import java.io.Serializable;

public class ActualizarAjusteBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4556375947369928544L;

	private String piCustomerId;
	private String piAjuste;
	private String piEstado;
	private String poCoderror;
	private String poDeserror;
	public String getPoCoderror() {
		return poCoderror;
	}
	public void setPoCoderror(String poCoderror) {
		this.poCoderror = poCoderror;
	}
	public String getPoDeserror() {
		return poDeserror;
	}
	public void setPoDeserror(String poDeserror) {
		this.poDeserror = poDeserror;
	}
	public String getPiCustomerId() {
		return piCustomerId;
	}
	public void setPiCustomerId(String piCustomerId) {
		this.piCustomerId = piCustomerId;
	}
	public String getPiAjuste() {
		return piAjuste;
	}
	public void setPiAjuste(String piAjuste) {
		this.piAjuste = piAjuste;
	}
	public String getPiEstado() {
		return piEstado;
	}
	public void setPiEstado(String piEstado) {
		this.piEstado = piEstado;
	}
	
}
