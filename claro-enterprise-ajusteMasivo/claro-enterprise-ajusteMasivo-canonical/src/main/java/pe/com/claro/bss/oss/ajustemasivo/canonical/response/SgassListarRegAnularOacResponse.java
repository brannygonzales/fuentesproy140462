package pe.com.claro.bss.oss.ajustemasivo.canonical.response;

import java.io.Serializable;
import java.util.List;

import pe.com.claro.bss.oss.ajustemasivo.canonical.bean.SgassListarRegAnularOacBean;

public class SgassListarRegAnularOacResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1117556546312289967L;
	
	private List<SgassListarRegAnularOacBean> koCursor;
	private String koCodigo;
	private String koMensaje;
	public List<SgassListarRegAnularOacBean> getKoCursor() {
		return koCursor;
	}
	public void setKoCursor(List<SgassListarRegAnularOacBean> koCursor) {
		this.koCursor = koCursor;
	}
	public String getKoCodigo() {
		return koCodigo;
	}
	public void setKoCodigo(String koCodigo) {
		this.koCodigo = koCodigo;
	}
	public String getKoMensaje() {
		return koMensaje;
	}
	public void setKoMensaje(String koMensaje) {
		this.koMensaje = koMensaje;
	}
	
}
