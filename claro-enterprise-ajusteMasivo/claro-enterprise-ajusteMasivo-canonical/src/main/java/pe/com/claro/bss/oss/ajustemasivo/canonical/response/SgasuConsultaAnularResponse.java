package pe.com.claro.bss.oss.ajustemasivo.canonical.response;

import java.io.Serializable;

public class SgasuConsultaAnularResponse  implements Serializable {

	private static final long serialVersionUID = 3477239679165897642L;
	private String koRegistro;
	private String koCodigo;
	private String koMensaje;
	
	public String getKoRegistro() {
		return koRegistro;
	}
	public void setKoRegistro(String koRegistro) {
		this.koRegistro = koRegistro;
	}
	public String getKoCodigo() {
		return koCodigo;
	}
	public void setKoCodigo(String koCodigo) {
		this.koCodigo = koCodigo;
	}
	public String getKoMensaje() {
		return koMensaje;
	}
	public void setKoMensaje(String koMensaje) {
		this.koMensaje = koMensaje;
	}
	

}
