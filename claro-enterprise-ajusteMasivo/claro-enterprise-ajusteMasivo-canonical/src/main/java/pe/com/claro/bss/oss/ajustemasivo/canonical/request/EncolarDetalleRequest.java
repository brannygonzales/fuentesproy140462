package pe.com.claro.bss.oss.ajustemasivo.canonical.request;

import java.io.Serializable;

public class EncolarDetalleRequest implements Serializable{

	private static final long serialVersionUID = 1L;
	private String piIdXLS;
	private String piCodAjuste;
	private String piTrama;


	public String getPiIdXLS() {
		return piIdXLS;
	}
	public void setPiIdXLS(String poIdXLS) {
		this.piIdXLS = poIdXLS;
	}
	public String getPiCodAjuste() {
		return piCodAjuste;
	}
	public void setPiCodAjuste(String piCodAjuste) {
		this.piCodAjuste = piCodAjuste;
	}
	public String getPiTrama() {
		return piTrama;
	}
	public void setPiTrama(String piTrama) {
		this.piTrama = piTrama;
	}
	
}
