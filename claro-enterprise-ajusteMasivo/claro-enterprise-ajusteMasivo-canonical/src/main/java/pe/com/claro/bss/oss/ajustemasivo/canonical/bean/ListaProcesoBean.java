package pe.com.claro.bss.oss.ajustemasivo.canonical.bean;

import java.io.Serializable;

public class ListaProcesoBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String sioptnCodproceso;
	private String tipoProceso;
	private String tipoServicio;
	private String archivo;
	private String estado;
	private String sioptdFecharegistro;
	private String sioptvUsuario;
	private String sioptvCantRegOk;
	private String sioptvCantRegError;
	private String sioptvCantRegistros;
	public String getSioptnCodproceso() {
		return sioptnCodproceso;
	}
	public void setSioptnCodproceso(String sioptnCodproceso) {
		this.sioptnCodproceso = sioptnCodproceso;
	}
	public String getTipoProceso() {
		return tipoProceso;
	}
	public void setTipoProceso(String tipoProceso) {
		this.tipoProceso = tipoProceso;
	}
	public String getTipoServicio() {
		return tipoServicio;
	}
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	public String getArchivo() {
		return archivo;
	}
	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getSioptdFecharegistro() {
		return sioptdFecharegistro;
	}
	public void setSioptdFecharegistro(String sioptdFecharegistro) {
		this.sioptdFecharegistro = sioptdFecharegistro;
	}
	public String getSioptvUsuario() {
		return sioptvUsuario;
	}
	public void setSioptvUsuario(String sioptvUsuario) {
		this.sioptvUsuario = sioptvUsuario;
	}
	
	public String getSioptvCantRegOk() {
		return sioptvCantRegOk;
	}
	public void setSioptvCantRegOk(String sioptvCantRegOk) {
		this.sioptvCantRegOk = sioptvCantRegOk;
	}
	public String getSioptvCantRegError() {
		return sioptvCantRegError;
	}
	public void setSioptvCantRegError(String sioptvCantRegError) {
		this.sioptvCantRegError = sioptvCantRegError;
	}
	public String getSioptvCantRegistros() {
		return sioptvCantRegistros;
	}
	public void setSioptvCantRegistros(String sioptvCantRegistros) {
		this.sioptvCantRegistros = sioptvCantRegistros;
	}
	
}
