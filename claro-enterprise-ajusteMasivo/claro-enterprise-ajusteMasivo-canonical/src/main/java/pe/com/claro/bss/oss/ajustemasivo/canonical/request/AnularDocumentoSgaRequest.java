package pe.com.claro.bss.oss.ajustemasivo.canonical.request;

import java.io.Serializable;

public class AnularDocumentoSgaRequest implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3501081295024984837L;
	private String kTipo;
	private String kTrama;
	private String kNumreg;
	private String kUsuario;
	private String kRegistro;
	public String getkTipo() {
		return kTipo;
	}
	public void setkTipo(String kTipo) {
		this.kTipo = kTipo;
	}
	public String getkTrama() {
		return kTrama;
	}
	public void setkTrama(String kTrama) {
		this.kTrama = kTrama;
	}
	public String getkNumreg() {
		return kNumreg;
	}
	public void setkNumreg(String kNumreg) {
		this.kNumreg = kNumreg;
	}
	public String getkUsuario() {
		return kUsuario;
	}
	public void setkUsuario(String kUsuario) {
		this.kUsuario = kUsuario;
	}
	public String getkRegistro() {
		return kRegistro;
	}
	public void setkRegistro(String kRegistro) {
		this.kRegistro = kRegistro;
	}
	
}
