package pe.com.claro.bss.oss.ajustemasivo.canonical.request;

import java.io.Serializable;

public class DocEnviosSunatRequest implements Serializable {


	private static final long serialVersionUID = 1L;
	private String piFechaIni;
	private String piFechaFin;

	public String getPiFechaIni() {
		return piFechaIni;
	}

	public void setPiFechaIni(String piFechaIni) {
		this.piFechaIni = piFechaIni;
	}

	public String getPiFechaFin() {
		return piFechaFin;
	}

	public void setPiFechaFin(String piFechaFin) {
		this.piFechaFin = piFechaFin;
	}


}
