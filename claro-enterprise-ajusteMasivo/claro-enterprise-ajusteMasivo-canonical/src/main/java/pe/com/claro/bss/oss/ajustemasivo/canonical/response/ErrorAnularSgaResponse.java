package pe.com.claro.bss.oss.ajustemasivo.canonical.response;

import java.io.Serializable;

public class ErrorAnularSgaResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3153646211066471619L;
	private String koCodigo;
	private String koMensaje;
	public String getKoCodigo() {
		return koCodigo;
	}
	public void setKoCodigo(String koCodigo) {
		this.koCodigo = koCodigo;
	}
	public String getKoMensaje() {
		return koMensaje;
	}
	public void setKoMensaje(String koMensaje) {
		this.koMensaje = koMensaje;
	}
	
}
