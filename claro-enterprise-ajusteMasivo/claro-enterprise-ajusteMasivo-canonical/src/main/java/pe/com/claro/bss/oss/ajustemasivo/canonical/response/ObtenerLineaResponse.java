package pe.com.claro.bss.oss.ajustemasivo.canonical.response;

import java.io.Serializable;

public class ObtenerLineaResponse  implements Serializable {
  private static final long serialVersionUID = -6624069647731232546L;
  private String poLinea;
  private String poCoderror;
  private String poDeserror;

  public String getPoLinea()
  {
    return this.poLinea;
  }
  public void setPoLinea(String poLinea) {
    this.poLinea = poLinea;
  }
  public String getPoCoderror() {
    return this.poCoderror;
  }
  public void setPoCoderror(String poCoderror) {
    this.poCoderror = poCoderror;
  }
  public String getPoDeserror() {
    return this.poDeserror;
  }
  public void setPoDeserror(String poDeserror) {
    this.poDeserror = poDeserror;
  }
}