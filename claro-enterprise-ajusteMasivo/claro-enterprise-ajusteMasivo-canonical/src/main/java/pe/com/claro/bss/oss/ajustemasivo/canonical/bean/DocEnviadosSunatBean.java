package pe.com.claro.bss.oss.ajustemasivo.canonical.bean;

import java.io.Serializable;

public class DocEnviadosSunatBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private String id;
	private String lote;
	private String tipo;
	private String fchEmision;
	private String fchEjecucion;
	private String legado;
	private String regLegado;
	private String regEntregados;
	private String montoAnulado;
	private String montoEntregado;
	private String logError;
	private String estado;
	private String fechaRegistro;
	private String usuarioRegistro;
	private String fechaActualiza;
	private String usuarioActualiza;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLote() {
		return lote;
	}
	public void setLote(String lote) {
		this.lote = lote;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getFchEmision() {
		return fchEmision;
	}
	public void setFchEmision(String fchEmision) {
		this.fchEmision = fchEmision;
	}
	public String getFchEjecucion() {
		return fchEjecucion;
	}
	public void setFchEjecucion(String fchEjecucion) {
		this.fchEjecucion = fchEjecucion;
	}
	public String getLegado() {
		return legado;
	}
	public void setLegado(String legado) {
		this.legado = legado;
	}
	public String getRegLegado() {
		return regLegado;
	}
	public void setRegLegado(String regLegado) {
		this.regLegado = regLegado;
	}
	public String getRegEntregados() {
		return regEntregados;
	}
	public void setRegEntregados(String regEntregados) {
		this.regEntregados = regEntregados;
	}
	public String getMontoAnulado() {
		return montoAnulado;
	}
	public void setMontoAnulado(String montoAnulado) {
		this.montoAnulado = montoAnulado;
	}
	public String getMontoEntregado() {
		return montoEntregado;
	}
	public void setMontoEntregado(String montoEntregado) {
		this.montoEntregado = montoEntregado;
	}
	public String getLogError() {
		return logError;
	}
	public void setLogError(String logError) {
		this.logError = logError;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public String getUsuarioRegistro() {
		return usuarioRegistro;
	}
	public void setUsuarioRegistro(String usuarioRegistro) {
		this.usuarioRegistro = usuarioRegistro;
	}
	public String getFechaActualiza() {
		return fechaActualiza;
	}
	public void setFechaActualiza(String fechaActualiza) {
		this.fechaActualiza = fechaActualiza;
	}
	public String getUsuarioActualiza() {
		return usuarioActualiza;
	}
	public void setUsuarioActualiza(String usuarioActualiza) {
		this.usuarioActualiza = usuarioActualiza;
	}
	
}
