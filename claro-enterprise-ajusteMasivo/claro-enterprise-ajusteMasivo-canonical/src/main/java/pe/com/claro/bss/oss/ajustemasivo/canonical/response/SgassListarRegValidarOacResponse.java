package pe.com.claro.bss.oss.ajustemasivo.canonical.response;

import java.io.Serializable;
import java.util.List;

import pe.com.claro.bss.oss.ajustemasivo.canonical.bean.SgassListarRegValidarOacBean;

public class SgassListarRegValidarOacResponse implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1512723415409944825L;
	private List<SgassListarRegValidarOacBean> koCursor;
	private String koCodigo;
	private String koMensaje;

	public String getKoCodigo() {
		return koCodigo;
	}
	public void setKoCodigo(String koCodigo) {
		this.koCodigo = koCodigo;
	}
	public String getKoMensaje() {
		return koMensaje;
	}
	public void setKoMensaje(String koMensaje) {
		this.koMensaje = koMensaje;
	}
	public List<SgassListarRegValidarOacBean> getKoCursor() {
		return koCursor;
	}
	public void setKoCursor(List<SgassListarRegValidarOacBean> koCursor) {
		this.koCursor = koCursor;
	}
	
}
