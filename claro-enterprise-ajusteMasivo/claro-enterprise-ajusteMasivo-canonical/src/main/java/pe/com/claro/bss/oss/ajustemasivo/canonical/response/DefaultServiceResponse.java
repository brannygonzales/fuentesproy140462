package pe.com.claro.bss.oss.ajustemasivo.canonical.response;

import java.io.Serializable;

public class DefaultServiceResponse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4091680906737106555L;
	private Integer categoria;
	private String idRespuesta;
	private String mensaje;
	private String excepcion;
	private String idTransaccional;
	
	public Integer getCategoria() {
		return categoria;
	}
	public void setCategoria(Integer categoria) {
		this.categoria = categoria;
	}
	public String getIdRespuesta() {
		return idRespuesta;
	}
	public void setIdRespuesta(String idRespuesta) {
		this.idRespuesta = idRespuesta;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getExcepcion() {
		return excepcion;
	}
	public void setExcepcion(String excepcion) {
		this.excepcion = excepcion;
	}
	public String getIdTransaccional() {
		return idTransaccional;
	}
	public void setIdTransaccional(String idTransaccional) {
		this.idTransaccional = idTransaccional;
	}
	
}
