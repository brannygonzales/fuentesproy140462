package pe.com.claro.bss.oss.ajustemasivo.canonical.response;

import java.io.Serializable;

public class SgasuValidaOacResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8577644911166978085L;

	private String koCodigo;
	private String koMensaje;
	public String getKoCodigo() {
		return koCodigo;
	}
	public void setKoCodigo(String koCodigo) {
		this.koCodigo = koCodigo;
	}
	public String getKoMensaje() {
		return koMensaje;
	}
	public void setKoMensaje(String koMensaje) {
		this.koMensaje = koMensaje;
	}
	
}
