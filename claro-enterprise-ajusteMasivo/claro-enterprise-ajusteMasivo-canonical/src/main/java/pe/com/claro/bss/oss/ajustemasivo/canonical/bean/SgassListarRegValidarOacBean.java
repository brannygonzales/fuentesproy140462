package pe.com.claro.bss.oss.ajustemasivo.canonical.bean;

import java.io.Serializable;

public class SgassListarRegValidarOacBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1365987495593864403L;
	private String pvCodAplicacion;
	private String pvTipoServicio;
	private String pvCodCuenta;
	private String pvTipoAjuste;
	private String pvNroDocAjuste;
	private String pvDocsAjuste;
	private String pvMonedaAjuste;
	private String pnMontoAjuste;
	private String pnSaldoAjuste;
	private String pvEstado;
	private String pdFechaAjuste;
	private String pvDocsAjusteSga;
	
	public String getPvCodAplicacion() {
		return pvCodAplicacion;
	}
	public void setPvCodAplicacion(String pvCodAplicacion) {
		this.pvCodAplicacion = pvCodAplicacion;
	}
	public String getPvTipoServicio() {
		return pvTipoServicio;
	}
	public void setPvTipoServicio(String pvTipoServicio) {
		this.pvTipoServicio = pvTipoServicio;
	}
	public String getPvCodCuenta() {
		return pvCodCuenta;
	}
	public void setPvCodCuenta(String pvCodCuenta) {
		this.pvCodCuenta = pvCodCuenta;
	}
	public String getPvTipoAjuste() {
		return pvTipoAjuste;
	}
	public void setPvTipoAjuste(String pvTipoAjuste) {
		this.pvTipoAjuste = pvTipoAjuste;
	}
	public String getPvNroDocAjuste() {
		return pvNroDocAjuste;
	}
	public void setPvNroDocAjuste(String pvNroDocAjuste) {
		this.pvNroDocAjuste = pvNroDocAjuste;
	}
	public String getPvDocsAjuste() {
		return pvDocsAjuste;
	}
	public void setPvDocsAjuste(String pvDocsAjuste) {
		this.pvDocsAjuste = pvDocsAjuste;
	}
	public String getPvMonedaAjuste() {
		return pvMonedaAjuste;
	}
	public void setPvMonedaAjuste(String pvMonedaAjuste) {
		this.pvMonedaAjuste = pvMonedaAjuste;
	}
	public String getPnMontoAjuste() {
		return pnMontoAjuste;
	}
	public void setPnMontoAjuste(String pnMontoAjuste) {
		this.pnMontoAjuste = pnMontoAjuste;
	}
	public String getPnSaldoAjuste() {
		return pnSaldoAjuste;
	}
	public void setPnSaldoAjuste(String pnSaldoAjuste) {
		this.pnSaldoAjuste = pnSaldoAjuste;
	}
	public String getPvEstado() {
		return pvEstado;
	}
	public void setPvEstado(String pvEstado) {
		this.pvEstado = pvEstado;
	}
	public String getPdFechaAjuste() {
		return pdFechaAjuste;
	}
	public void setPdFechaAjuste(String pdFechaAjuste) {
		this.pdFechaAjuste = pdFechaAjuste;
	}
	public String getPvDocsAjusteSga() {
		return pvDocsAjusteSga;
	}
	public void setPvDocsAjusteSga(String pvDocsAjusteSga) {
		this.pvDocsAjusteSga = pvDocsAjusteSga;
	}
	
	
	
}
