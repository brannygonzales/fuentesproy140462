package pe.com.claro.bss.oss.ajustemasivo.canonical.response;

import java.io.Serializable;

public class PrValRegistroResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1441853839251282153L;
	private String xvStatus;
	private String xvMessage;
	public String getXvStatus() {
		return xvStatus;
	}
	public void setXvStatus(String xvStatus) {
		this.xvStatus = xvStatus;
	}
	public String getXvMessage() {
		return xvMessage;
	}
	public void setXvMessage(String xvMessage) {
		this.xvMessage = xvMessage;
	}
		
}
