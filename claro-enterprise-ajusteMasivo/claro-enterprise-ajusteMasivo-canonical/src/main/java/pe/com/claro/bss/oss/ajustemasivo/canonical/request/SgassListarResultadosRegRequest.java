package pe.com.claro.bss.oss.ajustemasivo.canonical.request;

import java.io.Serializable;

public class SgassListarResultadosRegRequest implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4932956075553815626L;
	private String kRegistro;
	
	public String getkRegistro() {
		return kRegistro;
	}
	public void setkRegistro(String kRegistro) {
		this.kRegistro = kRegistro;
	}
	
}
