package pe.com.claro.bss.oss.ajustemasivo.canonical.request;

import java.io.Serializable;


public class AnulacionAjusteRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6586959681988846071L;

	private String piTrama;
	private String piUsuario;
	private String piUsuTipificacion;
	private String piTipoAjuste;
	private String piCorreoDestino;
	private String piNotaTipificacion;
	private String piCliente;
	private String piArchivo;
	
	
	public String getPiUsuTipificacion() {
		return piUsuTipificacion;
	}
	public void setPiUsuTipificacion(String piUsuTipificacion) {
		this.piUsuTipificacion = piUsuTipificacion;
	}
	public String getPiCorreoDestino() {
		return piCorreoDestino;
	}
	public void setPiCorreoDestino(String piCorreoDestino) {
		this.piCorreoDestino = piCorreoDestino;
	}
	public String getPiTrama() {
		return piTrama;
	}
	public void setPiTrama(String piTrama) {
		this.piTrama = piTrama;
	}
	public String getPiUsuario() {
		return piUsuario;
	}
	public void setPiUsuario(String piUsuario) {
		this.piUsuario = piUsuario;
	}
	public String getPiTipoAjuste() {
		return piTipoAjuste;
	}
	public void setPiTipoAjuste(String piTipoAjuste) {
		this.piTipoAjuste = piTipoAjuste;
	}
	public String getPiNotaTipificacion() {
		return piNotaTipificacion;
	}
	public void setPiNotaTipificacion(String piNotaTipificacion) {
		this.piNotaTipificacion = piNotaTipificacion;
	}

	public String getPiCliente() {
		return piCliente;
	}
	public void setPiCliente(String piCliente) {
		this.piCliente = piCliente;
	}
	public String getPiArchivo() {
		return piArchivo;
	}
	public void setPiArchivo(String piArchivo) {
		this.piArchivo = piArchivo;
	}
}
