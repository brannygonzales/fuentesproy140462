package pe.com.claro.controlesajustemasivo.exception;

public class ProcesoException extends BaseException {

	private static final long serialVersionUID = -6991986069553461997L;

	public ProcesoException(String mensajeError) {
		super(mensajeError);
	}

	public ProcesoException(String mensajeError, Throwable e) {
		super(mensajeError, e);
	}

}
