package pe.com.claro.controlesajustemasivo.bean;

import java.util.List;

public class AjustesIpcResponseBean extends ResponseBean {

	private String customerId;
	private String tipoMoneda;
	private double monto;
	private List<AjusteIPCBean> listaAjustes;
	
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getTipoMoneda() {
		return tipoMoneda;
	}
	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	public List<AjusteIPCBean> getListaAjustes() {
		return listaAjustes;
	}
	public void setListaAjustes(List<AjusteIPCBean> listaAjustes) {
		this.listaAjustes = listaAjustes;
	}
	
}
