package pe.com.claro.controlesajustemasivo.bean;

public class AjusteIPCBean {

	private String tipoAjuste;
	private String numeroAjuste;
	private String customerId;
	private String tipoMoneda;
	private double monto;
	
	public String getTipoAjuste() {
		return tipoAjuste;
	}
	public void setTipoAjuste(String tipoAjuste) {
		this.tipoAjuste = tipoAjuste;
	}
	public String getNumeroAjuste() {
		return numeroAjuste;
	}
	public void setNumeroAjuste(String numeroAjuste) {
		this.numeroAjuste = numeroAjuste;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getTipoMoneda() {
		return tipoMoneda;
	}
	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	
}
