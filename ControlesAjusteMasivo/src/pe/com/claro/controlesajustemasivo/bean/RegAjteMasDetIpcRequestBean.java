package pe.com.claro.controlesajustemasivo.bean;

public class RegAjteMasDetIpcRequestBean {

	private Integer idCab;
	private String tipoAjuste;
	private String customerId;
	private String numeroAjuste;
	private String tipoMoneda;
	private Double monto;
	private String tipoAjusteOAC;
	private String customerIdOAC;
	private String numeroAjusteOAC;
	private String tipoMonedaOAC;
	private Double montoOAC;
	private String usuario;
	
	public RegAjteMasDetIpcRequestBean() {
	}
	
	public RegAjteMasDetIpcRequestBean(String tipoAjuste, String customerId, String numeroAjuste, String tipoMoneda,
			Double monto, String usuario) {
		this.tipoAjuste = tipoAjuste;
		this.customerId = customerId;
		this.numeroAjuste = numeroAjuste;
		this.tipoMoneda = tipoMoneda;
		this.monto = monto;
		this.usuario = usuario;
	}
	
	public RegAjteMasDetIpcRequestBean(String customerIdOAC, String numeroAjusteOAC, String tipoMonedaOAC,
			Double montoOAC, String usuario) {
		this.customerIdOAC = customerIdOAC;
		this.numeroAjusteOAC = numeroAjusteOAC;
		this.tipoMonedaOAC = tipoMonedaOAC;
		this.montoOAC = montoOAC;
		this.usuario = usuario;
	}

	public Integer getIdCab() {
		return idCab;
	}
	public void setIdCab(Integer idCab) {
		this.idCab = idCab;
	}
	public String getTipoAjuste() {
		return tipoAjuste;
	}
	public void setTipoAjuste(String tipoAjuste) {
		this.tipoAjuste = tipoAjuste;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getNumeroAjuste() {
		return numeroAjuste;
	}
	public void setNumeroAjuste(String numeroAjuste) {
		this.numeroAjuste = numeroAjuste;
	}
	public String getTipoMoneda() {
		return tipoMoneda;
	}
	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}
	public Double getMonto() {
		return monto;
	}
	public void setMonto(Double monto) {
		this.monto = monto;
	}
	public String getTipoAjusteOAC() {
		return tipoAjusteOAC;
	}
	public void setTipoAjusteOAC(String tipoAjusteOAC) {
		this.tipoAjusteOAC = tipoAjusteOAC;
	}
	public String getCustomerIdOAC() {
		return customerIdOAC;
	}
	public void setCustomerIdOAC(String customerIdOAC) {
		this.customerIdOAC = customerIdOAC;
	}
	public String getNumeroAjusteOAC() {
		return numeroAjusteOAC;
	}
	public void setNumeroAjusteOAC(String numeroAjusteOAC) {
		this.numeroAjusteOAC = numeroAjusteOAC;
	}
	public String getTipoMonedaOAC() {
		return tipoMonedaOAC;
	}
	public void setTipoMonedaOAC(String tipoMonedaOAC) {
		this.tipoMonedaOAC = tipoMonedaOAC;
	}
	public Double getMontoOAC() {
		return montoOAC;
	}
	public void setMontoOAC(Double montoOAC) {
		this.montoOAC = montoOAC;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
}
