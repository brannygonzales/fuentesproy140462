package pe.com.claro.controlesajustemasivo.bean;

public class RegAjteMasIpcRequestBean {

	private Integer cantidadOrigenPos;
	private Integer cantidadOacPos;
	private Double montoOrigenPos;
	private Double montoOacPos;
	private String aplicacion;
	private String usuario;
	private Integer cantidadOrigenNeg;
	private Integer cantidadOacNeg;
	private Double montoOrigenNeg;
	private Double montoOacNeg;
	
	public Integer getCantidadOrigenPos() {
		return cantidadOrigenPos;
	}

	public void setCantidadOrigenPos(Integer cantidadOrigenPos) {
		this.cantidadOrigenPos = cantidadOrigenPos;
	}

	public Integer getCantidadOacPos() {
		return cantidadOacPos;
	}

	public void setCantidadOacPos(Integer cantidadOacPos) {
		this.cantidadOacPos = cantidadOacPos;
	}

	public Double getMontoOrigenPos() {
		return montoOrigenPos;
	}

	public void setMontoOrigenPos(Double montoOrigenPos) {
		this.montoOrigenPos = montoOrigenPos;
	}

	public Double getMontoOacPos() {
		return montoOacPos;
	}

	public void setMontoOacPos(Double montoOacPos) {
		this.montoOacPos = montoOacPos;
	}

	public Integer getCantidadOrigenNeg() {
		return cantidadOrigenNeg;
	}

	public void setCantidadOrigenNeg(Integer cantidadOrigenNeg) {
		this.cantidadOrigenNeg = cantidadOrigenNeg;
	}

	public Integer getCantidadOacNeg() {
		return cantidadOacNeg;
	}

	public void setCantidadOacNeg(Integer cantidadOacNeg) {
		this.cantidadOacNeg = cantidadOacNeg;
	}

	public Double getMontoOrigenNeg() {
		return montoOrigenNeg;
	}

	public void setMontoOrigenNeg(Double montoOrigenNeg) {
		this.montoOrigenNeg = montoOrigenNeg;
	}

	public Double getMontoOacNeg() {
		return montoOacNeg;
	}

	public void setMontoOacNeg(Double montoOacNeg) {
		this.montoOacNeg = montoOacNeg;
	}

	public String getAplicacion() {
		return aplicacion;
	}
	public void setAplicacion(String aplicacion) {
		this.aplicacion = aplicacion;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
}
