package pe.com.claro.controlesajustemasivo.bean;

import java.util.List;

public class AjustesIpcSgaResponseBean extends ResponseBean {

	private List<AjusteIPCBean> listaAjustes;

	public List<AjusteIPCBean> getListaAjustes() {
		return listaAjustes;
	}

	public void setListaAjustes(List<AjusteIPCBean> listaAjustes) {
		this.listaAjustes = listaAjustes;
	}
	
}
