package pe.com.claro.controlesajustemasivo.bean;

public class AjustesIpcRequestBean {

	private String tipoAjuste;
	private String numeroAjuste;
	private String customerId;
	
	public String getTipoAjuste() {
		return tipoAjuste;
	}
	public void setTipoAjuste(String tipoAjuste) {
		this.tipoAjuste = tipoAjuste;
	}
	public String getNumeroAjuste() {
		return numeroAjuste;
	}
	public void setNumeroAjuste(String numeroAjuste) {
		this.numeroAjuste = numeroAjuste;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
}
