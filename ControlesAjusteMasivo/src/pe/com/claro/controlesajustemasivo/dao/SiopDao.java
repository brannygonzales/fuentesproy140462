package pe.com.claro.controlesajustemasivo.dao;

import pe.com.claro.controlesajustemasivo.bean.AjustesIpcRequestBean;
import pe.com.claro.controlesajustemasivo.bean.AjustesIpcResponseBean;
import pe.com.claro.controlesajustemasivo.exception.DBException;

public interface SiopDao {

	AjustesIpcResponseBean consultarAjustesIpc(String mensajeLog, AjustesIpcRequestBean requestBean) throws DBException;
	
}
