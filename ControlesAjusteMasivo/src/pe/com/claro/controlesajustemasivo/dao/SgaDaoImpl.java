package pe.com.claro.controlesajustemasivo.dao;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.com.claro.controlesajustemasivo.bean.AjusteIPCBean;
import pe.com.claro.controlesajustemasivo.bean.AjustesIpcSgaResponseBean;
import pe.com.claro.controlesajustemasivo.dao.mapper.AjusteIpcSgaMapper;
import pe.com.claro.controlesajustemasivo.exception.DBException;
import pe.com.claro.controlesajustemasivo.util.Constantes;
import pe.com.claro.controlesajustemasivo.util.PropertiesExterno;

@Repository
public class SgaDaoImpl implements SgaDao {

	private static final Logger logger = Logger.getLogger(SgaDaoImpl.class);
	
	@Autowired
	@Qualifier("sgaDS")
	private DataSource sgaDS;
	
	@Autowired
	private PropertiesExterno propertiesExterno;
	
	@Override
	public AjustesIpcSgaResponseBean consultarAjustesIPC(String mensajeLog, Date fecha) throws DBException {
		long tiempoInicio = System.currentTimeMillis();
		AjustesIpcSgaResponseBean responseBean = new AjustesIpcSgaResponseBean();
		String nombreMetodo = "consultarAjustesIPC";
		StringBuffer storedProcedure = new StringBuffer();
		logger.info(mensajeLog + "[INICIO] - METODO: [" + nombreMetodo + " - DAO]");
		
		String nombreBD = propertiesExterno.DB_SGA_NOMBRE;
		String owner = propertiesExterno.DB_SGA_OWNER;
		String paquete = propertiesExterno.DB_SGA_PKG_AJUSTEMASIVOS;
		String procedure = propertiesExterno.DB_SGA_SP_AJUSTESIPC;
		Integer timeoutEjecucion = propertiesExterno.DB_SGA_TIMEOUT_EJECUCION_SEGUNDOS;
		
		storedProcedure.append(owner);
		storedProcedure.append(Constantes.PUNTO);
		storedProcedure.append(paquete);
		storedProcedure.append(Constantes.PUNTO);
		storedProcedure.append(procedure);
		
		try {
			logger.info(mensajeLog + " - PROCEDURE: [" + storedProcedure.toString() + "]");
			logger.info(mensajeLog + " - TIMEOUT EJECUCION: [" + timeoutEjecucion + "]");
			
			SqlParameterSource inputProcedure = new MapSqlParameterSource()
					.addValue("K_FECHA", fecha, OracleTypes.DATE);
			
			logger.info(mensajeLog + " - PARAMETROS [INPUT] ");
			logger.info(mensajeLog + " - [INPUT][K_FECHA]: " + fecha);
			
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(sgaDS);
			JdbcTemplate jdbcTemplate = simpleJdbcCall.getJdbcTemplate();
			jdbcTemplate.setQueryTimeout(timeoutEjecucion);
			
			simpleJdbcCall.withoutProcedureColumnMetaDataAccess();
			simpleJdbcCall.withSchemaName(owner);
			simpleJdbcCall.withCatalogName(paquete);
			simpleJdbcCall.withProcedureName(procedure);
			
			simpleJdbcCall.declareParameters(
					new SqlParameter("K_FECHA", OracleTypes.DATE),
					new SqlOutParameter("KO_CURSOR", OracleTypes.CURSOR, new AjusteIpcSgaMapper()),
					new SqlOutParameter("KO_CODIGO", OracleTypes.INTEGER),
					new SqlOutParameter("KO_MENSAJE", OracleTypes.VARCHAR));
			Map<String, Object> outputProcedure = simpleJdbcCall.execute(inputProcedure);
			logger.info(mensajeLog + "SP ejecutado");
			
			List<AjusteIPCBean> listaAjustes = outputProcedure.get("KO_CURSOR") == null ? 
					new ArrayList<AjusteIPCBean>() : (List<AjusteIPCBean>) outputProcedure.get("KO_CURSOR");
			Integer codigo = (Integer) outputProcedure.get("KO_CODIGO");
			String mensaje = (String) outputProcedure.get("KO_MENSAJE");
			
			logger.info(mensajeLog + " - PARAMETROS [OUTPUT]");
			logger.info(mensajeLog + " - [OUTPUT][KO_CURSOR][Cantidad de registros]: [" + listaAjustes.size() + "]");
			logger.info(mensajeLog + " - [OUTPUT][KO_CODIGO]: [" + codigo + "]");
			logger.info(mensajeLog + " - [OUTPUT][KO_MENSAJE]: [" + mensaje + "]");
			
			responseBean.setCodigo(codigo);
			responseBean.setMensaje(mensaje);
			responseBean.setListaAjustes(listaAjustes);
		} catch (Exception e) {
			logger.error(mensajeLog + "ERROR: [Exception] - [" + e.getMessage() + "] ", e);
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			String descripcionError = String.valueOf(errors.toString());
			
			if (descripcionError.toUpperCase(Locale.getDefault()).contains(Constantes.SQLTIMEOUTEXCEPTION)) {
				throw new DBException("Error de Timeout hacia BD", e, nombreBD, storedProcedure.toString());
			} else {
				throw new DBException("Error de disponibilidad hacia BD", e, nombreBD, storedProcedure.toString());
			}
		} finally {
			logger.info(mensajeLog + " - Tiempo TOTAL METODO: [" + ( System.currentTimeMillis() - tiempoInicio ) + " milisegundos ]" );
			logger.info(mensajeLog + "[FIN] - METODO: [" + nombreMetodo + " - DAO] " );
		}
		
		return responseBean;
	}

}
