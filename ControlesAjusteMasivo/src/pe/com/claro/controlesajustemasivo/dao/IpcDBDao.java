package pe.com.claro.controlesajustemasivo.dao;

import pe.com.claro.controlesajustemasivo.bean.RegAjteMasDetIpcRequestBean;
import pe.com.claro.controlesajustemasivo.bean.RegAjteMasIpcRequestBean;
import pe.com.claro.controlesajustemasivo.bean.RegAjteMasIpcResponseBean;
import pe.com.claro.controlesajustemasivo.bean.ResponseBean;
import pe.com.claro.controlesajustemasivo.exception.DBException;

public interface IpcDBDao {

	RegAjteMasIpcResponseBean registrarAjusteMasivo(String mensajeLog, RegAjteMasIpcRequestBean requestBean) throws DBException;
	
	ResponseBean registrarAjusteMasivoDetalle(String mensajeLog, RegAjteMasDetIpcRequestBean requestBean) throws DBException;
	
}
