package pe.com.claro.controlesajustemasivo.dao;

import java.util.Date;

import pe.com.claro.controlesajustemasivo.bean.AjustesIpcSgaResponseBean;
import pe.com.claro.controlesajustemasivo.exception.DBException;

public interface SgaDao {

	AjustesIpcSgaResponseBean consultarAjustesIPC(String mensajeLog, Date fecha) throws DBException;
	
}
