package pe.com.claro.controlesajustemasivo.dao;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Locale;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.com.claro.controlesajustemasivo.bean.AjustesIpcResponseBean;
import pe.com.claro.controlesajustemasivo.bean.RegAjteMasDetIpcRequestBean;
import pe.com.claro.controlesajustemasivo.bean.RegAjteMasIpcRequestBean;
import pe.com.claro.controlesajustemasivo.bean.RegAjteMasIpcResponseBean;
import pe.com.claro.controlesajustemasivo.bean.ResponseBean;
import pe.com.claro.controlesajustemasivo.exception.DBException;
import pe.com.claro.controlesajustemasivo.util.Constantes;
import pe.com.claro.controlesajustemasivo.util.PropertiesExterno;

@Repository
public class IpcDBDaoImpl implements IpcDBDao {

	private static final Logger logger = Logger.getLogger(IpcDBDaoImpl.class);
	
	@Autowired
	@Qualifier("ipcdbDS")
	private DataSource ipcdbDS;
	
	@Autowired
	private PropertiesExterno propertiesExterno;
	
	@Override
	public RegAjteMasIpcResponseBean registrarAjusteMasivo(String mensajeLog, RegAjteMasIpcRequestBean requestBean)
			throws DBException {
		long tiempoInicio = System.currentTimeMillis();
		RegAjteMasIpcResponseBean responseBean = new RegAjteMasIpcResponseBean();
		String nombreMetodo = "registrarAjusteMasivo";
		StringBuffer storedProcedure = new StringBuffer();
		logger.info(mensajeLog + "[INICIO] - METODO: [" + nombreMetodo + " - DAO]");
		
		String nombreBD = propertiesExterno.DB_IPCDB_NOMBRE;
		String owner = propertiesExterno.DB_IPCDB_OWNER;
		String paquete = propertiesExterno.DB_IPCDB_PKG_MESA_CONTROL;
		String procedure = propertiesExterno.DB_IPCDB_SP_CTRLAJUSTESMASIVO;
		Integer timeoutEjecucion = propertiesExterno.DB_IPCDB_TIMEOUT_EJECUCION_SEGUNDOS;
		
		storedProcedure.append(owner);
		storedProcedure.append(Constantes.PUNTO);
		storedProcedure.append(paquete);
		storedProcedure.append(Constantes.PUNTO);
		storedProcedure.append(procedure);
		
		try {
			logger.info(mensajeLog + " - PROCEDURE: [" + storedProcedure.toString() + "]");
			logger.info(mensajeLog + " - TIMEOUT EJECUCION: [" + timeoutEjecucion + "]");
			
			SqlParameterSource inputProcedure = new MapSqlParameterSource()
					.addValue("PI_CANTIDADORIGEN_POS", requestBean.getCantidadOrigenPos(), OracleTypes.INTEGER)
					.addValue("PI_CANTIDADOAC_POS", requestBean.getCantidadOacPos(), OracleTypes.INTEGER)
					.addValue("PI_MONTOORIGEN_POS", requestBean.getMontoOrigenPos(), OracleTypes.DOUBLE)
					.addValue("PI_MONTOOAC_POS", requestBean.getMontoOacPos(), OracleTypes.DOUBLE)
					.addValue("PI_APLICACION", requestBean.getAplicacion(), OracleTypes.VARCHAR)
					.addValue("PI_USUARIO", requestBean.getUsuario(), OracleTypes.VARCHAR)
					.addValue("PI_CANTIDADORIGEN_NEG", requestBean.getCantidadOrigenNeg(), OracleTypes.INTEGER)
					.addValue("PI_CANTIDADOAC_NEG", requestBean.getCantidadOacNeg(), OracleTypes.INTEGER)
					.addValue("PI_MONTOORIGEN_NEG", requestBean.getMontoOrigenNeg(), OracleTypes.DOUBLE)
					.addValue("PI_MONTOOAC_NEG", requestBean.getMontoOacNeg(), OracleTypes.DOUBLE);
			
			logger.info(mensajeLog + " - PARAMETROS [INPUT] ");
			logger.info(mensajeLog + " - [INPUT][PI_CANTIDADORIGEN_POS]: " + requestBean.getCantidadOrigenPos());
			logger.info(mensajeLog + " - [INPUT][PI_CANTIDADOAC_POS]: " + requestBean.getCantidadOacPos());
			logger.info(mensajeLog + " - [INPUT][PI_MONTOORIGEN_POS]: " + requestBean.getMontoOrigenPos());
			logger.info(mensajeLog + " - [INPUT][PI_MONTOOAC_POS]: " + requestBean.getMontoOacPos());
			logger.info(mensajeLog + " - [INPUT][PI_APLICACION]: " + requestBean.getAplicacion());
			logger.info(mensajeLog + " - [INPUT][PI_USUARIO]: " + requestBean.getUsuario());
			logger.info(mensajeLog + " - [INPUT][PI_CANTIDADORIGEN_NEG]: " + requestBean.getCantidadOrigenNeg());
			logger.info(mensajeLog + " - [INPUT][PI_CANTIDADOAC_NEG]: " + requestBean.getCantidadOacNeg());
			logger.info(mensajeLog + " - [INPUT][PI_MONTOORIGEN_NEG]: " + requestBean.getMontoOrigenNeg());
			logger.info(mensajeLog + " - [INPUT][PI_MONTOOAC_NEG]: " + requestBean.getMontoOacNeg());
			
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(ipcdbDS);
			JdbcTemplate jdbcTemplate = simpleJdbcCall.getJdbcTemplate();
			jdbcTemplate.setQueryTimeout(timeoutEjecucion);
			
			simpleJdbcCall.withoutProcedureColumnMetaDataAccess();
			simpleJdbcCall.withSchemaName(owner);
			simpleJdbcCall.withCatalogName(paquete);
			simpleJdbcCall.withProcedureName(procedure);
			
			simpleJdbcCall.declareParameters(
					new SqlParameter("PI_CANTIDADORIGEN_POS", OracleTypes.INTEGER),
					new SqlParameter("PI_CANTIDADOAC_POS", OracleTypes.INTEGER),
					new SqlParameter("PI_MONTOORIGEN_POS", OracleTypes.DOUBLE),
					new SqlParameter("PI_MONTOOAC_POS", OracleTypes.DOUBLE),
					new SqlParameter("PI_APLICACION", OracleTypes.VARCHAR),
					new SqlParameter("PI_USUARIO", OracleTypes.VARCHAR),
					new SqlParameter("PI_CANTIDADORIGEN_NEG", OracleTypes.INTEGER),
					new SqlParameter("PI_CANTIDADOAC_NEG", OracleTypes.INTEGER),
					new SqlParameter("PI_MONTOORIGEN_NEG", OracleTypes.DOUBLE),
					new SqlParameter("PI_MONTOOAC_NEG", OracleTypes.DOUBLE),
					new SqlOutParameter("PO_ID", OracleTypes.INTEGER),
					new SqlOutParameter("PO_CODIGO", OracleTypes.INTEGER),
					new SqlOutParameter("PO_ERROR_MSG", OracleTypes.VARCHAR));
			Map<String, Object> outputProcedure = simpleJdbcCall.execute(inputProcedure);
			logger.info(mensajeLog + "SP ejecutado");
			
			Integer id = (Integer) outputProcedure.get("PO_ID");
			Integer codigo = (Integer) outputProcedure.get("PO_CODIGO");
			String mensaje = (String) outputProcedure.get("PO_ERROR_MSG");
			
			logger.info(mensajeLog + " - PARAMETROS [OUTPUT]");
			logger.info(mensajeLog + " - [OUTPUT][PO_ID]: [" + id + "]");
			logger.info(mensajeLog + " - [OUTPUT][PO_CODIGO]: [" + codigo + "]");
			logger.info(mensajeLog + " - [OUTPUT][PO_ERROR_MSG]: [" + mensaje + "]");
			
			responseBean.setCodigo(codigo);
			responseBean.setMensaje(mensaje);
			responseBean.setId(id);
		} catch (Exception e) {
			logger.error(mensajeLog + "ERROR: [Exception] - [" + e.getMessage() + "] ", e);
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			String descripcionError = String.valueOf(errors.toString());
			
			if (descripcionError.toUpperCase(Locale.getDefault()).contains(Constantes.SQLTIMEOUTEXCEPTION)) {
				throw new DBException("Error de Timeout hacia BD", e, nombreBD, storedProcedure.toString());
			} else {
				throw new DBException("Error de disponibilidad hacia BD", e, nombreBD, storedProcedure.toString());
			}
		} finally {
			logger.info(mensajeLog + " - Tiempo TOTAL METODO: [" + ( System.currentTimeMillis() - tiempoInicio ) + " milisegundos ]" );
			logger.info(mensajeLog + "[FIN] - METODO: [" + nombreMetodo + " - DAO] " );
		}
		
		return responseBean;
	}

	@Override
	public ResponseBean registrarAjusteMasivoDetalle(String mensajeLog, RegAjteMasDetIpcRequestBean requestBean)
			throws DBException {
		long tiempoInicio = System.currentTimeMillis();
		AjustesIpcResponseBean responseBean = new AjustesIpcResponseBean();
		String nombreMetodo = "registrarAjusteMasivoDetalle";
		StringBuffer storedProcedure = new StringBuffer();
		logger.info(mensajeLog + "[INICIO] - METODO: [" + nombreMetodo + " - DAO]");
		
		String nombreBD = propertiesExterno.DB_IPCDB_NOMBRE;
		String owner = propertiesExterno.DB_IPCDB_OWNER;
		String paquete = propertiesExterno.DB_IPCDB_PKG_MESA_CONTROL;
		String procedure = propertiesExterno.DB_IPCDB_SP_CTRLAJUSTESMASIVODET;
		Integer timeoutEjecucion = propertiesExterno.DB_IPCDB_TIMEOUT_EJECUCION_SEGUNDOS;
		
		storedProcedure.append(owner);
		storedProcedure.append(Constantes.PUNTO);
		storedProcedure.append(paquete);
		storedProcedure.append(Constantes.PUNTO);
		storedProcedure.append(procedure);
		
		try {
			logger.info(mensajeLog + " - PROCEDURE: [" + storedProcedure.toString() + "]");
			logger.info(mensajeLog + " - TIMEOUT EJECUCION: [" + timeoutEjecucion + "]");
			
			SqlParameterSource inputProcedure = new MapSqlParameterSource()
					.addValue("PI_IDCAB", requestBean.getIdCab(), OracleTypes.INTEGER)
					.addValue("PI_TIPOAJUSTE", requestBean.getTipoAjuste(), OracleTypes.VARCHAR)
					.addValue("PI_CUSTOMERID", requestBean.getCustomerId(), OracleTypes.VARCHAR)
					.addValue("PI_NUMEROAJUSTE", requestBean.getNumeroAjuste(), OracleTypes.VARCHAR)
					.addValue("PI_TIPOMONEDA", requestBean.getTipoMoneda(), OracleTypes.VARCHAR)
					.addValue("PI_MONTO", requestBean.getMonto(), OracleTypes.DOUBLE)
					.addValue("PI_TIPOAJUSTEOAC", requestBean.getTipoAjusteOAC(), OracleTypes.VARCHAR)
					.addValue("PI_CUSTOMERIDOAC", requestBean.getCustomerIdOAC(), OracleTypes.VARCHAR)
					.addValue("PI_NUMEROAJUSTEOAC", requestBean.getNumeroAjusteOAC(), OracleTypes.VARCHAR)
					.addValue("PI_TIPOMONEDAOAC", requestBean.getTipoMonedaOAC(), OracleTypes.VARCHAR)
					.addValue("PI_MONTOOAC", requestBean.getMontoOAC(), OracleTypes.DOUBLE)
					.addValue("PI_USUARIO", requestBean.getUsuario(), OracleTypes.VARCHAR);
			
			logger.info(mensajeLog + " - PARAMETROS [INPUT] ");
			logger.info(mensajeLog + " - [INPUT][PI_IDCAB]: " + requestBean.getIdCab());
			logger.info(mensajeLog + " - [INPUT][PI_TIPOAJUSTE]: " + requestBean.getTipoAjuste());
			logger.info(mensajeLog + " - [INPUT][PI_CUSTOMERID]: " + requestBean.getCustomerId());
			logger.info(mensajeLog + " - [INPUT][PI_NUMEROAJUSTE]: " + requestBean.getNumeroAjuste());
			logger.info(mensajeLog + " - [INPUT][PI_TIPOMONEDA]: " + requestBean.getTipoMoneda());
			logger.info(mensajeLog + " - [INPUT][PI_MONTO]: " + requestBean.getMonto());
			logger.info(mensajeLog + " - [INPUT][PI_TIPOAJUSTEOAC]: " + requestBean.getTipoAjusteOAC());
			logger.info(mensajeLog + " - [INPUT][PI_CUSTOMERIDOAC]: " + requestBean.getCustomerIdOAC());
			logger.info(mensajeLog + " - [INPUT][PI_NUMEROAJUSTEOAC]: " + requestBean.getNumeroAjusteOAC());
			logger.info(mensajeLog + " - [INPUT][PI_TIPOMONEDAOAC]: " + requestBean.getTipoMonedaOAC());
			logger.info(mensajeLog + " - [INPUT][PI_MONTOOAC]: " + requestBean.getMontoOAC());
			logger.info(mensajeLog + " - [INPUT][PI_USUARIO]: " + requestBean.getUsuario());
			
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(ipcdbDS);
			JdbcTemplate jdbcTemplate = simpleJdbcCall.getJdbcTemplate();
			jdbcTemplate.setQueryTimeout(timeoutEjecucion);
			
			simpleJdbcCall.withoutProcedureColumnMetaDataAccess();
			simpleJdbcCall.withSchemaName(owner);
			simpleJdbcCall.withCatalogName(paquete);
			simpleJdbcCall.withProcedureName(procedure);
			
			simpleJdbcCall.declareParameters(
					new SqlParameter("PI_IDCAB", OracleTypes.INTEGER),
					new SqlParameter("PI_TIPOAJUSTE", OracleTypes.VARCHAR),
					new SqlParameter("PI_CUSTOMERID", OracleTypes.VARCHAR),
					new SqlParameter("PI_NUMEROAJUSTE", OracleTypes.VARCHAR),
					new SqlParameter("PI_TIPOMONEDA", OracleTypes.VARCHAR),
					new SqlParameter("PI_MONTO", OracleTypes.DOUBLE),
					new SqlParameter("PI_TIPOAJUSTEOAC", OracleTypes.VARCHAR),
					new SqlParameter("PI_CUSTOMERIDOAC", OracleTypes.VARCHAR),
					new SqlParameter("PI_NUMEROAJUSTEOAC", OracleTypes.VARCHAR),
					new SqlParameter("PI_TIPOMONEDAOAC", OracleTypes.VARCHAR),
					new SqlParameter("PI_MONTOOAC", OracleTypes.DOUBLE),
					new SqlParameter("PI_USUARIO", OracleTypes.VARCHAR),
					new SqlOutParameter("PO_CODIGO", OracleTypes.INTEGER),
					new SqlOutParameter("PO_ERROR_MSG", OracleTypes.VARCHAR));
			Map<String, Object> outputProcedure = simpleJdbcCall.execute(inputProcedure);
			logger.info(mensajeLog + "SP ejecutado");
			
			Integer codigo = (Integer) outputProcedure.get("PO_CODIGO");
			String mensaje = (String) outputProcedure.get("PO_ERROR_MSG");
			
			logger.info(mensajeLog + " - PARAMETROS [OUTPUT]");
			logger.info(mensajeLog + " - [OUTPUT][PO_CODIGO]: [" + codigo + "]");
			logger.info(mensajeLog + " - [OUTPUT][PO_ERROR_MSG]: [" + mensaje + "]");
			
			responseBean.setCodigo(codigo);
			responseBean.setMensaje(mensaje);
		} catch (Exception e) {
			logger.error(mensajeLog + "ERROR: [Exception] - [" + e.getMessage() + "] ", e);
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			String descripcionError = String.valueOf(errors.toString());
			
			if (descripcionError.toUpperCase(Locale.getDefault()).contains(Constantes.SQLTIMEOUTEXCEPTION)) {
				throw new DBException("Error de Timeout hacia BD", e, nombreBD, storedProcedure.toString());
			} else {
				throw new DBException("Error de disponibilidad hacia BD", e, nombreBD, storedProcedure.toString());
			}
		} finally {
			logger.info(mensajeLog + " - Tiempo TOTAL METODO: [" + ( System.currentTimeMillis() - tiempoInicio ) + " milisegundos ]" );
			logger.info(mensajeLog + "[FIN] - METODO: [" + nombreMetodo + " - DAO] " );
		}
		
		return responseBean;
	}

}
