package pe.com.claro.controlesajustemasivo.dao;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Locale;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.com.claro.controlesajustemasivo.bean.AjustesIpcRequestBean;
import pe.com.claro.controlesajustemasivo.bean.AjustesIpcResponseBean;
import pe.com.claro.controlesajustemasivo.exception.DBException;
import pe.com.claro.controlesajustemasivo.util.Constantes;
import pe.com.claro.controlesajustemasivo.util.PropertiesExterno;

@Repository
public class SiopDaoImpl implements SiopDao {

	private static final Logger logger = Logger.getLogger(SiopDaoImpl.class);
	
	@Autowired
	@Qualifier("siopDS")
	private DataSource siopDS;
	
	@Autowired
	private PropertiesExterno propertiesExterno;
	
	@Override
	public AjustesIpcResponseBean consultarAjustesIpc(String mensajeLog, AjustesIpcRequestBean requestBean)
			throws DBException {
		long tiempoInicio = System.currentTimeMillis();
		AjustesIpcResponseBean responseBean = new AjustesIpcResponseBean();
		String nombreMetodo = "consultarAjustesIpc";
		StringBuffer storedProcedure = new StringBuffer();
		logger.info(mensajeLog + "[INICIO] - METODO: [" + nombreMetodo + " - DAO]");
		
		String nombreBD = propertiesExterno.DB_SIOP_NOMBRE;
		String owner = propertiesExterno.DB_SIOP_OWNER;
		String paquete = propertiesExterno.DB_SIOP_PKG_AJUSTEMASIVO;
		String procedure = propertiesExterno.DB_SIOP_SP_AJUSTEIPC;
		Integer timeoutEjecucion = propertiesExterno.DB_SIOP_TIMEOUT_EJECUCION_SEGUNDOS;
		
		storedProcedure.append(owner);
		storedProcedure.append(Constantes.PUNTO);
		storedProcedure.append(paquete);
		storedProcedure.append(Constantes.PUNTO);
		storedProcedure.append(procedure);
		
		try {
			logger.info(mensajeLog + " - PROCEDURE: [" + storedProcedure.toString() + "]");
			logger.info(mensajeLog + " - TIMEOUT EJECUCION: [" + timeoutEjecucion + "]");
			
			SqlParameterSource inputProcedure = new MapSqlParameterSource()
					.addValue("K_CUSTOMERID", requestBean.getCustomerId(), OracleTypes.VARCHAR)
					.addValue("K_NRODOCAJUSTE", requestBean.getNumeroAjuste(), OracleTypes.VARCHAR);
			
			logger.info(mensajeLog + " - PARAMETROS [INPUT] ");
			logger.info(mensajeLog + " - [INPUT][K_CUSTOMERID]: " + requestBean.getCustomerId());
			logger.info(mensajeLog + " - [INPUT][K_NRODOCAJUSTE]: " + requestBean.getNumeroAjuste());
			
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(siopDS);
			JdbcTemplate jdbcTemplate = simpleJdbcCall.getJdbcTemplate();
			jdbcTemplate.setQueryTimeout(timeoutEjecucion);
			
			simpleJdbcCall.withoutProcedureColumnMetaDataAccess();
			simpleJdbcCall.withSchemaName(owner);
			simpleJdbcCall.withCatalogName(paquete);
			simpleJdbcCall.withProcedureName(procedure);
			
			simpleJdbcCall.declareParameters(
					new SqlParameter("K_CUSTOMERID", OracleTypes.VARCHAR),
					new SqlParameter("K_NRODOCAJUSTE", OracleTypes.VARCHAR),
					new SqlOutParameter("KO_MONTO", OracleTypes.DOUBLE),
					new SqlOutParameter("KO_MONEDA", OracleTypes.VARCHAR),
					new SqlOutParameter("KO_CODIGO", OracleTypes.INTEGER),
					new SqlOutParameter("KO_MENSAJE", OracleTypes.VARCHAR));
			Map<String, Object> outputProcedure = simpleJdbcCall.execute(inputProcedure);
			logger.info(mensajeLog + "SP ejecutado");
			
			String moneda = (String) outputProcedure.get("KO_MONEDA");
			double monto = outputProcedure.get("KO_MONTO") == null ? Constantes.CERO : (Double) outputProcedure.get("KO_MONTO");
			Integer codigo = (Integer) outputProcedure.get("KO_CODIGO");
			String mensaje = (String) outputProcedure.get("KO_MENSAJE");
			
			logger.info(mensajeLog + " - PARAMETROS [OUTPUT]");
			logger.info(mensajeLog + " - [OUTPUT][KO_MONEDA]: [" + moneda + "]");
			logger.info(mensajeLog + " - [OUTPUT][KO_MONTO]: [" + monto + "]");
			logger.info(mensajeLog + " - [OUTPUT][KO_CODIGO]: [" + codigo + "]");
			logger.info(mensajeLog + " - [OUTPUT][KO_MENSAJE]: [" + mensaje + "]");
			
			responseBean.setCodigo(codigo);
			responseBean.setMensaje(mensaje);
			responseBean.setTipoMoneda(moneda);
			responseBean.setMonto(monto);
		} catch (Exception e) {
			logger.error(mensajeLog + "ERROR: [Exception] - [" + e.getMessage() + "] ", e);
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			String descripcionError = String.valueOf(errors.toString());
			
			if (descripcionError.toUpperCase(Locale.getDefault()).contains(Constantes.SQLTIMEOUTEXCEPTION)) {
				throw new DBException("Error de Timeout hacia BD", e, nombreBD, storedProcedure.toString());
			} else {
				throw new DBException("Error de disponibilidad hacia BD", e, nombreBD, storedProcedure.toString());
			}
		} finally {
			logger.info(mensajeLog + " - Tiempo TOTAL METODO: [" + ( System.currentTimeMillis() - tiempoInicio ) + " milisegundos ]" );
			logger.info(mensajeLog + "[FIN] - METODO: [" + nombreMetodo + " - DAO] " );
		}
		
		return responseBean;
	}

}
