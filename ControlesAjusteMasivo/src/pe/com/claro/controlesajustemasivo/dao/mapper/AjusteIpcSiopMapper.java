package pe.com.claro.controlesajustemasivo.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pe.com.claro.controlesajustemasivo.bean.AjusteIPCBean;

public class AjusteIpcSiopMapper implements RowMapper<AjusteIPCBean> {

	@Override
	public AjusteIPCBean mapRow(ResultSet rs, int fila) throws SQLException {
		AjusteIPCBean bean = new AjusteIPCBean();
		bean.setCustomerId(rs.getString("K_CUSTOMERID"));
		bean.setNumeroAjuste(rs.getString("K_NUMERODOC"));
		bean.setMonto(rs.getDouble("K_MONTO"));
		bean.setTipoMoneda(rs.getString("K_MONEDA"));
		return bean;
	}

}
