package pe.com.claro.controlesajustemasivo.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pe.com.claro.controlesajustemasivo.bean.AjusteIPCBean;

public class AjusteIpcSgaMapper implements RowMapper<AjusteIPCBean> {

	@Override
	public AjusteIPCBean mapRow(ResultSet rs, int fila) throws SQLException {
		AjusteIPCBean bean = new AjusteIPCBean();
		bean.setTipoAjuste(rs.getString("K_TIPOAJUSTE"));
		bean.setNumeroAjuste(rs.getString("K_NUMEROAJUSTE"));
		bean.setCustomerId(rs.getString("K_CUSTOMERID"));
		bean.setTipoMoneda(rs.getString("K_TIPOMONEDA"));
		bean.setMonto(rs.getDouble("K_MONTO"));
		return bean;
	}

}
