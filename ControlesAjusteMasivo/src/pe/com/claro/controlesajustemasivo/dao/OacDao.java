package pe.com.claro.controlesajustemasivo.dao;

import java.util.Date;

import pe.com.claro.controlesajustemasivo.bean.AjustesIpcRequestBean;
import pe.com.claro.controlesajustemasivo.bean.AjustesIpcResponseBean;
import pe.com.claro.controlesajustemasivo.exception.DBException;

public interface OacDao {

	AjustesIpcResponseBean consultarAjustesIpcSga(String mensajeLog, AjustesIpcRequestBean requestBean) throws DBException;
	
	AjustesIpcResponseBean consultarAjustesIpcSiop(String mensajeLog, Date fechaProceso) throws DBException;
	
}
