package pe.com.claro.controlesajustemasivo.dao;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import pe.com.claro.controlesajustemasivo.bean.AjusteIPCBean;
import pe.com.claro.controlesajustemasivo.bean.AjustesIpcRequestBean;
import pe.com.claro.controlesajustemasivo.bean.AjustesIpcResponseBean;
import pe.com.claro.controlesajustemasivo.exception.DBException;
import pe.com.claro.controlesajustemasivo.util.Constantes;
import pe.com.claro.controlesajustemasivo.util.PropertiesExterno;

@Repository
public class OacDaoImpl implements OacDao {

	private static final Logger logger = Logger.getLogger(OacDaoImpl.class);
	
	@Autowired
	@Qualifier("oacDS")
	private DataSource oacDS;
	
	@Autowired
	private PropertiesExterno propertiesExterno;
	
	@Override
	public AjustesIpcResponseBean consultarAjustesIpcSga(String mensajeLog, AjustesIpcRequestBean requestBean)
			throws DBException {
		long tiempoInicio = System.currentTimeMillis();
		AjustesIpcResponseBean responseBean = new AjustesIpcResponseBean();
		String nombreMetodo = "consultarAjustesIpcSga";
		StringBuffer storedProcedure = new StringBuffer();
		logger.info(mensajeLog + "[INICIO] - METODO: [" + nombreMetodo + " - DAO]");
		
		String nombreBD = propertiesExterno.DB_OAC_NOMBRE;
		String owner = propertiesExterno.DB_OAC_OWNER;
		String paquete = propertiesExterno.DB_OAC_PKG_HBDO_AJUSTES;
		String procedure = propertiesExterno.DB_OAC_SP_AJUSTESIPC;
		Integer timeoutEjecucion = propertiesExterno.DB_OAC_TIMEOUT_EJECUCION_SEGUNDOS;
		
		storedProcedure.append(owner);
		storedProcedure.append(Constantes.PUNTO);
		storedProcedure.append(paquete);
		storedProcedure.append(Constantes.PUNTO);
		storedProcedure.append(procedure);
		
		try {
			logger.info(mensajeLog + " - PROCEDURE: [" + storedProcedure.toString() + "]");
			logger.info(mensajeLog + " - TIMEOUT EJECUCION: [" + timeoutEjecucion + "]");
			
			SqlParameterSource inputProcedure = new MapSqlParameterSource()
					.addValue("K_TIPOAJUSTE", requestBean.getTipoAjuste(), OracleTypes.VARCHAR)
					.addValue("K_NUMEROAJUSTE", requestBean.getNumeroAjuste(), OracleTypes.VARCHAR);
			
			logger.info(mensajeLog + " - PARAMETROS [INPUT] ");
			logger.info(mensajeLog + " - [INPUT][K_TIPOAJUSTE]: " + requestBean.getTipoAjuste());
			logger.info(mensajeLog + " - [INPUT][K_NUMEROAJUSTE]: " + requestBean.getNumeroAjuste());
			
			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(oacDS);
			JdbcTemplate jdbcTemplate = simpleJdbcCall.getJdbcTemplate();
			jdbcTemplate.setQueryTimeout(timeoutEjecucion);
			
			simpleJdbcCall.withoutProcedureColumnMetaDataAccess();
			simpleJdbcCall.withSchemaName(owner);
			simpleJdbcCall.withCatalogName(paquete);
			simpleJdbcCall.withProcedureName(procedure);
			
			simpleJdbcCall.declareParameters(
					new SqlParameter("K_TIPOAJUSTE", OracleTypes.VARCHAR),
					new SqlParameter("K_NUMEROAJUSTE", OracleTypes.VARCHAR),
					new SqlOutParameter("KO_CUSTOMERID", OracleTypes.VARCHAR),
					new SqlOutParameter("KO_TIPOMONEDA", OracleTypes.VARCHAR),
					new SqlOutParameter("KO_MONTO", OracleTypes.DOUBLE),
					new SqlOutParameter("KO_STATUS", OracleTypes.VARCHAR),
					new SqlOutParameter("KO_MESSAGE", OracleTypes.VARCHAR));
			Map<String, Object> outputProcedure = simpleJdbcCall.execute(inputProcedure);
			logger.info(mensajeLog + "SP ejecutado");
			
			String customerId = (String) outputProcedure.get("KO_CUSTOMERID");
			String tipoMoneda = (String) outputProcedure.get("KO_TIPOMONEDA");
			Double monto = (Double) outputProcedure.get("KO_MONTO");
			String status = (String) outputProcedure.get("KO_STATUS");
			String mensaje = (String) outputProcedure.get("KO_MESSAGE");
			
			logger.info(mensajeLog + " - PARAMETROS [OUTPUT]");
			logger.info(mensajeLog + " - [OUTPUT][KO_CUSTOMERID]: [" + customerId + "]");
			logger.info(mensajeLog + " - [OUTPUT][KO_TIPOMONEDA]: [" + tipoMoneda + "]");
			logger.info(mensajeLog + " - [OUTPUT][KO_MONTO]: [" + monto + "]");
			logger.info(mensajeLog + " - [OUTPUT][KO_STATUS]: [" + status + "]");
			logger.info(mensajeLog + " - [OUTPUT][KO_MESSAGE]: [" + mensaje + "]");
			
			responseBean.setCodigo(Integer.valueOf(status));
			responseBean.setMensaje(mensaje);
			responseBean.setCustomerId(customerId);
			responseBean.setTipoMoneda(tipoMoneda);
			responseBean.setMonto(monto);
		} catch (Exception e) {
			logger.error(mensajeLog + "ERROR: [Exception] - [" + e.getMessage() + "] ", e);
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			String descripcionError = String.valueOf(errors.toString());
			
			if (descripcionError.toUpperCase(Locale.getDefault()).contains(Constantes.SQLTIMEOUTEXCEPTION)) {
				throw new DBException("Error de Timeout hacia BD", e, nombreBD, storedProcedure.toString());
			} else {
				throw new DBException("Error de disponibilidad hacia BD", e, nombreBD, storedProcedure.toString());
			}
		} finally {
			logger.info(mensajeLog + " - Tiempo TOTAL METODO: [" + ( System.currentTimeMillis() - tiempoInicio ) + " milisegundos ]" );
			logger.info(mensajeLog + "[FIN] - METODO: [" + nombreMetodo + " - DAO] " );
		}
		
		return responseBean;
	}

//	@Override
//	public AjustesIpcResponseBean consultarAjustesIpcSiop(String mensajeLog, Date fechaProceso) throws DBException {
//		long tiempoInicio = System.currentTimeMillis();
//		AjustesIpcResponseBean responseBean = new AjustesIpcResponseBean();
//		String nombreMetodo = "consultarAjustesIpcSiop";
//		StringBuffer storedProcedure = new StringBuffer();
//		logger.info(mensajeLog + "[INICIO] - METODO: [" + nombreMetodo + " - DAO]");
//		
//		String nombreBD = propertiesExterno.DB_OAC_NOMBRE;
//		String owner = propertiesExterno.DB_OAC_OWNER;
//		String paquete = propertiesExterno.DB_OAC_PKG_CL_AJUSTES;
//		String procedure = propertiesExterno.DB_OAC_SP_AJUSTESIPCSIOP;
//		Integer timeoutEjecucion = propertiesExterno.DB_OAC_TIMEOUT_EJECUCION_SEGUNDOS;
//		
//		storedProcedure.append(owner);
//		storedProcedure.append(Constantes.PUNTO);
//		storedProcedure.append(paquete);
//		storedProcedure.append(Constantes.PUNTO);
//		storedProcedure.append(procedure);
//		
//		try {
//			logger.info(mensajeLog + " - PROCEDURE: [" + storedProcedure.toString() + "]");
//			logger.info(mensajeLog + " - TIMEOUT EJECUCION: [" + timeoutEjecucion + "]");
//			
//			SqlParameterSource inputProcedure = new MapSqlParameterSource()
//					.addValue("PI_FECHAPROCESO", fechaProceso, OracleTypes.DATE);
//			
//			logger.info(mensajeLog + " - PARAMETROS [INPUT] ");
//			logger.info(mensajeLog + " - [INPUT][PI_FECHAPROCESO]: " + fechaProceso);
//			
//			SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(oacDS);
//			JdbcTemplate jdbcTemplate = simpleJdbcCall.getJdbcTemplate();
//			jdbcTemplate.setQueryTimeout(timeoutEjecucion);
//			
//			simpleJdbcCall.withoutProcedureColumnMetaDataAccess();
//			simpleJdbcCall.withSchemaName(owner);
//			simpleJdbcCall.withCatalogName(paquete);
//			simpleJdbcCall.withProcedureName(procedure);
//			
//			simpleJdbcCall.declareParameters(
//					new SqlParameter("PI_FECHAPROCESO", OracleTypes.DATE),
//					new SqlOutParameter("PO_CURSOR", OracleTypes.CURSOR, new AjusteIpcSiopMapper()),
//					new SqlOutParameter("PO_CODERROR", OracleTypes.VARCHAR),
//					new SqlOutParameter("PO_DESERROR", OracleTypes.VARCHAR));
//			Map<String, Object> outputProcedure = simpleJdbcCall.execute(inputProcedure);
//			logger.info(mensajeLog + "SP ejecutado");
//			
//			List<AjusteIPCBean> listaAjustes = outputProcedure.get("PO_CURSOR") == null ? 
//					new ArrayList<AjusteIPCBean>() : (List<AjusteIPCBean>) outputProcedure.get("PO_CURSOR");
//			String codigo = (String) outputProcedure.get("PO_CODERROR");
//			String mensaje = (String) outputProcedure.get("PO_DESERROR");
//			
//			logger.info(mensajeLog + " - PARAMETROS [OUTPUT]");
//			logger.info(mensajeLog + " - [OUTPUT][PO_CURSOR][Cantidad de registros]: [" + listaAjustes.size() + "]");
//			logger.info(mensajeLog + " - [OUTPUT][PO_CODERROR]: [" + codigo + "]");
//			logger.info(mensajeLog + " - [OUTPUT][PO_DESERROR]: [" + mensaje + "]");
//			
//			responseBean.setCodigo(Integer.valueOf(codigo));
//			responseBean.setMensaje(mensaje);
//			responseBean.setListaAjustes(listaAjustes);
//		} catch (Exception e) {
//			logger.error(mensajeLog + "ERROR: [Exception] - [" + e.getMessage() + "] ", e);
//			StringWriter errors = new StringWriter();
//			e.printStackTrace(new PrintWriter(errors));
//			String descripcionError = String.valueOf(errors.toString());
//			
//			if (descripcionError.toUpperCase(Locale.getDefault()).contains(Constantes.SQLTIMEOUTEXCEPTION)) {
//				throw new DBException("Error de Timeout hacia BD", e, nombreBD, storedProcedure.toString());
//			} else {
//				throw new DBException("Error de disponibilidad hacia BD", e, nombreBD, storedProcedure.toString());
//			}
//		} finally {
//			logger.info(mensajeLog + " - Tiempo TOTAL METODO: [" + ( System.currentTimeMillis() - tiempoInicio ) + " milisegundos ]" );
//			logger.info(mensajeLog + "[FIN] - METODO: [" + nombreMetodo + " - DAO] " );
//		}
//		
//		return responseBean;
//	}
	
	@Override
	public AjustesIpcResponseBean consultarAjustesIpcSiop(String mensajeLog, Date fechaProceso) throws DBException {
		long tiempoInicio = System.currentTimeMillis();
		AjustesIpcResponseBean responseBean = new AjustesIpcResponseBean();
		String nombreMetodo = "consultarAjustesIpcSiop";
		StringBuffer storedProcedure = new StringBuffer();
		logger.info(mensajeLog + "[INICIO] - METODO: [" + nombreMetodo + " - DAO]");
		
		String nombreBD = propertiesExterno.DB_OAC_NOMBRE;
		String owner = propertiesExterno.DB_OAC_OWNER;
		String paquete = propertiesExterno.DB_OAC_PKG_CL_AJUSTES;
		String procedure = propertiesExterno.DB_OAC_SP_AJUSTESIPCSIOP;
		Integer timeoutEjecucion = propertiesExterno.DB_OAC_TIMEOUT_EJECUCION_SEGUNDOS;
		
		storedProcedure.append(owner);
		storedProcedure.append(Constantes.PUNTO);
		storedProcedure.append(paquete);
		storedProcedure.append(Constantes.PUNTO);
		storedProcedure.append(procedure);
		
		try {
			logger.info(mensajeLog + " - PROCEDURE: [" + storedProcedure.toString() + "]");
			logger.info(mensajeLog + " - TIMEOUT EJECUCION: [" + timeoutEjecucion + "]");
			
			java.sql.Date fechaProcesoSql = new java.sql.Date(fechaProceso.getTime());
			logger.info(mensajeLog + " - PARAMETROS [INPUT] ");
			logger.info(mensajeLog + " - [INPUT][PI_FECHAPROCESO]: " + fechaProcesoSql);
			
			Connection cn = oacDS.getConnection();
			CallableStatement call = cn.prepareCall("CALL " + storedProcedure.toString() + " (?,?,?,?)");
			call.setQueryTimeout(timeoutEjecucion);
			call.setDate(1, fechaProcesoSql);//PI_FECHAPROCESO
			call.registerOutParameter(2, OracleTypes.CURSOR);//PO_CURSOR
			call.registerOutParameter(3, OracleTypes.VARCHAR);//PO_CODERROR
			call.registerOutParameter(4, OracleTypes.VARCHAR);//PO_DESERROR
			call.execute();
			logger.info(mensajeLog + "SP ejecutado");
			
			ResultSet rs = (ResultSet) call.getObject(2);
			List<AjusteIPCBean> listaAjustes = new ArrayList<AjusteIPCBean>();
			AjusteIPCBean ajuste;
			while (rs != null && rs.next()) {
				ajuste = new AjusteIPCBean();
				ajuste.setCustomerId(rs.getString("K_CUSTOMERID"));
				ajuste.setNumeroAjuste(rs.getString("K_NUMERODOC"));
				ajuste.setMonto(rs.getDouble("K_MONTO"));
				ajuste.setTipoMoneda(rs.getString("K_MONEDA"));
				listaAjustes.add(ajuste);
			}
			
			String codigo = call.getString(3);
			String mensaje = call.getString(4);
			rs.close();
			
			logger.info(mensajeLog + " - PARAMETROS [OUTPUT]");
			logger.info(mensajeLog + " - [OUTPUT][PO_CURSOR][Cantidad de registros]: [" + listaAjustes.size() + "]");
			logger.info(mensajeLog + " - [OUTPUT][PO_CODERROR]: [" + codigo + "]");
			logger.info(mensajeLog + " - [OUTPUT][PO_DESERROR]: [" + mensaje + "]");
			
			responseBean.setCodigo(Integer.valueOf(codigo));
			responseBean.setMensaje(mensaje);
			responseBean.setListaAjustes(listaAjustes);
		} catch (Exception e) {
			logger.error(mensajeLog + "ERROR: [Exception] - [" + e.getMessage() + "] ", e);
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			String descripcionError = String.valueOf(errors.toString());
			
			if (descripcionError.toUpperCase(Locale.getDefault()).contains(Constantes.SQLTIMEOUTEXCEPTION)) {
				throw new DBException("Error de Timeout hacia BD", e, nombreBD, storedProcedure.toString());
			} else {
				throw new DBException("Error de disponibilidad hacia BD", e, nombreBD, storedProcedure.toString());
			}
		} finally {
			logger.info(mensajeLog + " - Tiempo TOTAL METODO: [" + ( System.currentTimeMillis() - tiempoInicio ) + " milisegundos ]" );
			logger.info(mensajeLog + "[FIN] - METODO: [" + nombreMetodo + " - DAO] " );
		}
		
		return responseBean;
	}

}
