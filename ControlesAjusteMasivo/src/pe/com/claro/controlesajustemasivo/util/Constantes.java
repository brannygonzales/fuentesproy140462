package pe.com.claro.controlesajustemasivo.util;

public class Constantes {

	public static final String PUNTO = ".";
	public static final String VACIO = "";
	
	public static final String SQLTIMEOUTEXCEPTION = "SQLTIMEOUTEXCEPTION";
	
	public static final int _UNO = -1;
	public static final int CERO = 0;
	
	public static final String APLICACION_SIOP = "SIOP";
	public static final String APLICACION_SGA = "SGA";
	
	public static final String FORMATO_FECHA_DD_MM_YYYY = "dd/MM/yyyy";
	
	public static final String REPLACE_FECHA = "[DDMMYYYY]";
	public static final String REPLACE_TIPO_ERROR = "[TIPO_ERROR]";
	public static final String REPLACE_MENSAJE_ERROR = "[MENSAJE_ERROR]";
	
	public static final String TIPO_ERROR_BASE_DATOS = "de Base de Datos";
	public static final String TIPO_ERROR_FUNCIONAL = "funcional";
	public static final String TIPO_ERROR_INESPERADO = "inesperado";
	
}
