package pe.com.claro.controlesajustemasivo.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PropertiesExterno {

	@Value("${log4j.file.dir}")
	public String LOG4J_FILE_DIR;
	
	@Value("${oracle.jdbc.ipcdb.usuario}")
	public String ORACLE_JDBC_IPCDB_USUARIO;
	
	@Value("${db.sga.nombre}")
	public String DB_SGA_NOMBRE;
	
	@Value("${db.sga.owner}")
	public String DB_SGA_OWNER;
	
	@Value("${db.sga.pkg.ajustemasivos}")
	public String DB_SGA_PKG_AJUSTEMASIVOS;
	
	@Value("${db.sga.sp.ajustesipc}")
	public String DB_SGA_SP_AJUSTESIPC;
	
	@Value("${db.sga.timeout.ejecucion.segundos}")
	public Integer DB_SGA_TIMEOUT_EJECUCION_SEGUNDOS;
	
	@Value("${db.oac.nombre}")
	public String DB_OAC_NOMBRE;
	
	@Value("${db.oac.owner}")
	public String DB_OAC_OWNER;
	
	@Value("${db.oac.pkg.hbdo.ajustes}")
	public String DB_OAC_PKG_HBDO_AJUSTES;
	
	@Value("${db.oac.sp.ajustesipc}")
	public String DB_OAC_SP_AJUSTESIPC;
	
	@Value("${db.oac.pkg.cl.ajustes}")
	public String DB_OAC_PKG_CL_AJUSTES;
	
	@Value("${db.oac.sp.ajustesipcsiop}")
	public String DB_OAC_SP_AJUSTESIPCSIOP;
	
	@Value("${db.oac.timeout.ejecucion.segundos}")
	public Integer DB_OAC_TIMEOUT_EJECUCION_SEGUNDOS;
	
	@Value("${db.siop.nombre}")
	public String DB_SIOP_NOMBRE;
	
	@Value("${db.siop.owner}")
	public String DB_SIOP_OWNER;
	
	@Value("${db.siop.pkg.ajustemasivo}")
	public String DB_SIOP_PKG_AJUSTEMASIVO;
	
	@Value("${db.siop.sp.ajusteipc}")
	public String DB_SIOP_SP_AJUSTEIPC;
	
	@Value("${db.siop.timeout.ejecucion.segundos}")
	public Integer DB_SIOP_TIMEOUT_EJECUCION_SEGUNDOS;
	
	@Value("${db.ipcdb.nombre}")
	public String DB_IPCDB_NOMBRE;
	
	@Value("${db.ipcdb.owner}")
	public String DB_IPCDB_OWNER;
	
	@Value("${db.ipcdb.pkg.mesa.control}")
	public String DB_IPCDB_PKG_MESA_CONTROL;
	
	@Value("${db.ipcdb.sp.ctrlajustemasivo}")
	public String DB_IPCDB_SP_CTRLAJUSTESMASIVO;
	
	@Value("${db.ipcdb.sp.ctrlajustemasivodet}")
	public String DB_IPCDB_SP_CTRLAJUSTESMASIVODET;
	
	@Value("${db.ipcdb.timeout.ejecucion.segundos}")
	public Integer DB_IPCDB_TIMEOUT_EJECUCION_SEGUNDOS;
	
	@Value("${ws.envioCorreo.endpointaddress}")
	public String WS_ENVIOCORREO_ENDPOINTADDRESS;
	
	@Value("${ws.envioCorreo.max.timeout.conexion}")
	public String WS_ENVIOCORREO_MAX_TIMEOUT_CONEXION;
	
	@Value("${ws.envioCorreo.max.timeout.request}")
	public String WS_ENVIOCORREO_MAX_TIMEOUT_REQUEST;
	
	@Value("${ws.envioCorreo.destinatario}")
    public String WS_ENVIOCORREO_DESTINATARIO;
    
    @Value("${ws.envioCorreo.remitente}")
    public String WS_ENVIOCORREO_REMITENTE;
    
    @Value("${ws.envioCorreo.asunto}")
    public String WS_ENVIOCORREO_ASUNTO;
    
    @Value("${ws.envioCorreo.html.flag}")
    public String WS_ENVIOCORREO_HTMLFLAG;
    
    @Value("${ws.envioCorreo.mensaje}")
    public String WS_ENVIOCORREO_MENSAJE;
    
    @Value("${ws.param.auditoria.usuario.aplicacion}")
    public String WS_PARAM_AUDITORIA_USUARIOAPLICACION;
    
    @Value("${ws.param.auditoria.nombre.aplicacion}")
    public String WS_PARAM_AUDITORIA_NOMBREAPLICACION;
	
}
