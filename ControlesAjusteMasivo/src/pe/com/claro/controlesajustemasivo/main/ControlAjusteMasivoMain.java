package pe.com.claro.controlesajustemasivo.main;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import pe.com.claro.controlesajustemasivo.business.ControlAjusteMasivoBusiness;
import pe.com.claro.controlesajustemasivo.util.PropertiesExterno;

@Component
public class ControlAjusteMasivoMain {

	private static final Logger logger = Logger.getLogger(ControlAjusteMasivoMain.class);
	private static final String CONFIG_PATH = "./spring/applicationContext.xml";
	private static ApplicationContext objContextoSpring;
	
	@Autowired
	private ControlAjusteMasivoBusiness controlAjusteMasivo;
	
	@Autowired
	private PropertiesExterno propertiesExterno;
	
	public static void main(String[] args) {
		long tiempoInicio = System.currentTimeMillis();
		String idTransaccion = args[0];
		String msjTx = "[main idTx=" + idTransaccion + "] ";
	    
	    try {
	    	objContextoSpring = new ClassPathXmlApplicationContext( CONFIG_PATH );
	    	ControlAjusteMasivoMain main = (ControlAjusteMasivoMain) objContextoSpring.getBean(ControlAjusteMasivoMain.class);
	    	main.configurarProperties();
			logger.info(msjTx + "=================================");
		    logger.info(msjTx + "====== Inicio del Proceso =======");
	    	main.iniciarProceso(msjTx, args);
	    } catch (Exception e) {
			logger.error(msjTx + "Error al Levantar Contexto Spring: " + e.getMessage(), e);
		} finally {
			logger.info(msjTx + "###### [FIN]. Tiempo total del proceso: " + (System.currentTimeMillis() - tiempoInicio) + " (ms) ######");
		}
	}
	
	public void iniciarProceso(String mensajeLog, String[] args) {
		String idTransaccion = args[0];
		controlAjusteMasivo.ejecutarProceso(mensajeLog, idTransaccion);
	}
	
	private void configurarProperties() {
		PropertyConfigurator.configure(propertiesExterno.LOG4J_FILE_DIR);
	}

}
