package pe.com.claro.controlesajustemasivo.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pe.com.claro.controlesajustemasivo.bean.AjusteIPCBean;
import pe.com.claro.controlesajustemasivo.bean.AjustesIpcRequestBean;
import pe.com.claro.controlesajustemasivo.bean.AjustesIpcResponseBean;
import pe.com.claro.controlesajustemasivo.bean.AjustesIpcSgaResponseBean;
import pe.com.claro.controlesajustemasivo.bean.RegAjteMasDetIpcRequestBean;
import pe.com.claro.controlesajustemasivo.bean.RegAjteMasIpcRequestBean;
import pe.com.claro.controlesajustemasivo.bean.RegAjteMasIpcResponseBean;
import pe.com.claro.controlesajustemasivo.bean.ResponseBean;
import pe.com.claro.controlesajustemasivo.dao.IpcDBDao;
import pe.com.claro.controlesajustemasivo.dao.OacDao;
import pe.com.claro.controlesajustemasivo.dao.SgaDao;
import pe.com.claro.controlesajustemasivo.dao.SiopDao;
import pe.com.claro.controlesajustemasivo.exception.DBException;
import pe.com.claro.controlesajustemasivo.exception.ProcesoException;
import pe.com.claro.controlesajustemasivo.service.EnvioCorreoSBSoap;
import pe.com.claro.controlesajustemasivo.service.type.AuditoriaRequest;
import pe.com.claro.controlesajustemasivo.service.type.EnvioCorreoRequest;
import pe.com.claro.controlesajustemasivo.util.Constantes;
import pe.com.claro.controlesajustemasivo.util.PropertiesExterno;
import pe.com.claro.controlesajustemasivo.util.UtilEAI;

@Component
public class ControlAjusteMasivoBusiness {

	private static final Logger logger = Logger.getLogger(ControlAjusteMasivoBusiness.class);
	
	@Autowired
	private OacDao oacDao;
	
	@Autowired
	private SiopDao siopDao;
	
	@Autowired
	private IpcDBDao ipcdbDao;
	
	@Autowired
	private SgaDao sgaDao;
	
	@Autowired
	private EnvioCorreoSBSoap envioCorreoSBSoap;
	
	@Autowired
	private PropertiesExterno propertiesExterno;
	
	public void ejecutarProceso(String mensajeLog, String idTransaccion) {
		Date dateAhora = new Date();
		Date fechaProceso = UtilEAI.addDays(mensajeLog, dateAhora, Constantes._UNO);
		
		conciliarOacConSiop(mensajeLog, idTransaccion, fechaProceso);
		conciliarSgaConOac(mensajeLog, idTransaccion, fechaProceso);
	}

	private void conciliarOacConSiop(String mensajeLog, String idTransaccion, Date fechaProceso) /*throws DBException, ProcesoException */{
		try {
			logger.info(mensajeLog + "********** 1. Obtener ajustes de OAC para SIOP **********");
			AjustesIpcResponseBean ajustesIpcOacSiop = oacDao.consultarAjustesIpcSiop(mensajeLog, fechaProceso);
			
			if (ajustesIpcOacSiop.getCodigo() != Constantes.CERO || ajustesIpcOacSiop.getListaAjustes() == null 
					|| ajustesIpcOacSiop.getListaAjustes().isEmpty()) {
				logger.info(mensajeLog + "No se pudo obtener los ajustes de OAC para SIOP");
				String mensajeError = "No se pudo obtener los ajustes de OAC para SIOP" + (ajustesIpcOacSiop.getCodigo() == Constantes.CERO ? 
						Constantes.VACIO : " - " + ajustesIpcOacSiop.getMensaje());
				throw new ProcesoException(mensajeError);
			}
			
			List<RegAjteMasDetIpcRequestBean> listaDetalleIpc = new ArrayList<RegAjteMasDetIpcRequestBean>();
			AjustesIpcRequestBean ajustesIpcRequestBean;
			AjustesIpcResponseBean ajustesIpcSiop;
			RegAjteMasDetIpcRequestBean regDetalleIpc;
			int cantidadPositivaSiop = Constantes.CERO, cantidadNegativaSiop = Constantes.CERO;
			int cantidadPositivaOac = Constantes.CERO, cantidadNegativaOac = Constantes.CERO;
			double montoPositivoOac = Constantes.CERO, montoPositivoSiop = Constantes.CERO;
			double montoNegativoOac = Constantes.CERO, montoNegativoSiop = Constantes.CERO;
			String usuarioIpc = propertiesExterno.ORACLE_JDBC_IPCDB_USUARIO;
			for (AjusteIPCBean ajusteOAC : ajustesIpcOacSiop.getListaAjustes()) {
				logger.info(mensajeLog + "********** 2. Obtener ajustes de SIOP **********");
				ajustesIpcRequestBean = new AjustesIpcRequestBean();
				ajustesIpcRequestBean.setCustomerId(ajusteOAC.getCustomerId());
				ajustesIpcRequestBean.setNumeroAjuste(ajusteOAC.getNumeroAjuste());
				ajustesIpcSiop = siopDao.consultarAjustesIpc(mensajeLog, ajustesIpcRequestBean);
				
				logger.info(mensajeLog + "********** 3. Realizar conciliacion OAC y SIOP **********");
				if (ajustesIpcSiop.getCodigo() != Constantes.CERO) {
					logger.info(mensajeLog + "No se pudo encontrar el ajuste en SIOP");
					logger.info(mensajeLog + "customerId OAC: " + ajusteOAC.getCustomerId());
					logger.info(mensajeLog + "numeroAjuste OAC: " + ajusteOAC.getNumeroAjuste());
					
					regDetalleIpc = new RegAjteMasDetIpcRequestBean(ajusteOAC.getCustomerId(), ajusteOAC.getNumeroAjuste(), 
							ajusteOAC.getTipoMoneda(), ajusteOAC.getMonto(), usuarioIpc);
					listaDetalleIpc.add(regDetalleIpc);
				} else if (ajustesIpcSiop.getMonto() != ajusteOAC.getMonto() 
						|| !Optional.ofNullable(ajustesIpcSiop.getTipoMoneda()).equals(Optional.ofNullable(ajusteOAC.getTipoMoneda()))) {
					logger.info(mensajeLog + "Monto o moneda diferente entre OAC y SIOP");
					logger.info(mensajeLog + "monto OAC: " + ajusteOAC.getMonto() + " - monto SIOP: " + ajustesIpcSiop.getMonto());
					logger.info(mensajeLog + "moneda OAC: " + ajusteOAC.getTipoMoneda() + " - moneda SIOP: " + ajustesIpcSiop.getTipoMoneda());
					
					regDetalleIpc = new RegAjteMasDetIpcRequestBean(ajusteOAC.getCustomerId(), ajusteOAC.getNumeroAjuste(), 
							ajusteOAC.getTipoMoneda(), ajusteOAC.getMonto(), usuarioIpc);
					regDetalleIpc.setCustomerId(ajusteOAC.getCustomerId());
					regDetalleIpc.setNumeroAjuste(ajusteOAC.getNumeroAjuste());
					regDetalleIpc.setTipoMoneda(ajustesIpcSiop.getTipoMoneda());
					regDetalleIpc.setMonto(ajustesIpcSiop.getMonto());
					listaDetalleIpc.add(regDetalleIpc);
					
					if (ajustesIpcSiop.getMonto() >= Constantes.CERO) {
						cantidadPositivaSiop++;
						montoPositivoSiop = montoPositivoSiop + ajustesIpcSiop.getMonto();
					} else {
						cantidadNegativaSiop++;
						montoNegativoSiop = montoNegativoSiop + ajustesIpcSiop.getMonto();
					}
				} else {
					logger.info(mensajeLog + "Conciliacion exitosa entre OAC y SIOP");
					if (ajustesIpcSiop.getMonto() >= Constantes.CERO) {
						cantidadPositivaSiop++;
						montoPositivoSiop = montoPositivoSiop + ajustesIpcSiop.getMonto();
					} else {
						cantidadNegativaSiop++;
						montoNegativoSiop = montoNegativoSiop + ajustesIpcSiop.getMonto();
					}
				}
				
				if (ajusteOAC.getMonto() >= Constantes.CERO) {
					cantidadPositivaOac++;
					montoPositivoOac = montoPositivoOac + ajusteOAC.getMonto();
				} else {
					cantidadNegativaOac++;
					montoNegativoOac = montoNegativoOac + ajusteOAC.getMonto();
				}
			}
			
			logger.info(mensajeLog + "********** 4. Registrar transaccion OAC-SIOP en IPCDB **********");
			RegAjteMasIpcResponseBean regIpcResponseBean = registrarTransaccionIPCDB(mensajeLog, cantidadPositivaSiop, cantidadPositivaOac,
					montoPositivoSiop, montoPositivoOac, usuarioIpc, Constantes.APLICACION_SIOP, cantidadNegativaSiop, cantidadNegativaOac,
					montoNegativoSiop, montoNegativoOac);
			
			if (regIpcResponseBean.getCodigo() != Constantes.CERO) {
				logger.info(mensajeLog + "No se pudo registrar en IPCDB los datos de OAC y SIOP");
				String mensajeError = "No se pudo registrar en IPCDB los datos de OAC y SIOP" + (regIpcResponseBean.getCodigo() == Constantes.CERO ? 
						Constantes.VACIO : " - " + regIpcResponseBean.getMensaje());
				throw new ProcesoException(mensajeError);
			}
			
			if (!listaDetalleIpc.isEmpty()) {
				logger.info(mensajeLog + "********** 5. Registrar diferencias OAC-SIOP en IPCDB **********");
				registrarDiferenciasIPCDB(mensajeLog, listaDetalleIpc, regIpcResponseBean.getId());
			}
		} catch (DBException e) {
			logger.error(mensajeLog + "Ocurrio una excepcion [DBException] en la conciliacion de OAC con SIOP: " + e.getMessage(), e);
			String mensajeCorreo = propertiesExterno.WS_ENVIOCORREO_MENSAJE.replace(Constantes.REPLACE_TIPO_ERROR, 
					Constantes.TIPO_ERROR_BASE_DATOS).replace(Constantes.REPLACE_MENSAJE_ERROR, e.getCause().getMessage());
			enviarCorreo(mensajeLog, idTransaccion, fechaProceso, mensajeCorreo);
		} catch (ProcesoException e) {
			logger.error(mensajeLog + "Ocurrio una excepcion [ProcesoException] en la conciliacion de OAC con SIOP: " + e.getMessage(), e);
			String mensajeCorreo = propertiesExterno.WS_ENVIOCORREO_MENSAJE.replace(Constantes.REPLACE_TIPO_ERROR, 
					Constantes.TIPO_ERROR_FUNCIONAL).replace(Constantes.REPLACE_MENSAJE_ERROR, e.getMessage());
			enviarCorreo(mensajeLog, idTransaccion, fechaProceso, mensajeCorreo);
		} catch (Exception e) {
		    logger.error(mensajeLog + "Ocurrio una excepcion general en la conciliacion de OAC con SIOP: " + e.getMessage(), e);
		    String mensajeCorreo = propertiesExterno.WS_ENVIOCORREO_MENSAJE.replace(Constantes.REPLACE_TIPO_ERROR, 
					Constantes.TIPO_ERROR_INESPERADO).replace(Constantes.REPLACE_MENSAJE_ERROR, e.getMessage());
			enviarCorreo(mensajeLog, idTransaccion, fechaProceso, mensajeCorreo);
	    }
	}

	private void conciliarSgaConOac(String mensajeLog, String idTransaccion, Date fechaProceso) /*throws DBException, ProcesoException */{
		try {
			logger.info(mensajeLog + "********** 6. Obtener ajustes de SGA **********");
			AjustesIpcSgaResponseBean ajustesIpcSga = sgaDao.consultarAjustesIPC(mensajeLog, fechaProceso);
			
			if (ajustesIpcSga.getCodigo() != Constantes.CERO || ajustesIpcSga.getListaAjustes() == null 
					|| ajustesIpcSga.getListaAjustes().isEmpty()) {
				logger.info(mensajeLog + "No se pudo obtener los ajustes de SGA");
				String mensajeError = "No se pudo obtener los ajustes de SGA" + (ajustesIpcSga.getCodigo() == Constantes.CERO ? 
						Constantes.VACIO : " - " + ajustesIpcSga.getMensaje());
				throw new ProcesoException(mensajeError);
			}
			
			List<RegAjteMasDetIpcRequestBean> listaDetalleIpc = new ArrayList<RegAjteMasDetIpcRequestBean>();
			AjustesIpcRequestBean ajustesIpcRequestBean;
			AjustesIpcResponseBean ajustesIpcOacSga;
			RegAjteMasDetIpcRequestBean regDetalleIpc;
			int cantidadOacPos = Constantes.CERO, cantidadSgaPos = Constantes.CERO;
			int cantidadOacNeg = Constantes.CERO, cantidadSgaNeg = Constantes.CERO;
			double montoOacPos = Constantes.CERO, montoSgaPos = Constantes.CERO;
			double montoOacNeg = Constantes.CERO, montoSgaNeg = Constantes.CERO;
			String usuarioIpc = propertiesExterno.ORACLE_JDBC_IPCDB_USUARIO;
			for (AjusteIPCBean ajusteSga : ajustesIpcSga.getListaAjustes()) {
				logger.info(mensajeLog + "********** 7. Obtener ajustes de OAC para SGA **********");
				ajustesIpcRequestBean = new AjustesIpcRequestBean();
				ajustesIpcRequestBean.setTipoAjuste(ajusteSga.getTipoAjuste());
				ajustesIpcRequestBean.setNumeroAjuste(ajusteSga.getNumeroAjuste());
				ajustesIpcOacSga = oacDao.consultarAjustesIpcSga(mensajeLog, ajustesIpcRequestBean);
				
				logger.info(mensajeLog + "********** 8. Realizar conciliacion SGA y OAC **********");
				if (ajustesIpcOacSga.getCodigo() != Constantes.CERO) {
					logger.info(mensajeLog + "No se pudo encontrar el ajuste en OAC");
					logger.info(mensajeLog + "tipoAjuste SGA: " + ajusteSga.getTipoAjuste());
					logger.info(mensajeLog + "numeroAjuste SGA: " + ajusteSga.getNumeroAjuste());
					
					regDetalleIpc = new RegAjteMasDetIpcRequestBean(ajusteSga.getTipoAjuste(), ajusteSga.getCustomerId(), 
							ajusteSga.getNumeroAjuste(), ajusteSga.getTipoMoneda(), ajusteSga.getMonto(), usuarioIpc);
					listaDetalleIpc.add(regDetalleIpc);
				} else if (ajustesIpcOacSga.getMonto() != ajusteSga.getMonto() 
						|| !Optional.ofNullable(ajustesIpcOacSga.getTipoMoneda()).equals(Optional.ofNullable(ajusteSga.getTipoMoneda())) 
						|| !Optional.ofNullable(ajustesIpcOacSga.getCustomerId()).equals(Optional.ofNullable(ajusteSga.getCustomerId()))) {
					logger.info(mensajeLog + "Monto, moneda o customerId diferente entre SGA y OAC");
					logger.info(mensajeLog + "monto SGA: " + ajusteSga.getMonto() + " - monto OAC: " + ajustesIpcOacSga.getMonto());
					logger.info(mensajeLog + "moneda SGA: " + ajusteSga.getTipoMoneda() + " - moneda OAC: " + ajustesIpcOacSga.getTipoMoneda());
					logger.info(mensajeLog + "customerId SGA: " + ajusteSga.getCustomerId() + " - customerId OAC: " + ajustesIpcOacSga.getCustomerId());
					
					regDetalleIpc = new RegAjteMasDetIpcRequestBean(ajusteSga.getTipoAjuste(), ajusteSga.getCustomerId(), 
							ajusteSga.getNumeroAjuste(), ajusteSga.getTipoMoneda(), ajusteSga.getMonto(), usuarioIpc);
					regDetalleIpc.setTipoAjusteOAC(ajusteSga.getTipoAjuste());
					regDetalleIpc.setCustomerIdOAC(ajustesIpcOacSga.getCustomerId());
					regDetalleIpc.setNumeroAjusteOAC(ajusteSga.getNumeroAjuste());
					regDetalleIpc.setTipoMonedaOAC(ajustesIpcOacSga.getTipoMoneda());
					regDetalleIpc.setMontoOAC(ajustesIpcOacSga.getMonto());
					listaDetalleIpc.add(regDetalleIpc);
					
					if (ajustesIpcOacSga.getMonto() >= Constantes.CERO) {
						cantidadOacPos++;
						montoOacPos = montoOacPos + ajustesIpcOacSga.getMonto();
					} else {
						cantidadOacNeg++;
						montoOacNeg = montoOacNeg + ajustesIpcOacSga.getMonto();
					}
				} else {
					logger.info(mensajeLog + "Conciliacion exitosa entre SGA y OAC");
					if (ajustesIpcOacSga.getMonto() >= Constantes.CERO) {
						cantidadOacPos++;
						montoOacPos = montoOacPos + ajustesIpcOacSga.getMonto();
					} else {
						cantidadOacNeg++;
						montoOacNeg = montoOacNeg + ajustesIpcOacSga.getMonto();
					}
				}
				
				if (ajusteSga.getMonto() >= Constantes.CERO) {
					cantidadSgaPos++;
					montoSgaPos = montoSgaPos + ajusteSga.getMonto();
				} else {
					cantidadSgaNeg++;
					montoSgaNeg = montoSgaNeg + ajusteSga.getMonto();
				}
			}
			
			logger.info(mensajeLog + "********** 9. Registrar transaccion SGA-OAC en IPCDB **********");
			RegAjteMasIpcResponseBean regIpcResponseBean = registrarTransaccionIPCDB(mensajeLog, cantidadSgaPos, cantidadOacPos, 
					montoSgaPos, montoOacPos, usuarioIpc, Constantes.APLICACION_SGA, cantidadSgaNeg, cantidadOacNeg, montoSgaNeg, 
					montoOacNeg);
			
			if (regIpcResponseBean.getCodigo() != Constantes.CERO) {
				logger.info(mensajeLog + "No se pudo registrar en IPCDB los datos de SGA y OAC");
				String mensajeError = "No se pudo registrar en IPCDB los datos de SGA y OAC" + (regIpcResponseBean.getCodigo() == Constantes.CERO ? 
						Constantes.VACIO : " - " + regIpcResponseBean.getMensaje());
				throw new ProcesoException(mensajeError);
			}
			
			if (!listaDetalleIpc.isEmpty()) {
				logger.info(mensajeLog + "********** 10. Registrar diferencias SGA-OAC en IPCDB **********");
				registrarDiferenciasIPCDB(mensajeLog, listaDetalleIpc, regIpcResponseBean.getId());
			}
		} catch (DBException e) {
			logger.error(mensajeLog + "Ocurrio una excepcion [DBException] en la conciliacion de OAC con SGA: " + e.getMessage(), e);
			String mensajeCorreo = propertiesExterno.WS_ENVIOCORREO_MENSAJE.replace(Constantes.REPLACE_TIPO_ERROR, 
					Constantes.TIPO_ERROR_BASE_DATOS).replace(Constantes.REPLACE_MENSAJE_ERROR, e.getCause().getMessage());
			enviarCorreo(mensajeLog, idTransaccion, fechaProceso, mensajeCorreo);
		} catch (ProcesoException e) {
			logger.error(mensajeLog + "Ocurrio una excepcion [ProcesoException] en la conciliacion de OAC con SGA: " + e.getMessage(), e);
			String mensajeCorreo = propertiesExterno.WS_ENVIOCORREO_MENSAJE.replace(Constantes.REPLACE_TIPO_ERROR, 
					Constantes.TIPO_ERROR_FUNCIONAL).replace(Constantes.REPLACE_MENSAJE_ERROR, e.getMessage());
			enviarCorreo(mensajeLog, idTransaccion, fechaProceso, mensajeCorreo);
		} catch (Exception e) {
		    logger.error(mensajeLog + "Ocurrio una excepcion general en la conciliacion de OAC con SGA: " + e.getMessage(), e);
		    String mensajeCorreo = propertiesExterno.WS_ENVIOCORREO_MENSAJE.replace(Constantes.REPLACE_TIPO_ERROR, 
					Constantes.TIPO_ERROR_INESPERADO).replace(Constantes.REPLACE_MENSAJE_ERROR, e.getMessage());
			enviarCorreo(mensajeLog, idTransaccion, fechaProceso, mensajeCorreo);
	    }
	}

	private RegAjteMasIpcResponseBean registrarTransaccionIPCDB(String mensajeLog, int cantidadOrigenPos, int cantidadOacPos, 
			double montoOrigenPos, double montoOacPos, String usuarioIpc, String aplicacion, int cantidadOrigenNeg, 
			int cantidadOacNeg, double montoOrigenNeg, double montoOacNeg) throws DBException {
		RegAjteMasIpcRequestBean regIpcRequestBean = new RegAjteMasIpcRequestBean();
		regIpcRequestBean.setCantidadOrigenPos(cantidadOrigenPos);
		regIpcRequestBean.setCantidadOacPos(cantidadOacPos);
		regIpcRequestBean.setMontoOrigenPos(montoOrigenPos);
		regIpcRequestBean.setMontoOacPos(montoOacPos);
		regIpcRequestBean.setUsuario(usuarioIpc);
		regIpcRequestBean.setAplicacion(aplicacion);
		regIpcRequestBean.setCantidadOrigenNeg(cantidadOrigenNeg);
		regIpcRequestBean.setCantidadOacNeg(cantidadOacNeg);
		regIpcRequestBean.setMontoOrigenNeg(montoOrigenNeg);
		regIpcRequestBean.setMontoOacNeg(montoOacNeg);
		return ipcdbDao.registrarAjusteMasivo(mensajeLog, regIpcRequestBean);
	}

	private void registrarDiferenciasIPCDB(String mensajeLog, List<RegAjteMasDetIpcRequestBean> listaDetalleIpc,
			Integer idCab) throws DBException, ProcesoException {
		ResponseBean responseBean;
		for (RegAjteMasDetIpcRequestBean detalleIpc : listaDetalleIpc) {
			detalleIpc.setIdCab(idCab);
			responseBean = ipcdbDao.registrarAjusteMasivoDetalle(mensajeLog, detalleIpc);
			
			if (responseBean.getCodigo() != Constantes.CERO) {
				logger.info(mensajeLog + "Ocurrio un error en IPCDB al registrar diferencias");
				String mensajeError = "Ocurrio un error en IPCDB al registrar diferencias" + (responseBean.getCodigo() == Constantes.CERO ? 
						Constantes.VACIO : " - " + responseBean.getMensaje());
				throw new ProcesoException(mensajeError);
			}
		}
	}
	
	private void enviarCorreo(String mensajeLog, String idTransaccion, Date fechaProceso, String mensajeCorreo) {
		logger.info(mensajeLog + "********** 11. Enviar correo con detalle de error **********");
		EnvioCorreoRequest requestCorreo = new EnvioCorreoRequest();
		requestCorreo.setDestinatario(propertiesExterno.WS_ENVIOCORREO_DESTINATARIO);
     	requestCorreo.setRemitente(propertiesExterno.WS_ENVIOCORREO_REMITENTE);
     	requestCorreo.setAsunto(propertiesExterno.WS_ENVIOCORREO_ASUNTO.replace(Constantes.REPLACE_FECHA, 
     			UtilEAI.formatDate(mensajeLog, fechaProceso, Constantes.FORMATO_FECHA_DD_MM_YYYY)));
     	requestCorreo.setHtmlFlag(propertiesExterno.WS_ENVIOCORREO_HTMLFLAG);
     	
     	AuditoriaRequest auditoriaCorreo = new AuditoriaRequest();
     	auditoriaCorreo.setIdTransaccion(idTransaccion);
    	auditoriaCorreo.setIpAplicacion(UtilEAI.obtenerIP(mensajeLog));
    	auditoriaCorreo.setUsuarioAplicacion(propertiesExterno.WS_PARAM_AUDITORIA_USUARIOAPLICACION);
    	auditoriaCorreo.setNombreAplicacion(propertiesExterno.WS_PARAM_AUDITORIA_NOMBREAPLICACION);
    	requestCorreo.setAuditoriaRequest(auditoriaCorreo);
    	
    	requestCorreo.setMensaje(mensajeCorreo);
    	envioCorreoSBSoap.enviarCorreo(mensajeLog, requestCorreo);
	}
	
}
