package pe.com.claro.controlesajustemasivo.service.type;

public class EnvioCorreoResponse {

	private AuditoriaResponse auditoriaResponse;

	public AuditoriaResponse getAuditoriaResponse() {
		return auditoriaResponse;
	}

	public void setAuditoriaResponse(AuditoriaResponse auditoriaResponse) {
		this.auditoriaResponse = auditoriaResponse;
	}
	
}
