package pe.com.claro.controlesajustemasivo.service;

import pe.com.claro.controlesajustemasivo.service.type.EnvioCorreoRequest;
import pe.com.claro.controlesajustemasivo.service.type.EnvioCorreoResponse;

/**
 * @author DMB
 *
 */
public interface EnvioCorreoSBSoap {
	
	EnvioCorreoResponse enviarCorreo(String mensajeLog, EnvioCorreoRequest request);
	
}
