package pe.com.claro.controlesajustemasivo.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import pe.com.claro.controlesajustemasivo.service.type.ArchivoAdjuntoType;
import pe.com.claro.controlesajustemasivo.service.type.AuditoriaResponse;
import pe.com.claro.controlesajustemasivo.service.type.EnvioCorreoRequest;
import pe.com.claro.controlesajustemasivo.service.type.EnvioCorreoResponse;
import pe.com.claro.controlesajustemasivo.util.JAXBUtilitarios;
import pe.com.claro.controlesajustemasivo.util.PropertiesExterno;
import pe.com.claro.eai.util.enviocorreo.EnvioCorreoSBPortType;
import pe.com.claro.eai.util.enviocorreo.types.ArchivoAdjunto;
import pe.com.claro.eai.util.enviocorreo.types.AuditTypeRequest;
import pe.com.claro.eai.util.enviocorreo.types.EnviarCorreoRequest;
import pe.com.claro.eai.util.enviocorreo.types.EnviarCorreoResponse;
import pe.com.claro.eai.util.enviocorreo.types.ListaArchivosAdjuntos;

@Service
public class EnvioCorreoSBSoapImpl implements EnvioCorreoSBSoap {

	private static final Logger LOGGER = Logger.getLogger(EnvioCorreoSBSoapImpl.class);
	
	@Autowired
	@Qualifier( value="envioCorreoSBPort" )
	private EnvioCorreoSBPortType envioCorreoSBPort;
	
	@Autowired
	private PropertiesExterno propertiesExterno;
	
	@Override
	public EnvioCorreoResponse enviarCorreo(String mensajeLog, EnvioCorreoRequest request) {
		long tiempoInicio = System.currentTimeMillis();
		String nombreMetodo = "enviarCorreo";
		LOGGER.info( mensajeLog + "[INICIO] - METODO: " + nombreMetodo);
		EnvioCorreoResponse response = new EnvioCorreoResponse();
		
		try {
			AuditTypeRequest auditoriaRequest = new AuditTypeRequest();
			auditoriaRequest.setIdTransaccion(request.getAuditoriaRequest().getIdTransaccion());
			auditoriaRequest.setIpAplicacion(request.getAuditoriaRequest().getIpAplicacion());
			auditoriaRequest.setCodigoAplicacion(request.getAuditoriaRequest().getNombreAplicacion());
			auditoriaRequest.setUsrAplicacion(request.getAuditoriaRequest().getUsuarioAplicacion());
			
			EnviarCorreoRequest requestWS = new EnviarCorreoRequest();
			requestWS.setAuditRequest(auditoriaRequest);
			requestWS.setRemitente(request.getRemitente());
			requestWS.setDestinatario(request.getDestinatario());
			requestWS.setAsunto(request.getAsunto());
			requestWS.setMensaje(request.getMensaje());
			requestWS.setHtmlFlag(request.getHtmlFlag());
			
			if (request.getListaArchivosAdjuntos() != null && !request.getListaArchivosAdjuntos().isEmpty()) {
				ListaArchivosAdjuntos lstArchivos = new ListaArchivosAdjuntos();
				ArchivoAdjunto archivo;
				for (ArchivoAdjuntoType archivoAdjunto : request.getListaArchivosAdjuntos()) {
					archivo = new ArchivoAdjunto();
					archivo.setNombre(archivoAdjunto.getNombre());
					archivo.setCabecera(archivoAdjunto.getCabecera());
					archivo.setArchivo(archivoAdjunto.getArchivo());
					lstArchivos.getArchivoAdjunto().add(archivo);
				}
				requestWS.setListaArchivosAdjuntos(lstArchivos);
			}
			
			LOGGER.info( mensajeLog + "Se invoca WS '" + propertiesExterno.WS_ENVIOCORREO_ENDPOINTADDRESS + "' - metodo '" + nombreMetodo );
			LOGGER.info(mensajeLog + "Tiempo de Timeout de Conexion: " + propertiesExterno.WS_ENVIOCORREO_MAX_TIMEOUT_CONEXION);
			LOGGER.info(mensajeLog + "Tiempo de Timeout de Ejecucion: " + propertiesExterno.WS_ENVIOCORREO_MAX_TIMEOUT_REQUEST);
			LOGGER.info(mensajeLog + "Datos de entrada:" + JAXBUtilitarios.anyObjectToXmlText(requestWS));
			
			EnviarCorreoResponse responseWS = envioCorreoSBPort.enviarCorreo(requestWS);			
			LOGGER.info( mensajeLog + "[SE EJECUTO WS " + propertiesExterno.WS_ENVIOCORREO_ENDPOINTADDRESS + "] - METODO: " + nombreMetodo);
			LOGGER.info( mensajeLog + "RESPONSE: " + JAXBUtilitarios.anyObjectToXmlText( responseWS ));
			
			AuditoriaResponse auditoriaResponse = new AuditoriaResponse();
			auditoriaResponse.setIdTransaccion(responseWS.getAuditResponse().getIdTransaccion());
			auditoriaResponse.setCodigoRespuesta(responseWS.getAuditResponse().getCodigoRespuesta());
			auditoriaResponse.setMensajeRespuesta(responseWS.getAuditResponse().getMensajeRespuesta());
			
			response.setAuditoriaResponse(auditoriaResponse);
		} catch (Exception e) {
			LOGGER.error( mensajeLog + "Error WSException invocando WS '" + propertiesExterno.WS_ENVIOCORREO_ENDPOINTADDRESS + "' - metodo '" + nombreMetodo + "'", e );
		} finally {
			LOGGER.info( mensajeLog + "Tiempo total de proceso(ms): " + (System.currentTimeMillis() - tiempoInicio) );
			LOGGER.info( mensajeLog + "[FIN] - METODO: " + nombreMetodo );
		}
		
		return response;
	}

}
