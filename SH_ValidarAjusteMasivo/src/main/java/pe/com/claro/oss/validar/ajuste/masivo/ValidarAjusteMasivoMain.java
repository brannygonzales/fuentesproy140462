package pe.com.claro.oss.validar.ajuste.masivo;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import pe.com.claro.oss.validar.ajuste.masivo.service.ValidarAjusteMasivoService;
import pe.com.claro.oss.validar.ajuste.masivo.util.Constantes;
import pe.com.claro.oss.validar.ajuste.masivo.util.PropertiesExternos;


@Component("ValidarAjusteMasivoMain")
public class ValidarAjusteMasivoMain {
	
	@Autowired
	private PropertiesExternos propertiesExterno;
	@Autowired
	private ValidarAjusteMasivoService validarAjusteMasivoService;
	private static Logger logger = Logger
			.getLogger(ValidarAjusteMasivoMain.class);
	private static ApplicationContext objContextoSpring;

	public static void main(String[] args) {
		long startTime = System.currentTimeMillis();
		String idTransaction = args[0];
		String mensajeTransaccion = "[ValidarAjusteMasivoMain idTx="
				+ idTransaction + "] ";
		try {
			objContextoSpring = new ClassPathXmlApplicationContext(
					Constantes.URL_CONTEXT);
			ValidarAjusteMasivoMain main = objContextoSpring
					.getBean(ValidarAjusteMasivoMain.class);
			main.iniciarProceso( idTransaction);
		} catch (NoSuchBeanDefinitionException e) {
			logger.error("ERROR [NoSuchBeanDefinitionException] - : ", e);
			throw new BeanCreationException(
					"Error NoSuchBeanDefinitionException" + e.getStackTrace());
		} finally {
			logger.info(mensajeTransaccion+" [FIN]   - Actividad 7" );
			try {
				logger.info(mensajeTransaccion+ "[Fin de metodo main] Tiempo total de proceso(ms): " + (System.currentTimeMillis() - startTime) + " milisegundos.");
			} catch (Exception e) {
				  logger.error(mensajeTransaccion+"Error al cerrar Contexto: "+ e +".");
			}
		}

	}
	
	public void iniciarProceso(String idTransaccion) {

		String mensajeTx =  "[launch idTx=" + idTransaccion + " ]";
		try {
			PropertyConfigurator.configure(propertiesExterno.vLog4JDir);
			validarAjusteMasivoService.run(idTransaccion);
		} catch (Exception e) {
			logger.error("ERROR [Exception] - : ", e);
			throw new BeanCreationException("Error BeanCreationException"
					+ e.getCause());
		}finally {
			logger.info(mensajeTx + "terminarProceso");
			logger.info(mensajeTx
					+ "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

		}
	}

}
