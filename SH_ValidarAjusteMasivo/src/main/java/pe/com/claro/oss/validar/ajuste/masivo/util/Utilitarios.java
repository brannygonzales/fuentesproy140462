package pe.com.claro.oss.validar.ajuste.masivo.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.log4j.Logger;

public class Utilitarios {

	private static Logger logger = Logger.getLogger(Utilitarios.class);

	public Utilitarios() {
		super();
	}

	public static String getStackTraceFromException(Exception exception) {
		StringWriter stringWriter = new StringWriter();
		exception.printStackTrace(new PrintWriter(stringWriter, true));
		return stringWriter.toString();
	}

	public static String formatearFecha(Date fechaDate, SimpleDateFormat formatter) {
		String fecha = "";
		try {

			if (fechaDate != null && formatter != null) {
				fecha = formatter.format(fechaDate);
			}
		} catch (Exception e) {
			logger.error("Error al formatear fecha.", e);
		}
		return fecha;
	}

	public static String formatearFecha(Date fechaDate, String formato) {
		String fecha = "";
		try {

			if (fechaDate != null && formato != null) {
				SimpleDateFormat sdf = new SimpleDateFormat(formato, Locale.getDefault());
				fecha = sdf.format(fechaDate);
			}
		} catch (Exception e) {
			logger.error("Error al formatear fecha.", e);
		}
		return fecha;
	}

	public static boolean eliminarFichero(String mensajeTransaccion, File fichero) {
		boolean exito = false;
		String metodo = "eliminarFichero";
		String cadMensaje = mensajeTransaccion + "[" + metodo + "] ";

		logger.info(cadMensaje + Utilitarios.msjInicioProceso(metodo, Constantes.UTILITARIOS));

		Long ini = 0L;
		Long end = 0L;

		Long duracion = 0L;

		ini = System.currentTimeMillis();

		try {

			if (!fichero.exists()) {
				exito = false;
				logger.info(cadMensaje + "El archivo data no existe.");
			} else {
				fichero.delete();
				exito = true;
				logger.info(cadMensaje + "El archivo data fue eliminado.");

			}
		} catch (Exception e) {
			exito = false;
			mensajeExceptionFile(cadMensaje, e);
		} finally {
			end = System.currentTimeMillis();
			duracion = end - ini;
			logger.info(cadMensaje + Constantes.MENSAJE_TIEMPO_DURACION.replace(Constantes.CADENAREPLACEDURACION,
					String.valueOf(duracion)));
			logger.info(cadMensaje + Utilitarios.msjFinProceso(metodo, Constantes.UTILITARIOS));
		}

		return exito;

	}

	public static boolean desComprimirFichero(String mensajeTransaccion, String filePath, String fileZIP) {
		boolean exito = false;
		String metodo = "desComprimirFichero";
		String cadMensaje = mensajeTransaccion + "[" + metodo + "] ";
		logger.info(cadMensaje + Utilitarios.msjInicioProceso(metodo, Constantes.UTILITARIOS));

		Long ini = 0L;
		Long end = 0L;
		Long duracion = 0L;

		ini = System.currentTimeMillis();
		try {

			String scriptEjecutar;

			scriptEjecutar = Constantes.COMANDO_DESCOMPRIMIR_ARCHIVO + Constantes.ESPACIO_BLANCO + filePath + fileZIP
					+ Constantes.ESPACIO_BLANCO + Constantes.COMANDO_DEPOSITAR_ARCHIVO + filePath;

			logger.info(cadMensaje + "Comando a ejecutarse:" + scriptEjecutar);

			Runtime.getRuntime().exec(scriptEjecutar);
			exito = true;
		} catch (IOException e) {
			exito = false;
			mensajeExceptionFile(cadMensaje, e);

		} finally {
			end = System.currentTimeMillis();

			duracion = end - ini;
			logger.info(cadMensaje + Constantes.MENSAJE_TIEMPO_DURACION.replace(Constantes.CADENAREPLACEDURACION,
					String.valueOf(duracion)));

			logger.info(cadMensaje + Utilitarios.msjFinProceso(metodo, Constantes.UTILITARIOS));
		}

		return exito;
	}

	public static boolean eliminarContenidoFolder(String mensajeTransaccion, File folder) {

		boolean exito = false;
		String metodo = "eliminarContenidoFolder";
		String cadMensaje = mensajeTransaccion + "[" + metodo + "] ";
		logger.info(cadMensaje + Utilitarios.msjInicioProceso(metodo, Constantes.UTILITARIOS));

		Long ini = 0L;
		Long end = 0L;

		Long duracion = 0L;

		ini = System.currentTimeMillis();

		try {
			String[] entries = folder.list();

			if (!folder.exists()) {
				exito = false;
				logger.info(cadMensaje + "La ruta no existe.");
			} else {
				for (String s : entries) {
					File currentFile = new File(folder.getPath(), s);
					currentFile.delete();
					logger.info(cadMensaje + "El archivo:" + currentFile.getName() + " data fue eliminado.");
				}
				exito = true;

			}

		} catch (Exception e) {
			mensajeExceptionFile(cadMensaje, e);
		} finally {
			end = System.currentTimeMillis();

			duracion = end - ini;
			logger.info(cadMensaje + Constantes.MENSAJE_TIEMPO_DURACION.replace(Constantes.CADENAREPLACEDURACION,
					String.valueOf(duracion)));

			logger.info(cadMensaje + Utilitarios.msjFinProceso(metodo, Constantes.UTILITARIOS));
		}

		return exito;
	}

	public static String msjInicioProceso(String metodo, String ambito) {
		return Constantes.INICIO + " -  METODO: [" + metodo + " - " + ambito + "] ";
	}

	public static String msjFinProceso(String metodo, String ambito) {
		return Constantes.FIN + " -  METODO: [" + metodo + " - " + ambito + "] ";
	}

	public static void msjCargaCTL(String mensajeTransaccion, int codigoProceso) {
		if (codigoProceso == 0) {
			logger.info(mensajeTransaccion + "Carga ha sido completada");
		} else if (codigoProceso == 1) {
			logger.error(mensajeTransaccion + "Ha sucedido un problema - FAILED");
		} else if (codigoProceso == 2) {
			logger.warn(mensajeTransaccion + "Ha sucedido un problema - WARN");
		} else if (codigoProceso == 3) {
			logger.fatal(mensajeTransaccion + "Ha sucedido un problema - FATAL");
		} else {
			logger.error(mensajeTransaccion + "Ha sucedido un problema");
		}

	}

	public static void infoConexion(String mensajeTransaccion, String jndi, String bd, String owner, String pkg,
			String sp) {
		logger.info(mensajeTransaccion + Constantes.JNDI.replace(Constantes.VALOR_REEMPLAZO, jndi));

		logger.info(mensajeTransaccion + Constantes.BD.replace(Constantes.VALOR_REEMPLAZO, bd));

		logger.info(mensajeTransaccion + Constantes.OWNER.replace(Constantes.VALOR_REEMPLAZO, owner));

		logger.info(mensajeTransaccion + Constantes.PACKAGE.replace(Constantes.VALOR_REEMPLAZO, pkg));

		logger.info(mensajeTransaccion + Constantes.PROCEDURE.replace(Constantes.VALOR_REEMPLAZO, sp));

	}

	static void mensajeExceptionFile(String mensajeTransaccion, Exception e) {
		String textoError = Utilitarios.getStackTraceFromException(e);
		logger.error(mensajeTransaccion + Constantes.METODO_MENSAJE_EXCEPTION + textoError, e);

		logger.error(mensajeTransaccion + Constantes.ESPACIO_BLANCO + e.getMessage(), e);
	}

}
