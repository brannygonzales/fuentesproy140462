package pe.com.claro.oss.validar.ajuste.masivo.bean;

import java.util.List;

public class ListarCabeceraAjusteBean {
	
	private String idProceso;
	private List<AjusteCabeceraBean> listaAjusteCabecera;
	private String flagDet;
	private String codigoRespuesta;
	private String mensajeRespuesta;
	
	public String getIdProceso() {
		return idProceso;
	}
	public void setIdProceso(String idProceso) {
		this.idProceso = idProceso;
	}
	public List<AjusteCabeceraBean> getListaAjusteCabecera() {
		return listaAjusteCabecera;
	}
	public void setListaAjusteCabecera(List<AjusteCabeceraBean> listaAjusteCabecera) {
		this.listaAjusteCabecera = listaAjusteCabecera;
	}
	public String getFlagDet() {
		return flagDet;
	}
	public void setFlagDet(String flagDet) {
		this.flagDet = flagDet;
	}
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
	public String getMensajeRespuesta() {
		return mensajeRespuesta;
	}
	public void setMensajeRespuesta(String mensajeRespuesta) {
		this.mensajeRespuesta = mensajeRespuesta;
	}
	
	

}
