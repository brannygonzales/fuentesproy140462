package pe.com.claro.oss.validar.ajuste.masivo.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;
import oracle.sql.ROWID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import pe.com.claro.oss.validar.ajuste.masivo.bean.ActualizaEstadoBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ActualizaMontoCabBscsBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ActualizaMtoCabBscsBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ActualizaProcesoTmpBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ActualizarMontoDetalleBSCSBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.AjusteCabeceraBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.DatosAjusteSiop;
import pe.com.claro.oss.validar.ajuste.masivo.bean.DetalleArchPlanoBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.DetalleTemporalBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ListaDatosAjusteSiop;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ListaDetalleTemporalBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ListaProcesoExcelBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ListarCabeceraAjusteBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ListarDetalleArchPlanoBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ObtieneRubroBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ProcesoExcelBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ProcesoTmpBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ResponseBase;
import pe.com.claro.oss.validar.ajuste.masivo.bean.RubroBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.TmpAmDetBean;
import pe.com.claro.oss.validar.ajuste.masivo.exception.DBException;
import pe.com.claro.oss.validar.ajuste.masivo.util.Constantes;
import pe.com.claro.oss.validar.ajuste.masivo.util.JAXBUtilitarios;
import pe.com.claro.oss.validar.ajuste.masivo.util.PropertiesExternos;
import pe.com.claro.oss.validar.ajuste.masivo.util.Utilitarios;

@Repository
public class SiopDAOImpl implements ISiopDAO{
	
	private static Logger logger = Logger.getLogger(SiopDAOImpl.class);
	
	@Autowired
	private PropertiesExternos propertiesExterno;
	
	@Autowired
	@Qualifier("siopDS")
	private DataSource siopDS;
	
	
	SimpleJdbcCall objJdbcCall = null;

	JdbcTemplate objJdbcTemplate = null;
	
	@Override
	public ListaProcesoExcelBean obtenerProcesoExcel(String mensajeTransaccion) throws DBException {
		
		String metodoListarProceso = "obtenerProcesoExcel";

		String cadMensaje = mensajeTransaccion + "[" + metodoListarProceso + "] ";

		logger.info(cadMensaje + Utilitarios.msjInicioProceso(metodoListarProceso, Constantes.DAO));

		Utilitarios.infoConexion(mensajeTransaccion, propertiesExterno.dbSiopdbConexion, propertiesExterno.dbSiopdbNombre,
				propertiesExterno.dbSiopdbOwner, propertiesExterno.dbSiopPkgAjusteMasivo,
				propertiesExterno.dbSiopListarProcesosExcel);
		ListaProcesoExcelBean response = null;
		
		try {
			logger.info(
					mensajeTransaccion + "Intento [" + metodoListarProceso + "] : ");
			response = new ListaProcesoExcelBean();
			objJdbcCall = new SimpleJdbcCall(siopDS);
			objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			objJdbcTemplate.setQueryTimeout(propertiesExterno.DB_SIOP_CONNECTION_TIMEOUT);
			
			SqlOutParameter outCodigoRespuesta = new SqlOutParameter(Constantes.CODIGORESPUESTA,
					OracleTypes.VARCHAR);
			SqlOutParameter outMensajeRespuesta = new SqlOutParameter(Constantes.MENSAJERESPUESTA,
					OracleTypes.VARCHAR);
			
			SqlOutParameter outCursorProcesos= new SqlOutParameter(propertiesExterno.dbSiopListaProcesosExcelCursor,
					OracleTypes.CURSOR,	new RowMapper<ProcesoExcelBean>() {
						public ProcesoExcelBean mapRow(ResultSet rs, int arg1) throws SQLException {
							ProcesoExcelBean procesoBean = new ProcesoExcelBean();
							procesoBean.setIdProceso(rs.getString(propertiesExterno.dbSiopListaProcesosExcelColumnaIdProceso)==null?Constantes.VACIO: rs.getString(propertiesExterno.dbSiopListaProcesosExcelColumnaIdProceso));
							return procesoBean;
							}
						});
			
			SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess()
					.withSchemaName(propertiesExterno.dbSiopdbOwner)
					.withCatalogName(propertiesExterno.dbSiopPkgAjusteMasivo)
					.withProcedureName(propertiesExterno.dbSiopListarProcesosExcel)
					.declareParameters(outCursorProcesos, outCodigoRespuesta, outMensajeRespuesta);
			
			logger.info(cadMensaje + Constantes.MENSAJE_EJECUCION_SP + "[" + propertiesExterno.dbSiopdbOwner + "."
					+ propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopListarProcesosExcel + "]");
			
			MapSqlParameterSource map = new MapSqlParameterSource();

			Map<String, Object> result = procedureConsulta.execute(map);
			@SuppressWarnings("unchecked")
			List<ProcesoExcelBean> listaProcesos = (List<ProcesoExcelBean>) result.get(propertiesExterno.dbSiopListaProcesosExcelCursor);

			String poCodRpta = (String) result.get(Constantes.CODIGORESPUESTA);

			String poMsjRpta = (String) result.get(Constantes.MENSAJERESPUESTA);
			
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_COD_RPTA]:" + poCodRpta);
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_MSJ_RPTA]:" + poMsjRpta);

			response.setListaProcesos(listaProcesos);
			response.setCodigoRespuesta(poCodRpta);
			response.setMensajeRespuesta(poMsjRpta);

		} catch (Exception e) {
			String codigoError = "";
			codigoError = propertiesExterno.spIdt1Codigo;
			String msgError = "Hubo un error tecnico al ejecutar el PROCEDURE: ["
					+ propertiesExterno.dbSiopdbOwner + "." + propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopListarProcesosExcel + "] ";
			response.setCodigoRespuesta(codigoError);
			response.setMensajeRespuesta(msgError);
			logger.error(mensajeTransaccion + e.getMessage(), e);
			logger.error(mensajeTransaccion + msgError);
			throw new DBException(codigoError,msgError, e);	
		}finally {
			logger.info(cadMensaje + Utilitarios.msjFinProceso(metodoListarProceso, Constantes.DAO));
		}
		
		return response;
	}

	@Override
	public ActualizaProcesoTmpBean actualizarProcesoTmp(ActualizaProcesoTmpBean actualizaProcesoBean,
			String mensajeTransaccion) throws DBException {
		String metodoActualizaProceso = "actualizarProcesoTmp";

		String cadMensaje = mensajeTransaccion + "[" + metodoActualizaProceso + "] ";

		logger.info(cadMensaje + Utilitarios.msjInicioProceso(metodoActualizaProceso, Constantes.DAO));

		Utilitarios.infoConexion(mensajeTransaccion, propertiesExterno.dbSiopdbConexion, propertiesExterno.dbSiopdbNombre,
				propertiesExterno.dbSiopdbOwner, propertiesExterno.dbSiopPkgAjusteMasivo,
				propertiesExterno.dbSiopActualizaProcesoTmp);
		ActualizaProcesoTmpBean response = null;
		
		try {
			logger.info(
					mensajeTransaccion + "Intento [" + metodoActualizaProceso + "] : ");
			response = new ActualizaProcesoTmpBean();
			objJdbcCall = new SimpleJdbcCall(siopDS);
			objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			objJdbcTemplate.setQueryTimeout(propertiesExterno.DB_SIOP_CONNECTION_TIMEOUT);
			
			SqlParameter inIdProceso = new SqlParameter(propertiesExterno.dbSiopActualizaProcesoTmpInputIdProceso, OracleTypes.NUMBER);
			
			SqlOutParameter outCodigoRespuesta = new SqlOutParameter(Constantes.CODIGORESPUESTA,
					OracleTypes.VARCHAR);
			SqlOutParameter outMensajeRespuesta = new SqlOutParameter(Constantes.MENSAJERESPUESTA,
					OracleTypes.VARCHAR);
			
			SqlOutParameter outCursorProcesos= new SqlOutParameter(propertiesExterno.dbSiopActualizaProcesoTmpCursor,
					OracleTypes.CURSOR,	new RowMapper<ProcesoTmpBean>() {
						public ProcesoTmpBean mapRow(ResultSet rs, int arg1) throws SQLException {
							ProcesoTmpBean procesoBean = new ProcesoTmpBean();
							procesoBean.setIdProceso(rs.getString(propertiesExterno.dbSiopActualizaProcesoTmpColumnaProceso)==null?Constantes.VACIO: rs.getString(propertiesExterno.dbSiopActualizaProcesoTmpColumnaProceso));
							procesoBean.setCustId(rs.getString(propertiesExterno.dbSiopActualizaProcesoTmpColumnaCustId)==null?Constantes.VACIO: rs.getString(propertiesExterno.dbSiopActualizaProcesoTmpColumnaCustId));
							procesoBean.setEvento(rs.getString(propertiesExterno.dbSiopActualizaProcesoTmpColumnaEvento)==null?Constantes.VACIO: rs.getString(propertiesExterno.dbSiopActualizaProcesoTmpColumnaEvento));
							procesoBean.setRecibo(rs.getString(propertiesExterno.dbSiopActualizaProcesoTmpColumnaRecibo)==null?Constantes.VACIO: rs.getString(propertiesExterno.dbSiopActualizaProcesoTmpColumnaRecibo));
							procesoBean.setServicio(rs.getString(propertiesExterno.dbSiopActualizaProcesoTmpColumnaServicio)==null?Constantes.VACIO: rs.getString(propertiesExterno.dbSiopActualizaProcesoTmpColumnaServicio));
							return procesoBean;
							}
						});
			
			SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess()
					.withSchemaName(propertiesExterno.dbSiopdbOwner)
					.withCatalogName(propertiesExterno.dbSiopPkgAjusteMasivo)
					.withProcedureName(propertiesExterno.dbSiopActualizaProcesoTmp)
					.declareParameters(inIdProceso,outCursorProcesos, outCodigoRespuesta, outMensajeRespuesta);
			
			logger.info(cadMensaje + Constantes.MENSAJE_EJECUCION_SP + "[" + propertiesExterno.dbSiopdbOwner + "."
					+ propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopActualizaProcesoTmp + "]");
			
			MapSqlParameterSource map = new MapSqlParameterSource();
			map.addValue(propertiesExterno.dbSiopActualizaProcesoTmpInputIdProceso, actualizaProcesoBean.getProceso());
			Map<String, Object> result = procedureConsulta.execute(map);
			@SuppressWarnings("unchecked")
			List<ProcesoTmpBean> listaProcesos = (List<ProcesoTmpBean>) result.get(propertiesExterno.dbSiopActualizaProcesoTmpCursor);

			String poCodRpta = (String) result.get(Constantes.CODIGORESPUESTA);

			String poMsjRpta = (String) result.get(Constantes.MENSAJERESPUESTA);
			
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + propertiesExterno.dbSiopActualizaProcesoTmpInputIdProceso+ "]:" + actualizaProcesoBean.getProceso());
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_COD_RPTA]:" + poCodRpta);
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_MSJ_RPTA]:" + poMsjRpta);

			response.setListaProcesoTmpBean(listaProcesos);
			response.setCodigoRespuesta(poCodRpta);
			response.setMensajeRespuesta(poMsjRpta);
			
			logger.info( cadMensaje.concat(" Datos obtenidos : \n ".concat(JAXBUtilitarios.anyObjectToXmlText(response))));
		} catch (Exception e) {
			String codigoError = "";
			codigoError = propertiesExterno.spIdt1Codigo;
			String msgError = "Hubo un error tecnico al ejecutar el PROCEDURE: ["
					+ propertiesExterno.dbSiopdbOwner + "." + propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopActualizaProcesoTmp + "] ";
			response.setCodigoRespuesta(codigoError);
			response.setMensajeRespuesta(msgError);
			logger.error(mensajeTransaccion + e.getMessage(), e);
			logger.error(mensajeTransaccion + msgError);
			throw new DBException(codigoError,msgError, e);	
		}finally {
			logger.info(cadMensaje + Utilitarios.msjFinProceso(metodoActualizaProceso, Constantes.DAO));
		}
		
		return response;
	}

	@Override
	public ListaDatosAjusteSiop listarDatosAjusteSiop(ListaDatosAjusteSiop datosAjusteSiop, String mensajeTransaccion)
			throws DBException {
		String metodoActualizaProceso = "listarDatosAjusteSiop";

		String cadMensaje = mensajeTransaccion + "[" + metodoActualizaProceso + "] ";

		logger.info(cadMensaje + Utilitarios.msjInicioProceso(metodoActualizaProceso, Constantes.DAO));

		Utilitarios.infoConexion(mensajeTransaccion, propertiesExterno.dbSiopdbConexion, propertiesExterno.dbSiopdbNombre,
				propertiesExterno.dbSiopdbOwner, propertiesExterno.dbSiopPkgAjusteMasivo,
				propertiesExterno.dbSiopObtenerAjusteSiop);
		ListaDatosAjusteSiop response = null;
		
		try {
			logger.info(
					mensajeTransaccion + "Intento [" + metodoActualizaProceso + "] : ");
			response = new ListaDatosAjusteSiop();
			objJdbcCall = new SimpleJdbcCall(siopDS);
			objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			objJdbcTemplate.setQueryTimeout(propertiesExterno.DB_SIOP_CONNECTION_TIMEOUT);
			
			SqlParameter inDocReferencia = new SqlParameter(Constantes.DOC_REFERENCIA, OracleTypes.VARCHAR);
			SqlParameter inTipoAjuste = new SqlParameter(Constantes.TIPO_AJUSTE, OracleTypes.VARCHAR);
			SqlOutParameter outCodigoRespuesta = new SqlOutParameter(Constantes.CODIGORESPUESTA,
					OracleTypes.VARCHAR);
			SqlOutParameter outMensajeRespuesta = new SqlOutParameter(Constantes.MENSAJERESPUESTA,
					OracleTypes.VARCHAR);
			
			SqlOutParameter outCursorProcesos= new SqlOutParameter(Constantes.CURSORPROCESOS,
					OracleTypes.CURSOR,	new RowMapper<DatosAjusteSiop>() {
						public DatosAjusteSiop mapRow(ResultSet rs, int arg1) throws SQLException {
							DatosAjusteSiop procesoBean = new DatosAjusteSiop();
							procesoBean.setIdProceso(rs.getString(Constantes.CURSOR_DATOSSIOP_IDPROCESO)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_IDPROCESO));
							procesoBean.setCustomerId(rs.getString(Constantes.CURSOR_DATOSSIOP_CUSTOMERID)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_CUSTOMERID));
							procesoBean.setCustCode(rs.getString(Constantes.CURSOR_DATOSSIOP_CUSTCODE)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_CUSTCODE));
							procesoBean.setRuc(rs.getString(Constantes.CURSOR_DATOSSIOP_RUC)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_RUC));
							procesoBean.setRazonSocial(rs.getString(Constantes.CURSOR_DATOSSIOP_RAZONSOCIAL)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_RAZONSOCIAL));
							procesoBean.setContacto(rs.getString(Constantes.CURSOR_DATOSSIOP_CONTACTO)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_CONTACTO));
							procesoBean.setDireccion(rs.getString(Constantes.CURSOR_DATOSSIOP_DIRECCION)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_DIRECCION));
							procesoBean.setNota(rs.getString(Constantes.CURSOR_DATOSSIOP_NOTA)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_NOTA));
							procesoBean.setDepartamento(rs.getString(Constantes.CURSOR_DATOSSIOP_DEPARTAMENTO)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_DEPARTAMENTO));
							procesoBean.setProvincia(rs.getString(Constantes.CURSOR_DATOSSIOP_PROVINCIA)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_PROVINCIA));
							procesoBean.setDistrito(rs.getString(Constantes.CURSOR_DATOSSIOP_DISTRITO)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_DISTRITO));
							procesoBean.setTelefono(rs.getString(Constantes.CURSOR_DATOSSIOP_TELEFONO)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_TELEFONO));
							procesoBean.setAfiliacion(rs.getString(Constantes.CURSOR_DATOSSIOP_AFILIACION)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_AFILIACION));
							procesoBean.setEmail(rs.getString(Constantes.CURSOR_DATOSSIOP_EMAIL)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_EMAIL));
							procesoBean.setFactura(rs.getString(Constantes.CURSOR_DATOSSIOP_FACTURA)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_FACTURA));
							procesoBean.setMontoFactura(rs.getString(Constantes.CURSOR_DATOSSIOP_MONTOFACTURA)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_MONTOFACTURA));
							procesoBean.setIgvFactura(rs.getString(Constantes.CURSOR_DATOSSIOP_IGVFACTURA)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_IGVFACTURA));
							procesoBean.setIgvVigente(rs.getString(Constantes.CURSOR_DATOSSIOP_IGVVIGENTE)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_IGVVIGENTE));
							procesoBean.setEmision(rs.getString(Constantes.CURSOR_DATOSSIOP_EMISION)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_EMISION));
							procesoBean.setAmpliacion(rs.getString(Constantes.CURSOR_DATOSSIOP_AMPLIACION)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_AMPLIACION));
							procesoBean.setBillCycle(rs.getString(Constantes.CURSOR_DATOSSIOP_BILLCYCLE)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_BILLCYCLE));
							procesoBean.setInicioCiclo(rs.getString(Constantes.CURSOR_DATOSSIOP_INICIOCICLO)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_INICIOCICLO));
							procesoBean.setFinCiclo(rs.getString(Constantes.CURSOR_DATOSSIOP_FINCICLO)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_FINCICLO));
							procesoBean.setPlan(rs.getString(Constantes.CURSOR_DATOSSIOP_PLAN)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_PLAN));
							procesoBean.setDesPlan(rs.getString(Constantes.CURSOR_DATOSSIOP_DESPLAN)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_DESPLAN));
							procesoBean.setServ(rs.getString(Constantes.CURSOR_DATOSSIOP_SNCODE)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_SNCODE));
							procesoBean.setDesServ(rs.getString(Constantes.CURSOR_DATOSSIOP_DESSNCODE)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_DESSNCODE));
							procesoBean.setDesServ(rs.getString(Constantes.CURSOR_DATOSSIOP_DESSNCODE)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_DESSNCODE));
							procesoBean.setCtaPer(rs.getString(Constantes.CURSOR_DATOSSIOP_CTACONTABLE)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_CTACONTABLE));
							procesoBean.setCtaMex(rs.getString(Constantes.CURSOR_DATOSSIOP_CTACONTABLESAP)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_CTACONTABLESAP));
							procesoBean.setCtaMex(rs.getString(Constantes.CURSOR_DATOSSIOP_CTACONTABLESAP)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_CTACONTABLESAP));
							procesoBean.setMontoSinIgv(rs.getString(Constantes.CURSOR_DATOSSIOP_MTOIGV)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_MTOIGV));
							procesoBean.setAfectoIgv(rs.getString(Constantes.CURSOR_DATOSSIOP_AFECTOIGV)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_AFECTOIGV));
							procesoBean.setCentroBeneficio(rs.getString(Constantes.CURSOR_DATOSSIOP_CENTRO)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_CENTRO));
							procesoBean.setEvento(rs.getString(Constantes.CURSOR_DATOSSIOP_EVENTO)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_EVENTO));
							procesoBean.setTipoAjuste(rs.getString(Constantes.CAMPO_HISTORICO_TIPO_AJUSTE)==null?Constantes.VACIO: rs.getString(Constantes.CAMPO_HISTORICO_TIPO_AJUSTE));
							procesoBean.setMontoConIgv(rs.getString(Constantes.CAMPO_HISTORICO_MONTO_IGV)==null?Constantes.VACIO: rs.getString(Constantes.CAMPO_HISTORICO_MONTO_IGV));
							procesoBean.setTipoCliente(rs.getString(Constantes.CAMPO_CABECERA_TIPO_CLIENTE)==null?Constantes.VACIO: rs.getString(Constantes.CAMPO_CABECERA_TIPO_CLIENTE));
							procesoBean.setFechaEmision(rs.getString(Constantes.PO_FECHAEMI)==null?Constantes.VACIO: rs.getString(Constantes.PO_FECHAEMI));
							procesoBean.setFechaVencimiento(rs.getString(Constantes.PO_FECHAVEN)==null?Constantes.VACIO: rs.getString(Constantes.PO_FECHAVEN));
							procesoBean.setCtaIgv(rs.getString(Constantes.PO_CTAIGV)==null?Constantes.VACIO: rs.getString(Constantes.PO_CTAIGV));
							procesoBean.setPorcentaje(rs.getDouble(Constantes.PO_PORCENTAJE));
							procesoBean.setId(rs.getString(Constantes.PO_ID)==null?Constantes.VACIO: rs.getString(Constantes.PO_ID));
							procesoBean.setTmcode(rs.getString(Constantes.PO_TMCODE)==null?Constantes.VACIO: rs.getString(Constantes.PO_TMCODE));
							return procesoBean;
							
							}
						});
			
			SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess()
					.withSchemaName(propertiesExterno.dbSiopdbOwner)
					.withCatalogName(propertiesExterno.dbSiopPkgAjusteMasivo)
					.withProcedureName(propertiesExterno.dbSiopObtenerAjusteSiop)
					.declareParameters(inDocReferencia,inTipoAjuste,outCursorProcesos, outCodigoRespuesta, outMensajeRespuesta);
			
			logger.info(cadMensaje + Constantes.MENSAJE_EJECUCION_SP + "[" + propertiesExterno.dbSiopdbOwner + "."
					+ propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopObtenerAjusteSiop + "]");
			
			MapSqlParameterSource map = new MapSqlParameterSource();
			map.addValue(Constantes.DOC_REFERENCIA, datosAjusteSiop.getDocReferencia());
			map.addValue(Constantes.TIPO_AJUSTE, datosAjusteSiop.getTipoAjuste());
			Map<String, Object> result = procedureConsulta.execute(map);
			@SuppressWarnings("unchecked")
			List<DatosAjusteSiop> listaDatosAjusteSiop = (List<DatosAjusteSiop>) result.get(Constantes.CURSORPROCESOS);

			String poCodRpta = (String) result.get(Constantes.CODIGORESPUESTA);

			String poMsjRpta = (String) result.get(Constantes.MENSAJERESPUESTA);
			
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.DOC_REFERENCIA+ "]:" + datosAjusteSiop.getDocReferencia());	
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.TIPO_AJUSTE+ "]:" + datosAjusteSiop.getTipoAjuste());


			response.setListaDatosAjusteSiop(listaDatosAjusteSiop);
			response.setCodigoRespuesta(poCodRpta);
			response.setMensajeRespuesta(poMsjRpta);
			logger.info( cadMensaje.concat(" Datos obtenidos : \n ".concat(JAXBUtilitarios.anyObjectToXmlText(response))));
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_COD_RPTA]:" + poCodRpta);
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_MSJ_RPTA]:" + poMsjRpta);

		} catch (Exception e) {
			String codigoError = "";
			codigoError = propertiesExterno.spIdt1Codigo;
			String msgError = "Hubo un error tecnico al ejecutar el PROCEDURE: ["
					+ propertiesExterno.dbSiopdbOwner + "." + propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopObtenerAjusteSiop + "] ";
			response.setCodigoRespuesta(codigoError);
			response.setMensajeRespuesta(msgError);
			logger.error(mensajeTransaccion + e.getMessage(), e);
			logger.error(mensajeTransaccion + msgError);
			throw new DBException(codigoError,msgError, e);	
		}finally {
			logger.info(cadMensaje + Utilitarios.msjFinProceso(metodoActualizaProceso, Constantes.DAO));
		}
		
		return response;
	}

	@Override
	public ListarCabeceraAjusteBean listarCabeceraAjuste(ListarCabeceraAjusteBean listarCabeceraAjusteBean,
			String mensajeTransaccion) throws DBException {
		String metodoActualizaProceso = "listarCabeceraAjuste";

		String cadMensaje = mensajeTransaccion + "[" + metodoActualizaProceso + "] ";

		logger.info(cadMensaje + Utilitarios.msjInicioProceso(metodoActualizaProceso, Constantes.DAO));

		Utilitarios.infoConexion(mensajeTransaccion, propertiesExterno.dbSiopdbConexion, propertiesExterno.dbSiopdbNombre,
				propertiesExterno.dbSiopdbOwner, propertiesExterno.dbSiopPkgAjusteMasivo,
				propertiesExterno.dbSiopObtenerCabeceraAjusteSiop);
		ListarCabeceraAjusteBean response = null;
		try {
			logger.info(
					mensajeTransaccion + "Intento [" + metodoActualizaProceso + "] : ");
			response = new ListarCabeceraAjusteBean();
			objJdbcCall = new SimpleJdbcCall(siopDS);
			objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			objJdbcTemplate.setQueryTimeout(propertiesExterno.DB_SIOP_CONNECTION_TIMEOUT);
			
			SqlParameter inIdProceso = new SqlParameter(Constantes.ENTRADA_PROCESO, OracleTypes.VARCHAR);
			SqlOutParameter outCursorProcesos= new SqlOutParameter(Constantes.CURSORPROCESOS,
					OracleTypes.CURSOR,	new RowMapper<AjusteCabeceraBean>() {
						public AjusteCabeceraBean mapRow(ResultSet rs, int arg1) throws SQLException {
							AjusteCabeceraBean procesoBean = new AjusteCabeceraBean();
							procesoBean.setId(Integer.parseInt(rs.getString(Constantes.CURSOR_CABECERA_ID)== null?Constantes.CADENA_NUMERO_NULO:rs.getString(Constantes.CURSOR_CABECERA_ID)));
							procesoBean.setIdProceso(Integer.parseInt(rs.getString(Constantes.CURSOR_CABECERA_ID_PROCESO)== null?Constantes.CADENA_NUMERO_NULO:rs.getString(Constantes.CURSOR_CABECERA_ID_PROCESO)));
							procesoBean.setCodigoAjuste(rs.getString(Constantes.CURSOR_CABECERA_CODIGO_AJUSTE)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_CABECERA_CODIGO_AJUSTE));
							procesoBean.setTipoAjuste(rs.getString(Constantes.CURSOR_CABECERA_TIPO_AJUSTE)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_CABECERA_TIPO_AJUSTE));
							procesoBean.setTipoDocAjuste(rs.getString(Constantes.CURSOR_CABECERA_TIPO_DOC_AJUSTE)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_CABECERA_TIPO_DOC_AJUSTE));
							procesoBean.setDocReferencia(rs.getString(Constantes.CURSOR_CABECERA_DOC_REFERENCIA)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_CABECERA_DOC_REFERENCIA));
							return procesoBean;
							}
						});
			SqlOutParameter outFlagDetalle = new SqlOutParameter(Constantes.FLAG_DETALLE,
					OracleTypes.VARCHAR);
			SqlOutParameter outCodigoRespuesta = new SqlOutParameter(Constantes.CODIGORESPUESTA,
					OracleTypes.VARCHAR);
			SqlOutParameter outMensajeRespuesta = new SqlOutParameter(Constantes.MENSAJERESPUESTA,
					OracleTypes.VARCHAR);
			
			SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess()
					.withSchemaName(propertiesExterno.dbSiopdbOwner)
					.withCatalogName(propertiesExterno.dbSiopPkgAjusteMasivo)
					.withProcedureName(propertiesExterno.dbSiopObtenerCabeceraAjusteSiop)
					.declareParameters(inIdProceso,outCursorProcesos, outFlagDetalle,outCodigoRespuesta, outMensajeRespuesta);
			
			logger.info(cadMensaje + Constantes.MENSAJE_EJECUCION_SP + "[" + propertiesExterno.dbSiopdbOwner + "."
					+ propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopObtenerCabeceraAjusteSiop + "]");
			
			
			MapSqlParameterSource map = new MapSqlParameterSource();
			map.addValue(Constantes.ENTRADA_PROCESO, listarCabeceraAjusteBean.getIdProceso());
			Map<String, Object> result = procedureConsulta.execute(map);
			@SuppressWarnings("unchecked")
			List<AjusteCabeceraBean> listaDatosAjusteCabecera = (List<AjusteCabeceraBean>) result.get(Constantes.CURSORPROCESOS);
			
			String poFlagDetalle = (String) result.get(Constantes.FLAG_DETALLE);

			String poCodRpta = (String) result.get(Constantes.CODIGORESPUESTA);

			String poMsjRpta = (String) result.get(Constantes.MENSAJERESPUESTA);
			
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.ENTRADA_PROCESO+ "]:" + listarCabeceraAjusteBean.getIdProceso());	
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_COD_RPTA]:" + poFlagDetalle);
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_COD_RPTA]:" + poCodRpta);
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_MSJ_RPTA]:" + poMsjRpta);


			response.setListaAjusteCabecera(listaDatosAjusteCabecera);
			response.setFlagDet(poFlagDetalle);
			response.setCodigoRespuesta(poCodRpta);
			response.setMensajeRespuesta(poMsjRpta);
			logger.info( cadMensaje.concat(" Datos obtenidos : \n ".concat(JAXBUtilitarios.anyObjectToXmlText(response))));
		} catch (Exception e) {
			String codigoError = "";
			codigoError = propertiesExterno.spIdt1Codigo;
			String msgError = "Hubo un error tecnico al ejecutar el PROCEDURE: ["
					+ propertiesExterno.dbSiopdbOwner + "." + propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopObtenerCabeceraAjusteSiop + "] ";
			response.setCodigoRespuesta(codigoError);
			response.setMensajeRespuesta(msgError);
			logger.error(mensajeTransaccion + e.getMessage(), e);
			logger.error(mensajeTransaccion + msgError);
			throw new DBException(codigoError,msgError, e);	
		}finally {
			logger.info(cadMensaje + Utilitarios.msjFinProceso(metodoActualizaProceso, Constantes.DAO));
		}
		

		
		return response;
	}

	@Override
	public ListarDetalleArchPlanoBean listarDetalle(ListarDetalleArchPlanoBean bean, String mensajeTransaccion)
			throws DBException {
		String metodoActualizaProceso = "listarDetalle";

		String cadMensaje = mensajeTransaccion + "[" + metodoActualizaProceso + "] ";

		logger.info(cadMensaje + Utilitarios.msjInicioProceso(metodoActualizaProceso, Constantes.DAO));

		Utilitarios.infoConexion(mensajeTransaccion, propertiesExterno.dbSiopdbConexion, propertiesExterno.dbSiopdbNombre,
				propertiesExterno.dbSiopdbOwner, propertiesExterno.dbSiopPkgAjusteMasivo,
				propertiesExterno.dbSiopListarArchPlano);
		ListarDetalleArchPlanoBean response = null;	

		try {
			logger.info(
					mensajeTransaccion + "Intento [" + metodoActualizaProceso + "] : ");
			response = new ListarDetalleArchPlanoBean();
			objJdbcCall = new SimpleJdbcCall(siopDS);
			objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			objJdbcTemplate.setQueryTimeout(propertiesExterno.DB_SIOP_CONNECTION_TIMEOUT);
			
			SqlParameter inIdProceso = new SqlParameter(Constantes.ENTRADA_PROCESO, OracleTypes.INTEGER);
			SqlParameter inDocReferencia = new SqlParameter(Constantes.ENTRADA_DOCREFERENCIA, OracleTypes.VARCHAR);
			SqlOutParameter outCodigoRespuesta = new SqlOutParameter(Constantes.CODIGORESPUESTA,
					OracleTypes.VARCHAR);
			SqlOutParameter outMensajeRespuesta = new SqlOutParameter(Constantes.MENSAJERESPUESTA,
					OracleTypes.VARCHAR);
			SqlOutParameter outCursorProcesos= new SqlOutParameter(Constantes.CURSORPROCESOS,
					OracleTypes.CURSOR,	new RowMapper<DetalleArchPlanoBean>() {
						public DetalleArchPlanoBean mapRow(ResultSet rs, int arg1) throws SQLException {
							DetalleArchPlanoBean procesoBean = new DetalleArchPlanoBean();
							procesoBean.setIdproceso(rs.getString(Constantes.IDPROCESO)==null?Constantes.VACIO: rs.getString(Constantes.IDPROCESO));
//							procesoBean.setId(rs.getString(Constantes.ID)==null?Constantes.VACIO: rs.getString(Constantes.ID));
							procesoBean.setCustomerid(rs.getString(Constantes.CUSTOMERID)==null?Constantes.VACIO: rs.getString(Constantes.CUSTOMERID));
							procesoBean.setCustcode(rs.getString(Constantes.CUSTCODE)==null?Constantes.VACIO: rs.getString(Constantes.CUSTCODE));
							procesoBean.setRuc(rs.getString(Constantes.RUC)==null?Constantes.VACIO: rs.getString(Constantes.RUC));
							procesoBean.setRazonsocial(rs.getString(Constantes.RAZONSOCIAL)==null?Constantes.VACIO: rs.getString(Constantes.RAZONSOCIAL));
							procesoBean.setContacto(rs.getString(Constantes.CONTACTO)==null?Constantes.VACIO: rs.getString(Constantes.CONTACTO));
							procesoBean.setTipocliente(rs.getString(Constantes.TIPOCLIENTE)==null?Constantes.VACIO: rs.getString(Constantes.TIPOCLIENTE));
							procesoBean.setDireccion(rs.getString(Constantes.DIRECCION)==null?Constantes.VACIO: rs.getString(Constantes.DIRECCION));
							procesoBean.setNotadir(rs.getString(Constantes.NOTADIR)==null?Constantes.VACIO: rs.getString(Constantes.NOTADIR));
							procesoBean.setDepartamento(rs.getString(Constantes.DEPARTAMENTO)==null?Constantes.VACIO: rs.getString(Constantes.DEPARTAMENTO));
							procesoBean.setProvincia(rs.getString(Constantes.PROVINCIA)==null?Constantes.VACIO: rs.getString(Constantes.PROVINCIA));
							procesoBean.setDistrito(rs.getString(Constantes.DISTRITO)==null?Constantes.VACIO: rs.getString(Constantes.DISTRITO));
							procesoBean.setTelefono(rs.getString(Constantes.TELEFONO)==null?Constantes.VACIO: rs.getString(Constantes.TELEFONO));
							procesoBean.setAfiliacion(rs.getString(Constantes.AFILIACION)==null?Constantes.VACIO: rs.getString(Constantes.AFILIACION));
							procesoBean.setEmail(rs.getString(Constantes.EMAIL)==null?Constantes.VACIO: rs.getString(Constantes.EMAIL));
							procesoBean.setFactura(rs.getString(Constantes.FACTURA)==null?Constantes.VACIO: rs.getString(Constantes.FACTURA));
							procesoBean.setMontofactura(rs.getString(Constantes.MONTOFACTURA)==null?Constantes.VACIO: rs.getString(Constantes.MONTOFACTURA));
							procesoBean.setIgv(rs.getString(Constantes.IGV)==null?Constantes.VACIO: rs.getString(Constantes.IGV));
							procesoBean.setIgvvigente(rs.getString(Constantes.IGVVIGENTE)==null?Constantes.VACIO: rs.getString(Constantes.IGVVIGENTE));
							procesoBean.setEmision(rs.getString(Constantes.EMISION)==null?Constantes.VACIO: rs.getString(Constantes.EMISION));
							procesoBean.setAmpliacion(rs.getString(Constantes.AMPLIACION)==null?Constantes.VACIO: rs.getString(Constantes.AMPLIACION));
							procesoBean.setBillcycle(rs.getString(Constantes.BILLCYCLE)==null?Constantes.VACIO: rs.getString(Constantes.BILLCYCLE));
							procesoBean.setIniciociclo(rs.getString(Constantes.INICIOCICLO)==null?Constantes.VACIO: rs.getString(Constantes.INICIOCICLO));
							procesoBean.setFinciclo(rs.getString(Constantes.FINCICLO)==null?Constantes.VACIO: rs.getString(Constantes.FINCICLO));
							procesoBean.setPlan(rs.getString(Constantes.PLAN)==null?Constantes.VACIO: rs.getString(Constantes.PLAN));
							procesoBean.setDesplan(rs.getString(Constantes.DESPLAN)==null?Constantes.VACIO: rs.getString(Constantes.DESPLAN));
							procesoBean.setServ(rs.getString(Constantes.SERV)==null?Constantes.VACIO: rs.getString(Constantes.SERV));
							procesoBean.setDesserv(rs.getString(Constantes.DESSERV)==null?Constantes.VACIO: rs.getString(Constantes.DESSERV));
							procesoBean.setCtaper(rs.getString(Constantes.CTAPER)==null?Constantes.VACIO: rs.getString(Constantes.CTAPER));
							procesoBean.setCtamex(rs.getString(Constantes.CTAMEX)==null?Constantes.VACIO: rs.getString(Constantes.CTAMEX));
							procesoBean.setMontoserv(rs.getString(Constantes.MONTOSERV)==null?Constantes.VACIO: rs.getString(Constantes.MONTOSERV));
							procesoBean.setAfectoigv(rs.getString(Constantes.AFECTOIGV)==null?Constantes.VACIO: rs.getString(Constantes.AFECTOIGV));
							procesoBean.setCebe(rs.getString(Constantes.CEBE)==null?Constantes.VACIO: rs.getString(Constantes.CEBE));
							procesoBean.setFechaEmision(rs.getString(Constantes.EMISION)==null?Constantes.VACIO: rs.getString(Constantes.EMISION));
							procesoBean.setFechaVencimiento(rs.getString(Constantes.AMPLIACION)==null?Constantes.VACIO: rs.getString(Constantes.AMPLIACION));
							procesoBean.setCtaigv(rs.getString(Constantes.CTAIGV)==null?Constantes.VACIO: rs.getString(Constantes.CTAIGV));
							procesoBean.setPorcentaje(rs.getDouble(Constantes.PORCENTAJE));
							return procesoBean;
							}
						});
			SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess()
					.withSchemaName(propertiesExterno.dbSiopdbOwner)
					.withCatalogName(propertiesExterno.dbSiopPkgAjusteMasivo)
					.withProcedureName(propertiesExterno.dbSiopListarArchPlano)
					.declareParameters(inIdProceso,inDocReferencia,outCursorProcesos, outCodigoRespuesta, outMensajeRespuesta);
			
			logger.info(cadMensaje + Constantes.MENSAJE_EJECUCION_SP + "[" + propertiesExterno.dbSiopdbOwner + "."
					+ propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopListarArchPlano + "]");
			
			MapSqlParameterSource map = new MapSqlParameterSource();
			map.addValue(Constantes.ENTRADA_PROCESO, bean.getIdProceso());
			map.addValue(Constantes.ENTRADA_DOCREFERENCIA, bean.getDocReferencia());
			Map<String, Object> result = procedureConsulta.execute(map);
			@SuppressWarnings("unchecked")
			List<DetalleArchPlanoBean> listaDatosAjusteCabecera = (List<DetalleArchPlanoBean>) result.get(Constantes.CURSORPROCESOS);	

			String poCodRpta = (String) result.get(Constantes.CODIGORESPUESTA);

			String poMsjRpta = (String) result.get(Constantes.MENSAJERESPUESTA);
			
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.ENTRADA_PROCESO+ "]:" + bean.getIdProceso());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.ENTRADA_DOCREFERENCIA+ "]:" + bean.getDocReferencia());
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_COD_RPTA]:" + poCodRpta);
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_MSJ_RPTA]:" + poMsjRpta);
			

			response.setListaDetalle(listaDatosAjusteCabecera);
			response.setCodigoRespuesta(poCodRpta);
			response.setMensajeRespuesta(poMsjRpta);
			logger.info( cadMensaje.concat(" Datos obtenidos : \n ".concat(JAXBUtilitarios.anyObjectToXmlText(response))));
		} catch (Exception e) {
			String codigoError = "";
			codigoError = propertiesExterno.spIdt1Codigo;
			String msgError = "Hubo un error tecnico al ejecutar el PROCEDURE: ["
					+ propertiesExterno.dbSiopdbOwner + "." + propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopListarArchPlano + "] ";
			response.setCodigoRespuesta(codigoError);
			response.setMensajeRespuesta(msgError);
			logger.error(mensajeTransaccion + e.getMessage(), e);
			logger.error(mensajeTransaccion + msgError);
			throw new DBException(codigoError,msgError, e);	
		}finally {
			logger.info(cadMensaje + Utilitarios.msjFinProceso(metodoActualizaProceso, Constantes.DAO));
		}
		return response;
	}

	@Override
	public ActualizaEstadoBean actualizarEstado(ActualizaEstadoBean bean, String mensajeTransaccion)
			throws DBException {
		String metodoActualizaProceso = "actualizarEstado";

		String cadMensaje = mensajeTransaccion + "[" + metodoActualizaProceso + "] ";

		logger.info(cadMensaje + Utilitarios.msjInicioProceso(metodoActualizaProceso, Constantes.DAO));

		Utilitarios.infoConexion(mensajeTransaccion, propertiesExterno.dbSiopdbConexion, propertiesExterno.dbSiopdbNombre,
				propertiesExterno.dbSiopdbOwner, propertiesExterno.dbSiopPkgAjusteMasivo,
				propertiesExterno.dbSiopActEstado);
		ActualizaEstadoBean response = null;	

		try {
			logger.info(
					mensajeTransaccion + "Intento [" + metodoActualizaProceso + "] : ");
			response = new ActualizaEstadoBean();
			objJdbcCall = new SimpleJdbcCall(siopDS);
			objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			objJdbcTemplate.setQueryTimeout(propertiesExterno.DB_SIOP_CONNECTION_TIMEOUT);
			
			SqlParameter inId = new SqlParameter(Constantes.PI_ID, OracleTypes.VARCHAR);
			SqlParameter inEstado = new SqlParameter(Constantes.COD_ESTADO, OracleTypes.VARCHAR);
			SqlOutParameter outCodigoRespuesta = new SqlOutParameter(Constantes.CODIGORESPUESTA,
					OracleTypes.VARCHAR);
			SqlOutParameter outMensajeRespuesta = new SqlOutParameter(Constantes.MENSAJERESPUESTA,
					OracleTypes.VARCHAR);
			SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess()
					.withSchemaName(propertiesExterno.dbSiopdbOwner)
					.withCatalogName(propertiesExterno.dbSiopPkgAjusteMasivo)
					.withProcedureName(propertiesExterno.dbSiopActEstado)
					.declareParameters(inId,inEstado, outCodigoRespuesta, outMensajeRespuesta);
			
			logger.info(cadMensaje + Constantes.MENSAJE_EJECUCION_SP + "[" + propertiesExterno.dbSiopdbOwner + "."
					+ propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopActEstado + "]");
			
			MapSqlParameterSource map = new MapSqlParameterSource();
			map.addValue(Constantes.PI_ID, bean.getId());
			map.addValue(Constantes.COD_ESTADO, bean.getEstado());
			Map<String, Object> result = procedureConsulta.execute(map);


			String poCodRpta = (String) result.get(Constantes.CODIGORESPUESTA);

			String poMsjRpta = (String) result.get(Constantes.MENSAJERESPUESTA);
			
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_ID+ "]:" + bean.getId());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.COD_ESTADO+ "]:" + bean.getEstado());
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_COD_RPTA]:" + poCodRpta);
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_MSJ_RPTA]:" + poMsjRpta);
		
			response.setCodigoRespuesta(poCodRpta);
			response.setMensajeRespuesta(poMsjRpta);
			logger.info( cadMensaje.concat(" Datos obtenidos : \n ".concat(JAXBUtilitarios.anyObjectToXmlText(response))));

		} catch (Exception e) {
			String codigoError = "";
			codigoError = propertiesExterno.spIdt1Codigo;
			String msgError = "Hubo un error tecnico al ejecutar el PROCEDURE: ["
					+ propertiesExterno.dbSiopdbOwner + "." + propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopListarArchPlano + "] ";
			response.setCodigoRespuesta(codigoError);
			response.setMensajeRespuesta(msgError);
			logger.error(mensajeTransaccion + e.getMessage(), e);
			logger.error(mensajeTransaccion + msgError);
			throw new DBException(codigoError,msgError, e);	
		}finally {
			logger.info(cadMensaje + Utilitarios.msjFinProceso(metodoActualizaProceso, Constantes.DAO));
		}
		return response;
	}

	@Override
	public ResponseBase registrarDetalleRecBscsAMTotal(DetalleArchPlanoBean detalleArchivoPlano,
			String mensajeTransaccion,Integer id) throws DBException {
		String metodoActualizaProceso = "registrarDetalleRecBscsAMTotal";

		String cadMensaje = mensajeTransaccion + "[" + metodoActualizaProceso + "] ";

		logger.info(cadMensaje + Utilitarios.msjInicioProceso(metodoActualizaProceso, Constantes.DAO));

		Utilitarios.infoConexion(mensajeTransaccion, propertiesExterno.dbSiopdbConexion, propertiesExterno.dbSiopdbNombre,
				propertiesExterno.dbSiopdbOwner, propertiesExterno.dbSiopPkgAjusteMasivo,
				propertiesExterno.dbSiopRegDetalleBSCSTotal);
		
		ResponseBase response = null;
		try {
			logger.info(
					mensajeTransaccion + "Intento [" + metodoActualizaProceso + "] : ");
			response = new ResponseBase();
			objJdbcCall = new SimpleJdbcCall(siopDS);
			objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			objJdbcTemplate.setQueryTimeout(propertiesExterno.DB_SIOP_CONNECTION_TIMEOUT);
			
			SqlParameter inId = new SqlParameter(Constantes.PI_ID, OracleTypes.VARCHAR);
			SqlParameter inIdproceso = new SqlParameter(Constantes.PI_IDPROCESO, OracleTypes.VARCHAR);
			SqlParameter inCuentacontable = new SqlParameter(Constantes.PI_CUENTACONTABLE, OracleTypes.VARCHAR);
			SqlParameter inEvento = new SqlParameter(Constantes.PI_EVENTO, OracleTypes.VARCHAR);
			SqlParameter inCuentacontablesap = new SqlParameter(Constantes.PI_CUENTACONTABLESAP, OracleTypes.VARCHAR);
			SqlParameter inAfecto = new SqlParameter(Constantes.PI_AFECTO, OracleTypes.VARCHAR);
			SqlParameter inMontosinigv = new SqlParameter(Constantes.PI_MONTOSINIGV, OracleTypes.VARCHAR);
			SqlParameter inMontoconigv = new SqlParameter(Constantes.PI_MONTOCONIGV, OracleTypes.VARCHAR);
			SqlParameter inNombrerubro = new SqlParameter(Constantes.PI_NOMBRERUBRO, OracleTypes.VARCHAR);
			SqlParameter inComceptorubro = new SqlParameter(Constantes.PI_COMCEPTORUBRO, OracleTypes.VARCHAR);
			SqlParameter inSncode = new SqlParameter(Constantes.PI_SNCODE, OracleTypes.VARCHAR);
			SqlParameter inTmcode = new SqlParameter(Constantes.PI_TMCODE, OracleTypes.VARCHAR);
			SqlParameter inLinea = new SqlParameter(Constantes.PI_LINEA, OracleTypes.VARCHAR);
			SqlParameter inPlan = new SqlParameter(Constantes.PI_PLAN, OracleTypes.VARCHAR);
			SqlParameter inCentrobenef = new SqlParameter(Constantes.PI_CENTROBENEF, OracleTypes.VARCHAR);
			SqlParameter inEstadoin = new SqlParameter(Constantes.PI_ESTADO, OracleTypes.VARCHAR);
			SqlParameter inCtaigv = new SqlParameter(Constantes.PI_CTAIGV, OracleTypes.VARCHAR);
			SqlParameter inPorcentaje = new SqlParameter(Constantes.PI_PORCENTAJE, OracleTypes.NUMBER);
			SqlOutParameter outCodigoRespuesta = new SqlOutParameter(Constantes.CODIGORESPUESTA,
					OracleTypes.VARCHAR);
			SqlOutParameter outMensajeRespuesta = new SqlOutParameter(Constantes.MENSAJERESPUESTA,
					OracleTypes.VARCHAR);
			
			SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess()
					.withSchemaName(propertiesExterno.dbSiopdbOwner)
					.withCatalogName(propertiesExterno.dbSiopPkgAjusteMasivo)
					.withProcedureName(propertiesExterno.dbSiopRegDetalleBSCSTotal)
					.declareParameters(inId,
							inIdproceso,
							inCuentacontable,
							inEvento,
							inCuentacontablesap,
							inAfecto,
							inMontosinigv,
							inMontoconigv,
							inNombrerubro,
							inComceptorubro,
							inSncode,
							inTmcode,
							inLinea,
							inPlan,
							inCentrobenef,
							inEstadoin,
							inCtaigv,
							inPorcentaje,
							outCodigoRespuesta, outMensajeRespuesta);
			
			logger.info(cadMensaje + Constantes.MENSAJE_EJECUCION_SP + "[" + propertiesExterno.dbSiopdbOwner + "."
					+ propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopRegDetalleBSCSTotal + "]");
			
			MapSqlParameterSource map = new MapSqlParameterSource();
			map.addValue(Constantes.PI_ID,id);
			map.addValue(Constantes.PI_IDPROCESO,detalleArchivoPlano.getIdproceso());
			map.addValue(Constantes.PI_CUENTACONTABLE,detalleArchivoPlano.getCtaper());
			map.addValue(Constantes.PI_EVENTO,Constantes.CADENAVACIA);
			map.addValue(Constantes.PI_CUENTACONTABLESAP,detalleArchivoPlano.getCtamex());
			map.addValue(Constantes.PI_AFECTO,detalleArchivoPlano.getAfectoigv());
			map.addValue(Constantes.PI_MONTOSINIGV,detalleArchivoPlano.getMontoserv());
			map.addValue(Constantes.PI_MONTOCONIGV,detalleArchivoPlano.getMontoConIgv());
			map.addValue(Constantes.PI_NOMBRERUBRO,detalleArchivoPlano.getNombreRubro());
			map.addValue(Constantes.PI_COMCEPTORUBRO,detalleArchivoPlano.getConceptoRubro());
			map.addValue(Constantes.PI_SNCODE,detalleArchivoPlano.getServ());
			map.addValue(Constantes.PI_TMCODE,detalleArchivoPlano.getPlan());
			map.addValue(Constantes.PI_LINEA,detalleArchivoPlano.getTelefono());
			map.addValue(Constantes.PI_PLAN,detalleArchivoPlano.getDesplan());
			map.addValue(Constantes.PI_CENTROBENEF,detalleArchivoPlano.getCebe());
			map.addValue(Constantes.PI_ESTADO,Constantes.BLOQUEO_VALID);
			map.addValue(Constantes.PI_CTAIGV,detalleArchivoPlano.getCtaigv());
			map.addValue(Constantes.PI_PORCENTAJE, detalleArchivoPlano.getPorcentaje());

			logger.info(cadMensaje + Constantes.MENSAJE_EJECUCION_SP + "[" + propertiesExterno.dbSiopdbOwner + "."
					+ propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopRegDetalleBSCSTotal + "]");
			logger.info( cadMensaje.concat(" PARAMETROS [INPUT] : \n ".concat(JAXBUtilitarios.anyObjectToXmlText(detalleArchivoPlano))));
			Map<String, Object> result = procedureConsulta.execute(map);


			String poCodRpta = (String) result.get(Constantes.CODIGORESPUESTA);

			String poMsjRpta = (String) result.get(Constantes.MENSAJERESPUESTA);
			
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_COD_RPTA]:" + poCodRpta);
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_MSJ_RPTA]:" + poMsjRpta);
		
			response.setCodigoRespuesta(poCodRpta);
			response.setMensajeRespuesta(poMsjRpta);
			logger.info( cadMensaje.concat(" Datos obtenidos : \n ".concat(JAXBUtilitarios.anyObjectToXmlText(response))));
		} catch (Exception e) {
			String codigoError = "";
			codigoError = propertiesExterno.spIdt1Codigo;
			String msgError = "Hubo un error tecnico al ejecutar el PROCEDURE: ["
					+ propertiesExterno.dbSiopdbOwner + "." + propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopRegDetalleBSCSTotal + "] ";
			response.setCodigoRespuesta(codigoError);
			response.setMensajeRespuesta(msgError);
			logger.error(mensajeTransaccion + e.getMessage(), e);
			logger.error(mensajeTransaccion + msgError);
			throw new DBException(codigoError,msgError, e);	
		}finally {
			
		}
		return response;
	}

	@Override
	public ActualizarMontoDetalleBSCSBean actualizarMontosDetalle(ActualizarMontoDetalleBSCSBean bean,
			String mensajeTransaccion) throws DBException {
		String metodoActualizaProceso = "actualizarMontosDetalle";

		String cadMensaje = mensajeTransaccion + "[" + metodoActualizaProceso + "] ";

		logger.info(cadMensaje + Utilitarios.msjInicioProceso(metodoActualizaProceso, Constantes.DAO));

		Utilitarios.infoConexion(mensajeTransaccion, propertiesExterno.dbSiopdbConexion, propertiesExterno.dbSiopdbNombre,
				propertiesExterno.dbSiopdbOwner, propertiesExterno.dbSiopPkgAjusteMasivo,
				propertiesExterno.dbSiopActualizaMontosDetBSCS);
		ActualizarMontoDetalleBSCSBean response = null;	
		try {
			logger.info(
					mensajeTransaccion + "Intento [" + metodoActualizaProceso + "] : ");
			response = new ActualizarMontoDetalleBSCSBean();
			objJdbcCall = new SimpleJdbcCall(siopDS);
			objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			objJdbcTemplate.setQueryTimeout(propertiesExterno.DB_SIOP_CONNECTION_TIMEOUT);
			
			SqlParameter inId = new SqlParameter(Constantes.PI_SPACTMONDET_ID, OracleTypes.INTEGER);
			SqlParameter inIdProceso = new SqlParameter(Constantes.PI_SPACTMONDET_IDPROCESO, OracleTypes.INTEGER);
			SqlParameter inSnCode = new SqlParameter(Constantes.PI_SPACTMONDET_SNCODE, OracleTypes.VARCHAR);
			SqlParameter inMontoConIgv = new SqlParameter(Constantes.PI_SPACTMONDET_MONTOCONIGV, OracleTypes.NUMBER);
			SqlOutParameter outCodigoRespuesta = new SqlOutParameter(Constantes.CODIGORESPUESTA,
					OracleTypes.VARCHAR);
			SqlOutParameter outMensajeRespuesta = new SqlOutParameter(Constantes.MENSAJERESPUESTA,
					OracleTypes.VARCHAR);
			SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess()
					.withSchemaName(propertiesExterno.dbSiopdbOwner)
					.withCatalogName(propertiesExterno.dbSiopPkgAjusteMasivo)
					.withProcedureName(propertiesExterno.dbSiopActualizaMontosDetBSCS)
					.declareParameters(inId,inIdProceso,inSnCode, inMontoConIgv ,outCodigoRespuesta, outMensajeRespuesta);
			
			logger.info(cadMensaje + Constantes.MENSAJE_EJECUCION_SP + "[" + propertiesExterno.dbSiopdbOwner + "."
					+ propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopActualizaMontosDetBSCS + "]");
			
			MapSqlParameterSource map = new MapSqlParameterSource();
			map.addValue(Constantes.PI_SPACTMONDET_ID, bean.getId());
			map.addValue(Constantes.PI_SPACTMONDET_IDPROCESO, bean.getIdProceso());
			map.addValue(Constantes.PI_SPACTMONDET_SNCODE, bean.getSnCode());
			map.addValue(Constantes.PI_SPACTMONDET_MONTOCONIGV, bean.getMontoConIgv());
			Map<String, Object> result = procedureConsulta.execute(map);


			String poCodRpta = (String) result.get(Constantes.CODIGORESPUESTA);

			String poMsjRpta = (String) result.get(Constantes.MENSAJERESPUESTA);
			
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_SPACTMONDET_ID+ "]:" + bean.getId());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_SPACTMONDET_IDPROCESO+ "]:" + bean.getIdProceso());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_SPACTMONDET_SNCODE+ "]:" + bean.getSnCode());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_SPACTMONDET_MONTOCONIGV+ "]:" + bean.getMontoConIgv());
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_COD_RPTA]:" + poCodRpta);
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_MSJ_RPTA]:" + poMsjRpta);
		
			response.setCodigoRespuesta(poCodRpta);
			response.setMensajeRespuesta(poMsjRpta);
			logger.info( cadMensaje.concat(" Datos obtenidos : \n ".concat(JAXBUtilitarios.anyObjectToXmlText(response))));
		} catch (Exception e) {
			String codigoError = "";
			codigoError = propertiesExterno.spIdt1Codigo;
			String msgError = "Hubo un error tecnico al ejecutar el PROCEDURE: ["
					+ propertiesExterno.dbSiopdbOwner + "." + propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopActualizaMontosDetBSCS + "] ";
			response.setCodigoRespuesta(codigoError);
			response.setMensajeRespuesta(msgError);
			logger.error(mensajeTransaccion + e.getMessage(), e);
			logger.error(mensajeTransaccion + msgError);
			throw new DBException(codigoError,msgError, e);	
		}finally {
			logger.info(cadMensaje + Utilitarios.msjFinProceso(metodoActualizaProceso, Constantes.DAO));
		}
		return response;
	}

	@Override
	public ListaDetalleTemporalBean listarDetalleTemporal(ListaDetalleTemporalBean bean, String mensajeTransaccion)
			throws DBException {
		String metodoActualizaProceso = "listarDetalleTemporal";

		String cadMensaje = mensajeTransaccion + "[" + metodoActualizaProceso + "] ";

		logger.info(cadMensaje + Utilitarios.msjInicioProceso(metodoActualizaProceso, Constantes.DAO));

		Utilitarios.infoConexion(mensajeTransaccion, propertiesExterno.dbSiopdbConexion, propertiesExterno.dbSiopdbNombre,
				propertiesExterno.dbSiopdbOwner, propertiesExterno.dbSiopPkgAjusteMasivo,
				propertiesExterno.dbSiopListaTmp);
		ListaDetalleTemporalBean response = null;
		
		try {
			
			logger.info(
					mensajeTransaccion + "Intento [" + metodoActualizaProceso + "] : ");
			response = new ListaDetalleTemporalBean();
			objJdbcCall = new SimpleJdbcCall(siopDS);
			objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			objJdbcTemplate.setQueryTimeout(propertiesExterno.DB_SIOP_CONNECTION_TIMEOUT);
			
			SqlParameter inId = new SqlParameter(Constantes.PI_ID,OracleTypes.INTEGER);
			SqlParameter inIdProceso = new SqlParameter(Constantes.ENTRADA_PROCESO, OracleTypes.INTEGER);
			SqlParameter inDocReferencia = new SqlParameter(Constantes.DOC_REFERENCIA, OracleTypes.VARCHAR);
			SqlOutParameter outCursorProcesos= new SqlOutParameter(Constantes.CURSORPROCESOS,
					OracleTypes.CURSOR,	new RowMapper<DetalleTemporalBean>() {
						public DetalleTemporalBean mapRow(ResultSet rs, int arg1) throws SQLException {
							DetalleTemporalBean procesoBean = new DetalleTemporalBean();
							procesoBean.setId(Integer.parseInt(rs.getString(Constantes.TMP_DETALLE_ID)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_ID)));
							procesoBean.setIdproceso(Integer.parseInt(rs.getString(Constantes.TMP_DETALLE_IDPROCESO)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_IDPROCESO)));
							procesoBean.setDocreferencia(rs.getString(Constantes.TMP_DETALLE_DOCREFERENCIA)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_DOCREFERENCIA));
							procesoBean.setSncode(rs.getString(Constantes.TMP_DETALLE_SNCODE)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_SNCODE));
							procesoBean.setAfecto(rs.getString(Constantes.TMP_DETALLE_AFECTO)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_AFECTO));
							procesoBean.setCustomerid(rs.getString(Constantes.TMP_DETALLE_CUSTOMERID)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_CUSTOMERID));
							procesoBean.setCustcode(rs.getString(Constantes.TMP_DETALLE_CUSTCODE)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_CUSTCODE));
							procesoBean.setRuc(rs.getString(Constantes.TMP_DETALLE_RUC)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_RUC));
							procesoBean.setRazonsocial(rs.getString(Constantes.TMP_DETALLE_RAZONSOCIAL)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_RAZONSOCIAL));
							procesoBean.setContacto(rs.getString(Constantes.TMP_DETALLE_CONTACTO)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_CONTACTO));
							procesoBean.setDireccion(rs.getString(Constantes.TMP_DETALLE_DIRECCION)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_DIRECCION));
							procesoBean.setNotadir(rs.getString(Constantes.TMP_DETALLE_NOTADIR)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_NOTADIR));
							procesoBean.setDepartamento(rs.getString(Constantes.TMP_DETALLE_DEPARTAMENTO)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_DEPARTAMENTO));
							procesoBean.setProvincia(rs.getString(Constantes.TMP_DETALLE_PROVINCIA)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_PROVINCIA));
							procesoBean.setDistrito(rs.getString(Constantes.TMP_DETALLE_DISTRITO)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_DISTRITO));
							procesoBean.setNrotelefono(rs.getString(Constantes.TMP_DETALLE_NROTELEFONO)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_NROTELEFONO));
							procesoBean.setAfiliacion(rs.getString(Constantes.TMP_DETALLE_AFILIACION)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_AFILIACION));
							procesoBean.setEmail(rs.getString(Constantes.TMP_DETALLE_EMAIL)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_EMAIL));
							procesoBean.setFactura(rs.getString(Constantes.TMP_DETALLE_FACTURA)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_FACTURA));
							procesoBean.setMontofactura(rs.getString(Constantes.TMP_DETALLE_MONTOFACTURA)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_MONTOFACTURA));
							procesoBean.setEmision(rs.getString(Constantes.TMP_DETALLE_EMISION)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_EMISION));
							procesoBean.setAmpliacion(rs.getString(Constantes.TMP_DETALLE_AMPLIACION)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_AMPLIACION));
							procesoBean.setBillcycle(rs.getString(Constantes.TMP_DETALLE_BILLCYCLE)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_BILLCYCLE));
							procesoBean.setFechainicio(rs.getString(Constantes.TMP_DETALLE_FECHAINICIO)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_FECHAINICIO));
							procesoBean.setFechafin(rs.getString(Constantes.TMP_DETALLE_FECHAFIN)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_FECHAFIN));
							procesoBean.setPlan(rs.getString(Constantes.TMP_DETALLE_PLAN)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_PLAN));
							procesoBean.setCuentacontable(rs.getString(Constantes.TMP_DETALLE_CUENTACONTABLE)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_CUENTACONTABLE));
							procesoBean.setCuentacontablesap(rs.getString(Constantes.TMP_DETALLE_CUENTACONTABLESAP)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_CUENTACONTABLESAP));
							procesoBean.setMontosinigv(rs.getString(Constantes.TMP_DETALLE_MONTOSINIGV)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_MONTOSINIGV));
							procesoBean.setMontoconigv(rs.getString(Constantes.TMP_DETALLE_MONTOCONIGV)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_MONTOCONIGV));
							procesoBean.setCentrobenef(rs.getString(Constantes.TMP_DETALLE_CENTROBENEF)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_CENTROBENEF));
							procesoBean.setEvento(rs.getString(Constantes.TMP_DETALLE_EVENTO)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DETALLE_EVENTO));
							procesoBean.setTmCode(rs.getString(Constantes.CAMPO_TMP_TMCODE)==null?Constantes.VACIO: rs.getString(Constantes.CAMPO_TMP_TMCODE));
							procesoBean.setRowid(rs.getString(Constantes.CAMPO_ROWID_DET));
							procesoBean.setLinea(rs.getString(Constantes.TMP_DET_LINEA)==null?Constantes.VACIO: rs.getString(Constantes.TMP_DET_LINEA));
							return procesoBean;
							}
						});
			SqlOutParameter outCodigoRespuesta = new SqlOutParameter(Constantes.CODIGORESPUESTA,
					OracleTypes.VARCHAR);
			SqlOutParameter outMensajeRespuesta = new SqlOutParameter(Constantes.MENSAJERESPUESTA,
					OracleTypes.VARCHAR);
			SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess()
					.withSchemaName(propertiesExterno.dbSiopdbOwner)
					.withCatalogName(propertiesExterno.dbSiopPkgAjusteMasivo)
					.withProcedureName(propertiesExterno.dbSiopListaTmp)
					.declareParameters(inId,inIdProceso,inDocReferencia,outCursorProcesos,outCodigoRespuesta, outMensajeRespuesta);
			logger.info(cadMensaje + Constantes.MENSAJE_EJECUCION_SP + "[" + propertiesExterno.dbSiopdbOwner + "."
					+ propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopListaTmp + "]");
			
			MapSqlParameterSource map = new MapSqlParameterSource();
			map.addValue(Constantes.PI_ID, bean.getId());
			map.addValue(Constantes.ENTRADA_PROCESO, bean.getIdProceso());
			map.addValue(Constantes.DOC_REFERENCIA, bean.getDocReferencia());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_ID+ "]:" + bean.getId());	
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.ENTRADA_PROCESO+ "]:" + bean.getIdProceso());	
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.DOC_REFERENCIA+ "]:" + bean.getDocReferencia());	
			Map<String, Object> result = procedureConsulta.execute(map);
			@SuppressWarnings("unchecked")
			List<DetalleTemporalBean> listaDatosAjusteCabecera = (List<DetalleTemporalBean>) result.get(Constantes.CURSORPROCESOS);

			String poCodRpta = (String) result.get(Constantes.CODIGORESPUESTA);

			String poMsjRpta = (String) result.get(Constantes.MENSAJERESPUESTA);
			
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_ID+ "]:" + bean.getId());	
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.ENTRADA_PROCESO+ "]:" + bean.getIdProceso());	
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.DOC_REFERENCIA+ "]:" + bean.getDocReferencia());	
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_COD_RPTA]:" + poCodRpta);
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_MSJ_RPTA]:" + poMsjRpta);


			response.setLista(listaDatosAjusteCabecera);
			response.setCodigoRespuesta(poCodRpta);
			response.setMensajeRespuesta(poMsjRpta);
			logger.info( cadMensaje.concat(" Datos obtenidos : \n ".concat(JAXBUtilitarios.anyObjectToXmlText(response))));
		} catch (Exception e) {
			String codigoError = "";
			codigoError = propertiesExterno.spIdt1Codigo;
			String msgError = "Hubo un error tecnico al ejecutar el PROCEDURE: ["
					+ propertiesExterno.dbSiopdbOwner + "." + propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopListaTmp + "] ";
			response.setCodigoRespuesta(codigoError);
			response.setMensajeRespuesta(msgError);
			logger.error(mensajeTransaccion + e.getMessage(), e);
			logger.error(mensajeTransaccion + msgError);
			throw new DBException(codigoError,msgError, e);	
		}finally {
			logger.info(cadMensaje + Utilitarios.msjFinProceso(metodoActualizaProceso, Constantes.DAO));
		}
		return response;
	}

	@Override
	public ActualizaMontoCabBscsBean actualizarMontoCab(ActualizaMontoCabBscsBean bean, String mensajeTransaccion)
			throws DBException {
		String metodoActualizaProceso = "actualizarMontoCab";

		String cadMensaje = mensajeTransaccion + "[" + metodoActualizaProceso + "] ";

		logger.info(cadMensaje + Utilitarios.msjInicioProceso(metodoActualizaProceso, Constantes.DAO));

		Utilitarios.infoConexion(mensajeTransaccion, propertiesExterno.dbSiopdbConexion, propertiesExterno.dbSiopdbNombre,
				propertiesExterno.dbSiopdbOwner, propertiesExterno.dbSiopPkgAjusteMasivo,
				propertiesExterno.dbSiopActualizaMontosCabBSCS);
		ActualizaMontoCabBscsBean response = null;
		
		try {
			logger.info(
					mensajeTransaccion + "Intento [" + metodoActualizaProceso + "] : ");
			response = new ActualizaMontoCabBscsBean();
			objJdbcCall = new SimpleJdbcCall(siopDS);
			objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			objJdbcTemplate.setQueryTimeout(propertiesExterno.DB_SIOP_CONNECTION_TIMEOUT);
			
			SqlParameter inId = new SqlParameter(Constantes.PI_ID, OracleTypes.INTEGER);
			SqlParameter intMontoSinIgv = new SqlParameter(Constantes.TMP_IN_MONTOSINIGV, OracleTypes.NUMBER);
			SqlParameter intMontoInafecto = new SqlParameter(Constantes.TMP_IN_MONTOINAFECTO, OracleTypes.NUMBER);
			SqlParameter intMontoConIgv = new SqlParameter(Constantes.TMP_IN_MONTOCONIGV, OracleTypes.NUMBER);
			SqlOutParameter outCodigoRespuesta = new SqlOutParameter(Constantes.CODIGORESPUESTA,
					OracleTypes.VARCHAR);
			SqlOutParameter outMensajeRespuesta = new SqlOutParameter(Constantes.MENSAJERESPUESTA,
					OracleTypes.VARCHAR);
			SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess()
					.withSchemaName(propertiesExterno.dbSiopdbOwner)
					.withCatalogName(propertiesExterno.dbSiopPkgAjusteMasivo)
					.withProcedureName(propertiesExterno.dbSiopActualizaMontosCabBSCS)
					.declareParameters(inId,intMontoSinIgv,intMontoInafecto,intMontoConIgv, outCodigoRespuesta, outMensajeRespuesta);
			logger.info(cadMensaje + Constantes.MENSAJE_EJECUCION_SP + "[" + propertiesExterno.dbSiopdbOwner + "."
					+ propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopActualizaMontosCabBSCS + "]");
			
			MapSqlParameterSource map = new MapSqlParameterSource();
			map.addValue(Constantes.PI_ID, bean.getId());
			map.addValue(Constantes.TMP_IN_MONTOSINIGV, bean.getMontoSinIgv());
			map.addValue(Constantes.TMP_IN_MONTOINAFECTO, bean.getMontoInafecto());
			map.addValue(Constantes.TMP_IN_MONTOCONIGV, bean.getMontoConIgv());
			Map<String, Object> result = procedureConsulta.execute(map);


			String poCodRpta = (String) result.get(Constantes.CODIGORESPUESTA);

			String poMsjRpta = (String) result.get(Constantes.MENSAJERESPUESTA);
			
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_ID+ "]:" + bean.getId());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.TMP_IN_MONTOSINIGV+ "]:" + bean.getMontoSinIgv());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.TMP_IN_MONTOINAFECTO+ "]:" + bean.getMontoInafecto());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.TMP_IN_MONTOCONIGV+ "]:" + bean.getMontoConIgv());
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_COD_RPTA]:" + poCodRpta);
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_MSJ_RPTA]:" + poMsjRpta);
		
			response.setCodigoRespuesta(poCodRpta);
			response.setMensajeRespuesta(poMsjRpta);
			logger.info( cadMensaje.concat(" Datos obtenidos : \n ".concat(JAXBUtilitarios.anyObjectToXmlText(response))));
			
		} catch (Exception e) {
			String codigoError = "";
			codigoError = propertiesExterno.spIdt1Codigo;
			String msgError = "Hubo un error tecnico al ejecutar el PROCEDURE: ["
					+ propertiesExterno.dbSiopdbOwner + "." + propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopActualizaMontosCabBSCS + "] ";
			response.setCodigoRespuesta(codigoError);
			response.setMensajeRespuesta(msgError);
			logger.error(mensajeTransaccion + e.getMessage(), e);
			logger.error(mensajeTransaccion + msgError);
			throw new DBException(codigoError,msgError, e);	
		}finally {
			logger.info(cadMensaje + Utilitarios.msjFinProceso(metodoActualizaProceso, Constantes.DAO));
		}
		return response;
	}

	@Override
	public ListaDatosAjusteSiop listarDetalleHistoricoDocAjuste(ListaDatosAjusteSiop bean, String mensajeTransaccion)
			throws DBException {
		String metodoActualizaProceso = "listarDetalleHistoricoDocAjuste";

		String cadMensaje = mensajeTransaccion + "[" + metodoActualizaProceso + "] ";

		logger.info(cadMensaje + Utilitarios.msjInicioProceso(metodoActualizaProceso, Constantes.DAO));

		Utilitarios.infoConexion(mensajeTransaccion, propertiesExterno.dbSiopdbConexion, propertiesExterno.dbSiopdbNombre,
				propertiesExterno.dbSiopdbOwner, propertiesExterno.dbSiopPkgAjusteMasivo,
				propertiesExterno.dbSiopListaHistoricoAjus);
		ListaDatosAjusteSiop response = null;
		
		try {
			logger.info(
					mensajeTransaccion + "Intento [" + metodoActualizaProceso + "] : ");
			response = new ListaDatosAjusteSiop();
			objJdbcCall = new SimpleJdbcCall(siopDS);
			objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			objJdbcTemplate.setQueryTimeout(propertiesExterno.DB_SIOP_CONNECTION_TIMEOUT);
			
			SqlParameter inDocAjuste = new SqlParameter(Constantes.PI_DOC_AJUSTE, OracleTypes.VARCHAR);
			SqlOutParameter outCodigoRespuesta = new SqlOutParameter(Constantes.CODIGORESPUESTA,
					OracleTypes.VARCHAR);
			SqlOutParameter outMensajeRespuesta = new SqlOutParameter(Constantes.MENSAJERESPUESTA,
					OracleTypes.VARCHAR);
			
			SqlOutParameter outCursorProcesos= new SqlOutParameter(Constantes.CURSORPROCESOS,
					OracleTypes.CURSOR,	new RowMapper<DatosAjusteSiop>() {
						public DatosAjusteSiop mapRow(ResultSet rs, int arg1) throws SQLException {
							DatosAjusteSiop procesoBean = new DatosAjusteSiop();
							procesoBean.setIdProceso(rs.getString(Constantes.CURSOR_DATOSSIOP_IDPROCESO)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_IDPROCESO));
							procesoBean.setCustomerId(rs.getString(Constantes.CURSOR_DATOSSIOP_CUSTOMERID)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_CUSTOMERID));
							procesoBean.setCustCode(rs.getString(Constantes.CURSOR_DATOSSIOP_CUSTCODE)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_CUSTCODE));
							procesoBean.setRuc(rs.getString(Constantes.CURSOR_DATOSSIOP_RUC)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_RUC));
							procesoBean.setRazonSocial(rs.getString(Constantes.CURSOR_DATOSSIOP_RAZONSOCIAL)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_RAZONSOCIAL));
							procesoBean.setContacto(rs.getString(Constantes.CURSOR_DATOSSIOP_CONTACTO)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_CONTACTO));
							procesoBean.setDireccion(rs.getString(Constantes.CURSOR_DATOSSIOP_DIRECCION)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_DIRECCION));
							procesoBean.setNota(rs.getString(Constantes.CURSOR_DATOSSIOP_NOTA)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_NOTA));
							procesoBean.setDepartamento(rs.getString(Constantes.CURSOR_DATOSSIOP_DEPARTAMENTO)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_DEPARTAMENTO));
							procesoBean.setProvincia(rs.getString(Constantes.CURSOR_DATOSSIOP_PROVINCIA)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_PROVINCIA));
							procesoBean.setDistrito(rs.getString(Constantes.CURSOR_DATOSSIOP_DISTRITO)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_DISTRITO));
							procesoBean.setTelefono(rs.getString(Constantes.CURSOR_DATOSSIOP_TELEFONO)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_TELEFONO));
							procesoBean.setAfiliacion(rs.getString(Constantes.CURSOR_DATOSSIOP_AFILIACION)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_AFILIACION));
							procesoBean.setEmail(rs.getString(Constantes.CURSOR_DATOSSIOP_EMAIL)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_EMAIL));
							procesoBean.setFactura(rs.getString(Constantes.CURSOR_DATOSSIOP_FACTURA)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_FACTURA));
							procesoBean.setMontoFactura(rs.getString(Constantes.CURSOR_DATOSSIOP_MONTOFACTURA)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_MONTOFACTURA));
							procesoBean.setIgvFactura(rs.getString(Constantes.CURSOR_DATOSSIOP_IGVFACTURA)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_IGVFACTURA));
							procesoBean.setIgvVigente(rs.getString(Constantes.CURSOR_DATOSSIOP_IGVVIGENTE)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_IGVVIGENTE));
							procesoBean.setEmision(rs.getString(Constantes.CURSOR_DATOSSIOP_EMISION)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_EMISION));
							procesoBean.setAmpliacion(rs.getString(Constantes.CURSOR_DATOSSIOP_AMPLIACION)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_AMPLIACION));
							procesoBean.setBillCycle(rs.getString(Constantes.CURSOR_DATOSSIOP_BILLCYCLE)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_BILLCYCLE));
							procesoBean.setInicioCiclo(rs.getString(Constantes.CURSOR_DATOSSIOP_INICIOCICLO)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_INICIOCICLO));
							procesoBean.setFinCiclo(rs.getString(Constantes.CURSOR_DATOSSIOP_FINCICLO)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_FINCICLO));
							procesoBean.setPlan(rs.getString(Constantes.CURSOR_DATOSSIOP_PLAN)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_PLAN));
							procesoBean.setDesPlan(rs.getString(Constantes.CURSOR_DATOSSIOP_DESPLAN)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_DESPLAN));
							procesoBean.setServ(rs.getString(Constantes.CURSOR_DATOSSIOP_SNCODE)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_SNCODE));
							procesoBean.setDesServ(rs.getString(Constantes.CURSOR_DATOSSIOP_DESSNCODE)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_DESSNCODE));
							procesoBean.setDesServ(rs.getString(Constantes.CURSOR_DATOSSIOP_DESSNCODE)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_DESSNCODE));
							procesoBean.setCtaPer(rs.getString(Constantes.CURSOR_DATOSSIOP_CTACONTABLE)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_CTACONTABLE));
							procesoBean.setCtaMex(rs.getString(Constantes.CURSOR_DATOSSIOP_CTACONTABLESAP)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_CTACONTABLESAP));
							procesoBean.setCtaMex(rs.getString(Constantes.CURSOR_DATOSSIOP_CTACONTABLESAP)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_CTACONTABLESAP));
							procesoBean.setMontoSinIgv(rs.getString(Constantes.CURSOR_DATOSSIOP_MTOIGV)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_MTOIGV));
							procesoBean.setAfectoIgv(rs.getString(Constantes.CURSOR_DATOSSIOP_AFECTOIGV)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_AFECTOIGV));
							procesoBean.setCentroBeneficio(rs.getString(Constantes.CURSOR_DATOSSIOP_CENTRO)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_CENTRO));
							procesoBean.setTipoAjuste(rs.getString(Constantes.CAMPO_HISTORICO_TIPO_AJUSTE)==null?Constantes.VACIO: rs.getString(Constantes.CAMPO_HISTORICO_TIPO_AJUSTE));
							procesoBean.setMontoConIgv(rs.getString(Constantes.CAMPO_HISTORICO_MONTO_IGV)==null?Constantes.VACIO: rs.getString(Constantes.CAMPO_HISTORICO_MONTO_IGV));
							procesoBean.setFechaEmision(rs.getString(Constantes.PO_FECHAEMI)==null?Constantes.VACIO: rs.getString(Constantes.PO_FECHAEMI));
							procesoBean.setFechaVencimiento(rs.getString(Constantes.PO_FECHAVEN)==null?Constantes.VACIO: rs.getString(Constantes.PO_FECHAVEN));
							return procesoBean;
							
							}
						});
			
			SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess()
					.withSchemaName(propertiesExterno.dbSiopdbOwner)
					.withCatalogName(propertiesExterno.dbSiopPkgAjusteMasivo)
					.withProcedureName(propertiesExterno.dbSiopListaHistoricoAjus)
					.declareParameters(inDocAjuste,outCursorProcesos, outCodigoRespuesta, outMensajeRespuesta);
			
			logger.info(cadMensaje + Constantes.MENSAJE_EJECUCION_SP + "[" + propertiesExterno.dbSiopdbOwner + "."
					+ propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopListaHistoricoAjus + "]");
			
			MapSqlParameterSource map = new MapSqlParameterSource();
			map.addValue(Constantes.PI_DOC_AJUSTE, bean.getDocAjuste());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_DOC_AJUSTE+ "]:" + bean.getDocAjuste());
			Map<String, Object> result = procedureConsulta.execute(map);
			@SuppressWarnings("unchecked")
			List<DatosAjusteSiop> listaDatosAjusteSiop = (List<DatosAjusteSiop>) result.get(Constantes.CURSORPROCESOS);

			String poCodRpta = (String) result.get(Constantes.CODIGORESPUESTA);

			String poMsjRpta = (String) result.get(Constantes.MENSAJERESPUESTA);
			
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_DOC_AJUSTE+ "]:" + bean.getDocAjuste());


			response.setListaDatosAjusteSiop(listaDatosAjusteSiop);
			response.setCodigoRespuesta(poCodRpta);
			response.setMensajeRespuesta(poMsjRpta);
			logger.info( cadMensaje.concat(" Datos obtenidos : \n ".concat(JAXBUtilitarios.anyObjectToXmlText(response))));
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_COD_RPTA]:" + poCodRpta);
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_MSJ_RPTA]:" + poMsjRpta);

		} catch (Exception e) {
			String codigoError = "";
			codigoError = propertiesExterno.spIdt1Codigo;
			String msgError = "Hubo un error tecnico al ejecutar el PROCEDURE: ["
					+ propertiesExterno.dbSiopdbOwner + "." + propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopListaHistoricoAjus + "] ";
			response.setCodigoRespuesta(codigoError);
			response.setMensajeRespuesta(msgError);
			logger.error(mensajeTransaccion + e.getMessage(), e);
			logger.error(mensajeTransaccion + msgError);
			throw new DBException(codigoError,msgError, e);	
		}finally {
			logger.info(cadMensaje + Utilitarios.msjFinProceso(metodoActualizaProceso, Constantes.DAO));
		}
		
		return response;
	}

	@Override
	public ActualizaEstadoBean actualizarEstadoProc(ActualizaEstadoBean bean, String mensajeTransaccion)
			throws DBException {
		String metodoActualizaProceso = "actualizarEstadoProc";

		String cadMensaje = mensajeTransaccion + "[" + metodoActualizaProceso + "] ";

		logger.info(cadMensaje + Utilitarios.msjInicioProceso(metodoActualizaProceso, Constantes.DAO));

		Utilitarios.infoConexion(mensajeTransaccion, propertiesExterno.dbSiopdbConexion, propertiesExterno.dbSiopdbNombre,
				propertiesExterno.dbSiopdbOwner, propertiesExterno.dbSiopPkgAjusteMasivo,
				propertiesExterno.dbSiopActEstadoProc);
		ActualizaEstadoBean response = null;	

		try {
			logger.info(
					mensajeTransaccion + "Intento [" + metodoActualizaProceso + "] : ");
			response = new ActualizaEstadoBean();
			objJdbcCall = new SimpleJdbcCall(siopDS);
			objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			objJdbcTemplate.setQueryTimeout(propertiesExterno.DB_SIOP_CONNECTION_TIMEOUT);
			
			SqlParameter inIdProceso = new SqlParameter(Constantes.ENTRADA_PROCESO, OracleTypes.INTEGER);
			SqlParameter inEstado = new SqlParameter(Constantes.COD_ESTADO, OracleTypes.VARCHAR);
			SqlOutParameter outCodigoRespuesta = new SqlOutParameter(Constantes.CODIGORESPUESTA,
					OracleTypes.VARCHAR);
			SqlOutParameter outMensajeRespuesta = new SqlOutParameter(Constantes.MENSAJERESPUESTA,
					OracleTypes.VARCHAR);
			SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess()
					.withSchemaName(propertiesExterno.dbSiopdbOwner)
					.withCatalogName(propertiesExterno.dbSiopPkgAjusteMasivo)
					.withProcedureName(propertiesExterno.dbSiopActEstadoProc)
					.declareParameters(inIdProceso,inEstado, outCodigoRespuesta, outMensajeRespuesta);
			
			logger.info(cadMensaje + Constantes.MENSAJE_EJECUCION_SP + "[" + propertiesExterno.dbSiopdbOwner + "."
					+ propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopActEstadoProc + "]");
			
			MapSqlParameterSource map = new MapSqlParameterSource();
			map.addValue(Constantes.ENTRADA_PROCESO, bean.getIdProceso());
			map.addValue(Constantes.COD_ESTADO, bean.getEstado());
			Map<String, Object> result = procedureConsulta.execute(map);


			String poCodRpta = (String) result.get(Constantes.CODIGORESPUESTA);

			String poMsjRpta = (String) result.get(Constantes.MENSAJERESPUESTA);
			
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.ENTRADA_PROCESO+ "]:" + bean.getIdProceso());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.COD_ESTADO+ "]:" + bean.getEstado());
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_COD_RPTA]:" + poCodRpta);
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_MSJ_RPTA]:" + poMsjRpta);
		
			response.setIdProceso(bean.getIdProceso());
			response.setEstado(bean.getEstado());
			response.setCodigoRespuesta(poCodRpta);
			response.setMensajeRespuesta(poMsjRpta);
			logger.info( cadMensaje.concat(" Datos obtenidos : \n ".concat(JAXBUtilitarios.anyObjectToXmlText(response))));

		} catch (Exception e) {
			String codigoError = "";
			codigoError = propertiesExterno.spIdt1Codigo;
			String msgError = "Hubo un error tecnico al ejecutar el PROCEDURE: ["
					+ propertiesExterno.dbSiopdbOwner + "." + propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopListarArchPlano + "] ";
			response.setCodigoRespuesta(codigoError);
			response.setMensajeRespuesta(msgError);
			logger.error(mensajeTransaccion + e.getMessage(), e);
			logger.error(mensajeTransaccion + msgError);
			throw new DBException(codigoError,msgError, e);	
		}finally {
			logger.info(cadMensaje + Utilitarios.msjFinProceso(metodoActualizaProceso, Constantes.DAO));
		}
		return response;
	}

	@Override
	public ListarDetalleArchPlanoBean listarDetalleBscsAjuste(ListarDetalleArchPlanoBean bean,
			String mensajeTransaccion) throws DBException {
		String metodoActualizaProceso = "listarDetalleBscsAjuste";

		String cadMensaje = mensajeTransaccion + "[" + metodoActualizaProceso + "] ";

		logger.info(cadMensaje + Utilitarios.msjInicioProceso(metodoActualizaProceso, Constantes.DAO));

		Utilitarios.infoConexion(mensajeTransaccion, propertiesExterno.dbSiopdbConexion, propertiesExterno.dbSiopdbNombre,
				propertiesExterno.dbSiopdbOwner, propertiesExterno.dbSiopPkgAjusteMasivo,
				propertiesExterno.dbSiopDetBscsAjus);
		ListarDetalleArchPlanoBean response = null;	

		try {
			logger.info(
					mensajeTransaccion + "Intento [" + metodoActualizaProceso + "] : ");
			response = new ListarDetalleArchPlanoBean();
			objJdbcCall = new SimpleJdbcCall(siopDS);
			objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			objJdbcTemplate.setQueryTimeout(propertiesExterno.DB_SIOP_CONNECTION_TIMEOUT);
			
			SqlParameter inTipoAjuste = new SqlParameter(Constantes.PARAM_BSCS_TIPOAJUSTE, OracleTypes.VARCHAR);
			SqlParameter inTmCode = new SqlParameter(Constantes.PARAM_BSCS_TMCODE, OracleTypes.VARCHAR);
			SqlParameter inSnCode = new SqlParameter(Constantes.PARAM_BSCS_SNCODE, OracleTypes.VARCHAR);
			SqlParameter inCtaCont = new SqlParameter(Constantes.PARAM_BSCS_CTACONT, OracleTypes.VARCHAR);
			SqlParameter inCustomerId = new SqlParameter(Constantes.PARAM_BSCS_CUSTOMERID, OracleTypes.VARCHAR);
			SqlParameter inDocReferencia = new SqlParameter(Constantes.PARAM_BSCS_DOCREFERENCIA, OracleTypes.VARCHAR);
			SqlParameter inIdProceso = new SqlParameter(Constantes.PARAM_BSCS_IDPROCESO, OracleTypes.VARCHAR);
			
			SqlOutParameter outCodigoRespuesta = new SqlOutParameter(Constantes.CODIGORESPUESTA,
					OracleTypes.VARCHAR);
			SqlOutParameter outMensajeRespuesta = new SqlOutParameter(Constantes.MENSAJERESPUESTA,
					OracleTypes.VARCHAR);
			SqlOutParameter outCursorProcesos= new SqlOutParameter(Constantes.CURSORPROCESOS,
					OracleTypes.CURSOR,	new RowMapper<DetalleArchPlanoBean>() {
						public DetalleArchPlanoBean mapRow(ResultSet rs, int arg1) throws SQLException {
							DetalleArchPlanoBean procesoBean = new DetalleArchPlanoBean();
							procesoBean.setIdproceso(rs.getString(Constantes.IDPROCESO)==null?Constantes.VACIO: rs.getString(Constantes.IDPROCESO));
//							procesoBean.setId(rs.getString(Constantes.ID)==null?Constantes.VACIO: rs.getString(Constantes.ID));
							procesoBean.setCustomerid(rs.getString(Constantes.CUSTOMERID)==null?Constantes.VACIO: rs.getString(Constantes.CUSTOMERID));
							procesoBean.setCustcode(rs.getString(Constantes.CUSTCODE)==null?Constantes.VACIO: rs.getString(Constantes.CUSTCODE));
							procesoBean.setRuc(rs.getString(Constantes.RUC)==null?Constantes.VACIO: rs.getString(Constantes.RUC));
							procesoBean.setRazonsocial(rs.getString(Constantes.RAZONSOCIAL)==null?Constantes.VACIO: rs.getString(Constantes.RAZONSOCIAL));
							procesoBean.setContacto(rs.getString(Constantes.CONTACTO)==null?Constantes.VACIO: rs.getString(Constantes.CONTACTO));
							procesoBean.setTipocliente(rs.getString(Constantes.TIPOCLIENTE)==null?Constantes.VACIO: rs.getString(Constantes.TIPOCLIENTE));
							procesoBean.setDireccion(rs.getString(Constantes.DIRECCION)==null?Constantes.VACIO: rs.getString(Constantes.DIRECCION));
							procesoBean.setNotadir(rs.getString(Constantes.NOTADIR)==null?Constantes.VACIO: rs.getString(Constantes.NOTADIR));
							procesoBean.setDepartamento(rs.getString(Constantes.DEPARTAMENTO)==null?Constantes.VACIO: rs.getString(Constantes.DEPARTAMENTO));
							procesoBean.setProvincia(rs.getString(Constantes.PROVINCIA)==null?Constantes.VACIO: rs.getString(Constantes.PROVINCIA));
							procesoBean.setDistrito(rs.getString(Constantes.DISTRITO)==null?Constantes.VACIO: rs.getString(Constantes.DISTRITO));
							procesoBean.setTelefono(rs.getString(Constantes.TELEFONO)==null?Constantes.VACIO: rs.getString(Constantes.TELEFONO));
							procesoBean.setAfiliacion(rs.getString(Constantes.AFILIACION)==null?Constantes.VACIO: rs.getString(Constantes.AFILIACION));
							procesoBean.setEmail(rs.getString(Constantes.EMAIL)==null?Constantes.VACIO: rs.getString(Constantes.EMAIL));
							procesoBean.setFactura(rs.getString(Constantes.FACTURA)==null?Constantes.VACIO: rs.getString(Constantes.FACTURA));
							procesoBean.setMontofactura(rs.getString(Constantes.MONTOFACTURA)==null?Constantes.VACIO: rs.getString(Constantes.MONTOFACTURA));
							procesoBean.setIgv(rs.getString(Constantes.IGV)==null?Constantes.VACIO: rs.getString(Constantes.IGV));
							procesoBean.setIgvvigente(rs.getString(Constantes.IGVVIGENTE)==null?Constantes.VACIO: rs.getString(Constantes.IGVVIGENTE));
							procesoBean.setEmision(rs.getString(Constantes.EMISION)==null?Constantes.VACIO: rs.getString(Constantes.EMISION));
							procesoBean.setAmpliacion(rs.getString(Constantes.AMPLIACION)==null?Constantes.VACIO: rs.getString(Constantes.AMPLIACION));
							procesoBean.setBillcycle(rs.getString(Constantes.BILLCYCLE)==null?Constantes.VACIO: rs.getString(Constantes.BILLCYCLE));
							procesoBean.setIniciociclo(rs.getString(Constantes.INICIOCICLO)==null?Constantes.VACIO: rs.getString(Constantes.INICIOCICLO));
							procesoBean.setFinciclo(rs.getString(Constantes.FINCICLO)==null?Constantes.VACIO: rs.getString(Constantes.FINCICLO));
							procesoBean.setPlan(rs.getString(Constantes.PLAN)==null?Constantes.VACIO: rs.getString(Constantes.PLAN));
							procesoBean.setDesplan(rs.getString(Constantes.DESPLAN)==null?Constantes.VACIO: rs.getString(Constantes.DESPLAN));
							procesoBean.setServ(rs.getString(Constantes.SERV)==null?Constantes.VACIO: rs.getString(Constantes.SERV));
							procesoBean.setDesserv(rs.getString(Constantes.DESSERV)==null?Constantes.VACIO: rs.getString(Constantes.DESSERV));
							procesoBean.setCtaper(rs.getString(Constantes.CTAPER)==null?Constantes.VACIO: rs.getString(Constantes.CTAPER));
							procesoBean.setCtamex(rs.getString(Constantes.CTAMEX)==null?Constantes.VACIO: rs.getString(Constantes.CTAMEX));
							procesoBean.setMontoserv(rs.getString(Constantes.MONTOSERV)==null?Constantes.VACIO: rs.getString(Constantes.MONTOSERV));
							procesoBean.setAfectoigv(rs.getString(Constantes.AFECTOIGV)==null?Constantes.VACIO: rs.getString(Constantes.AFECTOIGV));
							procesoBean.setCebe(rs.getString(Constantes.CEBE)==null?Constantes.VACIO: rs.getString(Constantes.CEBE));
							procesoBean.setFechaEmision(rs.getString(Constantes.EMISION)==null?Constantes.VACIO: rs.getString(Constantes.EMISION));
							procesoBean.setFechaVencimiento(rs.getString(Constantes.AMPLIACION)==null?Constantes.VACIO: rs.getString(Constantes.AMPLIACION));
							procesoBean.setCtaigv(rs.getString(Constantes.CTAIGV)==null?Constantes.VACIO: rs.getString(Constantes.CTAIGV));
							procesoBean.setPorcentaje(rs.getDouble(Constantes.PORCENTAJE));
							return procesoBean;
							}
						});
			SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess()
					.withSchemaName(propertiesExterno.dbSiopdbOwner)
					.withCatalogName(propertiesExterno.dbSiopPkgAjusteMasivo)
					.withProcedureName(propertiesExterno.dbSiopDetBscsAjus)
					.declareParameters(inTipoAjuste,inTmCode,inSnCode,inCtaCont,inCustomerId,inDocReferencia,inIdProceso,outCursorProcesos, outCodigoRespuesta, outMensajeRespuesta);
			
			logger.info(cadMensaje + Constantes.MENSAJE_EJECUCION_SP + "[" + propertiesExterno.dbSiopdbOwner + "."
					+ propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopDetBscsAjus + "]");
			
			MapSqlParameterSource map = new MapSqlParameterSource();
			map.addValue(Constantes.PARAM_BSCS_TIPOAJUSTE, bean.getTipoAjuste());
			map.addValue(Constantes.PARAM_BSCS_TMCODE, bean.getTmCode());
			map.addValue(Constantes.PARAM_BSCS_SNCODE, bean.getSnCode());
			map.addValue(Constantes.PARAM_BSCS_CTACONT, bean.getCuentaContable());
			map.addValue(Constantes.PARAM_BSCS_CUSTOMERID, bean.getCustomerId());
			map.addValue(Constantes.PARAM_BSCS_DOCREFERENCIA, bean.getDocReferencia());
			map.addValue(Constantes.PARAM_BSCS_IDPROCESO, bean.getIdProceso());
			Map<String, Object> result = procedureConsulta.execute(map);
			@SuppressWarnings("unchecked")
			List<DetalleArchPlanoBean> listaDatosAjusteCabecera = (List<DetalleArchPlanoBean>) result.get(Constantes.CURSORPROCESOS);	

			String poCodRpta = (String) result.get(Constantes.CODIGORESPUESTA);

			String poMsjRpta = (String) result.get(Constantes.MENSAJERESPUESTA);
			
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PARAM_BSCS_TIPOAJUSTE+ "]:" + bean.getTipoAjuste());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PARAM_BSCS_TMCODE+ "]:" + bean.getTmCode());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PARAM_BSCS_SNCODE+ "]:" + bean.getSnCode());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PARAM_BSCS_CTACONT+ "]:" + bean.getCuentaContable());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PARAM_BSCS_CUSTOMERID+ "]:" + bean.getCustomerId());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PARAM_BSCS_DOCREFERENCIA+ "]:" + bean.getDocReferencia());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PARAM_BSCS_IDPROCESO+ "]:" + bean.getIdProceso());
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_COD_RPTA]:" + poCodRpta);
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_MSJ_RPTA]:" + poMsjRpta);
			

			response.setListaDetalle(listaDatosAjusteCabecera);
			response.setCodigoRespuesta(poCodRpta);
			response.setMensajeRespuesta(poMsjRpta);
			logger.info( cadMensaje.concat(" Datos obtenidos : \n ".concat(JAXBUtilitarios.anyObjectToXmlText(response))));
		} catch (Exception e) {
			String codigoError = "";
			codigoError = propertiesExterno.spIdt1Codigo;
			String msgError = "Hubo un error tecnico al ejecutar el PROCEDURE: ["
					+ propertiesExterno.dbSiopdbOwner + "." + propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopDetBscsAjus + "] ";
			response.setCodigoRespuesta(codigoError);
			response.setMensajeRespuesta(msgError);
			logger.error(mensajeTransaccion + e.getMessage(), e);
			logger.error(mensajeTransaccion + msgError);
			throw new DBException(codigoError,msgError, e);	
		}finally {
			logger.info(cadMensaje + Utilitarios.msjFinProceso(metodoActualizaProceso, Constantes.DAO));
		}
		return response;
	}

	@Override
	public ListaDatosAjusteSiop listarDetalleHistoricoSiop(ListarDetalleArchPlanoBean bean, String mensajeTransaccion) throws DBException {
		String metodoActualizaProceso = "listarDetalleHistoricoSiop";

		String cadMensaje = mensajeTransaccion + "[" + metodoActualizaProceso + "] ";

		logger.info(cadMensaje + Utilitarios.msjInicioProceso(metodoActualizaProceso, Constantes.DAO));

		Utilitarios.infoConexion(mensajeTransaccion, propertiesExterno.dbSiopdbConexion, propertiesExterno.dbSiopdbNombre,
				propertiesExterno.dbSiopdbOwner, propertiesExterno.dbSiopPkgAjusteMasivo,
				propertiesExterno.dbSiopDetHistSiop);
		ListaDatosAjusteSiop response = null;	

		try {
			logger.info(
					mensajeTransaccion + "Intento [" + metodoActualizaProceso + "] : ");
			response = new ListaDatosAjusteSiop();
			objJdbcCall = new SimpleJdbcCall(siopDS);
			objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			objJdbcTemplate.setQueryTimeout(propertiesExterno.DB_SIOP_CONNECTION_TIMEOUT);
			
			SqlParameter inTipoAjuste = new SqlParameter(Constantes.PARAM_BSCS_TIPOAJUSTE, OracleTypes.VARCHAR);
			SqlParameter inTmCode = new SqlParameter(Constantes.PARAM_BSCS_TMCODE, OracleTypes.VARCHAR);
			SqlParameter inSnCode = new SqlParameter(Constantes.PARAM_BSCS_SNCODE, OracleTypes.VARCHAR);
			SqlParameter inCtaCont = new SqlParameter(Constantes.PARAM_BSCS_CTACONT, OracleTypes.VARCHAR);
			SqlParameter inCustomerId = new SqlParameter(Constantes.PARAM_BSCS_CUSTOMERID, OracleTypes.VARCHAR);
			SqlParameter inDocReferencia = new SqlParameter(Constantes.PARAM_BSCS_DOCREFERENCIA, OracleTypes.VARCHAR);
			SqlParameter inIdProceso = new SqlParameter(Constantes.PARAM_BSCS_IDPROCESO, OracleTypes.VARCHAR);
			
			SqlOutParameter outCodigoRespuesta = new SqlOutParameter(Constantes.CODIGORESPUESTA,
					OracleTypes.VARCHAR);
			SqlOutParameter outMensajeRespuesta = new SqlOutParameter(Constantes.MENSAJERESPUESTA,
					OracleTypes.VARCHAR);
			SqlOutParameter outCursorProcesos= new SqlOutParameter(Constantes.CURSORPROCESOS,
					OracleTypes.CURSOR,	new RowMapper<DatosAjusteSiop>() {
						public DatosAjusteSiop mapRow(ResultSet rs, int arg1) throws SQLException {
							DatosAjusteSiop procesoBean = new DatosAjusteSiop();
							procesoBean.setIdProceso(rs.getString(Constantes.CURSOR_DATOSSIOP_IDPROCESO)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_IDPROCESO));
							procesoBean.setCustomerId(rs.getString(Constantes.CURSOR_DATOSSIOP_CUSTOMERID)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_CUSTOMERID));
							procesoBean.setCustCode(rs.getString(Constantes.CURSOR_DATOSSIOP_CUSTCODE)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_CUSTCODE));
							procesoBean.setRuc(rs.getString(Constantes.CURSOR_DATOSSIOP_RUC)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_RUC));
							procesoBean.setRazonSocial(rs.getString(Constantes.CURSOR_DATOSSIOP_RAZONSOCIAL)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_RAZONSOCIAL));
							procesoBean.setContacto(rs.getString(Constantes.CURSOR_DATOSSIOP_CONTACTO)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_CONTACTO));
							procesoBean.setDireccion(rs.getString(Constantes.CURSOR_DATOSSIOP_DIRECCION)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_DIRECCION));
							procesoBean.setNota(rs.getString(Constantes.CURSOR_DATOSSIOP_NOTA)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_NOTA));
							procesoBean.setDepartamento(rs.getString(Constantes.CURSOR_DATOSSIOP_DEPARTAMENTO)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_DEPARTAMENTO));
							procesoBean.setProvincia(rs.getString(Constantes.CURSOR_DATOSSIOP_PROVINCIA)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_PROVINCIA));
							procesoBean.setDistrito(rs.getString(Constantes.CURSOR_DATOSSIOP_DISTRITO)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_DISTRITO));
							procesoBean.setTelefono(rs.getString(Constantes.CURSOR_DATOSSIOP_TELEFONO)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_TELEFONO));
							procesoBean.setAfiliacion(rs.getString(Constantes.CURSOR_DATOSSIOP_AFILIACION)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_AFILIACION));
							procesoBean.setEmail(rs.getString(Constantes.CURSOR_DATOSSIOP_EMAIL)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_EMAIL));
							procesoBean.setFactura(rs.getString(Constantes.CURSOR_DATOSSIOP_FACTURA)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_FACTURA));
							procesoBean.setMontoFactura(rs.getString(Constantes.CURSOR_DATOSSIOP_MONTOFACTURA)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_MONTOFACTURA));
							procesoBean.setIgvFactura(rs.getString(Constantes.CURSOR_DATOSSIOP_IGVFACTURA)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_IGVFACTURA));
							procesoBean.setIgvVigente(rs.getString(Constantes.CURSOR_DATOSSIOP_IGVVIGENTE)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_IGVVIGENTE));
							procesoBean.setEmision(rs.getString(Constantes.CURSOR_DATOSSIOP_EMISION)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_EMISION));
							procesoBean.setAmpliacion(rs.getString(Constantes.CURSOR_DATOSSIOP_AMPLIACION)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_AMPLIACION));
							procesoBean.setBillCycle(rs.getString(Constantes.CURSOR_DATOSSIOP_BILLCYCLE)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_BILLCYCLE));
							procesoBean.setInicioCiclo(rs.getString(Constantes.CURSOR_DATOSSIOP_INICIOCICLO)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_INICIOCICLO));
							procesoBean.setFinCiclo(rs.getString(Constantes.CURSOR_DATOSSIOP_FINCICLO)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_FINCICLO));
							procesoBean.setPlan(rs.getString(Constantes.CURSOR_DATOSSIOP_PLAN)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_PLAN));
							procesoBean.setDesPlan(rs.getString(Constantes.CURSOR_DATOSSIOP_DESPLAN)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_DESPLAN));
							procesoBean.setServ(rs.getString(Constantes.CURSOR_DATOSSIOP_SNCODE)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_SNCODE));
							procesoBean.setDesServ(rs.getString(Constantes.CURSOR_DATOSSIOP_DESSNCODE)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_DESSNCODE));
							procesoBean.setDesServ(rs.getString(Constantes.CURSOR_DATOSSIOP_DESSNCODE)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_DESSNCODE));
							procesoBean.setCtaPer(rs.getString(Constantes.CURSOR_DATOSSIOP_CTACONTABLE)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_CTACONTABLE));
							procesoBean.setCtaMex(rs.getString(Constantes.CURSOR_DATOSSIOP_CTACONTABLESAP)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_CTACONTABLESAP));
							procesoBean.setCtaMex(rs.getString(Constantes.CURSOR_DATOSSIOP_CTACONTABLESAP)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_CTACONTABLESAP));
							procesoBean.setMontoSinIgv(rs.getString(Constantes.CURSOR_DATOSSIOP_MTOIGV)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_MTOIGV));
							procesoBean.setAfectoIgv(rs.getString(Constantes.CURSOR_DATOSSIOP_AFECTOIGV)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_AFECTOIGV));
							procesoBean.setCentroBeneficio(rs.getString(Constantes.CURSOR_DATOSSIOP_CENTRO)==null?Constantes.VACIO: rs.getString(Constantes.CURSOR_DATOSSIOP_CENTRO));
							procesoBean.setTipoAjuste(rs.getString(Constantes.CAMPO_HISTORICO_TIPO_AJUSTE)==null?Constantes.VACIO: rs.getString(Constantes.CAMPO_HISTORICO_TIPO_AJUSTE));
							procesoBean.setMontoConIgv(rs.getString(Constantes.CAMPO_HISTORICO_MONTO_IGV)==null?Constantes.VACIO: rs.getString(Constantes.CAMPO_HISTORICO_MONTO_IGV));
							procesoBean.setFechaEmision(rs.getString(Constantes.PO_FECHAEMI)==null?Constantes.VACIO: rs.getString(Constantes.PO_FECHAEMI));
							procesoBean.setFechaVencimiento(rs.getString(Constantes.PO_FECHAVEN)==null?Constantes.VACIO: rs.getString(Constantes.PO_FECHAVEN));
							procesoBean.setCtaIgv(rs.getString(Constantes.PO_CTAIGV)==null?Constantes.VACIO: rs.getString(Constantes.PO_CTAIGV));
							procesoBean.setPorcentaje(rs.getDouble(Constantes.PO_PORCENTAJE));
							procesoBean.setTmcode(rs.getString(Constantes.PO_TMCODE)==null?Constantes.VACIO: rs.getString(Constantes.PO_TMCODE));
							return procesoBean;
							
							}
						});
			SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess()
					.withSchemaName(propertiesExterno.dbSiopdbOwner)
					.withCatalogName(propertiesExterno.dbSiopPkgAjusteMasivo)
					.withProcedureName(propertiesExterno.dbSiopDetHistSiop)
					.declareParameters(inTipoAjuste,inTmCode,inSnCode,inCtaCont,inCustomerId,inDocReferencia,inIdProceso,outCursorProcesos, outCodigoRespuesta, outMensajeRespuesta);
			
			logger.info(cadMensaje + Constantes.MENSAJE_EJECUCION_SP + "[" + propertiesExterno.dbSiopdbOwner + "."
					+ propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopDetHistSiop + "]");
			
			MapSqlParameterSource map = new MapSqlParameterSource();
			map.addValue(Constantes.PARAM_BSCS_TIPOAJUSTE, bean.getTipoAjuste());
			map.addValue(Constantes.PARAM_BSCS_TMCODE, bean.getTmCode());
			map.addValue(Constantes.PARAM_BSCS_SNCODE, bean.getSnCode());
			map.addValue(Constantes.PARAM_BSCS_CTACONT, bean.getCuentaContable());
			map.addValue(Constantes.PARAM_BSCS_CUSTOMERID, bean.getCustomerId());
			map.addValue(Constantes.PARAM_BSCS_DOCREFERENCIA, bean.getDocReferencia());
			map.addValue(Constantes.PARAM_BSCS_IDPROCESO, bean.getIdProceso());
			Map<String, Object> result = procedureConsulta.execute(map);
			@SuppressWarnings("unchecked")
			List<DatosAjusteSiop> listaDatosAjusteCabecera = (List<DatosAjusteSiop>) result.get(Constantes.CURSORPROCESOS);	

			String poCodRpta = (String) result.get(Constantes.CODIGORESPUESTA);

			String poMsjRpta = (String) result.get(Constantes.MENSAJERESPUESTA);
			
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PARAM_BSCS_TIPOAJUSTE+ "]:" + bean.getTipoAjuste());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PARAM_BSCS_TMCODE+ "]:" + bean.getTmCode());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PARAM_BSCS_SNCODE+ "]:" + bean.getSnCode());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PARAM_BSCS_CTACONT+ "]:" + bean.getCuentaContable());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PARAM_BSCS_CUSTOMERID+ "]:" + bean.getCustomerId());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PARAM_BSCS_DOCREFERENCIA+ "]:" + bean.getDocReferencia());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PARAM_BSCS_IDPROCESO+ "]:" + bean.getIdProceso());
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_COD_RPTA]:" + poCodRpta);
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_MSJ_RPTA]:" + poMsjRpta);
			

			response.setListaDatosAjusteSiop(listaDatosAjusteCabecera);
			response.setCodigoRespuesta(poCodRpta);
			response.setMensajeRespuesta(poMsjRpta);
			logger.info( cadMensaje.concat(" Datos obtenidos : \n ".concat(JAXBUtilitarios.anyObjectToXmlText(response))));
		} catch (Exception e) {
			String codigoError = "";
			codigoError = propertiesExterno.spIdt1Codigo;
			String msgError = "Hubo un error tecnico al ejecutar el PROCEDURE: ["
					+ propertiesExterno.dbSiopdbOwner + "." + propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopDetHistSiop + "] ";
			response.setCodigoRespuesta(codigoError);
			response.setMensajeRespuesta(msgError);
			logger.error(mensajeTransaccion + e.getMessage(), e);
			logger.error(mensajeTransaccion + msgError);
			throw new DBException(codigoError,msgError, e);	
		}finally {
			logger.info(cadMensaje + Utilitarios.msjFinProceso(metodoActualizaProceso, Constantes.DAO));
		}
		return response;
	}

	@Override
	public ResponseBase completaDatosCabecera(DetalleArchPlanoBean bean, String mensajeTransaccion, Integer id)
			throws DBException {
		String metodoActualizaProceso = "completaDatosCabecera";

		String cadMensaje = mensajeTransaccion + "[" + metodoActualizaProceso + "] ";

		logger.info(cadMensaje + Utilitarios.msjInicioProceso(metodoActualizaProceso, Constantes.DAO));

		Utilitarios.infoConexion(mensajeTransaccion, propertiesExterno.dbSiopdbConexion, propertiesExterno.dbSiopdbNombre,
				propertiesExterno.dbSiopdbOwner, propertiesExterno.dbSiopPkgAjusteMasivo,
				propertiesExterno.dbSiopCompletCabecera);
		
		ResponseBase response = null;
		try {
			response = new ResponseBase();
			objJdbcCall = new SimpleJdbcCall(siopDS);
			objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			objJdbcTemplate.setQueryTimeout(propertiesExterno.DB_SIOP_CONNECTION_TIMEOUT);
			SqlParameter inTelefono = new SqlParameter( Constantes.PI_TELEFONO, OracleTypes.VARCHAR);
			SqlParameter inIniciociclo = new SqlParameter( Constantes.PI_INICIOCICLO, OracleTypes.VARCHAR);
			SqlParameter inFinciclo = new SqlParameter( Constantes.PI_FINCICLO, OracleTypes.VARCHAR);
			SqlParameter inCustcode = new SqlParameter( Constantes.PI_CUSTCODE, OracleTypes.VARCHAR);
			SqlParameter inRuc = new SqlParameter( Constantes.PI_RUC, OracleTypes.VARCHAR);
			SqlParameter inRazonsocial = new SqlParameter( Constantes.PI_RAZONSOCIAL, OracleTypes.VARCHAR);
			SqlParameter inContacto = new SqlParameter( Constantes.PI_CONTACTO, OracleTypes.VARCHAR);
			SqlParameter inTipocliente = new SqlParameter( Constantes.PI_TIPOCLIENTE, OracleTypes.VARCHAR);
			SqlParameter inDireccion = new SqlParameter( Constantes.PI_DIRECCION, OracleTypes.VARCHAR);
			SqlParameter inNotadir = new SqlParameter( Constantes.PI_NOTADIR, OracleTypes.VARCHAR);
			SqlParameter inDepartamento = new SqlParameter( Constantes.PI_DEPARTAMENTO, OracleTypes.VARCHAR);
			SqlParameter inProvincia = new SqlParameter( Constantes.PI_PROVINCIA, OracleTypes.VARCHAR);
			SqlParameter inDistrito = new SqlParameter( Constantes.PI_DISTRITO, OracleTypes.VARCHAR);
			SqlParameter inAfiliacion = new SqlParameter( Constantes.PI_AFILIACION, OracleTypes.VARCHAR);
			SqlParameter inEmail = new SqlParameter( Constantes.PI_EMAIL, OracleTypes.VARCHAR);
			SqlParameter inFactura = new SqlParameter( Constantes.PI_FACTURA, OracleTypes.VARCHAR);
			SqlParameter inEmision = new SqlParameter( Constantes.PI_EMISION, OracleTypes.VARCHAR);
			SqlParameter inAmpliacion = new SqlParameter( Constantes.PI_AMPLIACION, OracleTypes.VARCHAR);
			SqlParameter inBillcycle = new SqlParameter( Constantes.PI_BILLCYCLE, OracleTypes.VARCHAR);
			SqlParameter inMontofactura = new SqlParameter( Constantes.PI_MONTOFACTURA, OracleTypes.NUMBER);
			SqlParameter inIdProceso = new SqlParameter( Constantes.PI_IIDPROCESO, OracleTypes.VARCHAR);
			SqlParameter inFechaEmision = new SqlParameter( Constantes.PI_FECHAEMISION, OracleTypes.VARCHAR);
			SqlParameter inFechaVenc = new SqlParameter( Constantes.PI_FECHAVENC, OracleTypes.VARCHAR);
			SqlParameter inId = new SqlParameter( Constantes.PI_IID, OracleTypes.VARCHAR);
			SqlOutParameter outCodigoRespuesta = new SqlOutParameter(Constantes.CODIGORESPUESTA,
					OracleTypes.VARCHAR);
			SqlOutParameter outMensajeRespuesta = new SqlOutParameter(Constantes.MENSAJERESPUESTA,
					OracleTypes.VARCHAR);
			SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess()
					.withSchemaName(propertiesExterno.dbSiopdbOwner)
					.withCatalogName(propertiesExterno.dbSiopPkgAjusteMasivo)
					.withProcedureName(propertiesExterno.dbSiopCompletCabecera)
					.declareParameters(inTelefono,
							inIniciociclo,
							inFinciclo,
							inCustcode,
							inRuc,
							inRazonsocial,
							inContacto,
							inTipocliente,
							inDireccion,
							inNotadir,
							inDepartamento,
							inProvincia,
							inDistrito,
							inAfiliacion,
							inEmail,
							inFactura,
							inEmision,
							inAmpliacion,
							inBillcycle,
							inMontofactura,
							inIdProceso,inFechaEmision,inFechaVenc,inId
							,outCodigoRespuesta, outMensajeRespuesta);
			logger.info(cadMensaje + Constantes.MENSAJE_EJECUCION_SP + "[" + propertiesExterno.dbSiopdbOwner + "."
					+ propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopCompletCabecera + "]");
			
			MapSqlParameterSource map = new MapSqlParameterSource();
			map.addValue(Constantes.PI_TELEFONO, bean.getTelefono());
			map.addValue(Constantes.PI_INICIOCICLO, bean.getIniciociclo());
			map.addValue(Constantes.PI_FINCICLO, bean.getFinciclo());
			map.addValue(Constantes.PI_CUSTCODE, bean.getCustcode());
			map.addValue(Constantes.PI_RUC, bean.getRuc());
			map.addValue(Constantes.PI_RAZONSOCIAL, bean.getRazonsocial());
			map.addValue(Constantes.PI_CONTACTO, bean.getContacto());
			map.addValue(Constantes.PI_TIPOCLIENTE, bean.getTipocliente());
			map.addValue(Constantes.PI_DIRECCION, bean.getDireccion());
			map.addValue(Constantes.PI_NOTADIR, bean.getNotadir());
			map.addValue(Constantes.PI_DEPARTAMENTO, bean.getDepartamento());
			map.addValue(Constantes.PI_PROVINCIA, bean.getProvincia());
			map.addValue(Constantes.PI_DISTRITO, bean.getDistrito());
			map.addValue(Constantes.PI_AFILIACION, bean.getAfiliacion());
			map.addValue(Constantes.PI_EMAIL, bean.getEmail());
			map.addValue(Constantes.PI_FACTURA, bean.getFactura());
			map.addValue(Constantes.PI_EMISION, bean.getEmision());
			map.addValue(Constantes.PI_AMPLIACION, bean.getAmpliacion());
			map.addValue(Constantes.PI_BILLCYCLE, bean.getBillcycle());
			map.addValue(Constantes.PI_MONTOFACTURA, Double.parseDouble(bean.getMontofactura()));
			map.addValue(Constantes.PI_IIDPROCESO, bean.getIdproceso());
			map.addValue(Constantes.PI_FECHAEMISION, bean.getFechaEmision());
			map.addValue(Constantes.PI_FECHAVENC, bean.getFechaVencimiento());
			map.addValue(Constantes.PI_IID, id);
			Map<String, Object> result = procedureConsulta.execute(map);


			String poCodRpta = (String) result.get(Constantes.CODIGORESPUESTA);

			String poMsjRpta = (String) result.get(Constantes.MENSAJERESPUESTA);
			
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_TELEFONO     + "]:" + bean.getTelefono());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_INICIOCICLO + "]:" + bean.getIniciociclo());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_FINCICLO + "]:" + bean.getFinciclo());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_CUSTCODE + "]:" + bean.getCustcode());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_RUC + "]:" + bean.getRuc());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_RAZONSOCIAL + "]:" + bean.getRazonsocial());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_CONTACTO + "]:" + bean.getContacto());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_TIPOCLIENTE + "]:" + bean.getTipocliente());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_DIRECCION + "]:" + bean.getDireccion());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_NOTADIR + "]:" + bean.getNotadir());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_DEPARTAMENTO + "]:" + bean.getDepartamento());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_PROVINCIA + "]:" + bean.getProvincia());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_DISTRITO + "]:" + bean.getDistrito());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_AFILIACION + "]:" + bean.getAfiliacion());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_EMAIL + "]:" + bean.getEmail());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_FACTURA + "]:" + bean.getFactura());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_EMISION + "]:" + bean.getEmision());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_AMPLIACION + "]:" + bean.getAmpliacion());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_BILLCYCLE + "]:" + bean.getBillcycle());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_MONTOFACTURA + "]:" + bean.getMontofactura());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_IIDPROCESO + "]:" + bean.getIdproceso());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_FECHAEMISION + "]:" + bean.getFechaEmision());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_FECHAVENC + "]:" + bean.getFechaVencimiento());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_IID + "]:" + id);
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_COD_RPTA]:" + poCodRpta);
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_MSJ_RPTA]:" + poMsjRpta);
		
			response.setCodigoRespuesta(poCodRpta);
			response.setMensajeRespuesta(poMsjRpta);
			logger.info( cadMensaje.concat(" Datos obtenidos : \n ".concat(JAXBUtilitarios.anyObjectToXmlText(response))));

		} catch (Exception e) {
			String codigoError = "";
			codigoError = propertiesExterno.spIdt1Codigo;
			String msgError = "Hubo un error tecnico al ejecutar el PROCEDURE: ["
					+ propertiesExterno.dbSiopdbOwner + "." + propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopCompletCabecera + "] ";
			response.setCodigoRespuesta(codigoError);
			response.setMensajeRespuesta(msgError);
			logger.error(mensajeTransaccion + e.getMessage(), e);
			logger.error(mensajeTransaccion + msgError);
			throw new DBException(codigoError,msgError, e);	
		}finally {
			logger.info(cadMensaje + Utilitarios.msjFinProceso(metodoActualizaProceso, Constantes.DAO));
		}
		return response;
	}

	@Override
	public ObtieneRubroBean obtenerRubro(ObtieneRubroBean bean, String mensajeTransaccion) throws DBException {
		String metodoActualizaProceso = "obtenerRubro";

		String cadMensaje = mensajeTransaccion + "[" + metodoActualizaProceso + "] ";

		logger.info(cadMensaje + Utilitarios.msjInicioProceso(metodoActualizaProceso, Constantes.DAO));

		Utilitarios.infoConexion(mensajeTransaccion, propertiesExterno.dbSiopdbConexion, propertiesExterno.dbSiopdbNombre,
				propertiesExterno.dbSiopdbOwner, propertiesExterno.dbSiopPkgAjusteMasivo,
				propertiesExterno.dbSiopObtRubro);
		ObtieneRubroBean response = null;
		try {
			logger.info(
					mensajeTransaccion + "Intento [" + metodoActualizaProceso + "] : ");
			response = new ObtieneRubroBean();
			objJdbcCall = new SimpleJdbcCall(siopDS);
			objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			objJdbcTemplate.setQueryTimeout(propertiesExterno.DB_SIOP_CONNECTION_TIMEOUT);
			SqlParameter inTipoAjuste = new SqlParameter(Constantes.PIRUB_TIPOAJUSTE, OracleTypes.VARCHAR);
			SqlParameter inSnCode = new SqlParameter(Constantes.PIRUB_SNCODE, OracleTypes.VARCHAR);
			SqlOutParameter outCodigoRespuesta = new SqlOutParameter(Constantes.CODIGORESPUESTA,
					OracleTypes.VARCHAR);
			SqlOutParameter outMensajeRespuesta = new SqlOutParameter(Constantes.MENSAJERESPUESTA,
					OracleTypes.VARCHAR);
			
			SqlOutParameter outCursorProcesos= new SqlOutParameter(Constantes.CURSORPROCESOS,
					OracleTypes.CURSOR,	new RowMapper<RubroBean>() {
						public RubroBean mapRow(ResultSet rs, int arg1) throws SQLException {
							RubroBean procesoBean = new RubroBean();
							procesoBean.setNombreRubro(rs.getString(Constantes.CUR_NOMBRERUBRO)==null?Constantes.VACIO: rs.getString(Constantes.CUR_NOMBRERUBRO));
							procesoBean.setConceptoRubro(rs.getString(Constantes.CUR_CONCEPTORUBRO)==null?Constantes.VACIO: rs.getString(Constantes.CUR_CONCEPTORUBRO));


							return procesoBean;
							
							}
						});
			
			SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess()
					.withSchemaName(propertiesExterno.dbSiopdbOwner)
					.withCatalogName(propertiesExterno.dbSiopPkgAjusteMasivo)
					.withProcedureName(propertiesExterno.dbSiopObtRubro)
					.declareParameters(inTipoAjuste,inSnCode,outCursorProcesos, outCodigoRespuesta, outMensajeRespuesta);
			
			logger.info(cadMensaje + Constantes.MENSAJE_EJECUCION_SP + "[" + propertiesExterno.dbSiopdbOwner + "."
					+ propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopObtRubro + "]");
			
			MapSqlParameterSource map = new MapSqlParameterSource();
			map.addValue(Constantes.PIRUB_TIPOAJUSTE, bean.getTipoAjuste());
			map.addValue(Constantes.PIRUB_SNCODE, bean.getSnCode());
			Map<String, Object> result = procedureConsulta.execute(map);
			@SuppressWarnings("unchecked")
			List<RubroBean> listaDatosAjusteSiop = (List<RubroBean>) result.get(Constantes.CURSORPROCESOS);

			String poCodRpta = (String) result.get(Constantes.CODIGORESPUESTA);

			String poMsjRpta = (String) result.get(Constantes.MENSAJERESPUESTA);
			
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PIRUB_TIPOAJUSTE+ "]:" + bean.getTipoAjuste());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PIRUB_SNCODE+ "]:" + bean.getSnCode());


			response.setLista(listaDatosAjusteSiop);
			response.setCodigoError(poCodRpta);
			response.setMensajeError(poMsjRpta);
			logger.info( cadMensaje.concat(" Datos obtenidos : \n ".concat(JAXBUtilitarios.anyObjectToXmlText(response))));
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_COD_RPTA]:" + poCodRpta);
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_MSJ_RPTA]:" + poMsjRpta);			
		} catch (Exception e) {
			String codigoError = "";
			codigoError = propertiesExterno.spIdt1Codigo;
			String msgError = "Hubo un error tecnico al ejecutar el PROCEDURE: ["
					+ propertiesExterno.dbSiopdbOwner + "." + propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopObtRubro + "] ";
			response.setCodigoError(codigoError);
			response.setMensajeError(msgError);
			logger.error(mensajeTransaccion + e.getMessage(), e);
			logger.error(mensajeTransaccion + msgError);
			throw new DBException(codigoError,msgError, e);	
		}finally {
			logger.info(cadMensaje + Utilitarios.msjFinProceso(metodoActualizaProceso, Constantes.DAO));

		}
		return response;
	}

	@Override
	public ActualizaMtoCabBscsBean actualizarMtoIgv(ActualizaMtoCabBscsBean bean, String mensajeTransaccion)
			throws DBException {
		String metodoActualizaProceso = "actualizarMtoIgv";

		String cadMensaje = mensajeTransaccion + "[" + metodoActualizaProceso + "] ";

		logger.info(cadMensaje + Utilitarios.msjInicioProceso(metodoActualizaProceso, Constantes.DAO));

		Utilitarios.infoConexion(mensajeTransaccion, propertiesExterno.dbSiopdbConexion, propertiesExterno.dbSiopdbNombre,
				propertiesExterno.dbSiopdbOwner, propertiesExterno.dbSiopPkgAjusteMasivo,
				propertiesExterno.dbSiopActMtoCab);
		ActualizaMtoCabBscsBean response = null;
		try {
			logger.info(
					mensajeTransaccion + "Intento [" + metodoActualizaProceso + "] : ");
			response = new ActualizaMtoCabBscsBean();
			objJdbcCall = new SimpleJdbcCall(siopDS);
			objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			objJdbcTemplate.setQueryTimeout(propertiesExterno.DB_SIOP_CONNECTION_TIMEOUT);
			
			SqlParameter inTmCode = new SqlParameter(Constantes.TMCODE, OracleTypes.VARCHAR);
			SqlParameter inSnCode = new SqlParameter(Constantes.SNCODE, OracleTypes.VARCHAR);
			SqlParameter inEvento = new SqlParameter(Constantes.EVENTO, OracleTypes.VARCHAR);
			SqlParameter inIdProceso = new SqlParameter(Constantes.PI_IDPROCESO, OracleTypes.VARCHAR);
			SqlParameter inMtoConIgv = new SqlParameter(Constantes.PI_MONTOCONIGV, OracleTypes.VARCHAR);
			SqlParameter inId = new SqlParameter(Constantes.PI_ID, OracleTypes.VARCHAR);
			SqlParameter inRowid = new SqlParameter(Constantes.PI_ROWID_DET, OracleTypes.ROWID);
			SqlOutParameter outCodigoRespuesta = new SqlOutParameter(Constantes.CODIGORESPUESTA,
					OracleTypes.VARCHAR);
			SqlOutParameter outMensajeRespuesta = new SqlOutParameter(Constantes.MENSAJERESPUESTA,
					OracleTypes.VARCHAR);
			SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess()
					.withSchemaName(propertiesExterno.dbSiopdbOwner)
					.withCatalogName(propertiesExterno.dbSiopPkgAjusteMasivo)
					.withProcedureName(propertiesExterno.dbSiopActMtoCab)
					.declareParameters(inTmCode,inSnCode,inEvento, inIdProceso,inMtoConIgv,inId,inRowid,outCodigoRespuesta, outMensajeRespuesta);
			logger.info(cadMensaje + Constantes.MENSAJE_EJECUCION_SP + "[" + propertiesExterno.dbSiopdbOwner + "."
					+ propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopActMtoCab + "]");
			
			MapSqlParameterSource map = new MapSqlParameterSource();
			map.addValue(Constantes.TMCODE, bean.getTmCode());
			map.addValue(Constantes.SNCODE, bean.getSnCode());
			map.addValue(Constantes.EVENTO, bean.getEvento());
			map.addValue(Constantes.PI_IDPROCESO, bean.getIdProceso());
			map.addValue(Constantes.PI_MONTOCONIGV, bean.getMtoConIgv());
			map.addValue(Constantes.PI_ID, bean.getId());
			map.addValue(Constantes.PI_ROWID_DET, new ROWID(bean.getRowid()));
			Map<String, Object> result = procedureConsulta.execute(map);

			String poCodRpta = (String) result.get(Constantes.CODIGORESPUESTA);

			String poMsjRpta = (String) result.get(Constantes.MENSAJERESPUESTA);
			
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.TMCODE+ "]:" + bean.getTmCode());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.SNCODE+ "]:" + bean.getSnCode());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.EVENTO+ "]:" + bean.getEvento());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_IDPROCESO+ "]:" + bean.getIdProceso());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_MONTOCONIGV+ "]:" + bean.getMtoConIgv());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_ID+ "]:" + bean.getId());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_ROWID_DET+ "]:" + bean.getRowid());
			logger.info(cadMensaje + "PARAMETROS [OUTPUT][PO_COD_RPTA]:" + poCodRpta);
			logger.info(cadMensaje + "PARAMETROS [OUTPUT][PO_MSJ_RPTA]:" + poMsjRpta);
		
			response.setCodigoRespuesta(poCodRpta);
			response.setMensajeRespuesta(poMsjRpta);
			logger.info( cadMensaje.concat(" Datos obtenidos : \n ".concat(JAXBUtilitarios.anyObjectToXmlText(response))));
		} catch (Exception e) {
			String codigoError = "";
			codigoError = propertiesExterno.spIdt1Codigo;
			String msgError = "Hubo un error tecnico al ejecutar el PROCEDURE: ["
					+ propertiesExterno.dbSiopdbOwner + "." + propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopActMtoCab + "] ";
			response.setCodigoRespuesta(codigoError);
			response.setMensajeRespuesta(msgError);
			logger.error(mensajeTransaccion + e.getMessage(), e);
			logger.error(mensajeTransaccion + msgError);
			throw new DBException(codigoError,msgError, e);	
		}finally {
			logger.info(cadMensaje + Utilitarios.msjFinProceso(metodoActualizaProceso, Constantes.DAO));
		}
		return response;
	}

	@Override
	public ResponseBase completaDetalle(DetalleArchPlanoBean bean, String mensajeTransaccion, Integer id)
			throws DBException {
		String metodoActualizaProceso = "completaDetalle";

		String cadMensaje = mensajeTransaccion + "[" + metodoActualizaProceso + "] ";

		logger.info(cadMensaje + Utilitarios.msjInicioProceso(metodoActualizaProceso, Constantes.DAO));

		Utilitarios.infoConexion(mensajeTransaccion, propertiesExterno.dbSiopdbConexion, propertiesExterno.dbSiopdbNombre,
				propertiesExterno.dbSiopdbOwner, propertiesExterno.dbSiopPkgAjusteMasivo,
				propertiesExterno.dbSiopUpdtBscsPar);
		ResponseBase response = null;
		try {
			logger.info(
					mensajeTransaccion + "Intento [" + metodoActualizaProceso + "] : ");
			response = new ResponseBase();
			objJdbcCall = new SimpleJdbcCall(siopDS);
			objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			objJdbcTemplate.setQueryTimeout(propertiesExterno.DB_SIOP_CONNECTION_TIMEOUT);
			
			SqlParameter inDesplan = new SqlParameter(Constantes.PI_DESPLAN, OracleTypes.VARCHAR);
			SqlParameter inCebef = new SqlParameter(Constantes.PI_CEBEF, OracleTypes.VARCHAR);
			SqlParameter inCtaContable = new SqlParameter(Constantes.PI_CTACONTABLE, OracleTypes.VARCHAR);
			SqlParameter inAfecto = new SqlParameter(Constantes.AFECTO, OracleTypes.VARCHAR);
			SqlParameter inId = new SqlParameter(Constantes.PI_ID, OracleTypes.INTEGER);
			SqlParameter inIdProceso = new SqlParameter(Constantes.ID_PROCESO, OracleTypes.INTEGER);
			SqlParameter inSnCode = new SqlParameter(Constantes.SNCODE, OracleTypes.VARCHAR);
			SqlParameter inTmCode = new SqlParameter(Constantes.TMCODE, OracleTypes.VARCHAR);
			SqlParameter inNomRubro = new SqlParameter(Constantes.PI_NOMRUBRO, OracleTypes.VARCHAR);
			SqlParameter inConRubro = new SqlParameter(Constantes.PI_CONRUBRO, OracleTypes.VARCHAR);
			SqlParameter inCtaSap= new SqlParameter(Constantes.PI_CTASAP, OracleTypes.VARCHAR);
			SqlParameter inCtaigv = new SqlParameter(Constantes.PI_CTAIGV, OracleTypes.VARCHAR);
			SqlParameter inPorcentaje = new SqlParameter(Constantes.PI_PORCENTAJE, OracleTypes.NUMBER);
			SqlParameter inRowid = new SqlParameter(Constantes.PI_ROWID_DET, OracleTypes.ROWID);
			SqlOutParameter outCodigoRespuesta = new SqlOutParameter(Constantes.CODIGORESPUESTA,
					OracleTypes.VARCHAR);
			SqlOutParameter outMensajeRespuesta = new SqlOutParameter(Constantes.MENSAJERESPUESTA,
					OracleTypes.VARCHAR);
			SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess()
					.withSchemaName(propertiesExterno.dbSiopdbOwner)
					.withCatalogName(propertiesExterno.dbSiopPkgAjusteMasivo)
					.withProcedureName(propertiesExterno.dbSiopUpdtBscsPar)
					.declareParameters(inDesplan,inCebef,inCtaContable,inAfecto,inId,inIdProceso,inSnCode,inTmCode,inNomRubro,inConRubro,inCtaSap,
							inCtaigv,inPorcentaje,inRowid,
							outCodigoRespuesta, outMensajeRespuesta);
			logger.info(cadMensaje + Constantes.MENSAJE_EJECUCION_SP + "[" + propertiesExterno.dbSiopdbOwner + "."
					+ propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopUpdtBscsPar + "]");
			
			
			MapSqlParameterSource map = new MapSqlParameterSource();
			map.addValue(Constantes.PI_DESPLAN, bean.getDesplan());
			map.addValue(Constantes.PI_CEBEF, bean.getCebe());
			map.addValue(Constantes.PI_CTACONTABLE, bean.getCtaper());
			map.addValue(Constantes.AFECTO, bean.getAfectoigv());
			map.addValue(Constantes.PI_ID, id);
			map.addValue(Constantes.ID_PROCESO, bean.getIdproceso());
			map.addValue(Constantes.SNCODE, bean.getServ());
			map.addValue(Constantes.TMCODE, bean.getPlan());
			map.addValue(Constantes.PI_NOMRUBRO, bean.getNombreRubro());
			map.addValue(Constantes.PI_CONRUBRO, bean.getConceptoRubro());
			map.addValue(Constantes.PI_CTASAP, bean.getCtamex());
			map.addValue(Constantes.PI_CTAIGV, bean.getCtaigv());
			map.addValue(Constantes.PI_PORCENTAJE, bean.getPorcentaje());
			map.addValue(Constantes.PI_ROWID_DET, new ROWID(bean.getRowid()));
			Map<String, Object> result = procedureConsulta.execute(map);

			String poCodRpta = (String) result.get(Constantes.CODIGORESPUESTA);

			String poMsjRpta = (String) result.get(Constantes.MENSAJERESPUESTA);
			
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_DESPLAN+ "]:" + bean.getDesplan());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_CEBEF+ "]:" + bean.getCebe());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_CTACONTABLE+ "]:" + bean.getCtaper());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.AFECTO+ "]:" + bean.getAfectoigv());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_ID+ "]:" + id);
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.ID_PROCESO+ "]:" + bean.getIdproceso());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.SNCODE+ "]:" + bean.getServ());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.TMCODE+ "]:" + bean.getPlan());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_NOMRUBRO+ "]:" + bean.getNombreRubro());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_CONRUBRO+ "]:" + bean.getConceptoRubro());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_CTASAP+ "]:" + bean.getCtamex());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_CTAIGV+ "]:" + bean.getCtaigv());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_PORCENTAJE+ "]:" + bean.getPorcentaje());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.PI_ROWID_DET+ "]:" + bean.getRowid());
			logger.info(cadMensaje + "PARAMETROS [OUTPUT][PO_COD_RPTA]:" + poCodRpta);
			logger.info(cadMensaje + "PARAMETROS [OUTPUT][PO_MSJ_RPTA]:" + poMsjRpta);
			response.setCodigoRespuesta(poCodRpta);
			response.setMensajeRespuesta(poMsjRpta);
			logger.info( cadMensaje.concat(" Datos obtenidos : \n ".concat(JAXBUtilitarios.anyObjectToXmlText(response))));
		} catch (Exception e) {
			String codigoError = "";
			codigoError = propertiesExterno.spIdt1Codigo;
			String msgError = "Hubo un error tecnico al ejecutar el PROCEDURE: ["
					+ propertiesExterno.dbSiopdbOwner + "." + propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopUpdtBscsPar + "] ";
			response.setCodigoRespuesta(codigoError);
			response.setMensajeRespuesta(msgError);
			logger.error(mensajeTransaccion + e.getMessage(), e);
			logger.error(mensajeTransaccion + msgError);
			throw new DBException(codigoError,msgError, e);	
		}finally {
			logger.info(cadMensaje + Utilitarios.msjFinProceso(metodoActualizaProceso, Constantes.DAO));
		}
		return response;
	}

	@Override
	public ResponseBase insertarTmpDetAm(String mensajeTransaccion, TmpAmDetBean bean) throws DBException {
		ResponseBase response = new ResponseBase();
		String metodoActualizaProceso = "insertarTmpDetAm";
		String cadMensaje = mensajeTransaccion + "[" + metodoActualizaProceso + "] ";
		logger.info(cadMensaje + Utilitarios.msjInicioProceso(metodoActualizaProceso, Constantes.DAO));
		
		String conexion = propertiesExterno.dbSiopdbConexion;
		String nombreBD = propertiesExterno.dbSiopdbNombre;
		String owner = propertiesExterno.dbSiopdbOwner;
		String paquete = propertiesExterno.dbSiopPkgAjusteMasivo;
		String procedure = propertiesExterno.dbSiopInsTmpdetAm;
		
		Utilitarios.infoConexion(mensajeTransaccion, conexion, nombreBD, owner, paquete, procedure);
		
		try {
			objJdbcCall = new SimpleJdbcCall(siopDS);
			objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			objJdbcTemplate.setQueryTimeout(propertiesExterno.DB_SIOP_CONNECTION_TIMEOUT);
			
			SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess()
					.withSchemaName(owner)
					.withCatalogName(paquete)
					.withProcedureName(procedure)
					.declareParameters(
							new SqlParameter("PI_ID", OracleTypes.NUMBER),
							new SqlParameter("PI_IDPROCESO", OracleTypes.NUMBER),
							new SqlParameter("PI_CUENTACONTABLE", OracleTypes.VARCHAR),
							new SqlParameter("PI_EVENTO", OracleTypes.VARCHAR),
							new SqlParameter("PI_MONTOSINIGV", OracleTypes.NUMBER),
							new SqlParameter("PI_SNCODE", OracleTypes.VARCHAR),
							new SqlParameter("PI_TMCODE", OracleTypes.VARCHAR),
							new SqlParameter("PI_LINEA", OracleTypes.VARCHAR),
							new SqlParameter("PI_ESTADO", OracleTypes.VARCHAR),
							new SqlParameter("PI_FECHAREGISTRO", OracleTypes.DATE),
							new SqlOutParameter("PO_ROWID_DET", OracleTypes.ROWID),
							new SqlOutParameter("PO_CODERROR", OracleTypes.VARCHAR),
							new SqlOutParameter("PO_DESERROR", OracleTypes.VARCHAR));
			
			logger.info(cadMensaje + Constantes.MENSAJE_EJECUCION_SP + "[" + owner + "." + paquete + "." + procedure + "]");
			logger.info(cadMensaje + "PARAMETROS [INPUT][PI_ID]: " + bean.getId());
			logger.info(cadMensaje + "PARAMETROS [INPUT][PI_IDPROCESO]: " + bean.getIdProceso());
			logger.info(cadMensaje + "PARAMETROS [INPUT][PI_CUENTACONTABLE]: " + bean.getCuentaContable());
			logger.info(cadMensaje + "PARAMETROS [INPUT][PI_EVENTO]: " + bean.getEvento());
			logger.info(cadMensaje + "PARAMETROS [INPUT][PI_MONTOSINIGV]: " + bean.getMontoSinIgv());
			logger.info(cadMensaje + "PARAMETROS [INPUT][PI_SNCODE]: " + bean.getSnCode());
			logger.info(cadMensaje + "PARAMETROS [INPUT][PI_TMCODE]: " + bean.getTmCode());
			logger.info(cadMensaje + "PARAMETROS [INPUT][PI_LINEA]: " + bean.getLinea());
			logger.info(cadMensaje + "PARAMETROS [INPUT][PI_ESTADO]: " + bean.getEstado());
			logger.info(cadMensaje + "PARAMETROS [INPUT][PI_FECHAREGISTRO]: " + bean.getFechaRegistro());
			
			SqlParameterSource inputProcedure = new MapSqlParameterSource()
					.addValue("PI_ID", bean.getId(), OracleTypes.NUMBER)
					.addValue("PI_IDPROCESO", bean.getIdProceso(), OracleTypes.NUMBER)
					.addValue("PI_CUENTACONTABLE", bean.getCuentaContable(), OracleTypes.VARCHAR)
					.addValue("PI_EVENTO", bean.getEvento(), OracleTypes.VARCHAR)
					.addValue("PI_MONTOSINIGV", bean.getMontoSinIgv(), OracleTypes.NUMBER)
					.addValue("PI_SNCODE", bean.getSnCode(), OracleTypes.VARCHAR)
					.addValue("PI_TMCODE", bean.getTmCode(), OracleTypes.VARCHAR)
					.addValue("PI_LINEA", bean.getLinea(), OracleTypes.VARCHAR)
					.addValue("PI_ESTADO", bean.getEstado(), OracleTypes.VARCHAR)
					.addValue("PI_FECHAREGISTRO", bean.getFechaRegistro(), OracleTypes.DATE);
			Map<String, Object> outputProcedure = procedureConsulta.execute(inputProcedure);
			
			String poRowid = ((ROWID) outputProcedure.get("PO_ROWID_DET")).stringValue();
			String poCodRpta = (String) outputProcedure.get("PO_CODERROR");
			String poMsjRpta = (String) outputProcedure.get("PO_DESERROR");
			
			logger.info(cadMensaje + "PARAMETROS [OUTPUT][PO_ROWID_DET]:" + poRowid);
			logger.info(cadMensaje + "PARAMETROS [OUTPUT][PO_CODERROR]:" + poCodRpta);
			logger.info(cadMensaje + "PARAMETROS [OUTPUT][PO_DESERROR]:" + poMsjRpta);
			
			response.setRowid(poRowid);
			response.setCodigoRespuesta(poCodRpta);
			response.setMensajeRespuesta(poMsjRpta);
		} catch (Exception e) {
			String codigoError = "";
			codigoError = propertiesExterno.spIdt1Codigo;
			String msgError = "Hubo un error tecnico al ejecutar el PROCEDURE: ["
					+ propertiesExterno.dbSiopdbOwner + "." + propertiesExterno.dbSiopPkgAjusteMasivo + "."
					+ propertiesExterno.dbSiopInsTmpdetAm + "] ";
			response.setCodigoRespuesta(codigoError);
			response.setMensajeRespuesta(msgError);
			logger.error(mensajeTransaccion + e.getMessage(), e);
			logger.error(mensajeTransaccion + msgError);
			throw new DBException(codigoError,msgError, e);	
		} finally {
			logger.info(cadMensaje + Utilitarios.msjFinProceso(metodoActualizaProceso, Constantes.DAO));
		}
		
		return response;
	}

	@Override
	public ResponseBase eliminarTmpBscs(String mensajeTransaccion, Integer idProceso, String docReferencia) throws DBException {
		ResponseBase response = new ResponseBase();
		String metodoActualizaProceso = "eliminarTmpBscs";
		String cadMensaje = mensajeTransaccion + "[" + metodoActualizaProceso + "] ";
		logger.info(cadMensaje + Utilitarios.msjInicioProceso(metodoActualizaProceso, Constantes.DAO));
		
		String conexion = propertiesExterno.dbSiopdbConexion;
		String nombreBD = propertiesExterno.dbSiopdbNombre;
		String owner = propertiesExterno.dbSiopdbOwner;
		String paquete = propertiesExterno.dbSiopPkgAjusteMasivo;
		String procedure = propertiesExterno.dbSiopElimTmpBscs;
		Utilitarios.infoConexion(mensajeTransaccion, conexion, nombreBD, owner, paquete, procedure);
		
		try {
			objJdbcCall = new SimpleJdbcCall(siopDS);
			objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			objJdbcTemplate.setQueryTimeout(propertiesExterno.DB_SIOP_CONNECTION_TIMEOUT);
			
			SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess()
					.withSchemaName(owner)
					.withCatalogName(paquete)
					.withProcedureName(procedure)
					.declareParameters(
							new SqlParameter("PI_IDPROCESO", OracleTypes.INTEGER),
							new SqlParameter("PI_DOCREFERENCIA", OracleTypes.VARCHAR),
							new SqlOutParameter("PO_CODERROR", OracleTypes.VARCHAR),
							new SqlOutParameter("PO_DESERROR", OracleTypes.VARCHAR));
			
			logger.info(cadMensaje + Constantes.MENSAJE_EJECUCION_SP + "[" + owner + "." + paquete + "." + procedure + "]");
			logger.info(cadMensaje + "PARAMETROS [INPUT][PI_IDPROCESO]: " + idProceso);
			logger.info(cadMensaje + "PARAMETROS [INPUT][PI_DOCREFERENCIA]: " + docReferencia);
			
			SqlParameterSource inputProcedure = new MapSqlParameterSource()
					.addValue("PI_IDPROCESO", idProceso, OracleTypes.INTEGER)
					.addValue("PI_DOCREFERENCIA", docReferencia, OracleTypes.VARCHAR);
			Map<String, Object> outputProcedure = procedureConsulta.execute(inputProcedure);
			
			String poCodRpta = (String) outputProcedure.get("PO_CODERROR");
			String poMsjRpta = (String) outputProcedure.get("PO_DESERROR");
			
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_CODERROR]:" + poCodRpta);
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_DESERROR]:" + poMsjRpta);
			
			response.setCodigoRespuesta(poCodRpta);
			response.setMensajeRespuesta(poMsjRpta);
		} catch (Exception e) {
			String codigoError = "";
			codigoError = propertiesExterno.spIdt1Codigo;
			String msgError = "Hubo un error tecnico al ejecutar el PROCEDURE: ["
					+ owner + "." + paquete + "." + procedure + "] ";
			response.setCodigoRespuesta(codigoError);
			response.setMensajeRespuesta(msgError);
			logger.error(mensajeTransaccion + e.getMessage(), e);
			logger.error(mensajeTransaccion + msgError);
			throw new DBException(codigoError, msgError, e);
		} finally {
			logger.info(cadMensaje + Utilitarios.msjFinProceso(metodoActualizaProceso, Constantes.DAO));
		}
		
		return response;
	}

	@Override
	public ResponseBase eliminarTmpDetalle(String mensajeTransaccion, DetalleTemporalBean detalleTmp) throws DBException {
		ResponseBase response = new ResponseBase();
		String nombreMetodo = "eliminarTmpDetalle";
		String cadMensaje = mensajeTransaccion + "[" + nombreMetodo + "] ";
		logger.info(cadMensaje + Utilitarios.msjInicioProceso(nombreMetodo, Constantes.DAO));
		
		String conexion = propertiesExterno.dbSiopdbConexion;
		String nombreBD = propertiesExterno.dbSiopdbNombre;
		String owner = propertiesExterno.dbSiopdbOwner;
		String paquete = propertiesExterno.dbSiopPkgAjusteMasivo;
		String procedure = propertiesExterno.dbSiopElimTmpDet;
		Utilitarios.infoConexion(mensajeTransaccion, conexion, nombreBD, owner, paquete, procedure);
		
		try {
			objJdbcCall = new SimpleJdbcCall(siopDS);
			objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			objJdbcTemplate.setQueryTimeout(propertiesExterno.DB_SIOP_CONNECTION_TIMEOUT);
			
			SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess()
					.withSchemaName(owner)
					.withCatalogName(paquete)
					.withProcedureName(procedure)
					.declareParameters(
							new SqlParameter("PI_ID_PROCESO", OracleTypes.INTEGER),
							new SqlParameter("PI_ID", OracleTypes.INTEGER),
							new SqlParameter("PI_TMCODE", OracleTypes.VARCHAR),
							new SqlParameter("PI_SNCODE", OracleTypes.VARCHAR),
							new SqlOutParameter("PO_CODERROR", OracleTypes.VARCHAR),
							new SqlOutParameter("PO_DESERROR", OracleTypes.VARCHAR));
			
			logger.info(cadMensaje + Constantes.MENSAJE_EJECUCION_SP + "[" + owner + "." + paquete + "." + procedure + "]");
			logger.info(cadMensaje + "PARAMETROS [INPUT][PI_ID_PROCESO]: " + detalleTmp.getIdproceso());
			logger.info(cadMensaje + "PARAMETROS [INPUT][PI_ID]: " + detalleTmp.getId());
			logger.info(cadMensaje + "PARAMETROS [INPUT][PI_TMCODE]: " + detalleTmp.getTmCode());
			logger.info(cadMensaje + "PARAMETROS [INPUT][PI_SNCODE]: " + detalleTmp.getSncode());
			
			SqlParameterSource inputProcedure = new MapSqlParameterSource()
					.addValue("PI_ID_PROCESO", detalleTmp.getIdproceso(), OracleTypes.INTEGER)
					.addValue("PI_ID", detalleTmp.getId(), OracleTypes.INTEGER)
					.addValue("PI_TMCODE", detalleTmp.getTmCode(), OracleTypes.VARCHAR)
					.addValue("PI_SNCODE", detalleTmp.getSncode(), OracleTypes.VARCHAR);
			Map<String, Object> outputProcedure = procedureConsulta.execute(inputProcedure);
			
			String poCodRpta = (String) outputProcedure.get("PO_CODERROR");
			String poMsjRpta = (String) outputProcedure.get("PO_DESERROR");
			
			logger.info(cadMensaje + "PARAMETROS [OUTPUT][PO_CODERROR]:" + poCodRpta);
			logger.info(cadMensaje + "PARAMETROS [OUTPUT][PO_DESERROR]:" + poMsjRpta);
			
			response.setCodigoRespuesta(poCodRpta);
			response.setMensajeRespuesta(poMsjRpta);
		} catch (Exception e) {
			String codigoError = propertiesExterno.spIdt1Codigo;
			String msgError = "Hubo un error tecnico al ejecutar el PROCEDURE: ["
					+ owner + "." + paquete + "." + procedure + "] ";
			response.setCodigoRespuesta(codigoError);
			response.setMensajeRespuesta(msgError);
			logger.error(mensajeTransaccion + e.getMessage(), e);
			logger.error(mensajeTransaccion + msgError);
			throw new DBException(codigoError, msgError, e);
		} finally {
			logger.info(cadMensaje + Utilitarios.msjFinProceso(nombreMetodo, Constantes.DAO));
		}
		
		return response;
	}

}
