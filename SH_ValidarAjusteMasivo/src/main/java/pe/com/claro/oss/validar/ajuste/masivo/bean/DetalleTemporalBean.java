package pe.com.claro.oss.validar.ajuste.masivo.bean;

import java.util.List;

public class DetalleTemporalBean implements Cloneable {
	
	private Integer id;
	private Integer idproceso;
	private String docreferencia;
	private String sncode;
	private String afecto;
	private String customerid;
	private String custcode;
	private String ruc;
	private String razonsocial;
	private String contacto;
	private String direccion;
	private String notadir;
	private String departamento;
	private String provincia;
	private String distrito;
	private String nrotelefono;
	private String afiliacion;
	private String email;
	private String factura;
	private String montofactura;
	private String emision;
	private String ampliacion;
	private String billcycle;
	private String fechainicio;
	private String fechafin;
	private String plan;
	private String cuentacontable;
	private String cuentacontablesap;
	private String montosinigv;
	private String montoconigv;
	private String centrobenef;
	private String evento;
	private String tmCode;
	private String rowid;
	private String linea;
	private boolean found;
	private DetalleArchPlanoBean archivoAsociado;
	private List<DetalleArchPlanoBean> listaArchivoAsociado;
	private DatosAjusteSiop ajusteHistoricoAsociado;
//	private List<DatosAjusteSiop> listaAjusteHistoricoAsociado;
	private boolean foundHistoricoDocRef;
	private List<DatosAjusteSiop> listaAjustesDocRef;
	
	public DetalleTemporalBean() {
	}

	public DetalleTemporalBean(DetalleTemporalBean detalleTemporal) {
		this.id = detalleTemporal.getId();
		this.idproceso = detalleTemporal.getIdproceso();
		this.docreferencia = detalleTemporal.getDocreferencia();
		this.sncode = detalleTemporal.getSncode();
		this.afecto = detalleTemporal.getAfecto();
		this.customerid = detalleTemporal.getCustomerid();
		this.custcode = detalleTemporal.getCustcode();
		this.ruc = detalleTemporal.getRuc();
		this.razonsocial = detalleTemporal.getRazonsocial();
		this.contacto = detalleTemporal.getContacto();
		this.direccion = detalleTemporal.getDireccion();
		this.notadir = detalleTemporal.getNotadir();
		this.departamento = detalleTemporal.getDepartamento();
		this.provincia = detalleTemporal.getProvincia();
		this.distrito = detalleTemporal.getDistrito();
		this.nrotelefono = detalleTemporal.getNrotelefono();
		this.afiliacion = detalleTemporal.getAfiliacion();
		this.email = detalleTemporal.getEmail();
		this.factura = detalleTemporal.getFactura();
		this.montofactura = detalleTemporal.getMontofactura();
		this.emision = detalleTemporal.getEmision();
		this.ampliacion = detalleTemporal.getAmpliacion();
		this.billcycle = detalleTemporal.getBillcycle();
		this.fechainicio = detalleTemporal.getFechainicio();
		this.fechafin = detalleTemporal.getFechafin();
		this.plan = detalleTemporal.getPlan();
		this.cuentacontable = detalleTemporal.getCuentacontable();
		this.cuentacontablesap = detalleTemporal.getCuentacontablesap();
		this.montosinigv = detalleTemporal.getMontosinigv();
		this.montoconigv = detalleTemporal.getMontoconigv();
		this.centrobenef = detalleTemporal.getCentrobenef();
		this.evento = detalleTemporal.getEvento();
		this.tmCode = detalleTemporal.getTmCode();
		this.rowid = detalleTemporal.getRowid();
		this.linea = detalleTemporal.getLinea();
		this.found = detalleTemporal.isFound();
		this.archivoAsociado = detalleTemporal.getArchivoAsociado();
		this.listaArchivoAsociado = detalleTemporal.getListaArchivoAsociado();
		this.ajusteHistoricoAsociado = detalleTemporal.getAjusteHistoricoAsociado();
		this.foundHistoricoDocRef = detalleTemporal.isFoundHistoricoDocRef();
		this.listaAjustesDocRef = detalleTemporal.getListaAjustesDocRef();
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getIdproceso() {
		return idproceso;
	}
	public void setIdproceso(Integer idproceso) {
		this.idproceso = idproceso;
	}
	public String getDocreferencia() {
		return docreferencia;
	}
	public void setDocreferencia(String docreferencia) {
		this.docreferencia = docreferencia;
	}
	public String getSncode() {
		return sncode;
	}
	public void setSncode(String sncode) {
		this.sncode = sncode;
	}
	public String getAfecto() {
		return afecto;
	}
	public void setAfecto(String afecto) {
		this.afecto = afecto;
	}
	public String getCustomerid() {
		return customerid;
	}
	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}
	public String getCustcode() {
		return custcode;
	}
	public void setCustcode(String custcode) {
		this.custcode = custcode;
	}
	public String getRuc() {
		return ruc;
	}
	public void setRuc(String ruc) {
		this.ruc = ruc;
	}
	public String getRazonsocial() {
		return razonsocial;
	}
	public void setRazonsocial(String razonsocial) {
		this.razonsocial = razonsocial;
	}
	public String getContacto() {
		return contacto;
	}
	public void setContacto(String contacto) {
		this.contacto = contacto;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getNotadir() {
		return notadir;
	}
	public void setNotadir(String notadir) {
		this.notadir = notadir;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public String getNrotelefono() {
		return nrotelefono;
	}
	public void setNrotelefono(String nrotelefono) {
		this.nrotelefono = nrotelefono;
	}
	public String getAfiliacion() {
		return afiliacion;
	}
	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFactura() {
		return factura;
	}
	public void setFactura(String factura) {
		this.factura = factura;
	}
	public String getMontofactura() {
		return montofactura;
	}
	public void setMontofactura(String montofactura) {
		this.montofactura = montofactura;
	}
	public String getEmision() {
		return emision;
	}
	public void setEmision(String emision) {
		this.emision = emision;
	}
	public String getAmpliacion() {
		return ampliacion;
	}
	public void setAmpliacion(String ampliacion) {
		this.ampliacion = ampliacion;
	}
	public String getBillcycle() {
		return billcycle;
	}
	public void setBillcycle(String billcycle) {
		this.billcycle = billcycle;
	}
	public String getFechainicio() {
		return fechainicio;
	}
	public void setFechainicio(String fechainicio) {
		this.fechainicio = fechainicio;
	}
	public String getFechafin() {
		return fechafin;
	}
	public void setFechafin(String fechafin) {
		this.fechafin = fechafin;
	}
	public String getPlan() {
		return plan;
	}
	public void setPlan(String plan) {
		this.plan = plan;
	}
	public String getCuentacontable() {
		return cuentacontable;
	}
	public void setCuentacontable(String cuentacontable) {
		this.cuentacontable = cuentacontable;
	}
	public String getCuentacontablesap() {
		return cuentacontablesap;
	}
	public void setCuentacontablesap(String cuentacontablesap) {
		this.cuentacontablesap = cuentacontablesap;
	}
	public String getMontosinigv() {
		return montosinigv;
	}
	public void setMontosinigv(String montosinigv) {
		this.montosinigv = montosinigv;
	}
	public String getMontoconigv() {
		return montoconigv;
	}
	public void setMontoconigv(String montoconigv) {
		this.montoconigv = montoconigv;
	}
	public String getCentrobenef() {
		return centrobenef;
	}
	public void setCentrobenef(String centrobenef) {
		this.centrobenef = centrobenef;
	}
	public String getEvento() {
		return evento;
	}
	public void setEvento(String evento) {
		this.evento = evento;
	}
	public String getTmCode() {
		return tmCode;
	}
	public void setTmCode(String tmCode) {
		this.tmCode = tmCode;
	}
	public String getRowid() {
		return rowid;
	}
	public void setRowid(String rowid) {
		this.rowid = rowid;
	}
	public String getLinea() {
		return linea;
	}
	public void setLinea(String linea) {
		this.linea = linea;
	}
	public boolean isFound() {
		return found;
	}
	public void setFound(boolean found) {
		this.found = found;
	}
	public DetalleArchPlanoBean getArchivoAsociado() {
		return archivoAsociado;
	}
	public void setArchivoAsociado(DetalleArchPlanoBean archivoAsociado) {
		this.archivoAsociado = archivoAsociado;
	}
	public DatosAjusteSiop getAjusteHistoricoAsociado() {
		return ajusteHistoricoAsociado;
	}
	public void setAjusteHistoricoAsociado(DatosAjusteSiop ajusteHistoricoAsociado) {
		this.ajusteHistoricoAsociado = ajusteHistoricoAsociado;
	}
	public boolean isFoundHistoricoDocRef() {
		return foundHistoricoDocRef;
	}
	public void setFoundHistoricoDocRef(boolean foundHistoricoDocRef) {
		this.foundHistoricoDocRef = foundHistoricoDocRef;
	}
	public List<DatosAjusteSiop> getListaAjustesDocRef() {
		return listaAjustesDocRef;
	}
	public void setListaAjustesDocRef(List<DatosAjusteSiop> listaAjustesDocRef) {
		this.listaAjustesDocRef = listaAjustesDocRef;
	}
	public List<DetalleArchPlanoBean> getListaArchivoAsociado() {
		return listaArchivoAsociado;
	}
	public void setListaArchivoAsociado(List<DetalleArchPlanoBean> listaArchivoAsociado) {
		this.listaArchivoAsociado = listaArchivoAsociado;
	}
	@Override
	public Object clone() throws CloneNotSupportedException {
		DetalleTemporalBean detalleTemporal = new DetalleTemporalBean(this);
		return detalleTemporal;
	}

}
