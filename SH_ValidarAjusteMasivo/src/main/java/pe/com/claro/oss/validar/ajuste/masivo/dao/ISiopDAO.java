package pe.com.claro.oss.validar.ajuste.masivo.dao;

import pe.com.claro.oss.validar.ajuste.masivo.bean.ActualizaEstadoBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ActualizaMontoCabBscsBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ActualizaMtoCabBscsBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ActualizaProcesoTmpBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ActualizarMontoDetalleBSCSBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.DetalleArchPlanoBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.DetalleTemporalBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ListaDatosAjusteSiop;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ListaDetalleTemporalBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ListaProcesoExcelBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ListarCabeceraAjusteBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ListarDetalleArchPlanoBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ObtieneRubroBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ResponseBase;
import pe.com.claro.oss.validar.ajuste.masivo.bean.TmpAmDetBean;
import pe.com.claro.oss.validar.ajuste.masivo.exception.DBException;

public interface ISiopDAO {

	ListaProcesoExcelBean obtenerProcesoExcel(String mensajeTransaccion) throws DBException;
	ActualizaProcesoTmpBean actualizarProcesoTmp(ActualizaProcesoTmpBean actualizaProcesoBean, String mensajeTransaccion)throws DBException;
	ListaDatosAjusteSiop listarDatosAjusteSiop(ListaDatosAjusteSiop datosAjusteSiop,String mensajeTransaccion)throws DBException;
	ListarCabeceraAjusteBean listarCabeceraAjuste(ListarCabeceraAjusteBean listarCabeceraAjusteBean,String mensajeTransaccion) throws DBException;
	ListarDetalleArchPlanoBean listarDetalle(ListarDetalleArchPlanoBean bean, String mensajeTransaccion) throws DBException;
	ActualizaEstadoBean actualizarEstado(ActualizaEstadoBean bean ,String mensajeTransaccion) throws DBException;
	ResponseBase registrarDetalleRecBscsAMTotal(DetalleArchPlanoBean detalleArchivoPlano, String mensajeTransaccion,Integer id) throws DBException;
	ActualizarMontoDetalleBSCSBean actualizarMontosDetalle(ActualizarMontoDetalleBSCSBean bean, String mensajeTransaccion) throws DBException;
	ListaDetalleTemporalBean listarDetalleTemporal(ListaDetalleTemporalBean bean, String mensajeTransaccion) throws DBException;
	ActualizaMontoCabBscsBean actualizarMontoCab(ActualizaMontoCabBscsBean bean, String mensajeTransaccion) throws DBException;
	ListaDatosAjusteSiop listarDetalleHistoricoDocAjuste(ListaDatosAjusteSiop bean , String mensajeTransaccion) throws DBException;
	ActualizaEstadoBean actualizarEstadoProc(ActualizaEstadoBean bean ,String mensajeTransaccion) throws DBException;
	ListarDetalleArchPlanoBean listarDetalleBscsAjuste(ListarDetalleArchPlanoBean bean, String mensajeTransaccion) throws DBException;
	ListaDatosAjusteSiop listarDetalleHistoricoSiop(ListarDetalleArchPlanoBean bean, String mensajeTransaccion) throws DBException;
	ResponseBase completaDatosCabecera(DetalleArchPlanoBean bean,String mensajeTransaccion,Integer id)throws DBException;
	ObtieneRubroBean obtenerRubro(ObtieneRubroBean bean,String mensajeTransaccion) throws DBException;
	ActualizaMtoCabBscsBean actualizarMtoIgv(ActualizaMtoCabBscsBean bean,String mensajeTransaccion) throws DBException;
	ResponseBase completaDetalle(DetalleArchPlanoBean bean,String mensajeTransaccion,Integer id ) throws DBException;
	ResponseBase insertarTmpDetAm(String mensajeTransaccion, TmpAmDetBean bean) throws DBException;
	ResponseBase eliminarTmpBscs(String mensajeTransaccion, Integer idProceso, String docReferencia) throws DBException;
	ResponseBase eliminarTmpDetalle(String mensajeTransaccion, DetalleTemporalBean detalleTmp) throws DBException;
	
}
