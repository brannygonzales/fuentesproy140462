package pe.com.claro.oss.validar.ajuste.masivo.dao;

import pe.com.claro.oss.validar.ajuste.masivo.bean.ListarAjusteOacBean;
import pe.com.claro.oss.validar.ajuste.masivo.exception.DBException;

public interface IOacDAO {
	
	ListarAjusteOacBean listarAjustes(ListarAjusteOacBean bean, String mensajeTransaccion) throws DBException;
}
