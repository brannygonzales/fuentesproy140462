package pe.com.claro.oss.validar.ajuste.masivo.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.com.claro.oss.validar.ajuste.masivo.bean.AjusteOACBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ListarAjusteOacBean;
import pe.com.claro.oss.validar.ajuste.masivo.exception.DBException;
import pe.com.claro.oss.validar.ajuste.masivo.util.Constantes;
import pe.com.claro.oss.validar.ajuste.masivo.util.PropertiesExternos;
import pe.com.claro.oss.validar.ajuste.masivo.util.Utilitarios;

@Repository
public class OacDAOImpl implements IOacDAO {
	
	private static Logger logger = Logger.getLogger(OacDAOImpl.class);
	@Autowired
	private PropertiesExternos propertiesExterno;
	@Qualifier("oacDS")
	private DataSource oacDS;
	
	SimpleJdbcCall objJdbcCall = null;

	JdbcTemplate objJdbcTemplate = null;
	
	@Override
	public ListarAjusteOacBean listarAjustes(ListarAjusteOacBean bean, String mensajeTransaccion) throws DBException {
		String metodoListarProceso = "listarAjustes";

		String cadMensaje = mensajeTransaccion + "[" + metodoListarProceso + "] ";

		logger.info(cadMensaje + Utilitarios.msjInicioProceso(metodoListarProceso, Constantes.DAO));

		Utilitarios.infoConexion(mensajeTransaccion, propertiesExterno.dbOacdbConexion, propertiesExterno.dbOacbNombre,
				propertiesExterno.dbOacdbOwner, propertiesExterno.dbOacPkgAjusteMasivo,
				propertiesExterno.dbOacListarAjustes);
		ListarAjusteOacBean response = null;
		try {
			response = new ListarAjusteOacBean();
			objJdbcCall = new SimpleJdbcCall(oacDS);
			objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			objJdbcTemplate.setQueryTimeout(propertiesExterno.DB_OAC_CONNECTION_TIMEOUT);
			SqlParameter inDocReferencia = new SqlParameter(Constantes.DOC_REFERENCIA, OracleTypes.VARCHAR);
			SqlParameter inCustomerID = new SqlParameter(Constantes.OAC_IN_CUSTOMER_ID, OracleTypes.VARCHAR);
			SqlOutParameter outCursorProcesos= new SqlOutParameter(Constantes.CURSORPROCESOS,
					OracleTypes.CURSOR,	new RowMapper<AjusteOACBean>() {
						public AjusteOACBean mapRow(ResultSet rs, int arg1) throws SQLException {
							AjusteOACBean ajusteBean = new AjusteOACBean();
							ajusteBean.setComentarios(rs.getString(Constantes.OAC_COMENTARIOS)==null?Constantes.VACIO: rs.getString(Constantes.OAC_COMENTARIOS));
							ajusteBean.setCustomerId(rs.getString(Constantes.OAC_CUSTOMER_ID)==null?Constantes.VACIO: rs.getString(Constantes.OAC_CUSTOMER_ID));
							ajusteBean.setEstado(rs.getString(Constantes.OAC_ESTADO)==null?Constantes.VACIO: rs.getString(Constantes.OAC_ESTADO));
							ajusteBean.setFechaDeuda(rs.getString(Constantes.OAC_FECHA_DEUDA)==null?Constantes.VACIO: rs.getString(Constantes.OAC_FECHA_DEUDA));
							ajusteBean.setFechaFactura(rs.getString(Constantes.OAC_FECHA_FACTURA)==null?Constantes.VACIO: rs.getString(Constantes.OAC_FECHA_FACTURA));
							ajusteBean.setMontoReal(rs.getString(Constantes.OAC_MONTO_REAL)==null?Constantes.VACIO: rs.getString(Constantes.OAC_MONTO_REAL));
							ajusteBean.setMontoRestante(rs.getString(Constantes.OAC_MONTO_RESTANTE)==null?Constantes.VACIO: rs.getString(Constantes.OAC_MONTO_RESTANTE));
							ajusteBean.setNumeroFactura(rs.getString(Constantes.OAC_NUMERO_FACTURA)==null?Constantes.VACIO: rs.getString(Constantes.OAC_NUMERO_FACTURA));

							return ajusteBean;
							}
						});
			SqlOutParameter outCodigoRespuesta = new SqlOutParameter(Constantes.CODIGORESPUESTA,
					OracleTypes.VARCHAR);
			SqlOutParameter outMensajeRespuesta = new SqlOutParameter(Constantes.MENSAJERESPUESTA,
					OracleTypes.VARCHAR);
			
			SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess()
					.withSchemaName(propertiesExterno.dbOacdbOwner)
					.withCatalogName(propertiesExterno.dbOacPkgAjusteMasivo)
					.withProcedureName(propertiesExterno.dbOacListarAjustes)
					.declareParameters(inDocReferencia,inCustomerID,outCursorProcesos, outCodigoRespuesta, outMensajeRespuesta);
			
			logger.info(cadMensaje + Constantes.MENSAJE_EJECUCION_SP + "[" + propertiesExterno.dbOacdbOwner + "."
					+ propertiesExterno.dbOacPkgAjusteMasivo + "."
					+ propertiesExterno.dbOacListarAjustes + "]");
			MapSqlParameterSource map = new MapSqlParameterSource();
			map.addValue(Constantes.DOC_REFERENCIA, bean.getDocumentoReferencia());
			map.addValue(Constantes.OAC_IN_CUSTOMER_ID, bean.getCustomerId());	
			Map<String, Object> result = procedureConsulta.execute(map);
			
			@SuppressWarnings("unchecked")
			List<AjusteOACBean> listaAjustes= (List<AjusteOACBean>) result.get(Constantes.CURSORPROCESOS);

			String poCodRpta = (String) result.get(Constantes.CODIGORESPUESTA);

			String poMsjRpta = (String) result.get(Constantes.MENSAJERESPUESTA);
			
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.DOC_REFERENCIA+ "]:" + bean.getDocumentoReferencia());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + Constantes.OAC_IN_CUSTOMER_ID+ "]:" + bean.getCustomerId());
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_COD_RPTA]:" + poCodRpta);
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_MSJ_RPTA]:" + poMsjRpta);
			
			response.setListaAjusteOacBean(listaAjustes);
			response.setCodigoRespuesta(poCodRpta);
			response.setMensajeRespuesta(poMsjRpta);
		}catch (Exception e) {
			String codigoError = "";
			codigoError = propertiesExterno.spIdt1Codigo;
			String msgError = "Hubo un error tecnico al ejecutar el PROCEDURE: ["
					+ propertiesExterno.dbOacdbOwner + "." + propertiesExterno.dbOacPkgAjusteMasivo + "."
					+ propertiesExterno.dbOacListarAjustes + "] ";
			response.setCodigoRespuesta(codigoError);
			response.setMensajeRespuesta(msgError);
			logger.error(mensajeTransaccion + e.getMessage(), e);
			logger.error(mensajeTransaccion + msgError);
			throw new DBException(codigoError,msgError, e);	
		}
		return response;
	}

}
