package pe.com.claro.oss.validar.ajuste.masivo.bean;

public class ActualizarMontoDetalleBSCSBean {
	
	private Integer id;
	private Integer idProceso;
	private String snCode;
	private Double montoConIgv;
	private String codigoRespuesta;
	private String mensajeRespuesta;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getIdProceso() {
		return idProceso;
	}
	public void setIdProceso(Integer idProceso) {
		this.idProceso = idProceso;
	}
	public String getSnCode() {
		return snCode;
	}
	public void setSnCode(String snCode) {
		this.snCode = snCode;
	}
	public Double getMontoConIgv() {
		return montoConIgv;
	}
	public void setMontoConIgv(Double montoConIgv) {
		this.montoConIgv = montoConIgv;
	}
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
	public String getMensajeRespuesta() {
		return mensajeRespuesta;
	}
	public void setMensajeRespuesta(String mensajeRespuesta) {
		this.mensajeRespuesta = mensajeRespuesta;
	}

	
}
