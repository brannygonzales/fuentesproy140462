package pe.com.claro.oss.validar.ajuste.masivo.bean;

import java.util.List;

public class ListaDatosAjusteSiop {

	private String docReferencia;
	private String tipoAjuste;
	private List<DatosAjusteSiop> listaDatosAjusteSiop;
	private String codigoRespuesta;
	private String mensajeRespuesta;
	private String docAjuste;
	private String idProceso;
	private String tmCode;
	private String snCode;
	private String cuentaContable;
	private String customerId;
	
	public List<DatosAjusteSiop> getListaDatosAjusteSiop() {
		return listaDatosAjusteSiop;
	}
	public void setListaDatosAjusteSiop(List<DatosAjusteSiop> listaDatosAjusteSiop) {
		this.listaDatosAjusteSiop = listaDatosAjusteSiop;
	}
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
	public String getMensajeRespuesta() {
		return mensajeRespuesta;
	}
	public void setMensajeRespuesta(String mensajeRespuesta) {
		this.mensajeRespuesta = mensajeRespuesta;
	}
	public String getDocReferencia() {
		return docReferencia;
	}
	public void setDocReferencia(String docReferencia) {
		this.docReferencia = docReferencia;
	}
	public String getTipoAjuste() {
		return tipoAjuste;
	}
	public void setTipoAjuste(String tipoAjuste) {
		this.tipoAjuste = tipoAjuste;
	}
	public String getDocAjuste() {
		return docAjuste;
	}
	public void setDocAjuste(String docAjuste) {
		this.docAjuste = docAjuste;
	}
	public String getIdProceso() {
		return idProceso;
	}
	public void setIdProceso(String idProceso) {
		this.idProceso = idProceso;
	}
	public String getTmCode() {
		return tmCode;
	}
	public void setTmCode(String tmCode) {
		this.tmCode = tmCode;
	}
	public String getSnCode() {
		return snCode;
	}
	public void setSnCode(String snCode) {
		this.snCode = snCode;
	}
	public String getCuentaContable() {
		return cuentaContable;
	}
	public void setCuentaContable(String cuentaContable) {
		this.cuentaContable = cuentaContable;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
    

	
	
}
