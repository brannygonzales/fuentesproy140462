package pe.com.claro.oss.validar.ajuste.masivo.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.claro.oss.validar.ajuste.masivo.bean.ActualizaEstadoBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ActualizaMontoCabBscsBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ActualizaMtoCabBscsBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ActualizaProcesoTmpBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.AjusteCabeceraBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.DatosAjusteSiop;
import pe.com.claro.oss.validar.ajuste.masivo.bean.DetalleArchPlanoBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.DetalleTemporalBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.GenerarArchivoPlanoBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.InsertarTemporalBscsBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ListaDatosAjusteSiop;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ListaDetalleTemporalBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ListaProcesoExcelBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ListarCabeceraAjusteBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ListarDetalleArchPlanoBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ObtieneRubroBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ProcesoTmpBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ResponseBase;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ResponseUsuarioAppBase;
import pe.com.claro.oss.validar.ajuste.masivo.bean.TmpAmDetBean;
import pe.com.claro.oss.validar.ajuste.masivo.dao.IBscsDAO;
import pe.com.claro.oss.validar.ajuste.masivo.dao.ISiopDAO;
import pe.com.claro.oss.validar.ajuste.masivo.exception.DBException;
import pe.com.claro.oss.validar.ajuste.masivo.exception.SFTPException;
import pe.com.claro.oss.validar.ajuste.masivo.util.Constantes;
import pe.com.claro.oss.validar.ajuste.masivo.util.JAXBUtilitarios;
import pe.com.claro.oss.validar.ajuste.masivo.util.PropertiesExternos;
import pe.com.claro.oss.validar.ajuste.masivo.util.Util;
import pe.com.claro.oss.validar.ajuste.masivo.util.UtilSFTP;


@Service
public class ValidarAjusteMasivoServiceImpl implements ValidarAjusteMasivoService {

	private static Logger logger = Logger
			.getLogger(ValidarAjusteMasivoServiceImpl.class);
	
	@Autowired
	private ISiopDAO siopDAO;
	@Autowired
	private IBscsDAO bscsDAO;
	@Autowired
	private UtilSFTP utilSFTP;

	@Autowired
	private Util util;
	
	@Autowired
	private PropertiesExternos propertiesExterno;
	@Override
	public void run(String idTransaction) {
		String mensajeTx =  "[launch idTx=" + idTransaction + " ]";
		ListaProcesoExcelBean listaProcesoExcelBean =  null;
		ActualizaProcesoTmpBean actualizaProcBean = null;
		ActualizaProcesoTmpBean retornoActualizaProcBean = null;
		try {
			listaProcesoExcelBean = siopDAO.obtenerProcesoExcel(mensajeTx);//obtener el proceso registrado y pririzado y lo bloquea
			actualizaProcBean = new ActualizaProcesoTmpBean();
			InsertarTemporalBscsBean inTemp= null;
			if(listaProcesoExcelBean.getListaProcesos() == null || listaProcesoExcelBean.getListaProcesos().size() <=0){
				throw new DBException(listaProcesoExcelBean.getMensajeRespuesta());		
			}
			int proceso = Integer.parseInt(listaProcesoExcelBean.getListaProcesos().get(0).getIdProceso());
			actualizaProcBean.setProceso(proceso);
			List<ProcesoTmpBean> listaBSCS = new ArrayList<ProcesoTmpBean>();
			List<ProcesoTmpBean> listaSortedBSCS = new ArrayList<ProcesoTmpBean>();

			List<ProcesoTmpBean> listaSiop = new ArrayList<ProcesoTmpBean>();
			retornoActualizaProcBean = siopDAO.actualizarProcesoTmp(actualizaProcBean, mensajeTx);//Traer el excel al temporal
			if(retornoActualizaProcBean.getListaProcesoTmpBean() != null && retornoActualizaProcBean.getListaProcesoTmpBean().size() >=0  ){
				for(ProcesoTmpBean p:retornoActualizaProcBean.getListaProcesoTmpBean() ){
//					if(p.getRecibo().contains(propertiesExterno.serieDocumentoBscs)){
					if(validarSerieDocumentoBscs(p.getRecibo())){
						listaBSCS.add(p);
					}else{
						listaSiop.add(p);
					}

				}
				boolean flagError = false;
				String reciboComparador = null;
				if(listaBSCS.size() >0) {
					for(ProcesoTmpBean b: listaBSCS) {
						if(!b.getRecibo().equals(reciboComparador)) {
							listaSortedBSCS.add(b);
						}
						reciboComparador = b.getRecibo();
					}
				}
				
				if(listaSortedBSCS.size()>0){
					for(ProcesoTmpBean p:listaSortedBSCS ){

						inTemp = new InsertarTemporalBscsBean();
						inTemp.setProceso(p.getIdProceso());
						inTemp.setCustid(p.getCustId());
						inTemp.setRecibo(p.getRecibo());
						inTemp.setServicio(p.getServicio());
						inTemp.setEvento(p.getEvento());
						bscsDAO.insertarTemporalBSCS(inTemp, mensajeTx);
						
					}
					GenerarArchivoPlanoBean bean = new GenerarArchivoPlanoBean();
					GenerarArchivoPlanoBean retorno = new GenerarArchivoPlanoBean();
					bean.setCodigoProceso(listaProcesoExcelBean.getListaProcesos().get(0).getIdProceso());
					retorno = bscsDAO.generarArchivoPano(bean, mensajeTx);
					if(
							!retorno.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO) ||
							(retorno.getArchivoParcial()== null || retorno.getArchivoParcial().equals(Constantes.CADENAVACIA)) &&
							(retorno.getArchivoTotal()== null || retorno.getArchivoTotal().equals(Constantes.CADENAVACIA)) 
							
				    ){
						flagError = true;
					}else{
						String[] archivos = new String[3];					
						archivos[0] = retorno.getArchivoParcial();
						archivos[1] = retorno.getArchivoTotal();
						archivos[2] = retorno.getArchivoError();
						
						cargarAjustes(idTransaction,archivos);
						
						validaBSCS(listaBSCS.get(0),mensajeTx);//LGM:Proceso de validación de los T01 recibos cuyo detalle esta en BSCS						
					
					}

				}
				if(flagError == true){
					ActualizaEstadoBean actEstadoBean = new ActualizaEstadoBean();
					actEstadoBean.setIdProceso(Integer.parseInt(Constantes.CADENAVACIA + proceso));
					actEstadoBean.setEstado(Constantes.RECHAZADO_VALID);
					actEstadoBean = siopDAO.actualizarEstadoProc(actEstadoBean, mensajeTx);
					if(!actEstadoBean.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)){
						throw new DBException(actEstadoBean.getMensajeRespuesta());
					}
					
				}else{
					if(listaSiop.size()>0){
						validaSIOP(listaSiop.get(0), mensajeTx);
					}
				}

				
			}
		} catch (BeanCreationException e) {
			logger.error("ERROR [BeanCreationException] - : ", e);
			throw new BeanCreationException("Error BeanCreationException"
					+ e.getCause());
		} catch (DBException e) {
			logger.error("ERROR [DBException] - : ", e);
			throw new BeanCreationException("Error DBException"
					+ e.getCause());
		}catch (Exception e) {
			logger.error("ERROR [Exception] - : ", e);
			throw new BeanCreationException("Error BeanCreationException"
					+ e.getCause());
		}finally {
			logger.info(mensajeTx + "[Fin de metodo run]");

		}
		
	}


	void validaBSCS( ProcesoTmpBean p,String mensajeTx){
		
		ListarCabeceraAjusteBean driveListarCabecera = null;
		driveListarCabecera = new ListarCabeceraAjusteBean();
		driveListarCabecera.setIdProceso(p.getIdProceso());
		String codigoTipoAjuste = Constantes.CADENAVACIA;
		try {
			ListarCabeceraAjusteBean  localListarCabecera = siopDAO.listarCabeceraAjuste(driveListarCabecera, mensajeTx);//Obtener las cabeceras del proceso
			if( ( localListarCabecera.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)) &&
				  localListarCabecera.getListaAjusteCabecera().size() > 0
			   ){
				int contAjusteCab = 0;
//				lblAjusteCabecera:
				for(AjusteCabeceraBean cabeceraAjuste : localListarCabecera.getListaAjusteCabecera()){
					contAjusteCab++;
					codigoTipoAjuste = cabeceraAjuste.getCodigoAjuste();
					ListarDetalleArchPlanoBean listaDetalleReciboBSCS = new ListarDetalleArchPlanoBean();
					listaDetalleReciboBSCS.setIdProceso(cabeceraAjuste.getIdProceso().toString());
					listaDetalleReciboBSCS.setDocReferencia(cabeceraAjuste.getDocReferencia());
					listaDetalleReciboBSCS = siopDAO.listarDetalle(listaDetalleReciboBSCS, mensajeTx);//Obtener detalle del recibo en BSCS
					if(
						!listaDetalleReciboBSCS.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO) ||
						listaDetalleReciboBSCS.getListaDetalle().size() < 0
					  ){
						ActualizaEstadoBean actEstadoBean = new ActualizaEstadoBean();
						actEstadoBean.setId(cabeceraAjuste.getId());
						actEstadoBean.setEstado(Constantes.RECHAZADO_VALID);
						actEstadoBean = siopDAO.actualizarEstado(actEstadoBean, mensajeTx);
						if(!actEstadoBean.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)){
							throw new DBException(actEstadoBean.getMensajeRespuesta());
						}
//						break;
						continue;
					}
					if(codigoTipoAjuste.equals(Constantes.COD_TIPO_AJUSTE_TOTAL) || codigoTipoAjuste.equals(Constantes.COD_TIPO_AJUSTE_TOTAL_REFACTURACION) || codigoTipoAjuste.equals(Constantes.COD_TIPO_AJUSTE_REFACTURACION_TOTAL) ){
		
						double montoConIgv = 0;
						double igv = 0;
						int contDetallesInvalidos = 0;
						for(DetalleArchPlanoBean detalleArchivoPlano:listaDetalleReciboBSCS.getListaDetalle() ){
							if (!validarDetallePorcentaje(mensajeTx, detalleArchivoPlano, listaDetalleReciboBSCS.getListaDetalle())) {
								contDetallesInvalidos++;
//								ActualizaEstadoBean actEstadoBean = new ActualizaEstadoBean();
//								actEstadoBean.setId(cabeceraAjuste.getId());
//								actEstadoBean.setEstado(Constantes.RECHAZADO_VALID);
//								actEstadoBean = siopDAO.actualizarEstado(actEstadoBean, mensajeTx);
//								if(!actEstadoBean.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)){
//									throw new DBException(actEstadoBean.getMensajeRespuesta());
//								}
//								continue lblAjusteCabecera;
							}
							
							ResponseBase responseBean = null;
						
							if(detalleArchivoPlano.getAfectoigv().equals(Constantes.IS_AFECTOIGV)){
								
								igv = (100 + Double.parseDouble(detalleArchivoPlano.getIgv()==null?Constantes.IGV_DEFAULT:detalleArchivoPlano.getIgv()))/100;
								
								montoConIgv = Double.parseDouble(detalleArchivoPlano.getMontoserv()==null?Constantes.CADENA_NUMERO_NULO:detalleArchivoPlano.getMontoserv());
								
								montoConIgv = montoConIgv * igv;
							
							}else{
								
								montoConIgv = Double.parseDouble(detalleArchivoPlano.getMontoserv()==null?Constantes.CADENA_NUMERO_NULO:detalleArchivoPlano.getMontoserv());
								
							}
							ObtieneRubroBean  obtRubroBean = new ObtieneRubroBean();
							obtRubroBean.setSnCode(detalleArchivoPlano.getServ());
							obtRubroBean.setTipoAjuste(cabeceraAjuste.getTipoAjuste());
							obtRubroBean = siopDAO.obtenerRubro(obtRubroBean, mensajeTx);
							if(!obtRubroBean.getCodigoError().equals(Constantes.CODIGO_EXITO)) {
								throw new DBException(obtRubroBean.getMensajeError());
							}
							detalleArchivoPlano.setNombreRubro(obtRubroBean.getLista().get(0).getNombreRubro());
							detalleArchivoPlano.setConceptoRubro(obtRubroBean.getLista().get(0).getConceptoRubro());
							detalleArchivoPlano.setMontoConIgv(String.format("%.2f",montoConIgv ));
							responseBean = siopDAO.registrarDetalleRecBscsAMTotal(detalleArchivoPlano, mensajeTx, cabeceraAjuste.getId());
							if(!responseBean.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)) {
								throw new DBException(responseBean.getMensajeRespuesta());
							}//si hubo error al registrar el detalle
							responseBean = siopDAO.completaDatosCabecera(detalleArchivoPlano, mensajeTx, cabeceraAjuste.getId());/*LGM: Registrar la información de BSCS en el temporal para completar la informacion del recibo*/
							if(!responseBean.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)) {
								throw new DBException(responseBean.getMensajeRespuesta());
							}//si hubo error al completar la cabecera

						}
						/*LGM: Proceso de actualizar la cabecera*/
						//LGM: 1.Listar el detalle del temporal
						ListaDetalleTemporalBean listaDetalleTemporal = new ListaDetalleTemporalBean();
						listaDetalleTemporal.setId(cabeceraAjuste.getId());
						listaDetalleTemporal.setIdProceso(cabeceraAjuste.getIdProceso());
						listaDetalleTemporal.setDocReferencia(cabeceraAjuste.getDocReferencia());
						listaDetalleTemporal = siopDAO.listarDetalleTemporal(listaDetalleTemporal, mensajeTx);
						//LGM se declaran variables que almacenarán los montos
						double montoSinIgv = 0;
						double montoInafecto = 0;
						double montoContIgv = 0;
						
						if(listaDetalleTemporal.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO) && listaDetalleTemporal.getLista().size() > 0 ){
										
							for(DetalleTemporalBean detalleTemporal:listaDetalleTemporal.getLista()){
				
								montoSinIgv += Double.parseDouble(detalleTemporal.getMontosinigv()==null?Constantes.CADENA_NUMERO_NULO:detalleTemporal.getMontosinigv());
								
								if(detalleTemporal.getAfecto().equals(Constantes.IS_AFECTOIGV)){
							
									montoContIgv += Double.parseDouble(detalleTemporal.getMontoconigv()==null?Constantes.CADENA_NUMERO_NULO:detalleTemporal.getMontoconigv());
							
								}else{
					
									montoContIgv += Double.parseDouble(detalleTemporal.getMontosinigv()==null?Constantes.CADENA_NUMERO_NULO:detalleTemporal.getMontosinigv());
						
									montoInafecto += Double.parseDouble(detalleTemporal.getMontosinigv()==null?Constantes.CADENA_NUMERO_NULO:detalleTemporal.getMontosinigv());
									

								}
							}//LGM:Fin de calculo de los montos
							
							montoContIgv = Double.valueOf(Util.formatearNumero(montoContIgv));
							montoInafecto = Double.valueOf(Util.formatearNumero(montoInafecto));
							montoSinIgv = Double.valueOf(Util.formatearNumero(montoSinIgv));
							
							ActualizaMontoCabBscsBean actMontoBscsBean = new ActualizaMontoCabBscsBean();
							
							actMontoBscsBean.setId(cabeceraAjuste.getId());
						
							actMontoBscsBean.setMontoConIgv(montoContIgv);
						
							actMontoBscsBean.setMontoInafecto(montoInafecto);
						
							actMontoBscsBean.setMontoSinIgv(montoSinIgv);
							
							actMontoBscsBean = siopDAO.actualizarMontoCab(actMontoBscsBean, mensajeTx);// LGM Actualizar montos en la cabecera
							if(!actMontoBscsBean.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)){
								throw new DBException(actMontoBscsBean.getMensajeRespuesta());
							}//LGM Validar Actualizacion de Montos en Caso de Error
							
						}else{
							throw new DBException(listaDetalleTemporal.getMensajeRespuesta());
						}

						ActualizaEstadoBean actEstadoBean = new ActualizaEstadoBean();
						actEstadoBean.setId(cabeceraAjuste.getId());
						
						if (contDetallesInvalidos > 0) {
							actEstadoBean.setEstado(Constantes.RECHAZADO_VALID);
							actEstadoBean = siopDAO.actualizarEstado(actEstadoBean, mensajeTx);
							if(!actEstadoBean.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)){
								throw new DBException(actEstadoBean.getMensajeRespuesta());
							}
							continue;
						}
						
						ListaDatosAjusteSiop lbean = new ListaDatosAjusteSiop();
						lbean.setDocReferencia(cabeceraAjuste.getDocReferencia());
						lbean.setTipoAjuste(Constantes.TIPO_AJUSTE_AJUSTES);
						ListaDatosAjusteSiop listarAjustes = siopDAO.listarDatosAjusteSiop(lbean, mensajeTx);// Se consultan los ajustes del T01 en los históricos   
						if((listarAjustes.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)) && listarAjustes.getListaDatosAjusteSiop() != null && listarAjustes.getListaDatosAjusteSiop().size()>0 ){
							actEstadoBean.setEstado(Constantes.RECHAZADO_VALID);
							actEstadoBean = siopDAO.actualizarEstado(actEstadoBean, mensajeTx);
							if(!actEstadoBean.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)){
								throw new DBException(actEstadoBean.getMensajeRespuesta());
							}
						}else{
							actEstadoBean.setEstado(Constantes.APROB_VALID);
							actEstadoBean = siopDAO.actualizarEstado(actEstadoBean, mensajeTx);
							if(!actEstadoBean.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)){
								throw new DBException(actEstadoBean.getMensajeRespuesta());
							}
						}/*LGM:Proceso de validación: si es ajuste total no debe de tener ajustes en el historico*/
					}else{
						//LGM:.-Lógica de ajuste Parcial para un tipo de doc Referencia T001
						//Listar los detalles del temporal para saber los detalles a ajustar
						ActualizaEstadoBean actEstadoBean = new ActualizaEstadoBean();
						actEstadoBean.setId(cabeceraAjuste.getId());
						ListaDetalleTemporalBean listaDetalleTemporal = new ListaDetalleTemporalBean();
						listaDetalleTemporal.setId(cabeceraAjuste.getId());
						listaDetalleTemporal.setIdProceso(cabeceraAjuste.getIdProceso());
						listaDetalleTemporal.setDocReferencia(cabeceraAjuste.getDocReferencia());
						listaDetalleTemporal = siopDAO.listarDetalleTemporal(listaDetalleTemporal, mensajeTx);
						double montoAjustable = 0;
						double montoAjustar = 0;
						double montoDocRef = 0;
						double montoAjustes = 0;
						if(listaDetalleTemporal.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO) && listaDetalleTemporal.getLista().size() > 0 ){
							
							ListaDatosAjusteSiop listaDocRefAjuste = new ListaDatosAjusteSiop();
							if(listaDetalleReciboBSCS.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO) && listaDetalleReciboBSCS.getListaDetalle().size() > 0 ){
								for(DetalleTemporalBean detalleTemporal:listaDetalleTemporal.getLista() ){

									ListarDetalleArchPlanoBean listarBscs = new ListarDetalleArchPlanoBean();									
									listarBscs.setTipoAjuste(cabeceraAjuste.getCodigoAjuste());
									listarBscs.setTmCode(detalleTemporal.getTmCode());
									listarBscs.setSnCode(detalleTemporal.getSncode());
									listarBscs.setCuentaContable(detalleTemporal.getCuentacontable());
									listarBscs.setCustomerId(detalleTemporal.getCustomerid());
									listarBscs.setDocReferencia(detalleTemporal.getDocreferencia());
									listarBscs.setIdProceso(detalleTemporal.getIdproceso().toString());
									listarBscs = siopDAO.listarDetalleBscsAjuste(listarBscs, mensajeTx);
									if(!listarBscs.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO_0) || listarBscs.getListaDetalle().size() == 0) {
										detalleTemporal.setFound(false);
									} else if (Constantes.COD_TIPO_AJUSTE_PARCIAL_CARGO_FIJO.equals(codigoTipoAjuste) 
											|| Constantes.COD_TIPO_AJUSTE_REFACTURACION.equals(codigoTipoAjuste)) {
										detalleTemporal.setFound(true);
//										detalleTemporal.setArchivoAsociado(listarBscs.getListaDetalle().get(0));
										detalleTemporal.setListaArchivoAsociado(listarBscs.getListaDetalle());
									} else {
										detalleTemporal.setFound(true);
										detalleTemporal.setArchivoAsociado(listarBscs.getListaDetalle().get(0));
									}
								 }//LGM: Listar temporal
							     int contador = 0;
							     for(DetalleTemporalBean detalleTemporal:listaDetalleTemporal.getLista() ){
							    	 if(detalleTemporal.isFound() == false)
							    		 contador++;
							     }//contamos los temporales no empatados
							     if(contador > 0) {//sin un temporal no es empatado se rechaza el registro
							    	actEstadoBean.setId(cabeceraAjuste.getId());
									actEstadoBean.setEstado(Constantes.RECHAZADO_VALID);
									actEstadoBean = siopDAO.actualizarEstado(actEstadoBean, mensajeTx);
							     }else {
							    	 listaDetalleTemporal = calcularMontoSegunPorcentaje(mensajeTx, cabeceraAjuste, listaDetalleTemporal, 
							    			 Constantes.PROCESO_BSCS);
							    	 if (listaDetalleTemporal == null) {
							    		 actEstadoBean.setId(cabeceraAjuste.getId());
										 actEstadoBean.setEstado(Constantes.RECHAZADO_VALID);
										 actEstadoBean = siopDAO.actualizarEstado(actEstadoBean, mensajeTx);
							    		 continue;
							    	 }
							    	 
							 		 double montoSinIgv = 0;
									 double montoInafecto = 0;
									 double montoContIgv = 0;	 
							    	 for(DetalleTemporalBean detalleTemporal:listaDetalleTemporal.getLista() ){
							    		 /*Logica del calculo del monto con IGV*/
							    		 double igv = 0;
							    		 double montoConIgv = 0;
							    		 if(detalleTemporal.getArchivoAsociado().getAfectoigv().equals(Constantes.IS_AFECTOIGV)){
							    				
							    				igv = (100 + Double.parseDouble(detalleTemporal.getArchivoAsociado().getIgv()==null?Constantes.IGV_DEFAULT:detalleTemporal.getArchivoAsociado().getIgv()))/100;
							    				
							    				montoConIgv = Double.parseDouble(detalleTemporal.getMontosinigv()==null?Constantes.CADENA_NUMERO_NULO:detalleTemporal.getMontosinigv());
							
							    				montoConIgv = montoConIgv * igv;
							    				
							    			}else{
							    			
							    				montoConIgv = Double.parseDouble(detalleTemporal.getMontosinigv()==null?Constantes.CADENA_NUMERO_NULO:detalleTemporal.getMontosinigv());
							    			
							    			}
											ObtieneRubroBean  obtRubroBean = new ObtieneRubroBean();
											obtRubroBean.setSnCode(detalleTemporal.getArchivoAsociado().getServ());
											obtRubroBean.setTipoAjuste(cabeceraAjuste.getTipoAjuste());
											obtRubroBean = siopDAO.obtenerRubro(obtRubroBean, mensajeTx);
											if(!obtRubroBean.getCodigoError().equals(Constantes.CODIGO_EXITO)) {
												throw new DBException(obtRubroBean.getMensajeError());
											}
										detalleTemporal.getArchivoAsociado().setNombreRubro(obtRubroBean.getLista().get(0).getNombreRubro());
										detalleTemporal.getArchivoAsociado().setConceptoRubro(obtRubroBean.getLista().get(0).getConceptoRubro());
							    		 ActualizaMtoCabBscsBean mtoCab = new ActualizaMtoCabBscsBean();
							    		 mtoCab.setId(Constantes.CADENAVACIA+detalleTemporal.getId().toString());
							    		 mtoCab.setTmCode(detalleTemporal.getTmCode());
							    		 mtoCab.setSnCode(detalleTemporal.getSncode());
							    		 mtoCab.setMtoConIgv(String.format("%.2f",montoConIgv ));
							    		 mtoCab.setIdProceso(detalleTemporal.getIdproceso().toString());
							    		 mtoCab.setEvento(detalleTemporal.getEvento());
							    		 mtoCab.setRowid(detalleTemporal.getRowid());
							    		 mtoCab = siopDAO.actualizarMtoIgv(mtoCab, mensajeTx);
							    		 if(!mtoCab.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)){
												throw new DBException(mtoCab.getMensajeRespuesta());
											}
											ResponseBase responseBean = siopDAO.completaDatosCabecera(detalleTemporal.getArchivoAsociado(), mensajeTx, cabeceraAjuste.getId());/*LGM: Registrar la información de BSCS en el temporal para completar la informacion del recibo*/
											if(!responseBean.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)) {
												throw new DBException(responseBean.getMensajeRespuesta());
											}//si hubo error al completar la cabecera
											DetalleArchPlanoBean completaDetalleBean = detalleTemporal.getArchivoAsociado();
											completaDetalleBean.setRowid(detalleTemporal.getRowid());
											responseBean = siopDAO.completaDetalle(completaDetalleBean, mensajeTx, cabeceraAjuste.getId());
											if(!responseBean.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)) {
												throw new DBException(responseBean.getMensajeRespuesta());
											}
							    		 ///////////
							    		 	detalleTemporal.setMontoconigv(String.format("%.2f",montoConIgv ));
											
											montoSinIgv += Double.parseDouble(detalleTemporal.getMontosinigv()==null?Constantes.CADENA_NUMERO_NULO:detalleTemporal.getMontosinigv());
							
											if(detalleTemporal.getArchivoAsociado().getAfectoigv().equals(Constantes.IS_AFECTOIGV)){
										
												montoContIgv += Double.parseDouble(detalleTemporal.getMontoconigv()==null?Constantes.CADENA_NUMERO_NULO:detalleTemporal.getMontoconigv());
										
											}else{
									
												montoContIgv += Double.parseDouble(detalleTemporal.getMontosinigv()==null?Constantes.CADENA_NUMERO_NULO:detalleTemporal.getMontosinigv());
											
												montoInafecto += Double.parseDouble(detalleTemporal.getMontosinigv()==null?Constantes.CADENA_NUMERO_NULO:detalleTemporal.getMontosinigv());
											

											}
							    		 
											if(detalleTemporal.getArchivoAsociado().getAfectoigv().equals(Constantes.IS_AFECTOIGV)){
												igv = (100 + Double.parseDouble(detalleTemporal.getArchivoAsociado().getIgv()==null?Constantes.IGV_DEFAULT:detalleTemporal.getArchivoAsociado().getIgv()))/100;
												montoDocRef = Double.parseDouble(detalleTemporal.getArchivoAsociado().getMontoserv()==null?Constantes.CADENA_NUMERO_NULO:detalleTemporal.getArchivoAsociado().getMontoserv());
												montoDocRef = montoDocRef * igv;
											}else{
												montoDocRef = Double.parseDouble(detalleTemporal.getArchivoAsociado().getMontoserv()==null?Constantes.CADENA_NUMERO_NULO:detalleTemporal.getArchivoAsociado().getMontoserv());
											}
											//LGM INICIO --> Obtener el monto ajustar si es afecto a igv sera montoConIgv caso contrario montoSiIgv
//											if(detalleTemporal.getAfecto().equals(Constantes.IS_AFECTOIGV)){
//												montoAjustar = Double.parseDouble(detalleTemporal.getMontoconigv()==null?Constantes.CADENA_NUMERO_NULO:detalleTemporal.getMontoconigv());
//											}else{
//												montoAjustar = Double.parseDouble(detalleTemporal.getMontosinigv()==null?Constantes.CADENA_NUMERO_NULO:detalleTemporal.getMontosinigv());
//											}//LGM FIN --> Obtener el monto ajustar si es afecto a igv sera montoConIgv caso contrario montoSiIgv
											montoAjustar = obtenerMontoAjusteBscs(mensajeTx, detalleTemporal, listaDetalleTemporal.getLista());
											
											listaDocRefAjuste.setTipoAjuste(Constantes.TIPO_AJUSTE_HIS);
											listaDocRefAjuste.setDocReferencia(cabeceraAjuste.getDocReferencia());
											listaDocRefAjuste = siopDAO.listarDatosAjusteSiop(listaDocRefAjuste, mensajeTx);
											//LGM: inicio --> validar si tiene tipos de ajustes REC
											if(listaDocRefAjuste.getCodigoRespuesta().equals(Constantes.CODIGO_EXIS_REC)){
												//LGM: inicio Si tiene data de tipo REC, se considera como un error y se procede a actualizar
												actEstadoBean.setId(cabeceraAjuste.getId());
												actEstadoBean.setEstado(Constantes.RECHAZADO_VALID);
												actEstadoBean = siopDAO.actualizarEstado(actEstadoBean, mensajeTx);
												//LGM: fin Si tiene data de tipo REC, se considera como un error y se procede a actualizar
											}else if(listaDocRefAjuste.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)
													&& listaDocRefAjuste.getListaDatosAjusteSiop().size()>0){
												//LGM se trajeron correctamente los ajustes históricos
												//LGM inicio se recorren los ajustes históricos
												for(DatosAjusteSiop ajusteHistorico:listaDocRefAjuste.getListaDatosAjusteSiop()){
													if(
															detalleTemporal.getArchivoAsociado().getServ().equals(ajusteHistorico.getServ()) &&
															detalleTemporal.getArchivoAsociado().getPlan().equals(ajusteHistorico.getPlan()) &&
															detalleTemporal.getArchivoAsociado().getCtaper().equals(ajusteHistorico.getCtaPer())
													  ){
														if(ajusteHistorico.getTipoAjuste().equals(Constantes.CAMPO_TIPO_AJUSTE_NC)){
															if(ajusteHistorico.getAfectoIgv().equals(Constantes.AFECTOIGV)){
																montoAjustes += -1 *(Double.parseDouble(ajusteHistorico.getMontoConIgv()==null?Constantes.CADENA_NUMERO_NULO:ajusteHistorico.getMontoConIgv()));
															}else{
																montoAjustes += Double.parseDouble(ajusteHistorico.getMontoSinIgv()==null?Constantes.CADENA_NUMERO_NULO:ajusteHistorico.getMontoSinIgv());

															}
														}
													}
												}//LGM fin se recorren los ajustes históricos
												if(cabeceraAjuste.getTipoAjuste().equals(Constantes.CAMPO_TIPO_AJUSTE_NC)) {
													montoAjustable = montoDocRef + montoAjustes;//Calculo del monto ajustable
													if(montoAjustable >= montoAjustar){		
														actEstadoBean.setId(cabeceraAjuste.getId());
														actEstadoBean.setEstado(Constantes.APROB_VALID);
														actEstadoBean = siopDAO.actualizarEstado(actEstadoBean, mensajeTx);
													}else{
														actEstadoBean.setId(cabeceraAjuste.getId());
														actEstadoBean.setEstado(Constantes.RECHAZADO_VALID);
														actEstadoBean = siopDAO.actualizarEstado(actEstadoBean, mensajeTx);
													}
												}else {
													actEstadoBean.setId(cabeceraAjuste.getId());
													actEstadoBean.setEstado(Constantes.APROB_VALID);
													actEstadoBean = siopDAO.actualizarEstado(actEstadoBean, mensajeTx);
												}
	
												
											}else if(listaDocRefAjuste.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)
													&& listaDocRefAjuste.getListaDatosAjusteSiop().size() == 0){
												logger.info(mensajeTx + " No hay historico con doc referencia: " + cabeceraAjuste.getDocReferencia());
												if(cabeceraAjuste.getTipoAjuste().equals(Constantes.CAMPO_TIPO_AJUSTE_NC)) {
													logger.info(mensajeTx + " El tipo de ajuste es: " + cabeceraAjuste.getTipoAjuste());
													montoAjustable = montoDocRef;
													logger.info(mensajeTx + " montoAjustable: " + montoAjustable);
													logger.info(mensajeTx + " montoAjustar: " + montoAjustar);
													if (montoAjustable >= montoAjustar) {
														actEstadoBean.setId(cabeceraAjuste.getId());
														actEstadoBean.setEstado(Constantes.APROB_VALID);
														actEstadoBean = siopDAO.actualizarEstado(actEstadoBean, mensajeTx);
														logger.info(mensajeTx + " Se aprueba validacion para doc referencia: " + cabeceraAjuste.getDocReferencia());
													} else {
														actEstadoBean.setId(cabeceraAjuste.getId());
														actEstadoBean.setEstado(Constantes.RECHAZADO_VALID);
														actEstadoBean = siopDAO.actualizarEstado(actEstadoBean, mensajeTx);
														logger.info(mensajeTx + " Se rechaza validacion para doc referencia: " + cabeceraAjuste.getDocReferencia());
													}
												} else {
													actEstadoBean.setEstado(Constantes.APROB_VALID);
													actEstadoBean.setId(cabeceraAjuste.getId());
													actEstadoBean = siopDAO.actualizarEstado(actEstadoBean, mensajeTx);
												}
											}else{
												throw new DBException(listaDocRefAjuste.getMensajeRespuesta());
											}
							    	 }
							    	 ////
							    	 
							    	 	montoContIgv = Double.valueOf(Util.formatearNumero(montoContIgv));
										montoInafecto = Double.valueOf(Util.formatearNumero(montoInafecto));
										montoSinIgv = Double.valueOf(Util.formatearNumero(montoSinIgv));
							    	 
										ActualizaMontoCabBscsBean actMontoBscsBean = new ActualizaMontoCabBscsBean();
									
										actMontoBscsBean.setId(cabeceraAjuste.getId());
									
										actMontoBscsBean.setMontoConIgv(montoContIgv);
										
										actMontoBscsBean.setMontoInafecto(montoInafecto);
									
										actMontoBscsBean.setMontoSinIgv(montoSinIgv);
										
										actMontoBscsBean = siopDAO.actualizarMontoCab(actMontoBscsBean, mensajeTx);// LGM Actualizar montos en la cabecera
										if(!actMontoBscsBean.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)){
											throw new DBException(actMontoBscsBean.getMensajeRespuesta());
										}
							     }

									//LGM obtener los datos historicos que hagan match con el detalle del temporal a nivel de sncode,plan,lineañevento
									//}//LGM: Listar información en el histórico
								
							}else{
								throw new DBException(listaDetalleReciboBSCS.getMensajeRespuesta());
							}
						}else{
							throw new DBException(listaDetalleTemporal.getMensajeRespuesta());
						}

					}
					
					if ((!codigoTipoAjuste.equals(Constantes.COD_TIPO_AJUSTE_REFACTURACION_TOTAL) && !codigoTipoAjuste.equals(Constantes.COD_TIPO_AJUSTE_REFACTURACION))
							|| contAjusteCab == localListarCabecera.getListaAjusteCabecera().size()) {
						logger.info(mensajeTx + " Eliminar datos tabla temporal BSCS en SIOPDB");
						siopDAO.eliminarTmpBscs(mensajeTx, cabeceraAjuste.getIdProceso(), cabeceraAjuste.getDocReferencia());
					}
				}
			}else{
				throw new DBException(localListarCabecera.getMensajeRespuesta());	
			}

		} catch (DBException e) {
			logger.error("ERROR [DBException] - : ", e);
			throw new BeanCreationException("Error DBException: " + e.getCause());
		} catch (CloneNotSupportedException e) {
			logger.error("ERROR [CloneNotSupportedException] - : ", e);
			throw new BeanCreationException("Error CloneNotSupportedException: " + e.getCause());
		}
	}
	
	void validaSIOP(ProcesoTmpBean p,String mensajeTx){

		ListarCabeceraAjusteBean driveListarCabecera = null;
		driveListarCabecera = new ListarCabeceraAjusteBean();
		driveListarCabecera.setIdProceso(p.getIdProceso());
		String tipoAjuste = Constantes.CADENAVACIA;
		try {
			ListarCabeceraAjusteBean  localListarCabecera = siopDAO.listarCabeceraAjuste(driveListarCabecera, mensajeTx);//Obtener las cabeceras del proceso
			if( ( localListarCabecera.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)) &&
				  localListarCabecera.getListaAjusteCabecera().size() > 0
			   ){
				for(AjusteCabeceraBean cabeceraAjuste : localListarCabecera.getListaAjusteCabecera()){
					tipoAjuste = cabeceraAjuste.getCodigoAjuste();
					if(tipoAjuste.equals(Constantes.COD_TIPO_AJUSTE_TOTAL)|| tipoAjuste.equals(Constantes.COD_TIPO_AJUSTE_TOTAL_REFACTURACION) || tipoAjuste.equals(Constantes.COD_TIPO_AJUSTE_REFACTURACION_TOTAL)){
						ListaDatosAjusteSiop lbean = new ListaDatosAjusteSiop();
						lbean.setDocReferencia(cabeceraAjuste.getDocReferencia());
						lbean.setTipoAjuste(Constantes.TIPO_AJUSTE_AJUSTES);
						ListaDatosAjusteSiop listarAjustes = siopDAO.listarDatosAjusteSiop(lbean, mensajeTx);// Se consultan los ajustes del T01 en los históricos   
						ActualizaEstadoBean actEstadoBean = new ActualizaEstadoBean();
						actEstadoBean.setId(cabeceraAjuste.getId());
						if((listarAjustes.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)) && listarAjustes.getListaDatosAjusteSiop() != null && listarAjustes.getListaDatosAjusteSiop().size()<=0 ){
							actEstadoBean.setEstado(Constantes.RECHAZADO_VALID);
							actEstadoBean = siopDAO.actualizarEstado(actEstadoBean, mensajeTx);
							if(!actEstadoBean.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)){
								throw new DBException(actEstadoBean.getMensajeRespuesta());
							}
						}else{
							ActualizaMontoCabBscsBean actualizaMontoCab = registrarDetalleAjusteTotalSB99(mensajeTx, listarAjustes.getListaDatosAjusteSiop(), cabeceraAjuste);
							
							completarCabeceraSB99(mensajeTx, cabeceraAjuste, listarAjustes.getListaDatosAjusteSiop().get(Constantes.CERO_INT));
							
							if (actualizaMontoCab == null) {
								actEstadoBean.setEstado(Constantes.RECHAZADO_VALID);
								actEstadoBean = siopDAO.actualizarEstado(actEstadoBean, mensajeTx);
								if(!actEstadoBean.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)){
									throw new DBException(actEstadoBean.getMensajeRespuesta());
								}
								continue;
							}
							
							actualizaMontoCab = siopDAO.actualizarMontoCab(actualizaMontoCab, mensajeTx);
							if(!actualizaMontoCab.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)){
								throw new DBException(actualizaMontoCab.getMensajeRespuesta());
							}
							
							actEstadoBean.setEstado(Constantes.APROB_VALID);
							actEstadoBean = siopDAO.actualizarEstado(actEstadoBean, mensajeTx);
							if(!actEstadoBean.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)){
								throw new DBException(actEstadoBean.getMensajeRespuesta());
							}
						}/*LGM:Proceso de validación: si es ajuste total no debe de tener ajustes en el historico*/
					}else{
						//LGM:.-Lógica de ajuste Parcial para un tipo de doc Referencia SB99
						//Listar los detalles del temporal para saber los detalles a ajustar
						ListaDetalleTemporalBean listaDetalleTemporal = new ListaDetalleTemporalBean();
						listaDetalleTemporal.setId(cabeceraAjuste.getId());
						listaDetalleTemporal.setIdProceso(cabeceraAjuste.getIdProceso());
						listaDetalleTemporal.setDocReferencia(cabeceraAjuste.getDocReferencia());
						listaDetalleTemporal = siopDAO.listarDetalleTemporal(listaDetalleTemporal, mensajeTx);
						//double montoAjustable = 0;
						//double montoAjustar = 0;
						//double montoDocRef = 0;
						//double montoAjustes = 0;
						double montoRecibo = 0;
						double montoMaxAjustar = 0;
						if(listaDetalleTemporal.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO) && listaDetalleTemporal.getLista().size() > 0 ){
							//Listar Data del historico... como nroDocAjuste el SB99
							ListaDatosAjusteSiop listaDetDocAjuste = new ListaDatosAjusteSiop();
							//ListaDatosAjusteSiop listaDocRefAjuste = new ListaDatosAjusteSiop();
							listaDetDocAjuste.setDocAjuste(cabeceraAjuste.getDocReferencia());
							listaDetDocAjuste = siopDAO.listarDetalleHistoricoDocAjuste(listaDetDocAjuste, mensajeTx);
							if(listaDetDocAjuste.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO) && listaDetDocAjuste.getListaDatosAjusteSiop().size() > 0 ){
								
								for(DetalleTemporalBean detalleTemporal:listaDetalleTemporal.getLista() ){												
									ListarDetalleArchPlanoBean listarBscs = new ListarDetalleArchPlanoBean();
									ListaDatosAjusteSiop listaHistSiop = null;
									listarBscs.setTipoAjuste(cabeceraAjuste.getCodigoAjuste());
									listarBscs.setTmCode(detalleTemporal.getTmCode());
									listarBscs.setSnCode(detalleTemporal.getSncode());
									listarBscs.setCuentaContable(detalleTemporal.getCuentacontable());
									listarBscs.setCustomerId(detalleTemporal.getCustomerid());
									listarBscs.setDocReferencia(detalleTemporal.getDocreferencia());//Se consulta por el documento de ajuste
									listarBscs.setIdProceso(detalleTemporal.getIdproceso().toString());
									listaHistSiop = siopDAO.listarDetalleHistoricoSiop(listarBscs, mensajeTx);
									if(!listaHistSiop.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO_0) || listaHistSiop.getListaDatosAjusteSiop().size() == 0) {
										detalleTemporal.setFound(false);
									} else if (Constantes.COD_TIPO_AJUSTE_PARCIAL_CARGO_FIJO.equals(tipoAjuste) 
											|| Constantes.COD_TIPO_AJUSTE_REFACTURACION.equals(tipoAjuste)) {
										detalleTemporal.setFound(true);
										detalleTemporal.setListaAjustesDocRef(listaHistSiop.getListaDatosAjusteSiop());
									}else {
										detalleTemporal.setFound(true);
										detalleTemporal.setAjusteHistoricoAsociado(listaHistSiop.getListaDatosAjusteSiop().get(0));
									}
								 }//LGM: Listar temporal
							     int contador = 0;
							     for(DetalleTemporalBean detalleTemporal:listaDetalleTemporal.getLista() ){
							    	 if(detalleTemporal.isFound() == false)
							    		 contador++;
							     }//contamos los temporales no empatados
							     if(contador > 0) {//sin un temporal no es empatado se rechaza el registro
								    ActualizaEstadoBean actEstadoBean = new ActualizaEstadoBean();
							    	actEstadoBean.setId(cabeceraAjuste.getId());
									actEstadoBean.setEstado(Constantes.RECHAZADO_VALID);
									actEstadoBean = siopDAO.actualizarEstado(actEstadoBean, mensajeTx);
							     }else {
								    	listaDetalleTemporal = calcularMontoSegunPorcentaje(mensajeTx, cabeceraAjuste, listaDetalleTemporal, Constantes.CADENA_SIOP);
								    	if (listaDetalleTemporal == null) {
								    		ActualizaEstadoBean actEstadoBean = new ActualizaEstadoBean();
								    		actEstadoBean.setId(cabeceraAjuste.getId());
											actEstadoBean.setEstado(Constantes.RECHAZADO_VALID);
											actEstadoBean = siopDAO.actualizarEstado(actEstadoBean, mensajeTx);
								    		continue;
								    	}
							    	 
										double montoSinIgv = 0;
										double montoInafecto = 0;
										double montoContIgv = 0;
										for(DetalleTemporalBean detalleTemporal:listaDetalleTemporal.getLista() ){
											// Obtener los detalles del documento de referencia
											ListarDetalleArchPlanoBean beanConsultaHistorico = new ListarDetalleArchPlanoBean();
											ListaDatosAjusteSiop listaAjustesHistoricosDocRef = null;
											beanConsultaHistorico.setTipoAjuste(cabeceraAjuste.getCodigoAjuste());
											beanConsultaHistorico.setTmCode(detalleTemporal.getTmCode());
											beanConsultaHistorico.setSnCode(detalleTemporal.getSncode());
											beanConsultaHistorico.setCuentaContable(detalleTemporal.getCuentacontable());
											beanConsultaHistorico.setCustomerId(detalleTemporal.getCustomerid());
											beanConsultaHistorico.setDocReferencia(detalleTemporal.getDocreferencia());//Se consulta por el documento de referencia
											beanConsultaHistorico.setIdProceso(Constantes.DOC_REFERENCIA);
											listaAjustesHistoricosDocRef = siopDAO.listarDetalleHistoricoSiop(beanConsultaHistorico, mensajeTx);
											if(listaAjustesHistoricosDocRef.getCodigoRespuesta().equals(Constantes.PO_REFAC)) {
												ActualizaEstadoBean actEstadoBean = new ActualizaEstadoBean();
												actEstadoBean.setId(cabeceraAjuste.getId());
												actEstadoBean.setEstado(Constantes.RECHAZADO_VALID);
												actEstadoBean = siopDAO.actualizarEstado(actEstadoBean, mensajeTx);
											}else if(!listaAjustesHistoricosDocRef.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO_0) || listaAjustesHistoricosDocRef.getListaDatosAjusteSiop().size() == 0) {
												detalleTemporal.setFoundHistoricoDocRef(false);
											}else {
												detalleTemporal.setFoundHistoricoDocRef(true);
												detalleTemporal.setListaAjustesDocRef(listaAjustesHistoricosDocRef.getListaDatosAjusteSiop());
											}

											double igv = 0;
								    		double montoConIgv = 0;
								    		double conIgv = 0;
								    		double sinIgv = 0;
								    		if(detalleTemporal.getAjusteHistoricoAsociado().getAfectoIgv().equals(Constantes.IS_AFECTOIGV)){
								    			 
							    			    conIgv = Double.parseDouble(detalleTemporal.getAjusteHistoricoAsociado().getMontoConIgv()==null?Constantes.CADENA_NUMERO_NULO:detalleTemporal.getAjusteHistoricoAsociado().getMontoConIgv());
							    				sinIgv = Double.parseDouble(detalleTemporal.getAjusteHistoricoAsociado().getMontoSinIgv()==null?Constantes.CADENA_NUMERO_NULO:detalleTemporal.getAjusteHistoricoAsociado().getMontoSinIgv());
							    				igv = conIgv/sinIgv;
							    				
							    				montoConIgv = Double.parseDouble(detalleTemporal.getMontosinigv()==null?Constantes.CADENA_NUMERO_NULO:detalleTemporal.getMontosinigv());
							
							    				montoConIgv = montoConIgv * igv;
							    				
							    			}else{
							    			
							    				montoConIgv = Double.parseDouble(detalleTemporal.getMontosinigv()==null?Constantes.CADENA_NUMERO_NULO:detalleTemporal.getMontosinigv());
							    			
							    			}
							    		 
											ObtieneRubroBean  obtRubroBean = new ObtieneRubroBean();
											obtRubroBean.setSnCode(detalleTemporal.getAjusteHistoricoAsociado().getServ());
											obtRubroBean.setTipoAjuste(cabeceraAjuste.getTipoAjuste());
											obtRubroBean = siopDAO.obtenerRubro(obtRubroBean, mensajeTx);
											if(!obtRubroBean.getCodigoError().equals(Constantes.CODIGO_EXITO)) {
												throw new DBException(obtRubroBean.getMensajeError());
											}
								    		 ActualizaMtoCabBscsBean mtoCab = new ActualizaMtoCabBscsBean();
								    		 mtoCab.setId(Constantes.CADENAVACIA+detalleTemporal.getId().toString());
								    		 mtoCab.setTmCode(detalleTemporal.getTmCode());
								    		 mtoCab.setSnCode(detalleTemporal.getSncode());
								    		 mtoCab.setMtoConIgv(String.format("%.2f",montoConIgv ));
								    		 mtoCab.setIdProceso(detalleTemporal.getIdproceso().toString());
								    		 mtoCab.setEvento(detalleTemporal.getEvento());
								    		 mtoCab.setRowid(detalleTemporal.getRowid());
								    		 mtoCab = siopDAO.actualizarMtoIgv(mtoCab, mensajeTx);
								    		 if(!mtoCab.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)){
												throw new DBException(mtoCab.getMensajeRespuesta());
											 }
								    		 detalleTemporal.setMontoconigv(Constantes.CADENAVACIA+montoConIgv);
								    		 DetalleArchPlanoBean detalleArchivoPlano = Util.convertir(detalleTemporal.getAjusteHistoricoAsociado());
								    		 detalleArchivoPlano.setNombreRubro(obtRubroBean.getLista().get(0).getNombreRubro());
								    		 detalleArchivoPlano.setConceptoRubro(obtRubroBean.getLista().get(0).getConceptoRubro());
								    		 detalleArchivoPlano.setIdproceso(detalleTemporal.getIdproceso().toString());
												ResponseBase responseBean = siopDAO.completaDatosCabecera(detalleArchivoPlano, mensajeTx, cabeceraAjuste.getId());/*LGM: Registrar la información de BSCS en el temporal para completar la informacion del recibo*/
												if(!responseBean.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)) {
													throw new DBException(responseBean.getMensajeRespuesta());
												}//si hubo error al completar la cabecera
												detalleArchivoPlano.setRowid(detalleTemporal.getRowid());
												responseBean = siopDAO.completaDetalle(detalleArchivoPlano, mensajeTx, cabeceraAjuste.getId());
												if(!responseBean.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)) {
													throw new DBException(responseBean.getMensajeRespuesta());
												}
												
//												if(detalleTemporal.getAjusteHistoricoAsociado().getAfectoIgv().equals(Constantes.IS_AFECTOIGV)) {
//													montoRecibo = Double.parseDouble(detalleTemporal.getAjusteHistoricoAsociado().getMontoConIgv()==null?Constantes.CADENA_NUMERO_NULO:detalleTemporal.getAjusteHistoricoAsociado().getMontoConIgv());
//												}else {
//													montoRecibo = Double.parseDouble(detalleTemporal.getAjusteHistoricoAsociado().getMontoSinIgv()==null?Constantes.CADENA_NUMERO_NULO:detalleTemporal.getAjusteHistoricoAsociado().getMontoSinIgv());
//												}
												montoRecibo = obtenerMontoRecibo(mensajeTx, detalleTemporal, listaDetalleTemporal.getLista());
												montoMaxAjustar = montoRecibo;
												if (detalleTemporal.isFoundHistoricoDocRef()) {
													for(DatosAjusteSiop r:detalleTemporal.getListaAjustesDocRef()) {
														double monto= 0;
														if(r.getAfectoIgv().equals(Constantes.IS_AFECTOIGV)) {
															monto = Double.parseDouble(r.getMontoConIgv()==null?Constantes.CADENA_NUMERO_NULO:r.getMontoConIgv());
														}else {
															monto = Double.parseDouble(r.getMontoSinIgv()==null?Constantes.CADENA_NUMERO_NULO:r.getMontoSinIgv());
														}
														
														if(r.getTipoAjuste().equals(Constantes.CAMPO_TIPO_AJUSTE_NC)) {
															montoMaxAjustar = montoMaxAjustar + monto;
														}else {
															montoMaxAjustar = montoMaxAjustar - monto;
														}
														
													}
												}
												//Fin de los ajustes del doc de referencia
//												double montoValidar = Double.parseDouble(detalleTemporal.getMontoconigv()==null?Constantes.CADENA_NUMERO_NULO:detalleTemporal.getMontoconigv());
//												double montoValidar = obtenerMontoAjuste(mensajeTx, detalleTemporal, listaDetalleTemporal.getLista(), Constantes.PROCESO_SIOP);
												double montoValidar = obtenerMontoAjusteSiop(mensajeTx, detalleTemporal, listaDetalleTemporal.getLista());
												if( montoMaxAjustar >= montoValidar ) {
													ActualizaEstadoBean actEstadoBean = new ActualizaEstadoBean();
													actEstadoBean.setId(cabeceraAjuste.getId());
													actEstadoBean.setEstado(Constantes.APROB_VALID);
													actEstadoBean = siopDAO.actualizarEstado(actEstadoBean, mensajeTx);
												}else {
													ActualizaEstadoBean actEstadoBean = new ActualizaEstadoBean();
													actEstadoBean.setId(cabeceraAjuste.getId());
													actEstadoBean.setEstado(Constantes.RECHAZADO_VALID);
													actEstadoBean = siopDAO.actualizarEstado(actEstadoBean, mensajeTx);
												}
												montoSinIgv += Double.parseDouble(detalleTemporal.getMontosinigv()==null?Constantes.CADENA_NUMERO_NULO:detalleTemporal.getMontosinigv());
												
												if(detalleTemporal.getAjusteHistoricoAsociado().getAfectoIgv().equals(Constantes.IS_AFECTOIGV)){
											
													montoContIgv += Double.parseDouble(detalleTemporal.getMontoconigv()==null?Constantes.CADENA_NUMERO_NULO:detalleTemporal.getMontoconigv());
											
												}else{
									
													montoContIgv += Double.parseDouble(detalleTemporal.getMontosinigv()==null?Constantes.CADENA_NUMERO_NULO:detalleTemporal.getMontosinigv());
										
													montoInafecto += Double.parseDouble(detalleTemporal.getMontosinigv()==null?Constantes.CADENA_NUMERO_NULO:detalleTemporal.getMontosinigv());
													

												}		

										}//LGM: Listar temporal 
										ActualizaMontoCabBscsBean actMontoBscsBean = new ActualizaMontoCabBscsBean();
										
										actMontoBscsBean.setId(cabeceraAjuste.getId());
										
										actMontoBscsBean.setMontoConIgv((double)Math.round(montoContIgv * 100d) / 100d);
										
										actMontoBscsBean.setMontoInafecto((double)Math.round(montoInafecto * 100d) / 100d);
									
										actMontoBscsBean.setMontoSinIgv((double)Math.round(montoSinIgv * 100d) / 100d);
										
										actMontoBscsBean = siopDAO.actualizarMontoCab(actMontoBscsBean, mensajeTx);// LGM Actualizar montos en la cabecera
										if(!actMontoBscsBean.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)){
											throw new DBException(actMontoBscsBean.getMensajeRespuesta());
										}
							     }

							}else{
								ActualizaEstadoBean actEstadoBean = new ActualizaEstadoBean();
								actEstadoBean.setId(cabeceraAjuste.getId());
								actEstadoBean.setEstado(Constantes.RECHAZADO_VALID);
								actEstadoBean = siopDAO.actualizarEstado(actEstadoBean, mensajeTx);
							}
						}else{
							throw new DBException(listaDetalleTemporal.getMensajeRespuesta());
						}

						
					}
				}
			}else{
				throw new DBException(localListarCabecera.getMensajeRespuesta());	
			}
			
		} catch (DBException e) {
			logger.error("ERROR [DBException] - : ", e);
			throw new BeanCreationException("Error DBException" + e.getCause());
		} catch (CloneNotSupportedException e) {
			logger.error("ERROR [CloneNotSupportedException] - : ", e);
			throw new BeanCreationException("Error CloneNotSupportedException" + e.getCause());
		}
	}

	private double obtenerMontoAjusteSiop(String mensajeTx, DetalleTemporalBean detalleTemporal, List<DetalleTemporalBean> listaDetalleTemporal) {
		logger.info(mensajeTx + " *********** Obtener monto ajuste SIOP ***********");
		double montoAjuste = 0;
		for (DetalleTemporalBean detalleTmp : listaDetalleTemporal) {
			if (!detalleTemporal.getCustomerid().equals(detalleTmp.getCustomerid()) 
					|| !detalleTemporal.getSncode().equals(detalleTmp.getSncode())
					|| !detalleTemporal.getTmCode().equals(detalleTmp.getTmCode())) {
				continue;
			}
			
			double igv;
			double montoConIgv;
			double conIgv;
			double sinIgv;
			if(detalleTmp.getAjusteHistoricoAsociado().getAfectoIgv().equals(Constantes.IS_AFECTOIGV)){
				 
			    conIgv = Double.parseDouble(detalleTmp.getAjusteHistoricoAsociado().getMontoConIgv()==null?
			    		Constantes.CADENA_NUMERO_NULO:detalleTmp.getAjusteHistoricoAsociado().getMontoConIgv());
				sinIgv = Double.parseDouble(detalleTmp.getAjusteHistoricoAsociado().getMontoSinIgv()==null?
						Constantes.CADENA_NUMERO_NULO:detalleTmp.getAjusteHistoricoAsociado().getMontoSinIgv());
				igv = conIgv/sinIgv;
				
				montoConIgv = Double.parseDouble(detalleTmp.getMontosinigv()==null?Constantes.CADENA_NUMERO_NULO:detalleTmp.getMontosinigv());

				montoConIgv = montoConIgv * igv;
				
			}else{
			
				montoConIgv = Double.parseDouble(detalleTmp.getMontosinigv()==null?Constantes.CADENA_NUMERO_NULO:detalleTmp.getMontosinigv());
			
			}
			logger.info(mensajeTx + " customerId: " + detalleTmp.getCustomerid() + ", sncode: " + detalleTmp.getSncode() 
					+ ", tmcode: " + detalleTmp.getTmCode());
			logger.info(mensajeTx + " montoDivididoAjuste: " + montoConIgv);
			montoAjuste = montoAjuste + montoConIgv;
		}
		logger.info(mensajeTx + " montoAjuste: " + montoAjuste);
		return montoAjuste;
	}

	private double obtenerMontoRecibo(String mensajeTx, DetalleTemporalBean detalleTemporal, List<DetalleTemporalBean> listaDetalleTemporal) {
		logger.info(mensajeTx + " *********** Obtener monto recibo ***********");
		double montoRecibo = 0;
		for (DetalleTemporalBean detalleTmp : listaDetalleTemporal) {
			if (!detalleTemporal.getCustomerid().equals(detalleTmp.getCustomerid()) 
					|| !detalleTemporal.getSncode().equals(detalleTmp.getSncode())
					|| !detalleTemporal.getTmCode().equals(detalleTmp.getTmCode())) {
				continue;
			}
			
			double montoDivididoRecibo;
			if(detalleTmp.getAjusteHistoricoAsociado().getAfectoIgv().equals(Constantes.IS_AFECTOIGV)) {
				montoDivididoRecibo = Double.parseDouble(detalleTmp.getAjusteHistoricoAsociado().getMontoConIgv()==null?
						Constantes.CADENA_NUMERO_NULO:detalleTmp.getAjusteHistoricoAsociado().getMontoConIgv());
			} else {
				montoDivididoRecibo = Double.parseDouble(detalleTmp.getAjusteHistoricoAsociado().getMontoSinIgv()==null?
						Constantes.CADENA_NUMERO_NULO:detalleTmp.getAjusteHistoricoAsociado().getMontoSinIgv());
			}
			logger.info(mensajeTx + " customerId: " + detalleTmp.getCustomerid() + ", sncode: " + detalleTmp.getSncode() 
					+ ", tmcode: " + detalleTmp.getTmCode());
			logger.info(mensajeTx + " montoDivididoRecibo: " + montoDivididoRecibo);
			montoRecibo = montoRecibo + montoDivididoRecibo;
		}
//		if(detalleTemporal.getAjusteHistoricoAsociado().getAfectoIgv().equals(Constantes.IS_AFECTOIGV)) {
//			montoRecibo = Double.parseDouble(detalleTemporal.getAjusteHistoricoAsociado().getMontoConIgv()==null?Constantes.CADENA_NUMERO_NULO:detalleTemporal.getAjusteHistoricoAsociado().getMontoConIgv());
//		}else {
//			montoRecibo = Double.parseDouble(detalleTemporal.getAjusteHistoricoAsociado().getMontoSinIgv()==null?Constantes.CADENA_NUMERO_NULO:detalleTemporal.getAjusteHistoricoAsociado().getMontoSinIgv());
//		}
		logger.info(mensajeTx + " montoRecibo: " + montoRecibo);
		return montoRecibo;
	}	

	private boolean validarSerieDocumentoBscs(String recibo) {
		String[] arregloSeriesBscs = propertiesExterno.serieDocumentoBscs.split(Constantes.SIGNO_REPLACE_COMA);
		for (String serie : arregloSeriesBscs) {
			if (recibo.contains(serie)) {
				return true;
			}
		}
		return false;
	}
	
	private double obtenerMontoAjusteBscs(String mensajeTx, DetalleTemporalBean detalleTemporal, List<DetalleTemporalBean> listaDetalleTemporal) {
		logger.info(mensajeTx + " *********** Obtener monto ajuste para proceso BSCS ***********");
		double montoAjustar = 0;
		for (DetalleTemporalBean detalleTmp : listaDetalleTemporal) {
			if (!detalleTemporal.getCustomerid().equals(detalleTmp.getCustomerid()) 
					|| !detalleTemporal.getSncode().equals(detalleTmp.getSncode())
					|| !detalleTemporal.getTmCode().equals(detalleTmp.getTmCode())) {
				continue;
			}
			
			double montoDivididoTmp;
			if(detalleTmp.getAfecto().equals(Constantes.IS_AFECTOIGV)){
				montoDivididoTmp = Double.parseDouble(detalleTmp.getMontoconigv()==null?Constantes.CADENA_NUMERO_NULO:detalleTmp.getMontoconigv());
			}else{
				montoDivididoTmp = Double.parseDouble(detalleTmp.getMontosinigv()==null?Constantes.CADENA_NUMERO_NULO:detalleTmp.getMontosinigv());
			}
			logger.info(mensajeTx + " customerId: " + detalleTmp.getCustomerid() + ", sncode: " + detalleTmp.getSncode() 
					+ ", tmcode: " + detalleTmp.getTmCode());
			logger.info(mensajeTx + " montoDivididoTmp: " + montoDivididoTmp);
			montoAjustar = montoAjustar + montoDivididoTmp;
		}
		logger.info(mensajeTx + " montoDivididoTmpSum: " + montoAjustar);
		return montoAjustar;
	}

	private ListaDetalleTemporalBean calcularMontoSegunPorcentaje(String mensajeTx, AjusteCabeceraBean cabeceraAjuste, 
			ListaDetalleTemporalBean listaDetalleTemporal, String proceso) throws DBException, CloneNotSupportedException {
		logger.info(mensajeTx + " ************ Calcular monto segun porcentaje ************");
		logger.info(mensajeTx + " proceso: " + proceso);
		ListaDetalleTemporalBean nuevoObjListaDetTmp = new ListaDetalleTemporalBean();
		List<DetalleTemporalBean> listaNuevoDetalleTmp = new ArrayList<>();
		for (DetalleTemporalBean detalleTemporal : listaDetalleTemporal.getLista()) {
//			if (detalleTemporal.getListaArchivoAsociado().size() <= Constantes.INT_UNO) {
			if (validarOmisionCalculo(proceso, detalleTemporal)) {
				logger.info(mensajeTx + " No es necesario calcular");
				detalleTemporal.setArchivoAsociado(detalleTemporal.getListaArchivoAsociado() != null ? 
						detalleTemporal.getListaArchivoAsociado().get(0) : null);
				detalleTemporal.setAjusteHistoricoAsociado(detalleTemporal.getListaAjustesDocRef() != null ? 
						detalleTemporal.getListaAjustesDocRef().get(0) : null);
				listaNuevoDetalleTmp.add(detalleTemporal);
				continue;
			}
			
			if (validarDetallePorcentaje(mensajeTx, proceso, detalleTemporal)) {
				eliminarDetalleTemporal(mensajeTx, detalleTemporal);
				logger.info(mensajeTx + " Se agrega nuevos detalles al temporal segun porcentaje");
				List<DetalleTemporalBean> listaDetalleTmpPorc = agregarTemporalSegunPorcentaje(mensajeTx, cabeceraAjuste, detalleTemporal, proceso);
				listaNuevoDetalleTmp.addAll(listaDetalleTmpPorc);
			} else {
				logger.info(mensajeTx + " Detalle porcentaje incompleto, se rechazara validacion");
				return null;
			}
		}
		nuevoObjListaDetTmp.setLista(listaNuevoDetalleTmp);
		logger.info(mensajeTx.concat(" Nuevo objeto lista detalle temporal:\n".concat(JAXBUtilitarios.anyObjectToXmlText(nuevoObjListaDetTmp))));
		return nuevoObjListaDetTmp;
	}

	private boolean validarOmisionCalculo(String proceso, DetalleTemporalBean detalleTemporal) {
		if (Constantes.PROCESO_BSCS.equals(proceso)) {
			return detalleTemporal.getListaArchivoAsociado() != null && detalleTemporal.getListaArchivoAsociado().size() <= Constantes.INT_UNO;
		} else {
			return detalleTemporal.getListaAjustesDocRef() != null && detalleTemporal.getListaAjustesDocRef().size() <= Constantes.INT_UNO;
		}
	}
	
	private void eliminarDetalleTemporal(String mensajeTx, DetalleTemporalBean detalleTemporal) throws DBException {
		logger.info(mensajeTx + " Se elimina detalle temporal antiguo");
		ResponseBase responseBean = siopDAO.eliminarTmpDetalle(mensajeTx, detalleTemporal);
		if(!responseBean.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)) {
			throw new DBException(responseBean.getMensajeRespuesta());
		}
	}

	private List<DetalleTemporalBean> agregarTemporalSegunPorcentaje(String mensajeTx, AjusteCabeceraBean cabeceraAjuste, 
			DetalleTemporalBean detalleTemporal, String proceso) throws DBException, CloneNotSupportedException {
		List<DetalleTemporalBean> listaNuevoDetalleTmp;
		if (Constantes.PROCESO_BSCS.equals(proceso)) {
			listaNuevoDetalleTmp = agregarTmpSegunPorcBscs(mensajeTx, cabeceraAjuste, detalleTemporal);
		} else {
			listaNuevoDetalleTmp = agregarTmpSegunPorcSiop(mensajeTx, cabeceraAjuste, detalleTemporal);
		}
		return listaNuevoDetalleTmp;
	}

	private List<DetalleTemporalBean> agregarTmpSegunPorcSiop(String mensajeTx,AjusteCabeceraBean cabeceraAjuste,
			DetalleTemporalBean detalleTemporal) throws DBException, CloneNotSupportedException {
		List<DetalleTemporalBean> listaNuevoDetalleTmp = new ArrayList<>();
		for (DatosAjusteSiop detalleHistorico : detalleTemporal.getListaAjustesDocRef()) {
			DatosAjusteSiop beanTemporal = Util.convertir(detalleTemporal);
			beanTemporal.setCtaMex(detalleHistorico.getCtaMex());
			beanTemporal.setCtaPer(detalleHistorico.getCtaPer());
			double montoConIgv = Double.valueOf(detalleTemporal.getMontoconigv() == Constantes.VACIO ? 
					Constantes.CADENA_NUMERO_NULO : detalleTemporal.getMontoconigv()) * detalleHistorico.getPorcentaje();
			double montoSinIgv = Double.valueOf(detalleTemporal.getMontosinigv()) * detalleHistorico.getPorcentaje();
			beanTemporal.setMontoConIgv(Util.formatearNumero(montoConIgv));
			beanTemporal.setMontoSinIgv(Util.formatearNumero(montoSinIgv));
			String rowid = guardarDetalleTemporal(mensajeTx, cabeceraAjuste, cabeceraAjuste.getId(), beanTemporal);
			
			DetalleTemporalBean nuevoDetalleTmp = (DetalleTemporalBean) detalleTemporal.clone();
			nuevoDetalleTmp.setCuentacontable(detalleHistorico.getCtaPer());
			nuevoDetalleTmp.setCuentacontablesap(detalleHistorico.getCtaMex());
			nuevoDetalleTmp.setMontoconigv(Util.formatearNumero(montoConIgv));
			nuevoDetalleTmp.setMontosinigv(Util.formatearNumero(montoSinIgv));
			nuevoDetalleTmp.setRowid(rowid);
			nuevoDetalleTmp.setAjusteHistoricoAsociado(detalleHistorico);
			listaNuevoDetalleTmp.add(nuevoDetalleTmp);
		}
		return listaNuevoDetalleTmp;
	}

	private List<DetalleTemporalBean> agregarTmpSegunPorcBscs(String mensajeTx, AjusteCabeceraBean cabeceraAjuste,
			DetalleTemporalBean detalleTemporal) throws DBException, CloneNotSupportedException {
		List<DetalleTemporalBean> listaNuevoDetalleTmp = new ArrayList<>();
		for (DetalleArchPlanoBean detalleArchivoBscs : detalleTemporal.getListaArchivoAsociado()) {
			DatosAjusteSiop beanTemporal = Util.convertir(detalleTemporal);
			beanTemporal.setCtaMex(detalleArchivoBscs.getCtamex());
			beanTemporal.setCtaPer(detalleArchivoBscs.getCtaper());
			double montoConIgv = Double.valueOf(detalleTemporal.getMontoconigv() == Constantes.VACIO ? 
					Constantes.CADENA_NUMERO_NULO : detalleTemporal.getMontoconigv()) * detalleArchivoBscs.getPorcentaje();
			double montoSinIgv = Double.valueOf(detalleTemporal.getMontosinigv()) * detalleArchivoBscs.getPorcentaje();
//			beanTemporal.setMontoConIgv(String.valueOf(montoConIgv));
//			beanTemporal.setMontoSinIgv(String.valueOf(montoSinIgv));
			beanTemporal.setMontoConIgv(Util.formatearNumero(montoConIgv));
			beanTemporal.setMontoSinIgv(Util.formatearNumero(montoSinIgv));
			String rowid = guardarDetalleTemporal(mensajeTx, cabeceraAjuste, cabeceraAjuste.getId(), beanTemporal);
			
			DetalleTemporalBean nuevoDetalleTmp = (DetalleTemporalBean) detalleTemporal.clone();
			nuevoDetalleTmp.setCuentacontable(detalleArchivoBscs.getCtaper());
			nuevoDetalleTmp.setCuentacontablesap(detalleArchivoBscs.getCtamex());
			nuevoDetalleTmp.setMontoconigv(Util.formatearNumero(montoConIgv));
			nuevoDetalleTmp.setMontosinigv(Util.formatearNumero(montoSinIgv));
			nuevoDetalleTmp.setRowid(rowid);
			nuevoDetalleTmp.setArchivoAsociado(detalleArchivoBscs);
			listaNuevoDetalleTmp.add(nuevoDetalleTmp);
		}
		return listaNuevoDetalleTmp;
	}

	private boolean validarDetallePorcentaje(String mensajeTx, String proceso, DetalleTemporalBean detalleTemporal) {
		double sumPorcentaje = 0;
		if (Constantes.PROCESO_BSCS.equals(proceso)) {
			for (DetalleArchPlanoBean detalleArchivoBscs : detalleTemporal.getListaArchivoAsociado()) {
				sumPorcentaje = sumPorcentaje + detalleArchivoBscs.getPorcentaje();
			}
		} else {
			for (DatosAjusteSiop detalleHistorico : detalleTemporal.getListaAjustesDocRef()) {
				sumPorcentaje = sumPorcentaje + detalleHistorico.getPorcentaje();
			}
		}
		logger.info(mensajeTx + " sumPorcentaje: " + sumPorcentaje);
		return sumPorcentaje == Constantes.INT_UNO;
	}
	
	void cargarAjustes(String idTransaction, String[] archivos){

		final long vTiempoProceso = System.currentTimeMillis();

		String msjTx = idTransaction + "[cargarAjustes] ";
		logger.info(msjTx + "[INICIO cargarAjustes]");

		try {

			ResponseUsuarioAppBase mergeUsuario = new ResponseUsuarioAppBase();

			String fechaArchivo = JAXBUtilitarios
					.obtenerFechayyyyMMdd(this.propertiesExterno.pFORMATO_FECHA_NOMBRE);
			String rutaRemota = propertiesExterno.pRUTA_FILESERVER_ARCHIVO_REMOTA;
			String rutaLocal = propertiesExterno.vRutaDataCtlUserApp;
			String servidor = propertiesExterno.pServidorFileServerSftp;
			
			for (int i = 0; i < archivos.length; i++) {
				String nombreArchivoRemoto = archivos[i];
				if(nombreArchivoRemoto != null) {
					logger.info(msjTx + "nombreArchivoRemoto : "
							+ nombreArchivoRemoto);

					logger.info(msjTx
							+ " [INICIO] - Actividad 2 Obtener archivo UsuarioApp desde FileServer");

					String rutaArchivo = obtenerArchivoPlanoUsurio(msjTx,
							rutaRemota, rutaLocal, nombreArchivoRemoto,
							servidor);

					terminarConexiones(msjTx);

					if (rutaArchivo != null) {
						logger.info(msjTx
								+ "[FIN] - Actividad 3 - Guardar archivo en el Servidor de la Shell ");

						logger.info(msjTx + "ruta archivo total : "
								+ rutaArchivo);
						boolean estadoCompresion=true;

						if (estadoCompresion) {

							logger.info(msjTx + " Se pudo obtener  el archivo: "
									+ rutaArchivo + " correctamente");



							logger.info(msjTx
									+ "[INICIO] - Actividad 4 - Registrar txt "
									+ rutaArchivo + " en : " + propertiesExterno.dbSiopdbOwner+"."
									+ Constantes.TABLA_TEMPORAL);
							StringBuilder rutaCTL = new StringBuilder();
							rutaCTL.append(propertiesExterno.vRutaCtlUserApp);
							if(i==2) {
								if(archivos[2] != null) {
									rutaCTL.append(propertiesExterno.pNOMBRE_ARCHIVO_USUARIO_APP_CTL_ERROR);
								}
							}else {
								rutaCTL.append(propertiesExterno.pNOMBRE_ARCHIVO_USUARIO_APP_CTL);
							}
							
							rutaCTL.append(propertiesExterno.pEXTENSION_ARCHIVO_CTL);

							StringBuilder rutaBad = new StringBuilder();
							rutaBad.append(propertiesExterno.vRutaBadCtlUserApp);
							rutaBad.append(Constantes.GUION_ABAJO);
							rutaBad.append(fechaArchivo);
							rutaBad.append(propertiesExterno.pEXTENSION_ARCHIVO_BAD);

							logger.info(msjTx + "ruta Bad :  " + rutaBad);

							StringBuilder rutaLogCTL = new StringBuilder();
							rutaLogCTL.append(propertiesExterno.vRutaLogCtlUserApp);
							rutaLogCTL
									.append(nombreArchivoRemoto);
							rutaLogCTL
									.append(propertiesExterno.pEXTENSION_ARCHIVO_LOG);

							logger.info(msjTx + "ruta Log CTL :  " + rutaLogCTL);

		
							mergeUsuario = util.ejecutarSqlldr(msjTx,
									Constantes.CTL_SQLLDR, rutaBad.toString(),
									rutaLogCTL.toString(), rutaArchivo,
									rutaCTL.toString(),
									propertiesExterno.vRutaDiscarCtlUserApp,
									propertiesExterno.dbSiopdbUsuario,
									propertiesExterno.dbSiopdbPassword,
									propertiesExterno.cadenaSQLLDRSiop,
									propertiesExterno.pBindSizeCTL,
									propertiesExterno.pReadSizeCTL,
									propertiesExterno.prowsCTL);


							if (String.valueOf(mergeUsuario.getCodRespuesta()).equals(
									Constantes.CODIGO_EXITO_0)) {

								logger.info(msjTx
										+ "[FIN] - Actividad 4 - Registrar txt "
										+ rutaArchivo + " en "
										+ Constantes.TABLA_TEMPORAL);
								
							} 

							logger.info(msjTx
									+ "[INICIO] - Actividad 5. Eliminar Archivo de Servidor FTP TIMEAI  ");

							logger.info(msjTx + "Realizando el borrado del archivo : "
									+ rutaLocal);					
							logger.info(msjTx
									+ "[FIN] - Actividad 5. Eliminar Archivo de Servidor FTP TIMEAI  ");

						} else {

							logger.error(msjTx + " No se pudo descomprimir el archivo:"
									+ rutaArchivo + " correctamente");

						}

					} else {
						throw new SFTPException(propertiesExterno.pSFTP_ERROR_CODE,
								propertiesExterno.pSFTP_ERROR_DESCRIPTION_OBTENER);

					}
					
				}

			}


		}catch (SFTPException e) {
			logger.error(msjTx + " [ERROR SFTPException]: " + e.getCodError());
			logger.error(msjTx + " [ERROR SFTPException]: " + e.getMessage());

		} catch (Exception e) {
			logger.error(msjTx + " " + Constantes.ERROR_GENERAL + e);
		} finally {

			logger.info(msjTx
					+ "[FIN de metodo: run] Tiempo total de proceso(ms): "
					+ (System.currentTimeMillis() - vTiempoProceso)
					+ " milisegundos.");
		}
	}
	
	private String obtenerArchivoPlanoUsurio(String mensajeTransaccion,
			String rutaRemota, String rutaDestino, String nomberArchivoRemoto,
			String servidor) throws SFTPException {
		String obtenerArchivo = null;
		boolean conexionFileServer = true;

		try {

			for (int i = Constantes.INT_UNO; i <= Integer
					.parseInt(propertiesExterno.pCANTIDAD_REINTENTOS); i++) {

				logger.info(mensajeTransaccion + "Intento [conectarSFTP] : "
						+ (i) + " de maximo "
						+ propertiesExterno.pCANTIDAD_REINTENTOS);

				String usuarioFileServer = propertiesExterno.pUsuarioFileServerSftp;
				String passwordFileServer = propertiesExterno.pPasswordFileServerSftp;
				String puertoDMZ = propertiesExterno.pPuertoSftp;
				String timeOutDMZ = propertiesExterno.pTimeOutFileserverSftp;
				conexionFileServer = utilSFTP.conectarSftpFileServer(
						mensajeTransaccion, servidor, usuarioFileServer,
						passwordFileServer, puertoDMZ, timeOutDMZ);

				if (conexionFileServer) {

					logger.info(mensajeTransaccion
							+ " [FIN] - Actividad 2 Obtener archivo UsuarioApp desde FileServer");

					logger.info(mensajeTransaccion + "Buscando el archivo : "
							+ nomberArchivoRemoto + " en servidor : "
							+ servidor + " en la ruta " + rutaRemota);
					logger.info(mensajeTransaccion
							+ "[INICIO] - Actividad 3 - Guardar archivo en el Servidor de la Shell ");
					

					obtenerArchivo = utilSFTP.obtenerArchivoSFTP(
							mensajeTransaccion, rutaRemota, rutaDestino,
							nomberArchivoRemoto, servidor);

					if (obtenerArchivo != null) {
						break;
					}
					break;

				} else {
					logger.info("Esperando reintento de conexion");
					Thread.sleep(propertiesExterno.pTIEMPO_REINTENTO_CONEXION);
				}

			}

		} catch (Exception e) {

			throw new SFTPException(propertiesExterno.pSFTP_ERROR_CODE,
					propertiesExterno.pSFTP_ERROR_DESCRIPTION_OBTENER);

		} finally {

			logger.info(mensajeTransaccion + "FIN: [obtenerArchivoPlanoUsurio]");

		}
		return obtenerArchivo;

	}

	private void terminarConexiones(String mensajeTransaccion) {
		logger.info(mensajeTransaccion + " Cerrando conexiones ... ");

		utilSFTP.desconectarSFTP(mensajeTransaccion);

		logger.info(mensajeTransaccion + " Conexiones cerradas ... ");
	}
	
	private boolean validarDetallePorcentaje(String mensajeTx, DetalleArchPlanoBean detalleArchivoBscs, 
			List<DetalleArchPlanoBean> listaDetArchBscs) {
		double sumPorcentaje = 0;
		for (DetalleArchPlanoBean detalle : listaDetArchBscs) {
			if (detalleArchivoBscs.getPlan() != null && !Constantes.VACIO.equals(detalleArchivoBscs.getPlan())
					&& detalle.getCustomerid().equals(detalleArchivoBscs.getCustomerid()) && detalle.getServ().equals(detalleArchivoBscs.getServ())
					&& detalle.getPlan().equals(detalleArchivoBscs.getPlan())) {
				sumPorcentaje = sumPorcentaje + detalle.getPorcentaje();
			}
		}
		logger.info(mensajeTx + " sumPorcentaje: " + sumPorcentaje);
		return sumPorcentaje == Constantes.INT_UNO;
	}
	
	private boolean validarDetallePorcentaje(String mensajeTx, DatosAjusteSiop datoHistorico, List<DatosAjusteSiop> listaDatosAjusteSiop) {
		double sumPorcentaje = 0;
		for (DatosAjusteSiop detalle : listaDatosAjusteSiop) {
			if (datoHistorico.getTmcode() != null && !Constantes.VACIO.equals(datoHistorico.getTmcode()) 
					&& detalle.getIdProceso().equals(datoHistorico.getIdProceso()) && detalle.getServ().equals(datoHistorico.getServ()) 
					&& detalle.getId().equals(datoHistorico.getId()) && detalle.getTmcode().equals(datoHistorico.getTmcode())) {
				sumPorcentaje = sumPorcentaje + detalle.getPorcentaje();
			}
		}
		logger.info(mensajeTx + " sumPorcentaje: " + sumPorcentaje);
		return sumPorcentaje == Constantes.INT_UNO;
	}
	
	private ActualizaMontoCabBscsBean registrarDetalleAjusteTotalSB99(String mensajeTx, List<DatosAjusteSiop> listaDatosAjusteSiop, 
			AjusteCabeceraBean cabeceraAjuste) throws DBException {
		logger.info(mensajeTx + " ************ Registrar detalle Ajuste Total SB99 ************");
//		TmpAmDetBean tmpAmDetBean;
//		ResponseBase responseBean;
		int id = cabeceraAjuste.getId();
		double montoSinIgv, montoConIgv;
		double sumMontoSinIgv = 0, sumMontoInafecto = 0, sumMontoConIgv = 0;
		int contDetallesInvalidos = 0;
		for (DatosAjusteSiop datoHistorico : listaDatosAjusteSiop) {
			if (!validarDetallePorcentaje(mensajeTx, datoHistorico, listaDatosAjusteSiop)) {
//				logger.info(mensajeTx + " Detalle porcentaje Siop incompleto, se rechazara validacion");
//				return null;
				contDetallesInvalidos++;
			}
//			tmpAmDetBean = new TmpAmDetBean(id, cabeceraAjuste.getIdProceso(), datoHistorico.getCtaPer(), 
//					datoHistorico.getEvento(), Double.valueOf(datoHistorico.getMontoSinIgv()), datoHistorico.getServ(), datoHistorico.getPlan(), 
//					datoHistorico.getTelefono(), Constantes.BLOQUEO_VALID, new Date());
//			responseBean = siopDAO.insertarTmpDetAm(mensajeTx, tmpAmDetBean);
//			if(!responseBean.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)){
//				throw new DBException(responseBean.getMensajeRespuesta());
//			}
//			completarDetalleTemporal(mensajeTx, datoHistorico, cabeceraAjuste);
			guardarDetalleTemporal(mensajeTx, cabeceraAjuste, id, datoHistorico);
			
			montoSinIgv = Double.valueOf((datoHistorico.getMontoSinIgv() == null || Constantes.VACIO.equals(datoHistorico.getMontoSinIgv())) ? 
					Constantes.CADENA_NUMERO_NULO : datoHistorico.getMontoSinIgv());
			montoConIgv = Double.valueOf((datoHistorico.getMontoConIgv() == null || Constantes.VACIO.equals(datoHistorico.getMontoConIgv())) ? 
					Constantes.CADENA_NUMERO_NULO : datoHistorico.getMontoConIgv());
			
			sumMontoSinIgv = sumMontoSinIgv + montoSinIgv;
			if (montoConIgv > montoSinIgv) {//afecto
				sumMontoConIgv = sumMontoConIgv + montoConIgv;
			} else {
				sumMontoConIgv = sumMontoConIgv + montoSinIgv;
				sumMontoInafecto = sumMontoInafecto + montoSinIgv;
			}
		}
		
		if (contDetallesInvalidos > 0) {
			logger.info(mensajeTx + " Detalle porcentaje Siop incompleto, se rechazara validacion");
			return null;
		}
		
		ActualizaMontoCabBscsBean actualizaMontoCab = new ActualizaMontoCabBscsBean();
		actualizaMontoCab.setId(id);
		actualizaMontoCab.setMontoConIgv(sumMontoConIgv);
		actualizaMontoCab.setMontoInafecto(sumMontoInafecto);
		actualizaMontoCab.setMontoSinIgv(sumMontoSinIgv);
		return actualizaMontoCab;
	}

	private String guardarDetalleTemporal(String mensajeTx, AjusteCabeceraBean cabeceraAjuste, int id,
			DatosAjusteSiop detalleTemporal) throws DBException {
		TmpAmDetBean tmpAmDetBean = new TmpAmDetBean(id, cabeceraAjuste.getIdProceso(), detalleTemporal.getCtaPer(), 
				detalleTemporal.getEvento(), Double.valueOf(detalleTemporal.getMontoSinIgv()), detalleTemporal.getServ(), detalleTemporal.getTmcode(), 
				detalleTemporal.getTelefono(), Constantes.BLOQUEO_VALID, new Date());
		ResponseBase responseBean = siopDAO.insertarTmpDetAm(mensajeTx, tmpAmDetBean);
		if(!responseBean.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)){
			throw new DBException(responseBean.getMensajeRespuesta());
		}
		
		detalleTemporal.setRowid(responseBean.getRowid());
		completarDetalleTemporal(mensajeTx, detalleTemporal, cabeceraAjuste);
		return responseBean.getRowid();
	}

	private void completarCabeceraSB99(String mensajeTx, AjusteCabeceraBean cabeceraAjuste, DatosAjusteSiop datosAjusteSiop) 
			throws DBException {
		logger.info(mensajeTx + " ************ Completar cabecera SB99 ************");
		DetalleArchPlanoBean completaCabecera = new DetalleArchPlanoBean();
		completaCabecera.setTelefono(datosAjusteSiop.getTelefono());
		completaCabecera.setIniciociclo(datosAjusteSiop.getInicioCiclo());
		completaCabecera.setFinciclo(datosAjusteSiop.getFinCiclo());
		completaCabecera.setCustcode(datosAjusteSiop.getCustCode());
		completaCabecera.setRuc(datosAjusteSiop.getRuc());
		completaCabecera.setRazonsocial(datosAjusteSiop.getRazonSocial());
		completaCabecera.setContacto(datosAjusteSiop.getContacto());
		completaCabecera.setTipocliente(datosAjusteSiop.getTipoCliente());
		completaCabecera.setDireccion(datosAjusteSiop.getDireccion());
		completaCabecera.setNotadir(datosAjusteSiop.getNota());
		completaCabecera.setDepartamento(datosAjusteSiop.getDepartamento());
		completaCabecera.setProvincia(datosAjusteSiop.getProvincia());
		completaCabecera.setDistrito(datosAjusteSiop.getDistrito());
		completaCabecera.setAfiliacion(datosAjusteSiop.getAfiliacion());
		completaCabecera.setEmail(datosAjusteSiop.getEmail());
		completaCabecera.setFactura(datosAjusteSiop.getFactura());
		completaCabecera.setEmision(datosAjusteSiop.getEmision());
		completaCabecera.setAmpliacion(datosAjusteSiop.getAmpliacion());
		completaCabecera.setBillcycle(datosAjusteSiop.getBillCycle());
		completaCabecera.setMontofactura(datosAjusteSiop.getMontoFactura());
		completaCabecera.setIdproceso(String.valueOf(cabeceraAjuste.getIdProceso()));
		completaCabecera.setFechaEmision(datosAjusteSiop.getFechaEmision());
		completaCabecera.setFechaVencimiento(datosAjusteSiop.getFechaVencimiento());
		ResponseBase responseBean = siopDAO.completaDatosCabecera(completaCabecera, mensajeTx, cabeceraAjuste.getId());
		if(!responseBean.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)) {
			throw new DBException(responseBean.getMensajeRespuesta());
		}
	}

	private void completarDetalleTemporal(String mensajeTx, DatosAjusteSiop datoHistorico, AjusteCabeceraBean cabeceraAjuste) 
			throws DBException {
		logger.info(mensajeTx + " ************ Completar detalle temporal ************");
		ObtieneRubroBean obtieneRubro = new ObtieneRubroBean();
		obtieneRubro.setTipoAjuste(cabeceraAjuste.getCodigoAjuste());
		obtieneRubro.setSnCode(datoHistorico.getServ());
		obtieneRubro = siopDAO.obtenerRubro(obtieneRubro, mensajeTx);
		if(!obtieneRubro.getCodigoError().equals(Constantes.CODIGO_EXITO)) {
			throw new DBException(obtieneRubro.getMensajeError());
		}
		
		DetalleArchPlanoBean completaDetalle = new DetalleArchPlanoBean();
		completaDetalle.setDesplan(datoHistorico.getDesPlan());
		completaDetalle.setCebe(datoHistorico.getCentroBeneficio());
		completaDetalle.setCtaper(datoHistorico.getCtaPer());
		completaDetalle.setAfectoigv(datoHistorico.getAfectoIgv());
		completaDetalle.setIdproceso(String.valueOf(cabeceraAjuste.getIdProceso()));
		completaDetalle.setServ(datoHistorico.getServ());
		completaDetalle.setPlan(datoHistorico.getTmcode());
		completaDetalle.setNombreRubro(obtieneRubro.getLista().get(Constantes.CERO_INT).getNombreRubro());
		completaDetalle.setConceptoRubro(obtieneRubro.getLista().get(Constantes.CERO_INT).getConceptoRubro());
		completaDetalle.setCtamex(datoHistorico.getCtaMex());
		completaDetalle.setCtaigv(datoHistorico.getCtaIgv());
		completaDetalle.setPorcentaje(datoHistorico.getPorcentaje());
		completaDetalle.setRowid(datoHistorico.getRowid());
		ResponseBase responseBean = siopDAO.completaDetalle(completaDetalle, mensajeTx, cabeceraAjuste.getId());
		if(!responseBean.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)) {
			throw new DBException(responseBean.getMensajeRespuesta());
		}
		
		double montoConIgv = Double.valueOf((datoHistorico.getMontoConIgv() == null || Constantes.VACIO.equals(datoHistorico.getMontoConIgv())) ? 
				Constantes.CADENA_NUMERO_NULO : datoHistorico.getMontoConIgv());
		
		ActualizaMtoCabBscsBean mtoCab = new ActualizaMtoCabBscsBean();
		mtoCab.setId(String.valueOf(cabeceraAjuste.getId()));
		mtoCab.setTmCode(datoHistorico.getTmcode());
		mtoCab.setSnCode(datoHistorico.getServ());
		mtoCab.setMtoConIgv(String.format("%.2f", montoConIgv));
		mtoCab.setIdProceso(String.valueOf(cabeceraAjuste.getIdProceso()));
		mtoCab.setEvento(datoHistorico.getEvento());
		mtoCab.setRowid(datoHistorico.getRowid());
		mtoCab = siopDAO.actualizarMtoIgv(mtoCab, mensajeTx);
		if (!mtoCab.getCodigoRespuesta().equals(Constantes.CODIGO_EXITO)) {
			throw new DBException(mtoCab.getMensajeRespuesta());
		}
	}

}
