package pe.com.claro.oss.validar.ajuste.masivo.bean;

public class AjusteOACBean {
	
	private String customerId;
	private String numeroFactura;
	private String comentarios;
	private String fechaFactura;
	private String fechaDeuda;
	private String montoReal;
	private String montoRestante;
	private String estado;
	
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getNumeroFactura() {
		return numeroFactura;
	}
	public void setNumeroFactura(String numeroFactura) {
		this.numeroFactura = numeroFactura;
	}
	public String getComentarios() {
		return comentarios;
	}
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public String getFechaFactura() {
		return fechaFactura;
	}
	public void setFechaFactura(String fechaFactura) {
		this.fechaFactura = fechaFactura;
	}
	public String getFechaDeuda() {
		return fechaDeuda;
	}
	public void setFechaDeuda(String fechaDeuda) {
		this.fechaDeuda = fechaDeuda;
	}
	public String getMontoReal() {
		return montoReal;
	}
	public void setMontoReal(String montoReal) {
		this.montoReal = montoReal;
	}
	public String getMontoRestante() {
		return montoRestante;
	}
	public void setMontoRestante(String montoRestante) {
		this.montoRestante = montoRestante;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
	

}
