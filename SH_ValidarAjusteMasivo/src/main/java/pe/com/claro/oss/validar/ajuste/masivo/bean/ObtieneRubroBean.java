package pe.com.claro.oss.validar.ajuste.masivo.bean;

import java.util.List;

public class ObtieneRubroBean {
	
	private String tipoAjuste;
	private String snCode;
	private List<RubroBean> lista;
	private String codigoError;
	private String mensajeError;
	
	public String getTipoAjuste() {
		return tipoAjuste;
	}
	public void setTipoAjuste(String tipoAjuste) {
		this.tipoAjuste = tipoAjuste;
	}
	public String getSnCode() {
		return snCode;
	}
	public void setSnCode(String snCode) {
		this.snCode = snCode;
	}
	public List<RubroBean> getLista() {
		return lista;
	}
	public void setLista(List<RubroBean> lista) {
		this.lista = lista;
	}
	public String getCodigoError() {
		return codigoError;
	}
	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	
	
	

}
