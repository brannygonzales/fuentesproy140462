package pe.com.claro.oss.validar.ajuste.masivo.bean;

public class ActualizarMontosBean {
	
	private String idProceso;
	private double montoIGV;
	private double montoSinIGV;
	private double  montoInafecto;
	private String codigoRespuesta;
	private String mensajeRespuesta;
	
	public String getIdProceso() {
		return idProceso;
	}
	public void setIdProceso(String idProceso) {
		this.idProceso = idProceso;
	}
	public double getMontoIGV() {
		return montoIGV;
	}
	public void setMontoIGV(double montoIGV) {
		this.montoIGV = montoIGV;
	}
	public double getMontoSinIGV() {
		return montoSinIGV;
	}
	public void setMontoSinIGV(double montoSinIGV) {
		this.montoSinIGV = montoSinIGV;
	}
	public double getMontoInafecto() {
		return montoInafecto;
	}
	public void setMontoInafecto(double montoInafecto) {
		this.montoInafecto = montoInafecto;
	}
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
	public String getMensajeRespuesta() {
		return mensajeRespuesta;
	}
	public void setMensajeRespuesta(String mensajeRespuesta) {
		this.mensajeRespuesta = mensajeRespuesta;
	}
	
	
	
	

}
