package pe.com.claro.oss.validar.ajuste.masivo.bean;

public class DetalleArchPlanoBean {
	
	private String idproceso;
	private String id;
	private String customerid;
	private String custcode;
	private String ruc;
	private String razonsocial;
	private String contacto;
	private String tipocliente;
	private String direccion;
	private String notadir;
	private String departamento;
	private String provincia;
	private String distrito;
	private String telefono;
	private String afiliacion;
	private String email;
	private String factura;
	private String montofactura;
	private String igv;
	private String igvvigente;
	private String emision;
	private String ampliacion;
	private String billcycle;
	private String iniciociclo;
	private String finciclo;
	private String plan;
	private String desplan;
	private String serv;
	private String desserv;
	private String ctaper;
	private String ctamex;
	private String montoserv;
	private String afectoigv;
	private String cebe;
	private String montoConIgv;
	private String nombreRubro;
	private String conceptoRubro;
	private String fechaEmision;
	private String fechaVencimiento;
	private String ctaigv;
	private double porcentaje;
	private String rowid;
	
	public String getIdproceso() {
		return idproceso;
	}
	public void setIdproceso(String idproceso) {
		this.idproceso = idproceso;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCustomerid() {
		return customerid;
	}
	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}
	public String getCustcode() {
		return custcode;
	}
	public void setCustcode(String custcode) {
		this.custcode = custcode;
	}
	public String getRuc() {
		return ruc;
	}
	public void setRuc(String ruc) {
		this.ruc = ruc;
	}
	public String getRazonsocial() {
		return razonsocial;
	}
	public void setRazonsocial(String razonsocial) {
		this.razonsocial = razonsocial;
	}
	public String getContacto() {
		return contacto;
	}
	public void setContacto(String contacto) {
		this.contacto = contacto;
	}
	public String getTipocliente() {
		return tipocliente;
	}
	public void setTipocliente(String tipocliente) {
		this.tipocliente = tipocliente;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getNotadir() {
		return notadir;
	}
	public void setNotadir(String notadir) {
		this.notadir = notadir;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getAfiliacion() {
		return afiliacion;
	}
	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFactura() {
		return factura;
	}
	public void setFactura(String factura) {
		this.factura = factura;
	}
	public String getMontofactura() {
		return montofactura;
	}
	public void setMontofactura(String montofactura) {
		this.montofactura = montofactura;
	}
	public String getIgv() {
		return igv;
	}
	public void setIgv(String igv) {
		this.igv = igv;
	}
	public String getIgvvigente() {
		return igvvigente;
	}
	public void setIgvvigente(String igvvigente) {
		this.igvvigente = igvvigente;
	}
	public String getEmision() {
		return emision;
	}
	public void setEmision(String emision) {
		this.emision = emision;
	}
	public String getAmpliacion() {
		return ampliacion;
	}
	public void setAmpliacion(String ampliacion) {
		this.ampliacion = ampliacion;
	}
	public String getBillcycle() {
		return billcycle;
	}
	public void setBillcycle(String billcycle) {
		this.billcycle = billcycle;
	}
	public String getIniciociclo() {
		return iniciociclo;
	}
	public void setIniciociclo(String iniciociclo) {
		this.iniciociclo = iniciociclo;
	}
	public String getFinciclo() {
		return finciclo;
	}
	public void setFinciclo(String finciclo) {
		this.finciclo = finciclo;
	}
	public String getPlan() {
		return plan;
	}
	public void setPlan(String plan) {
		this.plan = plan;
	}
	public String getDesplan() {
		return desplan;
	}
	public void setDesplan(String desplan) {
		this.desplan = desplan;
	}
	public String getServ() {
		return serv;
	}
	public void setServ(String serv) {
		this.serv = serv;
	}
	public String getDesserv() {
		return desserv;
	}
	public void setDesserv(String desserv) {
		this.desserv = desserv;
	}
	public String getCtaper() {
		return ctaper;
	}
	public void setCtaper(String ctaper) {
		this.ctaper = ctaper;
	}
	public String getCtamex() {
		return ctamex;
	}
	public void setCtamex(String ctamex) {
		this.ctamex = ctamex;
	}
	public String getMontoserv() {
		return montoserv;
	}
	public void setMontoserv(String montoserv) {
		this.montoserv = montoserv;
	}
	public String getAfectoigv() {
		return afectoigv;
	}
	public void setAfectoigv(String afectoigv) {
		this.afectoigv = afectoigv;
	}
	public String getCebe() {
		return cebe;
	}
	public void setCebe(String cebe) {
		this.cebe = cebe;
	}
	public String getMontoConIgv() {
		return montoConIgv;
	}
	public void setMontoConIgv(String montoConIgv) {
		this.montoConIgv = montoConIgv;
	}
	public String getNombreRubro() {
		return nombreRubro;
	}
	public void setNombreRubro(String nombreRubro) {
		this.nombreRubro = nombreRubro;
	}
	public String getConceptoRubro() {
		return conceptoRubro;
	}
	public void setConceptoRubro(String conceptoRubro) {
		this.conceptoRubro = conceptoRubro;
	}
	public String getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	public String getCtaigv() {
		return ctaigv;
	}
	public void setCtaigv(String ctaigv) {
		this.ctaigv = ctaigv;
	}
	public double getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(double porcentaje) {
		this.porcentaje = porcentaje;
	}
	public String getRowid() {
		return rowid;
	}
	public void setRowid(String rowid) {
		this.rowid = rowid;
	}
	
}
