
package pe.com.claro.oss.validar.ajuste.masivo.util;

public class Constantes {

	public static final String CADENAVACIA = "";
	public static final String ESPACIO_BLANCO = " ";

	public static final String CARACTER_PUNTO_COMA = ";";

	public static final String CODIGO_EXITO = "0";
	public static final String CODIGO_WARN = "2";
	public static final String CODIGO_ERROR = "-1";
	public static final String LOG4J_PROPERTIES = "log4j.properties";
	public static final String URL_CONTEXT = "./spring/applicationContext.xml";
	public static final String VACIO = "";
	public static final String STRING_PUNTO = ".";
	public static final String SALTO_LINEA = "\n";

	public static final String FORMATO_FECHA_HORA = "dd/MM/yyyy HH:mm:ss";
	public static final String FORMATO_FECHA = "dd/MM/yyyy";
	public static final String FORMATO_FECHA_YYYYMMDD = "yyyyMMdd";
	public static final String NOMBRE_MAIN = "ValidarAjusteMasivoMain";

	// Mensajes
	public static final String METODO_MENSAJE_EXCEPTION = "Se ha producido un error [Exception] definido como:\n";

	public static final String MENSAJE_TIEMPO_DURACION = "Tiempo que demoro en la ejecucion[VALOR_DURACION ms]";

	public static final String CADENAREPLACEDURACION = "VALOR_DURACION";

	// Mensajes DAO
	public static final String METODO_DAO_MENSAJE_EXCEPTION = "Se ha producido un error [Exception] definido como:\n";
	public static final String METODO_DAO_MENSAJE_NESTEDRUNTIMEEXCEPTION = "Se ha producido un error [NestedRuntimeException] definido como:\n";
	public static final String SP_MENSAJE_ERROR_EJECUTANDO = "Error ejecutando";
	public static final String BD_CON_CORCHETE = "[$bd]";

	// Mensajes FTP
	public static final String METODO_FTP_MENSAJE_EXCEPTION = "Se ha producido un error [Exception] definido como:\n";

	public static final String ERROR_CONTEXTO = "Error al Levantar Contexto: ";

	public static final String ERROR_INESPERADO = "Error inesperado: ";

	public static final String ERROR_INICIAR_CONTEXTO = "Ocurrio un error al iniciar el contexto";
	public static final String ERROR_CERRAR_CONTEXTO = "Ocurrio un error al cerrar el contexto";

	public static final String ERROR_ARGUMENTO = "Error al convertir el argumento a Fecha";

	public static final String NOMBRE_METODO = "validarAjusteMasivo";

	public static final int INTENTO_UNO = 1;

	public static final String SEPARADOR = "|";
	public static final String GUION_BAJO = "_";
	public static final String CARACTER_SLASH = "/";
	public static final String CARACTER_ARROBA = "@";
	public static final String EXTENSION_ARCHIVO_TXT = ".txt";
	public static final String EXTENSION_ARCHIVO_ZIP = ".zip";
	public static final String EXTENSION_ARCHIVO_LOG = ".log";
	public static final String EXTENSION_ARCHIVO_BAD = ".bad";
	public static final String EXTENSION_ARCHIVO_CTL = ".ctl";

	public static final String COMANDO_EJECUTAR_SHELL = "sh";
	public static final String COMANDO_EJECUTAR_SQLLDR = "sqlldr";

	public static final String COMANDO_COMPRIMIR_ARCHIVO = "zip";
	public static final String COMANDO_DESCOMPRIMIR_ARCHIVO = "unzip";

	public static final String COMANDO_UBICAR_DIRECTORIO = "cd";

	public static final String COMANDO_BUSCAR_ARCHIVO = "find";

	public static final String COMANDO_DEPOSITAR_ARCHIVO = "-d";

	public static final String COMANDO_DESCOMPRIMIR_ARCHIVO_RUTA = "-d";
	public static final String propertiesExternos = ",";
	public static final String SIGNO_REPLACE_COMA = ",";;
	public static final String ARROBA="@";
	public static final String SLASH="/";
	public static final String GUION_ABAJO="_";
	// IDF
	public static final String CODIGO_EXITO_0 = "0";

	// IDT
	public static final String CODIGO_ERROR_1 = "1";
	// Parametros SP:

	public static final String CODIGORESPUESTA = "PO_CODERROR";

	public static final String MENSAJERESPUESTA = "PO_DESERROR";
	
	public static final String CODIGORESPUESTA_BSCS = "PO_CODERROR";

	public static final String MENSAJERESPUESTA_BSCS = "PO_MSJERROR";
	
	public static final String ARCHIVO_PLANO_ERROR ="PO_AM_ERR";
	
	public static final String ARCHIVO_PLANO_PARCIAL ="PO_AM_PARCIAL";

	public static final String ARCHIVO_PLANO_TOTAL ="PO_AM_TOTAL";

	public static final String FLAG_DETALLE = "PO_FLAG_DET";


	public static final String CURSORPROCESOS = "PO_CURSOR_PROC";
	
	public static final String DOC_REFERENCIA = "PI_DOC_REFERENCIA";
	
	public static final String TIPO_AJUSTE = "PI_TIPO_AJUSTE";
	
	public static final String TIPO_AJUSTE_REC = "REC";
	
	public static final String TIPO_AJUSTE_NC = "NC";
	
	public static final String TIPO_AJUSTE_ND = "ND";
	
	public static final String TIPO_AJUSTE_AJUSTES = "AJUSTES";
	
	public static final String OAC_COMENTARIOS = "COMMENTS";
	
	public static final String OAC_CUSTOMER_ID = "CUSTOMER_TRX_ID";
	
	public static final String OAC_ESTADO = "ATTRIBUTE6";
	
	public static final String OAC_FECHA_DEUDA = "DUE_DATE";
	
	public static final String OAC_FECHA_FACTURA = "TRX_DATE";
	
	public static final String OAC_MONTO_REAL = "AMOUNT_DUE_ORIGINAL";
	
	public static final String OAC_MONTO_RESTANTE = "AMOUNT_DUE_REMAINING";
	
	public static final String OAC_NUMERO_FACTURA = "TRX_NUMBER";
	
	public static final String OAC_IN_CUSTOMER_ID = "PI_CUSTOMER_ID";
	
	public static final String JNDI = "DATA SOURCE (JNDI): [XXX]";

	public static final String BD = "BD: [XXX]";

	public static final String OWNER = "OWNER: [XXX]";

	public static final String PACKAGE = "PACKAGE: [XXX]";

	public static final String PROCEDURE = "PROCEDURE: [XXX]";

	public static final String VALOR_REEMPLAZO = "XXX";

	public static final String MENSAJE_EJECUCION_SP = "Se esta ejecutando el SP ";
	
	public static final String ENTRADA_PROCESO = "PI_ID_PROCESO";
	
	public static final String ENTRADA_SERVICIO = "PI_SERVICIO";
	
	public static final String ENTRADA_PROCESO_BSCS = "PI_PROCESS_NUM";
	
	public static final String ENTRADA_DOCREFERENCIA = "PI_DOC_REFERENCIA";
	
	public static final String CURSOR_DATOSSIOP_IDPROCESO ="HAMCI_IDPROCESO";
	
	public static final String CURSOR_DATOSSIOP_CUSTOMERID ="HAMCV_CUSTOMERID";
	
	public static final String CURSOR_DATOSSIOP_CUSTCODE ="HAMCV_CUSTCODE";

	public static final String CURSOR_DATOSSIOP_RUC ="HAMCV_RUC";
	
	public static final String CURSOR_DATOSSIOP_RAZONSOCIAL ="HAMCV_RAZONSOCIAL";
	
	public static final String CURSOR_DATOSSIOP_CONTACTO ="HAMCV_CONTACTO";
	
	public static final String CURSOR_DATOSSIOP_DIRECCION="HAMCV_DIRECCION";

	public static final String CURSOR_DATOSSIOP_NOTA = "HAMCV_NOTADIR";
	
	public static final String CURSOR_DATOSSIOP_DEPARTAMENTO = "HAMCV_DEPARTAMENTO";
	
	public static final String CURSOR_DATOSSIOP_PROVINCIA = "HAMCV_PROVINCIA";
	
	public static final String CURSOR_DATOSSIOP_DISTRITO = "HAMCV_DISTRITO";
	
	public static final String CURSOR_DATOSSIOP_TELEFONO = "HAMCV_NROTELEFONO";
	
	public static final String CURSOR_DATOSSIOP_AFILIACION = "HAMCV_AFILIACION";
	
	public static final String CURSOR_DATOSSIOP_EMAIL = "HAMCV_EMAIL";
	
	public static final String CURSOR_DATOSSIOP_FACTURA = "HAMCV_FACTURA";
	
	public static final String CURSOR_DATOSSIOP_MONTOFACTURA = "HAMCN_MONTOFACTURA";
	
	public static final String CURSOR_DATOSSIOP_IGVFACTURA = "HAMCN_IGVFACTURA";
	
	public static final String CURSOR_DATOSSIOP_IGVVIGENTE = "HAMCN_IGVVIGENTE";	
	
	public static final String CURSOR_DATOSSIOP_EMISION = "HAMCV_EMISION";
	
	public static final String CURSOR_DATOSSIOP_AMPLIACION = "HAMCV_AMPLIACION";

	public static final String CURSOR_DATOSSIOP_BILLCYCLE = "HAMCV_BILLCYCLE";
	
	public static final String CURSOR_DATOSSIOP_INICIOCICLO = "HAMCD_FECHAINICIO";

	public static final String CURSOR_DATOSSIOP_FINCICLO = "HAMCD_FECHAFIN";

	public static final String CURSOR_DATOSSIOP_PLAN = "HAMDV_PLAN";
	
	public static final String CURSOR_DATOSSIOP_DESPLAN = "HAMDV_DESPLAN";
	
	public static final String CURSOR_DATOSSIOP_SNCODE = "HAMDV_SNCODE";
	
	public static final String CURSOR_DATOSSIOP_DESSNCODE = "HAMDV_DESSNCODE";
	
	public static final String CURSOR_DATOSSIOP_CTACONTABLE = "HAMDV_CUENTACONTABLE";

	public static final String CURSOR_DATOSSIOP_CTACONTABLESAP = "HAMDV_CUENTACONTABLESAP";
	
	public static final String CURSOR_DATOSSIOP_MTOIGV = "HAMDN_MONTOSINIGV";
	
	public static final String CURSOR_DATOSSIOP_AFECTOIGV = "HAMDC_AFECTO";
	
	public static final String CURSOR_DATOSSIOP_AFECTOINIGV = "HAMDN_MONTOCONIGV";
	
	public static final String CURSOR_DATOSSIOP_CENTRO = "HAMCV_CENTROBENEF";
	
	public static final String CURSOR_DATOSSIOP_EVENTO = "HAMDV_EVENTO";
	
	public static final String CADENA_NUMERO_NULO = "0";
	
	public static final String CURSOR_CABECERA_ID= "TAMCI_ID";
	
	public static final String CURSOR_CABECERA_ID_PROCESO = "TAMCI_IDPROCESO";
	
	public static final String CURSOR_CABECERA_CODIGO_AJUSTE = "TAMCV_CODAJUSTE";

	public static final String CURSOR_CABECERA_TIPO_AJUSTE = "TAMCV_TIPOAJUSTE";	
	
	public static final String CURSOR_CABECERA_TIPO_DOC_AJUSTE = "TAMCV_TIPODOCAJUSTE";
	
	public static final String CURSOR_CABECERA_DOC_REFERENCIA = "TAMCV_DOCREFERENCIA";

	public static final String CURSOR_CABECERA_SN_CODE = "TAMDV_SNCODE";

	public static final String CURSOR_CABECERA_EVENTO = "TAMDV_EVENTO";	
	
	public static final String COD_TIPO_AJUSTE_TOTAL= "AJUSTE_TOTAL";
	public static final String COD_TIPO_AJUSTE_PARCIAL_ROAMING = "AJUSTE_PARCIAL_ROAMING";
	public static final String COD_TIPO_AJUSTE_PARCIAL_TRAFICO = "AJUSTE_PARCIAL_TRAFICO";
	public static final String COD_TIPO_AJUSTE_PARCIAL_SVA = "AJUSTE_PARCIAL_SVA";
	public static final String COD_TIPO_AJUSTE_PARCIAL_OSC = "AJUSTE_PARCIAL_OSC";
	public static final String COD_TIPO_AJUSTE_PARCIAL_CARGO_FIJO = "AJUSTE_PARCIAL_CARGO_FIJO";
	public static final String COD_TIPO_AJUSTE_REFACTURACION = "AJUSTE_REFACTURACION";
	public static final String COD_TIPO_AJUSTE_TOTAL_REFACTURACION = "AJUSTE_TOTAL_REFACTURACION";
	public static final String COD_TIPO_AJUSTE_REFACTURACION_TOTAL = "AJUSTE_REFACTURACION_TOTAL";
	public static final String COD_ESTADO = "PI_ESTADO";

	
	public static final String IDPROCESO = "BAMCI_IDPROCESO";
	public static final String ID = "BAMCI_ID";
	public static final String CUSTOMERID = "BAMCV_CUSTOMERID";
	public static final String CUSTCODE = "BAMCV_CUSTCODE";
	public static final String RUC = "BAMCV_RUC";
	public static final String RAZONSOCIAL = "BAMCV_RAZONSOCIAL";
	public static final String CONTACTO = "BAMCV_CONTACTO";
	public static final String TIPOCLIENTE = "BAMCV_TIPOCLIENTE";
	public static final String DIRECCION = "BAMCV_DIRECCION";
	public static final String NOTADIR = "BAMCV_NOTADIR";
	public static final String DEPARTAMENTO = "BAMCV_DEPARTAMENTO";
	public static final String PROVINCIA = "BAMCV_PROVINCIA";
	public static final String DISTRITO = "BAMCV_DISTRITO";
	public static final String TELEFONO = "BAMCV_TELEFONO";
	public static final String AFILIACION = "BAMCV_AFILIACION";
	public static final String EMAIL = "BAMCV_EMAIL";
	public static final String FACTURA = "BAMCV_FACTURA";
	public static final String MONTOFACTURA = "BAMCN_MONTOFACTURA";
	public static final String IGV = "BAMCN_IGV";
	public static final String IGVVIGENTE = "BAMCN_IGVVIGENTE";
	public static final String EMISION = "BAMCV_EMISION";
	public static final String AMPLIACION = "BAMCV_AMPLIACION";
	public static final String BILLCYCLE = "BAMCV_BILLCYCLE";
	public static final String INICIOCICLO = "BAMCV_INICIOCICLO";
	public static final String FINCICLO = "BAMCV_FINCICLO";
	public static final String PLAN = "BAMCV_PLAN";
	public static final String DESPLAN = "BAMCV_DESPLAN";
	public static final String SERV = "BAMCV_SERV";
	public static final String DESSERV = "BAMCV_DESSERV";
	public static final String CTAPER = "BAMCV_CTAPER";
	public static final String CTAMEX = "BAMCV_CTAMEX";
	public static final String MONTOSERV = "BAMCN_MONTOSERV";
	public static final String AFECTOIGV = "BAMCV_AFECTOIGV";
	public static final String CEBE = "BAMCV_CEBE";
	public static final String CTAIGV = "BAMCV_CTAIGV";
	public static final String PORCENTAJE = "BAMCN_PORCENTAJE";
	public static final String MONTO_IGV = "PI_MONTOIGV";
	public static final String MONTO_SINIGV = "PI_MONTOSINIGV";
	public static final String MONTO_INAFECTO = "PI_MONTOINAFECTO";
	
	public static final String ID_PROCESO         = "PI_IDPROCESO";
	public static final String CUENTACONTABLE    = "PI_CUENTACONTABLE";    
	public static final String EVENTO            = "PI_EVENTO";
	public static final String CUENTACONTABLESAP = "PI_CUENTACONTABLESAP";
	public static final String AFECTO            = "PI_AFECTO";
	public static final String MONTOSINIGV       = "PI_MONTOSINIGV";
	public static final String MONTOCONIGV       = "PI_MONTOCONIGV";
	public static final String NOMBRERUBRO       = "PI_NOMBRERUBRO";
	public static final String COMCEPTORUBRO     = "PI_COMCEPTORUBRO";
	public static final String SNCODE            = "PI_SNCODE";
	public static final String TMCODE            = "PI_TMCODE";
	public static final String LINEA             = "PI_LINEA";
	public static final String I_PLAN              = "PI_PLAN";
	public static final String CENTROBENEF       = "PI_CENTROBENEF";
	public static final String ESTADO= "PI_ESTADO";

	public static final String BLOQUEO_VALID = "BLOQUEADO_VALIDACION";
	public static final String APROB_VALID = "APROBADO_VALIDACION";
	public static final String RECHAZADO_VALID = "RECHAZADO_VALIDACION";
	public static final String IS_AFECTOIGV = "SI";
	public static final String IGV_DEFAULT = "18";
	
	public static final String CADENA_BSCS = "T001";
	
	public static final String CADENA_SIOP = "SB99";
	public static final String ERROR_GENERAL = " Ocurrio un error durante la ejecucion ";
	public static final String TABLA_TEMPORAL = "BSCST_VALIDAM";
	public static final int INT_UNO = 1;
	// Codigos y Mensajes de Actividades

	public static final String T_CODIGO_RPTA_ACTIVIDAD_1 = "Codigo de Respuesta - Actividad 1:";
	public static final String T_MENSAJE_RPTA_ACTIVIDAD_1 = "Mensaje de Respuesta - Actividad 1:";

	public static final String T_CODIGO_RPTA_ACTIVIDAD_2 = "Codigo de Respuesta - Actividad 2:";
	public static final String T_MENSAJE_RPTA_ACTIVIDAD_2 = "Mensaje de Respuesta - Actividad 2:";

	public static final String T_CODIGO_RPTA_ACTIVIDAD_3 = "Codigo de Respuesta - Actividad 3:";
	public static final String T_MENSAJE_RPTA_ACTIVIDAD_3 = "Mensaje de Respuesta - Actividad 3:";

	public static final String T_CODIGO_RPTA_ACTIVIDAD_4 = "Codigo de Respuesta - Actividad 4:";
	public static final String T_MENSAJE_RPTA_ACTIVIDAD_4 = "Mensaje de Respuesta - Actividad 4:";

	public static final String INICIO = "[INICIO]";
	public static final String FIN = "[FIN]";

	public static final String DAO = "DAO";
	public static final String SERVICE = "Service";
	public static final String UTILITARIOS = "UTILITARIOS";
	public static final String FTP = "FTP";
	
	public static final String MENSAJE_EXITO = "Operacion exitosa";
	public static final String MENSAJE_ERROR = "Operacion errornea";

	public static final String CTL_SQLLDR="sqlldr ";
	public static final String CTL_USER_ID="userid=";
	public static final String CTL_CONTROL="control=";
	public static final String CTL_DATA="data=";
	public static final String CTL_LOG="log=";
	public static final String CTL_BAD="bad=";
	public static final String CTL_DISCARD="discard=";
	public static final String CTL_BINDSIZE="bindsize=";
	public static final String CTL_READSIZE="readsize=";
	public static final String CTL_ROWS="rows=";
	public static final String NO_MINUSCULA="no";
	
	public static final String TMP_IDPROCESO="TAMCI_IDPROCESO";
	public static final String TMP_CUSTOMERID="TAMCV_CUSTOMERID";
	public static final String TMP_CUSTCODE="TAMCV_CUSTCODE";
	public static final String TMP_RUC="TAMCV_RUC";
	public static final String TMP_RAZONSOCIAL="TAMCV_RAZONSOCIAL";
	public static final String TMP_CONTACTO="TAMCV_CONTACTO";
	public static final String TMP_DIRECCION="TAMCV_DIRECCION";
	public static final String TMP_NOTADIR="TAMCV_NOTADIR";
	public static final String TMP_DEPARTAMENTO="TAMCV_DEPARTAMENTO";
	public static final String TMP_PROVINCIA="TAMCV_PROVINCIA";
	public static final String TMP_DISTRITO="TAMCV_DISTRITO";
	public static final String TMP_NROTELEFONO="TAMCV_NROTELEFONO";
	public static final String TMP_AFILIACION="TAMCV_AFILIACION";
	public static final String TMP_EMAIL="TAMCV_EMAIL";
	public static final String TMP_FACTURA="TAMCV_FACTURA";
	public static final String TMP_MONTOFACTURA="TAMCN_MONTOFACTURA";
	public static final String TMP_EMISION="TAMCV_EMISION";
	public static final String TMP_AMPLIACION="TAMCV_AMPLIACION";
	public static final String TMP_BILLCYCLE="TAMCV_BILLCYCLE";
	public static final String TMP_FECHAINICIO="TAMCD_FECHAINICIO";
	public static final String TMP_FECHAFIN="TAMCD_FECHAFIN";
	public static final String TMP_PLAN="TAMDV_PLAN";
	public static final String TMP_SNCODE="TAMDV_SNCODE";
	public static final String TMP_CUENTACONTABLE="TAMDV_CUENTACONTABLE";
	public static final String TMP_CUENTACONTABLESAP="TAMDV_CUENTACONTABLESAP";
	public static final String TMP_MONTOSINIGV="TAMDN_MONTOSINIGV";
	public static final String TMP_MONTOCONIGV="TAMDN_MONTOCONIGV";
	public static final String TMP_CENTROBENEF="TAMCV_CENTROBENEF";
	

	
	//LGM:Campos para registrar el detalle
	
	public static final String TMP_DET_ID = "TAMDI_ID";
	public static final String TMP_DET_IDPROCESO = "TAMDI_IDPROCESO";
	public static final String TMP_DET_CUENTACONTABLE = "TAMDV_CUENTACONTABLE";
	public static final String TMP_DET_EVENTO = "TAMDV_EVENTO";
	public static final String TMP_DET_CUENTACONTABLESAP = "TAMDV_CUENTACONTABLESAP";
	public static final String TMP_DET_AFECTO = "TAMDC_AFECTO";
	public static final String TMP_DET_MONTOSINIGV = "TAMDN_MONTOSINIGV";
	public static final String TMP_DET_MONTOCONIGV = "TAMDN_MONTOCONIGV";
	public static final String TMP_DET_NOMBRERUBRO = "TAMDV_NOMBRERUBRO";
	public static final String TMP_DET_COMCEPTORUBRO = "TAMDV_COMCEPTORUBRO";
	public static final String TMP_DET_SNCODE = "TAMDV_SNCODE";
	public static final String TMP_DET_TMCODE = "TAMDV_TMCODE";
	public static final String TMP_DET_LINEA = "TAMDV_LINEA";
	public static final String TMP_DET_PLAN = "TAMDV_PLAN";
	public static final String TMP_DET_CENTROBENEF = "TAMCV_CENTROBENEF";
	public static final String TMP_DET_ESTADO = "TAMDV_ESTADO";
	public static final String TMP_DET_FECHAREGISTRO = "TAMDD_FECHAREGISTRO";

	
	public static final String PI_ID = "PI_ID";
	public static final String PI_IDPROCESO = "PI_IDPROCESO";
	public static final String PI_CUENTACONTABLE = "PI_CUENTACONTABLE";
	public static final String PI_EVENTO = "PI_EVENTO";
	public static final String PI_CUENTACONTABLESAP = "PI_CUENTACONTABLESAP";
	public static final String PI_AFECTO = "PI_AFECTO";
	public static final String PI_MONTOSINIGV = "PI_MONTOSINIGV";
	public static final String PI_MONTOCONIGV = "PI_MONTOCONIGV";
	public static final String PI_NOMBRERUBRO = "PI_NOMBRERUBRO";
	public static final String PI_COMCEPTORUBRO = "PI_COMCEPTORUBRO";
	public static final String PI_SNCODE = "PI_SNCODE";
	public static final String PI_TMCODE = "PI_TMCODE";
	public static final String PI_LINEA = "PI_LINEA";
	public static final String PI_PLAN = "PI_PLAN";
	public static final String PI_CENTROBENEF = "PI_CENTROBENEF";
	public static final String PI_ESTADO = "PI_ESTADO";
	public static final String PI_CTAIGV = "PI_CTAIGV";
	public static final String PI_PORCENTAJE = "PI_PORCENTAJE";
	public static final String PI_ROWID_DET = "PI_ROWID_DET";

	//SP de actualizar montos en el detalle proveniente de BSCS
	public static final String PI_SPACTMONDET_ID = "PI_ID";
	public static final String PI_SPACTMONDET_IDPROCESO = "PI_IDPROCESO";
	public static final String PI_SPACTMONDET_SNCODE = "PI_SNCODE";
	public static final String PI_SPACTMONDET_MONTOCONIGV = "PI_MONTOCONIGV";
	
	// Campos para listar el detalle del temporal
	public static final String TMP_DETALLE_ID = "TAMCI_ID";
	public static final String TMP_DETALLE_IDPROCESO = "TAMCI_IDPROCESO";
	public static final String TMP_DETALLE_DOCREFERENCIA = "TAMCV_DOCREFERENCIA";
	public static final String TMP_DETALLE_SNCODE = "TAMDV_SNCODE";
	public static final String TMP_DETALLE_AFECTO = "TAMDC_AFECTO";
	public static final String TMP_DETALLE_CUSTOMERID = "TAMCV_CUSTOMERID";
	public static final String TMP_DETALLE_CUSTCODE = "TAMCV_CUSTCODE";
	public static final String TMP_DETALLE_RUC = "TAMCV_RUC";
	public static final String TMP_DETALLE_RAZONSOCIAL = "TAMCV_RAZONSOCIAL";
	public static final String TMP_DETALLE_CONTACTO = "TAMCV_CONTACTO";
	public static final String TMP_DETALLE_DIRECCION = "TAMCV_DIRECCION";
	public static final String TMP_DETALLE_NOTADIR = "TAMCV_NOTADIR";
	public static final String TMP_DETALLE_DEPARTAMENTO = "TAMCV_DEPARTAMENTO";
	public static final String TMP_DETALLE_PROVINCIA = "TAMCV_PROVINCIA";
	public static final String TMP_DETALLE_DISTRITO = "TAMCV_DISTRITO";
	public static final String TMP_DETALLE_NROTELEFONO = "TAMCV_NROTELEFONO";
	public static final String TMP_DETALLE_AFILIACION = "TAMCV_AFILIACION";
	public static final String TMP_DETALLE_EMAIL = "TAMCV_EMAIL";
	public static final String TMP_DETALLE_FACTURA = "TAMCV_FACTURA";
	public static final String TMP_DETALLE_MONTOFACTURA = "TAMCN_MONTOFACTURA";
	public static final String TMP_DETALLE_EMISION = "TAMCV_EMISION";
	public static final String TMP_DETALLE_AMPLIACION = "TAMCV_AMPLIACION";
	public static final String TMP_DETALLE_BILLCYCLE = "TAMCV_BILLCYCLE";
	public static final String TMP_DETALLE_FECHAINICIO = "TAMCD_FECHAINICIO";
	public static final String TMP_DETALLE_FECHAFIN = "TAMCD_FECHAFIN";
	public static final String TMP_DETALLE_PLAN = "TAMDV_PLAN";
	public static final String TMP_DETALLE_CUENTACONTABLE = "TAMDV_CUENTACONTABLE";
	public static final String TMP_DETALLE_CUENTACONTABLESAP = "TAMDV_CUENTACONTABLESAP";
	public static final String TMP_DETALLE_MONTOSINIGV = "TAMDN_MONTOSINIGV";
	public static final String TMP_DETALLE_MONTOCONIGV = "TAMDN_MONTOCONIGV";
	public static final String TMP_DETALLE_CENTROBENEF = "TAMCV_CENTROBENEF";
	public static final String TMP_DETALLE_EVENTO = "TAMDV_EVENTO";
	//Actualizar Montos Cabecera MONTOSINIGV MONTOCONIGV	
	public static final String TMP_IN_MONTOSINIGV = "PI_MONTOSINIGV";
	public static final String TMP_IN_MONTOINAFECTO = "PI_MONTOINAFECTO";
	public static final String TMP_IN_MONTOCONIGV = "PI_MONTOCONIGV";
	//Parametro Entrada doc ajuste
	public static final String  PI_DOC_AJUSTE = "PI_DOC_AJUSTE";
	public static final String  TIPO_AJUSTE_HIS = "AJUS_HIST";
	public static final String  CODIGO_EXIS_REC = "2";
	public static final String  CAMPO_HISTORICO_TIPO_AJUSTE = "HAMCV_TIPOAJUSTE";
	public static final String  CAMPO_TIPO_AJUSTE_NC = "NC";
	public static final String  CAMPO_TIPO_AJUSTE_ND = "ND";
	public static final String  CAMPO_HISTORICO_MONTO_IGV = "HAMDN_MONTOCONIGV";
	public static final String  CAMPO_TMP_TMCODE = "TAMDV_TMCODE";
	public static final String  CAMPO_CABECERA_TIPO_CLIENTE = "HAMCV_TIPOCLIENTE";
	public static final String  CAMPO_ROWID_DET = "ROWID_DET";
	
	public static final String  PARAM_BSCS_TIPOAJUSTE = "PI_TIPOAJUST";
	public static final String  PARAM_BSCS_TMCODE = "PI_TMCODE";
	public static final String  PARAM_BSCS_SNCODE = "PI_SNCODE";
	public static final String  PARAM_BSCS_CTACONT = "PI_CTACONT";
	public static final String  PARAM_BSCS_CUSTOMERID = "PI_CUSTOMERID";
	public static final String  PARAM_BSCS_DOCREFERENCIA = "PI_DOCREFERENCIA";
	public static final String  PARAM_BSCS_IDPROCESO = "PI_IDPROCESO";


	public static final String PI_TELEFONO= "PI_TELEFONO";
	public static final String PI_INICIOCICLO= "PI_INICIOCICLO";
	public static final String PI_FINCICLO= "PI_FINCICLO";
	public static final String PI_CUSTCODE= "PI_CUSTCODE";
	public static final String PI_RUC= "PI_RUC";
	public static final String PI_RAZONSOCIAL= "PI_RAZONSOCIAL";
	public static final String PI_CONTACTO= "PI_CONTACTO";
	public static final String PI_TIPOCLIENTE= "PI_TIPOCLIENTE";
	public static final String PI_DIRECCION= "PI_DIRECCION";
	public static final String PI_NOTADIR= "PI_NOTADIR";
	public static final String PI_DEPARTAMENTO= "PI_DEPARTAMENTO";
	public static final String PI_PROVINCIA= "PI_PROVINCIA";
	public static final String PI_DISTRITO= "PI_DISTRITO";
	public static final String PI_AFILIACION= "PI_AFILIACION";
	public static final String PI_EMAIL= "PI_EMAIL";
	public static final String PI_FACTURA= "PI_FACTURA";
	public static final String PI_EMISION= "PI_EMISION";
	public static final String PI_AMPLIACION= "PI_AMPLIACION";
	public static final String PI_BILLCYCLE= "PI_BILLCYCLE";
	public static final String PI_MONTOFACTURA= "PI_MONTOFACTURA";
	public static final String PI_IIDPROCESO= "PI_IDPROCESO";
	public static final String PI_IID= "PI_ID";
	
	public static final String CUR_NOMBRERUBRO = "NOMBRE_RUBRO";
	public static final String CUR_CONCEPTORUBRO = "CONCEPTO_RUBRO";
	public static final String PIRUB_TIPOAJUSTE = "PI_TIPOAJUST";
	public static final String PIRUB_SNCODE = "PI_SNCODE";
	

	public static final String PI_DESPLAN = "PI_DESPLAN";
	public static final String PI_CEBEF = "PI_CEBEF";
	public static final String PI_CTACONTABLE = "PI_CTACONTABLE";
	public static final String PI_CTASAP = "PI_CTASAP";
	public static final String PI_NOMRUBRO= "PI_NOMBRUBRO";
	public static final String PI_CONRUBRO = "PI_CONCRUBRO";
	
	public static final String PO_REFAC = "4";
	public static final int CERO_INT = 0;
	
	public static final String PI_FECHAEMISION = "pi_fechaemision";
	public static final String PI_FECHAVENC = "pi_fechavenc";
	public static final String PO_FECHAEMI= "HAMCV_FECHAEMISIONAJ";
	public static final String PO_FECHAVEN= "HAMCV_FECHAVENCAJ";
	public static final String PO_CTAIGV = "HAMDV_CTAIGV";
	public static final String PO_PORCENTAJE = "HAMDN_PORCENTAJE";
	public static final String PO_ID = "HAMDI_ID";
	public static final String PO_TMCODE = "HAMDV_TMCODE";
	
	public static final String PROCESO_BSCS = "BSCS";
	public static final String PROCESO_SIOP = "SIOP";
	
}
