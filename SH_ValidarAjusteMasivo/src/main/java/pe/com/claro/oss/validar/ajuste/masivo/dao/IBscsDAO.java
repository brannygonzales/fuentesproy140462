package pe.com.claro.oss.validar.ajuste.masivo.dao;

import pe.com.claro.oss.validar.ajuste.masivo.bean.GenerarArchivoPlanoBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.InsertarTemporalBscsBean;
import pe.com.claro.oss.validar.ajuste.masivo.exception.DBException;

public interface IBscsDAO {
	
	InsertarTemporalBscsBean insertarTemporalBSCS(InsertarTemporalBscsBean bean, String mensajeTransaccion) throws DBException;
	GenerarArchivoPlanoBean generarArchivoPano(GenerarArchivoPlanoBean bean, String mensajeTransaccion) throws DBException;
}
