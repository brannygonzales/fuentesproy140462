package pe.com.claro.oss.validar.ajuste.masivo.util;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PropertiesExternos {

	@Value("${log4j.file.dir}")
	public String vLog4JDir;

	// CTL
	@Value("${ctl.userapp.file.dir}")
	public String vRutaCtlUserApp;

	@Value("${nombre.archivo.usuario.app.ctl}")
	public String pNOMBRE_ARCHIVO_USUARIO_APP_CTL;
	
	@Value("${nombre.archivo.usuario.app.ctl.error}")
	public String pNOMBRE_ARCHIVO_USUARIO_APP_CTL_ERROR;

	@Value("${extension.archivo.ctl}")
	public String pEXTENSION_ARCHIVO_CTL;

	@Value("${extension.archivo.bad}")
	public String pEXTENSION_ARCHIVO_BAD;

	@Value("${extension.archivo.dsc}")
	public String pEXTENSION_ARCHIVO_DSC;

	@Value("${extension.archivo.log}")
	public String pEXTENSION_ARCHIVO_LOG;
	
	@Value("${max.bindsize.ctl}")
	public int pBindSizeCTL;

	@Value("${max.readsize.ctl}")
	public int pReadSizeCTL;

	@Value("${max.rows.ctl}")
	public int prowsCTL;
	
	// SQLLDR
	@Value("${log.ctl.userapp.file.dir}")
	public String vRutaLogCtlUserApp;

	@Value("${data.ctl.userapp.file.dir}")
	public String vRutaDataCtlUserApp;

	@Value("${bad.ctl.userapp.file.dir}")
	public String vRutaBadCtlUserApp;

	@Value("${discard.ctl.userapp.file.dir}")
	public String vRutaDiscarCtlUserApp;
	
	//ARCHIVO PLANO LOCAL
	@Value("${nombre.archivo.app}")
	public String pNOMBRE_ARCHIVO_APP;

	
	@Value("${nombre.archivo.txt.app}")
	public String pNOMBRE_ARCHIVO_APP_TXT;

	@Value("${formato.fecha}")
	public String pFORMATO_FECHA;

	@Value("${formato.nombre.yyyyddmm}")
	public String pFORMATO_FECHA_NOMBRE;
	@Value("${oracle.jdbc.conexion.siop.ora}")
	public String cadenaSQLLDRSiop;
	
	
	//EXTENSION  ARCHIVO
	@Value("${extension.archivo.txt}")
	public String pEXTENSION_ARCHIVO_TXT;

	@Value("${extension.archivo.zip}")
	public String pEXTENSION_ARCHIVO_ZIP;

	//FTP FILESERVER
	@Value("${usuario.fileserver.sftp}")
    public String pUsuarioFileServerSftp;
    
    @Value("${password.fileserver.sftp}")
    public String pPasswordFileServerSftp;
    
    @Value("${nombre.ruta.archivo.fileserver.remota}")
    public String pRUTA_FILESERVER_ARCHIVO_REMOTA;

    @Value("${servidor.sftp.fileserver}")
    public String pServidorFileServerSftp;
    
    @Value("${mover.sftp.cantidad.reintentos}")
    public String pCANTIDAD_REINTENTOS;
    
    @Value("${carga.sftp.tipo.canal.sesion}")
    public String pTIPO_CANAL_SESION;
    
    @Value("${sftp.tiempo.reintento.conexion}")
    public Long pTIEMPO_REINTENTO_CONEXION;
    
    @Value("${carga.sftp.error.codigo}")
    public String pSFTP_ERROR_CODE;

    @Value("${carga.sftp.error.descripcion}")
    public String pSFTP_ERROR_DESCRIPTION;

    @Value("${obtiene.error.detalle.sftp}")
    public String pSFTP_ERROR_DESCRIPTION_OBTENER;

    @Value("${obtiene.error.detalle.bd}")
    public String pBD_ERROR_DESCRIPTION_OBTENER;
    
    @Value("${puerto.sftp}")
    public String pPuertoSftp;
    
    @Value("${timeOut.fileserver.sftp}")
    public String pTimeOutFileserverSftp;
    
    // BD
    
    @Value("${db.bscs}")
    public String dbBscsdbNombre;
    
    @Value("${db.bscs.owner}")
	public String dbBscsdbOwner;

    @Value("${oracle.jdbc.conexion.bscs}")
    public String dbBscsdbConexion;

    @Value("${oracle.jdbc.usuario.bscs}")
    public String dbBscsdbUsuario;

    @Value("${oracle.jdbc.password.bscs}")
    public String dbBscsdbPassword;
    
    @Value("${db.bscs.execution.timeout}")
	public int DB_BSCS_EXECUTION_TIMEOUT;
    
    @Value("${db.bscs.pkg_ajustemasivo}")
    public String dbBscsPkgAjusteMasivo;
    @Value("${db.bscs.insertartmeporal}")
    public String dbBscsIsertarTemporal;
    @Value("${db.bscs.insertartmeporal.input.proceso}")
    public String dbBscsIsertarTemporalInputProceso;
    @Value("${db.bscs.insertartmeporal.input.custid}")
    public String dbBscsIsertarTemporalInputCustId;
    @Value("${db.bscs.insertartmeporal.input.recibo}")
    public String dbBscsIsertarTemporalInputRecibo;
    @Value("${db.bscs.insertartmeporal.input.servicio}")
    public String dbBscsIsertarTemporalInputServicio;
    @Value("${db.bscs.insertartmeporal.input.evento}")
    public String dbBscsIsertarTemporalInputEvento;
    @Value("${db.bscs.generararchivoplano}")
    public String dbBscsGeneraArchivoPlano;
    //SIOP
    @Value("${db.siop}")
    public String dbSiopdbNombre;
    
    @Value("${db.siop.owner}")
	public String dbSiopdbOwner;

    @Value("${oracle.jdbc.conexion.siop}")
    public String dbSiopdbConexion;

    @Value("${oracle.jdbc.usuario.siop}")
    public String dbSiopdbUsuario;

    @Value("${oracle.jdbc.password.siop}")
    public String dbSiopdbPassword;
    
    @Value("${db.siop.execution.timeout}")
	public int DB_SIOP_EXECUTION_TIMEOUT;
    
    @Value("${db.siop.connection.timeout}")
	public int DB_SIOP_CONNECTION_TIMEOUT;
    
	@Value("${db.siop.pkg_ajustemasivo}")
	public String dbSiopPkgAjusteMasivo;

	@Value("${db.siop.listarprocesos.columna.idproceso}")
	public String dbSiopListarProcesosColumnaIdProceso;
	
	@Value("${db.siop.listarprocesosexcel}")
	public String dbSiopListarProcesosExcel;
	
	@Value("${db.siop.listarprocesosexcel.columna.idproceso}")
	public String dbSiopListaProcesosExcelColumnaIdProceso;
	
	@Value("${db.siop.listarprocesosexcel.cursor}")
	public String dbSiopListaProcesosExcelCursor;
	
	@Value("${db.siop.actualizaprocesotmp}")
	public String dbSiopActualizaProcesoTmp;
	@Value("${db.siop.actualizaprocesotmp.input.idproceso}")
	public String dbSiopActualizaProcesoTmpInputIdProceso;
	@Value("${db.siop.actualizaprocesotmp.cursor}")
	public String dbSiopActualizaProcesoTmpCursor;
	@Value("${db.siop.actualizaprocesotmp.columna.proceso}")
	public String dbSiopActualizaProcesoTmpColumnaProceso;
	@Value("${db.siop.actualizaprocesotmp.columna.custid}")
	public String dbSiopActualizaProcesoTmpColumnaCustId;
	@Value("${db.siop.actualizaprocesotmp.columna.recibo}")
	public String dbSiopActualizaProcesoTmpColumnaRecibo;
	@Value("${db.siop.actualizaprocesotmp.columna.servicio}")
	public String dbSiopActualizaProcesoTmpColumnaServicio;
	@Value("${db.siop.actualizaprocesotmp.columna.evento}")
	public String dbSiopActualizaProcesoTmpColumnaEvento;
	@Value("${db.siop.obtenerajustesiop}")
	public String dbSiopObtenerAjusteSiop;
	@Value("${db.siop.listarcabeceraajuste}")
	public String dbSiopObtenerCabeceraAjusteSiop;
	@Value("${db.siop.listardetarchplano}")
	public String dbSiopListarArchPlano;
	@Value("${db.siop.actestado}")
	public String dbSiopActEstado;
	@Value("${db.siop.listartemporal}")
	public String dbSiopListaTmp;
	@Value("${db.siop.insdetalle.bscs.total}")
	public String dbSiopRegDetalleBSCSTotal;
	@Value("${db.siop.actualizar.monto.detalle.bscs}")
	public String dbSiopActualizaMontosDetBSCS;
	@Value("${db.siop.actualizar.monto.cab.bscs}")
	public String dbSiopActualizaMontosCabBSCS;
	@Value("${db.siop.obtenerajustesiop.ajus}")
	public String dbSiopListaHistoricoAjus;
	@Value("${db.siop.actestado.proc}")
	public String dbSiopActEstadoProc;
	@Value("${db.siop.lista.bscs.ajus}")
	public String dbSiopDetBscsAjus;
	@Value("${db.siop.lista.detalle.historico.siop}")
	public String dbSiopDetHistSiop;
	@Value("${db.siop.completa.cabecera}")
	public String dbSiopCompletCabecera;
	@Value("${db.siop.obt.rubro}")
	public String dbSiopObtRubro;
	@Value("${db.siop.act.mto.cab}")
	public String dbSiopActMtoCab;
	@Value("${db.siop.updt.bscs.par}")
	public String dbSiopUpdtBscsPar;
	@Value("${db.siop.ins.tmpdet.am}")
	public String dbSiopInsTmpdetAm;
	@Value("${db.siop.elim.tmp.bscs}")
	public String dbSiopElimTmpBscs;
	@Value("${db.siop.elim.tmp.det}")
	public String dbSiopElimTmpDet;
	
	//OAC
    @Value("${db.oac}")
    public String dbOacbNombre;
    
    @Value("${db.oac.owner}")
	public String dbOacdbOwner;

    @Value("${oracle.jdbc.conexion.oac}")
    public String dbOacdbConexion;

    @Value("${oracle.jdbc.usuario.oac}")
    public String dbOacdbUsuario;

    @Value("${oracle.jdbc.password.oac}")
    public String dbOacdbPassword;
    
    @Value("${db.oac.execution.timeout}")
	public int DB_OAC_EXECUTION_TIMEOUT;
    
    @Value("${db.oac.connection.timeout}")
	public int DB_OAC_CONNECTION_TIMEOUT;
    
	@Value("${db.oac.pkg_ajustemasivo}")
	public String dbOacPkgAjusteMasivo;

	@Value("${db.oac.listarajustes}")
	public String dbOacListarAjustes;
    // IDT DB
 	@Value("${sp.idt1.codigo}")
 	public String spIdt1Codigo;

 	@Value("${sp.idt1.mensaje}")
 	public String spIdt1Mensaje;

 	@Value("${sp.idt2.codigo}")
 	public String spIdt2Codigo;

 	@Value("${sp.idt2.mensaje}")
 	public String spIdt2Mensaje;
    
 	@Value("${serie.documento.bscs}")
 	public String serieDocumentoBscs;
    
}
