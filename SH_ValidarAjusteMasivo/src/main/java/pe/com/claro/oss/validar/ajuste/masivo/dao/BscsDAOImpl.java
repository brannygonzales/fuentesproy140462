
package pe.com.claro.oss.validar.ajuste.masivo.dao;

import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.com.claro.oss.validar.ajuste.masivo.bean.GenerarArchivoPlanoBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.InsertarTemporalBscsBean;
import pe.com.claro.oss.validar.ajuste.masivo.exception.DBException;
import pe.com.claro.oss.validar.ajuste.masivo.util.Constantes;
import pe.com.claro.oss.validar.ajuste.masivo.util.PropertiesExternos;
import pe.com.claro.oss.validar.ajuste.masivo.util.Utilitarios;

@Repository
public class BscsDAOImpl implements IBscsDAO {

	private static Logger logger = Logger.getLogger(BscsDAOImpl.class);
	@Autowired
	private PropertiesExternos propertiesExterno;
	@Autowired
	@Qualifier("bscsDS")
	private DataSource bscsDS;
	
	SimpleJdbcCall objJdbcCall = null;

	JdbcTemplate objJdbcTemplate = null;
	
	@Override
	public InsertarTemporalBscsBean insertarTemporalBSCS(InsertarTemporalBscsBean bean, String mensajeTransaccion) throws DBException {
		
		String metodoListarProceso = "insertarTemporalBscs";

		String cadMensaje = mensajeTransaccion + "[" + metodoListarProceso + "] ";

		logger.info(cadMensaje + Utilitarios.msjInicioProceso(metodoListarProceso, Constantes.DAO));

		Utilitarios.infoConexion(mensajeTransaccion, propertiesExterno.dbBscsdbConexion, propertiesExterno.dbBscsdbNombre,
				propertiesExterno.dbBscsdbOwner, propertiesExterno.dbBscsPkgAjusteMasivo,
				propertiesExterno.dbBscsIsertarTemporal);
		InsertarTemporalBscsBean response = null;
		
		try {
			logger.info(
					mensajeTransaccion + "Intento [" + metodoListarProceso + "] : ");
			response = new InsertarTemporalBscsBean();
			objJdbcCall = new SimpleJdbcCall(bscsDS);
			objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			objJdbcTemplate.setQueryTimeout(propertiesExterno.DB_BSCS_EXECUTION_TIMEOUT);
			
			SqlParameter inProceso = new SqlParameter(propertiesExterno.dbBscsIsertarTemporalInputProceso, OracleTypes.INTEGER);
			SqlParameter inCustId= new SqlParameter(propertiesExterno.dbBscsIsertarTemporalInputCustId, OracleTypes.INTEGER);
			SqlParameter inRecibo= new SqlParameter(propertiesExterno.dbBscsIsertarTemporalInputRecibo, OracleTypes.VARCHAR);
			SqlParameter inServicio= new SqlParameter(propertiesExterno.dbBscsIsertarTemporalInputServicio, OracleTypes.INTEGER);
			SqlParameter inEvento= new SqlParameter(propertiesExterno.dbBscsIsertarTemporalInputEvento, OracleTypes.VARCHAR);
			SqlOutParameter outCodigoRespuesta = new SqlOutParameter(Constantes.CODIGORESPUESTA,
					OracleTypes.VARCHAR);
			SqlOutParameter outMensajeRespuesta = new SqlOutParameter(Constantes.MENSAJERESPUESTA,
					OracleTypes.VARCHAR);
			
			
			SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess()
					.withSchemaName(propertiesExterno.dbBscsdbOwner)
					.withCatalogName(propertiesExterno.dbBscsPkgAjusteMasivo)
					.withProcedureName(propertiesExterno.dbBscsIsertarTemporal)
					.declareParameters(inProceso,inCustId,inRecibo,inServicio,inEvento, outCodigoRespuesta, outMensajeRespuesta);
			
			logger.info(cadMensaje + Constantes.MENSAJE_EJECUCION_SP + "[" + propertiesExterno.dbBscsdbOwner + "."
					+ propertiesExterno.dbBscsPkgAjusteMasivo + "."
					+ propertiesExterno.dbBscsIsertarTemporal + "]");
			
			MapSqlParameterSource map = new MapSqlParameterSource();
			map.addValue(propertiesExterno.dbBscsIsertarTemporalInputProceso, bean.getProceso());
			map.addValue(propertiesExterno.dbBscsIsertarTemporalInputCustId, bean.getCustid());
			map.addValue(propertiesExterno.dbBscsIsertarTemporalInputRecibo, bean.getRecibo());
			map.addValue(propertiesExterno.dbBscsIsertarTemporalInputServicio, bean.getServicio().equals(Constantes.CADENAVACIA)?null:bean.getServicio());
			map.addValue(propertiesExterno.dbBscsIsertarTemporalInputEvento, bean.getEvento());
			Map<String, Object> result = procedureConsulta.execute(map);
			
			String poCodRpta = (String) result.get(Constantes.CODIGORESPUESTA);

			String poMsjRpta = (String) result.get(Constantes.MENSAJERESPUESTA);
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + propertiesExterno.dbBscsIsertarTemporalInputProceso+ "]:" + bean.getProceso());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + propertiesExterno.dbBscsIsertarTemporalInputCustId+ "]:" + bean.getCustid());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + propertiesExterno.dbBscsIsertarTemporalInputRecibo+ "]:" + bean.getRecibo());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + propertiesExterno.dbBscsIsertarTemporalInputServicio+ "]:" + bean.getServicio());
			logger.info(cadMensaje + "PARAMETROS [INPUT][" + propertiesExterno.dbBscsIsertarTemporalInputEvento+ "]:" + bean.getEvento());


			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_COD_RPTA]:" + poCodRpta);
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_MSJ_RPTA]:" + poMsjRpta);

			response.setCodigoRespuesta(poCodRpta);
			response.setMensajeRespuesta(poMsjRpta);

		} catch (Exception e) {
			String codigoError = "";
			codigoError = propertiesExterno.spIdt1Codigo;
			String msgError = "Hubo un error tecnico al ejecutar el PROCEDURE: ["
					+ propertiesExterno.dbBscsdbOwner + "." + propertiesExterno.dbBscsPkgAjusteMasivo + "."
					+ propertiesExterno.dbBscsIsertarTemporal + "] ";
			response.setCodigoRespuesta(codigoError);
			response.setMensajeRespuesta(msgError);
			logger.error(mensajeTransaccion + e.getMessage(), e);
			logger.error(mensajeTransaccion + msgError);
			throw new DBException(codigoError,msgError, e);	
		}finally {
			logger.info(cadMensaje + Utilitarios.msjFinProceso(metodoListarProceso, Constantes.DAO));
		}
		
		return response;
	}

	@Override
	public GenerarArchivoPlanoBean generarArchivoPano(GenerarArchivoPlanoBean bean, String mensajeTransaccion)
			throws DBException {
		String metodoListarProceso = "generarArchivoPano";

		String cadMensaje = mensajeTransaccion + "[" + metodoListarProceso + "] ";

		logger.info(cadMensaje + Utilitarios.msjInicioProceso(metodoListarProceso, Constantes.DAO));

		Utilitarios.infoConexion(mensajeTransaccion, propertiesExterno.dbBscsdbConexion, propertiesExterno.dbBscsdbNombre,
				propertiesExterno.dbBscsdbOwner, propertiesExterno.dbBscsPkgAjusteMasivo,
				propertiesExterno.dbBscsIsertarTemporal);
		GenerarArchivoPlanoBean response = null;
		
		try {
			logger.info(
					mensajeTransaccion + "Intento [" + metodoListarProceso + "] : ");
			response = new GenerarArchivoPlanoBean();
			objJdbcCall = new SimpleJdbcCall(bscsDS);
			objJdbcTemplate = objJdbcCall.getJdbcTemplate();
			objJdbcTemplate.setQueryTimeout(propertiesExterno.DB_BSCS_EXECUTION_TIMEOUT);
			
			SqlParameter inProceso = new SqlParameter(Constantes.ENTRADA_PROCESO_BSCS, OracleTypes.INTEGER);
			SqlOutParameter outArchivoError = new SqlOutParameter(Constantes.ARCHIVO_PLANO_ERROR,
					OracleTypes.VARCHAR);
			SqlOutParameter outArchivoTotal = new SqlOutParameter(Constantes.ARCHIVO_PLANO_TOTAL,
					OracleTypes.VARCHAR);
			SqlOutParameter outArchivoParcial = new SqlOutParameter(Constantes.ARCHIVO_PLANO_PARCIAL,
					OracleTypes.VARCHAR);
			SqlOutParameter outCodigoRespuesta = new SqlOutParameter(Constantes.CODIGORESPUESTA_BSCS,
					OracleTypes.VARCHAR);
			SqlOutParameter outMensajeRespuesta = new SqlOutParameter(Constantes.MENSAJERESPUESTA_BSCS,
					OracleTypes.VARCHAR);
			
			
			SimpleJdbcCall procedureConsulta = objJdbcCall.withoutProcedureColumnMetaDataAccess()
					.withSchemaName(propertiesExterno.dbBscsdbOwner)
					.withCatalogName(propertiesExterno.dbBscsPkgAjusteMasivo)
					.withProcedureName(propertiesExterno.dbBscsGeneraArchivoPlano)
					.declareParameters(inProceso, outCodigoRespuesta, outMensajeRespuesta,outArchivoError,outArchivoTotal,outArchivoParcial);
			
			logger.info(cadMensaje + Constantes.MENSAJE_EJECUCION_SP + "[" + propertiesExterno.dbBscsdbOwner + "."
					+ propertiesExterno.dbBscsPkgAjusteMasivo + "."
					+ propertiesExterno.dbBscsGeneraArchivoPlano + "]");
			
			MapSqlParameterSource map = new MapSqlParameterSource();
			map.addValue(Constantes.ENTRADA_PROCESO_BSCS, bean.getCodigoProceso());
			Map<String, Object> result = procedureConsulta.execute(map);
			
			String poCodRpta = (String) result.get(Constantes.CODIGORESPUESTA_BSCS);

			String poMsjRpta = (String) result.get(Constantes.MENSAJERESPUESTA_BSCS);
			
			String poArchivoError = (String) result.get(Constantes.ARCHIVO_PLANO_ERROR);
			
			String poArchivoTotal = (String) result.get(Constantes.ARCHIVO_PLANO_TOTAL);
			
			String poArchivoParcial = (String) result.get(Constantes.ARCHIVO_PLANO_PARCIAL);
			
			logger.info(cadMensaje + "PARAMETROS [INPUT][PI_PROCESS_NUM]:" + bean.getCodigoProceso());
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_COD_RPTA]:" + poCodRpta);
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_MSJ_RPTA]:" + poMsjRpta);
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_AM_ERR]:" + poArchivoError);
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_AM_PARCIAL]:" + poArchivoParcial);
			logger.info(cadMensaje + "PARAMETROS [OUPUT][PO_AM_TOTAL]:" + poArchivoTotal);

			response.setCodigoRespuesta(poCodRpta);
			response.setMensajeRespuesta(poMsjRpta);
			response.setArchivoError(poArchivoError);
			response.setArchivoTotal(poArchivoTotal);
			response.setArchivoParcial(poArchivoParcial);
		} catch (Exception e) {
			String codigoError = "";
			codigoError = propertiesExterno.spIdt1Codigo;
			String msgError = "Hubo un error tecnico al ejecutar el PROCEDURE: ["
					+ propertiesExterno.dbBscsdbOwner + "." + propertiesExterno.dbBscsPkgAjusteMasivo + "."
					+ propertiesExterno.dbBscsGeneraArchivoPlano + "] ";
			response.setCodigoRespuesta(codigoError);
			response.setMensajeRespuesta(msgError);
			logger.error(mensajeTransaccion + e.getMessage(), e);
			logger.error(mensajeTransaccion + msgError);
			throw new DBException(codigoError,msgError, e);	
		}finally {
			logger.info(cadMensaje + Utilitarios.msjFinProceso(metodoListarProceso, Constantes.DAO));
		}
		
		return response;
	}
}
