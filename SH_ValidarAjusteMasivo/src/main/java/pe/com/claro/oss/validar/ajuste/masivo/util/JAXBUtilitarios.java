package pe.com.claro.oss.validar.ajuste.masivo.util;

import java.io.StringWriter;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import org.apache.log4j.Logger;
import org.apache.xmlbeans.XmlObject;


public class JAXBUtilitarios {
	private static Logger logger =  Logger.getLogger(JAXBUtilitarios.class);
	@SuppressWarnings("rawtypes")
	private static HashMap<Class, JAXBContext> mapContexts = new HashMap<Class, JAXBContext>();

	
	public  static String obtenerFechayyyyMMdd( String formato ){
	        logger.info( "INICIO: [obtenerFechaConsulta]" );
	        String obtenerFechaConsulta=null;
			Date fechaDate = null;
        
	        
			if (formato != null) {
				fechaDate=new Date();
				SimpleDateFormat formatter = new SimpleDateFormat(formato);
				
				try {
					obtenerFechaConsulta=formatter.format(fechaDate);
				} catch (Exception e) {
					logger.error( "ERROR [Exception]: " + e.getMessage(), e);

				}
			}
	        
			return obtenerFechaConsulta;
	                
	    }
	   
	@SuppressWarnings("rawtypes")
	private static JAXBContext obtainJaxBContextFromClass(Class clas) {
		JAXBContext context;
		context = mapContexts.get(clas);
		if (context == null) {
			try {
				logger.info("Inicializando jaxcontext... para la clase "
						+ clas.getName());
				context = JAXBContext.newInstance(clas);
				mapContexts.put(clas, context);
			} catch (Exception e) {
				logger.error("Error creando JAXBContext:", e);
			}
		}
		return context;
	}

	public String getXmlTextFromJaxB(Object objJaxB) {
		String commandoRequestEnXml = null;
		JAXBContext context;
		try {
			context = obtainJaxBContextFromClass(objJaxB.getClass());
			Marshaller marshaller = context.createMarshaller();
			StringWriter xmlWriter = new StringWriter();
			marshaller.marshal(objJaxB, xmlWriter);
			XmlObject xmlObj = XmlObject.Factory.parse(xmlWriter.toString());
			commandoRequestEnXml = xmlObj.toString();
		} catch (Exception e) {
			logger.error("Error parseando object to xml:", e);
		}
		return commandoRequestEnXml;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static String anyObjectToXmlText(Object objJaxB) {
		String commandoRequestEnXml = null;
		if (objJaxB != null) {
			JAXBContext context;
			try {
				context = obtainJaxBContextFromClass(objJaxB.getClass());
				Marshaller marshaller = context.createMarshaller();
				StringWriter xmlWriter = new StringWriter();
				marshaller.marshal(new JAXBElement(new QName("", objJaxB
						.getClass().getName()), objJaxB.getClass(), objJaxB),
						xmlWriter);
				XmlObject xmlObj = XmlObject.Factory
						.parse(xmlWriter.toString());
				commandoRequestEnXml = xmlObj.toString();
			} catch (Exception e) {
				logger.error("Error parseando object to xml:", e);
			}
		} else {
			commandoRequestEnXml = "El objeto es nulo";
		}
		return commandoRequestEnXml;
	}

	public static String log(Object obj) {
		try {
			Class<?> clazz = obj.getClass();
			Field[] classes = clazz.getDeclaredFields();
			HashMap<String, String> map = new HashMap<String, String>();
			for (Field field : classes) {
				boolean flag = field.isAccessible();
				field.setAccessible(true);
				map.put(field.getName(), String.valueOf(field.get(obj)));
				field.setAccessible(flag);
			}
			return map.toString();
		} catch (Exception ex) {
			logger.error("Error log :", ex);
		}
		return null;
	}	   
	
	   
	   
	   
}
