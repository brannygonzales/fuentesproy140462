package pe.com.claro.oss.validar.ajuste.masivo.bean;

import java.math.BigDecimal;

public class ResponseUsuarioAppBase {

	private BigDecimal codRespuesta;
	private String msjRespuesta;
	
	public BigDecimal getCodRespuesta() {
		return codRespuesta;
	}
	public void setCodRespuesta(BigDecimal codRespuesta) {
		this.codRespuesta = codRespuesta;
	}
	public String getMsjRespuesta() {
		return msjRespuesta;
	}
	public void setMsjRespuesta(String msjRespuesta) {
		this.msjRespuesta = msjRespuesta;
	}

	

}
