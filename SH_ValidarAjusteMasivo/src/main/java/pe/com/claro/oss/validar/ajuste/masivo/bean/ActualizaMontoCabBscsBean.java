package pe.com.claro.oss.validar.ajuste.masivo.bean;

public class ActualizaMontoCabBscsBean {
	
	private Integer id;
	private Double montoSinIgv;
	private Double montoInafecto;
	private Double montoConIgv;
	private String codigoRespuesta;
	private String mensajeRespuesta;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Double getMontoSinIgv() {
		return montoSinIgv;
	}
	public void setMontoSinIgv(Double montoSinIgv) {
		this.montoSinIgv = montoSinIgv;
	}
	public Double getMontoInafecto() {
		return montoInafecto;
	}
	public void setMontoInafecto(Double montoInafecto) {
		this.montoInafecto = montoInafecto;
	}
	public Double getMontoConIgv() {
		return montoConIgv;
	}
	public void setMontoConIgv(Double montoConIgv) {
		this.montoConIgv = montoConIgv;
	}
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
	public String getMensajeRespuesta() {
		return mensajeRespuesta;
	}
	public void setMensajeRespuesta(String mensajeRespuesta) {
		this.mensajeRespuesta = mensajeRespuesta;
	}
	
	
	

}
