package pe.com.claro.oss.validar.ajuste.masivo.bean;

public class ResponseBase {

	private String codigoRespuesta;
	private String mensajeRespuesta;
	private String rowid;

	public String getCodigoRespuesta(){
		return codigoRespuesta;
	}

	public void setCodigoRespuesta( String codigoRespuesta ){
		this.codigoRespuesta = codigoRespuesta;
	}

	public String getMensajeRespuesta(){
		return mensajeRespuesta;
	}

	public void setMensajeRespuesta( String mensajeRespuesta ){
		this.mensajeRespuesta = mensajeRespuesta;
	}

	public String getRowid() {
		return rowid;
	}

	public void setRowid(String rowid) {
		this.rowid = rowid;
	}

}
