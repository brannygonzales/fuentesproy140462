package pe.com.claro.oss.validar.ajuste.masivo.bean;

import java.util.List;

public class ListaDetalleTemporalBean {
	
	private Integer id;
	private Integer idProceso;
	private String docReferencia;
	private List<DetalleTemporalBean> lista;
	private String codigoRespuesta;
	private String mensajeRespuesta;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getIdProceso() {
		return idProceso;
	}
	public void setIdProceso(Integer idProceso) {
		this.idProceso = idProceso;
	}
	public String getDocReferencia() {
		return docReferencia;
	}
	public void setDocReferencia(String docReferencia) {
		this.docReferencia = docReferencia;
	}
	public List<DetalleTemporalBean> getLista() {
		return lista;
	}
	public void setLista(List<DetalleTemporalBean> lista) {
		this.lista = lista;
	}
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
	public String getMensajeRespuesta() {
		return mensajeRespuesta;
	}
	public void setMensajeRespuesta(String mensajeRespuesta) {
		this.mensajeRespuesta = mensajeRespuesta;
	}
	
	

}
