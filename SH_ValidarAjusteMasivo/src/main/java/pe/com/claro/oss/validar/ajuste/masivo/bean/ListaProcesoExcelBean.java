package pe.com.claro.oss.validar.ajuste.masivo.bean;

import java.util.List;

public class ListaProcesoExcelBean {

	private List<ProcesoExcelBean> listaProcesos;
	private String codigoRespuesta;
	private String mensajeRespuesta;
	
	public List<ProcesoExcelBean> getListaProcesos() {
		return listaProcesos;
	}
	public void setListaProcesos(List<ProcesoExcelBean> listaProcesos) {
		this.listaProcesos = listaProcesos;
	}
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
	public String getMensajeRespuesta() {
		return mensajeRespuesta;
	}
	public void setMensajeRespuesta(String mensajeRespuesta) {
		this.mensajeRespuesta = mensajeRespuesta;
	}
	
	
}
