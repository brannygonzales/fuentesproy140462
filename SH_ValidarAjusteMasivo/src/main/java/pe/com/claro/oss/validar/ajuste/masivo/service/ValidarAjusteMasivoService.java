package pe.com.claro.oss.validar.ajuste.masivo.service;


public interface ValidarAjusteMasivoService {
	void run(String idTransaction);
}
