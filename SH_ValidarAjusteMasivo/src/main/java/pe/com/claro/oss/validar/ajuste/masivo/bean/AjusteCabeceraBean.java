package pe.com.claro.oss.validar.ajuste.masivo.bean;

public class AjusteCabeceraBean {
	
	private String codigoAjuste;
	private String tipoAjuste;
	private String tipoDocAjuste;
	private String docReferencia;
	private String snCode;
	private String evento;
	private Integer id;
	private Integer idProceso;
	
	public String getCodigoAjuste() {
		return codigoAjuste;
	}
	public void setCodigoAjuste(String codigoAjuste) {
		this.codigoAjuste = codigoAjuste;
	}
	public String getTipoAjuste() {
		return tipoAjuste;
	}
	public void setTipoAjuste(String tipoAjuste) {
		this.tipoAjuste = tipoAjuste;
	}
	public String getTipoDocAjuste() {
		return tipoDocAjuste;
	}
	public void setTipoDocAjuste(String tipoDocAjuste) {
		this.tipoDocAjuste = tipoDocAjuste;
	}
	public String getDocReferencia() {
		return docReferencia;
	}
	public void setDocReferencia(String docReferencia) {
		this.docReferencia = docReferencia;
	}
	public String getSnCode() {
		return snCode;
	}
	public void setSnCode(String snCode) {
		this.snCode = snCode;
	}
	public String getEvento() {
		return evento;
	}
	public void setEvento(String evento) {
		this.evento = evento;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getIdProceso() {
		return idProceso;
	}
	public void setIdProceso(Integer idProceso) {
		this.idProceso = idProceso;
	}
	
	

}
