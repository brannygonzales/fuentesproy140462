package pe.com.claro.oss.validar.ajuste.masivo.bean;

public class ActualizaEstadoBean {

		private Integer id;
		private Integer idProceso;
		private String estado;
		private String codigoRespuesta;
		private String mensajeRespuesta;
		
		public String getEstado() {
			return estado;
		}
		public void setEstado(String estado) {
			this.estado = estado;
		}
		public String getCodigoRespuesta() {
			return codigoRespuesta;
		}
		public void setCodigoRespuesta(String codigoRespuesta) {
			this.codigoRespuesta = codigoRespuesta;
		}
		public String getMensajeRespuesta() {
			return mensajeRespuesta;
		}
		public void setMensajeRespuesta(String mensajeRespuesta) {
			this.mensajeRespuesta = mensajeRespuesta;
		}
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public Integer getIdProceso() {
			return idProceso;
		}
		public void setIdProceso(Integer idProceso) {
			this.idProceso = idProceso;
		}

		
		
}
