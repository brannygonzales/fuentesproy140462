package pe.com.claro.oss.validar.ajuste.masivo.util;

import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemOptions;
import org.apache.commons.vfs2.provider.sftp.SftpClientFactory;
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pe.com.claro.oss.validar.ajuste.masivo.exception.SFTPException;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;

@Component
public class UtilSFTP {

	@Autowired
	private PropertiesExternos propertiesExternos;

	private final Logger logger = Logger.getLogger(this.getClass().getName());
	public static String SUCCESS = "1";
	public static String FILE_NOT_FOUND = "2";
	public static String ERROR = "3";

	private ChannelSftp ojbCanalSFTP = null;
	private Session objSesion = null;

	/**
	 * conectarSFTPDMZ1
	 * 
	 * @param trazabilidadParam
	 * @param vServidorSFTP
	 * @param vUsuarioSFTP
	 * @param vPasswordSFTP
	 * @param vPuertoSFTP
	 * @param vTimeOutSFTP
	 * @return boolean
	 */

	public boolean conectarSftpFileServer(String trazabilidadParam,
			String vServidorSFTP, String vUsuarioSFTP, String vPasswordSFTP,
			String vPuertoSFTP, String vTimeOutSFTP) {

		String trazabilidad = trazabilidadParam + "[conectarSFTP] ";
		this.logger.info(trazabilidad + "[INICIO] - METODO: [conectarSFTP] ");

		FileSystemOptions objFileSystemOptions = null;
		Channel objCanal = null;
		boolean estadoConexion = false;

		try {

			logger.info(trazabilidad + "- Conectandose al servidor "
					+ vServidorSFTP);
			if (this.ojbCanalSFTP != null) {
				this.logger
						.info(trazabilidad
								+ "La conexion 'SFTP' ya esta en uso, se procede a desconectar...");
				desconectarSFTP(trazabilidad);
			}

			objFileSystemOptions = new FileSystemOptions();

			SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(
					objFileSystemOptions, Constantes.NO_MINUSCULA);

			this.objSesion = SftpClientFactory.createConnection(vServidorSFTP,
					Integer.parseInt(vPuertoSFTP), vUsuarioSFTP.toCharArray(),
					vPasswordSFTP.toCharArray(), objFileSystemOptions);
			objCanal = this.objSesion
					.openChannel(propertiesExternos.pTIPO_CANAL_SESION);

			objCanal.connect(Integer.parseInt(vTimeOutSFTP));

			this.ojbCanalSFTP = (ChannelSftp) objCanal;
			estadoConexion = this.ojbCanalSFTP.isConnected();
			this.logger.info(trazabilidad + " Conexion creada SFTP ");
		} catch (FileSystemException e) {
			this.logger
					.error(trazabilidad + "ERROR [FileSystemException]: ", e);
			estadoConexion = false;
		} catch (Exception e) {
			this.logger.error(trazabilidad + "ERROR [Exception]: ", e);
			estadoConexion = false;
		} finally {
			this.logger.info(trazabilidad + "[FIN] - METODO: [conectarSFTP] ");
		}

		return estadoConexion;
	}

	/**
	 * desconectarSFTP
	 * 
	 * @param trazabilidadParam
	 * @return boolean
	 */
	public boolean desconectarSFTP(String trazabilidadParam) {

		String trazabilidad = trazabilidadParam + "[desconectarSFTP]";
		this.logger
				.info(trazabilidad + "[INICIO] - METODO: [desconectarSFTP] ");

		boolean estadoConexion = false;

		try {
			if (this.ojbCanalSFTP != null) {
				this.ojbCanalSFTP.exit();
			}

			if (this.objSesion != null) {
				this.objSesion.disconnect();
			}

			this.ojbCanalSFTP = null;
			estadoConexion = true;
		} catch (Exception e) {
			this.logger.error(trazabilidad + "ERROR [Exception]: ", e);
		} finally {
			this.logger.info(trazabilidad
					+ "[FIN] - METODO: [desconectarSFTP] ");
		}

		return estadoConexion;
	}

	public boolean enviarArchivoSftpFileServer(String trazabilidadParam,
			String rutaLocal, String rutaRemota, String nombreArchivo
			) throws SFTPException {
		String trazabilidad = trazabilidadParam
				+ "[enviarArchivoSftpFileServer]";
		boolean estadoSubida = true;
		this.logger.info(trazabilidad
				+ "[INICIO] - METODO: [cargarArchivoSFTP] ");
		String archivoRemoto = null;
		String directorioRemoto = null;
		try {
			
			directorioRemoto = rutaRemota + Constantes.SLASH;
			this.logger.info(trazabilidad + "Ruta Origen: " + rutaLocal);
			this.logger.info(trazabilidad + "Ruta Destino: " + directorioRemoto
					+ nombreArchivo);
			this.logger.info(trazabilidad + "Cargando archivo...");

			this.ojbCanalSFTP.put(rutaLocal + archivoRemoto, rutaRemota
					+ Constantes.SLASH);
			this.ojbCanalSFTP.lstat(directorioRemoto + nombreArchivo);
			estadoSubida = true;

		} catch (SftpException e) {
			this.logger.error(trazabilidad + "ERROR [SftpException]: ", e);
			throw new SFTPException(directorioRemoto, nombreArchivo);

		} catch (Exception e) {
			logger.error(trazabilidad + "ERROR enviando el archivo al FTP: ", e);
			throw new SFTPException(directorioRemoto, nombreArchivo);

		} finally {
			if (this.ojbCanalSFTP != null) {
				this.ojbCanalSFTP.exit();
			}

			if (this.objSesion != null) {
				this.objSesion.disconnect();
			}
		}

		return estadoSubida;

	}



	/**
	 * obtenerArchivoSFTP
	 * 
	 * @param vRutaRemotaArchivo
	 * @return ArchivoAsurionBean
	 */
//	@SuppressWarnings("unchecked")
	public String obtenerArchivoSFTP(String mensajeTransaccion,
			String vRutaRemotaArchivo, String vRutaDestino,
			String vArchivoRemoto,  String vServidor)
			throws SFTPException {

		long tiempoInicio = System.currentTimeMillis();

		String obtenerArchivo = null;
		
//		Vector<ChannelSftp.LsEntry> list;
		SftpATTRS existeArchivo =null;
		
		String trazabilidad = mensajeTransaccion + "[obtenerArchivoSFTP]";
		this.logger.info(trazabilidad
				+ "[INICIO] - METODO: [obtenerArchivoSFTP ] ");

		try {
			
			this.logger.info(trazabilidad + "ruta del origen :  "
					+ vRutaRemotaArchivo);
			
			this.logger.info(trazabilidad + " Directorio Remoto : "
					+  vRutaDestino);

			try {
				existeArchivo=this.ojbCanalSFTP.lstat(vRutaRemotaArchivo);
				
				if (existeArchivo !=null) {

					this.logger.info(trazabilidad + "Obteniendo archivo...");
//					this.ojbCanalSFTP.cd(vRutaRemotaArchivo);
////					list=this.ojbCanalSFTP.ls(vArchivoRemoto+"*");
//					list=this.ojbCanalSFTP.ls(vArchivoRemoto);
					
//					if (list.isEmpty()) {
//						
//						logger.info(trazabilidad+  vArchivoRemoto
//								+ " No se ha encontrado archivo en directorio remoto :  "
//								+ vRutaRemotaArchivo);
//					} else{

						this.ojbCanalSFTP.get(vRutaRemotaArchivo+vArchivoRemoto,
								vRutaDestino + vArchivoRemoto);
						
						this.logger.info(trazabilidad + "archivo : "
								+ vArchivoRemoto + " guardado correctamente en :  "
								+ vRutaDestino);
						
						obtenerArchivo=vRutaDestino+vArchivoRemoto;
//					}
					
				} else {
					this.logger.info(trazabilidad + "Carpeta no existe");
					obtenerArchivo=vRutaDestino+"No existe";
					
				}
				
			} catch (Exception e) {
				throw new SFTPException(vServidor	, vRutaRemotaArchivo, vArchivoRemoto, e);
				
			}

		} catch (Exception e) {
			throw new SFTPException(vServidor	, vRutaRemotaArchivo, vArchivoRemoto, e);
			
		}

		finally {
			this.logger.info(trazabilidad + "Tiempo Transcurrido (ms): ["
					+ (System.currentTimeMillis() - tiempoInicio) + "]");
			this.logger.info(trazabilidad
					+ "[FIN] - METODO: [ObtenerArchivoSFTP - SFTP]");
		}

		return obtenerArchivo;

	}

}
