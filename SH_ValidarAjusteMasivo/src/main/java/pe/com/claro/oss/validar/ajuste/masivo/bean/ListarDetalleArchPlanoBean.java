package pe.com.claro.oss.validar.ajuste.masivo.bean;

import java.util.List;

public class ListarDetalleArchPlanoBean {
	
	private String idProceso;
	private String docReferencia;
	private String tipoAjuste;
	private String tmCode;
	private String snCode;
	private String cuentaContable;
	private String customerId;
	private String codigoRespuesta;
	private String mensajeRespuesta;
	private List<DetalleArchPlanoBean> listaDetalle;
	
	public String getIdProceso() {
		return idProceso;
	}
	public void setIdProceso(String idProceso) {
		this.idProceso = idProceso;
	}
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
	public String getMensajeRespuesta() {
		return mensajeRespuesta;
	}
	public void setMensajeRespuesta(String mensajeRespuesta) {
		this.mensajeRespuesta = mensajeRespuesta;
	}
	public List<DetalleArchPlanoBean> getListaDetalle() {
		return listaDetalle;
	}
	public void setListaDetalle(List<DetalleArchPlanoBean> listaDetalle) {
		this.listaDetalle = listaDetalle;
	}
	public String getDocReferencia() {
		return docReferencia;
	}
	public void setDocReferencia(String docReferencia) {
		this.docReferencia = docReferencia;
	}
	public String getTipoAjuste() {
		return tipoAjuste;
	}
	public void setTipoAjuste(String tipoAjuste) {
		this.tipoAjuste = tipoAjuste;
	}
	public String getTmCode() {
		return tmCode;
	}
	public void setTmCode(String tmCode) {
		this.tmCode = tmCode;
	}
	public String getSnCode() {
		return snCode;
	}
	public void setSnCode(String snCode) {
		this.snCode = snCode;
	}
	public String getCuentaContable() {
		return cuentaContable;
	}
	public void setCuentaContable(String cuentaContable) {
		this.cuentaContable = cuentaContable;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	

}
