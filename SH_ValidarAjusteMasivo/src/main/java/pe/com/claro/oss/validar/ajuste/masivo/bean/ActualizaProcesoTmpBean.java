package pe.com.claro.oss.validar.ajuste.masivo.bean;

import java.util.List;

public class ActualizaProcesoTmpBean {
	
	private Integer proceso;
	private List<ProcesoTmpBean> listaProcesoTmpBean;
	private String codigoRespuesta;
	private String mensajeRespuesta;
	
	public Integer getProceso() {
		return proceso;
	}
	public void setProceso(Integer proceso) {
		this.proceso = proceso;
	}
	public List<ProcesoTmpBean> getListaProcesoTmpBean() {
		return listaProcesoTmpBean;
	}
	public void setListaProcesoTmpBean(List<ProcesoTmpBean> listaProcesoTmpBean) {
		this.listaProcesoTmpBean = listaProcesoTmpBean;
	}
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
	public String getMensajeRespuesta() {
		return mensajeRespuesta;
	}
	public void setMensajeRespuesta(String mensajeRespuesta) {
		this.mensajeRespuesta = mensajeRespuesta;
	}
	
	
	
	

}
