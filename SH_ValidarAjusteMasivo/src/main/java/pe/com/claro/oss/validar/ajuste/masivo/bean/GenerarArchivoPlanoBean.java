package pe.com.claro.oss.validar.ajuste.masivo.bean;

public class GenerarArchivoPlanoBean {
	
	private String codigoProceso;
	private String archivoError;
	private String archivoTotal;
	private String archivoParcial;	
	private String codigoRespuesta;
	private String mensajeRespuesta;
	
	public String getCodigoProceso() {
		return codigoProceso;
	}
	public void setCodigoProceso(String codigoProceso) {
		this.codigoProceso = codigoProceso;
	}
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
	public String getMensajeRespuesta() {
		return mensajeRespuesta;
	}
	public void setMensajeRespuesta(String mensajeRespuesta) {
		this.mensajeRespuesta = mensajeRespuesta;
	}
	public String getArchivoError() {
		return archivoError;
	}
	public void setArchivoError(String archivoError) {
		this.archivoError = archivoError;
	}
	public String getArchivoTotal() {
		return archivoTotal;
	}
	public void setArchivoTotal(String archivoTotal) {
		this.archivoTotal = archivoTotal;
	}
	public String getArchivoParcial() {
		return archivoParcial;
	}
	public void setArchivoParcial(String archivoParcial) {
		this.archivoParcial = archivoParcial;
	}
	
	
}
