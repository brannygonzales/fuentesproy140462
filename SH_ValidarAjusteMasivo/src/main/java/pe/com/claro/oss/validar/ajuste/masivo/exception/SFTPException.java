package pe.com.claro.oss.validar.ajuste.masivo.exception;

public class SFTPException extends BaseException {

	private static final long serialVersionUID = 1L;
	private String servidorFTP;

	public SFTPException(String servidorFTP, String rutaServidor,
			String nombreArchivo, Exception objException) {
		super(servidorFTP, rutaServidor, nombreArchivo, objException);

		this.servidorFTP = servidorFTP;
	}

	public SFTPException(String code, String message) {
		super(code, message);
	}

	public String getServidorFTP() {
		return servidorFTP;
	}

	public void setServidorFTP(String servidorFTP) {
		this.servidorFTP = servidorFTP;
	}

}