package pe.com.claro.oss.validar.ajuste.masivo.bean;

public class RubroBean {
	
	private String nombreRubro;
	private String conceptoRubro;
	
	public String getNombreRubro() {
		return nombreRubro;
	}
	public void setNombreRubro(String nombreRubro) {
		this.nombreRubro = nombreRubro;
	}
	public String getConceptoRubro() {
		return conceptoRubro;
	}
	public void setConceptoRubro(String conceptoRubro) {
		this.conceptoRubro = conceptoRubro;
	}
	
	

}
