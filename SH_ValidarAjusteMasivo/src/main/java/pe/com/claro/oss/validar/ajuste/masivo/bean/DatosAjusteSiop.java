package pe.com.claro.oss.validar.ajuste.masivo.bean;

public class DatosAjusteSiop {
	
	private String idProceso;
	private String customerId;
	private String custCode;
	private String ruc;
	private String razonSocial;
	private String contacto;
	private String tipoCliente;
	private String direccion;
	private String nota;
	private String departamento;
	private String provincia;
	private String distrito;
	private String telefono;
	private String afiliacion;
	private String email;
	private String factura;
	private String montoFactura;
	private String igvFactura;
	private String igvVigente;
	private String emision;
	private String ampliacion;
	private String billCycle;
	private String inicioCiclo;
	private String finCiclo;
	private String plan;
	private String desPlan;
	private String serv;
	private String desServ;
	private String ctaPer;
	private String ctaMex;
	private String montoSinIgv;
	private String afectoIgv;
	private String centroBeneficio;
	private String evento;
	private String tipoAjuste;
	private String montoConIgv;
	private String fechaEmision;
	private String fechaVencimiento;
	private String ctaIgv;
	private double porcentaje;
	private String id;
	private String tmcode;
	private String rowid;
	
	public String getRuc() {
		return ruc;
	}
	public void setRuc(String ruc) {
		this.ruc = ruc;
	}
	public String getIdProceso() {
		return idProceso;
	}
	public void setIdProceso(String idProceso) {
		this.idProceso = idProceso;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getCustCode() {
		return custCode;
	}
	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public String getContacto() {
		return contacto;
	}
	public void setContacto(String contacto) {
		this.contacto = contacto;
	}
	public String getTipoCliente() {
		return tipoCliente;
	}
	public void setTipoCliente(String tipoCliente) {
		this.tipoCliente = tipoCliente;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getNota() {
		return nota;
	}
	public void setNota(String nota) {
		this.nota = nota;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getAfiliacion() {
		return afiliacion;
	}
	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}
	public String getBillCycle() {
		return billCycle;
	}
	public void setBillCycle(String billCycle) {
		this.billCycle = billCycle;
	}
	public String getInicioCiclo() {
		return inicioCiclo;
	}
	public void setInicioCiclo(String inicioCiclo) {
		this.inicioCiclo = inicioCiclo;
	}
	public String getFinCiclo() {
		return finCiclo;
	}
	public void setFinCiclo(String finCiclo) {
		this.finCiclo = finCiclo;
	}
	public String getPlan() {
		return plan;
	}
	public void setPlan(String plan) {
		this.plan = plan;
	}
	public String getDesPlan() {
		return desPlan;
	}
	public void setDesPlan(String desPlan) {
		this.desPlan = desPlan;
	}
	public String getServ() {
		return serv;
	}
	public void setServ(String serv) {
		this.serv = serv;
	}
	public String getDesServ() {
		return desServ;
	}
	public void setDesServ(String desServ) {
		this.desServ = desServ;
	}
	public String getCtaPer() {
		return ctaPer;
	}
	public void setCtaPer(String ctaPer) {
		this.ctaPer = ctaPer;
	}
	public String getCtaMex() {
		return ctaMex;
	}
	public void setCtaMex(String ctaMex) {
		this.ctaMex = ctaMex;
	}
	public String getMontoSinIgv() {
		return montoSinIgv;
	}
	public void setMontoSinIgv(String montoSinIgv) {
		this.montoSinIgv = montoSinIgv;
	}
	public String getAfectoIgv() {
		return afectoIgv;
	}
	public void setAfectoIgv(String afectoIgv) {
		this.afectoIgv = afectoIgv;
	}
	public String getCentroBeneficio() {
		return centroBeneficio;
	}
	public void setCentroBeneficio(String centroBeneficio) {
		this.centroBeneficio = centroBeneficio;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFactura() {
		return factura;
	}
	public void setFactura(String factura) {
		this.factura = factura;
	}
	public String getMontoFactura() {
		return montoFactura;
	}
	public void setMontoFactura(String montoFactura) {
		this.montoFactura = montoFactura;
	}
	public String getIgvFactura() {
		return igvFactura;
	}
	public void setIgvFactura(String igvFactura) {
		this.igvFactura = igvFactura;
	}
	public String getIgvVigente() {
		return igvVigente;
	}
	public void setIgvVigente(String igvVigente) {
		this.igvVigente = igvVigente;
	}
	public String getEmision() {
		return emision;
	}
	public void setEmision(String emision) {
		this.emision = emision;
	}
	public String getAmpliacion() {
		return ampliacion;
	}
	public void setAmpliacion(String ampliacion) {
		this.ampliacion = ampliacion;
	}
	public String getEvento() {
		return evento;
	}
	public void setEvento(String evento) {
		this.evento = evento;
	}
	public String getTipoAjuste() {
		return tipoAjuste;
	}
	public void setTipoAjuste(String tipoAjuste) {
		this.tipoAjuste = tipoAjuste;
	}
	public String getMontoConIgv() {
		return montoConIgv;
	}
	public void setMontoConIgv(String montoConIgv) {
		this.montoConIgv = montoConIgv;
	}
	public String getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	public String getCtaIgv() {
		return ctaIgv;
	}
	public void setCtaIgv(String ctaIgv) {
		this.ctaIgv = ctaIgv;
	}
	public double getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(double porcentaje) {
		this.porcentaje = porcentaje;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTmcode() {
		return tmcode;
	}
	public void setTmcode(String tmcode) {
		this.tmcode = tmcode;
	}
	public String getRowid() {
		return rowid;
	}
	public void setRowid(String rowid) {
		this.rowid = rowid;
	}
	
}
