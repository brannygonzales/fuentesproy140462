package pe.com.claro.oss.validar.ajuste.masivo.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import pe.com.claro.oss.validar.ajuste.masivo.bean.DatosAjusteSiop;
import pe.com.claro.oss.validar.ajuste.masivo.bean.DetalleArchPlanoBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.DetalleTemporalBean;
import pe.com.claro.oss.validar.ajuste.masivo.bean.ResponseUsuarioAppBase;
import pe.com.claro.oss.validar.ajuste.masivo.exception.SFTPException;

import com.jcraft.jsch.JSchException;

@Component
public class Util {


	
	private final Logger logger = Logger.getLogger(this.getClass().getName());
	
	public Util() {
		super();
	}

	public static String getStackTraceFromException(Exception exception) {
		StringWriter stringWriter = new StringWriter();
		exception.printStackTrace(new PrintWriter(stringWriter, true));
		return stringWriter.toString();
	}
	
	
	public ResponseUsuarioAppBase ejecutarSqlldr(String mensajeTransaccion, String rutaSqlldr,
			String rutaBAD, String rutaLOG, String rutaDATA, String rutaCTL,
			String rutaDiscard,	 String usuario, String contrasenia, String BD,
			int bindsize, int readsize,int rows) throws SFTPException, JSchException {

		long tiempoInicial = System.currentTimeMillis();
		Long tiempoFinal = 0L;
		
		
		String metodo = "ejecutarSqlldr";
		String cadMensaje = mensajeTransaccion + "[" + metodo + "] ";

		logger.info(cadMensaje + "- Inicio del metodo ejecutarSqlldr");
		
		ResponseUsuarioAppBase response = new ResponseUsuarioAppBase();
		
		BigDecimal codigoRespuestaExito=new BigDecimal(Constantes.CODIGO_EXITO_0);
		BigDecimal codigoRespuestaError=new BigDecimal(Constantes.CODIGO_ERROR_1);
		
		try {
			
			File rutaFile=new File(rutaDATA);
			
			if (rutaFile.exists()) {
				
				logger.info(mensajeTransaccion + "- Ejecutando Sqlldr  en " + BD);
				
				
				StringBuilder comandoSQL=new StringBuilder();
				
				comandoSQL.append(rutaSqlldr).append(Constantes.ESPACIO_BLANCO)
					.append(Constantes.CTL_USER_ID)
					.append(usuario)
					.append(Constantes.SLASH)
					.append(contrasenia)
					.append(Constantes.ARROBA)
					.append(Constantes.SLASH)
					.append(Constantes.SLASH)
					.append(BD)
					.append(Constantes.ESPACIO_BLANCO)
					.append(Constantes.SIGNO_REPLACE_COMA)
					.append(Constantes.CTL_CONTROL)
					.append(rutaCTL)
					.append(Constantes.ESPACIO_BLANCO)
					.append(Constantes.SIGNO_REPLACE_COMA)
					.append(Constantes.CTL_DATA)
					.append(rutaDATA)
					.append(Constantes.ESPACIO_BLANCO)
					.append(Constantes.SIGNO_REPLACE_COMA)
					.append(Constantes.CTL_LOG)
					.append(rutaLOG)
					.append(Constantes.ESPACIO_BLANCO)
					.append(Constantes.SIGNO_REPLACE_COMA)
					.append(Constantes.CTL_BAD)
					.append(rutaBAD)
					.append(Constantes.ESPACIO_BLANCO)
					.append(Constantes.SIGNO_REPLACE_COMA)
					.append(Constantes.CTL_DISCARD)
					.append(rutaDiscard)
					.append(Constantes.ESPACIO_BLANCO)
					.append(Constantes.SIGNO_REPLACE_COMA)
					.append(Constantes.CTL_BINDSIZE)
					.append(bindsize)
					.append(Constantes.ESPACIO_BLANCO)
					.append(Constantes.SIGNO_REPLACE_COMA)
					.append(Constantes.CTL_READSIZE)
					.append(readsize)
					.append(Constantes.ESPACIO_BLANCO)
					.append(Constantes.SIGNO_REPLACE_COMA)
					.append(Constantes.CTL_ROWS)
					.append(rows);
				
					logger.info(mensajeTransaccion + "comando sqlldr : " + comandoSQL.toString());
					
					Process proceso= Runtime.getRuntime().exec(comandoSQL.toString());
					
					int respuestaProceso=proceso.waitFor();
					BufferedReader stdInput = new BufferedReader(new InputStreamReader(proceso.getInputStream()));
					
					BufferedReader stdError = new BufferedReader(new InputStreamReader(proceso.getErrorStream()));
					
					String cantidadLineas =null;
					while ((cantidadLineas = stdInput.readLine()) != null) {
						logger.info(cadMensaje + "Log de Ejecucion CTL :" + cantidadLineas);
					}

					while ((cantidadLineas = stdError.readLine()) != null) {
						logger.info(cadMensaje + " Log de Ejecucion CTL  :" + cantidadLineas);
					}
				
				
					if (respuestaProceso == 0  || respuestaProceso == 2) {
						logger.info(cadMensaje + "Carga ha sido completada");
						
						logger.info(cadMensaje +"Respuesta de carga : "  + respuestaProceso);

						
						response.setCodRespuesta(codigoRespuestaExito);
						
					} else{
						
						logger.info(cadMensaje +"Respuesta de carga : "  + respuestaProceso);
						response.setCodRespuesta(codigoRespuestaError);
						
					}
			} else {
				
				response.setCodRespuesta(codigoRespuestaError);
				logger.error(mensajeTransaccion + "archivo : " + rutaDATA + " no existe");
			}
			
		} catch (Exception e) {
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			logger.error("Error  Producido: " + sw.toString());
		} finally {
			tiempoFinal = System.currentTimeMillis();
			logger.info(cadMensaje + "Tiempo que demoro en la ejecucion[" + (tiempoFinal - tiempoInicial) + " ms]");
			logger.info(cadMensaje + "[FIN] - METODO: [" + metodo + " - Service] ");
		}
		
		return response;
	}
	
	
	public boolean desComprimirFichero(String mensajeTransaccion, String fileZIP , String rutaDescomprimida) {
		boolean exito = false;
		String metodo = "desComprimirFichero";
		String cadMensaje = mensajeTransaccion + "[" + metodo + "] ";
		logger.info(cadMensaje + "[INICIO] - METODO: [" + metodo + " - UTILITARIOS] ");

		Long ini = 0L;
		Long end = 0L;

		ini = System.currentTimeMillis();
		try {
			StringBuilder cmd = new StringBuilder();
			cmd.append(Constantes.COMANDO_DESCOMPRIMIR_ARCHIVO);
			cmd.append(Constantes.ESPACIO_BLANCO);
			cmd.append(fileZIP);
			cmd.append(Constantes.ESPACIO_BLANCO);
			cmd.append(Constantes.COMANDO_DESCOMPRIMIR_ARCHIVO_RUTA);
			cmd.append(Constantes.ESPACIO_BLANCO);
			cmd.append(rutaDescomprimida);
			
			Runtime.getRuntime().exec(cmd.toString());
			exito = true;
		} catch (Exception e ) {
			exito = false;
			String textoError = Util.getStackTraceFromException(e);
			logger.error(cadMensaje + Constantes.METODO_MENSAJE_EXCEPTION + textoError, e);

			String mensajeError = e.getMessage();

			logger.error(cadMensaje + Constantes.ESPACIO_BLANCO + mensajeError, e);

		} finally {
			end = System.currentTimeMillis();
			logger.info(cadMensaje + "Tiempo que demoro en la ejecucion[" + (end - ini) + " ms]");
			logger.info(cadMensaje + "[FIN] - METODO: [" + metodo + " - UTILITARIOS] ");
		}

		return exito;
	}
	
	
	
	public  boolean eliminarContenidoFolder(String mensajeTransaccion, File folder) {

		boolean exito = false;
		String metodo = "eliminarContenidoFolder";
		String cadMensaje = mensajeTransaccion + "[" + metodo + "] ";
		logger.info(cadMensaje + "[INICIO] - METODO: [" + metodo + " - UTILITARIOS] ");

		Long ini = 0L;
		Long end = 0L;

		ini = System.currentTimeMillis();

		try {
			String[] entries = folder.list();

			if (!folder.exists()) {
				exito = false;
				logger.info(cadMensaje + "La ruta no existe.");
			} else {
				for (String s : entries) {
					File currentFile = new File(folder.getPath(), s);
					
					currentFile.delete();
					logger.info(cadMensaje + "El archivo:" + currentFile.getName() + " data fue eliminado.");
				}
				exito = true;

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			end = System.currentTimeMillis();
			logger.info(cadMensaje + "Tiempo que demoro en la ejecucion[" + (end - ini) + " ms]");
			logger.info(cadMensaje + "[FIN] - METODO: [" + metodo + " - UTILITARIOS] ");
		}

		return exito;

	}
	
	public static DetalleArchPlanoBean convertir(DatosAjusteSiop bean) {
		DetalleArchPlanoBean beanSalida = new DetalleArchPlanoBean();
		beanSalida.setAfectoigv(bean.getAfectoIgv());
		beanSalida.setAfiliacion(bean.getAfiliacion());
		beanSalida.setAmpliacion(bean.getAmpliacion());
		beanSalida.setBillcycle(bean.getBillCycle());
		beanSalida.setCebe(bean.getCentroBeneficio());
		beanSalida.setContacto(bean.getContacto());
		beanSalida.setCtamex(bean.getCtaMex());
		beanSalida.setCtaper(bean.getCtaPer());
		beanSalida.setCustcode(bean.getCustCode());
		beanSalida.setCustomerid(bean.getCustomerId());
		beanSalida.setDepartamento(bean.getDepartamento());
//		beanSalida.setDesplan(bean.getDesPlan());
		beanSalida.setDesplan(bean.getPlan());
		beanSalida.setDesserv(bean.getDesServ());
		beanSalida.setDireccion(bean.getDireccion());
		beanSalida.setDistrito(bean.getDistrito());
		beanSalida.setEmail(bean.getEmail());
		beanSalida.setEmision(bean.getEmision());
		beanSalida.setFactura(bean.getFactura());
		beanSalida.setFinciclo(bean.getFinCiclo());
		beanSalida.setIdproceso(bean.getIdProceso());
		beanSalida.setIgvvigente(bean.getIgvVigente());
		beanSalida.setIniciociclo(bean.getInicioCiclo());
		beanSalida.setFinciclo(bean.getFinCiclo());
		beanSalida.setIdproceso(bean.getIdProceso());
		beanSalida.setIgv(bean.getIgvFactura());
		beanSalida.setIgvvigente(bean.getIgvVigente());
		beanSalida.setMontoConIgv(bean.getMontoConIgv());
		beanSalida.setMontofactura(bean.getMontoFactura());
		beanSalida.setNotadir(bean.getNota());
//		beanSalida.setPlan(bean.getPlan());
		beanSalida.setPlan(bean.getTmcode());
		beanSalida.setProvincia(bean.getProvincia());
		beanSalida.setRazonsocial(bean.getRazonSocial());
		beanSalida.setRuc(bean.getRuc());
		beanSalida.setServ(bean.getServ());
		beanSalida.setTelefono(bean.getTelefono());
		beanSalida.setTipocliente(bean.getTipoCliente());
		beanSalida.setFechaEmision(bean.getFechaEmision());
		beanSalida.setFechaVencimiento(bean.getFechaVencimiento());
		beanSalida.setCtaigv(bean.getCtaIgv());
		beanSalida.setPorcentaje(bean.getPorcentaje());
		return beanSalida;
	}
	
	public static DatosAjusteSiop convertir(DetalleArchPlanoBean bean) {
		DatosAjusteSiop beanSalida = new DatosAjusteSiop();
		beanSalida.setAfectoIgv(bean.getAfectoigv());
		beanSalida.setAfiliacion(bean.getAfiliacion());
		beanSalida.setAmpliacion(bean.getAmpliacion());
		beanSalida.setBillCycle(bean.getBillcycle());
		beanSalida.setCentroBeneficio(bean.getCebe());
		beanSalida.setContacto(bean.getContacto());
		beanSalida.setCtaMex(bean.getCtamex());
		beanSalida.setCtaPer(bean.getCtaper());
		beanSalida.setCustCode(bean.getCustcode());
		beanSalida.setCustomerId(bean.getCustomerid());
		beanSalida.setDepartamento(bean.getDepartamento());
		beanSalida.setDesPlan(bean.getDesplan());
		beanSalida.setDesServ(bean.getDesserv());
		beanSalida.setDireccion(bean.getDireccion());
		beanSalida.setDistrito(bean.getDistrito());
		beanSalida.setEmail(bean.getEmail());
		beanSalida.setEmision(bean.getEmision());
		beanSalida.setFactura(bean.getFactura());
		beanSalida.setFinCiclo(bean.getFinciclo());
		beanSalida.setIdProceso(bean.getIdproceso());
		beanSalida.setIgvVigente(bean.getIgvvigente());
		beanSalida.setInicioCiclo(bean.getIniciociclo());
		beanSalida.setIgvFactura(bean.getIgv());
		beanSalida.setMontoConIgv(bean.getMontoConIgv());
		beanSalida.setMontoFactura(bean.getMontofactura());
		beanSalida.setNota(bean.getNotadir());
		beanSalida.setPlan(bean.getPlan());
		beanSalida.setProvincia(bean.getProvincia());
		beanSalida.setRazonSocial(bean.getRazonsocial());
		beanSalida.setRuc(bean.getRuc());
		beanSalida.setServ(bean.getServ());
		beanSalida.setTelefono(bean.getTelefono());
		beanSalida.setTipoCliente(bean.getTipocliente());
		beanSalida.setFechaEmision(bean.getFechaEmision());
		beanSalida.setFechaVencimiento(bean.getFechaVencimiento());
		beanSalida.setCtaIgv(bean.getCtaigv());
		beanSalida.setPorcentaje(bean.getPorcentaje());
		return beanSalida;
	}
	
	public static DatosAjusteSiop convertir(DetalleTemporalBean bean) {
		DatosAjusteSiop beanSalida = new DatosAjusteSiop();
		beanSalida.setAfectoIgv(bean.getAfecto());
		beanSalida.setAfiliacion(bean.getAfiliacion());
		beanSalida.setAmpliacion(bean.getAmpliacion());
		beanSalida.setBillCycle(bean.getBillcycle());
		beanSalida.setCentroBeneficio(bean.getCentrobenef());
		beanSalida.setContacto(bean.getContacto());
		beanSalida.setCtaMex(bean.getCuentacontablesap());
		beanSalida.setCtaPer(bean.getCuentacontable());
		beanSalida.setCustCode(bean.getCustcode());
		beanSalida.setCustomerId(bean.getCustomerid());
		beanSalida.setDepartamento(bean.getDepartamento());
//		beanSalida.setDesPlan(bean.getPlan());
//		beanSalida.setDesServ(bean.getDesserv());
		beanSalida.setDireccion(bean.getDireccion());
		beanSalida.setDistrito(bean.getDistrito());
		beanSalida.setEmail(bean.getEmail());
		beanSalida.setEmision(bean.getEmision());
		beanSalida.setFactura(bean.getFactura());
//		beanSalida.setFinCiclo(bean.getFinciclo());
		beanSalida.setIdProceso(String.valueOf(bean.getIdproceso()));
//		beanSalida.setIgvVigente(bean.getIgvvigente());
//		beanSalida.setInicioCiclo(bean.getIniciociclo());
//		beanSalida.setIgvFactura(bean.getIgv());
		beanSalida.setMontoConIgv(bean.getMontoconigv());
		beanSalida.setMontoFactura(bean.getMontofactura());
		beanSalida.setNota(bean.getNotadir());
		beanSalida.setPlan(bean.getPlan());
		beanSalida.setProvincia(bean.getProvincia());
		beanSalida.setRazonSocial(bean.getRazonsocial());
		beanSalida.setRuc(bean.getRuc());
		beanSalida.setServ(bean.getSncode());
		beanSalida.setTelefono(bean.getLinea());
		beanSalida.setTmcode(bean.getTmCode());
		beanSalida.setRowid(bean.getRowid());
//		beanSalida.setTipoCliente(bean.getTipocliente());
//		beanSalida.setFechaEmision(bean.getFechaEmision());
//		beanSalida.setFechaVencimiento(bean.getFechaVencimiento());
//		beanSalida.setCtaIgv(bean.getCtaigv());
//		beanSalida.setPorcentaje(bean.getPorcentaje());
		return beanSalida;
	}
	
	public static String formatearNumero(double numero) {
		DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
        simbolos.setDecimalSeparator('.');
		DecimalFormat formateador = new DecimalFormat("########0.00", simbolos);
		return formateador.format(numero);
	}
	
//	public static void main(String[] args) throws SFTPException, JSchException {
//		String msjTx = "[prueba main]";
//		String rutaBad = "D:/Users/branlap/Desktop/SAPIA/TRABAJO/PROY-140462/EXTRAS/JIRA/SDRYD-6555/SH_BSCS_ValidaAjuste/BAD/_20200708.bad";
//		String rutaLogCTL = "D:/Users/branlap/Desktop/SAPIA/TRABAJO/PROY-140462/EXTRAS/JIRA/SDRYD-6555/SH_BSCS_ValidaAjuste/LOG/AM_TOTAL_08052020222020_54705.txt.log";
//		String rutaArchivo = "D:/Users/branlap/Desktop/SAPIA/TRABAJO/PROY-140462/EXTRAS/JIRA/SDRYD-6555/SH_BSCS_ValidaAjuste/cargaBSCS/AM_TOTAL_08052020222020_54705.txt";
//		String rutaCTL = "D:/Users/branlap/Desktop/SAPIA/TRABAJO/PROY-140462/DOCUMENTACION/SH_BSCS_ValidaAjuste/FALLA2/DESA/AMBSCSSIOP.ctl";
//		String rutaDiscard = "D:/Users/branlap/Desktop/SAPIA/TRABAJO/PROY-140462/EXTRAS/JIRA/SDRYD-6555/SH_BSCS_ValidaAjuste/DISCARD/";
//		String usuario = "usrsiop";
//		String password = "usr2adm";
//		String cadenaSQLLDR = "172.19.74.105:1521/tora10g";
//		int bindSizeCTL = 20000000;
//		int readSizeCTL = 2000000000;
//		int rowsCTL = 2000000;
//		
//		Util util = new Util();
//		util.ejecutarSqlldr(msjTx,
//				Constantes.CTL_SQLLDR, rutaBad,
//				rutaLogCTL, rutaArchivo,
//				rutaCTL,
//				rutaDiscard,
//				usuario,
//				password,
//				cadenaSQLLDR,
//				bindSizeCTL,
//				readSizeCTL,
//				rowsCTL);
//	}
	
}