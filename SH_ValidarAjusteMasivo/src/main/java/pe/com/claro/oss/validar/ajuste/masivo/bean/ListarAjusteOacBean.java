package pe.com.claro.oss.validar.ajuste.masivo.bean;

import java.util.List;

public class ListarAjusteOacBean {
	private String documentoReferencia;
	private String customerId;
	private String codigoRespuesta;
	private String mensajeRespuesta;
	
	private List<AjusteOACBean> listaAjusteOacBean;
	
	public String getDocumentoReferencia() {
		return documentoReferencia;
	}
	public void setDocumentoReferencia(String documentoReferencia) {
		this.documentoReferencia = documentoReferencia;
	}
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
	public String getMensajeRespuesta() {
		return mensajeRespuesta;
	}
	public void setMensajeRespuesta(String mensajeRespuesta) {
		this.mensajeRespuesta = mensajeRespuesta;
	}
	public List<AjusteOACBean> getListaAjusteOacBean() {
		return listaAjusteOacBean;
	}
	public void setListaAjusteOacBean(List<AjusteOACBean> listaAjusteOacBean) {
		this.listaAjusteOacBean = listaAjusteOacBean;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	

}
