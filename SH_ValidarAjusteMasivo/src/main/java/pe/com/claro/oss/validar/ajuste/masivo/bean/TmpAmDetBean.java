package pe.com.claro.oss.validar.ajuste.masivo.bean;

import java.util.Date;

public class TmpAmDetBean {

	private Integer id;
	private Integer idProceso;
	private String cuentaContable;
	private String evento;
	private String cuentaContableSap;
	private String afecto;
	private Double montoSinIgv;
	private Double montoConIgv;
	private String nombreRubro;
	private String conceptoRubro;
	private String snCode;
	private String tmCode;
	private String linea;
	private String plan;
	private String centroBenef;
	private String estado;
	private Date fechaRegistro;
	
	public TmpAmDetBean() {
	}
	
	public TmpAmDetBean(Integer id, Integer idProceso, String cuentaContable,
			String evento, Double montoSinIgv, String snCode, String tmCode,
			String linea, String estado, Date fechaRegistro) {
		this.id = id;
		this.idProceso = idProceso;
		this.cuentaContable = cuentaContable;
		this.evento = evento;
		this.montoSinIgv = montoSinIgv;
		this.snCode = snCode;
		this.tmCode = tmCode;
		this.linea = linea;
		this.estado = estado;
		this.fechaRegistro = fechaRegistro;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getIdProceso() {
		return idProceso;
	}
	public void setIdProceso(Integer idProceso) {
		this.idProceso = idProceso;
	}
	public String getCuentaContable() {
		return cuentaContable;
	}
	public void setCuentaContable(String cuentaContable) {
		this.cuentaContable = cuentaContable;
	}
	public String getEvento() {
		return evento;
	}
	public void setEvento(String evento) {
		this.evento = evento;
	}
	public String getCuentaContableSap() {
		return cuentaContableSap;
	}
	public void setCuentaContableSap(String cuentaContableSap) {
		this.cuentaContableSap = cuentaContableSap;
	}
	public String getAfecto() {
		return afecto;
	}
	public void setAfecto(String afecto) {
		this.afecto = afecto;
	}
	public Double getMontoSinIgv() {
		return montoSinIgv;
	}
	public void setMontoSinIgv(Double montoSinIgv) {
		this.montoSinIgv = montoSinIgv;
	}
	public Double getMontoConIgv() {
		return montoConIgv;
	}
	public void setMontoConIgv(Double montoConIgv) {
		this.montoConIgv = montoConIgv;
	}
	public String getNombreRubro() {
		return nombreRubro;
	}
	public void setNombreRubro(String nombreRubro) {
		this.nombreRubro = nombreRubro;
	}
	public String getConceptoRubro() {
		return conceptoRubro;
	}
	public void setConceptoRubro(String conceptoRubro) {
		this.conceptoRubro = conceptoRubro;
	}
	public String getSnCode() {
		return snCode;
	}
	public void setSnCode(String snCode) {
		this.snCode = snCode;
	}
	public String getTmCode() {
		return tmCode;
	}
	public void setTmCode(String tmCode) {
		this.tmCode = tmCode;
	}
	public String getLinea() {
		return linea;
	}
	public void setLinea(String linea) {
		this.linea = linea;
	}
	public String getPlan() {
		return plan;
	}
	public void setPlan(String plan) {
		this.plan = plan;
	}
	public String getCentroBenef() {
		return centroBenef;
	}
	public void setCentroBenef(String centroBenef) {
		this.centroBenef = centroBenef;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	
}
