package pe.com.claro.oss.generar.ajuste.masivo.dao;

import java.util.List;

import pe.com.claro.oss.generar.ajuste.masivo.bean.AjusteBean;
import pe.com.claro.oss.generar.ajuste.masivo.bean.DetalleEjecucionBean;

public interface SiopDAO {
	
	public List<AjusteBean> obtenerAjustesCabecera(String mensajeTransaccion);
	public List<String> generarSpool(String mensajeTransaccion, int procesoId, String tipo);
	public boolean migrarDataHistorico(String mensajeTransaccion, int procesoId);
	public List<DetalleEjecucionBean> obtenerDetalleEjecucion(String mensajeTransaccion, int procesoId, String estadoSpool);

}