package pe.com.claro.oss.generar.ajuste.masivo;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import pe.com.claro.oss.generar.ajuste.masivo.service.GenerarAjusteMasivoService;
import pe.com.claro.oss.generar.ajuste.masivo.util.Constantes;
import pe.com.claro.oss.generar.ajuste.masivo.util.PropertiesExternos;


@Component
public class GenerarAjusteMasivoMain {
	
	private static Logger logger = Logger.getLogger(GenerarAjusteMasivoMain.class);
	private static ApplicationContext objContextoSpring;
	
	@Autowired
	private PropertiesExternos propertiesExterno;
	@Autowired
	private GenerarAjusteMasivoService generarAjusteMasivoService;
	
	
	public static void main(String[] args) {		
		long startTime = System.currentTimeMillis();
		String idTransaction = args[0];
		String mensajeTransaccion = "[idTx=" + idTransaction + "] ";
		
		try {
			objContextoSpring = new ClassPathXmlApplicationContext(Constantes.URL_CONTEXT);
			GenerarAjusteMasivoMain main = objContextoSpring.getBean(GenerarAjusteMasivoMain.class);
			
			main.configurarProperties();
			logger.info(mensajeTransaccion + "************************* INICIO *************************");
			main.iniciarProceso( mensajeTransaccion, idTransaction );
		} 
		catch (Exception e) {
			logger.error(mensajeTransaccion + "Error: ", e);
		} 
		finally {
			logger.info(mensajeTransaccion + "Tiempo total de proceso(ms): " + (System.currentTimeMillis() - startTime) + " milisegundos.");
			logger.info(mensajeTransaccion + "************************* FIN *************************");
		}
	}
	
	private void iniciarProceso(String mensajeTransaccion, String idTransaction) {		
		generarAjusteMasivoService.run(mensajeTransaccion, idTransaction);
	}
	
	private void configurarProperties() {
		PropertyConfigurator.configure(propertiesExterno.vLog4JDir);
	}

}
