package pe.com.claro.oss.generar.ajuste.masivo.bean;

public class AjusteBean {

	private int id;
	private int procesoId;
	private String docReferencia;
	private boolean priorizado;
	private String tipoSpool;
	private String tipoNotificacion;
	private String nombreArchivo;
	private String correoDestino;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getProcesoId() {
		return procesoId;
	}
	public void setProcesoId(int procesoId) {
		this.procesoId = procesoId;
	}
	public String getDocReferencia() {
		return docReferencia;
	}
	public void setDocReferencia(String docReferencia) {
		this.docReferencia = docReferencia;
	}
	public boolean isPriorizado() {
		return priorizado;
	}
	public void setPriorizado(boolean priorizado) {
		this.priorizado = priorizado;
	}
	public String getTipoSpool() {
		return tipoSpool;
	}
	public void setTipoSpool(String tipoSpool) {
		this.tipoSpool = tipoSpool;
	}
	public String getTipoNotificacion() {
		return tipoNotificacion;
	}
	public void setTipoNotificacion(String tipoNotificacion) {
		this.tipoNotificacion = tipoNotificacion;
	}
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public String getCorreoDestino() {
		return correoDestino;
	}
	public void setCorreoDestino(String correoDestino) {
		this.correoDestino = correoDestino;
	}
	
}
