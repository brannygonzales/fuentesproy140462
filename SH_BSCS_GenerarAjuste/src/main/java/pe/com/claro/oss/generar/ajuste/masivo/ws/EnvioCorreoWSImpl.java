package pe.com.claro.oss.generar.ajuste.masivo.ws;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import pe.com.claro.eai.util.enviocorreo.EnvioCorreoSBPortType;
import pe.com.claro.eai.util.enviocorreo.types.EnviarCorreoRequest;
import pe.com.claro.eai.util.enviocorreo.types.EnviarCorreoResponse;
import pe.com.claro.oss.generar.ajuste.masivo.util.JAXBUtilitarios;
import pe.com.claro.oss.generar.ajuste.masivo.util.PropertiesExternos;

@Component
public class EnvioCorreoWSImpl implements EnvioCorreoWS {
	private static final Logger logger = Logger.getLogger(EnvioCorreoWSImpl.class);

	@Autowired
	@Qualifier(value = "envioCorreoWS")
	private EnvioCorreoSBPortType envioCorreoWS;
	@Autowired
	private PropertiesExternos propertiesExterno;

	@Override
	public EnviarCorreoResponse enviarCorreo(String mensajeTransaccion, EnviarCorreoRequest request) {
		EnviarCorreoResponse response = new EnviarCorreoResponse();
		long tiempoInicio = System.currentTimeMillis();
		
		try {
			mensajeTransaccion = mensajeTransaccion + "[enviarCorreo] ";
			log(mensajeTransaccion, " Inicio WS: [" + propertiesExterno.ws_envioCorreo_wsdl + "] - Operación: [enviarCorreo]");
			log(mensajeTransaccion, "Datos de entrada: \n[\n  " + JAXBUtilitarios.anyObjectToXmlText(mensajeTransaccion, request) + "\n]");

			response = envioCorreoWS.enviarCorreo(request);

			log(mensajeTransaccion, "Datos de salida: \n[\n  " + JAXBUtilitarios.anyObjectToXmlText(mensajeTransaccion, response) + "\n]");
		} 
		catch (Exception e) {
			logException(mensajeTransaccion, e);
		} 
		finally {
			log(mensajeTransaccion, " Fin WS: [" + propertiesExterno.ws_envioCorreo_wsdl + "] - Operación: [enviarCorreo] - Tiempo Total del proceso(ms)= " + (System.currentTimeMillis() - tiempoInicio) + " milisegundos.");
		}
		return response;
	}
	
	private static void log(String mensajeTransaccion, String texto) {
		logger.info(mensajeTransaccion + texto);
	}
	
	private static void logException(String mensajeTransaccion, Exception e) {
		logger.error(mensajeTransaccion + "ERROR: [Exception] - [" + e.getMessage() + "] ", e);
	}

}
