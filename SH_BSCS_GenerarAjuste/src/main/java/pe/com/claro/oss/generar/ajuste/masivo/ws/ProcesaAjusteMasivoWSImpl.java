package pe.com.claro.oss.generar.ajuste.masivo.ws;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import pe.com.claro.oss.generar.ajuste.masivo.bean.AjusteMasivoRequest;
import pe.com.claro.oss.generar.ajuste.masivo.bean.AjusteMasivoResponse;
import pe.com.claro.oss.generar.ajuste.masivo.util.Constantes;
import pe.com.claro.oss.generar.ajuste.masivo.util.PropertiesExternos;
import pe.com.claro.oss.generar.ajuste.masivo.util.Util;

@Service
public class ProcesaAjusteMasivoWSImpl implements ProcesaAjusteMasivoWS {

	private static Logger logger = Logger.getLogger(ProcesaAjusteMasivoWSImpl.class);

	@Autowired
	private PropertiesExternos propertiesExternos;
	
	@Override
	public AjusteMasivoResponse ajuste (String mensajeTransaccion, String idTransaccion, String usuarioApli, String msgid, AjusteMasivoRequest request) {
		AjusteMasivoResponse response = null;
		long tiempoInicio = System.currentTimeMillis();
		
		try {
			mensajeTransaccion = mensajeTransaccion + "[ajuste] ";
			log(mensajeTransaccion, " Inicio WS: [" + propertiesExternos.ws_procesaAjusteMasivo_ajuste_url + "] - Operación: [ajuste]");
			
			Gson inputGson = new Gson();
			String inputJson = inputGson.toJson(request, AjusteMasivoRequest.class);
			log(mensajeTransaccion, "Datos de entrada: \n[\n  " + inputJson + "\n]");

			Client client = Client.create();
			client.setConnectTimeout(propertiesExternos.ws_procesaAjusteMasivo_ajuste_connection_timeout);
			client.setReadTimeout(propertiesExternos.ws_procesaAjusteMasivo_ajuste_timeout);
	
			WebResource webResource = client.resource(propertiesExternos.ws_procesaAjusteMasivo_ajuste_url);
			ClientResponse clientResponse = null;
	
			clientResponse = webResource.accept(MediaType.APPLICATION_JSON)
									    .header( Constantes.NAME_HEADER_APPMSGID, msgid)
										.header( Constantes.NAME_HEADER_APP_IDTRANSACCION, idTransaccion)
							            .header(Constantes.NAME_HEADER_APPTIMESTAMP, Util.getFechaSistema())
							            .header(Constantes.NAME_HEADER_APPUSERID, usuarioApli)
							            .type( MediaType.APPLICATION_JSON)
										.post(ClientResponse.class, inputJson);
						
			String resultado = "El servicio no devolvío datos";
			if(clientResponse != null){
				if(clientResponse.getStatus() == 200){
					String rpta = clientResponse.getEntity(String.class);
					Gson outputGson = new Gson();
					resultado = rpta;
					response = outputGson.fromJson(rpta, AjusteMasivoResponse.class);	
				}
			}
			
			log(mensajeTransaccion, "Datos de salida: \n[ " + clientResponse+  "\n" + resultado +  "]");
		} 
		catch (Exception e) {
			logException(mensajeTransaccion, e);
		} 
		finally{
			log(mensajeTransaccion, " Fin WS: [" + propertiesExternos.ws_procesaAjusteMasivo_ajuste_url + "] - Operación: [ajuste] - Tiempo Total del proceso(ms)= " + (System.currentTimeMillis() - tiempoInicio) + " milisegundos.");
		}
		
		return response;
	}
	
	private static void log(String mensajeTransaccion, String texto) {
		logger.info(mensajeTransaccion + texto);
	}
	
	private static void logException(String mensajeTransaccion, Exception e) {
		logger.error(mensajeTransaccion + "ERROR: [Exception] - [" + e.getMessage() + "] ", e);
	}

}