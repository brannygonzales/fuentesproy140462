package pe.com.claro.oss.generar.ajuste.masivo.ws;

import pe.com.claro.oss.generar.ajuste.masivo.bean.AjusteMasivoRequest;
import pe.com.claro.oss.generar.ajuste.masivo.bean.AjusteMasivoResponse;

public interface ProcesaAjusteMasivoWS {

	public AjusteMasivoResponse ajuste (String mensajeTransaccion, String idTransaccion, String usuarioApli, String msgid, AjusteMasivoRequest request);
	
}

