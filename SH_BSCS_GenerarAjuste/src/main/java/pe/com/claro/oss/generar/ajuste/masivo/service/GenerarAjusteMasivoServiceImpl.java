package pe.com.claro.oss.generar.ajuste.masivo.service;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.claro.eai.util.enviocorreo.types.ArchivoAdjunto;
import pe.com.claro.eai.util.enviocorreo.types.AuditTypeRequest;
import pe.com.claro.eai.util.enviocorreo.types.EnviarCorreoRequest;
import pe.com.claro.eai.util.enviocorreo.types.EnviarCorreoResponse;
import pe.com.claro.eai.util.enviocorreo.types.ListaArchivosAdjuntos;
import pe.com.claro.oss.generar.ajuste.masivo.bean.AjusteBean;
import pe.com.claro.oss.generar.ajuste.masivo.bean.AjusteMasivoRequest;
import pe.com.claro.oss.generar.ajuste.masivo.bean.DetalleEjecucionBean;
import pe.com.claro.oss.generar.ajuste.masivo.dao.SiopDAO;
import pe.com.claro.oss.generar.ajuste.masivo.util.Constantes;
import pe.com.claro.oss.generar.ajuste.masivo.util.Constantes.EstadoSpool;
import pe.com.claro.oss.generar.ajuste.masivo.util.Constantes.TipoNotificacion;
import pe.com.claro.oss.generar.ajuste.masivo.util.Constantes.TipoSpool;
import pe.com.claro.oss.generar.ajuste.masivo.util.PropertiesExternos;
import pe.com.claro.oss.generar.ajuste.masivo.util.Util;
import pe.com.claro.oss.generar.ajuste.masivo.ws.EnvioCorreoWS;
import pe.com.claro.oss.generar.ajuste.masivo.ws.ProcesaAjusteMasivoWS;


@Service
public class GenerarAjusteMasivoServiceImpl implements GenerarAjusteMasivoService {

	private static Logger logger = Logger.getLogger(GenerarAjusteMasivoServiceImpl.class);
	
	@Autowired
	private PropertiesExternos propertiesExterno;
	@Autowired
	private SiopDAO siopDAO;
	@Autowired
	private EnvioCorreoWS envioCorreoWS;
	@Autowired
	private ProcesaAjusteMasivoWS procesaAjusteMasivoWS;
	
	private String mensajeTransaccion;
	private String idTransaction;
	
	@Override
	public void run (String mensajeTransaccion, String idTransaction) {
		this.mensajeTransaccion = mensajeTransaccion;
		this.idTransaction = idTransaction;
		
		List<AjusteBean> listaAjustes = siopDAO.obtenerAjustesCabecera(mensajeTransaccion);
		if(listaAjustes == null || listaAjustes.size() == 0)
			return ;
		
		AjusteBean objAjusteBean = listaAjustes.get(0);
		boolean estadoSpool = false;
		
		try {
			procesarAjuste(listaAjustes);
			if(!migrarDataHistorico(objAjusteBean)) {
				enviarDetalleEjecucion(objAjusteBean, estadoSpool);
				return;
			}
			
			if(objAjusteBean.isPriorizado()) 
				estadoSpool = generarSpool(listaAjustes, objAjusteBean);
		} 
		catch (Exception e) {
			logger.error(mensajeTransaccion + "Error: ", e);
		}
		finally {
			enviarDetalleEjecucion(objAjusteBean, estadoSpool);
			eliminarCarpetas();	
		}
	}
	
	private boolean migrarDataHistorico(AjusteBean objAjusteBean) {
		return siopDAO.migrarDataHistorico(mensajeTransaccion, objAjusteBean.getProcesoId());
	}
	
	private void enviarDetalleEjecucion(AjusteBean objAjusteBean, boolean estadoSpool) {
		String estado = EstadoSpool.NO_GENERADO.name();
		if(estadoSpool)
			estado = EstadoSpool.GENERADO.name();;
			
		List<DetalleEjecucionBean> listaDetalle = siopDAO.obtenerDetalleEjecucion(mensajeTransaccion, objAjusteBean.getProcesoId(), estado);
		
		if(listaDetalle != null && listaDetalle.size() > 0)
			enviarCorreoDetalleEjecucion(listaDetalle, estadoSpool, objAjusteBean);
	}
	
	private void procesarAjuste(List<AjusteBean> listaAjustes) {
		AjusteMasivoRequest request;
		for(AjusteBean objAjusteBean : listaAjustes) {
			request = new AjusteMasivoRequest();
			request.setProcesoId(String.valueOf(objAjusteBean.getProcesoId()));
			request.setNroRecibo(objAjusteBean.getDocReferencia());
			procesaAjusteMasivoWS.ajuste(mensajeTransaccion, idTransaction, Constantes.USR_SIOP, objAjusteBean.getDocReferencia(), request);
		}
	}
		
	private String[] filtrarListaPorTipoSpool (List<AjusteBean> listaAjustes) {
		Set<String> listaSinValoresRepetidos = new HashSet<>(listaAjustes.size());
		for(AjusteBean obj : listaAjustes) 
			listaSinValoresRepetidos.add(obj.getTipoSpool());
		
		log("listaSinValoresRepetidos.size(): " + listaSinValoresRepetidos.size());
		String[] listaFiltrada = new String[listaSinValoresRepetidos.size()];
		listaSinValoresRepetidos.toArray(listaFiltrada);
		return listaFiltrada;
	}
	
	private boolean generarSpool(List<AjusteBean> listaAjustes, AjusteBean objAjusteBean) {
		String[] listaTipoSpool = filtrarListaPorTipoSpool(listaAjustes);
		log("listaTipoSpool.length: " + listaTipoSpool.length);
		Map<String, List<String>> tramas = new HashMap<String, List<String>>();
		int totalNroSpool = 0;
		
		for(String tipoSpool : listaTipoSpool ) {
			if(tipoSpool.equals(TipoSpool.TRAMA_AJUSTE.name())) {
				totalNroSpool += Constantes.TOTAL_NRO_SPOOL_TRAMA_AJUSTE;
				tramas.putAll(generarSpoolTramaAjuste(objAjusteBean.getProcesoId()));
			}	
			if(tipoSpool.equals(TipoSpool.TRAMA_LARGA.name())) {
				totalNroSpool += Constantes.TOTAL_NRO_SPOOL_TRAMA_LARGA;
				tramas.putAll(generarSpoolTramaLarga(objAjusteBean.getProcesoId()));
			}
		}
		
		log("Nro total de Spool obtenidos : " + tramas.size());
		if(totalNroSpool == 0 || tramas.size() != totalNroSpool) 
			return false;
		
		String urlCarpeta = crearCarpeta();
		if(urlCarpeta == null) 
			return false;
				
		crearArchivos(tramas, urlCarpeta);
		if(!zipearArchivos(urlCarpeta, totalNroSpool))
			return false;
		
		if(!enviarCorreoSpool(objAjusteBean.getTipoNotificacion(), urlCarpeta))
			return false;
		
		return true;
	}
	
	private boolean enviarCorreoSpool(String tipoNotificacion, String urlCarpeta) {
		AuditTypeRequest auditTypeRequest=new AuditTypeRequest();
		auditTypeRequest.setIdTransaccion(idTransaction);
		auditTypeRequest.setIpAplicacion(Util.getIPHost());
		auditTypeRequest.setCodigoAplicacion(propertiesExterno.ws_envioCorreo_auditRequest_codigoAplicacion);
		auditTypeRequest.setUsrAplicacion(propertiesExterno.ws_envioCorreo_auditRequest_usuarioAplicacion);
		
		EnviarCorreoRequest request = new EnviarCorreoRequest();
		request.setAuditRequest(auditTypeRequest);
		request.setAsunto(propertiesExterno.ws_envioCorreo_spool_asunto);
		request.setRemitente(propertiesExterno.ws_envioCorreo_spool_remitente);
		request.setDestinatario(propertiesExterno.ws_envioCorreo_spool_destinatario);
		request.setHtmlFlag(propertiesExterno.ws_envioCorreo_spool_htmlFlag);
		request.setListaArchivosAdjuntos(getArchivoAdjunto(urlCarpeta));
		
		if(tipoNotificacion.equals(TipoNotificacion.ENVIO_ASESOR.name()))
			request.setMensaje(propertiesExterno.ws_envioCorreo_spool_mensaje_asesor);
		if(tipoNotificacion.equals(TipoNotificacion.ENVIO_DISTRIBUCION.name()))
			request.setMensaje(propertiesExterno.ws_envioCorreo_spool_mensaje_distribucion);
		if(tipoNotificacion.equals(TipoNotificacion.CARGA_REPOSITORIO.name()))
			request.setMensaje(propertiesExterno.ws_envioCorreo_spool_mensaje_repositorio);
		
		EnviarCorreoResponse response = envioCorreoWS.enviarCorreo(mensajeTransaccion, request);
		if(response.getAuditResponse() != null && response.getAuditResponse().getCodigoRespuesta().equals(Constantes.CODIGO_RESPUESTA_EXITOSA))
			return true;
		
		return false;
	}
	
	private void enviarCorreoDetalleEjecucion(List<DetalleEjecucionBean> listaDetalle, boolean estadoSpool, AjusteBean objAjusteBean) {
		AuditTypeRequest auditTypeRequest=new AuditTypeRequest();
		auditTypeRequest.setIdTransaccion(idTransaction);
		auditTypeRequest.setIpAplicacion(Util.getIPHost());
		auditTypeRequest.setCodigoAplicacion(propertiesExterno.ws_envioCorreo_auditRequest_codigoAplicacion);
		auditTypeRequest.setUsrAplicacion(propertiesExterno.ws_envioCorreo_auditRequest_usuarioAplicacion);
		
		EnviarCorreoRequest request = new EnviarCorreoRequest();
		request.setAuditRequest(auditTypeRequest);
		request.setAsunto(propertiesExterno.ws_envioCorreo_detalleEjecucion_asunto);
		request.setRemitente(propertiesExterno.ws_envioCorreo_detalleEjecucion_remitente);
//		request.setDestinatario(propertiesExterno.ws_envioCorreo_detalleEjecucion_destinatario);
		request.setDestinatario(objAjusteBean.getCorreoDestino());
		request.setHtmlFlag(propertiesExterno.ws_envioCorreo_detalleEjecucion_htmlFlag);
		request.setMensaje(getCuerpoCorreo(listaDetalle, estadoSpool, objAjusteBean));
		
		envioCorreoWS.enviarCorreo(mensajeTransaccion, request);
	}
	
	private String getCuerpoCorreo(List<DetalleEjecucionBean> listaDetalle, boolean estadoSpool, AjusteBean objAjusteBean) {
		StringBuilder filas = new StringBuilder();
		for(DetalleEjecucionBean obj : listaDetalle) {
			filas.append("<tr>");
				filas.append("<td>").append(obj.getDocReferencia()).append("</td>");
				filas.append("<td>").append(obj.getNroDocAjuste()).append("</td>");
				filas.append("<td align='center'>").append(obj.getTipificado()).append("</td>");
				filas.append("<td>").append(obj.getComentario()).append("</td>");
			filas.append("</tr>");
		}
		
		String mensaje = propertiesExterno.ws_envioCorreo_detalleEjecucion_mensaje;
		mensaje = mensaje.replace(Constantes.CUERPO_EMAIL_FILAS_REMPLAZAR, filas.toString());
		mensaje = mensaje.replace(Constantes.CUERPO_EMAIL_TEXTO_REMPLAZAR, 
								  Constantes.CUERPO_EMAIL_TEXTO_NOMBRE_ARCHIVO.replace(Constantes.CUERPO_EMAIL_NOMBRE_ARCHIVO_REMPLAZAR, objAjusteBean.getNombreArchivo()));
		
		if(estadoSpool)	
			mensaje = mensaje.replace(Constantes.CUERPO_EMAIL_SPOOL_REMPLAZAR, Constantes.CUERPO_EMAIL_TEXTO_SPOOL_GENERADO);
		else			
			mensaje = mensaje.replace(Constantes.CUERPO_EMAIL_SPOOL_REMPLAZAR, Constantes.CUERPO_EMAIL_TEXTO_SPOOL_NO_GENERADO);
		
		return mensaje;
	}
	
	private ListaArchivosAdjuntos getArchivoAdjunto(String urlCarpeta) {
		String urlArchivoZip = Util.getNombreArchivoZip(urlCarpeta, idTransaction);
		ArchivoAdjunto archivoAdjunto = new ArchivoAdjunto();
		archivoAdjunto.setArchivo(Util.convertirArchivoAByte(mensajeTransaccion, urlArchivoZip));
		archivoAdjunto.setNombre(Util.getNombreArchivoZip(idTransaction));
		
		ListaArchivosAdjuntos listaArchivos = new ListaArchivosAdjuntos();
		listaArchivos.getArchivoAdjunto().add(archivoAdjunto);
		return listaArchivos;
	}
	
	private String crearCarpeta() {
		String urlCarpeta = propertiesExterno.ruta_archivos_generados + idTransaction + "/";
		return Util.crearCarpeta(mensajeTransaccion, urlCarpeta);
	}
	
	private void eliminarCarpetas() {
		File carpetaZIP = new File(propertiesExterno.ruta_archivos_generados + idTransaction);
		Util.eliminarCarpeta(mensajeTransaccion, carpetaZIP);

}
	
	private void crearArchivos(Map<String, List<String>> tramas, String urlCarpeta) {
		for(Map.Entry<String, List<String>> trama : tramas.entrySet()) 
			Util.generarArchivo(mensajeTransaccion,  urlCarpeta, trama.getKey(), trama.getValue());
	}
	
	private boolean zipearArchivos(String urlCarpeta, int totalNroSpool) {
		return Util.generarArchivoZip(mensajeTransaccion, urlCarpeta, idTransaction, totalNroSpool);
	}
	
	private Map<String, List<String>> generarSpoolTramaAjuste(int procesoId) {
		List<String> listaCajaCliente = siopDAO.generarSpool(mensajeTransaccion, procesoId, TipoSpool.CAJA_CLIENTE.name());
		List<String> listaCajaFactura = siopDAO.generarSpool(mensajeTransaccion, procesoId, TipoSpool.CAJA_FACTURA.name());
		List<String> listaTotalHp 	  = siopDAO.generarSpool(mensajeTransaccion, procesoId, TipoSpool.TOTAL_HP.name());
		List<String> listaCambioDatos = siopDAO.generarSpool(mensajeTransaccion, procesoId, TipoSpool.CAMBIO_DATOS.name());
		List<String> listaTramaAjuste = siopDAO.generarSpool(mensajeTransaccion, procesoId, TipoSpool.TRAMA_AJUSTE.name());
		
		Map<String, List<String>> tramas = new HashMap<String, List<String>>();
		
		if(listaCajaCliente != null && !listaCajaCliente.isEmpty())
			tramas.put(Constantes.ARCHIVO_NCND_CAJA_CLIENTE, listaCajaCliente);
		if(listaCajaFactura != null && !listaCajaFactura.isEmpty())
			tramas.put(Constantes.ARCHIVO_NCND_CAJA_FACTURA, listaCajaFactura);
		if(listaTotalHp != null && !listaTotalHp.isEmpty())
			tramas.put(Constantes.ARCHIVO_SPOOL_TOTAL_HP, listaTotalHp);
		if(listaCambioDatos != null && !listaCambioDatos.isEmpty())
			tramas.put(Constantes.ARCHIVO_SPOOL_NCND_CAMBIO_DATOS, listaCambioDatos);
		if(listaTramaAjuste != null && !listaTramaAjuste.isEmpty())
			tramas.put(Constantes.ARCHIVO_SPOOL_NCND_HP, listaTramaAjuste);
		
		return tramas;
	}
	
	private Map<String, List<String>> generarSpoolTramaLarga(int procesoId) {
		List<String> listaTramaLarga = siopDAO.generarSpool(mensajeTransaccion, procesoId, TipoSpool.TRAMA_LARGA.name());
		Map<String, List<String>> tramas = new HashMap<String, List<String>>();
		
		if(listaTramaLarga != null && !listaTramaLarga.isEmpty()) {
			listaTramaLarga.set(0, Constantes.CABECERA_SPOOL_REFACTURACION);
			tramas.put(Constantes.ARCHIVO_SPOOL_REFACTURACION, listaTramaLarga);
		}
		return tramas;
	}

	private void log(String texto) {
		logger.info(mensajeTransaccion + texto);
	}

}
 