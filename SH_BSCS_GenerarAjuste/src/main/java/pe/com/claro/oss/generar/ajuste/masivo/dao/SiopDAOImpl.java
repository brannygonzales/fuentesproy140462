package pe.com.claro.oss.generar.ajuste.masivo.dao;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.com.claro.oss.generar.ajuste.masivo.bean.AjusteBean;
import pe.com.claro.oss.generar.ajuste.masivo.bean.DetalleEjecucionBean;
import pe.com.claro.oss.generar.ajuste.masivo.mapper.CursorDatos1Mapper;
import pe.com.claro.oss.generar.ajuste.masivo.mapper.CursorDatos2Mapper;
import pe.com.claro.oss.generar.ajuste.masivo.mapper.CursorDatos3Mapper;
import pe.com.claro.oss.generar.ajuste.masivo.util.Constantes;
import pe.com.claro.oss.generar.ajuste.masivo.util.PropertiesExternos;
import pe.com.claro.oss.generar.ajuste.masivo.util.Util;


@Repository
public class SiopDAOImpl implements SiopDAO {
	
	private static Logger logger = Logger.getLogger(SiopDAOImpl.class);
	
	@Autowired
	private PropertiesExternos propertiesExterno;
	
	@Autowired
	@Qualifier("siopDS")
	private DataSource siopDS;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AjusteBean> obtenerAjustesCabecera(String mensajeTransaccion) {
		List<AjusteBean> listaAjustes = null;
		String sp = Util.getProcedure(propertiesExterno.db_siop_owner, propertiesExterno.db_siop_pckAjusteMasivos, propertiesExterno.db_siop_spObtenerAjustesCabecera);
				
		try {
			mensajeTransaccion = mensajeTransaccion + "[obtenerAjustesCabecera] ";
			log(mensajeTransaccion, " Inicio PROCEDURE: [" + sp + "]");

			SimpleJdbcCall objJdbcCall = new SimpleJdbcCall(siopDS)
										 	.withoutProcedureColumnMetaDataAccess()
											.withSchemaName(propertiesExterno.db_siop_owner)
											.withCatalogName(propertiesExterno.db_siop_pckAjusteMasivos)
											.withProcedureName(propertiesExterno.db_siop_spObtenerAjustesCabecera)
											.declareParameters(new SqlOutParameter("PO_CURSOR", OracleTypes.CURSOR, new CursorDatos1Mapper() ),
															   new SqlOutParameter("PO_CODERROR", OracleTypes.VARCHAR),
															   new SqlOutParameter("PO_DESERROR", OracleTypes.VARCHAR) );
			
			objJdbcCall.getJdbcTemplate().setQueryTimeout(propertiesExterno.db_siop_connection_timeout);
			Map<String, Object> result = objJdbcCall.execute();
			
			listaAjustes = (List<AjusteBean>) result.get("PO_CURSOR");
			for(AjusteBean la:listaAjustes) {
				log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [getProcesoId:" + la.getProcesoId());
				log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [getDocReferencia:" + la.getDocReferencia() );
				log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [getTipoSpool:" + la.getTipoSpool() );
			}
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CURSOR:" + listaAjustes.size() + " registros devueltos]");
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CODERROR:" + result.get("PO_CODERROR").toString() + "]");
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_DESERROR:" + result.get("PO_DESERROR").toString() + "]");
		}
		catch (UncategorizedSQLException e) {
			logException(mensajeTransaccion, "El SP [" + sp + "] no devolvio algún dato correctamente");
		}
		catch (Exception e) {
			logException(mensajeTransaccion, e);
		}
		finally {
			log(mensajeTransaccion, " Fin PROCEDURE: [" + sp + "]");
		}
		
		return listaAjustes;
	}
		
	
	@SuppressWarnings("unchecked")
	@Override
	public List<String> generarSpool(String mensajeTransaccion, int procesoId, String tipo) {
		List<String> listaTrama = null;
		String sp = Util.getProcedure(propertiesExterno.db_siop_owner, propertiesExterno.db_siop_pckAjusteMasivos, propertiesExterno.db_siop_spGenerarSpool);
				
		try {
			mensajeTransaccion = mensajeTransaccion + "[generarSpool] ";
			
			log(mensajeTransaccion, " Inicio PROCEDURE: [" + sp + "]");
			log(mensajeTransaccion, "PARAMETROS [INPUT]: [PI_ID_PROCESO:" + procesoId + "]");
			log(mensajeTransaccion, "PARAMETROS [INPUT]: [PI_TIPO:" + tipo + "]");
			
			SimpleJdbcCall objJdbcCall = new SimpleJdbcCall(siopDS)
										 	.withoutProcedureColumnMetaDataAccess()
											.withSchemaName(propertiesExterno.db_siop_owner)
											.withCatalogName(propertiesExterno.db_siop_pckAjusteMasivos)
											.withProcedureName(propertiesExterno.db_siop_spGenerarSpool)
											.declareParameters(new SqlParameter("PI_ID_PROCESO", OracleTypes.NUMBER),
															   new SqlParameter("PI_TIPO", OracleTypes.VARCHAR),
															   new SqlOutParameter("PO_CURSOR", OracleTypes.CURSOR, new CursorDatos2Mapper() ),
															   new SqlOutParameter("PO_CODERROR", OracleTypes.VARCHAR),
															   new SqlOutParameter("PO_DESERROR", OracleTypes.VARCHAR));
			
			SqlParameterSource objParametrosIN = new MapSqlParameterSource().addValue("PI_ID_PROCESO", procesoId)
																			.addValue("PI_TIPO", tipo);
										
			objJdbcCall.getJdbcTemplate().setQueryTimeout(propertiesExterno.db_siop_connection_timeout);
			Map<String, Object> result = objJdbcCall.execute(objParametrosIN);
			
			listaTrama = (List<String>) result.get("PO_CURSOR");
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CURSOR:" + listaTrama.size() + " registro(s) devuelto(s)]");
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CODERROR:" + result.get("PO_CODERROR").toString() + "]");
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_DESERROR:" + result.get("PO_DESERROR").toString() + "]");
		}
		catch (UncategorizedSQLException e) {
			logException(mensajeTransaccion, "El SP [" + sp + "] no devolvio algún dato correctamente");
			logException(mensajeTransaccion, e);
		}
		catch (Exception e) {
			logException(mensajeTransaccion, e);
		}
		finally {
			log(mensajeTransaccion, " Fin PROCEDURE: [" + sp + "]");
		}
		
		return listaTrama;
	}
	
	@Override
	public boolean migrarDataHistorico(String mensajeTransaccion, int procesoId) {
		boolean resultado = false;
		String sp = Util.getProcedure(propertiesExterno.db_siop_owner, propertiesExterno.db_siop_pckAjusteMasivos, propertiesExterno.db_siop_spMigrarDataHistorico);
				
		try {
			mensajeTransaccion = mensajeTransaccion + "[migrarDataHistorico] ";
			
			log(mensajeTransaccion, " Inicio PROCEDURE: [" + sp + "]");
			log(mensajeTransaccion, "PARAMETROS [INPUT]: [PI_ID_PROCESO:" + procesoId + "]");
			
			SimpleJdbcCall objJdbcCall = new SimpleJdbcCall(siopDS)
										 	.withoutProcedureColumnMetaDataAccess()
											.withSchemaName(propertiesExterno.db_siop_owner)
											.withCatalogName(propertiesExterno.db_siop_pckAjusteMasivos)
											.withProcedureName(propertiesExterno.db_siop_spMigrarDataHistorico)
											.declareParameters(new SqlParameter("PI_ID_PROCESO", OracleTypes.INTEGER),
															   new SqlOutParameter("PO_CODERROR", OracleTypes.VARCHAR),
															   new SqlOutParameter("PO_DESERROR", OracleTypes.VARCHAR));
			
			SqlParameterSource objParametrosIN = new MapSqlParameterSource().addValue("PI_ID_PROCESO", procesoId);
										
			objJdbcCall.getJdbcTemplate().setQueryTimeout(propertiesExterno.db_siop_connection_timeout);
			Map<String, Object> result = objJdbcCall.execute(objParametrosIN);
			
			if(result.get("PO_CODERROR").toString().equals(Constantes.CODIGO_RESPUESTA_EXITOSA))
				resultado = true;
		
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CODERROR:" + result.get("PO_CODERROR").toString() + "]");
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_DESERROR:" + result.get("PO_DESERROR").toString() + "]");
		}
		catch (UncategorizedSQLException e) {
			logException(mensajeTransaccion, "El SP [" + sp + "] no devolvio algún dato correctamente");
		}
		catch (Exception e) {
			logException(mensajeTransaccion, e);
		}
		finally {
			log(mensajeTransaccion, " Fin PROCEDURE: [" + sp + "]");
		}
		
		return resultado;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DetalleEjecucionBean> obtenerDetalleEjecucion(String mensajeTransaccion, int procesoId, String estadoSpool) {
		List<DetalleEjecucionBean> listaDetalle = null;
		String sp = Util.getProcedure(propertiesExterno.db_siop_owner, propertiesExterno.db_siop_pckAjusteMasivos, propertiesExterno.db_siop_spObtenerDetalleProceso);
				
		try {
			mensajeTransaccion = mensajeTransaccion + "[obtenerDetalleEjecucion] ";
			
			log(mensajeTransaccion, " Inicio PROCEDURE: [" + sp + "]");
			log(mensajeTransaccion, "PARAMETROS [INPUT]: [PI_ID_PROCESO:" + procesoId + "]");
			log(mensajeTransaccion, "PARAMETROS [INPUT]: [PI_ESTADO_SPOOL:" + estadoSpool + "]");
			
			SimpleJdbcCall objJdbcCall = new SimpleJdbcCall(siopDS)
										 	.withoutProcedureColumnMetaDataAccess()
											.withSchemaName(propertiesExterno.db_siop_owner)
											.withCatalogName(propertiesExterno.db_siop_pckAjusteMasivos)
											.withProcedureName(propertiesExterno.db_siop_spObtenerDetalleProceso)
											.declareParameters(new SqlParameter("PI_ID_PROCESO", OracleTypes.INTEGER),
															   new SqlParameter("PI_ESTADO_SPOOL", OracleTypes.VARCHAR),
															   new SqlOutParameter("PO_CURSOR", OracleTypes.CURSOR, new CursorDatos3Mapper() ),
															   new SqlOutParameter("PO_CODERROR", OracleTypes.VARCHAR),
															   new SqlOutParameter("PO_DESERROR", OracleTypes.VARCHAR));
			
			SqlParameterSource objParametrosIN = new MapSqlParameterSource().addValue("PI_ID_PROCESO", procesoId)
																			.addValue("PI_ESTADO_SPOOL", estadoSpool);
										
			objJdbcCall.getJdbcTemplate().setQueryTimeout(propertiesExterno.db_siop_connection_timeout);
			Map<String, Object> result = objJdbcCall.execute(objParametrosIN);
			
			listaDetalle = (List<DetalleEjecucionBean>) result.get("PO_CURSOR");
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CURSOR:" + listaDetalle.size() + " registro(s) devuelto(s)]");
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CODERROR:" + result.get("PO_CODERROR").toString() + "]");
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_DESERROR:" + result.get("PO_DESERROR").toString() + "]");
		}
		catch (UncategorizedSQLException e) {
			logException(mensajeTransaccion, "El SP [" + sp + "] no devolvio algún dato correctamente");
		}
		catch (Exception e) {
			logException(mensajeTransaccion, e);
		}
		finally {
			log(mensajeTransaccion, " Fin PROCEDURE: [" + sp + "]");
		}
		
		return listaDetalle;
	}
	
	
	private static void log(String mensajeTransaccion, String texto) {
		logger.info(mensajeTransaccion + texto);
	}
	
	private static void logException(String mensajeTransaccion, Exception e) {
		logger.error(mensajeTransaccion + "ERROR: [Exception] - [" + e.getMessage() + "] ", e);
	}
	
	private static void logException(String mensajeTransaccion, String error) {
		logger.error(mensajeTransaccion + "ERROR: " + error );
	}

}
