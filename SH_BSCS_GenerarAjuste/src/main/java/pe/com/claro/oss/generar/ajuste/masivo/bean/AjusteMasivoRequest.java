package pe.com.claro.oss.generar.ajuste.masivo.bean;

public class AjusteMasivoRequest {

	private String procesoId;
	private String nroRecibo;
	
	public String getProcesoId() {
		return procesoId;
	}
	public void setProcesoId(String procesoId) {
		this.procesoId = procesoId;
	}
	public String getNroRecibo() {
		return nroRecibo;
	}
	public void setNroRecibo(String nroRecibo) {
		this.nroRecibo = nroRecibo;
	}

}
