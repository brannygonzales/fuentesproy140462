package pe.com.claro.oss.generar.ajuste.masivo.util;

public class Constantes {

	public static final String URL_CONTEXT = "./spring/applicationContext.xml";
	public static final String SEPARADORPUNTO = ".";
	public static final String TEXTO_VACIO = "";
	
	public static final String ARCHIVO_NCND_CAJA_CLIENTE = "MensajeNCND_Caja_Cliente.txt";
	public static final String ARCHIVO_NCND_CAJA_FACTURA = "MensajeNCND_Caja_Factura.txt";
	public static final String ARCHIVO_SPOOL_TOTAL_HP = "Spool_Total_HP.txt";
	public static final String ARCHIVO_SPOOL_NCND_CAMBIO_DATOS = "SpoolNCND_Cambio_de_Datos.txt";
	public static final String ARCHIVO_SPOOL_NCND_HP = "SpoolNCND_HP.txt";
	public static final String ARCHIVO_SPOOL_REFACTURACION = "Spool_Refacturacion.csv";
	
	public static final String CABECERA_SPOOL_REFACTURACION = "N;ELECTRONICO;CICLO;CODIGO_CLIENTE;CUENTA_CLIENTE;CLIENTE;CONTACTO;DIRECCION;REFERENCIA;DEPARTAMENTO;PROVINCIA;DISTRITO;NUM_DOC;TIPO_CLIENTE;FACTURA;FECHA_EMISION;TIPO;DOC;FECHA_EMISION;FECHA_VENCIMIENTO;REF01;REF02;REF03;REF04;REF05;REF06;REF07;REF08;REF09;REF10;REF11;REF12;REF13;REF14;REF15;REF16;DESDE;HASTA";

	public static final String CUERPO_EMAIL_TEXTO_REMPLAZAR = "[TEXTO]";
	public static final String CUERPO_EMAIL_SPOOL_REMPLAZAR = "[SPOOL]";
	public static final String CUERPO_EMAIL_FILAS_REMPLAZAR = "[FILAS]";
	public static final String CUERPO_EMAIL_NOMBRE_ARCHIVO_REMPLAZAR = "[ARCHIVO]";
	public static final String CUERPO_EMAIL_TEXTO_SPOOL_GENERADO = "El Spool fué generado.";
	public static final String CUERPO_EMAIL_TEXTO_SPOOL_NO_GENERADO = "El Spool no fué generado.";
	public static final String CUERPO_EMAIL_TEXTO_NOMBRE_ARCHIVO = "Se envía detalle de la ejecución del ajuste masivo del archivo <strong>[ARCHIVO]</strong>.";
	
	public static final String AJUSTE_PRIORIZADO = "PRIORIZADO";
	public static enum TipoSpool {CAJA_CLIENTE, CAJA_FACTURA, TOTAL_HP, CAMBIO_DATOS, TRAMA_AJUSTE, TRAMA_LARGA};
	public static enum TipoNotificacion {ENVIO_ASESOR, ENVIO_DISTRIBUCION, CARGA_REPOSITORIO}; 
	public static enum EstadoSpool {GENERADO, NO_GENERADO}; 
	
	public static int TOTAL_NRO_SPOOL_TRAMA_AJUSTE = 5; 
	public static int TOTAL_NRO_SPOOL_TRAMA_LARGA = 1; 
	public static String CODIGO_RESPUESTA_EXITOSA = "0";
	
    public static final String NAME_HEADER_APP_IDTRANSACCION = "idTransaccion";
    public static final String NAME_HEADER_APPMSGID = "msgid";
    public static final String NAME_HEADER_APPTIMESTAMP = "timestamp";
    public static final String NAME_HEADER_APPUSERID = "userid";
    public static final String USR_SIOP = "USRSIOP";
	
}
