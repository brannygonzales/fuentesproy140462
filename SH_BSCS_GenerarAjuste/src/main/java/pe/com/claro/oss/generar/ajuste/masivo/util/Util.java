package pe.com.claro.oss.generar.ajuste.masivo.util;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class Util {
	
	private static Logger logger = Logger.getLogger(Util.class);
		
	public static String getProcedure(String owner, String pck, String sp) {
		StringBuilder sb = new StringBuilder();
		sb.append(owner).append(Constantes.SEPARADORPUNTO).append(pck).append(Constantes.SEPARADORPUNTO).append(sp);
		return sb.toString();
	}
	
	public static String getIPHost() {
		String hostAddress;
		try {
			hostAddress = Inet4Address.getLocalHost().getHostAddress();
		} 
		catch (UnknownHostException e) {
			hostAddress = Constantes.TEXTO_VACIO;
		}
		return hostAddress;
	}
	
	public static String getFechaSistema() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		return dateFormat.format(new Date());
	}
	
	public static String crearCarpeta(String mensajeTransaccion, String urlCarpeta) {
		File carpeta = new File(urlCarpeta);
		if(carpeta.mkdirs()) { 
			logger.warn(mensajeTransaccion + "Carpeta creada : " + urlCarpeta);
			return urlCarpeta;
		}	
		else { 
			logger.warn(mensajeTransaccion + "No se pudo crear la carepta : " + urlCarpeta);
			return null;
		}	
	}
	
	public static void eliminarCarpeta(String mensajeTransaccion, File archivo) {
		if (!archivo.exists()) 
			return;

	    if (archivo.isDirectory()){ 
	        for (File file : archivo.listFiles()) 
	        	eliminarCarpeta(null, file);
	    }
	    
	    if(archivo.delete()) {  
	    	if(mensajeTransaccion != null)
	    		logger.info(mensajeTransaccion + "Archivo eliminado : " + archivo.getPath());
	    }
	    else {
			if(mensajeTransaccion != null)
				logger.warn(mensajeTransaccion + "No se pudo eliminar el archivo : " + archivo.getPath());
		}	
	}
	
	public static boolean generarArchivoZip(String mensajeTransaccion, String urlCarpeta, String idTransaction, int totalNroSpool) {
		try {
			File carpetaComprimir = new File(urlCarpeta);
			
			if (carpetaComprimir.exists()) {
				File[] listaArchivos = carpetaComprimir.listFiles();
				logger.info(mensajeTransaccion + "Número de archivos a comprimir: " + listaArchivos.length);
				
				if(listaArchivos.length != totalNroSpool)
					return false;
				
				ZipOutputStream zous = new ZipOutputStream(new FileOutputStream(getNombreArchivoZip(urlCarpeta, idTransaction)));
				
				for (int i = 0; i < listaArchivos.length; i++) {
					logger.info(mensajeTransaccion + "Comprimiendo el archivo: " + listaArchivos[i].getName());
					ZipEntry archivoZip = new ZipEntry(listaArchivos[i].getName());
					zous.putNextEntry(archivoZip);
					
					FileInputStream fis = new FileInputStream(urlCarpeta + archivoZip.getName());
					int leer;
					byte[] buffer = new byte[1024];
					while (0 < (leer = fis.read(buffer))) {
						zous.write(buffer, 0, leer);
					}
					fis.close();
				}
				zous.closeEntry();
				zous.close();	
				logger.info(mensajeTransaccion + "Archivo zip creado: " + idTransaction + ".zip");
			}
			else
				logger.warn(mensajeTransaccion + "La carpeta " + urlCarpeta + " no existe");
		} 
		catch (Exception e) {
			logger.error(mensajeTransaccion + "Error: ", e);
			return false;
		}
		return true;
	}
	
	public static void generarArchivo(String mensajeTransaccion, String urlCarpeta, String nombreArchivo, List<String> listaData) {
		try {
			try(FileWriter flwriter = new FileWriter(urlCarpeta + nombreArchivo)){
				try(BufferedWriter bfwriter = new BufferedWriter(flwriter)){
					for (String data : listaData) {
						bfwriter.write(data);
						bfwriter.newLine();
					}
					
					logger.info(mensajeTransaccion + "Archivo creado: " + nombreArchivo);
				}
			}
 		} 
		catch (Exception e) {
			logger.error(mensajeTransaccion + "Error: ", e);
		} 
	}
	
	public static byte[] convertirArchivoAByte(String mensajeTransaccion, String urlArchivo) {
		byte bytes[] = null;
		try (FileInputStream fis = new FileInputStream(new File(urlArchivo))) {
		    try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
		        byte[] buffer = new byte[1024];
		        int leer = -1;
		        while ((leer = fis.read(buffer)) != -1) {
		            baos.write(buffer, 0, leer);
		        }
		        bytes = baos.toByteArray();
		    } 
		} 
		catch (Exception e) {
			logger.error(mensajeTransaccion + "Error: ", e);
		}
		return bytes;
	}
	
	public static String getNombreArchivoZip(String urlCarpeta, String idTransaction) {
		return urlCarpeta + getNombreArchivoZip(idTransaction);
	}
	
	public static String getNombreArchivoZip(String idTransaction) {
		return idTransaction + ".zip";
	}
	
}