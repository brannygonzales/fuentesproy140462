package pe.com.claro.oss.generar.ajuste.masivo.service;

public interface GenerarAjusteMasivoService {
	
	void run(String mensajeTransaccion, String idTransaction);

}
