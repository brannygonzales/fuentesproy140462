package pe.com.claro.oss.generar.ajuste.masivo.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class CursorDatos2Mapper implements RowMapper<String>{

	@Override
	public String mapRow(ResultSet rs, int numeroFila) throws SQLException {
		return rs.getString(1);
	}
	

}
