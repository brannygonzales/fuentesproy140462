package pe.com.claro.oss.generar.ajuste.masivo.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pe.com.claro.oss.generar.ajuste.masivo.bean.DetalleEjecucionBean;

public class CursorDatos3Mapper implements RowMapper<DetalleEjecucionBean>{

	@Override
	public DetalleEjecucionBean mapRow(ResultSet rs, int numeroFila) throws SQLException {
		DetalleEjecucionBean objDetalleEjecucionBean = new DetalleEjecucionBean();
		objDetalleEjecucionBean.setDocReferencia(rs.getString("DOCREFERENCIA"));
		objDetalleEjecucionBean.setNroDocAjuste(rs.getString("NRODOCAJUSTE"));
		objDetalleEjecucionBean.setTipificado(rs.getString("TIPIFICADO"));
		objDetalleEjecucionBean.setComentario(rs.getString("COMENTARIO"));
		return objDetalleEjecucionBean;
	}
	
}
