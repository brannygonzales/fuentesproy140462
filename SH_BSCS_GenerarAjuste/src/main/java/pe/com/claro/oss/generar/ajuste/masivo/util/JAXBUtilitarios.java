package pe.com.claro.oss.generar.ajuste.masivo.util;

import java.io.StringWriter;
import java.util.HashMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import org.apache.log4j.Logger;
import org.apache.xmlbeans.XmlObject;


public class JAXBUtilitarios {

	private static Logger logger =  Logger.getLogger(JAXBUtilitarios.class);
	private static HashMap<Class, JAXBContext>	mapContexts	= new HashMap<Class, JAXBContext>();

	
	@SuppressWarnings( { "unchecked", "rawtypes" } )
	public static String anyObjectToXmlText(String mensajeTransaccion, Object objJaxB ){
		String xmlText = "El objeto es nulo";
		if( objJaxB != null ){
			try{
				JAXBContext context = obtainJaxBContextFromClass(mensajeTransaccion, objJaxB.getClass());
				Marshaller marshaller = context.createMarshaller();
				StringWriter xmlWriter = new StringWriter();
				marshaller.marshal( new JAXBElement( new QName( "", objJaxB.getClass().getName() ), objJaxB.getClass(), objJaxB ), xmlWriter );
				XmlObject xmlObj = XmlObject.Factory.parse( xmlWriter.toString() );
				xmlText = xmlObj.toString();
			}
			catch( Exception e ){
				logger.error(mensajeTransaccion + "ERROR: [Exception] - [" + e.getMessage() + "] ", e);
			}
		}
		return xmlText;
	}
	  

	@SuppressWarnings( "rawtypes" )
	private static JAXBContext obtainJaxBContextFromClass(String mensajeTransaccion,  Class clas){
		JAXBContext context = mapContexts.get( clas );
		if( context == null ){
			try{
				context = JAXBContext.newInstance( clas );
				mapContexts.put( clas, context );
			}
			catch( Exception e ){;
				logger.error(mensajeTransaccion + "ERROR: [Exception] - [" + e.getMessage() + "] ", e);
			}
		}
		return context;
	}
	   
}
