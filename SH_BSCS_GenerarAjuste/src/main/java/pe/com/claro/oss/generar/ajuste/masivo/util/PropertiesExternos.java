package pe.com.claro.oss.generar.ajuste.masivo.util;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PropertiesExternos {

	@Value("${log4j.file.dir}")
	public String vLog4JDir;
	
	@Value("${db.siop}")
	public String db_siop;
	@Value("${db.siop.owner}")
	public String db_siop_owner;
	@Value("${db.siop.pckAjusteMasivos}")
	public String db_siop_pckAjusteMasivos;
	@Value("${db.siop.spObtenerAjustesCabecera}")
	public String db_siop_spObtenerAjustesCabecera;
	@Value("${db.siop.connection.timeout}")
	public int db_siop_connection_timeout;
	@Value("${db.siop.spGenerarSpool}")
	public String db_siop_spGenerarSpool;
	@Value("${db.siop.spMigrarDataHistorico}")
	public String db_siop_spMigrarDataHistorico;
	@Value("${db.siop.spObtenerDetalleProceso}")
	public String db_siop_spObtenerDetalleProceso;
			
			
	@Value("${ruta.archivos.generados}")
	public String ruta_archivos_generados;
		
	@Value("${ws.envioCorreo.wsdl}")
	public String ws_envioCorreo_wsdl;

	@Value("${ws.envioCorreo.auditRequest.codigoAplicacion}")
	public String ws_envioCorreo_auditRequest_codigoAplicacion;
	@Value("${ws.envioCorreo.auditRequest.usuarioAplicacion}")
	public String ws_envioCorreo_auditRequest_usuarioAplicacion;
	@Value("${ws.envioCorreo.spool.asunto}")
	public String ws_envioCorreo_spool_asunto;
	@Value("${ws.envioCorreo.spool.remitente}")
	public String ws_envioCorreo_spool_remitente;
	@Value("${ws.envioCorreo.spool.destinatario}")
	public String ws_envioCorreo_spool_destinatario;
	@Value("${ws.envioCorreo.spool.htmlFlag}")
	public String ws_envioCorreo_spool_htmlFlag;
	@Value("${ws.envioCorreo.spool.mensaje.asesor}")
	public String ws_envioCorreo_spool_mensaje_asesor;
	@Value("${ws.envioCorreo.spool.mensaje.distribucion}")
	public String ws_envioCorreo_spool_mensaje_distribucion;
	@Value("${ws.envioCorreo.spool.mensaje.repositorio}")
	public String ws_envioCorreo_spool_mensaje_repositorio;
	
	@Value("${ws.envioCorreo.detalleEjecucion.asunto}")
	public String ws_envioCorreo_detalleEjecucion_asunto;
	@Value("${ws.envioCorreo.detalleEjecucion.remitente}")
	public String ws_envioCorreo_detalleEjecucion_remitente;
	@Value("${ws.envioCorreo.detalleEjecucion.destinatario}")
	public String ws_envioCorreo_detalleEjecucion_destinatario;
	@Value("${ws.envioCorreo.detalleEjecucion.htmlFlag}")
	public String ws_envioCorreo_detalleEjecucion_htmlFlag;
	@Value("${ws.envioCorreo.detalleEjecucion.mensaje}")
	public String ws_envioCorreo_detalleEjecucion_mensaje;
	
	
	@Value("${ws.procesaAjusteMasivo.ajuste.url}")
	public String ws_procesaAjusteMasivo_ajuste_url;
	@Value("${ws.procesaAjusteMasivo.ajuste.timeout}")
	public int ws_procesaAjusteMasivo_ajuste_timeout;
	@Value("${ws.procesaAjusteMasivo.ajuste.connection.timeout}")
	public int ws_procesaAjusteMasivo_ajuste_connection_timeout;
		
}
