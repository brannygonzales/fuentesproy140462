package pe.com.claro.oss.generar.ajuste.masivo.bean;

public class DetalleEjecucionBean {

	private String docReferencia;
	private String nroDocAjuste;
	private String tipificado;
	private String comentario;
	
	public String getDocReferencia() {
		return docReferencia;
	}
	public void setDocReferencia(String docReferencia) {
		this.docReferencia = docReferencia;
	}
	public String getNroDocAjuste() {
		return nroDocAjuste;
	}
	public void setNroDocAjuste(String nroDocAjuste) {
		this.nroDocAjuste = nroDocAjuste;
	}
	public String getTipificado() {
		return tipificado;
	}
	public void setTipificado(String tipificado) {
		this.tipificado = tipificado;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
		
}
