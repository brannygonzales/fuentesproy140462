package pe.com.claro.oss.generar.ajuste.masivo.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pe.com.claro.oss.generar.ajuste.masivo.bean.AjusteBean;
import pe.com.claro.oss.generar.ajuste.masivo.util.Constantes;

public class CursorDatos1Mapper implements RowMapper<AjusteBean>{
	
	@Override
	public AjusteBean mapRow(ResultSet rs, int numeroFila) throws SQLException {
		AjusteBean objAjusteBean = new AjusteBean();
		objAjusteBean.setId(rs.getInt("TAMCI_ID"));
		objAjusteBean.setProcesoId(rs.getInt("TAMCI_IDPROCESO"));
//		objAjusteBean.setProcesoId(3698);
		objAjusteBean.setDocReferencia(rs.getString("TAMCV_DOCREFERENCIA"));
		objAjusteBean.setTipoSpool(rs.getString("TAMCI_TIPOSPOOLHP"));
		objAjusteBean.setTipoNotificacion(rs.getString("TAMCV_TIPONOT"));
		objAjusteBean.setNombreArchivo(rs.getString("TAMCV_ARCHIVO"));
		objAjusteBean.setCorreoDestino(rs.getString("TAMCV_CORREODESTINO"));
		
		if(rs.getString("TAMCV_ENVIARPFACT").equals(Constantes.AJUSTE_PRIORIZADO))
			objAjusteBean.setPriorizado(true);
		
		return objAjusteBean;
	}
	

}
