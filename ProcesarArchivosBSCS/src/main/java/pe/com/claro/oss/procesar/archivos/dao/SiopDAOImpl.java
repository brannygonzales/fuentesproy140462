package pe.com.claro.oss.procesar.archivos.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.com.claro.oss.procesar.archivos.bean.AjusteBean;
import pe.com.claro.oss.procesar.archivos.bean.AsientoContableBean;
import pe.com.claro.oss.procesar.archivos.bean.DetalleEjecucionBean;
import pe.com.claro.oss.procesar.archivos.mapper.CursorDatos1Mapper;
import pe.com.claro.oss.procesar.archivos.mapper.CursorDatos2Mapper;
import pe.com.claro.oss.procesar.archivos.mapper.CursorDatos3Mapper;
import pe.com.claro.oss.procesar.archivos.mapper.CursorDatos4Mapper;
import pe.com.claro.oss.procesar.archivos.util.PropertiesExternos;
import pe.com.claro.oss.procesar.archivos.util.Util;

@Repository
public class SiopDAOImpl implements SiopDAO {
	
	private static Logger logger = Logger.getLogger(SiopDAOImpl.class);
	
	@Autowired
	private PropertiesExternos propertiesExterno;
	
	@Autowired
	@Qualifier("siopDS")
	private DataSource siopDS;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AjusteBean> obtenerAjustesCabeceraHistorico(String mensajeTransaccion) {
		List<AjusteBean> listaAjustes = null;
		String sp = Util.getProcedure(propertiesExterno.db_siop_owner, propertiesExterno.db_siop_pckAjusteMasivos, propertiesExterno.db_siop_spObtenerAjustesCabeceraHistorico);
				
		try {
			mensajeTransaccion = mensajeTransaccion + "[obtenerAjustesCabeceraHistorico] ";
			log(mensajeTransaccion, " Inicio PROCEDURE: [" + sp + "]");

			SimpleJdbcCall objJdbcCall = new SimpleJdbcCall(siopDS)
										 	.withoutProcedureColumnMetaDataAccess()
											.withSchemaName(propertiesExterno.db_siop_owner)
											.withCatalogName(propertiesExterno.db_siop_pckAjusteMasivos)
											.withProcedureName(propertiesExterno.db_siop_spObtenerAjustesCabeceraHistorico)
											.declareParameters(new SqlOutParameter("PO_CURSOR", OracleTypes.CURSOR, new CursorDatos1Mapper() ),
															   new SqlOutParameter("PO_CODERROR", OracleTypes.VARCHAR),
															   new SqlOutParameter("PO_DESERROR", OracleTypes.VARCHAR) );
			
			objJdbcCall.getJdbcTemplate().setQueryTimeout(propertiesExterno.db_siop_connection_timeout);
			Map<String, Object> result = objJdbcCall.execute();
			
			listaAjustes = (List<AjusteBean>) result.get("PO_CURSOR");
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CURSOR:" + listaAjustes.size() + " registros devueltos]");
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CODERROR:" + result.get("PO_CODERROR").toString() + "]");
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_DESERROR:" + result.get("PO_DESERROR").toString() + "]");
		}
		catch (UncategorizedSQLException e) {
			logException(mensajeTransaccion, "El SP [" + sp + "] no devolvio algún dato correctamente");
		}
		catch (Exception e) {
			logException(mensajeTransaccion, e);
		}
		finally {
			log(mensajeTransaccion, " Fin PROCEDURE: [" + sp + "]");
		}
		
		return listaAjustes;
	}
			
	@SuppressWarnings("unchecked")
	@Override
	public List<String> generarSpool(String mensajeTransaccion, int procesoId, String tipo) {
		List<String> listaTrama = null;
		String sp = Util.getProcedure(propertiesExterno.db_siop_owner, propertiesExterno.db_siop_pckAjusteMasivos, propertiesExterno.db_siop_spGenerarSpool);
				
		try {
			mensajeTransaccion = mensajeTransaccion + "[generarSpool] ";
			
			log(mensajeTransaccion, " Inicio PROCEDURE: [" + sp + "]");
			log(mensajeTransaccion, "PARAMETROS [INPUT]: [PI_ID_PROCESO:" + procesoId + "]");
			log(mensajeTransaccion, "PARAMETROS [INPUT]: [PI_TIPO:" + tipo + "]");
			
			SimpleJdbcCall objJdbcCall = new SimpleJdbcCall(siopDS)
										 	.withoutProcedureColumnMetaDataAccess()
											.withSchemaName(propertiesExterno.db_siop_owner)
											.withCatalogName(propertiesExterno.db_siop_pckAjusteMasivos)
											.withProcedureName(propertiesExterno.db_siop_spGenerarSpool)
											.declareParameters(new SqlParameter("PI_ID_PROCESO", OracleTypes.NUMBER),
															   new SqlParameter("PI_TIPO", OracleTypes.VARCHAR),
															   new SqlOutParameter("PO_CURSOR", OracleTypes.CURSOR, new CursorDatos2Mapper() ),
															   new SqlOutParameter("PO_CODERROR", OracleTypes.VARCHAR),
															   new SqlOutParameter("PO_DESERROR", OracleTypes.VARCHAR));
			
			SqlParameterSource objParametrosIN = new MapSqlParameterSource().addValue("PI_ID_PROCESO", procesoId)
																			.addValue("PI_TIPO", tipo);
										
			objJdbcCall.getJdbcTemplate().setQueryTimeout(propertiesExterno.db_siop_connection_timeout);
			Map<String, Object> result = objJdbcCall.execute(objParametrosIN);
			
			listaTrama = (List<String>) result.get("PO_CURSOR");
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CURSOR:" + listaTrama.size() + " registro(s) devuelto(s)]");
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CODERROR:" + result.get("PO_CODERROR").toString() + "]");
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_DESERROR:" + result.get("PO_DESERROR").toString() + "]");
		}
		catch (UncategorizedSQLException e) {
			logException(mensajeTransaccion, "El SP [" + sp + "] no devolvio algun dato correctamente");
		}
		catch (Exception e) {
			logException(mensajeTransaccion, e);
		}
		finally {
			log(mensajeTransaccion, " Fin PROCEDURE: [" + sp + "]");
		}
		
		return listaTrama;
	}
		
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, List<AsientoContableBean>> obtenerAsientoContable(String mensajeTransaccion, int procesoId) {
		Map<String, List<AsientoContableBean>> mapAsientoContable = new HashMap<>();
		List<AsientoContableBean> listaAsientoContableNC = null, listaAsientoContableND = null, listaAsientoContableREC = null;
		String sp = Util.getProcedure(propertiesExterno.db_siop_owner, propertiesExterno.db_siop_pckAjusteMasivos, propertiesExterno.db_siop_spObtenerAsientoContable);
				
		try {
			mensajeTransaccion = mensajeTransaccion + "[obtenerAsientoContable] ";
			
			log(mensajeTransaccion, " Inicio PROCEDURE: [" + sp + "]");
			log(mensajeTransaccion, "PARAMETROS [INPUT]: [PI_ID_PROCESO:" + procesoId + "]");
			
			SimpleJdbcCall objJdbcCall = new SimpleJdbcCall(siopDS)
										 	.withoutProcedureColumnMetaDataAccess()
											.withSchemaName(propertiesExterno.db_siop_owner)
											. withCatalogName(propertiesExterno.db_siop_pckAjusteMasivos)
											.withProcedureName(propertiesExterno.db_siop_spObtenerAsientoContable)
											.declareParameters(new SqlParameter("PI_ID_PROCESO", OracleTypes.INTEGER),
															   new SqlOutParameter("PO_CURSOR_NC", OracleTypes.CURSOR, new CursorDatos3Mapper() ),
															   new SqlOutParameter("PO_CURSOR_ND", OracleTypes.CURSOR, new CursorDatos3Mapper() ),
															   new SqlOutParameter("PO_CURSOR_REC", OracleTypes.CURSOR, new CursorDatos3Mapper() ),
															   new SqlOutParameter("PO_CODERROR", OracleTypes.VARCHAR),
															   new SqlOutParameter("PO_DESERROR", OracleTypes.VARCHAR));
			
			SqlParameterSource objParametrosIN = new MapSqlParameterSource().addValue("PI_ID_PROCESO", procesoId);
										
			objJdbcCall.getJdbcTemplate().setQueryTimeout(propertiesExterno.db_siop_connection_timeout);
			Map<String, Object> result = objJdbcCall.execute(objParametrosIN);
			
			listaAsientoContableNC = (List<AsientoContableBean>) result.get("PO_CURSOR_NC");
			listaAsientoContableND = (List<AsientoContableBean>) result.get("PO_CURSOR_ND");
			listaAsientoContableREC = (List<AsientoContableBean>) result.get("PO_CURSOR_REC");
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CURSOR_NC:" + listaAsientoContableNC.size() + " registro(s) devuelto(s)]");
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CURSOR_ND:" + listaAsientoContableND.size() + " registro(s) devuelto(s)]");
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CURSOR_REC:" + listaAsientoContableREC.size() + " registro(s) devuelto(s)]");
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CODERROR:" + result.get("PO_CODERROR").toString() + "]");
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_DESERROR:" + result.get("PO_DESERROR").toString() + "]");
			
			mapAsientoContable.put("NC", listaAsientoContableNC);
			mapAsientoContable.put("ND", listaAsientoContableND);
			mapAsientoContable.put("REC", listaAsientoContableREC);
		}
		catch (UncategorizedSQLException e) {
			logException(mensajeTransaccion, "El SP [" + sp + "] no devolvio algun dato correctamente");
		}
		catch (Exception e) {
			logException(mensajeTransaccion, e);
		}
		finally {
			log(mensajeTransaccion, " Fin PROCEDURE: [" + sp + "]");
		}
		
		return mapAsientoContable;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<String> obtenerRegistroVenta(String mensajeTransaccion, int procesoId) {
		List<String> listaTrama = null;
		String sp = Util.getProcedure(propertiesExterno.db_siop_owner, propertiesExterno.db_siop_pckAjusteMasivos, propertiesExterno.db_siop_spObtenerRegistroVentas);
				
		try {
			mensajeTransaccion = mensajeTransaccion + "[obtenerRegistroVenta] ";
			
			log(mensajeTransaccion, " Inicio PROCEDURE: [" + sp + "]");
			log(mensajeTransaccion, "PARAMETROS [INPUT]: [PI_ID_PROCESO:" + procesoId + "]");
			
			SimpleJdbcCall objJdbcCall = new SimpleJdbcCall(siopDS)
										 	.withoutProcedureColumnMetaDataAccess()
											.withSchemaName(propertiesExterno.db_siop_owner)
											. withCatalogName(propertiesExterno.db_siop_pckAjusteMasivos)
											.withProcedureName(propertiesExterno.db_siop_spObtenerRegistroVentas)
											.declareParameters(new SqlParameter("PI_ID_PROCESO", OracleTypes.INTEGER),
															   new SqlOutParameter("PO_CURSOR", OracleTypes.CURSOR, new CursorDatos2Mapper() ),
															   new SqlOutParameter("PO_CODERROR", OracleTypes.VARCHAR),
															   new SqlOutParameter("PO_DESERROR", OracleTypes.VARCHAR));
			
			SqlParameterSource objParametrosIN = new MapSqlParameterSource().addValue("PI_ID_PROCESO", procesoId);
										
			objJdbcCall.getJdbcTemplate().setQueryTimeout(propertiesExterno.db_siop_connection_timeout);
			Map<String, Object> result = objJdbcCall.execute(objParametrosIN);
			
			listaTrama = (List<String>) result.get("PO_CURSOR");
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CURSOR:" + listaTrama.size() + " registro(s) devuelto(s)]");
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CODERROR:" + result.get("PO_CODERROR").toString() + "]");
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_DESERROR:" + result.get("PO_DESERROR").toString() + "]");
		}
		catch (UncategorizedSQLException e) {
			logException(mensajeTransaccion, "El SP [" + sp + "] no devolvio algun dato correctamente");
		}
		catch (Exception e) {
			logException(mensajeTransaccion, e);
		}
		finally {
			log(mensajeTransaccion, " Fin PROCEDURE: [" + sp + "]");
		}
		
		return listaTrama;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public DetalleEjecucionBean obtenerDetalleEjecucionHistorico(String mensajeTransaccion, int procesoId, String tipoAjuste, String estadoSpool, String estadoRegVenta) {
		DetalleEjecucionBean objDetalleEjecucion = null;
		String sp = Util.getProcedure(propertiesExterno.db_siop_owner, propertiesExterno.db_siop_pckAjusteMasivos, propertiesExterno.db_siop_spObtenerDetalleProcesoHistorico);
				
		try {
			mensajeTransaccion = mensajeTransaccion + "[obtenerDetalleEjecucionHistorico] ";
			
			log(mensajeTransaccion, " Inicio PROCEDURE: [" + sp + "]");
			log(mensajeTransaccion, "PARAMETROS [INPUT]: [PI_ID_PROCESO:" + procesoId + "]");
			log(mensajeTransaccion, "PARAMETROS [INPUT]: [PI_TIPO_AJUSTE:" + tipoAjuste + "]");
			log(mensajeTransaccion, "PARAMETROS [INPUT]: [PI_ESTADO_SPOOL:" + estadoSpool + "]");
//			log(mensajeTransaccion, "PARAMETROS [INPUT]: [PI_ESTADO_ASIENTO:" + estadoAsientoContable + "]");
			log(mensajeTransaccion, "PARAMETROS [INPUT]: [PI_ESTADO_REG_VENTA:" + estadoRegVenta + "]");
			
			SimpleJdbcCall objJdbcCall = new SimpleJdbcCall(siopDS)
										 	.withoutProcedureColumnMetaDataAccess()
											.withSchemaName(propertiesExterno.db_siop_owner)
											. withCatalogName(propertiesExterno.db_siop_pckAjusteMasivos)
											.withProcedureName(propertiesExterno.db_siop_spObtenerDetalleProcesoHistorico)
											.declareParameters(new SqlParameter("PI_ID_PROCESO", OracleTypes.INTEGER),
															   new SqlParameter("PI_TIPO_AJUSTE", OracleTypes.VARCHAR),
															   new SqlParameter("PI_ESTADO_SPOOL", OracleTypes.VARCHAR),
//															   new SqlParameter("PI_ESTADO_ASIENTO", OracleTypes.VARCHAR),
															   new SqlParameter("PI_ESTADO_REG_VENTA", OracleTypes.VARCHAR),
															   new SqlOutParameter("PO_CURSOR", OracleTypes.CURSOR, new CursorDatos4Mapper() ),
															   new SqlOutParameter("PO_CODERROR", OracleTypes.VARCHAR),
															   new SqlOutParameter("PO_DESERROR", OracleTypes.VARCHAR));
			
			SqlParameterSource objParametrosIN = new MapSqlParameterSource().addValue("PI_ID_PROCESO", procesoId)
																			.addValue("PI_TIPO_AJUSTE", tipoAjuste)
																			.addValue("PI_ESTADO_SPOOL", estadoSpool)
//																			.addValue("PI_ESTADO_ASIENTO", estadoAsientoContable)
																			.addValue("PI_ESTADO_REG_VENTA", estadoRegVenta);
										
			objJdbcCall.getJdbcTemplate().setQueryTimeout(propertiesExterno.db_siop_connection_timeout);
			Map<String, Object> result = objJdbcCall.execute(objParametrosIN);
			
			List<DetalleEjecucionBean> listaDetalleEjecucion = (List<DetalleEjecucionBean>) result.get("PO_CURSOR");
			if(listaDetalleEjecucion != null && listaDetalleEjecucion.size() > 0)
				objDetalleEjecucion = listaDetalleEjecucion.get(0);
				
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CURSOR:" + listaDetalleEjecucion.size() + " registro(s) devuelto(s)]");
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CODERROR:" + result.get("PO_CODERROR").toString() + "]");
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_DESERROR:" + result.get("PO_DESERROR").toString() + "]");
		}
		catch (UncategorizedSQLException e) {
			logException(mensajeTransaccion, "El SP [" + sp + "] no devolvio algun dato correctamente");
		}
		catch (Exception e) {
			logException(mensajeTransaccion, e);
		}
		finally {
			log(mensajeTransaccion, " Fin PROCEDURE: [" + sp + "]");
		}
		
		return objDetalleEjecucion;
	}

	@Override
	public void guardarAsientoContable(String mensajeTransaccion, int procesoId, String tipoAjuste, String estadoAsientoContable) {
		String sp = Util.getProcedure(propertiesExterno.db_siop_owner, propertiesExterno.db_siop_pckAjusteMasivos, propertiesExterno.db_siop_spGuardarAsientoContable);
		
		try {
			mensajeTransaccion = mensajeTransaccion + "[guardarAsientoContable] ";
			log(mensajeTransaccion, " Inicio PROCEDURE: [" + sp + "]");
			log(mensajeTransaccion, "PARAMETROS [INPUT]: [PI_ID_PROCESO: " + procesoId + "]");
			log(mensajeTransaccion, "PARAMETROS [INPUT]: [PI_TIPO_AJUSTE: " + tipoAjuste + "]");
			log(mensajeTransaccion, "PARAMETROS [INPUT]: [PI_ESTADO_ASIENTO: " + estadoAsientoContable + "]");
			
			SimpleJdbcCall objJdbcCall = new SimpleJdbcCall(siopDS)
				 	.withoutProcedureColumnMetaDataAccess()
					.withSchemaName(propertiesExterno.db_siop_owner)
					.withCatalogName(propertiesExterno.db_siop_pckAjusteMasivos)
					.withProcedureName(propertiesExterno.db_siop_spGuardarAsientoContable)
					.declareParameters(new SqlParameter("PI_ID_PROCESO", OracleTypes.INTEGER),
									   new SqlParameter("PI_TIPO_AJUSTE", OracleTypes.VARCHAR),
									   new SqlParameter("PI_ESTADO_ASIENTO", OracleTypes.VARCHAR),
									   new SqlOutParameter("PO_CODERROR", OracleTypes.VARCHAR),
									   new SqlOutParameter("PO_DESERROR", OracleTypes.VARCHAR));
			
			SqlParameterSource objParametrosIN = new MapSqlParameterSource()
					.addValue("PI_ID_PROCESO", procesoId)
					.addValue("PI_TIPO_AJUSTE", tipoAjuste)
					.addValue("PI_ESTADO_ASIENTO", estadoAsientoContable);
			
			objJdbcCall.getJdbcTemplate().setQueryTimeout(propertiesExterno.db_siop_connection_timeout);
			Map<String, Object> result = objJdbcCall.execute(objParametrosIN);
			
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_CODERROR: " + result.get("PO_CODERROR").toString() + "]");
			log(mensajeTransaccion, "PARAMETROS [OUTPUT]: [PO_DESERROR: " + result.get("PO_DESERROR").toString() + "]");
		} catch (UncategorizedSQLException e) {
			logException(mensajeTransaccion, "El SP [" + sp + "] no devolvio algun dato correctamente");
		} catch (Exception e) {
			logException(mensajeTransaccion, e);
		} finally {
			log(mensajeTransaccion, " Fin PROCEDURE: [" + sp + "]");
		}
	}
	
	private static void log(String mensajeTransaccion, String texto) {
		logger.info(mensajeTransaccion + texto);
	}
	
	private static void logException(String mensajeTransaccion, Exception e) {
		logger.error(mensajeTransaccion + "ERROR: [Exception] - [" + e.getMessage() + "] ", e);
	}
	
	private static void logException(String mensajeTransaccion, String error) {
		logger.error(mensajeTransaccion + "ERROR: " + error );
	}

}
