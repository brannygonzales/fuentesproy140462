package pe.com.claro.oss.procesar.archivos.service;

public interface ProcesarArchivosBSCSService {
	
	void run(String mensajeTransaccion, String idTransaction);

}
