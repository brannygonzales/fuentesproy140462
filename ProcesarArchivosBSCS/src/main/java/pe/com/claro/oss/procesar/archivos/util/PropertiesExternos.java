package pe.com.claro.oss.procesar.archivos.util;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PropertiesExternos {

	@Value("${log4j.file.dir}")
	public String vLog4JDir;
	
	@Value("${db.siop}")
	public String db_siop;
	@Value("${db.siop.owner}")
	public String db_siop_owner;
	@Value("${db.siop.connection.timeout}")
	public int db_siop_connection_timeout;
	@Value("${db.siop.pckAjusteMasivos}")
	public String db_siop_pckAjusteMasivos;
	@Value("${db.siop.spObtenerAjustesCabeceraHistorico}")
	public String db_siop_spObtenerAjustesCabeceraHistorico;
	@Value("${db.siop.spGenerarSpool}")
	public String db_siop_spGenerarSpool;
	@Value("${db.siop.spObtenerAsientoContable}")
	public String db_siop_spObtenerAsientoContable;
	@Value("${db.siop.spObtenerRegistroVentas}")
	public String db_siop_spObtenerRegistroVentas;
	@Value("${db.siop.spObtenerDetalleProcesoHistorico}")
	public String db_siop_spObtenerDetalleProcesoHistorico;
	@Value("${db.siop.spGuardarAsientoContable}")
	public String db_siop_spGuardarAsientoContable;
			
	@Value("${ruta.archivos.generados}")
	public String ruta_archivos_generados;
	@Value("${ruta.registro.ventas}")
	public String ruta_registro_ventas;
		
	@Value("${ws.envioCorreo.wsdl}")
	public String ws_envioCorreo_wsdl;
	@Value("${ws.envioCorreo.auditRequest.codigoAplicacion}")
	public String ws_envioCorreo_auditRequest_codigoAplicacion;
	@Value("${ws.envioCorreo.auditRequest.usuarioAplicacion}")
	public String ws_envioCorreo_auditRequest_usuarioAplicacion;
	@Value("${ws.envioCorreo.spool.asunto}")
	public String ws_envioCorreo_spool_asunto;
	@Value("${ws.envioCorreo.spool.remitente}")
	public String ws_envioCorreo_spool_remitente;
	@Value("${ws.envioCorreo.spool.destinatario}")
	public String ws_envioCorreo_spool_destinatario;
	@Value("${ws.envioCorreo.spool.htmlFlag}")
	public String ws_envioCorreo_spool_htmlFlag;
	@Value("${ws.envioCorreo.spool.mensaje.asesor}")
	public String ws_envioCorreo_spool_mensaje_asesor;
	@Value("${ws.envioCorreo.spool.mensaje.distribucion}")
	public String ws_envioCorreo_spool_mensaje_distribucion;
	@Value("${ws.envioCorreo.spool.mensaje.repositorio}")
	public String ws_envioCorreo_spool_mensaje_repositorio;
	@Value("${ws.envioCorreo.detalleEjecucion.asunto}")
	public String ws_envioCorreo_detalleEjecucion_asunto;
	@Value("${ws.envioCorreo.detalleEjecucion.remitente}")
	public String ws_envioCorreo_detalleEjecucion_remitente;
	@Value("${ws.envioCorreo.detalleEjecucion.destinatario}")
	public String ws_envioCorreo_detalleEjecucion_destinatario;
	@Value("${ws.envioCorreo.detalleEjecucion.htmlFlag}")
	public String ws_envioCorreo_detalleEjecucion_htmlFlag;
	@Value("${ws.envioCorreo.detalleEjecucion.mensaje}")
	public String ws_envioCorreo_detalleEjecucion_mensaje;

	@Value("${ws.portalSap.wsdl}")
	public String ws_portalSap_wsdl;
	@Value("${ws.portalSap.auditRequest.nombreAplicacion}")
	public String ws_portalSap_auditRequest_nombreAplicacion;
	@Value("${ws.portalSap.auditRequest.usuarioAplicacion}")
	public String ws_portalSap_auditRequest_usuarioAplicacion;
	@Value("${ws.portalSap.version}")
	public String ws_portalSap_version;
	

	@Value("${fileServer.servidor}")
	public String fileServer_servidor;
	@Value("${fileServer.puerto}")
	public int fileServer_puerto;
	@Value("${fileServer.usuario}")
	public String fileServer_usuario;
	@Value("${fileServer.password}")
	public String fileServer_password;
	@Value("${fileServer.timeout}")
	public int fileServer_timeout;
	@Value("${fileServer.protocolo}")
	public String fileServer_protocolo;
	@Value("${fileServer.ruta.local}")
	public String fileServer_ruta_local;
	@Value("${fileServer.ruta.remota}")
	public String fileServer_ruta_remota;
	
}
