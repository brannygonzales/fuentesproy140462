package pe.com.claro.oss.procesar.archivos.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pe.com.claro.oss.procesar.archivos.bean.DetalleEjecucionBean;

public class CursorDatos4Mapper implements RowMapper<DetalleEjecucionBean>{

	@Override
	public DetalleEjecucionBean mapRow(ResultSet rs, int numeroFila) throws SQLException {
		DetalleEjecucionBean objDetalleEjecucionBean = new DetalleEjecucionBean();
		objDetalleEjecucionBean.setSpool(rs.getString("SPOOL"));
		objDetalleEjecucionBean.setAsientoContable(rs.getString("ASIENTO_CONTABLE"));
		objDetalleEjecucionBean.setRegistroVenta(rs.getString("REGISTRO_VENTA"));
		
		return objDetalleEjecucionBean;
	}
	
}