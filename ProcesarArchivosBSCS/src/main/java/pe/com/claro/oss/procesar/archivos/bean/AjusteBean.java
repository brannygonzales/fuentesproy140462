package pe.com.claro.oss.procesar.archivos.bean;

public class AjusteBean {


	private int procesoId;
	private boolean priorizado;
	private String tipoSpool;
	private String tipoNotificacion;
	private String nombreArchivo;
	private String estadoSpool;
	
	public int getProcesoId() {
		return procesoId;
	}
	public void setProcesoId(int procesoId) {
		this.procesoId = procesoId;
	}
	public boolean isPriorizado() {
		return priorizado;
	}
	public void setPriorizado(boolean priorizado) {
		this.priorizado = priorizado;
	}
	public String getTipoSpool() {
		return tipoSpool;
	}
	public void setTipoSpool(String tipoSpool) {
		this.tipoSpool = tipoSpool;
	}
	public String getTipoNotificacion() {
		return tipoNotificacion;
	}
	public void setTipoNotificacion(String tipoNotificacion) {
		this.tipoNotificacion = tipoNotificacion;
	}
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public String getEstadoSpool() {
		return estadoSpool;
	}
	public void setEstadoSpool(String estadoSpool) {
		this.estadoSpool = estadoSpool;
	}
	
}
