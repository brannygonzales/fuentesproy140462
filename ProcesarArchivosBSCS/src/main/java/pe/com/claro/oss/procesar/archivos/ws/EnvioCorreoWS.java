package pe.com.claro.oss.procesar.archivos.ws;

import pe.com.claro.eai.util.enviocorreo.types.EnviarCorreoRequest;
import pe.com.claro.eai.util.enviocorreo.types.EnviarCorreoResponse;

public interface EnvioCorreoWS {
	
	public EnviarCorreoResponse enviarCorreo (String mensajeTransaccion, EnviarCorreoRequest request);

}
