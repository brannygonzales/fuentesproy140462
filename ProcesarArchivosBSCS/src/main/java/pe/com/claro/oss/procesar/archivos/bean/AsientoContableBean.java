package pe.com.claro.oss.procesar.archivos.bean;

public class AsientoContableBean {
	
    private String tipoRegistro;
    private String fechaDocumento;
    private String claseDocumento;
    private String sociedad;
    private String fechaContabilizacion;
    private String fechaConversion;
    private String claveMoneda;
    private String numeroDocRef;
    private String textoCabecera;
    private String claveContabilizacion;
    private String cuenta;
    private String indicador;
    private String importe;
    private String claveCondicion;
    private String fechaBase;
    private String claveBloqueo;
    private String numeroAsignacion;
    private String textoPosicion;
    private String centroCoste;
    private String centroBeneficio;
    private String numeroOrden;
    private String tipoCambio;
    private String calcImpuesto;
    private String indIVA;
    private String indRetencion;
    private String posicionPresupuestaria;
    private String mesContable;
    private String viaPago;
    private String centroGestor;
    private String division;
    private String importeLocal;
    private String importeCtaMayor;
    private String elementoPlanEstructura;

    public String getTipoRegistro() {
		return tipoRegistro;
	}
	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}
	public String getFechaDocumento() {
		return fechaDocumento;
	}
	public void setFechaDocumento(String fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}
	public String getClaseDocumento() {
		return claseDocumento;
	}
	public void setClaseDocumento(String claseDocumento) {
		this.claseDocumento = claseDocumento;
	}
	public String getSociedad() {
		return sociedad;
	}
	public void setSociedad(String sociedad) {
		this.sociedad = sociedad;
	}
	public String getFechaContabilizacion() {
		return fechaContabilizacion;
	}
	public void setFechaContabilizacion(String fechaContabilizacion) {
		this.fechaContabilizacion = fechaContabilizacion;
	}
	public String getFechaConversion() {
		return fechaConversion;
	}
	public void setFechaConversion(String fechaConversion) {
		this.fechaConversion = fechaConversion;
	}
	public String getClaveMoneda() {
		return claveMoneda;
	}
	public void setClaveMoneda(String claveMoneda) {
		this.claveMoneda = claveMoneda;
	}
	public String getNumeroDocRef() {
		return numeroDocRef;
	}
	public void setNumeroDocRef(String numeroDocRef) {
		this.numeroDocRef = numeroDocRef;
	}
	public String getTextoCabecera() {
		return textoCabecera;
	}
	public void setTextoCabecera(String textoCabecera) {
		this.textoCabecera = textoCabecera;
	}
	public String getClaveContabilizacion() {
		return claveContabilizacion;
	}
	public void setClaveContabilizacion(String claveContabilizacion) {
		this.claveContabilizacion = claveContabilizacion;
	}
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	public String getIndicador() {
		return indicador;
	}
	public void setIndicador(String indicador) {
		this.indicador = indicador;
	}
	public String getImporte() {
		return importe;
	}
	public void setImporte(String importe) {
		this.importe = importe;
	}
	public String getClaveCondicion() {
		return claveCondicion;
	}
	public void setClaveCondicion(String claveCondicion) {
		this.claveCondicion = claveCondicion;
	}
	public String getFechaBase() {
		return fechaBase;
	}
	public void setFechaBase(String fechaBase) {
		this.fechaBase = fechaBase;
	}
	public String getClaveBloqueo() {
		return claveBloqueo;
	}
	public void setClaveBloqueo(String claveBloqueo) {
		this.claveBloqueo = claveBloqueo;
	}
	public String getNumeroAsignacion() {
		return numeroAsignacion;
	}
	public void setNumeroAsignacion(String numeroAsignacion) {
		this.numeroAsignacion = numeroAsignacion;
	}
	public String getTextoPosicion() {
		return textoPosicion;
	}
	public void setTextoPosicion(String textoPosicion) {
		this.textoPosicion = textoPosicion;
	}
	public String getCentroCoste() {
		return centroCoste;
	}
	public void setCentroCoste(String centroCoste) {
		this.centroCoste = centroCoste;
	}
	public String getCentroBeneficio() {
		return centroBeneficio;
	}
	public void setCentroBeneficio(String centroBeneficio) {
		this.centroBeneficio = centroBeneficio;
	}
	public String getNumeroOrden() {
		return numeroOrden;
	}
	public void setNumeroOrden(String numeroOrden) {
		this.numeroOrden = numeroOrden;
	}
	public String getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public String getCalcImpuesto() {
		return calcImpuesto;
	}
	public void setCalcImpuesto(String calcImpuesto) {
		this.calcImpuesto = calcImpuesto;
	}
	public String getIndIVA() {
		return indIVA;
	}
	public void setIndIVA(String indIVA) {
		this.indIVA = indIVA;
	}
	public String getIndRetencion() {
		return indRetencion;
	}
	public void setIndRetencion(String indRetencion) {
		this.indRetencion = indRetencion;
	}
	public String getPosicionPresupuestaria() {
		return posicionPresupuestaria;
	}
	public void setPosicionPresupuestaria(String posicionPresupuestaria) {
		this.posicionPresupuestaria = posicionPresupuestaria;
	}
	public String getMesContable() {
		return mesContable;
	}
	public void setMesContable(String mesContable) {
		this.mesContable = mesContable;
	}
	public String getViaPago() {
		return viaPago;
	}
	public void setViaPago(String viaPago) {
		this.viaPago = viaPago;
	}
	public String getCentroGestor() {
		return centroGestor;
	}
	public void setCentroGestor(String centroGestor) {
		this.centroGestor = centroGestor;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getImporteLocal() {
		return importeLocal;
	}
	public void setImporteLocal(String importeLocal) {
		this.importeLocal = importeLocal;
	}
	public String getImporteCtaMayor() {
		return importeCtaMayor;
	}
	public void setImporteCtaMayor(String importeCtaMayor) {
		this.importeCtaMayor = importeCtaMayor;
	}
	public String getElementoPlanEstructura() {
		return elementoPlanEstructura;
	}
	public void setElementoPlanEstructura(String elementoPlanEstructura) {
		this.elementoPlanEstructura = elementoPlanEstructura;
	}

}
