package pe.com.claro.oss.procesar.archivos.util;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.vfs2.FileSystemOptions;
import org.apache.commons.vfs2.provider.sftp.SftpClientFactory;
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.Session;

@Component
public class Util {
	
	private static Logger logger = Logger.getLogger(Util.class);
		
	public static String getProcedure(String owner, String pck, String sp) {
		StringBuilder sb = new StringBuilder();
		sb.append(owner).append(Constantes.SEPARADOR_PUNTO).append(pck).append(Constantes.SEPARADOR_PUNTO).append(sp);
		return sb.toString();
	}
	
	public static String getIPHost() {
		String hostAddress;
		try {
			hostAddress = Inet4Address.getLocalHost().getHostAddress();
		} 
		catch (UnknownHostException e) {
			hostAddress = Constantes.TEXTO_VACIO;
		}
		return hostAddress;
	}
	
	public static String getFechaSistema(String formato) {
		DateFormat dateFormat = new SimpleDateFormat(formato);
		return dateFormat.format(new Date());
	}
	
	public static String crearCarpeta(String mensajeTransaccion, String urlCarpeta) {
		File carpeta = new File(urlCarpeta);
		if(carpeta.mkdirs()) { 
			logger.warn(mensajeTransaccion + "Carpeta creada : " + urlCarpeta);
			return urlCarpeta;
		}	
		else { 
			logger.warn(mensajeTransaccion + "No se pudo crear la carepta : " + urlCarpeta);
			return null;
		}	
	}
	
	public static void eliminarCarpeta(String mensajeTransaccion, File archivo) {
		if (!archivo.exists()) 
			return;

	    if (archivo.isDirectory()){ 
	        for (File file : archivo.listFiles()) 
	        	eliminarCarpeta(null, file);
	    }
	    
	    if(archivo.delete()) {  
	    	if(mensajeTransaccion != null)
	    		logger.info(mensajeTransaccion + "Archivo eliminado : " + archivo.getPath());
	    }
	    else {
			if(mensajeTransaccion != null)
				logger.warn(mensajeTransaccion + "No se pudo eliminar el archivo : " + archivo.getPath());
		}	
	}
	
	public static boolean generarArchivoZip(String mensajeTransaccion, String urlCarpeta, String nombreArchivoZip, String idTransaction, int totalNroSpool) {
		try {
			File carpetaComprimir = new File(urlCarpeta);
		
			if (carpetaComprimir.exists()) {
				File[] listaArchivos = carpetaComprimir.listFiles();
				logger.info(mensajeTransaccion + "Número de archivos a comprimir: " + listaArchivos.length);
				
				if(listaArchivos.length != totalNroSpool)
					return false;
				
				ZipOutputStream zous = new ZipOutputStream(new FileOutputStream( urlCarpeta + nombreArchivoZip));
				
				for (int i = 0; i < listaArchivos.length; i++) {
					logger.info(mensajeTransaccion + "Comprimiendo el archivo: " + listaArchivos[i].getName());
					ZipEntry archivoZip = new ZipEntry(listaArchivos[i].getName());
					zous.putNextEntry(archivoZip);
	
					FileInputStream fis = new FileInputStream(urlCarpeta + archivoZip.getName());
					int leer;
					byte[] buffer = new byte[1024];
					while (0 < (leer = fis.read(buffer))) {
						zous.write(buffer, 0, leer);
					}
					fis.close();
				}
				zous.closeEntry();
				zous.close();	
				logger.info(mensajeTransaccion + "Archivo zip creado: " + nombreArchivoZip);
			}
			else
				logger.warn(mensajeTransaccion + "La carpeta " + urlCarpeta + " no existe");
		} 
		catch (Exception e) {
			logger.error(mensajeTransaccion + "Error: ", e);
			return false;
		}
		return true;
	}
	
	public static boolean generarArchivo(String mensajeTransaccion, String urlCarpeta, String nombreArchivo, List<String> listaData) {
		try {
			try(FileWriter flwriter = new FileWriter(urlCarpeta + nombreArchivo)){
				try(BufferedWriter bfwriter = new BufferedWriter(flwriter)){
					for (String data : listaData) {
						bfwriter.write(data);
						bfwriter.newLine();
					}
					
					logger.info(mensajeTransaccion + "Archivo creado: " + nombreArchivo);
				}
			}
			return true;
 		} 
		catch (Exception e) {
			logger.error(mensajeTransaccion + "Error: ", e);
			return false;
		} 
	}
	
	public static byte[] convertirArchivoAByte(String mensajeTransaccion, String urlArchivo) {
		byte bytes[] = null;
		try (FileInputStream fis = new FileInputStream(new File(urlArchivo))) {
		    try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
		        byte[] buffer = new byte[1024];
		        int leer = -1;
		        while ((leer = fis.read(buffer)) != -1) {
		            baos.write(buffer, 0, leer);
		        }
		        bytes = baos.toByteArray();
		    } 
		} 
		catch (Exception e) {
			logger.error(mensajeTransaccion + "Error: ", e);
		}
		return bytes;
	}
	
	public static ChannelSftp conectarFileServer(String mensajeTransaccion, String servidor, String usuario, String password, int puerto, int timeOut, String tipoCanal) {
		try {
			logger.info(mensajeTransaccion + "Estableciendo conexión con el servidor : " + servidor);
			
			FileSystemOptions objFileSystemOptions = new FileSystemOptions();
			SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(objFileSystemOptions, Constantes.NO_MINUSCULA);

			Session objSesion = SftpClientFactory.createConnection(servidor, puerto, usuario.toCharArray(), password.toCharArray(), objFileSystemOptions);
			Channel objCanal = objSesion.openChannel(tipoCanal);
			objCanal.connect(timeOut);

			ChannelSftp objCanalSFTP = (ChannelSftp) objCanal;
			if(objCanalSFTP.isConnected()) {
				logger.info(mensajeTransaccion + "Conexión establecida.");
				return objCanalSFTP;
			}	
		} 
		catch (Exception e) {
			logger.error(mensajeTransaccion + "Error: ", e);
		} 
	
		return null;
	}

	public static void desconectarFileServer(String mensajeTransaccion, ChannelSftp objCanalSFTP) {
		try {
			logger.info(mensajeTransaccion + "Eliminando conexión con el servidor : " + objCanalSFTP.getSession().getHost());
			
			if (objCanalSFTP != null) { 
				objCanalSFTP.getSession().disconnect();
				objCanalSFTP.disconnect();
				objCanalSFTP.exit();
				logger.info(mensajeTransaccion + "Conexión eliminada.");
			}
		} 
		catch (Exception e) {
			logger.error(mensajeTransaccion + "Error: ", e);
		}
	}
	
	public static boolean enviarArchivoFileServer(String mensajeTransaccion, ChannelSftp objCanalSFTP, String rutaLocal, String rutaRemota, String nombreArchivo){
		try {
			logger.info(mensajeTransaccion + "Enviando el archivo " + nombreArchivo + " al servidor: " + objCanalSFTP.getSession().getHost());
			logger.info(mensajeTransaccion + "Ruta local: " + rutaLocal); 
			logger.info(mensajeTransaccion + "Ruta remota: " + rutaRemota);
			
			objCanalSFTP.cd(rutaRemota);
			File archivo = new File(rutaLocal + nombreArchivo);
			FileInputStream fis = new FileInputStream(archivo);
			objCanalSFTP.put(fis, archivo.getName());
			fis.close();

	        logger.info(mensajeTransaccion + "Archivo " + nombreArchivo + " enviado.");
			return true;
		} 
		catch (Exception e) {
			logger.error(mensajeTransaccion + "Error: ", e);
		} 
		
		return false;
	}
	
	public static boolean copiarArchivo(String mensajeTransaccion, String archivoOrigen, String archivoDestino) {
        File origin = new File(archivoOrigen);
        File destination = new File(archivoDestino);
        if (origin.exists()) {
            try {
                InputStream in = new FileInputStream(origin);
                OutputStream out = new FileOutputStream(destination);
                // We use a buffer for the copy (Usamos un buffer para la copia).
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                in.close();
                out.close();
                return true;
            } catch (IOException ioe) {
            	logger.error(mensajeTransaccion + "Error: ", ioe);
                return false;
            }
        } else {
            return false;
        }
    }
	
}