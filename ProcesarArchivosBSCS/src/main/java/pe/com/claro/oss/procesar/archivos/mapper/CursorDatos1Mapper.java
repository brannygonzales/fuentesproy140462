package pe.com.claro.oss.procesar.archivos.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pe.com.claro.oss.procesar.archivos.bean.AjusteBean;
import pe.com.claro.oss.procesar.archivos.util.Constantes;

public class CursorDatos1Mapper implements RowMapper<AjusteBean>{
	
	@Override
	public AjusteBean mapRow(ResultSet rs, int numeroFila) throws SQLException {
		AjusteBean objAjusteBean = new AjusteBean();
		objAjusteBean.setProcesoId(rs.getInt("HAMCI_IDPROCESO"));
		objAjusteBean.setTipoSpool(rs.getString("HAMCI_TIPOSPOOLHP"));
		objAjusteBean.setTipoNotificacion(rs.getString("HAMCV_TIPONOT"));
		objAjusteBean.setNombreArchivo(rs.getString("HAMCV_ARCHIVO"));
		objAjusteBean.setEstadoSpool(rs.getString("HAMCI_ESTSPOOLHP"));
				
		if(rs.getString("HAMCV_ENVIARPFACT").equals(Constantes.AJUSTE_PRIORIZADO))
			objAjusteBean.setPriorizado(true);
		
		return objAjusteBean;
	}
	
}
