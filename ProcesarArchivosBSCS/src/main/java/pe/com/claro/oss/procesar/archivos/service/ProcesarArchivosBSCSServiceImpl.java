package pe.com.claro.oss.procesar.archivos.service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.claro.eai.util.enviocorreo.types.ArchivoAdjunto;
import pe.com.claro.eai.util.enviocorreo.types.AuditTypeRequest;
import pe.com.claro.eai.util.enviocorreo.types.EnviarCorreoRequest;
import pe.com.claro.eai.util.enviocorreo.types.EnviarCorreoResponse;
import pe.com.claro.eai.util.enviocorreo.types.ListaArchivosAdjuntos;
import pe.com.claro.oss.procesar.archivos.bean.AjusteBean;
import pe.com.claro.oss.procesar.archivos.bean.AsientoContableBean;
import pe.com.claro.oss.procesar.archivos.bean.DetalleEjecucionBean;
import pe.com.claro.oss.procesar.archivos.dao.SiopDAO;
import pe.com.claro.oss.procesar.archivos.util.Constantes;
import pe.com.claro.oss.procesar.archivos.util.Constantes.EstadosProceso;
import pe.com.claro.oss.procesar.archivos.util.Constantes.EstadosRegistroVenta;
import pe.com.claro.oss.procesar.archivos.util.Constantes.EstadosSpool;
import pe.com.claro.oss.procesar.archivos.util.Constantes.TipoNotificacion;
import pe.com.claro.oss.procesar.archivos.util.Constantes.TipoSpool;
import pe.com.claro.oss.procesar.archivos.util.PropertiesExternos;
import pe.com.claro.oss.procesar.archivos.util.Util;
import pe.com.claro.oss.procesar.archivos.ws.EnvioCorreoWS;
import pe.com.claro.oss.procesar.archivos.ws.PortalSapWS;
import pe.com.claro.soa.message.portal.AsientoBean;
import pe.com.claro.soa.message.portal.GenerarAsientoRequest;
import pe.com.claro.soa.message.portal.GenerarAsientoRequest.ListaAsiento;
import pe.com.claro.soa.message.portal.GenerarAsientoResponse;
import pe.com.claro.xsd.comun.AuditType;


@Service
public class ProcesarArchivosBSCSServiceImpl implements ProcesarArchivosBSCSService {

	private static Logger logger = Logger.getLogger(ProcesarArchivosBSCSServiceImpl.class);
	
	@Autowired
	private PropertiesExternos propertiesExterno;
	@Autowired
	private SiopDAO siopDAO;
	@Autowired
	private EnvioCorreoWS envioCorreoWS;
	@Autowired
	private PortalSapWS portalSapWS;
	
	private String mensajeTransaccion;
	private String idTransaction;
	private String idAutogenerado;
	private Map<EstadosProceso, String> listaEstados;
	private Map<String, String> mapTipoAjusteAsiento;
	
	@Override
	public void run (String mensajeTransaccion, String idTransaction) {
		this.mensajeTransaccion = mensajeTransaccion;
		this.idTransaction = idTransaction;
				
		List<AjusteBean> listaAjustes = siopDAO.obtenerAjustesCabeceraHistorico(mensajeTransaccion);
		if(listaAjustes == null || listaAjustes.size() == 0)
			return ;

		for(AjusteBean objAjusteBean : filtrarListaPorProcesoId(listaAjustes)) {
			try {
				listaEstados = new HashMap<EstadosProceso, String>();
				mapTipoAjusteAsiento = new HashMap<String, String>();
				idAutogenerado = Util.getFechaSistema(Constantes.FORMATO_YYYYMMDDHHMMSS);
				
				if(!objAjusteBean.isPriorizado()){ 
					if(generarSpool(objAjusteBean, listaAjustes)) 
						listaEstados.put(EstadosProceso.SPOOL_GENERADO, null);
				} else {
					if (EstadosSpool.GENERADO.name().equals(objAjusteBean.getEstadoSpool())) {
						listaEstados.put(EstadosProceso.SPOOL_GENERADO, null);
					}
				}
				registrarAsientoContable(objAjusteBean);
			} 
			catch (Exception e) {
				logger.error(mensajeTransaccion + "Error: ", e);
			}
			finally {
				enviarDetalleEjecucion(objAjusteBean);
				eliminarCarpetas();	
			}
		}			
	}
	
	private List<AjusteBean> filtrarListaPorProcesoId (List<AjusteBean> listaAjustes) {
		Set<Integer> listaSinValoresRepetidos = new HashSet<>(listaAjustes.size());
		List<AjusteBean> listaFiltrada = new ArrayList<AjusteBean>();
		for(AjusteBean obj : listaAjustes){
			if(listaSinValoresRepetidos.add(obj.getProcesoId()))
				listaFiltrada.add(obj);
		}
		return listaFiltrada;
	}
	
	private boolean generarSpool(AjusteBean objAjusteBean, List<AjusteBean> listaAjustes) {
		Map<String, List<String>> tramas = new HashMap<String, List<String>>();
		int totalNroSpool = 0;
		
		for(AjusteBean obj : listaAjustes) {
			if(objAjusteBean.getProcesoId() == obj.getProcesoId()) {
				
				if(obj.getTipoSpool().equals(TipoSpool.TRAMA_AJUSTE.name())) {
					totalNroSpool += Constantes.TOTAL_NRO_SPOOL_TRAMA_AJUSTE;
					tramas.putAll(generarSpoolTramaAjuste(objAjusteBean.getProcesoId()));
				}	
				if(obj.getTipoSpool().equals(TipoSpool.TRAMA_LARGA.name())) {
					totalNroSpool += Constantes.TOTAL_NRO_SPOOL_TRAMA_LARGA;
					tramas.putAll(generarSpoolTramaLarga(objAjusteBean.getProcesoId()));
				}
			}
		}
		
		log("Nro total de Spool obtenidos : " + tramas.size());
		if(totalNroSpool == 0 || tramas.size() != totalNroSpool) 
			return false;
		
		String urlCarpeta = crearCarpetaArchivosZip();
		if(urlCarpeta == null) 
			return false;

		String nombreArchivoZip = idAutogenerado + ".zip";
		crearArchivos(tramas, urlCarpeta);
		if(!zipearArchivos(urlCarpeta, nombreArchivoZip, totalNroSpool))
			return false;
		
		if(!enviarCorreoSpool(objAjusteBean.getTipoNotificacion(), urlCarpeta, nombreArchivoZip))
			return false;
		
		return true;
	}
	
	private void registrarAsientoContable(AjusteBean objAjusteBean) {
		Map<String, List<AsientoContableBean>> mapAsientoContable = siopDAO.obtenerAsientoContable(mensajeTransaccion, objAjusteBean.getProcesoId());
		
		AuditType auditType=new AuditType();
		auditType.setIdTransaccion(idTransaction);
		auditType.setIpApplicacion(Util.getIPHost());
		auditType.setNombreAplicacion(propertiesExterno.ws_portalSap_auditRequest_nombreAplicacion);
		auditType.setUsuarioAplicacion(propertiesExterno.ws_portalSap_auditRequest_usuarioAplicacion);
		
		GenerarAsientoRequest request = new GenerarAsientoRequest();
		request.setAuditRequest(auditType);
		request.setVersion(propertiesExterno.ws_portalSap_version);
		
		GenerarAsientoResponse response;
		for (Map.Entry<String, List<AsientoContableBean>> entry : mapAsientoContable.entrySet()) {
			if (entry.getValue() == null || entry.getValue().isEmpty()) {
				continue;
			}
			
			request.setListaAsiento(cargarListaAsiento(entry.getValue()));
			response = portalSapWS.generarAsiento(mensajeTransaccion, request);
			
			String claveReferenciaSAP = null;
			if(response.getAuditResponse() != null && 
			   response.getAuditResponse().getCodigoRespuesta().equals(Constantes.CODIGO_RESPUESTA_EXITOSA)){
				
//				String claveReferenciaSAP = null;
				if(response.getClaveReferencia().indexOf(Constantes.SEPARADOR_SAP) != -1) {
					claveReferenciaSAP = response.getClaveReferencia().split(Constantes.SEPARADOR_SAP)[0];
					siopDAO.guardarAsientoContable(mensajeTransaccion, objAjusteBean.getProcesoId(), entry.getKey(), claveReferenciaSAP);
				}
				
//				mapTipoAjusteAsiento.put(entry.getKey(), claveReferenciaSAP);
			}
			mapTipoAjusteAsiento.put(entry.getKey(), claveReferenciaSAP);
	    }
		
		generarRegistroVenta(objAjusteBean);
	}
	
	private ListaAsiento cargarListaAsiento(List<AsientoContableBean> listaAsientoContable) {
		ListaAsiento listaAsiento = new ListaAsiento(); 
		AsientoBean objAsientoBean;
		
		for(AsientoContableBean objAsientoContable : listaAsientoContable) {
			objAsientoBean = new AsientoBean();
			objAsientoBean.setTipoRegistro(objAsientoContable.getTipoRegistro());
		    objAsientoBean.setFechaDocumento(objAsientoContable.getFechaDocumento());
		    objAsientoBean.setClaseDocumento(objAsientoContable.getClaseDocumento());
		    objAsientoBean.setSociedad(objAsientoContable.getSociedad());
		    objAsientoBean.setFechaContabilizacion(objAsientoContable.getFechaContabilizacion());
		    objAsientoBean.setFechaConversion(objAsientoContable.getFechaConversion());
		    objAsientoBean.setClaveMoneda(objAsientoContable.getClaveMoneda());
		    objAsientoBean.setNumeroDocRef(objAsientoContable.getNumeroDocRef());
		    objAsientoBean.setTextoCabecera(objAsientoContable.getTextoCabecera());
		    objAsientoBean.setClaveContabilizacion(objAsientoContable.getClaveContabilizacion());
		    objAsientoBean.setCuenta(objAsientoContable.getCuenta());
		    objAsientoBean.setIndicador(objAsientoContable.getIndicador());
		    logger.info(mensajeTransaccion + "objAsientoContable.getImporte(): " + objAsientoContable.getImporte());
		    objAsientoBean.setImporte(objAsientoContable.getImporte());
		    objAsientoBean.setClaveCondicion(objAsientoContable.getClaveCondicion());
		    objAsientoBean.setFechaBase(objAsientoContable.getFechaBase());
		    objAsientoBean.setClaveBloqueo(objAsientoContable.getClaveBloqueo());
		    objAsientoBean.setNumeroAsignacion(objAsientoContable.getNumeroAsignacion());
		    objAsientoBean.setTextoPosicion(objAsientoContable.getTextoPosicion());
		    objAsientoBean.setCentroCoste(objAsientoContable.getCentroCoste());
		    objAsientoBean.setCentroBeneficio(objAsientoContable.getCentroBeneficio());
		    objAsientoBean.setNumeroOrden(objAsientoContable.getNumeroOrden());
		    objAsientoBean.setTipoCambio(objAsientoContable.getTipoCambio());
		    objAsientoBean.setCalcImpuesto(objAsientoContable.getCalcImpuesto());
		    objAsientoBean.setIndIVA(objAsientoContable.getIndIVA());
		    objAsientoBean.setIndRetencion(objAsientoContable.getIndRetencion());
		    objAsientoBean.setPosicionPresupuestaria(objAsientoContable.getPosicionPresupuestaria());
		    objAsientoBean.setMesContable(objAsientoContable.getMesContable());
		    objAsientoBean.setViaPago(objAsientoContable.getViaPago());
		    objAsientoBean.setCentroGestor(objAsientoContable.getCentroGestor());
		    objAsientoBean.setDivision(objAsientoContable.getDivision());
		    objAsientoBean.setImporteLocal(objAsientoContable.getImporteLocal());
		    objAsientoBean.setImporteCtaMayor(objAsientoContable.getImporteCtaMayor());
		    objAsientoBean.setElementoPlanEstructura(objAsientoContable.getElementoPlanEstructura());
			
			listaAsiento.getListaAsientoBean().add(objAsientoBean);
		}
		return listaAsiento;
	}
	
	private void  generarRegistroVenta(AjusteBean objAjusteBean) {
		List<String> listaRegistroVenta = siopDAO.obtenerRegistroVenta(mensajeTransaccion, objAjusteBean.getProcesoId());
		if(listaRegistroVenta == null)
			return;
		
		String urlCarpeta = crearCarpetaArchivosFiles();
		if(urlCarpeta == null) 
			return;
		
		String nombreArchivo = idAutogenerado + ".csv";
		boolean archivoCreado = Util.generarArchivo(mensajeTransaccion,  urlCarpeta, nombreArchivo, listaRegistroVenta);
		if(archivoCreado) {
//			subirArchivoFTP(urlCarpeta, nombreArchivo);
			copiarRegistroVenta(urlCarpeta, nombreArchivo);
		}
	}
	
	private void copiarRegistroVenta(String urlCarpetaOrigen, String nombreArchivo) {
		log("Se copiara registro de venta");
		String archivoOrigen = urlCarpetaOrigen + nombreArchivo;
		String archivoDestino = propertiesExterno.ruta_registro_ventas + nombreArchivo;
		log("archivoOrigen: " + archivoOrigen);
		log("archivoDestino: " + archivoDestino);
		boolean archivoCopiado = Util.copiarArchivo(mensajeTransaccion, archivoOrigen, archivoDestino);
		
		if (archivoCopiado) {
			log("Registro de venta copiado");
			listaEstados.put(EstadosProceso.REGISTRO_VENTA_GENERADO, null);
		}
	}
	
//	private void subirArchivoFTP(String urlCarpeta, String nombreArchivo) {
//		ChannelSftp objCanalSFTP = Util.conectarFileServer(mensajeTransaccion, propertiesExterno.fileServer_servidor, 
//																			   propertiesExterno.fileServer_usuario, 
//																			   propertiesExterno.fileServer_password, 
//																			   propertiesExterno.fileServer_puerto, 
//																			   propertiesExterno.fileServer_timeout, 
//																			   propertiesExterno.fileServer_protocolo);
//		if(objCanalSFTP == null)
//			return;
//		
//		boolean archivoEnviado = Util.enviarArchivoFileServer(mensajeTransaccion, objCanalSFTP, urlCarpeta, propertiesExterno.fileServer_ruta_remota, nombreArchivo);
//		Util.desconectarFileServer(mensajeTransaccion, objCanalSFTP);
//		
//		if(archivoEnviado)
//			listaEstados.put(EstadosProceso.REGISTRO_VENTA_GENERADO, null);
//	}
	
	private void enviarDetalleEjecucion(AjusteBean objAjusteBean) {
		String estadoSpool = EstadosSpool.NO_GENERADO.name(); 
		//String estadoAsientoContable = Constantes.TEXTO_VACIO;
		String estadoRegVenta = EstadosRegistroVenta.NO_GENERADO.name();
		
		if(listaEstados.containsKey(EstadosProceso.SPOOL_GENERADO))
			estadoSpool = EstadosSpool.GENERADO.name();
		
		if(listaEstados.containsKey(EstadosProceso.REGISTRO_VENTA_GENERADO))
			estadoRegVenta = EstadosRegistroVenta.GENERADO.name();
		
		log("mapTipoAjusteAsiento.size(): " + mapTipoAjusteAsiento.size());
		
		String tipoAjuste;
		for (Map.Entry<String, String> entry : mapTipoAjusteAsiento.entrySet()) {
			tipoAjuste = entry.getKey();
			//estadoAsientoContable = entry.getValue();
//			DetalleEjecucionBean objDetalleEjecucionBean = siopDAO.obtenerDetalleEjecucionHistorico(mensajeTransaccion, objAjusteBean.getProcesoId(), 
//					tipoAjuste, estadoSpool, estadoAsientoContable, estadoRegVenta);
			DetalleEjecucionBean objDetalleEjecucionBean = siopDAO.obtenerDetalleEjecucionHistorico(mensajeTransaccion, objAjusteBean.getProcesoId(), 
					tipoAjuste, estadoSpool, estadoRegVenta);
			
			if(objDetalleEjecucionBean != null)
				enviarCorreoDetalleEjecucion(objDetalleEjecucionBean, objAjusteBean);
		}
	}
				
	private boolean enviarCorreoSpool(String tipoNotificacion, String urlCarpeta, String nombreArchivoZip) {
		AuditTypeRequest auditTypeRequest=new AuditTypeRequest();
		auditTypeRequest.setIdTransaccion(idTransaction);
		auditTypeRequest.setIpAplicacion(Util.getIPHost());
		auditTypeRequest.setCodigoAplicacion(propertiesExterno.ws_envioCorreo_auditRequest_codigoAplicacion);
		auditTypeRequest.setUsrAplicacion(propertiesExterno.ws_envioCorreo_auditRequest_usuarioAplicacion);
		
		EnviarCorreoRequest request = new EnviarCorreoRequest();
		request.setAuditRequest(auditTypeRequest);
		request.setAsunto(propertiesExterno.ws_envioCorreo_spool_asunto);
		request.setRemitente(propertiesExterno.ws_envioCorreo_spool_remitente);
		request.setDestinatario(propertiesExterno.ws_envioCorreo_spool_destinatario);
		request.setHtmlFlag(propertiesExterno.ws_envioCorreo_spool_htmlFlag);
		request.setListaArchivosAdjuntos(getArchivoAdjunto(urlCarpeta, nombreArchivoZip));
		
		if(tipoNotificacion.equals(TipoNotificacion.ENVIO_ASESOR.name()))
			request.setMensaje(propertiesExterno.ws_envioCorreo_spool_mensaje_asesor);
		if(tipoNotificacion.equals(TipoNotificacion.ENVIO_DISTRIBUCION.name()))
			request.setMensaje(propertiesExterno.ws_envioCorreo_spool_mensaje_distribucion);
		if(tipoNotificacion.equals(TipoNotificacion.CARGA_REPOSITORIO.name()))
			request.setMensaje(propertiesExterno.ws_envioCorreo_spool_mensaje_repositorio);
		
		EnviarCorreoResponse response = envioCorreoWS.enviarCorreo(mensajeTransaccion, request);
		if(response.getAuditResponse() != null && response.getAuditResponse().getCodigoRespuesta().equals(Constantes.CODIGO_RESPUESTA_EXITOSA))
			return true;
		
		return false;
	}
	
	private void enviarCorreoDetalleEjecucion(DetalleEjecucionBean objDetalleEjecucionBean, AjusteBean objAjusteBean) {
		AuditTypeRequest auditTypeRequest=new AuditTypeRequest();
		auditTypeRequest.setIdTransaccion(idTransaction);
		auditTypeRequest.setIpAplicacion(Util.getIPHost());
		auditTypeRequest.setCodigoAplicacion(propertiesExterno.ws_envioCorreo_auditRequest_codigoAplicacion);
		auditTypeRequest.setUsrAplicacion(propertiesExterno.ws_envioCorreo_auditRequest_usuarioAplicacion);
		
		EnviarCorreoRequest request = new EnviarCorreoRequest();
		request.setAuditRequest(auditTypeRequest);
		request.setAsunto(propertiesExterno.ws_envioCorreo_detalleEjecucion_asunto);
		request.setRemitente(propertiesExterno.ws_envioCorreo_detalleEjecucion_remitente);
		request.setDestinatario(propertiesExterno.ws_envioCorreo_detalleEjecucion_destinatario);
		request.setHtmlFlag(propertiesExterno.ws_envioCorreo_detalleEjecucion_htmlFlag);
		request.setMensaje(getCuerpoCorreo(objDetalleEjecucionBean, objAjusteBean));
		
		envioCorreoWS.enviarCorreo(mensajeTransaccion, request);
	}
	
	private String getCuerpoCorreo(DetalleEjecucionBean objDetalleEjecucionBean, AjusteBean objAjusteBean) {
		String mensaje = propertiesExterno.ws_envioCorreo_detalleEjecucion_mensaje;
		mensaje = mensaje.replace(Constantes.CUERPO_EMAIL_TEXTO_REMPLAZAR, 
				  				  Constantes.CUERPO_EMAIL_TEXTO_NOMBRE_ARCHIVO.replace(Constantes.CUERPO_EMAIL_NOMBRE_ARCHIVO_REMPLAZAR, objAjusteBean.getNombreArchivo()));
		
		mensaje = mensaje.replace(Constantes.CUERPO_EMAIL_SPOOL_REMPLAZAR, objDetalleEjecucionBean.getSpool());
		mensaje = mensaje.replace(Constantes.CUERPO_EMAIL_ASIENTO_CONTABLE_REMPLAZAR, objDetalleEjecucionBean.getAsientoContable());
		mensaje = mensaje.replace(Constantes.CUERPO_EMAIL_REGISTRO_VENTA_REMPLAZAR, objDetalleEjecucionBean.getRegistroVenta());
		return mensaje;
	}
	
	private ListaArchivosAdjuntos getArchivoAdjunto(String urlCarpeta, String nombreArchivoZip) {
		ArchivoAdjunto archivoAdjunto = new ArchivoAdjunto();
		archivoAdjunto.setArchivo(Util.convertirArchivoAByte(mensajeTransaccion, urlCarpeta + nombreArchivoZip));
		archivoAdjunto.setNombre(nombreArchivoZip);
		
		ListaArchivosAdjuntos listaArchivos = new ListaArchivosAdjuntos();
		listaArchivos.getArchivoAdjunto().add(archivoAdjunto);
		return listaArchivos;
	}
	
	private String crearCarpetaArchivosZip() {
		String urlCarpeta = propertiesExterno.ruta_archivos_generados + idTransaction + "/" + idAutogenerado + "/";
		return Util.crearCarpeta(mensajeTransaccion, urlCarpeta);
	}
	
	private String crearCarpetaArchivosFiles() {
		String urlCarpeta = propertiesExterno.fileServer_ruta_local + idTransaction + "/" + idAutogenerado + "/";
		return Util.crearCarpeta(mensajeTransaccion, urlCarpeta);
	}
	
	private void eliminarCarpetas() {
		File carpetaZIP = new File(propertiesExterno.ruta_archivos_generados + idTransaction);
		File carpetaFILES = new File(propertiesExterno.fileServer_ruta_local + idTransaction);
		Util.eliminarCarpeta(mensajeTransaccion, carpetaZIP);
		Util.eliminarCarpeta(mensajeTransaccion, carpetaFILES);
	}
	
	private void crearArchivos(Map<String, List<String>> tramas, String urlCarpeta) {
		for(Map.Entry<String, List<String>> trama : tramas.entrySet()) 
			Util.generarArchivo(mensajeTransaccion,  urlCarpeta, trama.getKey(), trama.getValue());
	}
	
	private boolean zipearArchivos(String urlCarpeta, String nombreArchivoZip, int totalNroSpool) {
		return Util.generarArchivoZip(mensajeTransaccion, urlCarpeta, nombreArchivoZip, idTransaction, totalNroSpool);
	}
	
	private Map<String, List<String>> generarSpoolTramaAjuste(int procesoId) {
		List<String> listaCajaCliente = siopDAO.generarSpool(mensajeTransaccion, procesoId, TipoSpool.CAJA_CLIENTE.name());
		List<String> listaCajaFactura = siopDAO.generarSpool(mensajeTransaccion, procesoId, TipoSpool.CAJA_FACTURA.name());
		List<String> listaTotalHp 	  = siopDAO.generarSpool(mensajeTransaccion, procesoId, TipoSpool.TOTAL_HP.name());
		List<String> listaCambioDatos = siopDAO.generarSpool(mensajeTransaccion, procesoId, TipoSpool.CAMBIO_DATOS.name());
		List<String> listaTramaAjuste = siopDAO.generarSpool(mensajeTransaccion, procesoId, TipoSpool.TRAMA_AJUSTE.name());
		
		Map<String, List<String>> tramas = new HashMap<String, List<String>>();
		
		if(listaCajaCliente != null && !listaCajaCliente.isEmpty())
			tramas.put(Constantes.ARCHIVO_NCND_CAJA_CLIENTE, listaCajaCliente);
		if(listaCajaFactura != null && !listaCajaFactura.isEmpty())
			tramas.put(Constantes.ARCHIVO_NCND_CAJA_FACTURA, listaCajaFactura);
		if(listaTotalHp != null && !listaTotalHp.isEmpty())
			tramas.put(Constantes.ARCHIVO_SPOOL_TOTAL_HP, listaTotalHp);
		if(listaCambioDatos != null && !listaCambioDatos.isEmpty())
			tramas.put(Constantes.ARCHIVO_SPOOL_NCND_CAMBIO_DATOS, listaCambioDatos);
		if(listaTramaAjuste != null && !listaTramaAjuste.isEmpty())
			tramas.put(Constantes.ARCHIVO_SPOOL_NCND_HP, listaTramaAjuste);
		
		return tramas;
	}
	
	private Map<String, List<String>> generarSpoolTramaLarga(int procesoId) {
		List<String> listaTramaLarga = siopDAO.generarSpool(mensajeTransaccion, procesoId, TipoSpool.TRAMA_LARGA.name());
		Map<String, List<String>> tramas = new HashMap<String, List<String>>();
		
		if(listaTramaLarga != null && !listaTramaLarga.isEmpty()) {
			listaTramaLarga.set(0, Constantes.CABECERA_SPOOL_REFACTURACION);
			tramas.put(Constantes.ARCHIVO_SPOOL_REFACTURACION, listaTramaLarga);
		}
		return tramas;
	}

	private void log(String texto) {
		logger.info(mensajeTransaccion + texto);
	}

}
 