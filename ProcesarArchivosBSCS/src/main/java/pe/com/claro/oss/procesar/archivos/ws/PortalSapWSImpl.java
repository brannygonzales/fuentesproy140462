package pe.com.claro.oss.procesar.archivos.ws;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import pe.com.claro.oss.procesar.archivos.util.JAXBUtilitarios;
import pe.com.claro.oss.procesar.archivos.util.PropertiesExternos;
import pe.com.claro.soa.message.portal.GenerarAsientoRequest;
import pe.com.claro.soa.message.portal.GenerarAsientoResponse;
import pe.com.claro.soa.service.portalsap.PtPortal;
@Component
public class PortalSapWSImpl implements PortalSapWS {
	
	private static Logger logger = Logger.getLogger(PortalSapWSImpl.class);
	
	@Autowired
	@Qualifier(value = "portalSapWS")
	private PtPortal portalSapWS;
	@Autowired
	private PropertiesExternos propertiesExternos;
	
	@Override
	public GenerarAsientoResponse generarAsiento(String mensajeTransaccion, GenerarAsientoRequest request) {
		GenerarAsientoResponse response = new GenerarAsientoResponse();
		long tiempoInicio = System.currentTimeMillis();

		try {
			mensajeTransaccion = mensajeTransaccion + "[generarAsiento] ";
			log(mensajeTransaccion, " Inicio WS: [" + propertiesExternos.ws_portalSap_wsdl + "] - Operación: [generarAsiento]");
			log(mensajeTransaccion, "Datos de entrada: \n[\n  " + JAXBUtilitarios.anyObjectToXmlText(mensajeTransaccion, request) + "\n]");

			response = portalSapWS.generarAsiento(request);

			log(mensajeTransaccion, "Datos de salida: \n[\n  " + JAXBUtilitarios.anyObjectToXmlText(mensajeTransaccion, response) + "\n]");
		} 
		catch (Exception e) {
			logException(mensajeTransaccion, e);
		} 
		finally {
			log(mensajeTransaccion, " Fin WS: [" + propertiesExternos.ws_portalSap_wsdl + "] - Operación: [generarAsiento] - Tiempo Total del proceso(ms)= " + (System.currentTimeMillis() - tiempoInicio) + " milisegundos.");
		}
		return response;
	}
	
	private static void log(String mensajeTransaccion, String texto) {
		logger.info(mensajeTransaccion + texto);
	}
	
	private static void logException(String mensajeTransaccion, Exception e) {
		logger.error(mensajeTransaccion + "ERROR: [Exception] - [" + e.getMessage() + "] ", e);
	}

}
