package pe.com.claro.oss.procesar.archivos.ws;

import pe.com.claro.soa.message.portal.GenerarAsientoRequest;
import pe.com.claro.soa.message.portal.GenerarAsientoResponse;

public interface PortalSapWS {
	
	public GenerarAsientoResponse generarAsiento(String mensajeTransaccion, GenerarAsientoRequest partgenerarAsientoRequest);

}