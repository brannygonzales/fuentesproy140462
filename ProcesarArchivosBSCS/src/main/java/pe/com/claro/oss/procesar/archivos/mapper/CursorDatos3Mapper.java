package pe.com.claro.oss.procesar.archivos.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pe.com.claro.oss.procesar.archivos.bean.AsientoContableBean;

public class CursorDatos3Mapper implements RowMapper<AsientoContableBean>{

	@Override
	public AsientoContableBean mapRow(ResultSet rs, int numeroFila) throws SQLException {
		AsientoContableBean objAsientoContableBean = new AsientoContableBean();
		objAsientoContableBean.setTipoRegistro(rs.getString("TIPO_REGISTRO"));
	    objAsientoContableBean.setFechaDocumento(rs.getString("FECHA_DOCUMENTO"));
	    objAsientoContableBean.setClaseDocumento(rs.getString("CLASE_DOCUMENTO"));
	    objAsientoContableBean.setSociedad(rs.getString("SOCIEDAD"));
	    objAsientoContableBean.setFechaContabilizacion(rs.getString("FECHA_CONTABILIZACION"));
	    objAsientoContableBean.setFechaConversion(rs.getString("FECHA_CONVERSION"));
	    objAsientoContableBean.setClaveMoneda(rs.getString("CLAVE_MONEDA"));
	    objAsientoContableBean.setNumeroDocRef(rs.getString("NUM_DOC_REF"));
	    objAsientoContableBean.setTextoCabecera(rs.getString("TEXTO_CABECERA"));
	    objAsientoContableBean.setClaveContabilizacion(rs.getString("CLAVE_CONTABILIZACION"));
	    objAsientoContableBean.setCuenta(rs.getString("CUENTA"));
	    objAsientoContableBean.setIndicador(rs.getString("INDICADOR"));
	    objAsientoContableBean.setImporte(rs.getString("IMPORTE"));
	    objAsientoContableBean.setClaveCondicion(rs.getString("CLAVE_CONDICION"));
	    objAsientoContableBean.setFechaBase(rs.getString("FECHA_BASE"));
	    objAsientoContableBean.setClaveBloqueo(rs.getString("CLAVE_BLOQUEO"));
	    objAsientoContableBean.setNumeroAsignacion(rs.getString("NUMERO_ASIGNACION"));
	    objAsientoContableBean.setTextoPosicion(rs.getString("TEXTO_POSICION"));
	    objAsientoContableBean.setCentroCoste(rs.getString("CENTRO_COSTE"));
	    objAsientoContableBean.setCentroBeneficio(rs.getString("CENTRO_BENEFICIO"));
	    objAsientoContableBean.setNumeroOrden(rs.getString("NUMERO_ORDEN"));
	    objAsientoContableBean.setTipoCambio(rs.getString("TIPO_CAMBIO"));
	    objAsientoContableBean.setCalcImpuesto(rs.getString("CALC_IMPUESTO"));
	    objAsientoContableBean.setIndIVA(rs.getString("IND_IVA"));
	    objAsientoContableBean.setIndRetencion(rs.getString("IND_RETENCION"));
	    objAsientoContableBean.setPosicionPresupuestaria(rs.getString("POSICION_PRESUPUESTARIA"));
	    objAsientoContableBean.setMesContable(rs.getString("MES_CONTABLE"));
	    objAsientoContableBean.setViaPago(rs.getString("VIA_PAGO"));
	    objAsientoContableBean.setCentroGestor(rs.getString("CENTRO_GESTOR"));
	    objAsientoContableBean.setDivision(rs.getString("DIVISION"));
	    objAsientoContableBean.setImporteLocal(rs.getString("IMPORTE_LOCAL"));
	    objAsientoContableBean.setImporteCtaMayor(rs.getString("IMPORTE_CTA_MAYOR"));
	    objAsientoContableBean.setElementoPlanEstructura(rs.getString("ELEMENTO_PLAN_ESTRUCTURA"));
		return objAsientoContableBean;
	}
	
}
