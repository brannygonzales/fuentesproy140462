package pe.com.claro.oss.procesar.archivos.bean;

public class DetalleEjecucionBean {
	
	private String spool;
	private String asientoContable;
	private String registroVenta;
	
	public String getSpool() {
		return spool;
	}
	public void setSpool(String spool) {
		this.spool = spool;
	}
	public String getAsientoContable() {
		return asientoContable;
	}
	public void setAsientoContable(String asientoContable) {
		this.asientoContable = asientoContable;
	}
	public String getRegistroVenta() {
		return registroVenta;
	}
	public void setRegistroVenta(String registroVenta) {
		this.registroVenta = registroVenta;
	}
		
}