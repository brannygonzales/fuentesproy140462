package pe.com.claro.oss.procesar.archivos.dao;

import java.util.List;
import java.util.Map;

import pe.com.claro.oss.procesar.archivos.bean.AjusteBean;
import pe.com.claro.oss.procesar.archivos.bean.AsientoContableBean;
import pe.com.claro.oss.procesar.archivos.bean.DetalleEjecucionBean;

public interface SiopDAO {
	
	public List<AjusteBean> obtenerAjustesCabeceraHistorico(String mensajeTransaccion);
	public List<String> generarSpool(String mensajeTransaccion, int procesoId, String tipo);
	public Map<String, List<AsientoContableBean>> obtenerAsientoContable(String mensajeTransaccion, int procesoId);
	public List<String> obtenerRegistroVenta(String mensajeTransaccion, int procesoId);
//	public DetalleEjecucionBean obtenerDetalleEjecucionHistorico(String mensajeTransaccion, int procesoId, String tipoAjuste, String estadoSpool, String estadoAsientoContable, String estadoRegVenta);
	public DetalleEjecucionBean obtenerDetalleEjecucionHistorico(String mensajeTransaccion, int procesoId, String tipoAjuste, String estadoSpool, String estadoRegVenta);
	public void guardarAsientoContable(String mensajeTransaccion, int procesoId, String tipoAjuste, String estadoAsientoContable);

}